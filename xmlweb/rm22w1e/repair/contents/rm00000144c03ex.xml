<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000X" variety="S000X">
<name>1VD-FTV COOLING</name>
<ttl id="12011_S000X_7C3K5_T00D8" variety="T00D8">
<name>WATER PUMP</name>
<para id="RM00000144C03EX" category="A" type-id="80001" name-id="CO6DD-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM00000144C03EX_01" type-id="01" category="01">
<s-1 id="RM00000144C03EX_01_0134" proc-id="RM22W0E___00006WP00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000144C03EX_01_0095" proc-id="RM22W0E___00006WM00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
<s2>
<ptxt>Disconnect the cables from the negative (-) main battery and sub-battery terminals.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144C03EX_01_0132" proc-id="RM22W0E___000058500000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 7 clips and upper radiator support seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0101">
<ptxt>REMOVE FRONT WHEEL RH</ptxt>
</s-1>
<s-1 id="RM00000144C03EX_01_0123" proc-id="RM22W0E___000057200000">
<ptxt>REMOVE FRONT FENDER APRON SEAL FRONT RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 clips and front fender apron seal front RH.</ptxt>
<figure>
<graphic graphicname="A177003" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0128" proc-id="RM22W0E___000032O00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="A271735" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Loosen the clip and remove the front fender splash shield LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0129" proc-id="RM22W0E___000032N00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Loosen the clip and remove the front fender splash shield RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0130" proc-id="RM22W0E___000032P00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts and No. 1 engine under cover.</ptxt>
<figure>
<graphic graphicname="A178461" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0067" proc-id="RM22W0E___000032M00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the radiator reservoir cap to drain the coolant in the radiator.</ptxt>
</s2>
<s2>
<ptxt>Loosen the oil filter bracket drain cock plug to drain the coolant in the engine.</ptxt>
<figure>
<graphic graphicname="A174741E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
<s2>
<ptxt>Tighten the oil filter bracket drain cock plug.</ptxt>
<figure>
<graphic graphicname="A177208" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0107" proc-id="RM22W0E___000057000000">
<ptxt>REMOVE NO. 3 ENGINE ROOM WIRE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 4 wire harness clamps.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 nuts and No. 3 engine room wire.</ptxt>
<figure>
<graphic graphicname="A174837" width="7.106578999in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0105" proc-id="RM22W0E___000032500000">
<ptxt>REMOVE NO. 1 ENGINE COVER SUB-ASSEMBLY (w/ Intercooler)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and No. 1 engine cover.</ptxt>
<figure>
<graphic graphicname="A174701" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0089" proc-id="RM22W0E___000034300000">
<ptxt>REMOVE AIR CLEANER CAP SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the hose clamp.</ptxt>
<figure>
<graphic graphicname="A174698" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the mass air flow meter connector and using a clip remover, detach the wire harness clamp from the air cleaner cap.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 clamps and remove the air cleaner cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0090" proc-id="RM22W0E___000034400000">
<ptxt>REMOVE NO. 1 AIR CLEANER HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the hose clamp and remove the No. 1 air cleaner hose.</ptxt>
<figure>
<graphic graphicname="A174699" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0098" proc-id="RM22W0E___000034500000">
<ptxt>REMOVE INTAKE AIR CONNECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Viscous Heater:</ptxt>
<ptxt>Disconnect the 2 connectors from the viscous with magnet clutch heater and water temperature sensor.</ptxt>
<figure>
<graphic graphicname="A174749E04" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>w/ Viscous Heater</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/o Viscous Heater:</ptxt>
<ptxt>Disconnect the connector from the water temperature sensor.</ptxt>
</s2>
<s2>
<ptxt>Using a clip remover, detach the 3 wire harness clamps.</ptxt>
</s2>
<s2>
<ptxt>Loosen the 2 hose clamps and remove the 2 bolts and intake air connector.</ptxt>
<figure>
<graphic graphicname="A174710" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0108" proc-id="RM22W0E___000034B00000">
<ptxt>REMOVE VANE PUMP OIL RESERVOIR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Insert a screwdriver between the reservoir and oil reservoir bracket, push the claw, and then disconnect the reservoir by pulling it upwards.</ptxt>
<figure>
<graphic graphicname="A177428" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0133" proc-id="RM22W0E___000034D00000">
<ptxt>REMOVE NO. 1 OIL RESERVOIR BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and bracket.</ptxt>
<figure>
<graphic graphicname="A184319" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0109" proc-id="RM22W0E___000034C00000">
<ptxt>REMOVE RADIATOR RESERVOIR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 hoses.</ptxt>
<figure>
<graphic graphicname="A174735" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and radiator reservoir.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0110" proc-id="RM22W0E___00005KB00000">
<ptxt>REMOVE VANE PUMP ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and vane pump.</ptxt>
<figure>
<graphic graphicname="C172309" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the vane pump.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0043">
<ptxt>DISCONNECT NO. 1 RADIATOR HOSE</ptxt>
</s-1>
<s-1 id="RM00000144C03EX_01_0099">
<ptxt>DISCONNECT NO. 2 RADIATOR HOSE</ptxt>
</s-1>
<s-1 id="RM00000144C03EX_01_0124" proc-id="RM22W0E___000034600000">
<ptxt>REMOVE V-RIBBED BELT (w/ Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the lock nut and turn the bolt counterclockwise.</ptxt>
<figure>
<graphic graphicname="A174847" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the V-ribbed belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0125" proc-id="RM22W0E___000034700000">
<ptxt>REMOVE NO. 1 IDLER PULLEY (w/ Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, cover, No. 1 idler pulley and collar.</ptxt>
<figure>
<graphic graphicname="A177430" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0126" proc-id="RM22W0E___000034800000">
<ptxt>REMOVE NO. 3 IDLER PULLEY (w/ Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the nut and No. 3 idler pulley.</ptxt>
<figure>
<graphic graphicname="A177431" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0127" proc-id="RM22W0E___000034900000">
<ptxt>REMOVE V-RIBBED BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a wrench to the V-ribbed belt tensioner bracket, turn the wrench clockwise and remove the V-ribbed belt.</ptxt>
<figure>
<graphic graphicname="A174849" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0131" proc-id="RM22W0E___000058300000">
<ptxt>REMOVE OIL COOLER TUBE (for Automatic Transmission)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the inlet and outlet No. 1 oil cooler hose.</ptxt>
<figure>
<graphic graphicname="C167861" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the inlet No. 2 and No. 3 oil cooler hoses.</ptxt>
<figure>
<graphic graphicname="C175574" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the inlet No. 4 oil cooler hose.</ptxt>
<figure>
<graphic graphicname="C175575" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and transmission oil cooler tube.</ptxt>
<figure>
<graphic graphicname="C167866" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0111" proc-id="RM22W0E___000057300000">
<ptxt>REMOVE FAN SHROUD WITH FAN
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 4 nuts holding the fluid coupling fan.</ptxt>
<figure>
<graphic graphicname="A161930" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts holding the fan shroud.</ptxt>
<figure>
<graphic graphicname="A177427" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 nuts of the fluid coupling fan, and then remove the shroud together with the fluid coupling fan.</ptxt>
<figure>
<graphic graphicname="A179253" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0042">
<ptxt>REMOVE FAN PULLEY</ptxt>
</s-1>
<s-1 id="RM00000144C03EX_01_0112" proc-id="RM22W0E___00006WN00000">
<ptxt>REMOVE HEATER WATER PIPE SUB-ASSEMBLY (w/ Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and disconnect the heater water pipe from the viscous heater with magnet clutch.</ptxt>
<figure>
<graphic graphicname="A179255" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0104" proc-id="RM22W0E___00005CY00000">
<ptxt>REMOVE VISCOUS WITH MAGNET CLUTCH HEATER ASSEMBLY (w/ Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154426" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the connector and detach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Using pliers, grip the claws of the clips and slide the 2 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 heater hoses.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and heater assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0113" proc-id="RM22W0E___00005CZ00000">
<ptxt>REMOVE NO. 1 IDLER PULLEY BRACKET (w/ Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and No. 1 idler pulley bracket.</ptxt>
<figure>
<graphic graphicname="A177432" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0114" proc-id="RM22W0E___00005D000000">
<ptxt>REMOVE WATER INLET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 oil cooler hose from the water pump and clamp.</ptxt>
<figure>
<graphic graphicname="A174815" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 bolts and water inlet.</ptxt>
<figure>
<graphic graphicname="A174816" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0115" proc-id="RM22W0E___00005D100000">
<ptxt>REMOVE THERMOSTAT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the thermostat.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the thermostat.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C03EX_01_0122" proc-id="RM22W0E___00005EK00000">
<ptxt>REMOVE NO. 2 IDLER PULLEY (w/ Viscous Heater)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, cover, No. 2 idler pulley and collar.</ptxt>
<figure>
<graphic graphicname="A177433" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144C03EX_01_0117" proc-id="RM22W0E___00005EL00000">
<ptxt>REMOVE NO. 2 IDLER PULLEY BRACKET (w/ Viscous Heater)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and idler pulley bracket.</ptxt>
<figure>
<graphic graphicname="E154425" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144C03EX_01_0121" proc-id="RM22W0E___00006WO00000">
<ptxt>DISCONNECT NO. 1 OIL COOLER HOSE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A174819" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1>
</s-1>
<s-1 id="RM00000144C03EX_01_0118" proc-id="RM22W0E___00005EM00000">
<ptxt>REMOVE FAN BRACKET SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and fan bracket.</ptxt>
<figure>
<graphic graphicname="A174820" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144C03EX_01_0119" proc-id="RM22W0E___00005NI00000">
<ptxt>REMOVE WATER OUTLET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the engine coolant temperature sensor connector.</ptxt>
<figure>
<graphic graphicname="A161939" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts, disconnect the water outlet from the No. 2 water hose joint, and remove the water outlet and gasket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144C03EX_01_0120" proc-id="RM22W0E___00005GK00000">
<ptxt>REMOVE WATER PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 9 bolts, 2 nuts, water pump and gasket.</ptxt>
<figure>
<graphic graphicname="A174821" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>