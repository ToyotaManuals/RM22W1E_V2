<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T8_T00MB" variety="T00MB">
<name>THEFT DETERRENT SYSTEM (w/ Entry and Start System)</name>
<para id="RM000000W1208LX" category="J" type-id="3027N" name-id="TD70C-02" from="201301" to="201308">
<dtccode/>
<dtcname>Security Indicator Light Circuit</dtcname>
<subpara id="RM000000W1208LX_01" type-id="60" category="03" proc-id="RM22W0E___0000EL100000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>When the theft deterrent system is in the disarmed state, the security indicator will flash continuously if the immobiliser system is set, or not illuminate if the immobiliser system is not set.</ptxt>
<ptxt>When the theft deterrent system is in the armed state, the immobiliser system is automatically set and the security indicator will flash continuously.</ptxt>
<ptxt>When the theft deterrent system is in the arming preparation state and alarm sounding state, the certification ECU (smart key ECU assembly) causes the security indicator to be illuminated.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000W1208LX_02" type-id="32" category="03" proc-id="RM22W0E___0000EL200000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B184353E20" width="7.106578999in" height="2.775699831in"/>
</figure>
<figure>
<graphic graphicname="B184353E18" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W1208LX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000W1208LX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W1208LX_04_0001" proc-id="RM22W0E___0000EL300000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (SECURITY INDICATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Entry&amp;Start / Active Test.</ptxt>
</test1>
<test1>
<ptxt>According to the display on the intelligent tester, perform the Active Test.</ptxt>
<table pgwide="1">
<title>Entry&amp;Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Security Indicator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Security indicator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON / OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Security indicator illuminates.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1208LX_04_0003" fin="true">OK</down>
<right ref="RM000000W1208LX_04_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208LX_04_0008" proc-id="RM22W0E___0000EL500000">
<testtitle>CHECK VEHICLE EQUIPMENT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the multi-display is equipped on the vehicle.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>w/ Multi-display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/o Multi-display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W1208LX_04_0002" fin="false">A</down>
<right ref="RM000000W1208LX_04_0011" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000W1208LX_04_0002" proc-id="RM22W0E___0000EL400000">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-DISPLAY - CERTIFICATION ECU [SMART KEY ECU ASSEMBLY] AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F79 multi-display assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E29 certification ECU (smart key ECU assembly) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F79-10 (SEUC) - E29-2 (IND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F79-13 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F79-10 (SEUC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1208LX_04_0014" fin="false">OK</down>
<right ref="RM000000W1208LX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208LX_04_0014" proc-id="RM22W0E___0000EL700000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (SECURITY INDICATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F79 multi-display assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Entry &amp; Start / Active Test.</ptxt>
</test1>
<test1>
<ptxt>Operate the certification ECU (smart key ECU assembly) using the active test function and measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tester Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-2 (IND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Security Indicator ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-2 (IND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Security Indicator OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>Before performing this inspection, check that the battery voltage is between 11 and 14 V (not depleted). </ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W1208LX_04_0013" fin="true">OK</down>
<right ref="RM000000W1208LX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208LX_04_0011" proc-id="RM22W0E___0000EL600000">
<testtitle>CHECK HARNESS AND CONNECTOR (CLOCK - CERTIFICATION ECU [SMART KEY ECU ASSEMBLY] AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the g1 clock connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E29 certification ECU (smart key ECU assembly) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>g1-9 (LP) - E29-2 (IND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>g1-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>g1-9 (LP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1208LX_04_0015" fin="false">OK</down>
<right ref="RM000000W1208LX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208LX_04_0015" proc-id="RM22W0E___0000EL800000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (SECURITY INDICATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the g1 clock connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Entry &amp; Start / Active Test.</ptxt>
</test1>
<test1>
<ptxt>Operate the certification ECU (smart key ECU assembly) using the active test function and measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tester Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>g1-9 (LP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Security Indicator ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>g1-9 (LP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Security Indicator OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>Before performing this inspection, check that the battery voltage is between 11 and 14 V (not depleted). </ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W1208LX_04_0007" fin="true">OK</down>
<right ref="RM000000W1208LX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208LX_04_0003">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM000000W1208LX_04_0012">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W1208LX_04_0013">
<testtitle>REPLACE MULTI-DISPLAY<xref label="Seep01" href="RM000003B6H01RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W1208LX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W1208LX_04_0007">
<testtitle>REPLACE CLOCK<xref label="Seep01" href="RM0000038YQ00CX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>