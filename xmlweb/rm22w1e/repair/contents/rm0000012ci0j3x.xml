<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R3_T00K6" variety="T00K6">
<name>AUDIO AND VISUAL SYSTEM (w/ Navigation System)</name>
<para id="RM0000012CI0J3X" category="J" type-id="302YN" name-id="AVC17-01" from="201301" to="201308">
<dtccode/>
<dtcname>Radio Receiver Power Source Circuit</dtcname>
<subpara id="RM0000012CI0J3X_01" type-id="60" category="03" proc-id="RM22W0E___0000BX200000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit provides power to the multi-media module receiver assembly.</ptxt>
</content5>
</subpara>
<subpara id="RM0000012CI0J3X_02" type-id="32" category="03" proc-id="RM22W0E___0000BX300000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C260264E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000012CI0J3X_03" type-id="51" category="05" proc-id="RM22W0E___0000BX400000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000012CI0J3X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012CI0J3X_04_0001" proc-id="RM22W0E___0000BX500000">
<testtitle>CHECK CHECK HARNESS AND CONNECTOR (MULTI-MEDIA MODULE RECEIVER ASSEMBLY -&#13;
BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the multi-media module receiver assembly connector.</ptxt>
<figure>
<graphic graphicname="C250794E13" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F77-12 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F77-15 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-16 (ACC1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (ACC)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-17 (+B1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Multi-media Module Receiver Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM0000012CI0J3X_04_0003" fin="true">OK</down>
<right ref="RM0000012CI0J3X_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012CI0J3X_04_0003">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000011BR0KXX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012CI0J3X_04_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>