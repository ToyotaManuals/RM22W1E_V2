<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R3_T00K6" variety="T00K6">
<name>AUDIO AND VISUAL SYSTEM (w/ Navigation System)</name>
<para id="RM000003WPX066X" category="C" type-id="803MI" name-id="AVCCW-01" from="201301" to="201308">
<dtccode>B15B8</dtccode>
<dtcname>Harddisk Malfunction</dtcname>
<subpara id="RM000003WPX066X_01" type-id="60" category="03" proc-id="RM22W0E___0000C0C00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when the hard disc which is built into the multi-media module receiver assembly has an initialization malfunction or read/write errors occur more than a specified number of times.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B15B8</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Hard Disk Drive (HDD) malfunction</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Hard disc</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003WPX066X_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003WPX066X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003WPX066X_04_0001" proc-id="RM22W0E___0000C0D00000">
<testtitle>CHECK HARD DISC (OPERATION CHECK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Enter the "Failure Diagnosis" screen. Refer to Check Hard Disc in Operation Check (See page <xref label="Seep01" href="RM000003SKF0DBX"/>).</ptxt>
<figure>
<graphic graphicname="E225797E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Select "HDD Check" and check the result.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>"OK" is displayed.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>After selecting "HDD Check", it may take a while until the result is displayed.</ptxt>
</item>
<item>
<ptxt>If the cabin temperature is -20°C (-4°F) or lower, or 65°C (149°F) or higher, the hard disc may not operate normally, and "NG" may be shown on the display. Make sure to perform the inspection with the cabin at an appropriate temperature.</ptxt>
</item>
</list1>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003WPX066X_04_0006" fin="false">OK</down>
<right ref="RM000003WPX066X_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WPX066X_04_0006" proc-id="RM22W0E___0000C0G00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0N6X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WPX066X_04_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WPX066X_04_0004" proc-id="RM22W0E___0000C0F00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTCs are output again (See page <xref label="Seep01" href="RM0000011BU0N6X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The hard disc may be malfunctioning even if DTC B15B8 is not output.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003WPX066X_04_0002" fin="true">OK</down>
<right ref="RM000003WPX066X_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WPX066X_04_0003" proc-id="RM22W0E___0000C0E00000">
<testtitle>REPLACE HARD DISC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the hard disc with a new one (See page <xref label="Seep01" href="RM000002V0L01CX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WPX066X_04_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WPX066X_04_0007" proc-id="RM22W0E___0000C0H00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0N6X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WPX066X_04_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WPX066X_04_0008" proc-id="RM22W0E___0000C0I00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTCs are output again (See page <xref label="Seep01" href="RM0000011BU0N6X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The hard disc may be malfunctioning even if DTC B15B8 is not output.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003WPX066X_04_0010" fin="true">OK</down>
<right ref="RM000003WPX066X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WPX066X_04_0005">
<testtitle>REPLACE HARD DISC<xref label="Seep01" href="RM000002V0L01CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WPX066X_04_0002">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WPX066X_04_0009">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER  ASSEMBLY<xref label="Seep01" href="RM000003AHY01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WPX066X_04_0010">
<testtitle>END (HARD DISC IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>