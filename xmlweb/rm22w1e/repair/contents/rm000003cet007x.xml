<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002K" variety="S002K">
<name>WIPER / WASHER</name>
<ttl id="12068_S002K_7C3YL_T00RO" variety="T00RO">
<name>WIPER ECU</name>
<para id="RM000003CET007X" category="A" type-id="80001" name-id="WW5AT-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM000003CET007X_02" type-id="11" category="10" proc-id="RM22W0E___0000IVM00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for LHD and RHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003CET007X_01" type-id="01" category="01">
<s-1 id="RM000003CET007X_01_0032" proc-id="RM22W0E___0000IVL00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003CET007X_01_0002" proc-id="RM22W0E___0000IVJ00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003CET007X_01_0025" proc-id="RM22W0E___0000BC600001">
<ptxt>REMOVE INSTRUMENT SIDE PANEL RH (w/o Airbag Cut Off Switch)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181912E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 6 claws and remove the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003CET007X_01_0026" proc-id="RM22W0E___0000BC700001">
<ptxt>REMOVE INSTRUMENT SIDE PANEL RH (w/ Airbag Cut Off Switch)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180301E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 6 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003CET007X_01_0009" proc-id="RM22W0E___0000BOW00001">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY (w/ Floor Under Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180656" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the No. 2 instrument panel under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003CET007X_01_0031" proc-id="RM22W0E___0000AU300001">
<ptxt>REMOVE FRONT DOOR SCUFF PLATE RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM000003CET007X_01_0006" proc-id="RM22W0E___0000D7M00001">
<ptxt>REMOVE FRONT PASSENGER SIDE KNEE AIRBAG ASSEMBLY (w/ Passenger Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
<figure>
<graphic graphicname="B189606E01" width="2.775699831in" height="6.791605969in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the front passenger side knee airbag.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000003CET007X_01_0027" proc-id="RM22W0E___0000D7N00001">
<ptxt>REMOVE LOWER INSTRUMENT PANEL (w/o Passenger Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180302" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the lower instrument panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003CET007X_01_0028" proc-id="RM22W0E___0000D7O00001">
<ptxt>REMOVE NO. 3 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180303E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 6 claws and remove the No. 3 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003CET007X_01_0029" proc-id="RM22W0E___0000D7Q00001">
<ptxt>REMOVE INSTRUMENT PANEL BOX DOOR KNOB
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182302" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws and remove the instrument panel box door knob.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003CET007X_01_0030" proc-id="RM22W0E___0000D7P00001">
<ptxt>REMOVE LOWER NO. 2 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180304" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the lower No. 2 instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003CET007X_01_0010" proc-id="RM22W0E___0000IVK00001">
<ptxt>REMOVE WINDSHIELD WIPER ECU</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 connectors.</ptxt>
<figure>
<graphic graphicname="E160828" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and windshield wiper ECU.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>