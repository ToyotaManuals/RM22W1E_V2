<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM00000182V0J6X" category="C" type-id="803MB" name-id="NSACU-01" from="201301" to="201308">
<dtccode>B15C2</dtccode>
<dtcname>Speed Signal Malfunction</dtcname>
<subpara id="RM00000182V0J6X_01" type-id="60" category="03" proc-id="RM22W0E___0000CKJ00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The multi-media module receiver assembly receives a vehicle speed signal from the combination meter assembly and information from the navigation antenna, and then adjusts the vehicle position on the map. The multi-media module receiver assembly stores this DTC when the difference between the speed information that the navigation antenna assembly*1 or telephone antenna assembly*2 receives and the SPD pulse received from the combination meter assembly becomes large.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/o Roof Antenna</ptxt>
</item>
<item>
<ptxt>*2: w/ Roof Antenna</ptxt>
</item>
</list1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A voltage of 12 V or 5 V is output from each ECU and then input to the combination meter assembly. The signal is changed to a pulse signal at the transistor in the combination meter assembly. Each ECU controls the respective systems based on the pulse signal.</ptxt>
</item>
<item>
<ptxt>If a short occurs in any of the ECUs or in the wire harness connected to an ECU, all systems in the diagram below will not operate normally.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B15C2</ptxt>
</entry>
<entry valign="middle">
<ptxt>A difference between the GPS speed and SPD pulse is detected</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Meter / gauge system</ptxt>
</item>
<item>
<ptxt>Multi-media module receiver assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000182V0J6X_04" type-id="32" category="03" proc-id="RM22W0E___0000CKK00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E240105E11" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000182V0J6X_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM00000182V0J6X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000182V0J6X_05_0016" proc-id="RM22W0E___0000CKM00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0N7X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000182V0J6X_05_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000182V0J6X_05_0017" proc-id="RM22W0E___0000CKN00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTC is output again (See page <xref label="Seep01" href="RM0000011BU0N7X"/>). </ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000182V0J6X_05_0018" fin="true">OK</down>
<right ref="RM00000182V0J6X_05_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182V0J6X_05_0001" proc-id="RM22W0E___0000CKL00000">
<testtitle>CHECK VEHICLE SENSOR (OPERATION CHECK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Enter the "Vehicle Sensors" screen. Refer to Check GPS &amp; Vehicle Sensor in Operation Check (See page <xref label="Seep01" href="RM000003SKF0DCX"/>).</ptxt>
<figure>
<graphic graphicname="E176549" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>While driving the vehicle, compare the "Speed" indicator to the reading on the speedometer. Check if these readings are almost equal.</ptxt>
<atten4>
<ptxt>The combination meter assembly receives the vehicle speed signal from the skid control ECU via CAN communication. Therefore, perform the following inspection referring to values on the Data List of the skid control ECU because it is the source of the vehicle speed signal.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Vehicle speed displayed on the "Vehicle Sensors" screen is almost the same as the actual vehicle speed measured using the intelligent tester (See page <xref label="Seep02" href="RM000001DWY028X"/>).</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000182V0J6X_05_0019" fin="true">OK</down>
<right ref="RM00000182V0J6X_05_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182V0J6X_05_0018">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000182V0J6X_05_0010">
<testtitle>GO TO METER / GAUGE SYSTEM<xref label="Seep01" href="RM000002ZM601EX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000182V0J6X_05_0019">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01SX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>