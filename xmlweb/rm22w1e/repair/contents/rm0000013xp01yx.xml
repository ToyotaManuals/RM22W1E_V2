<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12013_S0017" variety="S0017">
<name>1VD-FTV STARTING</name>
<ttl id="12013_S0017_7C3LP_T00ES" variety="T00ES">
<name>GLOW PLUG</name>
<para id="RM0000013XP01YX" category="A" type-id="80001" name-id="ST622-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM0000013XP01YX_02" type-id="11" category="10" proc-id="RM22W0E___000076000000">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing an injector (including interchanging injectors between cylinders) or common rail, replace the corresponding injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>w/DPF</ptxt>
<ptxt>When fuel lines are disconnected, air may enter the fuel lines, leading to engine starting trouble. Therefore, perform forced regeneration and bleed the air from the fuel lines.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM0000013XP01YX_01" type-id="01" category="01">
<s-1 id="RM0000013XP01YX_01_0050" proc-id="RM22W0E___000075Y00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000013XP01YX_01_0051" proc-id="RM22W0E___000075Z00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000013XP01YX_01_0027" proc-id="RM22W0E___000075W00000">
<ptxt>REMOVE INTERCOOLER ASSEMBLY (w/ Intercooler)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000031FS003X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000013XP01YX_01_0049" proc-id="RM22W0E___000075X00000">
<ptxt>REMOVE INJECTION PIPES</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000031FK00RX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000013XP01YX_01_0009" proc-id="RM22W0E___00005DL00000">
<ptxt>REMOVE NO. 1 GLOW PLUG CONNECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screw grommets and 2 nuts, and disconnect the 2 wire harnesses.</ptxt>
</s2>
<s2>
<ptxt>Remove the 8 screw grommets, 8 nuts and 2 glow plug connectors.</ptxt>
<figure>
<graphic graphicname="A269587E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>LH</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013XP01YX_01_0010" proc-id="RM22W0E___00005DM00000">
<ptxt>REMOVE GLOW PLUG ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 10 mm deep socket wrench, remove the 8 glow plugs.</ptxt>
<figure>
<graphic graphicname="A269588E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>LH</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Before removing the glow plugs, thoroughly clean off all dirt, sand, etc.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>