<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7C3A9_T003C" variety="T003C">
<name>1VD-FTV STARTING</name>
<para id="RM000001987018X" category="F" type-id="30027" name-id="SS09L-24" from="201301">
<name>SERVICE DATA</name>
<subpara id="RM000001987018X_z0" proc-id="RM22W0E___000008200000">
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<tbody>
<row>
<entry>
<ptxt>Starter assembly</ptxt>
</entry>
<entry namest="COL2" nameend="COL3">
<ptxt>No-load characteristics current</ptxt>
</entry>
<entry>
<ptxt>200 A or less at 11 V</ptxt>
</entry>
</row>
<row>
<entry morerows="6">
<ptxt>Starter armature assembly</ptxt>
</entry>
<entry morerows="1">
<ptxt>Standard resistance</ptxt>
</entry>
<entry>
<ptxt>Commutator</ptxt>
</entry>
<entry>
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Commutator - Armature coil core</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry namest="COL2" nameend="COL3">
<ptxt>Maximum circle runout</ptxt>
</entry>
<entry>
<ptxt>0.05 mm (0.00197 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL2" nameend="COL3">
<ptxt>Standard commutator diameter</ptxt>
</entry>
<entry>
<ptxt>36.0 mm (1.42 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL2" nameend="COL3">
<ptxt>Minimum commutator diameter</ptxt>
</entry>
<entry>
<ptxt>35.0 mm (1.38 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL2" nameend="COL3">
<ptxt>Standard undercut depth</ptxt>
</entry>
<entry>
<ptxt>0.7 mm (0.0276 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL2" nameend="COL3">
<ptxt>Minimum undercut depth</ptxt>
</entry>
<entry>
<ptxt>0.2 mm (0.00787 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Starter yoke assembly</ptxt>
</entry>
<entry morerows="1">
<ptxt>Standard resistance</ptxt>
</entry>
<entry>
<ptxt>Terminal C plate - Brushes</ptxt>
</entry>
<entry>
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Brushes - Starter yoke body</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Brush length</ptxt>
</entry>
<entry namest="COL2" nameend="COL3">
<ptxt>Standard length</ptxt>
</entry>
<entry>
<ptxt>21.0 mm (0.827 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL2" nameend="COL3">
<ptxt>Minimum length</ptxt>
</entry>
<entry>
<ptxt>12.0 mm (0.472 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Starter brush holder assembly</ptxt>
</entry>
<entry>
<ptxt>Standard resistance</ptxt>
</entry>
<entry>
<ptxt>Positive (+) brush holder - Negative (-) brush holder</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Brush spring</ptxt>
</entry>
<entry namest="COL2" nameend="COL3">
<ptxt>Standard spring installed load</ptxt>
</entry>
<entry>
<ptxt>36 N (3.6 kgf, 8.0 lbf)</ptxt>
</entry>
</row>
<row>
<entry namest="COL2" nameend="COL3">
<ptxt>Minimum spring installed load</ptxt>
</entry>
<entry>
<ptxt>12 N (1.2 kgf, 2.7 lbf)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Magnet starter switch assembly</ptxt>
</entry>
<entry morerows="1">
<ptxt>Standard resistance</ptxt>
</entry>
<entry>
<ptxt>Terminal 50 - Terminal C</ptxt>
</entry>
<entry>
<ptxt>Below 0.3 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Terminal 50 - Housing body</ptxt>
</entry>
<entry>
<ptxt>Below 0.8 Ω</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Glow plug</ptxt>
</entry>
<entry>
<ptxt>Standard resistance</ptxt>
<ptxt>w/o DPF</ptxt>
</entry>
<entry morerows="1">
<ptxt>Glow plug terminal - Body ground</ptxt>
</entry>
<entry>
<ptxt>Approximately 1 Ω at 20°C (68°F)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Standard resistance</ptxt>
<ptxt>w/ DPF</ptxt>
</entry>
<entry>
<ptxt>0.7 Ω or higher at 20°C (68°F)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Starter relay</ptxt>
</entry>
<entry morerows="1">
<ptxt>Standard resistance</ptxt>
</entry>
<entry morerows="1">
<ptxt>3 - 5</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher (Battery voltage is not applied to terminals 1 and 2)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Below 1 Ω (Battery voltage is applied to terminals 1 and 2)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Starter cut relay</ptxt>
</entry>
<entry morerows="1">
<ptxt>Standard resistance</ptxt>
</entry>
<entry morerows="1">
<ptxt>3 - 5</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher (Battery voltage is not applied to terminals 1 and 2)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Below 1 Ω (Battery voltage is applied to terminals 1 and 2)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>ACC relay</ptxt>
<ptxt>(Main body ECU)</ptxt>
</entry>
<entry morerows="1">
<ptxt>Standard resistance</ptxt>
</entry>
<entry morerows="1">
<ptxt>2A-1 - 2D-8</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher (Battery voltage is not applied between 2D-45 and 2D-62)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Below 1 Ω (Battery voltage is applied between 2D-45 and 2D-62)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>No. 1 glow relay</ptxt>
</entry>
<entry morerows="1">
<ptxt>Standard resistance</ptxt>
</entry>
<entry morerows="1">
<ptxt>3 - 5</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher (Battery voltage is not applied to terminals 1 and 2)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Below 1 Ω (Battery voltage is applied to terminals 1 and 2)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>No. 2 Glow relay</ptxt>
</entry>
<entry morerows="1">
<ptxt>Standard resistance</ptxt>
</entry>
<entry morerows="1">
<ptxt>3 - 5</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher (Battery voltage is not applied to terminals 1 and 2)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Below 1 Ω (Battery voltage is applied to terminals 1 and 2)</ptxt>
</entry>
</row>
<row>
<entry morerows="3">
<ptxt>Engine switch</ptxt>
</entry>
<entry morerows="3">
<ptxt>Standard resistance</ptxt>
</entry>
<entry>
<ptxt>7 - 5</ptxt>
</entry>
<entry>
<ptxt>Below 1 Ω (Engine switch is pushed)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>2 - 5</ptxt>
</entry>
<entry>
<ptxt>Below 1 Ω (Engine switch is pushed)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>7 - 5</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher (Engine switch is not pushed)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>2 - 5</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher (Engine switch is not pushed)</ptxt>
</entry>
</row>
<row>
<entry morerows="3">
<ptxt>ignition switch</ptxt>
</entry>
<entry morerows="3">
<ptxt>Standard resistance</ptxt>
</entry>
<entry>
<ptxt>All combinations of all terminals</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher (LOCK)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>4 (AM1) - 2 (ACC)</ptxt>
</entry>
<entry>
<ptxt>Below 1 Ω (ACC)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>4 (AM1) - 2 (ACC)</ptxt>
<ptxt>4 (AM1) - 1 (IG1)</ptxt>
<ptxt>5 (AM2) - 6 (IG2)</ptxt>
</entry>
<entry>
<ptxt>Below 1 Ω (ON)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>4 (AM1) - 1 (IG1)</ptxt>
<ptxt>4 (AM1) - 3 (ST1)</ptxt>
<ptxt>5 (AM2) - 6 (IG2)</ptxt>
</entry>
<entry>
<ptxt>Below 1 Ω (START)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>