<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM000002PPN053X" category="C" type-id="803TA" name-id="ESYM8-03" from="201301" to="201308">
<dtccode>P1258</dtccode>
<dtcname>VNT Position Sensor Range/Performance Bank 2 Sensor 1</dtcname>
<dtccode>P1259</dtccode>
<dtcname>VNT Position Sensor Low Bank 2 Sensor 1</dtcname>
<dtccode>P1260</dtccode>
<dtcname>VNT Position Sensor High Bank 2 Sensor 1</dtcname>
<dtccode>P1262</dtccode>
<dtcname>VNT Position Sensor Low Bank 2 Sensor 2</dtcname>
<dtccode>P1263</dtccode>
<dtcname>VNT Position Sensor High Bank 2 Sensor 2</dtcname>
<dtccode>P2563</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Position Sensor "A" Circuit Range/Performance</dtcname>
<dtccode>P2564</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Position Sensor "A" Circuit Low</dtcname>
<dtccode>P2565</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Position Sensor "A" Circuit High</dtcname>
<dtccode>P2588</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Position Sensor "B" Circuit Low</dtcname>
<dtccode>P2589</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Position Sensor "B" Circuit High</dtcname>
<subpara id="RM000002PPN053X_01" type-id="60" category="03" proc-id="RM22W0E___000030I00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The variable nozzle vane type turbocharger consists primarily of a compressor wheel, turbine wheel, nozzle vane, unison ring, DC motor and nozzle vane position sensor.</ptxt>
<ptxt>The nozzle vane position sensor consists of a Hall IC and a magnetic yoke that rotates in unison with the movement of the linkage that actuates the nozzle vane. The nozzle vane position sensor converts the changes in the magnetic flux that are caused by the rotation of the DC motor (hence, the rotation of the magnetic yoke) into electric signals, and outputs them to the turbo motor driver. The turbo motor driver determines the actual nozzle vane position from the electric signals in order to calculate the target nozzle vane position.</ptxt>
<figure>
<graphic graphicname="A183002E02" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>P1258</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>The difference between VTI2 and VTA2 voltage is 0.8 V or more for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P1259</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>VTI2 voltage is 0.5 V or less for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in nozzle vane position sensor circuit</ptxt>
</item>
<item>
<ptxt>Turbo motor driver</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P1260</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>VTI2 voltage is 4.5 V or more for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in nozzle vane position sensor circuit</ptxt>
</item>
<item>
<ptxt>Turbo motor driver</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P1262</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>VTA2 voltage is 0.5 V or less for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in nozzle vane position sensor circuit</ptxt>
</item>
<item>
<ptxt>Turbo motor driver</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P1263</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>VTA2 voltage is 4.5 V or more for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in nozzle vane position sensor circuit</ptxt>
</item>
<item>
<ptxt>Turbo motor driver</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2563</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>The difference between VTA1 and VTA voltage is 0.8 V or more for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2564</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>VTA1 voltage is 0.5 V or less for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in nozzle vane position sensor circuit</ptxt>
</item>
<item>
<ptxt>Turbo motor driver</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2565</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>VTA1 voltage is 4.5 V or more for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in nozzle vane position sensor circuit</ptxt>
</item>
<item>
<ptxt>Turbo motor driver</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2588</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>VTA voltage is 0.5 V or less for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in nozzle vane position sensor circuit</ptxt>
</item>
<item>
<ptxt>Turbo motor driver</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2589</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>VTA voltage is 4.5 V or more for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in nozzle vane position sensor circuit</ptxt>
</item>
<item>
<ptxt>Turbo motor driver</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTC P1258, P1259, P1260, P1262, P1263, P2563, P2564, P2565, P2588 and/or P2589 is stored, the following symptoms may appear:</ptxt>
<ptxt>Stuck open malfunction:</ptxt>
<list1 type="nonmark">
<item>
<ptxt>- Lack of power</ptxt>
</item>
<item>
<ptxt>- Vehicle surge or hesitation under light or medium load</ptxt>
</item>
<item>
<ptxt>- White smoke</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000002PPN053X_02" type-id="32" category="03" proc-id="RM22W0E___000030J00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A182511E03" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002PPN053X_03" type-id="51" category="05" proc-id="RM22W0E___000030K00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002PPN053X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002PPN053X_04_0008" proc-id="RM22W0E___000030N00000">
<testtitle>INSPECT TURBO MOTOR DRIVER (for Bank 1 and Bank 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear fender splash shield sub-assembly RH.</ptxt>
</test1>
<test1>
<ptxt>Remove the front fender liner LH.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
</test1>
<atten4>
<ptxt>Record the output DTCs.</ptxt>
</atten4>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Interchange the connectors for the turbo motor driver of bank 1 and bank 2.</ptxt>
<figure>
<graphic graphicname="A176217" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<spec>
<title>Result</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display (DTC Output)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTCs change</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTCs do not change</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002PPN053X_04_0001" fin="false">A</down>
<right ref="RM000002PPN053X_04_0012" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002PPN053X_04_0001" proc-id="RM22W0E___000030L00000">
<testtitle>CHECK HARNESS AND CONNECTOR (TURBO MOTOR DRIVER - NOZZLE VANE POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the nozzle vane position sensor connector.</ptxt>
<figure>
<graphic graphicname="A184079E02" width="2.775699831in" height="5.787629434in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the turbo motor driver connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for Bank 1</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-5 (VTAI) - C89-1 (VTA1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-2 (VNE2) - C89-2 (VNE2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-8 (VNVC) - C89-3 (VNVC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-6 (VTA) - C89-4 (VTA)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-1 (E2S) - C89-5 (E2S)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-7 (VCS) - C89-6 (VCS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-5 (VTI2) - z66-1 (VTI2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-2 (VE2) - z66-2 (VE2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-8 (VNC2) - z66-3 (VNC2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-6 (VTA2) - z66-4 (VTA2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-1 (E2S2) - z66-5 (E2S2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-7 (VCS2) - z66-6 (VCS2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for Bank 1</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.39in"/>
<colspec colname="COLSPEC2" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-5 (VTAI) or C89-1 (VTA1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-2 (VNE2) or C89-2 (VNE2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-8 (VNVC) or C89-3 (VNVC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-6 (VTA) or C89-4 (VTA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-1 (E2S) or C89-5 (E2S) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-7 (VCS) or C89-6 (VCS) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-5 (VTI2) or z66-1 (VTI2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-2 (VE2) or z66-2 (VE2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-8 (VNC2) or z66-3 (VNC2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-6 (VTA2) or z66-4 (VTA2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-1 (E2S2) or z66-5 (E2S2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbo motor driver connector-7 (VCS2) or z66-6 (VCS2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</content6>
<res>
<down ref="RM000002PPN053X_04_0002" fin="false">OK</down>
<right ref="RM000002PPN053X_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PPN053X_04_0002" proc-id="RM22W0E___000030M00000">
<testtitle>REPLACE TURBOCHARGER SUB-ASSEMBLY (for Bank 1 or Bank 2) (NOZZLE VANE POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the turbocharger sub-assembly (for Bank 1 or Bank 2) (See page <xref label="Seep01" href="RM0000032A801WX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000002PPN053X_04_0014" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000002PPN053X_04_0012" proc-id="RM22W0E___000030O00000">
<testtitle>REPLACE TURBO MOTOR DRIVER (for Bank 1 or Bank 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the turbo motor driver (for Bank 1 or Bank 2) (See page <xref label="Seep01" href="RM000003A1500AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000002PPN053X_04_0014" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000002PPN053X_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<res>
<down ref="RM000002PPN053X_04_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002PPN053X_04_0014" proc-id="RM22W0E___000030P00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off and leave the vehicle for 15 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the tester to determine whether or not the DTC judgment has been carried out</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Powertrain / Engine / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P1258, P1259, P1260, P1262, P1263, P2563, P2564, P2565, P2588 and/or P2589.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or UNKNOWN, idle the engine.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002PPN053X_04_0015" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002PPN053X_04_0015">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>