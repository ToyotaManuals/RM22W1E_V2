<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UZ_T00O2" variety="T00O2">
<name>REAR SEAT CUSHION HEATER (for 60/40 Split Seat Type 60 Side)</name>
<para id="RM00000395R00VX" category="A" type-id="80001" name-id="SE6SW-03" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM00000395R00VX_02" type-id="11" category="10" proc-id="RM22W0E___0000G8I00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000395R00VX_01" type-id="01" category="01">
<s-1 id="RM00000395R00VX_01_0020" proc-id="RM22W0E___0000G8H00000">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep01" href="RM00000390Z00NX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000395R00VX_01_0001" proc-id="RM22W0E___0000G2K00000">
<ptxt>REMOVE SEAT ADJUSTER BOLT COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184012" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0002" proc-id="RM22W0E___0000G2L00000">
<ptxt>REMOVE RECLINING ADJUSTER RELEASE HANDLE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184013" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw and release handle.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0003" proc-id="RM22W0E___0000G2M00000">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184014E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws in the order shown in the illustration, and then remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0004" proc-id="RM22W0E___0000G2N00000">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184015" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0005" proc-id="RM22W0E___0000G2O00000">
<ptxt>REMOVE UPPER SEAT TRACK RAIL COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181222" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0006" proc-id="RM22W0E___0000G2P00000">
<ptxt>REMOVE UPPER SEAT TRACK RAIL COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184035" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0007" proc-id="RM22W0E___0000G2R00000">
<ptxt>REMOVE REAR NO. 2 SEAT LEG SIDE COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 screws and detach the 4 claws.</ptxt>
<figure>
<graphic graphicname="B181224" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt that is securing the fold seat stopper band and remove the seat leg side cover together with the fold seat stopper band.</ptxt>
<figure>
<graphic graphicname="B184044" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0008" proc-id="RM22W0E___0000G2J00000">
<ptxt>REMOVE REAR NO. 2 SEAT PROTECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Detach the claw and remove the wire harness connector.</ptxt>
<figure>
<graphic graphicname="B184016" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Detach the claw and remove the wire harness connector.</ptxt>
</s2>
<s2>
<ptxt>Using a clip remover, remove the clip.</ptxt>
<figure>
<graphic graphicname="B182595" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and remove the seat protector from the seat hinge.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws, open the protector and remove wire harness.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0009" proc-id="RM22W0E___0000G2V00000">
<ptxt>REMOVE REAR NO. 1 SEAT HINGE SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="B184045" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 nuts and hinge.</ptxt>
</s2>
<s2>
<ptxt>Detach the cable clamp and disconnect the cable.</ptxt>
<figure>
<graphic graphicname="B181176" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0010" proc-id="RM22W0E___0000G2W00000">
<ptxt>REMOVE REAR NO. 1 SEAT HINGE SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184017" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 nuts and hinge.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0011" proc-id="RM22W0E___0000G2X00000">
<ptxt>REMOVE SEAT BELT ANCHOR COVER CAP
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw and open the cap.</ptxt>
<figure>
<graphic graphicname="B181227" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw and anchor cover cap.</ptxt>
<figure>
<graphic graphicname="B181228" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0012" proc-id="RM22W0E___0000G2Y00000">
<ptxt>REMOVE REAR SEATBACK STAY COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181226" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0013" proc-id="RM22W0E___0000G2Z00000">
<ptxt>REMOVE REAR UNDER SIDE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 10 claws.</ptxt>
<figure>
<graphic graphicname="B181229" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 5 screws.</ptxt>
<figure>
<graphic graphicname="B181230" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 clips and 2 claws, and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0014" proc-id="RM22W0E___0000G3100000">
<ptxt>REMOVE REAR SEAT CUSHION COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181231" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0015" proc-id="RM22W0E___0000G3200000">
<ptxt>REMOVE REAR SEAT UNDER TRAY COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181181" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0021" proc-id="RM22W0E___0000G6Z00000">
<ptxt>REMOVE REAR NO. 1 SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184531E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and belt.</ptxt>
<atten4>
<ptxt>The No. 1 seat 3 point type belt anchor is fixed with the same bolt as the rear No. 1 seat inner belt LH. Therefore, it becomes disconnected when the bolt is removed.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0022" proc-id="RM22W0E___0000G7000000">
<ptxt>DISCONNECT NO. 1 SEAT 3 POINT TYPE BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184533E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the belt anchor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0017" proc-id="RM22W0E___0000G3400000">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<s3>
<ptxt>Detach the side airbag wire harness clamps.</ptxt>
</s3>
<s3>
<ptxt>Remove the side airbag wire harness through the hole in the seat cushion.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<s3>
<ptxt>Disconnect the 2 seat heater connectors.</ptxt>
<figure>
<graphic graphicname="B182596" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the seatback heater wire harness through the hole in the seat cushion. </ptxt>
<figure>
<graphic graphicname="B182597" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Remove the seat cushion cover with pad.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0018" proc-id="RM22W0E___0000G3500000">
<ptxt>REMOVE REAR SEPARATE TYPE SEAT CUSHION COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 pieces of fastening tape.</ptxt>
<figure>
<graphic graphicname="B181234E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the hog rings and cover.</ptxt>
<figure>
<graphic graphicname="B181236" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395R00VX_01_0019" proc-id="RM22W0E___0000G8G00000">
<ptxt>REMOVE REAR SEAT CUSHION HEATER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181187" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Cut off the tack pins which fasten the seat heater to the seat cushion cover, and then remove the seat cushion heater from the seat cushion cover.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>