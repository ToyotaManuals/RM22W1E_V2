<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000000PDS0XRX" category="D" type-id="304JI" name-id="ESZJM-01" from="201301">
<name>FREEZE FRAME DATA</name>
<subpara id="RM000000PDS0XRX_z0" proc-id="RM22W0E___00003MS00000">
<content5 releasenbr="1">
<step1>
<ptxt>DESCRIPTION</ptxt>
<list1 type="nonmark">
<item>
<ptxt>The ECM records vehicle and driving condition information as freeze frame data the moment a DTC is stored. When troubleshooting, freeze frame data can be helpful in determining whether the vehicle was moving or stationary, whether the air fuel ratio was lean or rich, as well as the other data recorded at the time of a malfunction.</ptxt>
<atten4>
<ptxt>If it is impossible to duplicate the problem even though a DTC is stored, confirm the freeze frame data.</ptxt>
</atten4>
</item>
</list1>
<list1 type="nonmark">
<item>
<ptxt>The ECM records engine conditions in the form of freeze frame data every 0.5 seconds. Using an GTS, 5 separate sets of freeze frame data can be checked.</ptxt>
<figure>
<graphic graphicname="A103809E49" width="2.775699831in" height="2.775699831in"/>
</figure>
</item>
<list2 type="unordered">
<item>
<ptxt>3 data sets from before the DTC was stored</ptxt>
</item>
<item>
<ptxt>1 data set from when the DTC was stored</ptxt>
</item>
<item>
<ptxt>1 data set from after the DTC was stored</ptxt>
</item>
</list2>
<list2 type="nonmark">
<item>
<ptxt>These data sets can be used to simulate the condition of the vehicle from around the time of the occurrence of the malfunction. The data may assist in identifying the cause of the malfunction, and in judging whether it was temporary or not.</ptxt>
</item>
</list2>
</list1>
</step1>
<step1>
<ptxt>PENDING FREEZE FRAME DATA</ptxt>
<atten4>
<ptxt>Pending freeze frame data is stored when a 2 trip DTC is first detected during the first trip.</ptxt>
</atten4>
<step2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the GTS on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</step2>
<step2>
<ptxt>Select a DTC in order to display its pending freeze frame data.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Pending freeze frame data is cleared when any of the following occurs.</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Using the GTS, the DTCs are cleared.</ptxt>
</item>
</list2>
<list2 type="unordered">
<item>
<ptxt>The cable is disconnected from the negative (-) auxiliary battery terminal.</ptxt>
</item>
</list2>
<list2 type="unordered">
<item>
<ptxt>40 trips with the engine fully warmed up have been performed after returning to normal. (Pending freeze frame data will not be cleared by only returning the system to normal.)</ptxt>
</item>
</list2>
<item>
<ptxt>With previous pending freeze frame data stored, if pending freeze frame data is newly stored when a 2 trip DTC is detected in the first trip, the old freeze frame data will be replaced with the new one of the newly detected DTC in the next trip.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="A251781E08" width="7.106578999in" height="5.787629434in"/>
</figure>
<figure>
<graphic graphicname="A251782E11" width="7.106578999in" height="5.787629434in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>LIST OF FREEZE FRAME DATA</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle speed</ptxt>
</entry>
<entry>
<ptxt>The speed indicated on the speedometer.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Target Idle Engine Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target idling speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Calculate Load</ptxt>
</entry>
<entry valign="middle">
<ptxt>Calculated load</ptxt>
</entry>
<entry valign="middle">
<ptxt>The load calculated by the ECM.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MAF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mass air flow volume</ptxt>
</entry>
<entry valign="middle">
<ptxt>If approximately 0.0 gm/sec:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The mass air flow meter power source circuit is open or shorted.</ptxt>
</item>
<item>
<ptxt>The VG circuit is open or shorted.</ptxt>
</item>
<item>
<ptxt>The E2G circuit is open.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Atmosphere Pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>Atmospheric pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MAP</ptxt>
</entry>
<entry valign="middle">
<ptxt>Absolute pressure inside intake manifold</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Coolant Temp</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine coolant temperature</ptxt>
</entry>
<entry valign="middle">
<ptxt>If -40°C (-40°F), the sensor circuit is open.</ptxt>
<ptxt>If 140°C (284°F) or higher, the sensor circuit is shorted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Intake Air</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake air temperature</ptxt>
</entry>
<entry valign="middle">
<ptxt>If -40°C (-40°F), the sensor circuit is open.</ptxt>
<ptxt>If 140°C (284°F) or higher, the sensor circuit is shorted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Intake Air Temp (Turbo)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake air temperature after intercooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>If -40°C (-40°F), the sensor circuit is open.</ptxt>
<ptxt>If 190°C (374°F) or higher, the sensor circuit is shorted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine Run Time</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accumulated engine running time</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Initial Engine Coolant Temp</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine coolant temperature at engine start</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Initial Intake Air Temp</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake air temperature at engine start</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery Voltage</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Alternate Duty Ratio</ptxt>
</entry>
<entry valign="middle">
<ptxt>Alternate duty ratio</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Viscous Heater Stop Request</ptxt>
</entry>
<entry valign="middle">
<ptxt>Viscous heater stop request</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Glow Relay Request</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of the glow relay request (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Glow Relay Request #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of the glow relay request (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Glow Indicator</ptxt>
</entry>
<entry valign="middle">
<ptxt>Glow Indicator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Accel Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accelerator pedal position sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Accel Sens. No.1 Volt %</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accelerator pedal position No. 1 output voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Accel Sens. No.2 Volt %</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accelerator pedal position No. 2 output voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Target Throttle Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target throttle position (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Target Throttle Position #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target throttle position (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Actual Throttle Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual throttle position (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Actual Throttle Position #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual throttle position (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Throttle Motor DUTY</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle actuator (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Throttle Motor DUTY #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle actuator (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Throttle Close Learning Val.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle fully closed position learned value (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Throttle Close Learning Val.#2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle fully closed position learned value (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Throttle Sensor Volt %</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle position sensor output voltage (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Throttl Sensor #2 Volt %</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle position sensor output voltage (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Injection Volume</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injection volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Inj. FB Vol. for Idle</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injection feedback value for idle speed control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Inj Vol Feedback Learning</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injection volume feedback learned value</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Idle Signal Output Value</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idle signal output value (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Idle Signal Output Value #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idle signal output value (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Injection Feedback Val #1 to Val #8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injection volume correction for cylinders 1 to 8</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pilot 1 Injection Period</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pilot 1 injection period</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pilot 2 Injection Period</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pilot 2 injection period</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Main Injection Period</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main injection period</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>After Injection Period</ptxt>
</entry>
<entry valign="middle">
<ptxt>Post injection period</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pilot 1 Injection Timing</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pilot 1 injection timing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pilot 2 Injection Timing</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pilot 2 injection timing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Main Injection Timing</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main injection timing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>After Injection Timing</ptxt>
</entry>
<entry valign="middle">
<ptxt>Post injection timing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Injector Pilot Quantity Learning</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pilot quantity learning status</ptxt>
</entry>
<entry valign="middle">
<ptxt>The status is only displayed while performing "Pilot Quantity Learning".</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Injection Pressure Correction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injection pressure feedback compensation volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Injection EDU Relay Request</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of the EDU relay request (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Injection EDU Relay Request #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of the EDU relay request (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Target Common Rail Pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target common rail pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Common Rail Pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fuel Temperature</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Target Pump SCV Current</ptxt>
</entry>
<entry valign="middle">
<ptxt>Final pump current target value</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pump SCV Learning Value</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pump current learned value</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pump SCV Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pump SCV Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pump SCV Duty Request</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pump SCV duty request</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pressure Discharge Valve</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressure discharge valve operation prohibition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>AF Lambda B1S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lambda equivalent ratio (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>AF Lambda B2S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lambda equivalent ratio (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>AFS Voltage B1S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor output voltage (bank 1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Performing control the injection volume or control the injection volume for air fuel ratio sensor function of Active Test enables technician to check output voltage of sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>AFS Voltage B2S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor output voltage (bank 2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Performing control the injection volume or control the injection volume for air fuel ratio sensor function of Active Test enables technician to check output voltage of sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>AFS Current B1S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor output current (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>AFS Current B2S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor output current (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>AF Sensor Learning Value</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor learning value (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>AF Sensor Learning Value #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor learning value (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Target EGR Valve Pos</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target EGR valve position (No. 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Target EGR Valve Pos #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target EGR valve position (No. 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Actual EGR Valve Pos</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual EGR valve position (No. 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Actual EGR Valve Pos #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual EGR valve position (No. 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>EGR Position Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR Position Sensor position (No. 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>EGR Position Sensor #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR Position Sensor position (No. 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>EGR Motor Duty #1</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR motor duty ratio (No. 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>EGR Motor Duty #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR motor duty ratio (No. 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>EGR Close Lrn. Val.</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR valve fully closed position learned value (No. 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>EGR Close Lrn. Val. #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR valve fully closed position learned value (No. 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Target Booster Pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target booster pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Target VN Turbo Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target VN turbo position (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Target VN Turbo Position #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target VN turbo position (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Actual VN Turbo Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual VN turbo position (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Actual VN Turbo Position #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual VN turbo position (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VN Position Sensor Out</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN position sensor output voltage (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VN Position Sensor Out #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN position sensor output voltage (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VN Motor Duty</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN turbo motor duty (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VN Motor Duty #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN turbo motor duty (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VN Close Learn Value</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN turbo fully closed position learned value (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VN Close Learn Value #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN turbo fully closed position learned value (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VN Turbo Max Angle</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN turbo maximum opening amount</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VN Turbo Min Angle</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN turbo minimum opening amount</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Temperature B1S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust temperature sensor (B1S1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If 0°C (32°F), sensor circuit is open</ptxt>
<ptxt>If 1000°C (1832°F) or more, sensor circuit is shorted</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Temperature B1S2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 exhaust temperature sensor (B1S2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If 0°C (32°F), sensor circuit is open</ptxt>
<ptxt>If 1000°C (1832°F) or more, sensor circuit is shorted</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Temperature B1S3</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 4 exhaust temperature sensor (B1S3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If 0°C (32°F), sensor circuit is open</ptxt>
<ptxt>If 1000°C (1832°F) or more, sensor circuit is shorted</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Temperature B2S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust temperature sensor (B2S1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If 0°C (32°F), sensor circuit is open</ptxt>
<ptxt>If 1000°C (1832°F) or more, sensor circuit is shorted</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Temperature B2S2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 3 exhaust temperature sensor (B2S2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If 0°C (32°F), sensor circuit is open</ptxt>
<ptxt>If 1000°C (1832°F) or more, sensor circuit is shorted</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Temperature B2S3</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 4 exhaust temperature sensor (B2S3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If 0°C (32°F), sensor circuit is open</ptxt>
<ptxt>If 1000°C (1832°F) or more, sensor circuit is shorted</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DPF Differential Pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>DPF differential pressure (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DPF Differential Pressure #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>DPF differential pressure (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Diff. Press. Sensor Corr.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Differential pressure sensor 0 point learned value (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Diff Press Sensor Corr #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Differential pressure sensor 0 point learned value (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Fuel Addition Injector Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of the exhaust fuel addition injector status (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Fuel Addition Injector Status #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of the exhaust fuel addition injector status (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Fuel Addition FB</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust fuel addition correction value (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Fuel Addition FB #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust fuel addition correction value (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DPNR/DPF Status Reju(PM)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PM forced regeneration status</ptxt>
</entry>
<entry valign="middle">
<ptxt>The status is only displayed while performing "Activate the DPF Rejuvenate (PM)"</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>PM Accumulation Ratio</ptxt>
</entry>
<entry valign="middle">
<ptxt>PM accumulation ratio (bank 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>PM Accumulation Ratio #2</ptxt>
</entry>
<entry valign="middle">
<ptxt>PM accumulation ratio (bank 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Starter Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Starter Control</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of the starter control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Power Steering Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power steering switch status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Starter Relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter relay status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ACC Relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>ACC relay status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Neutral Position SW Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Neutral position switch signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Transfer L4</ptxt>
</entry>
<entry valign="middle">
<ptxt>L4 status of the transfer</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stop Light Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A/C Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air conditioning signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Idle Up Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of the idle up signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Immobiliser Communication</ptxt>
</entry>
<entry valign="middle">
<ptxt>Immobiliser communication</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>TC Terminal</ptxt>
</entry>
<entry valign="middle">
<ptxt>TC terminal status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Time after DTC Cleared</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cumulative time after DTC cleared</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Distance from DTC Cleared</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accumulated distance after DTC cleared</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Warmup Cycle Cleared DTC</ptxt>
</entry>
<entry valign="middle">
<ptxt>Warmup cycles after DTC cleared</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Dist Batt Cable Disconnect</ptxt>
</entry>
<entry valign="middle">
<ptxt>Distance driven after battery cable disconnected</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>IG OFF Elapsed Time</ptxt>
</entry>
<entry valign="middle">
<ptxt>Time after engine switch turned off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>TC and TE1</ptxt>
</entry>
<entry valign="middle">
<ptxt>TC and TE1 terminals of DLC3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine Speed (Starter Off)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine speed when starter off</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1604 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Starter Count</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter on count</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1604 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Run Dist of Previous Trip</ptxt>
</entry>
<entry valign="middle">
<ptxt>Distance driven during previous trip</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1604 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Glow Request Lighting Time</ptxt>
</entry>
<entry valign="middle">
<ptxt>Glow request lighting time</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1604 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>IG-ON Time</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON time before engine start</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1604 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MAF Low</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mass airflow low</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1608 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Boost Pressure Low</ptxt>
</entry>
<entry valign="middle">
<ptxt>Boost pressure low</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1608 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Common Rail Pressure Low</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common rail pressure low</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1608 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine Coolant Temp High</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine coolant temperature high</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1608 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MAF/Estimate MAF Ratio</ptxt>
</entry>
<entry valign="middle">
<ptxt>Indicates the ratio of estimated intake air volume and the actual intake air volume</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1608 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rough Idle #1 to #8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rough idle status of cylinders 1 to 8</ptxt>
</entry>
<entry valign="middle">
<ptxt>This item is used for troubleshooting when DTC P1605 is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Electric Duty Feedback Value</ptxt>
</entry>
<entry valign="middle">
<ptxt>Electric load feedback value</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A/C Duty Feedback Value</ptxt>
</entry>
<entry valign="middle">
<ptxt>A/C load feedback value</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine Starting Time</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine starting time</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Minimum Engine Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Minimum engine speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ACM Inhibit</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSV for engine mount status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ACT VSV</ptxt>
</entry>
<entry valign="middle">
<ptxt>A/C cut status for Active Test</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Immobiliser Fuel Cut History</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of the immobiliser fuel cut history</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>