<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM00000187T07MX" category="C" type-id="302L2" name-id="ES169C-001" from="201301" to="201308">
<dtccode>P0405</dtccode>
<dtcname>Exhaust Gas Recirculation Sensor "A" Circuit Low</dtcname>
<dtccode>P0406</dtccode>
<dtcname>Exhaust Gas Recirculation Sensor "A" Circuit High</dtcname>
<dtccode>P0407</dtccode>
<dtcname>Exhaust Gas Recirculation Sensor "B" Circuit Low</dtcname>
<dtccode>P0408</dtccode>
<dtcname>Exhaust Gas Recirculation Sensor "B" Circuit High</dtcname>
<subpara id="RM00000187T07MX_01" type-id="60" category="03" proc-id="RM22W0E___00003ER00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The EGR valve position sensor is mounted on the EGR valve and used for detecting the lift amount of the valve. The lift amount detected by the sensor is provided to the ECM as feedback. The ECM then regulates the lift amount of the valve in accordance with engine running conditions.</ptxt>
<table pgwide="1">
<title>P0405 (No. 1), P0407 (No. 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR valve position sensor output voltage is less than 0.1 V for 5 seconds.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Open in EGR valve position sensor circuit</ptxt>
</item>
<item>
<ptxt>EGR valve assembly (EGR valve position sensor)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0406 (No. 1), P0408 (No. 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR valve position sensor output voltage is more than 4.9 V for 5 seconds.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Short in EGR valve position sensor circuit</ptxt>
</item>
<item>
<ptxt>EGR valve assembly (EGR valve position sensor)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000187T07MX_02" type-id="64" category="03" proc-id="RM22W0E___00003ES00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the output voltage of the EGR valve position sensor deviates from the normal operating range of 0.1 to 4.9 V for more than 5 seconds, the ECM interprets this as a malfunction of the sensor circuit, and illuminates the MIL.</ptxt>
</content5>
</subpara>
<subpara id="RM00000187T07MX_03" type-id="32" category="03" proc-id="RM22W0E___00003ET00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A182885E02" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000187T07MX_04" type-id="51" category="05" proc-id="RM22W0E___00003EU00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187T07MX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187T07MX_05_0003" proc-id="RM22W0E___00003EW00000">
<testtitle>CHECK HARNESS AND CONNECTOR (EGR VALVE POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 or No. 2 EGR valve assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>No. 1 (LHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.33in"/>
<colspec colname="COLSPEC0" colwidth="2.33in"/>
<colspec colname="COL2" colwidth="2.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C87-3 (VC) - C45-91 (VC)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C87-4 (EGLS) - C45-88 (EGLS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C87-2 (E2) - C45-92 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C87-3 (VC) or C45-91 (VC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C87-4 (EGLS) or C45-88 (EGLS) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>No. 2 (LHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.33in"/>
<colspec colname="COLSPEC0" colwidth="2.33in"/>
<colspec colname="COL2" colwidth="2.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C88-3 (VC) - C45-91 (VC)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C88-4 (EGS2) - C45-89 (EGS2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C88-2 (E2) - C45-92 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C88-3 (VC) or C45-91 (VC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C88-4 (EGS2) or C45-89 (EGS2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>No. 1 (RHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.33in"/>
<colspec colname="COLSPEC0" colwidth="2.33in"/>
<colspec colname="COL2" colwidth="2.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C87-3 (VC) - C46-91 (VC)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C87-4 (EGLS) - C46-88 (EGLS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C87-2 (E2) - C46-92 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C87-3 (VC) or C46-91 (VC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C87-4 (EGLS) or C46-88 (EGLS) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>No. 2 (RHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.33in"/>
<colspec colname="COLSPEC0" colwidth="2.33in"/>
<colspec colname="COL2" colwidth="2.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C88-3 (VC) - C46-91 (VC)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C88-4 (EGS2) - C46-89 (EGS2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C88-2 (E2) - C46-92 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C88-3 (VC) or C46-91 (VC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C88-4 (EGS2) or C46-89 (EGS2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187T07MX_05_0001" fin="false">OK</down>
<right ref="RM00000187T07MX_05_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187T07MX_05_0001" proc-id="RM22W0E___00003EV00000">
<testtitle>CHECK ECM TERMINAL VOLTAGE (VC TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 or No. 2 EGR valve assembly connector.</ptxt>
<figure>
<graphic graphicname="A165766E09" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<title>No. 1</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C87-3 (VC) - C87-2 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>No. 2</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C88-3 (VC) - C88-2 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>No. 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>No. 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to  EGR valve assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000187T07MX_05_0005" fin="false">OK</down>
<right ref="RM00000187T07MX_05_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187T07MX_05_0005" proc-id="RM22W0E___00003EY00000">
<testtitle>REPLACE EGR VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<ptxt>When DTC P0405 or P0406 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the No. 1 EGR valve assembly (See page <xref label="Seep01" href="RM0000031JM009X"/>).</ptxt>
</item>
</list1>
<ptxt>When DTC P0407 or P0408 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the No. 2 EGR valve assembly (See page <xref label="Seep02" href="RM0000031JM009X"/>).</ptxt>
</item>
</list1>
</content6>
<res>
<right ref="RM00000187T07MX_05_0007" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187T07MX_05_0004" proc-id="RM22W0E___00003EX00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187T07MX_05_0007" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187T07MX_05_0006" proc-id="RM22W0E___00003EZ00000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187T07MX_05_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187T07MX_05_0007" proc-id="RM22W0E___00003F000000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) for 5 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187T07MX_05_0008" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187T07MX_05_0008">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>