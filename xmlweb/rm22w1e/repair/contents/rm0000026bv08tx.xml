<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WO_T00PR" variety="T00PR">
<name>AIR CONDITIONING SYSTEM (for Manual Air Conditioning System)</name>
<para id="RM0000026BV08TX" category="C" type-id="305EX" name-id="ACGFO-02" from="201308">
<dtccode>B1422</dtccode>
<dtcname>Compressor Lock Sensor Circuit</dtcname>
<subpara id="RM0000026BV08TX_01" type-id="61" category="03" proc-id="RM22W0E___0000HCI00001">
<name>SYSTEM DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM sends the engine speed signal to the air conditioning amplifier assembly via CAN communication.</ptxt>
<ptxt>The air conditioning amplifier assembly sends an air conditioning operation request signal to the ECM via direct line, and the ECM sends a magnet clutch operation permission signal to the air conditioning amplifier assembly via direct line.</ptxt>
<ptxt>The air conditioning amplifier assembly reads the difference between compressor speed and engine speed. When the difference becomes too large, the air conditioning amplifier assembly determines that the cooler compressor assembly is locked and turns the magnet clutch assembly off.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1422</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>An open or short in the compressor lock sensor circuit.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Cooler compressor assembly</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Magnet clutch relay (MG CLT)</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000026BV08TX_02" type-id="32" category="03" proc-id="RM22W0E___0000HCJ00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E236888E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000026BV08TX_03" type-id="51" category="05" proc-id="RM22W0E___0000HCK00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>As DTC B1422 is also output when the fan &amp; generator V-belt is damaged or loose, inspect the belt before performing troubleshooting.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000026BV08TX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000026BV08TX_05_0001" proc-id="RM22W0E___0000HCL00001">
<testtitle>CHECK CAN COMMUNICATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally (See page <xref label="Seep01" href="RM000001RSW03ZX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>CAN DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000026BV08TX_05_0015" fin="false">A</down>
<right ref="RM0000026BV08TX_05_0005" fin="true">B</right>
<right ref="RM0000026BV08TX_05_0010" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0015" proc-id="RM22W0E___0000HCO00001">
<testtitle>INSPECT MAGNET CLUTCH RELAY (MG CLT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the magnet clutch relay (MG CLT).</ptxt>
<figure>
<graphic graphicname="E170970E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery voltage is not applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Battery voltage is applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000026BV08TX_05_0016" fin="false">OK</down>
<right ref="RM0000026BV08TX_05_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0016" proc-id="RM22W0E___0000HCP00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MAGNET CLUTCH RELAY - BATTERY, AIR CONDITIONING AMPLIFIER AND COOLER COMPRESSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the magnet clutch relay (MG CLT).</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E81 air conditioning amplifier assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C30 cooler compressor assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Magnet clutch relay terminal 1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Magnet clutch relay terminal 5 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Magnet clutch relay terminal 2 - E81-20 (MGC)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Magnet clutch relay terminal 3 - C30-3 (MG+)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Magnet clutch relay terminal 1 - Body ground</ptxt>
</entry>
<entry morerows="3" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="3" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Magnet clutch relay terminal 2 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Magnet clutch relay terminal 3 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Magnet clutch relay terminal 4 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000026BV08TX_05_0017" fin="false">OK</down>
<right ref="RM0000026BV08TX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0017" proc-id="RM22W0E___0000HCQ00001">
<testtitle>CHECK AIR CONDITIONING AMPLIFIER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the air conditioning amplifier assembly with its connectors still connected (See page <xref label="Seep01" href="RM0000039R501QX"/>).</ptxt>
<figure>
<graphic graphicname="E197830E36" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E81-20 (MGC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine idling</ptxt>
</item>
<item>
<ptxt>Blower switch LO level</ptxt>
</item>
<item>
<ptxt>A/C switch on (magnet clutch on permitted)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine idling</ptxt>
</item>
<item>
<ptxt>Blower switch LO level</ptxt>
</item>
<item>
<ptxt>A/C switch off or on (magnet clutch on not permitted)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Air Conditioning Amplifier Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000026BV08TX_05_0003" fin="false">OK</down>
<right ref="RM0000026BV08TX_05_0020" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0003" proc-id="RM22W0E___0000HCM00001">
<testtitle>INSPECT COOLER COMPRESSOR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the cooler compressor assembly.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for 1VD-FTV: See page  <xref label="Seep01" href="RM0000016AL08IX"/>.</ptxt>
</item>
<item>
<ptxt>for 1GR-FE: See page <xref label="Seep02" href="RM0000016AL08HX"/>.</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="E193115E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Check the magnet clutch operation.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → 3 (MG+)</ptxt>
<ptxt>Battery negative (-) → cooler compressor assembly body</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Magnet clutch engages</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 (SSR+) - 2 (SSR-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>160 to 320 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000026BV08TX_05_0004" fin="false">A</down>
<right ref="RM0000026BV08TX_05_0008" fin="true">B</right>
<right ref="RM0000026BV08TX_05_0013" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0004" proc-id="RM22W0E___0000HCN00001">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - COOLER COMPRESSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E81 air conditioning amplifier assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C30 cooler compressor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E81-8 (LOCK) - C30-1 (SSR+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-35 (SG-5) - C30-2 (SSR-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-8 (LOCK) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-35 (SG-5) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting according to problem symptoms table)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting according to the DTC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000026BV08TX_05_0006" fin="true">A</down>
<right ref="RM0000026BV08TX_05_0007" fin="true">B</right>
<right ref="RM0000026BV08TX_05_0009" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0020" proc-id="RM22W0E___0000HCR00001">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E81 air conditioning amplifier assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A38*1 or A52*2 ECM connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E81-17 (AC1) - A38-30 (AC1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-27 (ACT) - A38-18 (ACT)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-17 (AC1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-27 (ACT) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E81-17 (AC1) - A52-30 (AC1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-27 (ACT) - A52-18 (ACT)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-17 (AC1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-27 (ACT) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000026BV08TX_05_0021" fin="false">OK</down>
<right ref="RM0000026BV08TX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0021" proc-id="RM22W0E___0000HCS00001">
<testtitle>CHECK AIR CONDITIONING AMPLIFIER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the air conditioning amplifier assembly with its connectors still connected (See page <xref label="Seep01" href="RM0000039R501QX"/>).</ptxt>
<figure>
<graphic graphicname="E197830E37" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E81-17 (AC1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine idling</ptxt>
</item>
<item>
<ptxt>Blower switch LO level</ptxt>
</item>
<item>
<ptxt>A/C switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine idling</ptxt>
</item>
<item>
<ptxt>Blower switch LO level</ptxt>
</item>
<item>
<ptxt>A/C switch off</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>E81-27 (ACT) - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine idling</ptxt>
</item>
<item>
<ptxt>Blower switch LO level</ptxt>
</item>
<item>
<ptxt>A/C switch on (magnet clutch on)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine idling</ptxt>
</item>
<item>
<ptxt>Blower switch LO level</ptxt>
</item>
<item>
<ptxt>A/C switch off or on (magnet clutch off)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Air Conditioning Amplifier Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000026BV08TX_05_0007" fin="true">OK</down>
<right ref="RM0000026BV08TX_05_0022" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0022" proc-id="RM22W0E___0000HCT00001">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the air conditioning amplifier assembly with a new or normally functioning one (See page <xref label="Seep01" href="RM0000039R501QX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the air conditioning system to check it functions properly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Air conditioning system functions operate normally.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000026BV08TX_05_0023" fin="true">A</down>
<right ref="RM0000026BV08TX_05_0024" fin="true">B</right>
<right ref="RM0000026BV08TX_05_0025" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0006">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002LIQ02IX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0007">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501QX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0008">
<testtitle>REPLACE COOLER COMPRESSOR ASSEMBLY<xref label="Seep01" href="RM0000016AL08IX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0010">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0013">
<testtitle>REPLACE COOLER COMPRESSOR ASSEMBLY<xref label="Seep01" href="RM0000016AL08HX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0019">
<testtitle>REPLACE MAGNET CLUTCH RELAY (MG CLT)</testtitle>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0023">
<testtitle>END (AIR CONDITIONING AMPLIFIER ASSEMBLY IS FAULTY)</testtitle>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0024">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292037X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000026BV08TX_05_0025">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329203AX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>