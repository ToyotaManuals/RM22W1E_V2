<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UP_T00NS" variety="T00NS">
<name>REAR NO. 1 SEAT ASSEMBLY (for 60/40 Split Seat Type 60 Side)</name>
<para id="RM00000391100IX" category="A" type-id="8000E" name-id="SE6S9-03" from="201301" to="201308">
<name>REASSEMBLY</name>
<subpara id="RM00000391100IX_02" type-id="11" category="10" proc-id="RM22W0E___0000G5Q00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000391100IX_01" type-id="01" category="01">
<s-1 id="RM00000391100IX_01_0002" proc-id="RM22W0E___0000G4600000">
<ptxt>INSTALL REAR SEAT LOCK CONTROL LEVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181797" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the lock control lever with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0003" proc-id="RM22W0E___0000G4700000">
<ptxt>INSTALL REAR SEAT CUSHION FRAME SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181798" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Temporarily install the seat cushion frame with the 5 nuts and bolt.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 5 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a T40 "TORX" socket, tighten the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0001" proc-id="RM22W0E___0000G4500000">
<ptxt>INSTALL NO. 1 SEAT LEG ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181799" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T40 "TORX" socket, install the seat leg assembly with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0059" proc-id="RM22W0E___0000GIY00000">
<ptxt>INSTALL REAR SEAT INNER BELT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185853E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seat belt with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>The anchor part of the seat belt must not overlap the protruding part.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000391100IX_01_0004" proc-id="RM22W0E___0000G4800000">
<ptxt>INSTALL SEAT HEATER CONTROL SUB-ASSEMBLY LH (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182640" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the seat heater control.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0005" proc-id="RM22W0E___0000G4900000">
<ptxt>INSTALL REAR SEAT CUSHION HOLDER BRACKET LH (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182639" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the holder bracket with the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Connect the seat wire connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0006" proc-id="RM22W0E___0000G4A00000">
<ptxt>INSTALL REAR NO. 1 SEAT LOCK CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clamp to install the seat lock cable.</ptxt>
<figure>
<graphic graphicname="B181796E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 3 cable clamps to connect the 3 cables.</ptxt>
</s2>
<s2>
<ptxt>Connect the cable and install a new cable tie.</ptxt>
</s2>
<s2>
<ptxt>Install the lower seat track bracket with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B181203" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0007" proc-id="RM22W0E___0000G4B00000">
<ptxt>INSTALL FOLD SEAT LOCK CONTROL CABLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181795" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the 2 fold seat lock control cables.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 cable clamps to connect the 2 cables.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0008" proc-id="RM22W0E___0000G4C00000">
<ptxt>INSTALL REAR SEATBACK FRAME SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the lower seatback cover.</ptxt>
<figure>
<graphic graphicname="B184962" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Temporarily install the seatback frame with the 4 bolts.</ptxt>
<figure>
<graphic graphicname="B181793" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>LH Side:</ptxt>
<ptxt>Using a T40 "TORX" socket, tighten the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>RH Side:</ptxt>
<ptxt>Using a T55 "TORX" socket, tighten the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0009" proc-id="RM22W0E___0000G4D00000">
<ptxt>INSTALL NO. 2 FOLD SEAT LOCK CONTROL CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184052" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the clamp to install the lock control cable.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 cable clamps to connect the cable.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0010" proc-id="RM22W0E___0000G4E00000">
<ptxt>INSTALL RECLINING ADJUSTING CABLE ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181792" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the clamp to install the adjusting cable.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 cable clamps to connect the cable.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0061" proc-id="RM22W0E___0000GJC00000">
<ptxt>INSTALL NO. 1 SEAT 3 POINT TYPE BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185876E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Install the belt with the nut labeled A.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the belt sensor with the 3 nuts labeled B.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391100IX_01_0062" proc-id="RM22W0E___0000GJD00000">
<ptxt>INSTALL REAR SEAT SHOULDER BELT HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B188669" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seat belt as shown in the illustration.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B185854" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 3 claws to install the shoulder belt hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391100IX_01_0013" proc-id="RM22W0E___0000G4F00000">
<ptxt>INSTALL REAR LOWER SEATBACK COVER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184963" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0014" proc-id="RM22W0E___0000G4G00000">
<ptxt>INSTALL REAR SEATBACK HINGE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181237" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0015" proc-id="RM22W0E___0000G4H00000">
<ptxt>INSTALL REAR NO. 1 SEAT RECLINING COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181790" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0016" proc-id="RM22W0E___0000G4I00000">
<ptxt>INSTALL REAR NO. 2 SEAT RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181197" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the clamp to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0017" proc-id="RM22W0E___0000G4J00000">
<ptxt>INSTALL REAR SEAT RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181196" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0065" proc-id="RM22W0E___0000G5O00000">
<ptxt>INSTALL CUP HOLDER ASSEMBLY (w/ Center Armrest)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 clips to install the cup holder.</ptxt>
<figure>
<graphic graphicname="B184039" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
<figure>
<graphic graphicname="B190297" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0066" proc-id="RM22W0E___0000G5P00000">
<ptxt>INSTALL NO. 1 SEAT ARMREST PLATE (w/ Center Armrest)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184038" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws and 4 clips to install the 2 armrest plates.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0020">
<ptxt>INSTALL REAR CENTER SEAT HEADREST ASSEMBLY (w/ Center Armrest)</ptxt>
</s-1>
<s-1 id="RM00000391100IX_01_0021" proc-id="RM22W0E___0000G4K00000">
<ptxt>INSTALL REAR CENTER SEAT ARMREST ASSEMBLY (w/ Center Armrest)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181252" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T40 "TORX" socket, install the center seat armrest with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0022" proc-id="RM22W0E___0000G4L00000">
<ptxt>INSTALL SEATBACK STOPPER PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<s3>
<ptxt>Install the bush to the stopper plate.</ptxt>
</s3>
<s3>
<ptxt>Temporarily install the stopper plate with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="B181250" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Using a T40 "TORX" socket, tighten the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Tighten the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>153</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<s3>
<ptxt>Install the bush to the stopper plate.</ptxt>
</s3>
<s3>
<ptxt>Temporarily install the stopper plate with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="B181251" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Using a T40 "TORX" socket, tighten the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Tighten the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>153</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0023" proc-id="RM22W0E___0000G4M00000">
<ptxt>INSTALL REAR SEATBACK HEATER ASSEMBLY (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the seatback heater with the name stamp side facing the seatback cover.</ptxt>
</s2>
<s2>
<ptxt>Install the seatback heater with new tack pins.</ptxt>
<figure>
<graphic graphicname="B181208E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0024" proc-id="RM22W0E___0000G4N00000">
<ptxt>INSTALL REAR SEPARATE TYPE SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Using hog ring pliers, install the seatback cover with new hog rings.</ptxt>
<figure>
<graphic graphicname="B184037E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Using hog ring pliers, install the seatback cover with new hog rings.</ptxt>
<figure>
<graphic graphicname="B189167E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Connect the seatback cover bracket to the seatback pad.</ptxt>
<figure>
<graphic graphicname="B184024" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0025" proc-id="RM22W0E___0000G4O00000">
<ptxt>INSTALL SEATBACK COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seatback cover with pad.</ptxt>
</s2>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Pull out the seat belt.</ptxt>
<figure>
<graphic graphicname="B184030" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Pull out the seat belt.</ptxt>
<figure>
<graphic graphicname="B189169" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Pass the side airbag wire harness through the hole in the seatback.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Pass the seatback heater wire harness through the hole in the seatback.</ptxt>
<s3>
<figure>
<graphic graphicname="B184057" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Connect the seatback heater wire harness and install a new cable tie.</ptxt>
<figure>
<graphic graphicname="B182598E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<figure>
<graphic graphicname="B184020" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the seatback cover bracket to the seat frame with the cap nut.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>After the seatback trim cover is assembled, make sure the side airbag strap is not twisted.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Using hog ring pliers, install new hog rings to the seatback frame.</ptxt>
<figure>
<graphic graphicname="B262669E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Using hog ring pliers, install new hog rings to the seatback frame.</ptxt>
<figure>
<graphic graphicname="B262670E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0026" proc-id="RM22W0E___0000G4P00000">
<ptxt>INSTALL REAR SEAT SHOULDER BELT HOLE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181239" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0027" proc-id="RM22W0E___0000G4Q00000">
<ptxt>INSTALL SHOULDER BELT ANCHOR COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184040" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0028" proc-id="RM22W0E___0000G4R00000">
<ptxt>INSTALL REAR NO. 1 SEAT HEADREST SUPPORT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Attach the 4 claws to install the 2 supports.</ptxt>
<figure>
<graphic graphicname="B181190" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Attach the 8 claws to install the 4 supports.</ptxt>
<figure>
<graphic graphicname="B181243" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0029">
<ptxt>INSTALL REAR SEAT HEADREST ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM00000391100IX_01_0030" proc-id="RM22W0E___0000G4S00000">
<ptxt>INSTALL REAR SEATBACK BOARD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<figure>
<graphic graphicname="B184026" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Push the board down to attach the 2 claws on the bottom.</ptxt>
</s3>
<s3>
<ptxt>Attach the 2 claws on the top of the board to install it.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<figure>
<graphic graphicname="B189166" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Push the board down to attach the 3 claws on the bottom.</ptxt>
</s3>
<s3>
<ptxt>Attach the 3 claws on the top of the board to install it.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0031" proc-id="RM22W0E___0000G4T00000">
<ptxt>INSTALL REAR SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182667" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0032" proc-id="RM22W0E___0000G4U00000">
<ptxt>INSTALL REAR SEAT CUSHION HEATER ASSEMBLY (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the seat cushion heater with the name stamp side facing the seat cushion cover.</ptxt>
</s2>
<s2>
<ptxt>Install the seat cushion heater with new tack pins.</ptxt>
<figure>
<graphic graphicname="B181209E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0033" proc-id="RM22W0E___0000G4V00000">
<ptxt>INSTALL REAR SEPARATE TYPE SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184036E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using hog ring pliers, install the seat cushion cover to the seat cushion pad with new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Attach the 3 pieces of fastening tape.</ptxt>
<figure>
<graphic graphicname="B181234E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0034" proc-id="RM22W0E___0000G4W00000">
<ptxt>INSTALL SEAT CUSHION COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seat cushion cover with pad.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Pass the side airbag wire harness through the hole in the seat cushion.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<s3>
<ptxt>Pass the seatback heater wire harness through the hole in the seat cushion.</ptxt>
<figure>
<graphic graphicname="B184056" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Connect the 2 seat heater connectors.</ptxt>
<figure>
<graphic graphicname="B182596" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the side airbag wire harness clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0064" proc-id="RM22W0E___0000G6R00000">
<ptxt>CONNECT NO. 1 SEAT 3 POINT TYPE BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184533E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the belt anchor.</ptxt>
<atten3>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the vehicle body.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000391100IX_01_0060" proc-id="RM22W0E___0000G6Q00000">
<ptxt>INSTALL REAR NO. 1 SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184532E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the belt with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the vehicle body.</ptxt>
</atten3>
<atten4>
<ptxt>The No. 1 seat 3 point type belt anchor is fixed with the same bolt as the rear No. 1 seat inner belt LH. Therefore, connect it when installing the rear No. 1 seat inner belt LH.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000391100IX_01_0058">
<ptxt>INSTALL FOLD SEAT LOCK CONTROL CABLE SUB-ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM00000391100IX_01_0036" proc-id="RM22W0E___0000G4X00000">
<ptxt>INSTALL REAR SEAT TRACK HANDLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the cable clamp to connect the cable.</ptxt>
<figure>
<graphic graphicname="B181183" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 2 cable clamps to connect the 2 cables.</ptxt>
<figure>
<graphic graphicname="B181182" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the seat track handle with the 2 screws.</ptxt>
<figure>
<graphic graphicname="B182656" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0037" proc-id="RM22W0E___0000G4Y00000">
<ptxt>INSTALL REAR SEAT UNDER TRAY COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181181" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0038" proc-id="RM22W0E___0000G4Z00000">
<ptxt>INSTALL REAR SEAT CUSHION COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181231" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0039" proc-id="RM22W0E___0000G5000000">
<ptxt>INSTALL NO. 1 SEAT CUSHION PLATE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181180" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws to install the seat cushion plate.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0040" proc-id="RM22W0E___0000G5100000">
<ptxt>INSTALL REAR UNDER SIDE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181230" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws and 2 clips to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 5 screws.</ptxt>
</s2>
<s2>
<ptxt>Attach the 10 claws.</ptxt>
<figure>
<graphic graphicname="B181229" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0041" proc-id="RM22W0E___0000G5200000">
<ptxt>INSTALL REAR SEATBACK STAY COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181226" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0042" proc-id="RM22W0E___0000G5300000">
<ptxt>INSTALL SEAT BELT ANCHOR COVER CAP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the anchor cover cap with the screw.</ptxt>
<figure>
<graphic graphicname="B181228" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the claw to close the cap.</ptxt>
<figure>
<graphic graphicname="B184029" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0043" proc-id="RM22W0E___0000G5400000">
<ptxt>INSTALL REAR NO. 1 SEAT HINGE SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184017" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the hinge with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0044" proc-id="RM22W0E___0000G5500000">
<ptxt>INSTALL REAR NO. 1 SEAT HINGE SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the cable clamp to connect the cable.</ptxt>
<figure>
<graphic graphicname="B181176" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the hinge with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B184045" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0045" proc-id="RM22W0E___0000G5600000">
<ptxt>INSTALL REAR NO. 2 SEAT PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the wire harness, and attach 2 claws to close the protector.</ptxt>
</s2>
<s2>
<ptxt>Attach the claw to install the protector to the seat hinge.</ptxt>
<figure>
<graphic graphicname="B182595" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the claw to install the wire harness connector.</ptxt>
<figure>
<graphic graphicname="B184016" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Attach the claw to install the wire harness connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0046" proc-id="RM22W0E___0000G5700000">
<ptxt>INSTALL REAR SEAT SIDE HINGE MALE COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip to install the cover.</ptxt>
<figure>
<graphic graphicname="B181173" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0047" proc-id="RM22W0E___0000G5800000">
<ptxt>INSTALL REAR SEAT SIDE HINGE MALE COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0048" proc-id="RM22W0E___0000G5900000">
<ptxt>INSTALL REAR SEAT SIDE HINGE FEMALE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 6 claws to install the 2 covers.</ptxt>
<figure>
<graphic graphicname="B181172" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0063" proc-id="RM22W0E___0000G5M00000">
<ptxt>INSTALL FOLD SEAT STOPPER BAND ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fold seat stopper band to the seat leg side cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0049" proc-id="RM22W0E___0000G5A00000">
<ptxt>INSTALL REAR NO. 2 SEAT LEG SIDE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Pass the fold seat stopper band through the seat leg side cover and attach it to the seat cushion frame with the bolt.</ptxt>
<figure>
<graphic graphicname="B184044" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 4 claws to install the seat leg side cover.</ptxt>
<figure>
<graphic graphicname="B181224" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 4 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0050" proc-id="RM22W0E___0000G5B00000">
<ptxt>INSTALL NO. 1 SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181223" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the 2 covers.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0051" proc-id="RM22W0E___0000G5C00000">
<ptxt>INSTALL UPPER SEAT TRACK RAIL COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184041" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0052" proc-id="RM22W0E___0000G5D00000">
<ptxt>INSTALL UPPER SEAT TRACK RAIL COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184028" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0053" proc-id="RM22W0E___0000G5E00000">
<ptxt>INSTALL REAR NO. 1 SEAT RECLINING COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184015" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0054" proc-id="RM22W0E___0000G5F00000">
<ptxt>INSTALL REAR NO. 1 SEAT RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184027E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws in the order shown in the illustration to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0055" proc-id="RM22W0E___0000G5G00000">
<ptxt>INSTALL RECLINING ADJUSTER RELEASE HANDLE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184013" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the release handle with the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391100IX_01_0056" proc-id="RM22W0E___0000G5H00000">
<ptxt>INSTALL SEAT ADJUSTER BOLT COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184012" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>