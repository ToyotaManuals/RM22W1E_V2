<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000WZ1149X" category="J" type-id="3018M" name-id="ES11IS-003" from="201308">
<dtccode/>
<dtcname>MIL Circuit</dtcname>
<subpara id="RM000000WZ1149X_01" type-id="60" category="03" proc-id="RM22W0E___000023100001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The MIL (Malfunction Indicator Lamp) is used to indicate vehicle malfunction detections by the ECM. When the engine switch is turned on (IG), power is supplied to the MIL circuit, and the ECM provides the circuit ground which illuminates the MIL.</ptxt>
<ptxt>The MIL operation can be checked visually: When the engine switch is first turned on (IG), the MIL should be illuminated and should then turn off when the engine is started. If the MIL remains illuminated or is not illuminated, conduct the following troubleshooting procedure using the intelligent tester.</ptxt>
</content5>
</subpara>
<subpara id="RM000000WZ1149X_02" type-id="32" category="03" proc-id="RM22W0E___000023200001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A182518E06" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000WZ1149X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000WZ1149X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000WZ1149X_04_0011" proc-id="RM22W0E___000023700001">
<testtitle>CHECK THAT MIL IS ILLUMINATED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform troubleshooting in accordance with the table below.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL remains ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000WZ1149X_04_0001" fin="false">A</down>
<right ref="RM000000WZ1149X_04_0006" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0001" proc-id="RM22W0E___000023300001">
<testtitle>CHECK WHETHER MIL TURNS OFF</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on. </ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Check if any DTCs are output. Note down any DTCs.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK17ZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check if the MIL goes off.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>MIL goes off</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ1149X_04_0003" fin="true">OK</down>
<right ref="RM000000WZ1149X_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0002" proc-id="RM22W0E___000023400001">
<testtitle>CHECK HARNESS AND CONNECTOR (CHECK FOR SHORT IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A164768E16" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the A38 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check that the MIL is not illuminated.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>MIL is not illuminated.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ1149X_04_0004" fin="true">OK</down>
<right ref="RM000000WZ1149X_04_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0012" proc-id="RM22W0E___000023800001">
<testtitle>CHECK HARNESS AND CONNECTOR (COMBINATION METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A38 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E8 combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.89in"/>
<colspec colname="COLSPEC0" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A38-24 (W) or E8-12 (CHK) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ1149X_04_0009" fin="true">OK</down>
<right ref="RM000000WZ1149X_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0006" proc-id="RM22W0E___000023500001">
<testtitle>CHECK THAT MIL IS ILLUMINATED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the MIL is illuminated when the engine switch is turned on (IG).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>MIL is illuminated.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ1149X_04_0008" fin="true">OK</down>
<right ref="RM000000WZ1149X_04_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0013" proc-id="RM22W0E___000023900001">
<testtitle>CHECK THAT ENGINE STARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine starts</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine does not start*</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: The intelligent tester cannot communicate with the ECM.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000WZ1149X_04_0007" fin="false">A</down>
<right ref="RM000000WZ1149X_04_0015" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0007" proc-id="RM22W0E___000023600001">
<testtitle>CHECK HARNESS AND CONNECTOR (COMBINATION METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A38 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E8 combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.89in"/>
<colspec colname="COLSPEC0" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A38-24 (W) - E8-12 (CHK)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ1149X_04_0010" fin="true">OK</down>
<right ref="RM000000WZ1149X_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0003">
<testtitle>REPAIR CIRCUITS INDICATED BY OUTPUT DTCS<xref label="Seep01" href="RM0000032SF053X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0004">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292039X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0009">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM0000038ID00LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0008">
<testtitle>SYSTEM OK</testtitle>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0015">
<testtitle>GO TO VC OUTPUT CIRCUIT<xref label="Seep01" href="RM000001D6V0SAX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0010">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM0000038ID00LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WZ1149X_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>