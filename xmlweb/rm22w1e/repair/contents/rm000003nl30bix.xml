<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000003NL30BIX" category="C" type-id="8046T" name-id="BC86W-56" from="201308">
<dtccode>C1442</dtccode>
<dtcname>Invalid Data Received from Acceleration Sensor</dtcname>
<dtccode>C1443</dtccode>
<dtcname>Invalid Data Received from Yaw Rate Sensor</dtcname>
<subpara id="RM000003NL30BIX_01" type-id="60" category="03" proc-id="RM22W0E___0000AJT00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU (master cylinder solenoid) receives signals from the yaw rate sensor assembly via the CAN communication system.</ptxt>
<ptxt>The yaw rate sensor assembly has a built-in acceleration sensor.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.25in"/>
<colspec colname="COL2" colwidth="3.13in"/>
<colspec colname="COL3" colwidth="2.71in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1442</ptxt>
</entry>
<entry valign="middle">
<ptxt>An invalid data signal is transmitted continuously from the acceleration sensor for 10 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yaw rate sensor assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1443</ptxt>
</entry>
<entry valign="middle">
<ptxt>An invalid data signal is transmitted continuously from the yaw rate sensor for 10 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yaw rate sensor assembly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003NL30BIX_03" type-id="51" category="05" proc-id="RM22W0E___0000AJU00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the yaw rate sensor assembly, perform calibration (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003NL30BIX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003NL30BIX_04_0001" proc-id="RM22W0E___0000AJV00001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and idle it for 60 seconds, and then stop the engine.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.38in"/>
<colspec colname="COL2" colwidth="1.7in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003NL30BIX_04_0005" fin="true">A</down>
<right ref="RM000003NL30BIX_04_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003NL30BIX_04_0005">
<testtitle>REPLACE YAW RATE SENSOR ASSEMBLY<xref label="Seep01" href="RM000000SS506LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003NL30BIX_04_0004">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>