<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WN_T00PQ" variety="T00PQ">
<name>COMPRESSOR (for 3UR-FE)</name>
<para id="RM0000017ZZ068X" category="A" type-id="30014" name-id="ACGC6-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000017ZZ068X_01" type-id="01" category="01">
<s-1 id="RM0000017ZZ068X_01_0015" proc-id="RM22W0E___0000H9O00000">
<ptxt>ADJUST COMPRESSOR OIL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>When replacing the compressor and magnetic clutch with a new one, gradually discharge the refrigerant gas from the service valve, and drain the following amount of oil from the new compressor and magnetic clutch before installation.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>(Oil capacity inside the new compressor and magnetic clutch: 135 + 15 cc (4.6 + 0.51 fl.oz.)) - (Remaining oil amount in the removed compressor and magnetic clutch) = (Oil amount to be removed from the new compressor when replacing) </ptxt>
</specitem>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When checking the compressor oil level, follow the A/C system precautions.</ptxt>
</item>
<item>
<ptxt>If a new compressor and magnetic clutch is installed without removing some oil remaining in the pipes of the vehicle, the oil amount will be too large. This prevents heat exchange in the refrigerant cycle and causes refrigerant failure. </ptxt>
</item>
<item>
<ptxt>If the volume of oil remaining in the removed compressor and magnetic clutch is too small, check for oil leakage. </ptxt>
</item>
<item>
<ptxt>Be sure to use ND-OIL 8 or equivalent compressor oil. </ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZZ068X_01_0001" proc-id="RM22W0E___0000H9L00000">
<ptxt>INSTALL COOLER COMPRESSOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154188E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the cooler compressor and stud bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the 3 bolts and nut.</ptxt>
<torque>
<torqueitem>
<t-value1>24.5</t-value1>
<t-value2>250</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Tighten the bolts and nut in the order shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZZ068X_01_0003" proc-id="RM22W0E___0000H9N00000">
<ptxt>CONNECT SUCTION HOSE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the attached vinyl tape from the suction hose.</ptxt>
<figure>
<graphic graphicname="E154187" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Sufficiently apply compressor oil to a new O-ring and the fitting surface of the cooler compressor.</ptxt>
<spec>
<title>Compressor oil</title>
<specitem>
<ptxt>ND-OIL 8 or equivalent</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Install the O-ring to the suction hose.</ptxt>
</s2>
<s2>
<ptxt>Connect the suction hose to the cooler compressor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZZ068X_01_0002" proc-id="RM22W0E___0000H9M00000">
<ptxt>CONNECT NO. 1 COOLER REFRIGERANT DISCHARGE HOSE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154186" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the attached vinyl tape from the cooler refrigerant discharge hose.</ptxt>
</s2>
<s2>
<ptxt>Sufficiently apply compressor oil to a new O-ring and the fitting surface of the cooler compressor.</ptxt>
<spec>
<title>Compressor oil</title>
<specitem>
<ptxt>ND-OIL 8 or equivalent</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Install the O-ring to the discharge hose.</ptxt>
</s2>
<s2>
<ptxt>Connect the discharge hose to the cooler compressor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZZ068X_01_0009" proc-id="RM22W0E___00001ZF00000">
<ptxt>INSTALL FAN &amp; GENERATOR V BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the V belt onto every part.</ptxt>
<figure>
<graphic graphicname="A163771E05" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>While turning the belt tensioner counterclockwise, remove the bar.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Vane Pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Water Pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>No. 1 Idler</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Fan</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Generator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry>
<ptxt>Belt Tensioner</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry>
<ptxt>Crankshaft</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry>
<ptxt>Cooler Compressor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure that the V belt is properly set to each pulley.</ptxt>
</atten3>
</s2>
<s2>
<figure>
<graphic graphicname="B000540E14" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>After installing the belt, check that it fits properly in the ribbed grooves.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Make sure to check by hand that the belt has not slipped out of the grooves on the bottom of the pulley.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000017ZZ068X_01_0018" proc-id="RM22W0E___0000H9P00000">
<ptxt>INSTALL FRONT FENDER APRON SEAL LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E156500" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seal with the 4 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZZ068X_01_0011" proc-id="RM22W0E___000048900000">
<ptxt>CHARGE REFRIGERANT
</ptxt>
<content1 releasenbr="2">
<sst>
<sstitem>
<s-number>09985-20010</s-number>
<s-subnumber>09985-02130</s-subnumber>
<s-subnumber>09985-02150</s-subnumber>
<s-subnumber>09985-02090</s-subnumber>
<s-subnumber>09985-02110</s-subnumber>
<s-subnumber>09985-02010</s-subnumber>
<s-subnumber>09985-02050</s-subnumber>
<s-subnumber>09985-02060</s-subnumber>
<s-subnumber>09985-02070</s-subnumber>
</sstitem>
</sst>
<s2>
<ptxt>Perform vacuum purging using a vacuum pump.</ptxt>
</s2>
<s2>
<ptxt>Charge refrigerant HFC-134a (R134a).</ptxt>
<table pgwide="1">
<title>Standard:</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Condenser Core Thickness</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Conditioning Type</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>Refrigerant Charging Amount</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="3" valign="middle">
<ptxt>22 mm (0.866 in.)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>w/o Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>870 +/-30 g (30.7 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>870 +/-30 g (30.7 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>w/ Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>1010 +/-30 g (35.6 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>960 +/-30 g (33.9 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3" valign="middle">
<ptxt>16 mm (0.630 in.)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>w/o Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>770 +/-30 g (27.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>770 +/-30 g (27.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>w/ Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>970 +/-30 g (34.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>920 +/-30 g (32.5 +/-1.1 oz.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="I037365E19" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not operate the cooler compressor before charging refrigerant as the cooler compressor will not work properly without any refrigerant, and will overheat.</ptxt>
</item>
<item>
<ptxt>Approximately 200 g (7.05 oz.) of refrigerant may need to be charged after bubbles disappear. The refrigerant amount should be checked by measuring its quantity, and not with the sight glass.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000017ZZ068X_01_0012" proc-id="RM22W0E___000048A00000">
<ptxt>WARM UP ENGINE
</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Warm up the engine at less than 1850 rpm for 2 minutes or more after charging the refrigerant.</ptxt>
<atten3>
<ptxt>Be sure to warm up the compressor when turning the A/C switch is on after removing and installing the cooler refrigerant lines (including the compressor), to prevent damage to the compressor.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000017ZZ068X_01_0013" proc-id="RM22W0E___000048B00000">
<ptxt>CHECK FOR REFRIGERANT GAS LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>After recharging the refrigerant gas, check for refrigerant gas leakage using a halogen leak detector.</ptxt>
</s2>
<s2>
<ptxt>Perform the operation under these conditions:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stop the engine.</ptxt>
</item>
<item>
<ptxt>Secure good ventilation (the halogen leak detector may react to volatile gases other than refrigerant, such as evaporated gasoline or exhaust gas).</ptxt>
</item>
<item>
<ptxt>Repeat the test 2 or 3 times.</ptxt>
</item>
<item>
<ptxt>Make sure that some refrigerant remains in the refrigeration system. When compressor is off: approximately 392 to 588 kPa (4.0 to 6.0 kgf/cm<sup>2</sup>, 57 to 85 psi).</ptxt>
</item>
</list1>
</s2>
<s2>
<ptxt>Using a halogen leak detector, check the refrigerant line for leakage.</ptxt>
<figure>
<graphic graphicname="I042222E21" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>If a gas leak is not detected on the drain hose, remove the blower motor control (blower resistor) from the cooling unit. Insert the halogen leak detector sensor into the unit and perform the test.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and wait for approximately 20 minutes. Bring the halogen leak detector close to the pressure switch and perform the test.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000017ZZ068X_01_0020" proc-id="RM22W0E___000047Y00000">
<ptxt>INSTALL UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radiator support seal with the 7 clips.</ptxt>
<figure>
<graphic graphicname="B180890E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>