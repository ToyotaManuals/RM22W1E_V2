<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LW_T00EZ" variety="T00EZ">
<name>CRUISE CONTROL SYSTEM</name>
<para id="RM0000044B701VX" category="J" type-id="8046F" name-id="CC416-01" from="201301" to="201308">
<dtccode/>
<dtcname>Cruise SET Indicator Light Circuit</dtcname>
<subpara id="RM0000044B701VX_01" type-id="60" category="03" proc-id="RM22W0E___000079E00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>When the ECM detects a cruise control switch on signal from the cruise control switch, it sends it to the combination meter assembly through CAN communication. Then the cruise control SET indicator light turns on. </ptxt>
</item>
<item>
<ptxt>The cruise control SET indicator light circuit uses CAN communication. If there is a malfunction in this circuit, check for DTCs in the CAN communication system before troubleshooting this circuit.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM0000044B701VX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000044B701VX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000044B701VX_03_0001" proc-id="RM22W0E___000079F00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Select the item below in the Active Test, and then check that the cruise control SET indicator light illuminates (See page <xref label="Seep01" href="RM000002L7702YX"/>).</ptxt>
<table pgwide="1">
<title>Combination Meter</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Indicat. SET</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise control SET indicator light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON / OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Cruise control SET indicator light turns on/off.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000044B701VX_03_0002" fin="false">OK</down>
<right ref="RM0000044B701VX_03_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000044B701VX_03_0002" proc-id="RM22W0E___000079G00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the cruise control switch is functioning properly (See page <xref label="Seep01" href="RM000002L7702YX"/>).</ptxt>
<table pgwide="1">
<title>Cruise Control</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>SET/COAST Switch</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>-SET switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: -SET switch on</ptxt>
<ptxt>OFF: -SET switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On screen, item changes between ON and OFF according to above chart.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for 1GR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for 1UR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for 3UR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000044B701VX_03_0004" fin="true">A</down>
<right ref="RM0000044B701VX_03_0005" fin="true">B</right>
<right ref="RM0000044B701VX_03_0006" fin="true">C</right>
<right ref="RM0000044B701VX_03_0007" fin="true">D</right>
<right ref="RM0000044B701VX_03_0008" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM0000044B701VX_03_0003">
<testtitle>GO TO METER / GAUGE SYSTEM<xref label="Seep01" href="RM000002Z4L03NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044B701VX_03_0004">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000PLS0DAX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044B701VX_03_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202PX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044B701VX_03_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044B701VX_03_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044B701VX_03_0008">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202SX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>