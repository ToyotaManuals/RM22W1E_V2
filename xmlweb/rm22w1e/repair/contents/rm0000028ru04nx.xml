<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000G" variety="S000G">
<name>1UR-FE FUEL</name>
<ttl id="12008_S000G_7C3GL_T009O" variety="T009O">
<name>FUEL SYSTEM</name>
<para id="RM0000028RU04NX" category="L" type-id="3001A" name-id="FUAP3-01" from="201308">
<name>PRECAUTION</name>
<subpara id="RM0000028RU04NX_z0" proc-id="RM22W0E___00005VM00001">
<content5 releasenbr="1">
<atten2>
<list1 type="unordered">
<item>
<ptxt>Before working on the fuel system, disconnect the cable from the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>Do not smoke or be near an open flame when working on the fuel system.</ptxt>
</item>
<item>
<ptxt>Keep gasoline away from rubber and leather parts.</ptxt>
</item>
</list1>
</atten2>
<step1>
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Do not disconnect any part of the fuel system until you have discharged the fuel system pressure.</ptxt>
</item>
<item>
<ptxt>After discharging the fuel pressure, place a cloth or equivalent over fittings as you separate them to reduce the risk of fuel spray on yourself or in the engine compartment.</ptxt>
</item>
</list1>
</atten2>
<step2>
<ptxt>Disconnect the fuel pump ECU connector (See page <xref label="Seep01" href="RM000002YPO025X_01_0001"/>).</ptxt>
</step2>
<step2>
<ptxt>Start the engine. After the engine stops, turn the engine switch off.</ptxt>
<atten4>
<ptxt>DTC P0171/0174 (system too lean) may be stored.</ptxt>
</atten4>
</step2>
<step2>
<ptxt>Crank the engine again, and then check that the engine does not start.</ptxt>
</step2>
<step2>
<ptxt>Loosen the fuel tank cap, and then discharge the pressure in the fuel tank completely.</ptxt>
</step2>
<step2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep03" href="RM000003C32006X"/>).</ptxt>
</item>
</list1>
</atten3>
</step2>
<step2>
<ptxt>Connect the fuel pump ECU connector (See page <xref label="Seep04" href="RM000002YPM025X_01_0001"/>).</ptxt>
</step2>
</step1>
<step1>
<ptxt>FUEL SYSTEM</ptxt>
<step2>
<ptxt>When disconnecting the high fuel pressure line, a large amount of gasoline will spill out. Follow these procedures.</ptxt>
<step3>
<ptxt>Perform the "DISCHARGE FUEL SYSTEM PRESSURE" procedures above.</ptxt>
</step3>
<step3>
<ptxt>Disconnect the fuel tube.  </ptxt>
</step3>
<step3>
<ptxt>Drain any fuel remaining inside the fuel tube.</ptxt>
</step3>
<step3>
<ptxt>To protect the disconnected fuel tube from damage and contamination, cover it with a plastic bag.  </ptxt>
</step3>
<step3>
<ptxt>Put a container under the connecting part of the pressure line.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when removing and installing the injectors.</ptxt>
<figure>
<graphic graphicname="A163532E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>O-ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Fuel Delivery Pipe Sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Never reuse the O-ring.</ptxt>
</item>
<item>
<ptxt>When placing a new O-ring on the fuel injector assembly, do not damage it.</ptxt>
</item>
<item>
<ptxt>Coat a new O-ring with spindle oil or gasoline before installing it.</ptxt>
</item>
<item>
<ptxt>Do not use engine oil, gear oil or brake oil.</ptxt>
</item>
</list1>
</atten3>
</step2>
<step2>
<ptxt>Install the fuel injector assembly to the fuel delivery pipe sub-assembly and cylinder head sub-assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A163533E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Delivery Pipe Sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>O-ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Fuel Injector Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Insulator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Cylinder Head Sub-assembly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Before installing the fuel injector assembly, apply spindle oil or gasoline on the place where the fuel delivery pipe sub-assembly contacts the O-ring of the fuel injector assembly.</ptxt>
</atten3>
</step2>
<step2>
<ptxt>Observe these precautions when disconnecting the fuel tube connector (for quick type A). </ptxt>
<figure>
<graphic graphicname="A223364E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Fuel Hose Connector Cover Type:</ptxt>
<ptxt>Detach the lock claw by lifting up the cover as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Hose Connector Cover</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Check for dirt or mud on the pipe and around the connector before disconnection. Clean if necessary.</ptxt>
</step3>
<step3>
<ptxt>Pinch the connector and disconnect the connector and pipe.</ptxt>
<figure>
<graphic graphicname="A088336E26" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pinch</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
<figure>
<graphic graphicname="A224870E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube. </ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>O-ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Fuel Tube Connector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the connector and pipe are stuck, pinch the connector, push and pull the pipe to disconnect the pipe and pull it out.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Check for dirt or mud on the seal surface of the disconnected pipe. Clean if necessary.</ptxt>
</step3>
<step3>
<ptxt>To protect the disconnected pipe and connector from damage and contamination, cover them with a plastic bag.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when connecting the fuel tube connector (for quick type A). </ptxt>
<step3>
<ptxt>Before connecting the connector, check that there is no damage or contamination in the connecting part of the pipe.</ptxt>
</step3>
<step3>
<ptxt>Align the axis of the connector with the axis of the pipe. Push the pipe into the connector until the connector makes a "click" sound. If the connection is tight, apply a small amount of fresh spindle oil or gasoline to the tip of the pipe.</ptxt>
<figure>
<graphic graphicname="A071366E27" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>After connecting the pipe and connector, check that the pipe and connector are securely connected by trying to pull them apart.</ptxt>
<figure>
<graphic graphicname="A071366E28" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Fuel Hose Connector Cover Type:</ptxt>
<ptxt>Attach the lock claws to the connector by pushing down on the cover.</ptxt>
</step3>
<step3>
<ptxt>Check for any fuel leaks.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when disconnecting the fuel tube connector (for quick type B).</ptxt>
<step3>
<ptxt>Check that there is no damage or foreign matter on the part of the pipe that contacts the connector.</ptxt>
</step3>
<step3>
<ptxt>Detach the 2 claws of the connector retainer. Push the retainer out and disconnect the connector from the pipe.</ptxt>
<figure>
<graphic graphicname="A224877E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push Out</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100137" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the connector and pipe are stuck, pinch the fuel pipe by hand and push and pull the connector to disconnect it.</ptxt>
<figure>
<graphic graphicname="A091246E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for any dirt and foreign matter contamination in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
<figure>
<graphic graphicname="A224889E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector. </ptxt>
</item>
<item>
<ptxt>Do not allow any scratches or foreign matter on the parts when disconnecting them, as the fuel tube joint contains the O-rings that seal the plug.</ptxt>
</item>
<item>
<ptxt>Check for any dirt and foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Do not forcibly bend, twist or turn the nylon tube.</ptxt>
</item>
<item>
<ptxt>Protect the disconnected part by covering it with a plastic bag and tape after disconnecting the main tube.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the tube between your fingers and turn it carefully to free it. Then disconnect the main tube.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>O-ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Check for foreign matter on the seal surface of the disconnected pipe. Clean if necessary.</ptxt>
</step3>
<step3>
<ptxt>To protect the disconnected pipe and connector from damage and foreign matter, cover them with a plastic bag.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when connecting the fuel tube connector (for quick type B).</ptxt>
<step3>
<ptxt>Check for foreign matter on the pipe and around the connector before connecting it. Clean if necessary.</ptxt>
</step3>
<step3>
<ptxt>Align the axis of the connector with the axis of the pipe. Push the connector onto the pipe, and then push in the retainer.</ptxt>
<figure>
<graphic graphicname="A125397E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push In</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before connecting the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After connecting the pipe and connector, check that the pipe and connector are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<figure>
<graphic graphicname="A227055" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Observe the following precautions when disconnecting the fuel tube connector (for quick type C).</ptxt>
<step3>
<ptxt>Check for dirt or mud on the fuel tube connector and pipe before disconnection. Clean if necessary.</ptxt>
</step3>
<step3>
<ptxt>Pull the retainer up while pinching it on both sides by hand as shown in the illustration, and disconnect the fuel tube connector from the pipe.</ptxt>
<figure>
<graphic graphicname="A270948E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull up</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100137" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not use any tools.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>If the fuel tube connector and pipe are stuck, pinch the fuel pipe by hand and push and pull the connector to disconnect it.</ptxt>
<figure>
<graphic graphicname="A270949" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not use any tools.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>Check for dirt or mud on the pipe seal after disconnecting the fuel tube. Clean it if necessary.</ptxt>
</step3>
<step3>
<ptxt>Cover the disconnected fuel tube connector and pipe with a plastic bag.</ptxt>
<atten3>
<ptxt>Do not allow the fuel tube connector or pipe to be damaged or contaminated by foreign matter.</ptxt>
</atten3>
</step3>
</step2>
<step2>
<ptxt>Observe the following precautions when connecting the fuel tube connector (for quick type C).</ptxt>
<step3>
<ptxt>Align the shaft of the fuel tube connector with the shaft of the pipe, push the fuel tube connector in, and push down the retainer.</ptxt>
<figure>
<graphic graphicname="A270950E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Connect</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push down</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>After connecting the fuel tube connector, check that the fuel tube connector and pipe are securely connected by trying to pull them apart.</ptxt>
<figure>
<graphic graphicname="A270951" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when handling the nylon tube.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to turn the connection part of the nylon tube and quick connector when connecting them.</ptxt>
</item>
<item>
<ptxt>Do not kink the nylon tube.</ptxt>
</item>
<item>
<ptxt>Do not bend the nylon tube as this may cause blockage.</ptxt>
</item>
</list1>
</atten3>
</step2>
</step1>
<step1>
<ptxt>FUEL SUCTION WITH PUMP AND GAUGE TUBE ASSEMBLY</ptxt>
<atten3>
<ptxt>Do not disconnect the hoses indicated in the illustration.</ptxt>
</atten3>
<figure>
<graphic graphicname="A181496E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Single Tank Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Double Tank Type</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>INSPECT FOR FUEL LEAK</ptxt>
<ptxt>(See page <xref label="Seep05" href="RM000000Q460AXX_01_0071"/>)</ptxt>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>