<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TW_T00MZ" variety="T00MZ">
<name>AIRBAG SYSTEM</name>
<para id="RM0000038B400WX" category="C" type-id="305K7" name-id="RSGUA-01" from="201308">
<dtccode>B1662/45</dtccode>
<dtcname>Indicator Light Circuit Malfunction</dtcname>
<subpara id="RM0000038B400WX_01" type-id="60" category="03" proc-id="RM22W0E___0000FBR00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The indicator light circuit consists of the center airbag sensor and the combination meter.</ptxt>
<ptxt>When the center airbag sensor detects a malfunction in the SRS airbag system, the SRS warning light comes on to inform the driver. It also comes on when the source voltage drops, and automatically goes off in approximately 10 seconds after the source voltage returns to normal (DTC is not stored).</ptxt>
<ptxt>The SRS warning light comes on for 6 seconds after the ignition switch is turned to ON and goes off if the system is normal.</ptxt>
<ptxt>When an open circuit is detected, such as when the connector between the combination meter and the center airbag sensor is disconnected, the SRS warning light remains on even when approximately 6 seconds elapsed after the ignition switch is turned to ON.</ptxt>
<ptxt>DTC B1662/45 is stored when a malfunction is detected in the indicator light circuit.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1662/45</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives an open circuit signal, a short to ground signal or a short circuit to ground signal or a short circuit to B+ signal in the indicator light circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A combination meter malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Combination meter assembly (SRS warning light)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000038B400WX_02" type-id="32" category="03" proc-id="RM22W0E___0000FBS00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C159525E06" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000038B400WX_03" type-id="51" category="05" proc-id="RM22W0E___0000FBT00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM0000038B400WX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000038B400WX_04_0001" proc-id="RM22W0E___0000FBU00001">
<testtitle>CHECK CAN COMMUNICATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if a CAN communication DTC is output.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL3" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000038B400WX_04_0002" fin="false">A</down>
<right ref="RM0000038B400WX_04_0007" fin="true">B</right>
<right ref="RM0000038B400WX_04_0015" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000038B400WX_04_0002" proc-id="RM22W0E___0000FBV00001">
<testtitle>CHECK BATTERY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage of the battery.</ptxt>
<spec>
<title>Standard Voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000038B400WX_04_0003" fin="false">OK</down>
<right ref="RM0000038B400WX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000038B400WX_04_0003" proc-id="RM22W0E___0000FBW00001">
<testtitle>CHECK CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors are properly connected to the center airbag sensor and combination meter.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are properly connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000038B400WX_04_0004" fin="false">OK</down>
<right ref="RM0000038B400WX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000038B400WX_04_0004" proc-id="RM22W0E___0000FBX00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SOURCE VOLTAGE OF CENTER AIRBAG SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor.</ptxt>
<figure>
<graphic graphicname="C148471E09" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Operate all the components of the electrical system (defogger, wipers, headlights, heater blower, etc.).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E47-21 (IG2) - E47-25 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E47-21 (IG2) - E47-26 (E2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000038B400WX_04_0005" fin="false">OK</down>
<right ref="RM0000038B400WX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000038B400WX_04_0005" proc-id="RM22W0E___0000FBY00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SOURCE VOLTAGE OF COMBINATION METER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C173438E08" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E7 connector from the combination meter.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E7-18 (IG+) - E7-15 (ET)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000038B400WX_04_0006" fin="false">OK</down>
<right ref="RM0000038B400WX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000038B400WX_04_0006" proc-id="RM22W0E___0000FBZ00001">
<testtitle>CHECK SRS WARNING LIGHT (SHORT TO GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Connect the connector to the combination meter.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the SRS warning light condition.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>After the primary check period, SRS warning light goes off for approximately 10 seconds and then remains on.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The primary check period is approximately 6 seconds after the ignition switch is turned ON.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000038B400WX_04_0014" fin="true">OK</down>
<right ref="RM0000038B400WX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000038B400WX_04_0007">
<testtitle>REPAIR CIRCUITS INDICATED BY OUTPUT DTCS<xref label="Seep01" href="RM000001RSW03ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000038B400WX_04_0008">
<testtitle>CHECK AND REPLACE BATTERY OR CHARGING SYSTEM</testtitle>
</testgrp>
<testgrp id="RM0000038B400WX_04_0009">
<testtitle>CONNECT CONNECTORS PROPERLY</testtitle>
</testgrp>
<testgrp id="RM0000038B400WX_04_0010">
<testtitle>REPLACE HARNESS AND CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000038B400WX_04_0011">
<testtitle>REPLACE HARNESS AND CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000038B400WX_04_0012">
<testtitle>GO TO METER / GAUGE SYSTEM<xref label="Seep01" href="RM000003BBU00FX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000038B400WX_04_0014">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000002Y2N00ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000038B400WX_04_0015">
<testtitle>REPAIR CIRCUITS INDICATED BY OUTPUT DTCS<xref label="Seep01" href="RM000001RSW040X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>