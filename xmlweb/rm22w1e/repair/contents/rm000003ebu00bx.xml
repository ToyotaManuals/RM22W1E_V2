<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3Y1_T00R4" variety="T00R4">
<name>POWER BACK DOOR SYSTEM</name>
<para id="RM000003EBU00BX" category="T" type-id="3001H" name-id="DH04P-05" from="201301" to="201308">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM000003EBU00BX_z0" proc-id="RM22W0E___0000IM800000">
<content5 releasenbr="4">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses and relays related to this system before inspecting the suspected areas below.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Power Back Door System</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Power back door does not operate</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "ECU Power Source Circuit"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000025WO00GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Main Switch Circuit"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000025WP00FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace power back door unit assembly (power back door ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000039HB017X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="1" colsep="1" valign="middle" align="left">
<ptxt>Power back door does not operate with door control switch</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Power Back Door Open / Closer Switch Circuit"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000025WS00FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace power back door unit assembly (power back door ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000039HB017X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Power back door does not operate with back door lock control switch</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Door Control Switch Circuit"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000025WR00FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace main body ECU (cowl side junction block LH)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace power back door unit assembly (power back door ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000039HB017X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="3" colsep="1" valign="middle" align="left">
<ptxt>Power back door does not operate with door control transmitter</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Wireless Door Lock Control System</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XX205DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>CAN Communication System (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSO08CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>CAN Communication System (for RHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSO08DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace power back door unit assembly (power back door ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000039HB017X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="4" colsep="1" valign="middle" align="left">
<ptxt>No-answer back</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Power Back Door Warning Buzzer Circuit"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000025WT00FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lighting System (EXT)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002WKL01CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>CAN Communication System (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSO08CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>CAN Communication System (for RHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSO08DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace power back door unit assembly (power back door ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000039HB017X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Jam protection function is activated while power back door is operating</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Fitting condition of back door</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Power Back Door Sensor Circuit"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002AE0022X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace power back door unit assembly (power back door ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000039HB017X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="1" colsep="1" valign="middle" align="left">
<ptxt>Back door closer does not operate</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Back Door Lock Motor Circuit"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003FSD00BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace power back door unit assembly (power back door ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000039HB017X" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>