<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3Y0_T00R3" variety="T00R3">
<name>TAIL GATE CLOSER SYSTEM</name>
<para id="RM000003EUM00AX" category="C" type-id="803VV" name-id="DH08F-04" from="201301" to="201308">
<dtccode>B225A</dtccode>
<dtcname>Tail Gate Panel Position Switch RH</dtcname>
<subpara id="RM000003EUM00AX_01" type-id="60" category="03" proc-id="RM22W0E___0000ILE00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The No. 2 main body ECU receives the close position switch signal from the lower tail gate lock assembly RH and detects the open/closed status of the lower tail gate. DTC B225A is stored when the No. 2 main body ECU receives a close position switch malfunction signal from the lower tail gate lock assembly RH.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B225A</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A close position switch malfunction (open or short circuit) signal is received from the lower tail gate lock.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Lower tail gate lock assembly RH</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>No. 2 main body ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003EUM00AX_02" type-id="32" category="03" proc-id="RM22W0E___0000ILF00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B312260E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003EUM00AX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003EUM00AX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003EUM00AX_04_0001" proc-id="RM22W0E___0000ILG00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (POSITION SWITCH RH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the Data List for proper functioning of the close position switch (See page <xref label="Seep01" href="RM000003GU1002X"/>).</ptxt>
<table pgwide="1">
<title>Body No. 4</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Position Switch RH	</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Close position switch RH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Lower tail gate closed</ptxt>
<ptxt>OFF: Lower tail gate open</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003EUM00AX_04_0004" fin="false">OK</down>
<right ref="RM000003EUM00AX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003EUM00AX_04_0004" proc-id="RM22W0E___0000ILJ00000">
<testtitle>CHECK DTC OUTPUT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Starting with the lower tail gate closed, open and close the lower tail gate 6 times.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep03" href="RM000003EUH008X"/>).</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000003EUH008X"/>).</ptxt>
</test1>
<test1>
<ptxt>Starting with the lower tail gate closed, open and close the lower tail gate 6 times.</ptxt>
</test1>
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep02" href="RM000003EUH008X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B225A is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003EUM00AX_04_0009" fin="true">OK</down>
<right ref="RM000003EUM00AX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003EUM00AX_04_0002" proc-id="RM22W0E___0000ILH00000">
<testtitle>INSPECT LOWER TAIL GATE LOCK ASSEMBLY RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the lower tail gate lock assembly RH (See page <xref label="Seep01" href="RM000001BLH063X"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the lower tail gate lock assembly RH (See page <xref label="Seep02" href="RM000003GFE00AX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003EUM00AX_04_0003" fin="false">OK</down>
<right ref="RM000003EUM00AX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003EUM00AX_04_0003" proc-id="RM22W0E___0000ILI00000">
<testtitle>CHECK HARNESS AND CONNECTOR (LOWER TAIL GATE LOCK ASSEMBLY RH - NO. 2 MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the h4 lower tail gate lock assembly RH connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the L50 No. 2 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>h4-7 (HND) - L50-17 (LMT)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>h4-8 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>h4-7 (HND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003EUM00AX_04_0005" fin="true">OK</down>
<right ref="RM000003EUM00AX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003EUM00AX_04_0005">
<testtitle>REPLACE NO. 2 MAIN BODY ECU<xref label="Seep01" href="RM0000038MO00QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003EUM00AX_04_0006">
<testtitle>REPLACE LOWER TAIL GATE LOCK ASSEMBLY RH<xref label="Seep01" href="RM000001BLH063X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003EUM00AX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003EUM00AX_04_0009">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>