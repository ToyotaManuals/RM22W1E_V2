<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000000XHY0K5X" category="D" type-id="304JI" name-id="BC002O-508" from="201308">
<name>FREEZE FRAME DATA</name>
<subpara id="RM000000XHY0K5X_z0" proc-id="RM22W0E___0000AD700001">
<content5 releasenbr="1">
<step1>
<ptxt>FREEZE FRAME DATA</ptxt>
<step2>
<ptxt>Whenever a DTC is stored, the skid control ECU (master cylinder solenoid) stores the current vehicle (sensor) state as freeze frame data.</ptxt>
</step2>
<step2>
<ptxt>The skid control ECU (master cylinder solenoid) stores the number of times (maximum: 31) the ignition switch has been turned from off to ON since the last time the ABS/VSC was activated.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the vehicle is stopped or at a low speed (7 km/h (4 mph) or less), or if a DTC is stored, the skid control ECU (master cylinder solenoid) will stop counting the number of times the ignition switch has been turned from off to ON.</ptxt>
</item>
<item>
<ptxt>Freeze frame data at the time the ABS/VSC operates:</ptxt>
<ptxt>The skid control ECU (master cylinder solenoid) stores and updates data whenever the ABS/VSC operates.</ptxt>
<ptxt>When the ECU stores data at the time a DTC is stored, the data stored during ABS/VSC operation is cleared.</ptxt>
</item>
<item>
<ptxt>Freeze frame data at the time a DTC is stored:</ptxt>
<ptxt>When the skid control ECU (master cylinder solenoid) stores data at the time a DTC is stored, no updates will be performed until the data is cleared.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
<step1>
<ptxt>CHECK FREEZE FRAME DATA</ptxt>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the GTS on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Trouble Codes.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the GTS, select the trouble code data display with freeze frame data.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.57in"/>
<colspec colname="COL2" colwidth="1.79in"/>
<colspec colname="COLSPEC0" colwidth="1.89in"/>
<colspec colname="COL3" colwidth="1.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Elapsed Time after Freeze Trigger</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Elapsed time after freeze trigger/ Min.: 0 msec, Max.: 500 msec</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Number of IG ON</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Number of times ignition switch turned to ON after storing freeze frame data/ 0 to 31</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Buzzer</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Buzzer/ ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Buzzer on</ptxt>
<ptxt>OFF: Buzzer off</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The combination meter has a built-in buzzer.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stop Light SW</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Stop light switch/ ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Brake pedal depressed</ptxt>
<ptxt>OFF: Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Parking Brake SW</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Parking brake switch/ ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Parking brake applied</ptxt>
<ptxt>OFF: Parking brake released</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Reservoir Warning SW</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Brake fluid level warning switch/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Gear Position</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Gear position information/ P,N, R, 1st-8th, Fail or Not R</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Actual gear position</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Shift lever position information/ P,N, R, D/M, 1st-6th/B or Fail</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Actual shift lever position</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Operated System</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Operated system status/ ABS, VSC, TRC, BA, HAB, PBA, PB, HA-CTRL, Fail, TSC, Sys or Non</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ABS: ABS is activated.</ptxt>
<ptxt>VSC: VSC is activated.</ptxt>
<ptxt>TRC: TRC is activated.</ptxt>
<ptxt>BA: BA is activated.</ptxt>
<ptxt>PBA: PBA is activated.</ptxt>
<ptxt>PB: PB is activated.</ptxt>
<ptxt>HA-CTRL: Hill-start assist control is activated.</ptxt>
<ptxt>Fail: Fail safe mode is activated.</ptxt>
<ptxt>Non: No operation is activated.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Master Cylinder Sensor</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master cylinder pressure sensor reading/ Min.: 0.00 V, Max.: 5.00 V</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>When brake pedal released: 0.3 to 0.9 V</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The value increases when the brake pedal is depressed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>M/C Sensor Grade</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master cylinder pressure sensor change/ Min.: -30 MPa/s, Max.: 225 MPa/s</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>When brake pedal released or pedal held at constant position: 0 MPa/s</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Changes in proportion with the pedal movement speed when the brake pedal is being operated.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Yaw Rate Sensor</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Yaw rate sensor/ Min.: -128°/s, Max.: 127°/s</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle stopped: 0°/s</ptxt>
<ptxt>Turning right: -128 to 0°/s</ptxt>
<ptxt>Turning left: 0 to 127°/s</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Lateral G</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Lateral G/ Min.: -25.10 m/s<sup>2</sup>, Max.: 24.90 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Changes in proportion with the pedal movement speed when the brake pedal is being operated.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Forward and Rearward G</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Forward and backward G/ Min.: - 25.10 m/s<sup>2</sup>, Max.: 24.90 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Changes in proportion with acceleration during acceleration/deceleration.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FR Wheel Speed</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front wheel speed sensor RH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle stopped: 0 km/h (0 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FL Wheel Speed</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front wheel speed sensor LH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle stopped: 0 km/h (0 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RR Wheel Speed</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear wheel speed sensor RH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle stopped: 0 km/h (0 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RL Wheel Speed</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear wheel speed sensor LH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle stopped: 0 km/h (0 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle Speed</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Maximum wheel speed sensor reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle stopped: 0 km/h (0 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>When driving at constant speed: No large fluctuations</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle Speed Grade</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle acceleration/ Min.: -25.10 m/s<sup>2</sup>, Max.: 24.90 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle stopped: 0 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Changes in proportion with the vehicle acceleration/deceleration during driving.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FR Wheel Direction</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front wheel RH direction signal/ Forward or Back</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Forward: Forward</ptxt>
<ptxt>Back: Backward</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FL Wheel Direction</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front wheel LH direction signal/ Forward or Back</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Forward: Forward</ptxt>
<ptxt>Back: Backward</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RR Wheel Direction</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear wheel RH direction signal/ Forward or Back</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Forward: Forward</ptxt>
<ptxt>Back: Backward</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RL Wheel Direction</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear wheel LH direction signal/ Forward or Back</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Forward: Forward</ptxt>
<ptxt>Back: Backward</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Real Engine Torque</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Actual engine torque/ Min.: -1024.0 Nm, Max.: 1023.0 Nm</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Accelerator Opening Angle %</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Throttle valve opening amount/ Min.: 0.0%, Max.: 128.0%</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>When accelerator pedal released: 0%</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Changes in proportion with the movement of the pedal during accelerator pedal operation.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Steering Angle Sensor</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Steering sensor/ Min.: - 3276.8°, Max.: 3276.7°</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Left turn: Increase</ptxt>
<ptxt>Right turn: Decrease</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pre Crash Brake OFF Switch*</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Pre-crash system cancel switch/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>TRC/VSC Off Mode</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>TRC/VSC off mode/ Normal, TRC OFF or VSC OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Normal: Normal mode</ptxt>
<ptxt>TRC OFF: TRC off mode</ptxt>
<ptxt>VSC OFF: VSC off mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Dynamic Radar Cruise Control System</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>CLEAR FREEZE FRAME DATA</ptxt>
<atten3>
<ptxt>Clearing the DTCs will also clear the freeze frame data.</ptxt>
</atten3>
<step2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Clear the DTCs and freeze frame data (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>