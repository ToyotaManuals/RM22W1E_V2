<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000002PPN05OX" category="C" type-id="802WX" name-id="ES17I3-001" from="201308">
<dtccode>P2564</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Position Sensor "A" Circuit Low</dtcname>
<dtccode>P2565</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Position Sensor "A" Circuit High</dtcname>
<dtccode>P2588</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Position Sensor "B" Circuit Low</dtcname>
<dtccode>P2589</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Position Sensor "B" Circuit High</dtcname>
<subpara id="RM000002PPN05OX_01" type-id="60" category="03" proc-id="RM22W0E___00003TP00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The variable nozzle vane type turbocharger consists primarily of a compressor wheel, turbine wheel, nozzle vane, unison ring, DC motor and nozzle vane position sensor.</ptxt>
<ptxt>The nozzle vane position sensor consists of a Hall IC and a magnetic yoke that rotates in unison with the movement of the linkage that actuates the nozzle vane. The nozzle vane position sensor converts the changes in the magnetic flux that are caused by the rotation of the DC motor (hence, the rotation of the magnetic yoke) into electric signals, and outputs them to the ECM. The ECM determines the actual nozzle vane position from the electric signals in order to calculate the target nozzle vane position.</ptxt>
<figure>
<graphic graphicname="A221597E02" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>P2564 (Bank 1), P2588 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>VNA or VNA2 voltage is 0.1 V or less for 2.1 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in nozzle vane position sensor circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2565 (Bank 1), P2589 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>VNA or VNA2 voltage is 4.9 V or higher for 2.1 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Nozzle vane position sensor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in nozzle vane position sensor circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTC P2564, P2565, P2588 and/or P2589 is stored due to the nozzle vane being stuck open, the following symptoms may appear:</ptxt>
<list1 type="nonmark">
<item>
<ptxt>- Lack of power</ptxt>
</item>
<item>
<ptxt>- Vehicle surge or hesitation under light or medium load</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000002PPN05OX_02" type-id="32" category="03" proc-id="RM22W0E___00003TQ00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A165736E07" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002PPN05OX_03" type-id="51" category="05" proc-id="RM22W0E___00003TR00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08XX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK08XX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002PPN05OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002PPN05OX_04_0008" proc-id="RM22W0E___00003TV00001">
<testtitle>CHECK OTHER DTC OUTPUT (IN ADDITION TO DTC P2564 P2565, P2588 OR P2589)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2564 P2565, P2588 or P2589 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2564 P2565, P2588 or P2589 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002PPN05OX_04_0001" fin="false">A</down>
<right ref="RM000002PPN05OX_04_0018" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002PPN05OX_04_0001" proc-id="RM22W0E___00003TS00001">
<testtitle>CHECK HARNESS AND CONNECTOR (ECM - NOZZLE VANE POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the nozzle vane position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C89-3 (VNVC) - C45-120 (VNVC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C89-2 (VNE2) - C45-97 (VNE2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C89-1 (VTA1) - C45-74 (VNA)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-3 (VNC2) - C45-120 (VNVC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-2 (VE2) - C45-97 (VNE2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-1 (VTI2) - C45-117 (VNA2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C89-3 (VNVC) or C45-120 (VNVC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C89-1 (VTA1) or C45-74 (VNA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-3 (VNC2) or C45-120 (VNVC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-1 (VTI2) or C45-117 (VNA2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C89-3 (VNVC) - C46-120 (VNVC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C89-2 (VNE2) - C46-97 (VNE2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C89-1 (VTA1) - C46-74 (VNA)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-3 (VNC2) - C46-120 (VNVC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-2 (VE2) - C46-97 (VNE2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-1 (VTI2) - C46-117 (VNA2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C89-3 (VNVC) or C46-120 (VNVC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C89-1 (VTA1) or C46-74 (VNA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-3 (VNC2) or C46-120 (VNVC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-1 (VTI2) or C46-117 (VNA2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the nozzle vane position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002PPN05OX_04_0017" fin="false">OK</down>
<right ref="RM000002PPN05OX_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PPN05OX_04_0017" proc-id="RM22W0E___00003TY00001">
<testtitle>INSPECT ECM (VNVC VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the nozzle vane position sensor connector.</ptxt>
<figure>
<graphic graphicname="A251333E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C89-3 (VNVC) - C89-2 (VNE2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z66-3 (VNC2) - z66-2 (VE2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Bank 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Bank 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Nozzle Vane Position Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002PPN05OX_04_0002" fin="false">OK</down>
<right ref="RM000002PPN05OX_04_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PPN05OX_04_0002" proc-id="RM22W0E___00003TT00001">
<testtitle>REPLACE TURBOCHARGER SUB-ASSEMBLY (NOZZLE VANE POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<ptxt>When DTC P2564 or P2565 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the turbocharger sub-assembly (for bank 1) (See page <xref label="Seep01" href="RM0000032A8023X"/>).</ptxt>
</item>
</list1>
<ptxt>When DTC P2588 or P2589 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the turbocharger sub-assembly (for bank 2) (See page <xref label="Seep02" href="RM0000032A8023X"/>).</ptxt>
</item>
</list1>
</content6>
<res>
<right ref="RM000002PPN05OX_04_0014" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000002PPN05OX_04_0012" proc-id="RM22W0E___00003TW00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329203AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000002PPN05OX_04_0014" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000002PPN05OX_04_0006" proc-id="RM22W0E___00003TU00001">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002PPN05OX_04_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002PPN05OX_04_0014" proc-id="RM22W0E___00003TX00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK189X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off and leave the vehicle for 15 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the GTS to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Engine and ECT / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P2564 P2565, P2588 or P2589.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or N/A, idle the engine.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002PPN05OX_04_0015" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002PPN05OX_04_0015">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000002PPN05OX_04_0018">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW065X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>