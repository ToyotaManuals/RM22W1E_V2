<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0019" variety="S0019">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0019_7C3MB_T00FE" variety="T00FE">
<name>OIL PUMP</name>
<para id="RM0000013FA05FX" category="A" type-id="8000E" name-id="AT34X-01" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM0000013FA05FX_01" type-id="01" category="01">
<s-1 id="RM0000013FA05FX_01_0001" proc-id="RM22W0E___00007MW00000">
<ptxt>INSTALL FRONT OIL PUMP OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, tap in a new oil seal.</ptxt>
<figure>
<graphic graphicname="D027961E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09350-30020</s-number>
<s-subnumber>09351-32140</s-subnumber>
</sstitem>
</sst>
<atten4>
<ptxt>The oil seal end should be flush with the outer edge of the pump body.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Coat the lip of the oil seal with MP grease.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05FX_01_0002" proc-id="RM22W0E___00007MX00000">
<ptxt>FIX FRONT OIL PUMP BODY SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the oil pump body on the torque converter clutch.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05FX_01_0003" proc-id="RM22W0E___00007MY00000">
<ptxt>INSTALL FRONT OIL PUMP DRIVEN GEAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the driven gear with ATF.</ptxt>
<figure>
<graphic graphicname="D027959E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the driven gear to the oil pump body.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05FX_01_0004" proc-id="RM22W0E___00007MZ00000">
<ptxt>INSTALL FRONT OIL PUMP DRIVE GEAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the drive gear with ATF.</ptxt>
<figure>
<graphic graphicname="D027958E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the drive gear to the oil pump body.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05FX_01_0005" proc-id="RM22W0E___00007N000000">
<ptxt>INSTALL FRONT OIL PUMP BODY O-RING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat a new O-ring with ATF and install it to the oil pump body.</ptxt>
<figure>
<graphic graphicname="D027963E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05FX_01_0006" proc-id="RM22W0E___00007N100000">
<ptxt>INSTALL STATOR SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the stator shaft with the bolt holes.</ptxt>
<figure>
<graphic graphicname="D027953E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 14 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>112</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05FX_01_0007" proc-id="RM22W0E___00007N200000">
<ptxt>INSTALL CLUTCH DRUM OIL SEAL RING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat 3 new oil seal rings with ATF.</ptxt>
<figure>
<graphic graphicname="D027952E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Squeeze the ends of the 3 oil seal rings together with an overlap distance of 8 mm (0.314 in.) or less, and then install them to the starter shaft groove.</ptxt>
<atten3>
<ptxt>Do not excessively widen the ring.</ptxt>
</atten3>
<atten4>
<ptxt>After installing the oil seal rings, check that they rotate smoothly.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05FX_01_0010" proc-id="RM22W0E___00007MM00000">
<ptxt>INSPECT OIL PUMP DRIVE GEAR ROTATION
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="D027962E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Place the oil pump body on the torque converter clutch.</ptxt>
</s2>
<s2>
<ptxt>Check that the drive gear rotates smoothly.</ptxt>
</s2>
<s2>
<ptxt>Remove the oil pump assembly from the torque converter.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000013FA05FX_01_0009" proc-id="RM22W0E___00007N300000">
<ptxt>INSTALL AUTOMATIC TRANSMISSION CASE O-RING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat a new O-ring with ATF and install it to the oil pump assembly.</ptxt>
<figure>
<graphic graphicname="D027950E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>