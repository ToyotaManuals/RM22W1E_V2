<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SN_T00LQ" variety="T00LQ">
<name>POWER DOOR LOCK CONTROL SYSTEM</name>
<para id="RM000002T6K05PX" category="D" type-id="303FF" name-id="DL1XE-67" from="201301">
<name>OPERATION CHECK</name>
<subpara id="RM000002T6K05PX_z0" proc-id="RM22W0E___0000E7Z00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK POWER DOOR LOCK OPERATION</ptxt>
<step2>
<ptxt>Check the basic function.</ptxt>
<step3>
<ptxt>Check that all doors lock when the door lock control switch (for manual operation) is turned to lock and all doors unlock when turned to unlock.</ptxt>
</step3>
<step3>
<ptxt>Check that all doors lock when the driver side door lock key cylinder is turned to lock using the key.</ptxt>
</step3>
<step3>
<ptxt>When the ignition switch is turned to ON and the driver seat belt is fastened, check that the manual unlock prohibition function operates when the doors are locked using the transmitter or key.</ptxt>
</step3>
<step3>
<ptxt>Check that only the driver side door unlocks when the driver side door key cylinder is turned to unlock once again within 3 seconds using the key (2-step unlocking function).</ptxt>
</step3>
</step2>
<step2>
<ptxt>Check the key lock-in prevention function.</ptxt>
<atten3>
<ptxt>In order to prevent the key from being locked in, the inspection should be made with the driver side door glass open.</ptxt>
</atten3>
<step3>
<ptxt>Turn the ignition switch to ON or ACC.</ptxt>
</step3>
<step3>
<ptxt>With the driver side door open, check that all doors unlock immediately after the door lock knob for the driver side door is locked.</ptxt>
</step3>
<step3>
<ptxt>With the driver side door open, check that all doors unlock immediately after the door control switch (for manual operation) is turned to lock.</ptxt>
</step3>
<step3>
<ptxt>With the driver side door open, lock the driver side door lock knob and hold it for 2 seconds or more. Then close the driver side door and check that all doors unlock.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Check the security function.</ptxt>
<step3>
<ptxt>Close all doors with the driver side door glass open so that the door control switch can be operated from outside the vehicle.</ptxt>
</step3>
<step3>
<ptxt>Remove the key from the inside of the vehicle, open the driver side door and close and lock the door without using the key. Then, check that all doors do not unlock when the door control switch (for manual operation) is turned to unlock from outside the vehicle.</ptxt>
</step3>
<step3>
<ptxt>Remove the key from the inside of the vehicle, and then close and lock the driver side door with the key. Under this condition, check that all doors do not unlock when the door control switch (for manual operation) is turned to unlock from outside the vehicle.</ptxt>
</step3>
<step3>
<ptxt>Remove the key from the inside of the vehicle, close the driver side door and lock the door with the wireless door lock operation. Then, check that all doors do not unlock when the door control switch (for manual operation) is turned to unlock from outside the vehicle.</ptxt>
<atten4>
<ptxt>Check that the security function is canceled under the following conditions:</ptxt>
</atten4>
<list1 type="unordered">
<item>
<ptxt>The ignition switch is turned to ON.</ptxt>
</item>
<item>
<ptxt>The driver side door is unlocked using the key.</ptxt>
</item>
<item>
<ptxt>The door control switch (for manual operation) is turned to unlock after the door lock knob is unlocked manually.</ptxt>
</item>
<item>
<ptxt>The doors are unlocked by a wireless operation.</ptxt>
</item>
</list1>
</step3>
</step2>
<step2>
<ptxt>Check the illumination function.</ptxt>
<step3>
<ptxt>Set the map light switch to the DOOR position.</ptxt>
</step3>
<step3>
<ptxt>With all the doors locked, check that all doors unlock when the driver side door lock cylinder is turned to unlock using the key and that the map light turns on.</ptxt>
</step3>
<step3>
<ptxt>Check that the map light turns off in approximately 15 seconds after the doors have not been opened.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK DOUBLE LOCKING FUNCTION</ptxt>
<atten2>
<ptxt>Do not activate the double locking function when there are people in the vehicle because the doors cannot be opened from the inside of the vehicle.</ptxt>
<ptxt>Under emergency conditions, such as if the power has been cut, this function can be canceled by inserting the key (mechanical key) into the driver door key cylinder and turning it to the unlock side.</ptxt>
</atten2>
<step2>
<ptxt>Check that the double locking function is set.</ptxt>
<step3>
<ptxt>Open all the windows.</ptxt>
</step3>
<step3>
<ptxt>Turn the ignition switch off.</ptxt>
</step3>
<step3>
<ptxt>Open and close the driver side door, and make sure that all doors, including the back door, are closed securely.</ptxt>
</step3>
<step3>
<ptxt>Press the transmitter lock switch twice within 5 seconds, and check that the hazard warning lights flash once at this time to indicate that the double locking function has been set.</ptxt>
<atten4>
<ptxt>The double locking function can be turned on by pressing the lock switch on the outside handle 2 times within 3 seconds.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Try to unlock each door using the inside handle, and check that none of the doors unlock.</ptxt>
<atten3>
<ptxt>The double locking function does not affect the back door.</ptxt>
</atten3>
</step3>
</step2>
<step2>
<ptxt>Check that the double locking function is canceled.</ptxt>
<step3>
<ptxt>Press the transmitter unlock switch while the double locking function is set, and check that the hazard warning lights flash twice at this time to indicate that the system is canceled.</ptxt>
</step3>
<step3>
<ptxt>Check that the doors can be opened by using both the inside and outside handles.</ptxt>
<atten3>
<ptxt>If the battery of the transmitter is depleted, only the driver side door can be opened using the key. However, the theft deterrent system still remains on. The theft alarm will sound when the door is opened without using the transmitter.</ptxt>
</atten3>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>