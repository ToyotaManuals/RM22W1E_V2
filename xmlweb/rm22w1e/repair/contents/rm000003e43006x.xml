<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000003E43006X" category="C" type-id="802M0" name-id="ACIKP-01" from="201301" to="201308">
<dtccode>B1455/55</dtccode>
<dtcname>Driver Side Air Outlet Damper FOOT/DEF Control Servo Motor Circuit</dtcname>
<subpara id="RM000003E43006X_01" type-id="60" category="03" proc-id="RM22W0E___0000H6V00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Air outlet damper servo motor (for DEF mode) sends pulse signals to inform the air conditioning amplifier assembly of the damper position. The air conditioning amplifier assembly activates the motor (normal or reverse) based on the signals to move the air outlet damper (for DEF mode) to any position, which controls the air outlet changes.</ptxt>
<atten4>
<ptxt>Confirm that no mechanical problem is present because this trouble code can be output when either a damper link or damper is mechanically locked.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1455/55</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The air outlet damper position does not change even if the air conditioning amplifier assembly operates the air outlet damper servo motor (for DEF mode)</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Air outlet damper servo motor (for DEF mode)</ptxt>
</item>
<item>
<ptxt>Air conditioning harness assembly</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003E43006X_02" type-id="32" category="03" proc-id="RM22W0E___0000H6W00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E161749E13" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003E43006X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003E43006X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003E43006X_04_0001" proc-id="RM22W0E___0000H6X00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (AIR OUTLET DAMPER SERVO MOTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the servo motor is functioning properly. </ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A/O Damper FOOT/DEF Pos(P)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air outlet damper servo motor (for DEF mode) target pulse /</ptxt>
<ptxt>Min.: 6, Max.: 117</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>FACE1: 117 (pulse)</ptxt>
</item>
<item>
<ptxt>FACE2: 109 (pulse)</ptxt>
</item>
<item>
<ptxt>B/L1: 93 (pulse)</ptxt>
</item>
<item>
<ptxt>B/L2: 81 (pulse)</ptxt>
</item>
<item>
<ptxt>FOOT-D: 52 (pulse)</ptxt>
</item>
<item>
<ptxt>FOOT-R: 41 (pulse)</ptxt>
</item>
<item>
<ptxt>FOOT-F: 41 (pulse)</ptxt>
</item>
<item>
<ptxt>F/D: 18 (pulse)</ptxt>
</item>
<item>
<ptxt>DEF: 6 (pulse)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (Checking from the DTC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (Checking from the PROBLEM SYMPTOMS TABLE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003E43006X_04_0002" fin="false">A</down>
<right ref="RM000003E43006X_04_0008" fin="true">B</right>
<right ref="RM000003E43006X_04_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003E43006X_04_0002" proc-id="RM22W0E___0000H6Y00000">
<testtitle>CHECK AIR OUTLET DAMPER SERVO MOTOR (FOR DEF MODE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air outlet servo motor (for DEF mode) (See page <xref label="Seep01" href="RM000003AXS02VX"/>).</ptxt>
<atten4>
<ptxt>Since the servo motor cannot be inspected while it is removed from the vehicle, replace the servo motor with a normal one.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000002LIT037X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep03" href="RM000002LIT037X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1455/55 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1455/55 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003E43006X_04_0009" fin="true">A</down>
<right ref="RM000003E43006X_04_0003" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000003E43006X_04_0003" proc-id="RM22W0E___0000H6Z00000">
<testtitle>CHECK AIR CONDITIONING HARNESS ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air conditioning harness assembly (See page <xref label="Seep01" href="RM000003AXS02VX"/>).</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep03" href="RM000002LIT037X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000002LIT037X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1455/55 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1455/55 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003E43006X_04_0010" fin="true">A</down>
<right ref="RM000003E43006X_04_0007" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003E43006X_04_0008">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002LIQ022X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E43006X_04_0007">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E43006X_04_0009">
<testtitle>END (AIR OUTLET DAMPER SERVO MOTOR [FOR DEF MODE] IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000003E43006X_04_0010">
<testtitle>END (AIR CONDITIONING HARNESS ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>