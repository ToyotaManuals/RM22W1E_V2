<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001S" variety="S001S">
<name>STEERING COLUMN</name>
<ttl id="12038_S001S_7C3QC_T00JF" variety="T00JF">
<name>STEERING LOCK SYSTEM</name>
<para id="RM000001T1F04BX" category="C" type-id="3053C" name-id="SR2UL-03" from="201301">
<dtccode>B2782</dtccode>
<dtcname>Power Source Control ECU Malfunction</dtcname>
<subpara id="RM000001T1F04BX_01" type-id="60" category="03" proc-id="RM22W0E___0000BCZ00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The steering lock ECU activates the steering lock motor by the power from the main body ECU through the IGE circuit. This prevents the steering from being locked while the vehicle is moving.</ptxt>
<ptxt>The diagnosis information of the steering lock ECU is transmitted to the intelligent tester via the certification ECU (smart key ECU assembly) as the steering lock ECU is not connected to the CAN communication system.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<colspec colname="COL3" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2782</ptxt>
</entry>
<entry valign="middle">
<ptxt>IGE power supply circuit malfunction</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Steering lock actuator assembly (steering lock ECU)</ptxt>
</item>
<item>
<ptxt>Main body ECU</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001T1F04BX_02" type-id="32" category="03" proc-id="RM22W0E___0000BD000000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C165587E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001T1F04BX_03" type-id="51" category="05" proc-id="RM22W0E___0000BD100000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>When the engine switch is off, the main body ECU may occasionally go into a non-active state called sleep mode. Therefore, before proceeding with the inspection, it is necessary to perform the following steps to wake up the ECU:</ptxt>
<ptxt>With the engine switch off, open the driver door. Then (with the engine switch still off) open and close any door several times at 1.5 second intervals.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001T1F04BX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001T1F04BX_04_0001" proc-id="RM22W0E___0000BD200000">
<testtitle>CHECK DTC OUTPUT (B2785)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000001T1B03ZX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2782 is output, but DTC B2785 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T1F04BX_04_0002" fin="false">OK</down>
<right ref="RM000001T1F04BX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T1F04BX_04_0002" proc-id="RM22W0E___0000BD300000">
<testtitle>INSPECT STEERING LOCK ECU</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C165193E08" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E26-3 (IGE) - E26-1 (GND)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Steering lock motor is operating</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E26-3 (IGE) - E26-1 (GND)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Steering lock motor is not operating</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.76in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Within specified range (for Power tilt and telescopic)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Outside specified range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Within specified range (for Manual tilt and telescopic)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When the steering lock is activated and the engine switch is turned on (IG), the steering lock motor activates to release the steering lock. The motor operation occurs for 2 to 15 seconds.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000001T1F04BX_04_0006" fin="true">A</down>
<right ref="RM000001T1F04BX_04_0003" fin="false">B</right>
<right ref="RM000001T1F04BX_04_0010" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001T1F04BX_04_0006">
<testtitle>REPLACE STEERING LOCK ACTUATOR ASSEMBLY (STEERING LOCK ECU)<xref label="Seep01" href="RM0000039SJ014X_01_0001"/>
</testtitle>
</testgrp>
<testgrp id="RM000001T1F04BX_04_0003" proc-id="RM22W0E___0000BD400000">
<testtitle>CHECK HARNESS AND CONNECTOR (STEERING LOCK ECU - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C164812E12" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E26 steering lock ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E1 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E26-3 (IGE) - E1-19 (SLR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E26-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E26-3 (IGE) or E1-19 (SLR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T1F04BX_04_0009" fin="true">OK</down>
<right ref="RM000001T1F04BX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T1F04BX_04_0009">
<testtitle>REPLACE MAIN BODY ECU</testtitle>
</testgrp>
<testgrp id="RM000001T1F04BX_04_0005">
<testtitle>GO TO LIN COMMUNICATION SYSTEM (B2785)<xref label="Seep01" href="RM000002XHV086X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001T1F04BX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001T1F04BX_04_0010">
<testtitle>REPLACE STEERING LOCK ACTUATOR ASSEMBLY (STEERING LOCK ECU)<xref label="Seep01" href="RM000000MID008X_01_0055"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>