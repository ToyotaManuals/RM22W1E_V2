<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UU_T00NX" variety="T00NX">
<name>REAR SEATBACK HEATER (for 60/40 Split Seat Type 60 Side)</name>
<para id="RM000003BE900OX" category="A" type-id="80001" name-id="SE6SQ-04" from="201308">
<name>REMOVAL</name>
<subpara id="RM000003BE900OX_02" type-id="11" category="10" proc-id="RM22W0E___0000G7100001">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003BE900OX_01" type-id="01" category="01">
<s-1 id="RM000003BE900OX_01_0001" proc-id="RM22W0E___0000G6X00001">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep01" href="RM00000390Z00RX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BE900OX_01_0028" proc-id="RM22W0E___0000G2K00001">
<ptxt>REMOVE SEAT ADJUSTER BOLT COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184012" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0029" proc-id="RM22W0E___0000G2L00001">
<ptxt>REMOVE RECLINING ADJUSTER RELEASE HANDLE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184013" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw and release handle.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0030" proc-id="RM22W0E___0000G2M00001">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184014E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws in the order shown in the illustration, and then remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0031" proc-id="RM22W0E___0000G2N00001">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184015" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0032" proc-id="RM22W0E___0000G2O00001">
<ptxt>REMOVE UPPER SEAT TRACK RAIL COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181222" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0033" proc-id="RM22W0E___0000G2P00001">
<ptxt>REMOVE UPPER SEAT TRACK RAIL COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184035" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0034" proc-id="RM22W0E___0000G2R00001">
<ptxt>REMOVE REAR NO. 2 SEAT LEG SIDE COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 screws and detach the 4 claws.</ptxt>
<figure>
<graphic graphicname="B181224" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt that is securing the fold seat stopper band and remove the seat leg side cover together with the fold seat stopper band.</ptxt>
<figure>
<graphic graphicname="B184044" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0035" proc-id="RM22W0E___0000G2J00001">
<ptxt>REMOVE REAR NO. 2 SEAT PROTECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Detach the claw and remove the wire harness connector.</ptxt>
<figure>
<graphic graphicname="B184016" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Detach the claw and remove the wire harness connector.</ptxt>
</s2>
<s2>
<ptxt>Using a clip remover, remove the clip.</ptxt>
<figure>
<graphic graphicname="B182595" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and remove the seat protector from the seat hinge.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws, open the protector and remove wire harness.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0036" proc-id="RM22W0E___0000G2V00001">
<ptxt>REMOVE REAR NO. 1 SEAT HINGE SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="B184045" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 nuts and hinge.</ptxt>
</s2>
<s2>
<ptxt>Detach the cable clamp and disconnect the cable.</ptxt>
<figure>
<graphic graphicname="B181176" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0037" proc-id="RM22W0E___0000G2W00001">
<ptxt>REMOVE REAR NO. 1 SEAT HINGE SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184017" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 nuts and hinge.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0038" proc-id="RM22W0E___0000G2X00001">
<ptxt>REMOVE SEAT BELT ANCHOR COVER CAP
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw and open the cap.</ptxt>
<figure>
<graphic graphicname="B181227" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw and anchor cover cap.</ptxt>
<figure>
<graphic graphicname="B181228" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0039" proc-id="RM22W0E___0000G2Y00001">
<ptxt>REMOVE REAR SEATBACK STAY COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181226" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0040" proc-id="RM22W0E___0000G2Z00001">
<ptxt>REMOVE REAR UNDER SIDE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 10 claws.</ptxt>
<figure>
<graphic graphicname="B181229" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 5 screws.</ptxt>
<figure>
<graphic graphicname="B181230" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 clips and 2 claws, and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0041" proc-id="RM22W0E___0000G3100001">
<ptxt>REMOVE REAR SEAT CUSHION COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181231" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0042" proc-id="RM22W0E___0000G3200001">
<ptxt>REMOVE REAR SEAT UNDER TRAY COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181181" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0051" proc-id="RM22W0E___0000G6Z00001">
<ptxt>REMOVE REAR NO. 1 SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184531E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and belt.</ptxt>
<atten4>
<ptxt>The No. 1 seat 3 point type belt anchor is fixed with the same bolt as the rear No. 1 seat inner belt LH. Therefore, it becomes disconnected when the bolt is removed.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0052" proc-id="RM22W0E___0000G7000001">
<ptxt>DISCONNECT NO. 1 SEAT 3 POINT TYPE BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184533E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the belt anchor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0043" proc-id="RM22W0E___0000G3400001">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<s3>
<ptxt>Detach the side airbag wire harness clamps.</ptxt>
</s3>
<s3>
<ptxt>Remove the side airbag wire harness through the hole in the seat cushion.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<s3>
<ptxt>Disconnect the 2 seat heater connectors.</ptxt>
<figure>
<graphic graphicname="B182596" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the seatback heater wire harness through the hole in the seat cushion. </ptxt>
<figure>
<graphic graphicname="B182597" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Remove the seat cushion cover with pad.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0044" proc-id="RM22W0E___0000G3700001">
<ptxt>REMOVE REAR SEATBACK COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181188E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0045" proc-id="RM22W0E___0000G3800001">
<ptxt>REMOVE REAR SEATBACK BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181241" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<s3>
<ptxt>Detach the 2 claws on the top of the board.</ptxt>
</s3>
<s3>
<ptxt>Pull up the board to detach the 2 claws on the bottom of the board and remove it.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<figure>
<graphic graphicname="B181240" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Detach the 3 claws on the top of the board.</ptxt>
</s3>
<s3>
<ptxt>Pull up the board to detach the 3 claws on the bottom of the board and remove it.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0021">
<ptxt>REMOVE REAR SEAT HEADREST ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM000003BE900OX_01_0046" proc-id="RM22W0E___0000G3900001">
<ptxt>REMOVE REAR NO. 1 SEAT HEADREST SUPPORT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181190" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Detach the 4 claws and remove the 2 supports.</ptxt>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Detach the 8 claws and remove the 4 supports.</ptxt>
<figure>
<graphic graphicname="B181243" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0047" proc-id="RM22W0E___0000G3A00001">
<ptxt>REMOVE SHOULDER BELT ANCHOR COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181238E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0048" proc-id="RM22W0E___0000G3B00001">
<ptxt>REMOVE REAR SEAT BELT HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181239" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0049" proc-id="RM22W0E___0000G3C00001">
<ptxt>REMOVE SEATBACK COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Remove the hog rings.</ptxt>
<figure>
<graphic graphicname="B262667" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Remove the hog rings.</ptxt>
<figure>
<graphic graphicname="B262668" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<s3>
<ptxt>Remove the cap nut and seatback cover bracket from the seat frame.</ptxt>
<figure>
<graphic graphicname="B184020" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the side airbag wire harness through the hole in the seatback.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<s3>
<ptxt>Cut the cable tie and disconnect the seatback heater wire harness.</ptxt>
<figure>
<graphic graphicname="B182598E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the seatback heater wire harness through the hole in the seatback.</ptxt>
<figure>
<graphic graphicname="B182638" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Pull the seat belt into the inside of the seat.</ptxt>
<figure>
<graphic graphicname="B181245" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Pull the seat belt into the inside of the seat.</ptxt>
<figure>
<graphic graphicname="B181248" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the seatback cover with pad.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0050" proc-id="RM22W0E___0000G3D00001">
<ptxt>REMOVE REAR SEPARATE TYPE SEATBACK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Disconnect the seatback cover bracket from the seatback pad.</ptxt>
<figure>
<graphic graphicname="B184021" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Remove the hog rings and rear seatback cover.</ptxt>
<figure>
<graphic graphicname="B181246" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Remove the hog rings and rear seatback cover.</ptxt>
<figure>
<graphic graphicname="B181249" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003BE900OX_01_0027" proc-id="RM22W0E___0000G6Y00001">
<ptxt>REMOVE REAR SEATBACK HEATER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181195" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Cut off the tack pins which fasten the seatback heater to the seatback cover, and then remove the seatback heater from the seatback cover.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>