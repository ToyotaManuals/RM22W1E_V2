<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002K" variety="S002K">
<name>WIPER / WASHER</name>
<ttl id="12068_S002K_7C3Y9_T00RC" variety="T00RC">
<name>WIPER AND WASHER SYSTEM (w/ Rain Sensor)</name>
<para id="RM000002M5W013X" category="D" type-id="303F2" name-id="WW0XW-17" from="201301">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000002M5W013X_z0" proc-id="RM22W0E___0000IS300000">
<content5 releasenbr="1">
<step1>
<ptxt>FRONT WIPER SYSTEM</ptxt>
<step2>
<ptxt>The front wiper system receives the wiper operation signal from the windshield wiper switch, and the windshield wiper ECU operates the front wiper motor.</ptxt>
</step2>
<step2>
<ptxt>The windshield wiper ECU adjusts the intermittent interval during front wiper motor operation according to commands received from the windshield wiper switch. This adjustment is based on vehicle speed, brake signals, etc., and a determination of the vehicle driving and stopping conditions during the intermittent operation. The system has the following functions:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Function</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle speed sensing intermittent wiper function*</ptxt>
</entry>
<entry valign="middle">
<ptxt>This function controls the intermittent interval of the wipers according to the vehicle speed when the wiper switch is in the INT position.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle speed switching function</ptxt>
</entry>
<entry valign="middle">
<ptxt>This function automatically switches the wipers on or off when the vehicle is stopped or starts moving with the wiper switch in the LO position.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt> *: The vehicle speed sensing intermittent wiper function operates even when the fail-safe function operates due to a malfunction in the auto wiper function.</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>FRONT WASHER SYSTEM</ptxt>
<step2>
<ptxt>The front washer system receives the wiper operation signal from the windshield wiper switch, and the windshield wiper ECU operates the front washer motor.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CONTINUOUS FRONT WASHER WIPER SYSTEM</ptxt>
<step2>
<ptxt>The continuous front washer system receives the washer operation signal from the windshield wiper switch, and the windshield wiper ECU operates the front wiper motor. The front wipers are operated 3 times after the front washer is stopped.</ptxt>
</step2>
<step2>
<ptxt>The front wiper stops after operating 3 times from the point where the front washer switch has been on for more than 40 seconds continuously. The front wipers are then operated once at an interval of 3 to 7 seconds. The interval is determined by the vehicle speed signal received from the combination meter.</ptxt>
</step2>
</step1>
<step1>
<ptxt>AUTOMATIC WIPER SYSTEM</ptxt>
<step2>
<ptxt>In the automatic wiper system, the rain sensor mounted in the upper portion of the windshield sends a front wiper operation command, based on the amount of rainfall, to the windshield wiper ECU. The front wipers operate when the windshield wiper switch is in the AUTO position. The rain sensor sensitivity can be adjusted with the rain sensor adjusting switch.</ptxt>
</step2>
<step2>
<ptxt>In this system, the windshield wiper ECU receives the vehicle speed signal from the combination meter, and operates the front wipers according to the vehicle speed.</ptxt>
</step2>
<step2>
<ptxt>The windshield wiper ECU adjusts the intermittent interval during wiper motor operation according to commands received from the rain sensor. This adjustment is based on vehicle speed, etc., and a determination of the vehicle driving and stopping conditions during the intermittent operation.</ptxt>
</step2>
</step1>
<step1>
<ptxt>HEADLIGHT CLEANER SYSTEM (w/ Headlight Cleaner System)</ptxt>
<step2>
<ptxt>The headlight cleaner system operates when the headlights are on and the headlight cleaner switch is pushed on. It also operates when the headlights are on and the front wiper washer system is operating. However, it does not operate when the daytime running light system is operating.</ptxt>
</step2>
<step2>
<ptxt>The headlight cleaner system also operates when the front washer switch is first operated with the headlights on after the engine switch is on (IG).*</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Headlight Auto Leveling</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>WASHER NOZZLE HEATER SYSTEM (w/ Washer Nozzle Heater System)</ptxt>
<step2>
<ptxt>The washer nozzle heater system operates when the ambient temperature drops to 5°C (41°F) or less while the engine switch is on (IG).</ptxt>
</step2>
<step2>
<ptxt>The washer nozzle heater system stops operating when the engine switch is turned off, or when the ambient temperature reaches 6°C (42°F) or higher while the engine switch is on (IG).</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>