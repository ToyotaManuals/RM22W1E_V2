<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7C6SQ_T00U8" variety="T00U8">
<name>LIGHTING (INT)</name>
<para id="RM000004AW400RX" category="F" type-id="30027" name-id="SS4QM-28" from="201301">
<name>SERVICE DATA</name>
<subpara id="RM000004AW400RX_z0" proc-id="RM22W0E___0000JYL00000">
<content5 releasenbr="1">
<table pgwide="1">
<title>VANITY LIGHT</title>
<tgroup cols="4" align="center">
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Connector Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle">
<ptxt>Component without harness connected</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>2 (SW) - 1 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Visor raised</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Visor lowered and mirror cover closed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Visor lowered and mirror cover open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>ROOM LIGHT (for Front)</title>
<tgroup cols="4" align="center">
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Connector Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="6" valign="middle">
<ptxt>Component without harness connected</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R7-6 (RILL) - R7-17 (GND9)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear dome light off</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear dome light on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>R7-10 (ILL-) - R7-17 (GND9)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight dimmer switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Headlight dimmer switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>R7-13 (ILL+) - R7-17 (GND9)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (ACC)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>R7-15 (+B) - R7-17 (GND9)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>R7-16 (DOME) - R7-17 (GND9)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>FRONT DOOR COURTESY SWITCH</title>
<tgroup cols="4" align="center">
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Connector Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Component without harness connected</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin not pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pin pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>REAR DOOR COURTESY SWITCH</title>
<tgroup cols="4" align="center">
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Connector Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Component without harness connected</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin not pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pin pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>BACK DOOR COURTESY SWITCH (for Spare Wheel Carrier)</title>
<tgroup cols="4" align="center">
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Connector Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Component without harness connected</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin not pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pin pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>RELAY</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery voltage not applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery voltage applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>