<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R4_T00K7" variety="T00K7">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM00000183U08QX" category="C" type-id="8011G" name-id="AV6XS-04" from="201301" to="201308">
<dtccode>01-D6</dtccode>
<dtcname>No Master</dtcname>
<dtccode>01-D7</dtccode>
<dtcname>Connection Check Error</dtcname>
<subpara id="RM00000183U08QX_03" type-id="60" category="03" proc-id="RM22W0E___0000C9I00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>01-D6</ptxt>
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The device that stores (stored) the DTC has (had) been disconnected while the engine switch on (IG) or (ACC).</ptxt>
</item>
<item>
<ptxt>The master device has (had) been disconnected when this DTC is stored.</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Radio receiver assembly power source circuit</ptxt>
</item>
<item>
<ptxt>Power source circuit of the component which has stored this code</ptxt>
</item>
<item>
<ptxt>AVC-LAN circuit between the radio receiver assembly and component which has stored this code</ptxt>
</item>
<item>
<ptxt>Component which has stored this code</ptxt>
</item>
<item>
<ptxt>Radio receiver assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>01-D7</ptxt>
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The device that stored the code has (had) been disconnected after the engine starts (started).</ptxt>
</item>
<item>
<ptxt>The master device has (had) been disconnected when this DTC is (was) stored.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: Even if no fault is present, this DTC may be stored depending on the battery condition or engine start voltage.</ptxt>
</item>
<item>
<ptxt>*2: When 210 seconds have elapsed after disconnecting the power supply connector of the master component with the engine switch on (IG) or (ACC), this DTC is stored.</ptxt>
</item>
</list1>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before starting troubleshooting, be sure to clear the DTCs stored due to the reasons described in the HINT above. Then, check for DTCs and troubleshoot according to the output DTCs.</ptxt>
</item>
<item>
<ptxt>The radio receiver assembly is the master unit.</ptxt>
</item>
<item>
<ptxt>Be sure to clear and recheck for the DTCs after the inspection is completed to confirm that no DTCs are output.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM00000183U08QX_01" type-id="51" category="05" proc-id="RM22W0E___0000C9800000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Be sure to read Description before performing the following procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000183U08QX_02" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000183U08QX_02_0001" proc-id="RM22W0E___0000C9900000">
<testtitle>CHECK RADIO RECEIVER ASSEMBLY POWER SOURCE CIRCUIT</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>Refer to Radio Receiver Power Source Circuit (See page <xref label="Seep01" href="RM0000012CI0J4X"/>).</ptxt>
</atten4>
<ptxt>If the power source circuit is operating normally, proceed to the next step.</ptxt>
</content6>
<res>
<down ref="RM00000183U08QX_02_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000183U08QX_02_0009" proc-id="RM22W0E___0000C9E00000">
<testtitle>IDENTIFY COMPONENT WHICH HAS STORED THIS CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Enter diagnostic mode.</ptxt>
<figure>
<graphic graphicname="E110915E13" width="7.106578999in" height="6.791605969in"/>
</figure>
</test1>
<test1>
<ptxt>Press preset switch "2" to change the mode to "Detailed Information Mode".</ptxt>
</test1>
<test1>
<ptxt>Identify the component which has stored this code.</ptxt>
<table pgwide="1">
<title>Component Table</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle">
<ptxt>Physical Address</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Stereo component amplifier assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>440</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Multi-media interface ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>388</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>"P440" set by the stereo component amplifier assembly shown in the preceding illustration as an example.</ptxt>
</item>
<item>
<ptxt>For details of the DTC display, refer to DTC Check/Clear (See page <xref label="Seep01" href="RM0000014CZ0DBX"/>).</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000183U08QX_02_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000183U08QX_02_0013" proc-id="RM22W0E___0000C9H00000">
<testtitle>CHECK COMPONENT SHOWN BY SUB-CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Select the component shown by the sub-code.</ptxt>
<table pgwide="1">
<title>Component Table</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Except radio receiver assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Radio receiver assembly (190)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000183U08QX_02_0002" fin="false">A</down>
<right ref="RM00000183U08QX_02_0007" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000183U08QX_02_0002" proc-id="RM22W0E___0000C9A00000">
<testtitle>CHECK POWER SOURCE CIRCUIT OF COMPONENT WHICH HAS STORED THIS CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the power source circuit of the component which has stored this code.</ptxt>
<ptxt>If the power source circuit is operating normally, proceed to the next step.</ptxt>
<table pgwide="1">
<title>Component Table</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Stereo component amplifier assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stereo component amplifier power source circuit (See page <xref label="Seep02" href="RM0000012CJ0CMX"/>)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Multi-media interface ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-media interface ECU power source circuit (See page <xref label="Seep03" href="RM000003XYW04NX"/>)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000183U08QX_02_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000183U08QX_02_0003" proc-id="RM22W0E___0000C9B00000">
<testtitle>INSPECT RADIO RECEIVER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F1 and F55 radio receiver assembly connectors.</ptxt>
<figure>
<graphic graphicname="E194165E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F1-5 (ATX+) - F1-15 (ATX-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F55-9 (TXM+) - F55-10 (TXM-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Component without harness connected</ptxt>
<ptxt>(Radio Receiver Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Component without harness connected</ptxt>
<ptxt>(Radio Receiver Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000183U08QX_02_0004" fin="false">OK</down>
<right ref="RM00000183U08QX_02_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000183U08QX_02_0004" proc-id="RM22W0E___0000C9C00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AVC-LAN CIRCUIT)</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>For details of the connectors, refer to Terminals of ECU (See page <xref label="Seep01" href="RM0000012A70DSX"/>).</ptxt>
</atten4>
<test1>
<ptxt>Referring to the following AVC-LAN wiring diagram, check the AVC-LAN circuit between the radio receiver assembly and component which has stored this code.</ptxt>
<test2>
<ptxt>Disconnect all connectors between the radio receiver assembly and component which has stored this code.</ptxt>
</test2>
<test2>
<ptxt>Check for an open or short in the AVC-LAN circuit between the radio receiver assembly and component which has stored this code.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There is no open or short circuit.</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="E194159E07" width="7.106578999in" height="5.787629434in"/>
</figure>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000183U08QX_02_0005" fin="false">OK</down>
<right ref="RM00000183U08QX_02_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000183U08QX_02_0005" proc-id="RM22W0E___0000C9D00000">
<testtitle>REPLACE COMPONENT WHICH HAS STORED THIS CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the component which has stored this code with a known good one. </ptxt>
</test1>
</content6>
<res>
<down ref="RM00000183U08QX_02_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000183U08QX_02_0011" proc-id="RM22W0E___0000C9F00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000014CZ0DBX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000183U08QX_02_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000183U08QX_02_0012" proc-id="RM22W0E___0000C9G00000">
<testtitle>RECHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same problem occurs again.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction disappears.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000183U08QX_02_0006" fin="true">OK</down>
<right ref="RM00000183U08QX_02_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000183U08QX_02_0006">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM00000183U08QX_02_0007">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AI400BX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000183U08QX_02_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>