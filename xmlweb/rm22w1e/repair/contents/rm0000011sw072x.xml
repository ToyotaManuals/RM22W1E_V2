<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZC_T00SF" variety="T00SF">
<name>LIGHTING SYSTEM</name>
<para id="RM0000011SW072X" category="C" type-id="303CC" name-id="LE63P-02" from="201308">
<dtccode>B2415</dtccode>
<dtcname>Vehicle Speed Sensor Malfunction</dtcname>
<subpara id="RM0000011SW072X_01" type-id="60" category="03" proc-id="RM22W0E___0000J6S00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The headlight swivel ECU receives signals indicating the front wheel speed from the skid control ECU using CAN communication.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2415</ptxt>
</entry>
<entry valign="middle">
<ptxt>Malfunction in the vehicle speed sensor.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Vehicle stability control system (w/ VSC)</ptxt>
</item>
<item>
<ptxt>Anti-lock brake system (w/o VSC)</ptxt>
</item>
<item>
<ptxt>Headlight swivel ECU assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000011SW072X_02" type-id="32" category="03" proc-id="RM22W0E___0000J6T00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E246612E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000011SW072X_03" type-id="51" category="05" proc-id="RM22W0E___0000J6U00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>First perform the communication function inspections in How to Proceed with Troubleshooting to confirm that there are no CAN communication malfunctions before troubleshooting this DTC.</ptxt>
</item>
<item>
<ptxt>After replacing the headlight swivel ECU, initialization of the ECU is necessary (except w/ Dynamic Headlight Auto Leveling) (See page <xref label="Seep01" href="RM000002CE4030X"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM0000011SW072X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000011SW072X_04_0001" proc-id="RM22W0E___0000J6V00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002WP401YX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000002WP401YX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2415 output does not occur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011SW072X_04_0013" fin="true">OK</down>
<right ref="RM0000011SW072X_04_0023" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SW072X_04_0013">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SW072X_04_0023" proc-id="RM22W0E___0000J6X00001">
<testtitle>CHECK VEHICLE TYPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the vehicle type.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>w/ VSC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/o VSC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011SW072X_04_0012" fin="false">A</down>
<right ref="RM0000011SW072X_04_0024" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM0000011SW072X_04_0012" proc-id="RM22W0E___0000J6W00001">
<testtitle>CHECK FOR DTC (VEHICLE STABILITY CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC output does not occur.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011SW072X_04_0004" fin="true">A</down>
<right ref="RM0000011SW072X_04_0026" fin="true">B</right>
<right ref="RM0000011SW072X_04_0010" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000011SW072X_04_0024" proc-id="RM22W0E___0000J6Y00001">
<testtitle>CHECK FOR DTC (ANTI-LOCK BRAKE SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00VX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000046KV00VX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC output does not occur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011SW072X_04_0004" fin="true">OK</down>
<right ref="RM0000011SW072X_04_0025" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SW072X_04_0004">
<testtitle>REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY<xref label="Seep01" href="RM000003A5S007X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SW072X_04_0010">
<testtitle>GO TO VEHICLE STABILITY CONTROL SYSTEM<xref label="Seep01" href="RM000001DWP02NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SW072X_04_0025">
<testtitle>GO TO ANTI-LOCK BRAKE SYSTEM<xref label="Seep01" href="RM000001DWP02OX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SW072X_04_0026">
<testtitle>REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY<xref label="Seep01" href="RM000003AV900DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>