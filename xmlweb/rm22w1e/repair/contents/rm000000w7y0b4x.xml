<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3ML_T00FO" variety="T00FO">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV)</name>
<para id="RM000000W7Y0B4X" category="C" type-id="302FF" name-id="AT9N8-02" from="201308">
<dtccode>P0717</dtccode>
<dtcname>Turbine Speed Sensor Circuit No Signal</dtcname>
<subpara id="RM000000W7Y0B4X_01" type-id="60" category="03" proc-id="RM22W0E___000081200001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This sensor detects the rotation speed of the turbine which shows the input revolution of the transmission. By comparing the input turbine speed signal NT with the counter gear speed sensor signal SP2, the ECM detects the shift timing of the gears and appropriately controls the engine torque and hydraulic pressure according to various conditions. As a result, the gears shift smoothly.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0717</ptxt>
</entry>
<entry valign="middle">
<ptxt>All the conditions below are detected for 5 sec. or more (1 trip detection logic*1, 2 trip detection logic*2).</ptxt>
<ptxt>(a) Gear change is not performed.</ptxt>
<ptxt>(b) The gear position is 4th, 5th or 6th.</ptxt>
<ptxt>(c) The T/M input shaft rpm is 300 rpm or less.</ptxt>
<ptxt>(d) The T/M output shaft rpm is 1000 rpm or more.</ptxt>
<ptxt>(e) The park/neutral position switch:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The NSW input signal is OFF.</ptxt>
</item>
<item>
<ptxt>The R input signal is OFF.</ptxt>
</item>
</list1>
<ptxt>(f) The shift solenoid valves and park/neutral position switch are operating normally.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in speed sensor NT circuit</ptxt>
</item>
<item>
<ptxt>Speed sensor NT</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake or gear, etc.)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: w/ OBD</ptxt>
</item>
<item>
<ptxt>*2: w/o OBD</ptxt>
</item>
</list1>
</atten4>
<ptxt>Reference: Inspect using an oscilloscope.</ptxt>
<ptxt>Check the waveform of the ECM connector.</ptxt>
<spec>
<title>Standard</title>
<table>
<title>for LHD</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COL3" colwidth="1.03in"/>
<colspec colname="COL4" colwidth="1.04in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>C45-4 (NT+) - C45-3 (NT-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 V/DIV., 2 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine idling (shift lever in P or N)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Refer to illustration</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COL3" colwidth="1.03in"/>
<colspec colname="COL4" colwidth="1.04in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>C46-4 (NT+) - C46-3 (NT-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 V/DIV., 2 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine idling (shift lever in P or N)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Refer to illustration</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="C175014E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W7Y0B4X_02" type-id="64" category="03" proc-id="RM22W0E___000081300001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates that a pulse is not output from the speed sensor NT (turbine (input) speed sensor) or is output only a little. The NT terminal of the ECM detects the pulse signal from the speed sensor NT (input RPM). The ECM outputs a gear shift signal comparing the input speed sensor NT with the output speed sensor SP2.</ptxt>
<ptxt>While the vehicle is operating in the 4th, 5th or 6th gear position with the shift lever in D, if the input shaft revolution is less than 300 rpm*1 although the output shaft revolution is 1000 rpm or more*2, the ECM detects the trouble, illuminates the MIL and stores the DTC.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: Pulse is not output or is irregularly output.</ptxt>
</item>
<item>
<ptxt>*2: The vehicle speed is approximately 50 km/h (31 mph) or more.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000W7Y0B4X_07" type-id="32" category="03" proc-id="RM22W0E___000081400001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C155187E23" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W7Y0B4X_08" type-id="51" category="05" proc-id="RM22W0E___000081500001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
<table pgwide="1">
<title>ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>SPD (NT)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Input shaft speed/</ptxt>
<ptxt>Min.: 0 rpm</ptxt>
<ptxt>Max.: 12750 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up:</ptxt>
<list1 type="unordered">
<item>
<ptxt>ON (after warming up engine): Input turbine speed (NT) equal to engine speed</ptxt>
</item>
<item>
<ptxt>OFF (idling with shift lever in N): Input turbine speed (NT) nearly equal to engine speed</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>The data is displayed in increments of 50 rpm.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>SPD (NT) is always 0 while driving:</ptxt>
<ptxt>Open or short in the sensor or circuit.</ptxt>
</item>
<item>
<ptxt>SPD (NT) is always more than 0 and less than 300 rpm while driving the vehicle at 50 km/h (31 mph) or more:</ptxt>
<ptxt>Sensor trouble, improper installation, or intermittent connection trouble of the circuit.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000000W7Y0B4X_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W7Y0B4X_09_0001" proc-id="RM22W0E___000081600001">
<testtitle>INSPECT SPEED SENSOR NT INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the speed sensor NT installation.</ptxt>
<figure>
<graphic graphicname="C159067E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>The installation bolt is tightened properly and there is no clearance between the sensor and transmission case.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W7Y0B4X_09_0002" fin="false">OK</down>
<right ref="RM000000W7Y0B4X_09_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W7Y0B4X_09_0002" proc-id="RM22W0E___000081700001">
<testtitle>INSPECT SPEED SENSOR NT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the speed sensor connector.</ptxt>
<figure>
<graphic graphicname="C159420E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>560 to 680 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W7Y0B4X_09_0003" fin="false">OK</down>
<right ref="RM000000W7Y0B4X_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W7Y0B4X_09_0003" proc-id="RM22W0E___000081800001">
<testtitle>CHECK HARNESS AND CONNECTOR (SPEED SENSOR NT - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="C177486E15" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C45-4 (NT+) - C45-3 (NT-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>560 to 680 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-4 (NT+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-3 (NT-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C46-4 (NT+) - C46-3 (NT-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>560 to 680 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-4 (NT+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-3 (NT-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W7Y0B4X_09_0005" fin="true">OK</down>
<right ref="RM000000W7Y0B4X_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W7Y0B4X_09_0004">
<testtitle>SECURELY INSTALL OR REPLACE SPEED SENSOR NT<xref label="Seep01" href="RM000002BL403LX_01_0001"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W7Y0B4X_09_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329203AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W7Y0B4X_09_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W7Y0B4X_09_0007">
<testtitle>REPLACE SPEED SENSOR NT<xref label="Seep01" href="RM000002BL403LX_01_0001"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>