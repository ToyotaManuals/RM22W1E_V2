<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7C3BL_T004O" variety="T004O">
<name>3UR-FE ENGINE MECHANICAL</name>
<para id="RM000002YUX02LX" category="F" type-id="30027" name-id="SS3MR-47" from="201301">
<name>SERVICE DATA</name>
<subpara id="RM000002YUX02LX_z0" proc-id="RM22W0E___00000A100000">
<content5 releasenbr="1">
<table pgwide="1">
<title>Engine</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<tbody>
<row>
<entry morerows="1">
<ptxt>Ignition timing</ptxt>
</entry>
<entry>
<ptxt>Terminal TC and CG of DLC3 connected</ptxt>
<ptxt>(Transmission in neutral and A/C switch OFF)</ptxt>
</entry>
<entry>
<ptxt>8 to 12° BTDC @ idle</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Terminals TC and CG of DLC3 disconnected</ptxt>
<ptxt>(Transmission in neutral and A/C switch OFF)</ptxt>
</entry>
<entry>
<ptxt>7 to 24° BTDC @ idle</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Idle speed</ptxt>
<ptxt>(Transmission in neutral and A/C switch OFF)</ptxt>
</entry>
<entry>
<ptxt>650 to 750 rpm</ptxt>
</entry>
</row>
<row>
<entry morerows="2">
<ptxt>Compression</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>1300 kPa (13.3 kgf/cm<sup>2</sup>, 189 psi) or more</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>1000 kPa (10.2 kgf/cm<sup>2</sup>, 145 psi)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Difference</ptxt>
</entry>
<entry>
<ptxt>100 kPa (1.0 kgf/cm<sup>2</sup>, 15 psi) or less</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Engine Unit</title>
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COLSPEC3" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.4in"/>
<tbody>
<row>
<entry morerows="1">
<ptxt>Cylinder head set bolt</ptxt>
</entry>
<entry morerows="1">
<ptxt>Outside thread diameter</ptxt>
</entry>
<entry namest="COLSPEC3" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>10.85 to 11.00 mm (0.427 to 0.433 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC3" nameend="COL3">
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>10.6 mm (0.417 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="6">
<ptxt>Camshaft</ptxt>
</entry>
<entry>
<ptxt>Circle runout</ptxt>
</entry>
<entry namest="COLSPEC3" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.04 mm (0.00157 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3">
<ptxt>Cam lobe height</ptxt>
</entry>
<entry morerows="1">
<ptxt>Intake</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>44.291 to 44.441 mm (1.744 to 1.750 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>44.241 mm (1.742 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Exhaust</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>44.196 to 44.346 mm (1.740 to 1.746 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>44.146 mm (1.738 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Journal diameter</ptxt>
</entry>
<entry>
<ptxt>No. 1 journal</ptxt>
</entry>
<entry morerows="1">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>29.956 to 29.970 mm (1.179 to 1.180 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Other journals</ptxt>
</entry>
<entry>
<ptxt>25.959 to 25.975 mm (1.022 to 1.023 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3">
<ptxt>Camshaft oil clearance</ptxt>
</entry>
<entry morerows="1">
<ptxt>No. 1 journal</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry namest="COL3" nameend="COL4">
<ptxt>0.030 to 0.065 mm (0.00118 to 0.00256 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry namest="COL3" nameend="COL4">
<ptxt>0.10 mm (0.00394 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Other journals</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry namest="COL3" nameend="COL4">
<ptxt>0.025 to 0.062 mm (0.000984 to 0.00244 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry namest="COL3" nameend="COL4">
<ptxt>0.09 mm (0.00354 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Timing chain</ptxt>
</entry>
<entry>
<ptxt>No. 1 chain</ptxt>
</entry>
<entry morerows="1">
<ptxt>Chain elongation</ptxt>
</entry>
<entry morerows="1">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>136.9 mm (5.39 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 2 chain</ptxt>
</entry>
<entry>
<ptxt>137.6 mm (5.42 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Sprocket diameter (w/ chain)</ptxt>
</entry>
<entry>
<ptxt>Crankshaft timing sprocket RH</ptxt>
</entry>
<entry namest="COLSPEC3" nameend="COL3" morerows="1">
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>61.4 mm (2.42 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Crankshaft timing sprocket LH</ptxt>
</entry>
<entry>
<ptxt>61.4 mm (2.42 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 2 chain tensioner</ptxt>
</entry>
<entry morerows="3">
<ptxt>Depth</ptxt>
</entry>
<entry namest="COLSPEC3" nameend="COL3" morerows="3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.9 mm (0.0354 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 3 chain tensioner</ptxt>
</entry>
<entry>
<ptxt>0.9 mm (0.0354 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Chain tensioner slipper</ptxt>
</entry>
<entry>
<ptxt>1.0 mm (0.0394 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 1 chain vibration damper</ptxt>
</entry>
<entry>
<ptxt>1.0 mm (0.0394 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Exhaust manifold</ptxt>
</entry>
<entry namest="COL2" nameend="COL3">
<ptxt>Warpage</ptxt>
</entry>
<entry>
<ptxt>0.7 mm (0.0276 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Ring pin</ptxt>
</entry>
<entry morerows="1">
<ptxt>Protrusion height</ptxt>
</entry>
<entry namest="COLSPEC3" nameend="COL3">
<ptxt>A</ptxt>
</entry>
<entry>
<ptxt>7.5 to 8.5 mm (0.295 to 0.335 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC3" nameend="COL3">
<ptxt>B</ptxt>
</entry>
<entry>
<ptxt>3.6 to 4.6 mm (0.142 to 0.181 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Cylinder Head</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<tbody>
<row>
<entry morerows="1">
<ptxt>Camshaft thrust clearance</ptxt>
</entry>
<entry namest="COL2" nameend="COLSPEC0">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.08 to 0.135 mm (0.00315 to 0.00531 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL2" nameend="COLSPEC0">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.15 mm (0.00591 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="2">
<ptxt>Compression spring</ptxt>
</entry>
<entry>
<ptxt>Free length</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>49.39 mm (1.94 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Deviation</ptxt>
</entry>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>1.0 mm (0.0394 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Angle (reference)</ptxt>
</entry>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>2°</ptxt>
</entry>
</row>
<row>
<entry morerows="5">
<ptxt>Intake valve</ptxt>
</entry>
<entry>
<ptxt>Valve stem diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>5.470 to 5.485 mm (0.215 to 0.216 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Margin thickness</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>1.25 mm (0.0492 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>0.50 mm (0.0197 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Overall length</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>105.85 mm (4.17 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>105.35 mm (4.15 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Width</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>1.1 to 1.5 mm (0.0433 to 0.0591 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="5">
<ptxt>Exhaust valve</ptxt>
</entry>
<entry>
<ptxt>Valve stem diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>5.465 to 5.480 mm (0.215 to 0.216 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Margin thickness</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>1.4 mm (0.0551 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>0.50 mm (0.0197 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Overall length</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>110.40 mm (4.35 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>109.90 mm (4.33 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Width</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>1.1 to 1.5 mm (0.0433 to 0.0591 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Valve guide bush</ptxt>
</entry>
<entry>
<ptxt>Inside diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>5.51 to 5.53 mm (0.217 to 0.218 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3">
<ptxt>Valve guide bush oil clearance</ptxt>
</entry>
<entry morerows="1">
<ptxt>Intake</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.025 to 0.060 mm (0.000984 to 0.00236 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.08 mm (0.00315 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Exhaust</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.030 to 0.065 mm (0.00118 to 0.00256 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.10 mm (0.00394 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3">
<ptxt>Cylinder head warpage</ptxt>
</entry>
<entry>
<ptxt>Cylinder head lower side</ptxt>
</entry>
<entry morerows="2">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.05 mm (0.00197 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Intake side</ptxt>
</entry>
<entry>
<ptxt>0.08 mm (0.00315 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Exhaust side</ptxt>
</entry>
<entry>
<ptxt>0.05 mm (0.00197 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>-</ptxt>
</entry>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.10 mm (0.00394 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Valve guide bush</ptxt>
</entry>
<entry>
<ptxt>Bush bore diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>10.285 to 10.306 mm (0.405 to 0.406 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Cylinder Block</title>
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.34in"/>
<colspec colname="COLSPEC0" colwidth="1.50in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.40in"/>
<tbody>
<row>
<entry morerows="15">
<ptxt>Connecting rod</ptxt>
</entry>
<entry morerows="1">
<ptxt>Thrust clearance</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.15 to 0.55 mm (0.00591 to 0.0217 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.70 mm (0.0276 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Oil clearance</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.025 to 0.050 mm (0.000984 to 0.00197 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt> Maximum</ptxt>
</entry>
<entry>
<ptxt>0.070 mm (0.00276 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="5">
<ptxt>Standard sized bearing center wall thickness</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 2</ptxt>
</entry>
<entry>
<ptxt>1.489 to 1.492 mm (0.0586 to0.0587 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 3</ptxt>
</entry>
<entry>
<ptxt>1.492 to 1.495 mm (0.0587 to 0.0589 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 4</ptxt>
</entry>
<entry>
<ptxt>1.495 to 1.498 mm (0.0589 to 0.0590 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 5</ptxt>
</entry>
<entry>
<ptxt>1.498 to 1.501 mm (0.0590 to 0.0591 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 6</ptxt>
</entry>
<entry>
<ptxt>1.501 to 1.504 mm (0.0591 to 0.0592 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 7</ptxt>
</entry>
<entry>
<ptxt>1.504 to 1.507 mm (0.0592 to 0.0593 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3">
<ptxt>Big end inside diameter</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 1</ptxt>
</entry>
<entry>
<ptxt>59.000 to 59.006 mm (2.32283 to 2.32307 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 2</ptxt>
</entry>
<entry>
<ptxt>59.006 to 59.012 mm (2.32307 to 2.32330 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 3</ptxt>
</entry>
<entry>
<ptxt>59.012 to 59.018 mm (2.32330 to 2.32354 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 4</ptxt>
</entry>
<entry>
<ptxt>59.018 to 59.024 mm (2.32354 to 2.32377 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Bend</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.05 mm (0.00197 in.) per 100 mm (3.94 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Twist</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.15 mm (0.00591 in.) per 100 mm (3.94 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="2">
<ptxt>Crankshaft pin</ptxt>
</entry>
<entry morerows="2">
<ptxt>Diameter</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 1</ptxt>
</entry>
<entry>
<ptxt>55.994 to 56.000 mm (2.20448 to 2.20472 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 2</ptxt>
</entry>
<entry>
<ptxt>55.988 to 55.994 mm (2.20425 to 2.20448 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark 3</ptxt>
</entry>
<entry>
<ptxt>55.982 to 55.988 mm (2.20401 to 2.20425 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="2">
<ptxt>Cylinder block</ptxt>
</entry>
<entry>
<ptxt>Warpage</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.07 mm (0.00276 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Cylinder bore</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>94.000 to 94.012 mm (3.7008 to 3.7013 in.) </ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>94.200 mm (3.7087 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="8">
<ptxt>Piston ring</ptxt>
</entry>
<entry morerows="2">
<ptxt>Groove clearance</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>No. 1</ptxt>
</entry>
<entry>
<ptxt>0.020 to 0.070 mm (0.000787 to 0.00276 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>No. 2</ptxt>
</entry>
<entry>
<ptxt>0.020 to 0.060 mm (0.000787 to 0.00236 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Oil</ptxt>
</entry>
<entry>
<ptxt>0.070 to 0.145 mm (0.00276 to 0.00571 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="5">
<ptxt>End gap</ptxt>
</entry>
<entry morerows="2">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>No. 1</ptxt>
</entry>
<entry>
<ptxt>0.22 to 0.32 mm (0.00866 to 0.0126 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 2</ptxt>
</entry>
<entry>
<ptxt>0.35 to 0.45 mm (0.0138 to 0.0177 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Oil</ptxt>
</entry>
<entry>
<ptxt>0.10 to 0.40 mm (0.00394 to 0.0157 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="2">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>No. 1</ptxt>
</entry>
<entry>
<ptxt>0.42 mm (0.0165 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 2</ptxt>
</entry>
<entry>
<ptxt>0.55 mm (0.0217 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Oil</ptxt>
</entry>
<entry>
<ptxt>0.45 mm (0.0177 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3">
<ptxt>Piston</ptxt>
</entry>
<entry morerows="1">
<ptxt>Diameter</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>93.950 to 93.960 mm (3.6988 to 3.6992 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>93.815 mm (3.6935 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Oil clearance</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.020 to 0.072 mm (0.000787 to 0.00283 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.385 mm (0.0152 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="7">
<ptxt>Piston pin</ptxt>
</entry>
<entry morerows="2">
<ptxt>Hole inside diameter</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark A</ptxt>
</entry>
<entry>
<ptxt>21.998 to 22.001 mm (0.86606 to 0.86618 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark B</ptxt>
</entry>
<entry>
<ptxt>22.001 to 22.004 mm (0.86618 to 0.86630 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark C</ptxt>
</entry>
<entry>
<ptxt>22.004 to 22.007 mm (0.86630 to 0.86642 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="2">
<ptxt>Diameter</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark A</ptxt>
</entry>
<entry>
<ptxt>21.998 to 22.001 mm (0.86606 to 0.86618 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark B</ptxt>
</entry>
<entry>
<ptxt>22.001 to 22.004 mm (0.86618 to 0.86630 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark C</ptxt>
</entry>
<entry>
<ptxt>22.004 to 22.007 mm (0.86630 to 0.86642 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Oil clearance</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>-0.002 to 0.004 mm (-0.0000787 to 0.000157 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt> 0.015 mm (0.000591 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="4">
<ptxt>Connecting rod bush</ptxt>
</entry>
<entry morerows="2">
<ptxt>Inside diameter</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark A</ptxt>
</entry>
<entry>
<ptxt>22.005 to 22.008 mm (0.86634 to 0.86645 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark B</ptxt>
</entry>
<entry>
<ptxt>22.008 to 22.011 mm (0.86645 to 0.86657 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Mark C</ptxt>
</entry>
<entry>
<ptxt>22.011 to 22.014 mm (0.86657 to 0.86669 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Oil clearance</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.005 to 0.011 mm (0.000197 to 0.000433 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.03 mm (0.00118 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="11">
<ptxt>Crankshaft</ptxt>
</entry>
<entry morerows="1">
<ptxt>Thrust clearance</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.020 to 0.220 mm (0.000787 to 0.00866 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.30 mm (0.0118 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Thrust washer thickness</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>2.44 to 2.49 mm (0.0961 to 0.0980 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Circle runout</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.06 mm (0.00236 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Main journal</ptxt>
</entry>
<entry>
<ptxt>Diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>66.988 to 67.000 mm (2.6373 to 2.6378 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Taper and out-of-round</ptxt>
</entry>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.02 mm (0.000787 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Crank pin </ptxt>
</entry>
<entry>
<ptxt>Diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>55.982 to 56.000 mm (2.2040 to 2.2047 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Taper and out-of-round</ptxt>
</entry>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.02 mm (0.000787 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3">
<ptxt>Oil clearance</ptxt>
</entry>
<entry morerows="1">
<ptxt>No. 1 and No. 5 journals</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.017 to 0.030 mm (0.000669 to 0.00118 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.050 mm (0.00197 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Other journals</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.024 to 0.037 mm (0.000945 to 0.00146 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.060 mm (0.00236 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="10">
<ptxt>Crankshaft journal bearing center wall thickness</ptxt>
<ptxt>No. 1 and No. 5 journals</ptxt>
</entry>
<entry morerows="5">
<ptxt>Upper bearing</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>4</ptxt>
</entry>
<entry>
<ptxt>2.501 to 2.504 (0.0985 to 0.0986 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>5</ptxt>
</entry>
<entry>
<ptxt>2.504 to 2.507 (0.0986 to 0.0987 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>6</ptxt>
</entry>
<entry>
<ptxt>2.507 to 2.510 (0.0987 to 0.0988 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>7</ptxt>
</entry>
<entry>
<ptxt>2.510 to 2.513 (0.0988 to 0.0989 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>8</ptxt>
</entry>
<entry>
<ptxt>2.513 to 2.516 (0.0989 to 0.0991 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>9</ptxt>
</entry>
<entry>
<ptxt>2.516 to 2.519 (0.0991 to 0.0992 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="4">
<ptxt>Lower bearing</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>5</ptxt>
</entry>
<entry>
<ptxt>2.488 to 2.491 (0.0980 to 0.0981 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>6</ptxt>
</entry>
<entry>
<ptxt>2.491 to 2.494 (0.0981 to 0.0982 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>7</ptxt>
</entry>
<entry>
<ptxt>2.494 to 2.497 (0.0982 to 0.0983 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>8</ptxt>
</entry>
<entry>
<ptxt>2.497 to 2.500 (0.0983 to 0.0984 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>9</ptxt>
</entry>
<entry>
<ptxt>2.500 to 2.503 (0.0984 to 0.0985 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="10">
<ptxt>Bearing center wall thickness</ptxt>
<ptxt>Other journal</ptxt>
</entry>
<entry morerows="5">
<ptxt>Upper bearing</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>3</ptxt>
</entry>
<entry>
<ptxt>2.482 to 2.485 (0.0977 to 0.0978 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>4</ptxt>
</entry>
<entry>
<ptxt>2.485 to 2.488 (0.0978 to 0.0980 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>5</ptxt>
</entry>
<entry>
<ptxt>2.488 to 2.491 (0.0980 to 0.0981 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>6</ptxt>
</entry>
<entry>
<ptxt>2.491 to 2.494 (0.0981 to 0.0982 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>7</ptxt>
</entry>
<entry>
<ptxt>2.494 to 2.497 (0.0982 to 0.0983 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>8</ptxt>
</entry>
<entry>
<ptxt>2.497 to 2.500 (0.0983 to 0.0984 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="4">
<ptxt>Lower bearing</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>4</ptxt>
</entry>
<entry>
<ptxt>2.501 to 2.504 (0.0985 to 0.0986 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>5</ptxt>
</entry>
<entry>
<ptxt>2.504 to 2.507 (0.0986 to 0.0987 in.) </ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>6</ptxt>
</entry>
<entry>
<ptxt>2.507 to 2.510 (0.0987 to 0.0988 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>7</ptxt>
</entry>
<entry>
<ptxt>2.510 to 2.513 (0.0988 to 0.0989 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>8</ptxt>
</entry>
<entry>
<ptxt>2.513 to 2.516 (0.0989 to 0.0991 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Connecting rod bolt</ptxt>
</entry>
<entry morerows="1">
<ptxt>Tension portion diameter</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>8.5 to 8.6 mm (0.335 to 0.339 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL3">
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>8.3 mm (0.327 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3">
<ptxt>Crankshaft bearing cap set bolt</ptxt>
</entry>
<entry morerows="3">
<ptxt>Diameter</ptxt>
</entry>
<entry morerows="1">
<ptxt>Bolt A</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>10.5 to 11.0 mm (0.413 to 0.433 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>10.4 mm (0.409 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Bolt B</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>9.5 to 10.0 mm (0.374 to 0.394 in.) </ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>9.4 mm (0.370 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>