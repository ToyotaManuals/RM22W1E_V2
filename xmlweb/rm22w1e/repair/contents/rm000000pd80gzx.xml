<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000000PD80GZX" category="L" type-id="3001A" name-id="ES00D3-328" from="201301">
<name>PRECAUTION</name>
<subpara id="RM000000PD80GZX_z0" proc-id="RM22W0E___00000B100000">
<content5 releasenbr="2">
<step1>
<ptxt>IGNITION SWITCH EXPRESSION</ptxt>
<step2>
<ptxt>The type of ignition switch used on this model differs according to the specifications of the vehicle. The expressions listed in the table below are used in this section.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Expression</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition Switch (Position)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine Switch (Condition)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LOCK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Switch ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On (ACC)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On (IG)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine Start</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>START</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Start</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>INITIALIZATION</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform initialization (throttle position) after replacing the throttle with motor body assembly or cleaning any throttle body components (See page <xref label="Seep04" href="RM000000Q0M04CX_01_0032"/>).</ptxt>
</item>
<item>
<ptxt>Perform initialization (throttle position) after reconnecting the battery cable or replacing the ECM (See page <xref label="Seep05" href="RM000000Q0M04CX_01_0032"/>).</ptxt>
</item>
<item>
<ptxt>Perform Reset Memory (AT initialization) when replacing the automatic transmission assembly, engine assembly or ECM (See page <xref label="Seep01" href="RM000000W7F0NDX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Initialization cannot be completed only by disconnecting and reconnecting the battery cable.</ptxt>
</atten4>
</step1>
<step1>
<ptxt>WHEN USING GTS</ptxt>
<atten2>
<ptxt>Observe the following items for safety reasons:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Read the GTS instruction books before using the GTS.</ptxt>
</item>
<item>
<ptxt>Make sure that the GTS cable is not caught on the pedals, shift lever or steering wheel when driving with the GTS connected to the vehicle.</ptxt>
</item>
<item>
<ptxt>When driving the vehicle for testing purposes using the GTS, two persons are required. One person drives the vehicle and the other operates the GTS.</ptxt>
</item>
</list1>
</atten2>
</step1>
<step1>
<ptxt>DISCONNECT AND RECONNECT CABLE OF NEGATIVE BATTERY TERMINAL</ptxt>
<step2>
<ptxt>Before performing electrical work, disconnect the cable from the negative (-) battery terminal in order to prevent short circuits and burnouts.</ptxt>
<figure>
<graphic graphicname="D033496E11" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cable</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Negative (-) Battery Terminal</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Before disconnecting and reconnecting the battery cable, turn the ignition switch off and the headlight dimmer switch off. Then loosen the terminal nut completely. Do not damage the cable or terminal.</ptxt>
</step2>
<step2>
<ptxt>When the battery cable is disconnected, the clock and radio settings and stored DTCs are cleared. Therefore, before disconnecting the battery cable, make a note of the settings.</ptxt>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep03" href="RM000003C32005X"/>).</ptxt>
</atten3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>