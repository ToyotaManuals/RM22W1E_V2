<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000001AR104WX" category="C" type-id="303H2" name-id="ESZKI-01" from="201301" to="201308">
<dtccode>P1229</dtccode>
<dtcname>Fuel Pump System</dtcname>
<subpara id="RM000001AR104WX_01" type-id="60" category="03" proc-id="RM22W0E___00003KN00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<title>P1229</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idling for 60 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>The actual fuel pressure exceeds the target fuel pressure by 5000 kPa or more for 30 seconds.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in suction control valve circuit</ptxt>
</item>
<item>
<ptxt>Suction control valve (fuel supply pump assembly)</ptxt>
</item>
<item>
<ptxt>Fuel pressure sensor (common rail assembly (for bank 1))</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P1229</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Common Rail Pressure</ptxt>
</item>
<item>
<ptxt>Target Common Rail Pressure</ptxt>
</item>
<item>
<ptxt>Injection Pressure Correction</ptxt>
</item>
<item>
<ptxt>Target Pump SCV Current</ptxt>
</item>
<item>
<ptxt>Pump SCV Learning Value</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>For more information on the fuel supply pump assembly (suction control valve) and common rail system, refer to System Description (See page <xref label="Seep01" href="RM000000XSN037X"/>).</ptxt>
</item>
<item>
<ptxt>When DTC P1229 is stored, check the internal fuel pressure of the common rail assembly by entering the following menus: Engine and ECT / Data List / Common Rail Pressure, Target Common Rail Pressure.</ptxt>
<ptxt>Under a stable condition such as idling after warming up the engine, Common Rail Pressure is within 5000 kPa of Target Common Rail Pressure when normal.</ptxt>
</item>
<item>
<ptxt>When there is a problem with the movement of the fuel supply pump assembly (when Common Rail Pressure is higher than Target Common Rail Pressure due to a problem in which the suction valve has difficulty closing), the values of Injection Pressure Correction and Target Pump SCV Current decrease.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000001AR104WX_02" type-id="64" category="03" proc-id="RM22W0E___00003KO00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<topic>
<title>P1229 (Fuel overfeed):</title>
<ptxt>The ECM stores this DTC if the actual fuel pressure inside the common rail remains higher than the target fuel pressure, despite the ECM closing the suction control valve. This DTC indicates that the suction control valve may be stuck open, or there may be a short in its circuit.</ptxt>
<ptxt>If this DTC is stored, the ECM enters fail-safe mode and limits the engine power. The ECM continues operating in fail-safe mode until the engine switch is turned off.</ptxt>
</topic>
</content5>
</subpara>
<subpara id="RM000001AR104WX_06" type-id="32" category="03" proc-id="RM22W0E___00003KP00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0088 (See page <xref label="Seep01" href="RM000001884091X_06"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000001AR104WX_07" type-id="51" category="05" proc-id="RM22W0E___00003KQ00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>When this DTC is stored, be sure to carefully examine "Common Rail Pressure", "Target Common Rail Pressure", "Target Pump SCV Current", and "Pump SCV Learning Value" in the freeze frame data.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000001AR104WX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001AR104WX_08_0001" proc-id="RM22W0E___00003KR00000">
<testtitle>CHECK OTHER DTC OUTPUT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P1229 and "P0190, P0192 and/or P0193" are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001AR104WX_08_0021" fin="false">A</down>
<right ref="RM000001AR104WX_08_0010" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000001AR104WX_08_0021" proc-id="RM22W0E___00003KV00000">
<testtitle>READ VALUE USING GTS (COMMON RAIL PRESSURE AND TARGET COMMON RAIL PRESSURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Common Rail Pressure and Target Common Rail Pressure.</ptxt>
</test1>
<test1>
<ptxt>Check that the internal fuel pressure of the common rail assembly is within the specification below.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Common Rail Pressure is within 5000 kPa of Target Common Rail Pressure when engine is idling after warming up the engine.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001AR104WX_08_0026" fin="true">OK</down>
<right ref="RM000001AR104WX_08_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001AR104WX_08_0008" proc-id="RM22W0E___00003KS00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SUCTION CONTROL VALVE - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the suction control valve connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z64-1 (+B) - C45- 104 (PCV+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z64-2 (PCV) - C45-105 (PCV-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z64-1 (+B) or C45- 104 (PCV+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z64-2 (PCV) or C45-105 (PCV-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z64-1 (+B) - C46- 104 (PCV+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z64-2 (PCV) - C46-105 (PCV-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z64-1 (+B) or C46- 104 (PCV+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z64-2 (PCV) or C46-105 (PCV-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the suction control valve connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM000001AR104WX_08_0023" fin="false">OK</right>
<right ref="RM000001AR104WX_08_0014" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001AR104WX_08_0014" proc-id="RM22W0E___00003KU00000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001AR104WX_08_0022" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001AR104WX_08_0022" proc-id="RM22W0E___00003KW00000">
<testtitle>PERFORM ACTIVE TEST USING GTS (TEST THE FUEL LEAK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Test the Fuel Leak / Data List / Common Rail Pressure and Target Common Rail Pressure.</ptxt>
</test1>
<test1>
<ptxt>Take a snapshot with the GTS during the Active Test.</ptxt>
</test1>
<test1>
<ptxt>Measure the difference between the target fuel pressure (Target Common Rail Pressure) and the actual fuel pressure (Common Rail Pressure) when the "Test the Fuel Leak" Active Test is performed (See page <xref label="Seep01" href="RM0000012X107TX"/>).</ptxt>
<atten4>
<ptxt>In order to obtain an exact measurement, perform the Active Test 5 times and measure the difference once each time the Active Test is performed.</ptxt>
</atten4>
<figure>
<graphic graphicname="A227749E02" width="7.106578999in" height="4.7836529in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>The difference between the target fuel pressure and the actual fuel pressure 2 seconds after the Active Test starts is less than 10000 kPa.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>"Target Common Rail Pressure" is the target fuel pressure controlled by the ECM.</ptxt>
</item>
<item>
<ptxt>"Common Rail Pressure" is the actual fuel pressure.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>The difference between the target fuel pressure and the actual fuel pressure 2 seconds after the Active Test starts is less than 10000 kPa.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001AR104WX_08_0023" fin="false">A</down>
<right ref="RM000001AR104WX_08_0027" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001AR104WX_08_0023" proc-id="RM22W0E___00003KX00000">
<testtitle>CLEAN FUEL FILTER CASE AND REPLACE FUEL FILTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clean the fuel filter case and replace the fuel filter.</ptxt>
<atten4>
<ptxt>Be sure to clean the inside of the fuel filter case as the fuel injectors may not operate properly if the fuel filter is installed with foreign matter remaining inside the fuel filter case.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000001AR104WX_08_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001AR104WX_08_0012" proc-id="RM22W0E___00003KT00000">
<testtitle>REPLACE FUEL SUPPLY PUMP ASSEMBLY (SUCTION CONTROL VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the fuel supply pump assembly (See page <xref label="Seep01" href="RM0000031EL006X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001AR104WX_08_0024" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001AR104WX_08_0024" proc-id="RM22W0E___00003KY00000">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM0000031ED006X_01_0003"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform PM forced regeneration (See page <xref label="Seep02" href="RM00000141502YX_01_0014"/>).</ptxt>
<atten4>
<ptxt>When fuel lines are disconnected, air may enter the fuel lines, leading to engine starting trouble. Therefore, perform forced regeneration and bleed the air from the fuel lines.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000001AR104WX_08_0025" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001AR104WX_08_0025" proc-id="RM22W0E___00003KZ00000">
<testtitle>PERFORM SUPPLY PUMP INITIALIZATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform supply pump initialization (See page <xref label="Seep01" href="RM000000TIN06OX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001AR104WX_08_0027" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001AR104WX_08_0027" proc-id="RM22W0E___00003L000000">
<testtitle>CHECK CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Idle the engine for 60 seconds, and then run it at 2500 rpm without load for 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the GTS to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Engine and ECT / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P1229.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or N/A, increase the idling time.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM000001AR104WX_08_0018" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001AR104WX_08_0018">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000001AR104WX_08_0010">
<testtitle>GO TO FUEL RAIL PRESSURE SENSOR CIRCUIT<xref label="Seep01" href="RM00000188008OX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001AR104WX_08_0026">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ10BX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>