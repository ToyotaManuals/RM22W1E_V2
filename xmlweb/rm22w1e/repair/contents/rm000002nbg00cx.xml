<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SD_T00LG" variety="T00LG">
<name>CAN COMMUNICATION SYSTEM (for RHD)</name>
<para id="RM000002NBG00CX" category="U" type-id="303FI" name-id="NW04M-03" from="201301">
<name>SYSTEM DIAGRAM</name>
<subpara id="RM000002NBG00CX_z0" proc-id="RM22W0E___0000DOW00000">
<content5 releasenbr="1">
<step1>
<ptxt>SYSTEM DIAGRAM</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The master cylinder solenoid (skid control ECU) stores steering sensor and yaw rate sensor assembly DTCs and performs DTC communication by receiving information from the steering sensor and yaw rate sensor.</ptxt>
</item>
<item>
<ptxt>The ECM uses the CAN communication system to perform DTC communication instead of the conventional communication line (SIL).</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>SYSTEM DIAGRAM</ptxt>
<figure>
<graphic graphicname="C260966E01" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ECU/Sensor Name</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1</ptxt>
</entry>
<entry>
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2</ptxt>
</entry>
<entry>
<ptxt>Network gateway ECU</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3</ptxt>
</entry>
<entry>
<ptxt>Main body ECU (cowl side junction block LH)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4</ptxt>
</entry>
<entry>
<ptxt>DLC3</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5</ptxt>
</entry>
<entry>
<ptxt>Air conditioning amplifier assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>6</ptxt>
</entry>
<entry>
<ptxt>Center airbag sensor assembly*1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7</ptxt>
</entry>
<entry>
<ptxt>Multi-media module receiver assembly*2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>8</ptxt>
</entry>
<entry>
<ptxt>Headlight swivel ECU assembly*3</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>9</ptxt>
</entry>
<entry>
<ptxt>Combination meter assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>Certification ECU (smart key ECU assembly)*4</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>11</ptxt>
</entry>
<entry>
<ptxt>Multiplex tilt and telescopic ECU*5</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>12</ptxt>
</entry>
<entry>
<ptxt>Outer mirror control ECU assembly*6</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>13</ptxt>
</entry>
<entry>
<ptxt>Windshield wiper ECU*7</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>14</ptxt>
</entry>
<entry>
<ptxt>Position control ECU and switch*6</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>15</ptxt>
</entry>
<entry>
<ptxt>Power back door unit assembly (power back door ECU)*8</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>16</ptxt>
</entry>
<entry>
<ptxt>No. 2 main body ECU*8</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>17</ptxt>
</entry>
<entry>
<ptxt>Master cylinder solenoid (skid control ECU)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>18</ptxt>
</entry>
<entry>
<ptxt>Yaw rate sensor assembly*9</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>19</ptxt>
</entry>
<entry>
<ptxt>Steering control ECU*10</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>20</ptxt>
</entry>
<entry>
<ptxt>Steering angle sensor*9</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>4WD control ECU</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>22</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Parking assist ECU*11</ptxt>
</item>
<item>
<ptxt>Clearance warning ECU assembly*12</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>23</ptxt>
</entry>
<entry>
<ptxt>Seat belt control ECU*13</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>24</ptxt>
</entry>
<entry>
<ptxt>Power steering ECU assembly*14</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>25</ptxt>
</entry>
<entry>
<ptxt>Distance control ECU assembly*15</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>26</ptxt>
</entry>
<entry>
<ptxt>Suspension control ECU*16</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>27</ptxt>
</entry>
<entry>
<ptxt>No. 1 CAN junction connector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Airbag System</ptxt>
</item>
<item>
<ptxt>*2: w/ Navigation System</ptxt>
</item>
<item>
<ptxt>*3: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*4: w/ Entry and Start System</ptxt>
</item>
<item>
<ptxt>*5: w/ Power Tilt and Power Telescopic Steering Column System</ptxt>
</item>
<item>
<ptxt>*6: w/ Seat Memory</ptxt>
</item>
<item>
<ptxt>*7: w/ Rain Sensor</ptxt>
</item>
<item>
<ptxt>*8: w/ Power Back Door System</ptxt>
</item>
<item>
<ptxt>*9: w/ Vehicle Stability Control System</ptxt>
</item>
<item>
<ptxt>*10: w/ Variable Gear Ratio Steering System</ptxt>
</item>
<item>
<ptxt>*11: w/ Parking Assist Monitor System</ptxt>
</item>
<item>
<ptxt>*12: w/ TOYOTA Parking Assist-sensor System</ptxt>
</item>
<item>
<ptxt>*13: w/ Pre-crash Safety System</ptxt>
</item>
<item>
<ptxt>*14: for 1VD-FTV with DPF</ptxt>
</item>
<item>
<ptxt>*15: w/ Dynamic Radar Cruise Control System</ptxt>
</item>
<item>
<ptxt>*16: w/ Active Height Control Suspension</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>V Bus</ptxt>
<figure>
<graphic graphicname="C260069E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</step1>
<step1>
<ptxt>Movement Bus</ptxt>
<figure>
<graphic graphicname="C260070E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</step1>
<step1>
<ptxt>MS Bus</ptxt>
<figure>
<graphic graphicname="C260967E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>