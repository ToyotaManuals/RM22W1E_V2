<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000002IFH02BX" category="C" type-id="302I8" name-id="ES11HB-001" from="201301">
<dtccode>P0606</dtccode>
<dtcname>ECM / PCM Processor</dtcname>
<subpara id="RM000002IFH02BX_01" type-id="60" category="03" proc-id="RM22W0E___00000JB00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM continuously monitors its main and sub CPUs. This self-check ensures that the ECM is functioning properly. If outputs from the CPUs are different and deviate from the standard, the ECM will illuminate the MIL and store a DTC immediately.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0606</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>There is an ECM main CPU error.</ptxt>
</item>
<item>
<ptxt>There is an ECM sub CPU error.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002IFH02BX_06" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002IFH02BX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002IFH02BX_05_0001" proc-id="RM22W0E___00000JC00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (P0606)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14FX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal and wait for 1 minute.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0606 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002IFH02BX_05_0003" fin="true">A</down>
<right ref="RM000002IFH02BX_05_0002" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002IFH02BX_05_0002">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ107X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002IFH02BX_05_0003">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202PX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>