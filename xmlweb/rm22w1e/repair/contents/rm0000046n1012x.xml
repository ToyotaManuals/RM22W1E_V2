<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM0000046N1012X" category="C" type-id="804T9" name-id="ES16DT-002" from="201308">
<dtccode>P144C</dtccode>
<dtcname>Open in Secondary Air Injection System Heater Control Circuit</dtcname>
<dtccode>P144D</dtccode>
<dtcname>Short in Secondary Air Injection System Heater Control Circuit</dtcname>
<subpara id="RM0000046N1012X_01" type-id="60" category="03" proc-id="RM22W0E___00001VT00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM operates the air pump heater relay (AI-PMP HTR) which causes the air pump heater to operate.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COLSPEC0" colwidth="3.12in"/>
<colspec colname="COL2" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P144C</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in the air pump heater relay circuit for 5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air pump heater relay circuit</ptxt>
</item>
<item>
<ptxt>Air pump heater relay (AI-PMP HTR)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P144D</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in the air pump heater relay circuit for 5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air pump heater relay circuit</ptxt>
</item>
<item>
<ptxt>Air pump heater relay (AI-PMP HTR)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000046N1012X_02" type-id="32" category="03" proc-id="RM22W0E___00001VU00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0412 (See page <xref label="Seep01" href="RM0000012FY04RX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000046N1012X_03" type-id="51" category="05" proc-id="RM22W0E___00001VV00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000046N1012X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000046N1012X_04_0001" proc-id="RM22W0E___00001VW00001">
<testtitle>INSPECT AIR PUMP HEATER RELAY (AI-PMP HTR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the air pump heater relay (AI-PMP HTR) (See page <xref label="Seep01" href="RM000003BLB02WX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000046N1012X_04_0002" fin="false">OK</down>
<right ref="RM0000046N1012X_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000046N1012X_04_0002" proc-id="RM22W0E___00001VX00001">
<testtitle>CHECK AIR PUMP HEATER RELAY (AI-PMP HTR) (POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the air pump heater relay (AI-PMP HTR) .</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="A281075E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Air pump heater relay (AI-PMP HTR) terminal 2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Room Relay Block</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Pump Heater Relay (AI-PMP HTR) </ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reinstall the air pump heater relay (AI-PMP HTR) .</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000046N1012X_04_0003" fin="false">OK</down>
<right ref="RM0000046N1012X_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000046N1012X_04_0003" proc-id="RM22W0E___00001VY00001">
<testtitle>CHECK HARNESS AND CONNECTOR (ECM - AIR PUMP HEATER RELAY (AI-PMP HTR))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the air pump heater relay (AI-PMP HTR) .</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COLSPEC2" colwidth="2.35in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Air pump heater relay (AI-PMP HTR) terminal 1 - C46-60 (HAI1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Air pump heater relay (AI-PMP HTR) terminal 1 or C46-60 (HAI1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COLSPEC2" colwidth="2.35in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Air pump heater relay (AI-PMP HTR) terminal 1 - C45-60 (HAI1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Air pump heater relay (AI-PMP HTR) terminal 1 or C45-60 (HAI1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the air pump heater relay (AI-PMP HTR) .</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000046N1012X_04_0005" fin="true">OK</down>
<right ref="RM0000046N1012X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000046N1012X_04_0007">
<testtitle>REPLACE AIR PUMP HEATER RELAY (AI-PMP HTR)</testtitle>
</testgrp>
<testgrp id="RM0000046N1012X_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000046N1012X_04_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000046N1012X_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>