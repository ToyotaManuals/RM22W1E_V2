<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C40D_T00TG" variety="T00TG">
<name>GRAPHIC DECAL</name>
<para id="RM000003B2100CX" category="A" type-id="30014" name-id="ET7WO-02" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM000003B2100CX_01" type-id="11" category="10" proc-id="RM22W0E___0000JOV00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>When installing the stripe tape, heat the vehicle body and stripe tape using a heat light.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>Standard Heating Temperature</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Body</ptxt>
</entry>
<entry valign="middle">
<ptxt>40 to 60°C (104 to 140°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stripe Tape</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not heat the vehicle body and stripe tape excessively.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM000003B2100CX_02" type-id="01" category="01">
<s-1 id="RM000003B2100CX_02_0001" proc-id="RM22W0E___0000JOW00000">
<ptxt>REPAIR INSTRUCTION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the vehicle body surface</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body surface.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any tape adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Installation temperature</ptxt>
<s3>
<ptxt>When the ambient temperature is below 15°C (59°F), perform the installation procedure after warming the vehicle body surface (installation surface of the door frame) and tape up to between 20 and 30°C (68 and 86°F) using a heat light. When the ambient temperature is above 35°C (95°F), cool the vehicle body surface (installation surface of the door frame) and tape down to between 20 and 30°C (68 and 86°F) prior to installation.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The most appropriate temperature for installing the tape is 25°C (77°F).</ptxt>
</item>
<item>
<ptxt>When the temperature is low, the tape turns stiff and falls off easily. When the temperature is high, the tape loses elasticity.</ptxt>
</item>
</list1>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Before installation</ptxt>
<s3>
<ptxt>Make sure any dirt on and around the vehicle body surface where the tape will be installed (installation surface of the door frame) is removed, and that the surface is smooth. If the surface is rough or dirt remains when pressing the tape onto the surface, air will be trapped under the tape and result in a poor appearance.</ptxt>
<atten4>
<ptxt>Spray water on the shop floor to settle any dust.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Key points for handling the tape</ptxt>
<s3>
<ptxt>The tape bends and rolls up easily. Store the tape between flat pieces of cardboard or other similar objects and keep it dry and level.</ptxt>
<atten3>
<ptxt>Do not bend the tape or leave it in high temperature places.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Key points for the installation of the tape (how to use a squeegee and the installation procedure for flat surfaces)</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Position the tape with a high level of accuracy to achieve a neat finish and to avoid peeling.</ptxt>
</item>
<item>
<ptxt>The tape cannot be reused because it deforms and will not fit after removal.</ptxt>
</item>
</list1>
</atten3>
<s3>
<ptxt>To avoid air bubbles, slightly raise the part of the tape that is going to be applied so that its adhesive surface does not touch the vehicle body while applying the tape. Tilt the squeegee at 40 to 50° (for pressing forward) or 30 to 45° (for pulling) to the vehicle body surface and press the tape onto the vehicle body surface with a force of 20 to 30 N (2 to 3 kgf) at a constant slow speed of 3 to 7 cm (1.2 to 2.8 in.) per second.</ptxt>
<figure>
<graphic graphicname="B147252E21" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten3>
<ptxt>Be sure to observe the specified pressing speed, force, and angle of the squeegee to avoid wrinkles or air bubbles.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Either angle of the squeegee (for pressing forward or for pulling) is acceptable.</ptxt>
</item>
<item>
<ptxt>Be sure to apply the tape while removing the release paper 10 to 20 mm (0.393 to 0.787 in.) from the edge of the squeegee.</ptxt>
</item>
</list1>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Key points for the installation of the tape (how to use a squeegee and the installation procedure for hemming surfaces)</ptxt>
<s3>
<figure>
<graphic graphicname="B147253E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If it is difficult to press the tape, press it in several steps as shown in the illustration. Use your fingers or the padded surface of a squeegee to slowly apply the tape to the hem of the vehicle, especially for a small hem.</ptxt>
<atten4>
<ptxt>When applying tape to the backside of a hem, remove the release paper and use your fingers or the padded surface of a squeegee.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Key points for the installation of the tape (how to use a squeegee and the installation procedure for corners)</ptxt>
<s3>
<ptxt>Remove the release paper and apply the tape carefully with your fingers.</ptxt>
</s3>
<s3>
<ptxt>Before applying the tape to each corner, heat the tape using a heat light and gradually apply it to avoid wrinkles on the tape and achieve a neat finish.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check after installation</ptxt>
<s3>
<ptxt>After completing the application, check if the tape is applied neatly. If the tape is not applied neatly, apply new tape.</ptxt>
<atten3>
<ptxt>Do not reuse the tape.</ptxt>
</atten3>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2100CX_02_0002" proc-id="RM22W0E___0000JOX00000">
<ptxt>INSTALL REAR QUARTER STRIPE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the perforation of the stripe tape with the door parting line and body parting line as shown in the illustration and apply the stripe tape.</ptxt>
<figure>
<graphic graphicname="B309354E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Body Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Lid</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the application sheet.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2100CX_02_0003" proc-id="RM22W0E___0000JOY00000">
<ptxt>INSTALL FRONT QUARTER STRIPE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Type A:</ptxt>
<figure>
<graphic graphicname="B309355E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Body Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Lid</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Install the front quarter stripe.</ptxt>
<s3>
<ptxt>Align the perforations of the stripe tape with the door parting line and body parting line as shown in the illustration and apply the stripe tape.</ptxt>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for Type B:</ptxt>
<figure>
<graphic graphicname="B309356E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Body Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Lid</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Install the front quarter stripe.</ptxt>
<s3>
<ptxt>Align the perforations of the stripe tape with the door parting line and body parting line as shown in the illustration and apply the stripe tape.</ptxt>
<atten4>
<ptxt>On the RH side, align the perforation of the stripe tape with the fuel lid parting line.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2100CX_02_0004" proc-id="RM22W0E___0000JOZ00000">
<ptxt>INSTALL REAR DOOR OUTSIDE STRIPE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Type A:</ptxt>
<figure>
<graphic graphicname="B309357E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door Parting Line</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Install the rear door outside stripe.</ptxt>
<s3>
<ptxt>Align the perforations of the stripe tape with the door parting lines as shown in the illustration and apply the stripe tape.</ptxt>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for Type B:</ptxt>
<figure>
<graphic graphicname="B309358E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door Parting Line</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Install the rear door outside stripe.</ptxt>
<s3>
<ptxt>Align the perforations of the stripe tape with the door parting lines as shown in the illustration and apply the stripe tape.</ptxt>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2100CX_02_0005" proc-id="RM22W0E___0000JP000000">
<ptxt>INSTALL FRONT DOOR STRIPE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Type A:</ptxt>
<figure>
<graphic graphicname="B309359E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door Parting Line</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Install the front door stripe.</ptxt>
<s3>
<ptxt>Align the perforations of the stripe tape with the door parting lines as shown in the illustration and apply the stripe tape.</ptxt>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for Type B:</ptxt>
<figure>
<graphic graphicname="B309360E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door Parting Line</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Install the front door stripe.</ptxt>
<s3>
<ptxt>Align the perforations of the stripe tape with the door parting lines as shown in the illustration and apply the stripe tape.</ptxt>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2100CX_02_0006" proc-id="RM22W0E___0000JP100000">
<ptxt>INSTALL FENDER STRIPE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Type A:</ptxt>
<figure>
<graphic graphicname="B309361E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fender Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight Parting Line</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Install the fender stripe.</ptxt>
<s3>
<ptxt>Align the perforated end of the stripe tape with the fender parting line and the other end of the stripe tape with the headlight parting line as shown in the illustration, and apply the stripe tape.</ptxt>
<atten4>
<ptxt>For snorkel-equipped vehicles, there is no Fender Stripe RH.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for Type B:</ptxt>
<figure>
<graphic graphicname="B309362E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Side Turn Signal Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fender Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side Turn Signal Light</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Install the fender stripe.</ptxt>
<s3>
<ptxt>Align the perforated end of the stripe tape with the fender parting line and the other end of the stripe tape with the headlight parting line as shown in the illustration, and apply the stripe tape.</ptxt>
<atten4>
<ptxt>w/ Side Turn Signal Light:</ptxt>
<ptxt>Align the stripe tape with the side turn signal light.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>