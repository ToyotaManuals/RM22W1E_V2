<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM0000053D600OX" category="C" type-id="803XR" name-id="NSAT3-01" from="201308">
<dtccode>B15E5</dtccode>
<dtcname>Electric Throttle Malfunction</dtcname>
<subpara id="RM0000053D600OX_01" type-id="60" category="03" proc-id="RM22W0E___0000CN900001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COLSPEC0" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B15E5</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Electric throttle malfunction</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>SFI system (1GR-FE, 3UR-FE, 1UR-FE)</ptxt>
</item>
<item>
<ptxt>ECD system (1VD-FTV)</ptxt>
</item>
<item>
<ptxt>Multi-media module receiver assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000053D600OX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000053D600OX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000053D600OX_03_0001" proc-id="RM22W0E___0000CNA00001">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs and check if the same DTCs are output.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: <xref label="Seep01" href="RM000001RSO09SX"/>
</ptxt>
</item>
<item>
<ptxt>for RHD: <xref label="Seep02" href="RM000001RSO09TX"/>
</ptxt>
</item>
</list1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs are output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs are output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000053D600OX_03_0002" fin="false">A</down>
<right ref="RM0000053D600OX_03_0005" fin="true">B</right>
<right ref="RM0000053D600OX_03_0010" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000053D600OX_03_0002" proc-id="RM22W0E___0000CNB00001">
<testtitle>CHECK DTC (SFI SYSTEM OR ECD SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs and check if the same DTCs are output.</ptxt>
<atten4>
<ptxt>Refer to SFI system or ECD system.</ptxt>
<list1 type="unordered">
<item>
<ptxt>SFI system (1GR-FE): <xref label="Seep01" href="RM000000PDK185X"/>
</ptxt>
</item>
<item>
<ptxt>SFI system (1UR-FE): <xref label="Seep02" href="RM000000PDK187X"/>
</ptxt>
</item>
<item>
<ptxt>SFI system (3UR-FE): <xref label="Seep03" href="RM000000PDK17ZX"/>
</ptxt>
</item>
<item>
<ptxt>ECD system (1VD-FTV [w/ DPF]): <xref label="Seep04" href="RM000000PDK189X"/>
</ptxt>
</item>
<item>
<ptxt>ECD system (1VD-FTV [w/o DPF]): <xref label="Seep05" href="RM000000PDK18AX"/>
</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000053D600OX_03_0003" fin="false">OK</down>
<right ref="RM0000053D600OX_03_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000053D600OX_03_0003" proc-id="RM22W0E___0000CNC00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0PQX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000053D600OX_03_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000053D600OX_03_0004" proc-id="RM22W0E___0000CND00001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTC are output again (See page <xref label="Seep01" href="RM0000011BU0PQX"/>). </ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000053D600OX_03_0009" fin="true">OK</down>
<right ref="RM0000053D600OX_03_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000053D600OX_03_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000053D600OX_03_0006">
<testtitle>GO TO SFI SYSTEM OR ECD SYSTEM</testtitle>
</testgrp>
<testgrp id="RM0000053D600OX_03_0008">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY024X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000053D600OX_03_0009">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000053D600OX_03_0010">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>