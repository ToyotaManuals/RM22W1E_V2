<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MU_T00FX" variety="T00FX">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV with DPF)</name>
<para id="RM000000W800CYX" category="C" type-id="302FP" name-id="AT5T3-07" from="201308">
<dtccode>P0756</dtccode>
<dtcname>Shift Solenoid "B" Performance (Shift Solenoid Valve S2)</dtcname>
<subpara id="RM000000W800CYX_01" type-id="60" category="03" proc-id="RM22W0E___00008O200001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM uses signals from the output shaft speed sensor and input speed sensor to detect the actual gear position (1st, 2nd, 3rd, 4th, 5th or 6th gear).</ptxt>
<ptxt>Then the ECM compares the actual gear with the shift schedule in the ECM memory to detect mechanical problems with the shift solenoid valves, valve body or automatic transmission (clutch, brake, gear, etc.).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0756</ptxt>
</entry>
<entry valign="middle">
<ptxt>A shift solenoid valve S2 stuck ON malfunction*1:</ptxt>
<ptxt>Shifting to 3rd and 5th gears is impossible.</ptxt>
<ptxt>The ECM determines there is a malfunction when the following conditions are both met (2 trip detection logic).</ptxt>
<ptxt>(a) When the ECM directs the transmission to switch to 5th gear, the actual gear is shifted to 6th.</ptxt>
<ptxt>(b) When the ECM directs the transmission to switch to 6th gear, the actual gear is shifted to 6th.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift solenoid valve S2 remains open</ptxt>
</item>
<item>
<ptxt>Valve body is blocked</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake, gear, etc.)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0756</ptxt>
</entry>
<entry valign="middle">
<ptxt>A shift solenoid valve S2 stuck OFF malfunction*2:</ptxt>
<ptxt>The vehicle starts in 3rd gear and shifting to 6th gear is impossible.</ptxt>
<ptxt>The ECM determines there is a malfunction when the following conditions are both met (2 trip detection logic).</ptxt>
<ptxt>(a) When the ECM directs the transmission to switch to 1st gear, the actual gear is shifted to 3rd.</ptxt>
<ptxt>(b) When the ECM directs the transmission to switch to 6th gear, the actual gear is shifted to 5th.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift solenoid valve S2 remains closed</ptxt>
</item>
<item>
<ptxt>Valve body is blocked</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake, gear, etc.)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Gear positions in the event of a solenoid valve mechanical problem:</ptxt>
<table pgwide="1">
<tgroup cols="7">
<colspec colname="COL1" align="left" colwidth="1.13in"/>
<colspec colname="COL2" colwidth="0.99in"/>
<colspec colname="COL3" colwidth="0.99in"/>
<colspec colname="COL4" colwidth="0.99in"/>
<colspec colname="COL5" colwidth="0.99in"/>
<colspec colname="COL6" colwidth="0.99in"/>
<colspec colname="COL7" colwidth="1.00in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>ECM gear shift command</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6th</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*1: Actual gear position under S2 stuck ON malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6th</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2: Actual gear position under S2 stuck OFF malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM000000W800CYX_02" type-id="64" category="03" proc-id="RM22W0E___00008O300001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates a stuck ON malfunction or stuck OFF malfunction of the shift solenoid valve S2.</ptxt>
<ptxt>The ECM commands gear shifts by turning the shift solenoid valves ON/OFF. When the gear position commanded by the ECM and the actual gear position are not the same, the ECM illuminates the MIL and stores the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W800CYX_06" type-id="51" category="05" proc-id="RM22W0E___00008O400001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the GTS to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the GTS on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the GTS, perform the Active Test.</ptxt>
<atten4>
<ptxt>While driving, the shift position can be forcibly changed with the GTS.</ptxt>
<ptxt>Comparing the shift position commanded by the Active Test with the actual shift position enables you to confirm the problem (See page <xref label="Seep01" href="RM000000O8L0P8X"/>).</ptxt>
</atten4>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Control the Shift Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve and set each shift position</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Press "→" button: Shift up</ptxt>
</item>
<item>
<ptxt>Press "←" button: Shift down</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>It is possible to check the operation of the shift solenoid valves.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<ptxt>The vehicle speed is 50 km/h (31 mph) or less.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>This test can be conducted when the vehicle speed is 50 km/h (31 mph) or less.</ptxt>
</item>
<item>
<ptxt>The 4th to 5th and 5th to 6th up-shifts must be performed with the accelerator pedal released.</ptxt>
</item>
<item>
<ptxt>The 6th to 5th and 5th to 4th down-shifts must be performed with the accelerator pedal released.</ptxt>
</item>
<item>
<ptxt>Do not operate the accelerator pedal for at least 2 seconds after shifting and do not shift successively.</ptxt>
</item>
<item>
<ptxt>The shift position commanded by the ECM is shown in the Data List display on the GTS.</ptxt>
</item>
<item>
<ptxt>The shift solenoid valve S2 turns ON/OFF normally when the shift lever is in D.</ptxt>
<table pgwide="1">
<tgroup cols="7">
<colspec colname="COL1" align="left" colwidth="1.13in"/>
<colspec colname="COL2" colwidth="0.99in"/>
<colspec colname="COL3" colwidth="0.99in"/>
<colspec colname="COL4" colwidth="0.99in"/>
<colspec colname="COL5" colwidth="0.99in"/>
<colspec colname="COL6" colwidth="0.99in"/>
<colspec colname="COL7" colwidth="1.00in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>ECM gear shift command</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6th</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shift solenoid valve S2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000000W800CYX_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W800CYX_07_0001" proc-id="RM22W0E___00008O500001">
<testtitle>CHECK DTC OUTPUT (IN ADDITION TO DTC P0756)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the GTS.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only P0756 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0756 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any other codes besides P0756 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W800CYX_07_0002" fin="false">A</down>
<right ref="RM000000W800CYX_07_0005" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000W800CYX_07_0002" proc-id="RM22W0E___00008O600001">
<testtitle>INSPECT SHIFT SOLENOID VALVE S2</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the shift solenoid valve S2.</ptxt>
<figure>
<graphic graphicname="C208650E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Shift solenoid valve S2 connector terminal - Shift solenoid valve S2 body</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 15 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Apply 12 V battery voltage to the shift solenoid valve and check that the valve moves and makes an operating noise.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery positive (+) → Shift solenoid valve S2 connector</ptxt>
</item>
<item>
<ptxt>Battery negative (-) → Shift solenoid valve S2 body</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Valve moves and makes an operating noise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Shift Solenoid Valve S2)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000W800CYX_07_0011" fin="false">OK</down>
<right ref="RM000000W800CYX_07_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W800CYX_07_0011" proc-id="RM22W0E___00008O700001">
<testtitle>INSPECT TRANSMISSION VALVE BODY ASSEMBLY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the transmission valve body assembly (See page <xref label="Seep01" href="RM000000O9L06JX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There are no foreign objects on any valve.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000W800CYX_07_0004" fin="true">OK</down>
<right ref="RM000000W800CYX_07_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W800CYX_07_0004">
<testtitle>REPAIR OR REPLACE AUTOMATIC TRANSMISSION ASSEMBLY<xref label="Seep01" href="RM0000018ZD04WX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W800CYX_07_0005">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000030G909IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W800CYX_07_0006">
<testtitle>REPLACE SHIFT SOLENOID VALVE S2<xref label="Seep01" href="RM000000O9L06JX_02_0009"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W800CYX_07_0007">
<testtitle>REPAIR OR REPLACE TRANSMISSION VALVE BODY ASSEMBLY<xref label="Seep01" href="RM0000013CM04WX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>