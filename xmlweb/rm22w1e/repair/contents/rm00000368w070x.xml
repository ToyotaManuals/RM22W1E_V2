<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM00000368W070X" category="C" type-id="803OJ" name-id="BCDWW-01" from="201301" to="201308">
<dtccode>C1417</dtccode>
<dtcname>High Power Supply Voltage Malfunction</dtcname>
<subpara id="RM00000368W070X_01" type-id="60" category="03" proc-id="RM22W0E___0000AQ700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If a malfunction is detected in the power supply circuit, the skid control ECU (master cylinder solenoid) stores this DTC and the fail-safe function prohibits ABS/VSC/TRC operation.</ptxt>
<ptxt>This DTC is stored when the IG1 terminal voltage deviates from the DTC detection condition due to a malfunction in the power supply or charging circuit such as the battery or generator circuit, etc.</ptxt>
<ptxt>This DTC is cleared when the IG1 terminal voltage returns to normal.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.92in"/>
<colspec colname="COL2" colwidth="3.09in"/>
<colspec colname="COL3" colwidth="3.07in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1417</ptxt>
</entry>
<entry valign="middle">
<ptxt>The IG1 terminal voltage is 17.4 V or higher for 0.8 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Charging system</ptxt>
</item>
<item>
<ptxt>Power source circuit</ptxt>
</item>
<item>
<ptxt>Internal power supply circuit of the skid control ECU (master cylinder solenoid)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000368W070X_02" type-id="32" category="03" proc-id="RM22W0E___0000AQ800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC C1241 (See page <xref label="Seep01" href="RM000001DXW02GX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000368W070X_03" type-id="51" category="05" proc-id="RM22W0E___0000AQ900000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration (See page <xref label="Seep01" href="RM000001DWZ01OX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000368W070X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000368W070X_04_0009" proc-id="RM22W0E___0000AQA00000">
<testtitle>CHECK HARNESS AND CONNECTOR (IG1 TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the skid control ECU (master cylinder solenoid) connector. </ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214161E73" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-46 (IG1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
<ptxt>(Engine running)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU [Master Cylinder Solenoid])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 1UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000368W070X_04_0013" fin="false">A</down>
<right ref="RM00000368W070X_04_0007" fin="true">B</right>
<right ref="RM00000368W070X_04_0015" fin="true">C</right>
<right ref="RM00000368W070X_04_0016" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM00000368W070X_04_0013" proc-id="RM22W0E___0000AQB00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00NX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000368W070X_04_0014" fin="true">A</down>
<right ref="RM00000368W070X_04_0010" fin="true">B</right>
<right ref="RM00000368W070X_04_0017" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000368W070X_04_0010">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000368W070X_04_0007">
<testtitle>GO TO CHARGING SYSTEM (ON-VEHICLE INSPECTION)<xref label="Seep01" href="RM0000017VF03YX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000368W070X_04_0014">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000368W070X_04_0015">
<testtitle>GO TO CHARGING SYSTEM (ON-VEHICLE INSPECTION)<xref label="Seep01" href="RM000001AR90AOX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000368W070X_04_0016">
<testtitle>GO TO CHARGING SYSTEM (ON-VEHICLE INSPECTION)<xref label="Seep01" href="RM0000017VF03XX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000368W070X_04_0017">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>