<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SM_T00LP" variety="T00LP">
<name>WIRELESS DOOR LOCK CONTROL SYSTEM (w/ Entry and Start System)</name>
<para id="RM000000XX402QX" category="U" type-id="3001G" name-id="DL8CE-02" from="201301">
<name>TERMINALS OF ECU</name>
<subpara id="RM000000XX402QX_z0" proc-id="RM22W0E___0000E7400000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</ptxt>
<figure>
<graphic graphicname="C172609E06" width="7.106578999in" height="8.799559038in"/>
</figure>
<step2>
<ptxt>Disconnect the 2A, 2D and E3 ECU connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2D-62 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E3-1 (GND3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2A-1 (ACC) - 2D-62 (GND2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ACC power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (ACC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2A-1 (IG) - 2D-62 (GND2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>IG power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Reconnect the 2A, 2D and E3 ECU connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E1-24 (DCTY) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>L - Body ground*1</ptxt>
<ptxt>Y - Body ground*2</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Driver side door courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side door open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Driver side door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E2-21 (PCTY) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Y - Body ground*1</ptxt>
<ptxt>L - Body ground*2</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Passenger side door courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Passenger side door open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Passenger side door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>2C-2 (LCTY) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear side door LH courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear side door LH open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rear side door LH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E2-7 (RCTY) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>G - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear side door RH courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear side door RH open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rear side door RH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E2-25 (BCTY) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Back door courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E3-7 (HZSW) - Body ground*4</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>BE - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Turn signal light drive</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hazard warning signal switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Hazard warning signal switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E2-15 (DIM) - 2D-62 (GND2)*3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Headlight low drive</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight dimmer switch low</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2B-18 (HRLY) - 2D-62 (GND2)*3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Headlight (high) drive</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight dimmer switch high</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2A-1 (TRLY) - 2D-62 (GND2)*3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Taillight drive</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight dimmer switch tail</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
<item>
<ptxt>*3: w/ Panic Switch</ptxt>
</item>
<item>
<ptxt>*4: for GRJ200L-GNANKC, URJ202L-GNTEKC</ptxt>
</item>
</list1>
<ptxt>If the result is not as specified, the ECU may have a malfunction.</ptxt>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 1.</ptxt>
<figure>
<graphic graphicname="B122376E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 1 (Reference)</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E1-24 (DCTY) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, and driver side door courtesy switch off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 2.</ptxt>
<figure>
<graphic graphicname="B129408E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 2 (Reference)</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E1-24 (DCTY) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, and driver side door courtesy switch off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 3.</ptxt>
<figure>
<graphic graphicname="B189909E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 3 (Reference)</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E2-21 (PCTY) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, and passenger side door courtesy switch off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 4.</ptxt>
<figure>
<graphic graphicname="B189910E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 4 (Reference)</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E2-21 (PCTY) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, and passenger side door courtesy switch off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 5.</ptxt>
<figure>
<graphic graphicname="B122376E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 5 (Reference)</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2C-2 (LCTY) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, and rear LH side door courtesy switch off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 6.</ptxt>
<figure>
<graphic graphicname="B129408E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 6 (Reference)</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2C-2 (LCTY) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, and rear LH side door courtesy switch off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 7.</ptxt>
<figure>
<graphic graphicname="B122376E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 7 (Reference)</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E2-7 (RCTY) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, and rear RH side door courtesy switch off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 8.</ptxt>
<figure>
<graphic graphicname="B129408E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 8 (Reference)</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E2-7 (RCTY) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, and rear RH side door courtesy switch off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 9.</ptxt>
<figure>
<graphic graphicname="B122376E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 9 (Reference)</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E2-25 (BCTY) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, and Back door closed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 10.</ptxt>
<figure>
<graphic graphicname="B129408E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 10 (Reference)</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E2-25 (BCTY) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, and Back door closed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>CHECK CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</ptxt>
<figure>
<graphic graphicname="B106648E46" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the E29 and E30 ECU connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-17 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-1 (+B) - E29-17 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E29-18 (IG) - E29-17 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>IG power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Reconnect the E29 and E30 ECU connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-39 (RSSI) - E29-17 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>

<entry valign="middle" align="center">
<ptxt>Door control receiver electric wave existence signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, all doors closed and transmitter switch not pressed → pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V → Below 1 V </ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-38 (RDA) - E29-17 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LG - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door control receiver data input signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, all doors closed and transmitter switch not pressed → pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V → 11 to 14 V → Below 1 V </ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-29 (RCO) - E29-17 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door control receiver power source</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, all doors closed and transmitter switch not pressed → pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V → 4.6 to 5.4 V → Below 1 V </ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-21 (BZR) - Body ground*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wireless door lock buzzer output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wireless door lock buzzer ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-21 (BZR) - Body ground*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wireless door lock buzzer output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wireless door lock buzzer OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: Except Europe and China</ptxt>
</atten4>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, the ECU may have a malfunction.</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>CHECK DOOR CONTROL RECEIVER</ptxt>
<figure>
<graphic graphicname="E159309E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the L21 receiver connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L21-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L21-4 (+5) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.6 to 5.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>CHECK POWER BACK DOOR UNIT (w/ Power Back Door System)</ptxt>
<figure>
<graphic graphicname="H100802E30" width="7.106578999in" height="2.775699831in"/>
</figure>
<step2>
<ptxt>Disconnect the L52 unit connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L52-11 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L52-10 (ECUB) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Reconnect the L52 unit connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L52-26 (BZR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power back door warning buzzer signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door warning buzzer is sounding</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L52-26 (BZR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power back door warning buzzer signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door warning buzzer is stopped</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, the ECU may have a malfunction.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>