<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MT_T00FW" variety="T00FW">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1UR-FE)</name>
<para id="RM000003CS700RX" category="J" type-id="302OH" name-id="AT9AH-01" from="201301" to="201308">
<dtccode/>
<dtcname>L4 Position Switch Circuit</dtcname>
<subpara id="RM000003CS700RX_01" type-id="60" category="03" proc-id="RM22W0E___00008L600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The four wheel drive control ECU judges whether the transfer is in the L4 position based on the information from the limit switch inside the transfer shift actuator assembly which is detected by the ECM. The four wheel drive control ECU then inputs a signal to the combination meter assembly via CAN communication and turns on the 4LO indicator.</ptxt>
</content5>
</subpara>
<subpara id="RM000003CS700RX_03" type-id="32" category="03" proc-id="RM22W0E___00008L700000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2772 (See page <xref label="Seep01" href="RM000002IR003EX_06"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000003CS700RX_04" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003CS700RX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003CS700RX_05_0006" proc-id="RM22W0E___00008L800000">
<testtitle>CHECK HARNESS AND CONNECTOR (FOUR WHEEL DRIVE CONTROL ECU - BODY GROUND)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the four wheel drive control ECU connector.</ptxt>
<figure>
<graphic graphicname="C209926E19" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A27-21 (L4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Four Wheel Drive Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000003CS700RX_05_0004" fin="true">OK</down>
<right ref="RM000003CS700RX_05_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CS700RX_05_0007" proc-id="RM22W0E___00008L900000">
<testtitle>CHECK HARNESS AND CONNECTOR (FOUR WHEEL DRIVE CONTROL ECU - ECM)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="C219460E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the A27 four wheel drive control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A38-21 (L4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A52-21 (L4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6><res>
<down ref="RM000003CS700RX_05_0003" fin="true">OK</down>
<right ref="RM000003CS700RX_05_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CS700RX_05_0003">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003CS700RX_05_0004">
<testtitle>GO TO TRANSFER SYSTEM INSPECTION<xref label="Seep01" href="RM0000038UF00MX_01_0006"/>
</testtitle>
</testgrp>
<testgrp id="RM000003CS700RX_05_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>