<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C3ZV_T00SY" variety="T00SY">
<name>FRONT BUMPER (for Standard)</name>
<para id="RM0000038JX01UX" category="A" type-id="80001" name-id="ET8ED-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000038JX01UX_01" type-id="01" category="01">
<s-1 id="RM0000038JX01UX_01_0018" proc-id="RM22W0E___0000JIF00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0017" proc-id="RM22W0E___0000JIE00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL (for HID Headlight)</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0001" proc-id="RM22W0E___000011Z00002">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0002" proc-id="RM22W0E___000011Y00002">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0016" proc-id="RM22W0E___000049F00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 7 clips and radiator support seal.</ptxt>
<figure>
<graphic graphicname="B180890E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038JX01UX_01_0015" proc-id="RM22W0E___000049G00000">
<ptxt>REMOVE RADIATOR GRILLE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the radiator grille assembly.</ptxt>
<figure>
<graphic graphicname="B302174E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clips and 8 claws, and remove the radiator grille assembly.</ptxt>
</s2>
<s2>
<ptxt>w/ Wide View Front Monitor System:</ptxt>
<s3>
<ptxt>Disconnect the connector.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000038JX01UX_01_0005" proc-id="RM22W0E___00004SS00001">
<ptxt>REMOVE FRONT BUMPER COVER</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>For the front bumper cover with garnish, use the procedure described below.</ptxt>
</atten4>
<s2>
<ptxt>Put protective tape around the front bumper cover.</ptxt>
<figure>
<graphic graphicname="B302149E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a T30 "TORX" socket, remove the 6 screws.</ptxt>
<figure>
<graphic graphicname="B313279E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>LH Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>RH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 3 clips, 4 screws and 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 10 claws and remove the front bumper cover.</ptxt>
<figure>
<graphic graphicname="B302150" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System or w/ Fog Light:</ptxt>
<ptxt>Disconnect the No. 4 engine room wire connector and remove the front bumper cover.</ptxt>
</s2>
<s2>
<ptxt>w/ Headlight Cleaner System:</ptxt>
<figure>
<graphic graphicname="B181927" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the headlight cleaner hose and remove the front bumper cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0006" proc-id="RM22W0E___0000JI500001">
<ptxt>REMOVE FRONT BUMPER ENERGY ABSORBER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front bumper energy absorber.</ptxt>
<figure>
<graphic graphicname="B302151" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0007" proc-id="RM22W0E___0000JI600001">
<ptxt>REMOVE FRONT BUMPER REINFORCEMENT SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 6 nuts and front bumper reinforcement sub-assembly.</ptxt>
<figure>
<graphic graphicname="B302152" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0008" proc-id="RM22W0E___0000JI700001">
<ptxt>REMOVE FRONT NO. 2 BUMPER EXTENSION SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 nuts and front No. 2 bumper extension sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B302153" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0009" proc-id="RM22W0E___0000JI800001">
<ptxt>REMOVE FRONT NO. 2 BUMPER EXTENSION SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0010" proc-id="RM22W0E___0000JI900001">
<ptxt>REMOVE FRONT BUMPER SIDE SUPPORT LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws.</ptxt>
<figure>
<graphic graphicname="B313278" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 3 clips and remove the front bumper side support LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0011" proc-id="RM22W0E___0000JIA00001">
<ptxt>REMOVE FRONT BUMPER SIDE SUPPORT RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0012" proc-id="RM22W0E___0000JIB00001">
<ptxt>REMOVE HEADLIGHT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Halogen Headlight:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM000003B7A00BX"/>)</ptxt>
</s2>
<s2>
<ptxt>for HID Headlight:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM000003B7A00AX"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0013" proc-id="RM22W0E___0000JIC00001">
<ptxt>REMOVE HEADLIGHT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JX01UX_01_0014" proc-id="RM22W0E___0000JID00001">
<ptxt>REMOVE FRONT UPPER CENTER BUMPER RETAINER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and front upper center bumper retainer.</ptxt>
<figure>
<graphic graphicname="B302155" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>