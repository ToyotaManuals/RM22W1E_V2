<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM000000PN60IBX" category="J" type-id="300DE" name-id="NS8W5-01" from="201301" to="201308">
<dtccode/>
<dtcname>GPS Mark is not Displayed</dtcname>
<subpara id="RM000000PN60IBX_01" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000PN60IBX_02" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PN60IBX_02_0001" proc-id="RM22W0E___0000CHX00000">
<testtitle>CHECK CABIN</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the cabin for any object that might interrupt radio reception on the instrument panel. If such an object exists, remove it and check if the GPS mark reappears.</ptxt>
<atten4>
<ptxt>The GPS uses extremely weak radio waves originating from satellites. If the signal is interrupted by obstructions or other radio waves, the GPS may not be able to properly receive the signal.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The GPS mark appears.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PN60IBX_02_0002" fin="true">OK</down>
<right ref="RM000000PN60IBX_02_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PN60IBX_02_0002">
<testtitle>NORMAL OPERATION</testtitle>
</testgrp>
<testgrp id="RM000000PN60IBX_02_0003" proc-id="RM22W0E___0000CHY00000">
<testtitle>CHECK SURROUNDINGS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the vehicle is in a location where GPS signal reception is poor. If the vehicle is in such a place, relocate the vehicle and check if the GPS mark reappears.</ptxt>
<atten4>
<ptxt>The GPS uses 24 satellites in 6 orbits. At any point in time, 4 satellites should be able to pinpoint your vehicle. However, GPS signals may not reach the vehicle due to influence from the surroundings, vehicle direction and time. For examples, see the following illustration.</ptxt>
</atten4>
<figure>
<graphic graphicname="I100196E28" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Example</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>In a tunnel</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>In a building</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Under an overpass</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>On a forest or tree-lined path</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*f</ptxt>
</entry>
<entry valign="middle">
<ptxt>Between tall buildings</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*g</ptxt>
</entry>
<entry valign="middle">
<ptxt>Under a cliff or overhang</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The GPS mark is displayed.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PN60IBX_02_0004" fin="true">OK</down>
<right ref="RM000000PN60IBX_02_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PN60IBX_02_0004">
<testtitle>SYSTEM RETURNS TO NORMAL</testtitle>
</testgrp>
<testgrp id="RM000000PN60IBX_02_0005" proc-id="RM22W0E___0000CHZ00000">
<testtitle>CHECK GPS INFORMATION (OPERATION CHECK)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E225802" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Enter the "GPS Information" screen. Refer to Check GPS &amp; Vehicle Sensors in Operation Check (See page <xref label="Seep01" href="RM000003SKF0DCX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check how many of the following codes appear in the "STS" column.</ptxt>
<atten4>
<ptxt>T, U or P appears.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>At least 3 codes appear.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PN60IBX_02_0007" fin="true">OK</down>
<right ref="RM000000PN60IBX_02_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PN60IBX_02_0006">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000011BR0KYX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PN60IBX_02_0007">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01SX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>