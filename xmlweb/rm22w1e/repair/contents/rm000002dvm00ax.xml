<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002DVM00AX" category="J" type-id="30384" name-id="AC8ZT-04" from="201301" to="201308">
<dtccode/>
<dtcname>Rear Air Conditioning Relay Circuit</dtcname>
<subpara id="RM000002DVM00AX_01" type-id="60" category="03" proc-id="RM22W0E___0000H4R00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the No. 2 air conditioning control assembly (rear blower switch) is set to position LO or higher, the contact of the RR A/C relay is closed, current flows to the rear blower motor, and the rear blower motor operates. The rear blower motor speed can be changed by exchanging the ground and the rear blower resistor circuit with the No. 2 air conditioning control assembly (rear blower switch).</ptxt>
</content5>
</subpara>
<subpara id="RM000002DVM00AX_02" type-id="32" category="03" proc-id="RM22W0E___0000H4S00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E162050E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002DVM00AX_04" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002DVM00AX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002DVM00AX_03_0001" proc-id="RM22W0E___0000H4T00000">
<testtitle>INSPECT FUSE (A/C IG, RR A/C NO.2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the A/C IG and RR A/C NO. 2 fuses from the cowl side junction block LH.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A/C IG fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RR A/C NO. 2 fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0002" fin="false">OK</down>
<right ref="RM000002DVM00AX_03_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0002" proc-id="RM22W0E___0000H4U00000">
<testtitle>INSPECT RR A/C RELAY</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B106753E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Remove the RR A/C relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>3 - 4</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery voltage is not applied between terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery voltage is not applied between terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 - 4</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery voltage is applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery voltage is applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0003" fin="false">OK</down>
<right ref="RM000002DVM00AX_03_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0003" proc-id="RM22W0E___0000H4V00000">
<testtitle>INSPECT REAR BLOWER MOTOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E158869E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the z49 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the battery's positive (+) lead to terminal 2 and the negative (-) lead to terminal 1, then check that the rear blower motor operates smoothly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The rear blower motor operates smoothly.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Measure the current according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Current</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z49-1 (-) - z49-2 (+)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear blower motor operates</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 to 3 A</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0004" fin="false">OK</down>
<right ref="RM000002DVM00AX_03_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0004" proc-id="RM22W0E___0000H4W00000">
<testtitle>INSPECT NO. 2 AIR CONDITIONING CONTROL ASSEMBLY (REAR BLOWER SWITCH)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E162133E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Remove the No. 2 air conditioning control assembly (See page <xref label="Seep01" href="RM000004J7V005X"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL2" colwidth="1.35in"/>
<colspec colname="COLSPEC1" colwidth="1.35in"/>
<colspec colname="COL3" colwidth="1.43in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ALL - E89-6 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E89-5 (LO) - E89-6 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>LO</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E89-3 (M1) - E89-6 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>M1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E89-4 (M2) - E89-6 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>M2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E89-8 (HI) - E89-6 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>HI</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0005" fin="false">OK</down>
<right ref="RM000002DVM00AX_03_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0005" proc-id="RM22W0E___0000H4X00000">
<testtitle>INSPECT REAR BLOWER RESISTOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E158871E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Remove the rear blower resistor (See page <xref label="Seep01" href="RM000003AFE00OX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z52-1 (HI) - z52-2 (M1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.32 to 1.52 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z52-1 (HI) - z52-3 (M2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.39 to 0.45 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z52-1 (HI) - z52-4 (LO)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.24 to 2.4 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0022" fin="false">OK</down>
<right ref="RM000002DVM00AX_03_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0022" proc-id="RM22W0E___0000H5200000">
<testtitle>CHECK REAR COOLING UNIT EXPANSION VALVE (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the rear cooling unit expansion valve (See page <xref label="Seep01" href="RM000001JR302QX"/>).</ptxt>
<atten4>
<ptxt>Since the rear cooling unit expansion valve cannot be inspected while it is removed from the vehicle, replace the rear cooling unit expansion valve with a normal one and check that the condition returns to normal.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Same problem does not occur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0024" fin="true">OK</down>
<right ref="RM000002DVM00AX_03_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0006" proc-id="RM22W0E___0000H4Y00000">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR BLOWER RESISTOR - WIRING AIR CONDITIONING HARNESS)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E162134E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Disconnect the wiring air conditioning harness sub-assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the z52 rear blower resistor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>6 (M1) - z52-2 (M1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>5 (M2) - z52-3 (M2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>4 (HI) - z52-1 (HI)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>7 (LO) - z52-4 (LO)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0023" fin="false">OK</down>
<right ref="RM000002DVM00AX_03_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0023" proc-id="RM22W0E___0000H5300000">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR BLOWER SWITCH - WIRING AIR CONDITIONING HARNESS)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E158698E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Disconnect the E89 No. 2 air conditioning control assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the Wiring air conditioning harness sub-assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E89-3 (M1) - 6 (M1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E89-4 (M2) - 5 (M2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E89-8 (HI) - 4 (HI)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E89-5 (LO) - 7 (LO)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E89-6 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0007" fin="false">OK</down>
<right ref="RM000002DVM00AX_03_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0007" proc-id="RM22W0E___0000H4Z00000">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR BLOWER RESISTOR - REAR BLOWER MOTOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E158873E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the z52 rear blower resistor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the z49 rear motor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC3" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z52-1 (HI) - z49-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0008" fin="false">OK</down>
<right ref="RM000002DVM00AX_03_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0008" proc-id="RM22W0E___0000H5000000">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR BLOWER MOTOR - BODY GROUND AND BATTERY)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E158874E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the z49 rear motor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z49-2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z49-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Heater control (blower switch) HI</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0009" fin="false">OK</down>
<right ref="RM000002DVM00AX_03_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0009" proc-id="RM22W0E___0000H5100000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E158875E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E81air conditioning amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E81-18 (RCRL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Heater control (blower switch): OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-18 (RCRL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Heater control (blower switch): ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.39in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E81-14 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002DVM00AX_03_0011" fin="true">OK</down>
<right ref="RM000002DVM00AX_03_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0012">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0014">
<testtitle>REPLACE REAR BLOWER MOTOR<xref label="Seep01" href="RM0000039Q200YX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0013">
<testtitle>REPLACE NO. 2 AIR CONDITIONING CONTROL ASSEMBLY<xref label="Seep01" href="RM000004J7V005X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0016">
<testtitle>REPLACE REAR BLOWER RESISTOR<xref label="Seep01" href="RM000003AFE00OX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0017">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0011">
<testtitle>PROCEED TO NEXT CIRCUIT INSPECTION SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002LIQ022X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0021">
<testtitle>REPLACE RR A/C RELAY</testtitle>
</testgrp>
<testgrp id="RM000002DVM00AX_03_0024">
<testtitle>CHECK REAR COOLING UNIT EXPANSION VALVE<xref label="Seep01" href="RM000001JR302QX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>