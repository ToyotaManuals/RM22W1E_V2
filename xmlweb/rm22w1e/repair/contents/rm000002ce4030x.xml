<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZC_T00SF" variety="T00SF">
<name>LIGHTING SYSTEM</name>
<para id="RM000002CE4030X" category="D" type-id="303FL" name-id="LI8TT-11" from="201301">
<name>INITIALIZATION</name>
<subpara id="RM000002CE4030X_z0" proc-id="RM22W0E___0000J7W00000">
<content5 releasenbr="1">
<step1>
<ptxt>HEIGHT CONTROL SENSOR SIGNAL INITIALIZATION (except w/ Active Height Control Suspension)</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Static Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
</list1>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Initialize the height control sensor signal after the vehicle height changes due to replacement of the suspension or after performing such operations as removal and reinstallation or replacement of the rear height control sensor LH.</ptxt>
</item>
<item>
<ptxt>Initialization is also necessary when the headlight leveling ECU*1 or headlight swivel ECU*2 is replaced.</ptxt>
</item>
<item>
<ptxt>Adjust the headlight aim after initializing the headlight leveling ECU*1 or headlight swivel ECU*2 (See page <xref label="Seep02" href="RM0000011MI095X"/>).</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>When a malfunction is detected in the automatic headlight beam level control system, the height control sensor signal initialization is impossible. Perform troubleshooting before initialization.</ptxt>
</item>
</list1>
</atten3>
</step1>
<descript-diag>
<descript-testgroup>
<testtitle>PREPARE VEHICLE FOR INITIALIZATION</testtitle>
<test1>
<ptxt>Unload the trunk and vehicle, and make sure that the spare tire, tools and jack are in their original positions.</ptxt>
</test1>
<test1>
<ptxt>Check that there are no occupants in the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Turn off the headlights.</ptxt>
</test1>
<test1>
<ptxt>Stop the vehicle on a level surface and keep the vehicle height unchanged.</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK WARNING LIGHT</testtitle>
<test1>
<ptxt>Turn the ignition switch to ON and check the headlight beam level control system warning light.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>The headlight leveling ECU*1 or headlight swivel ECU*2 is replaced with a new one.</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Static Headlight Auto Leveling:</ptxt>
<ptxt>Warning light continuously blinks 6 times at 2 Hz.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ Dynamic Headlight Auto Leveling:</ptxt>
<ptxt>Warning light continuously blinks 2 times at 2 Hz.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Removal and reinstallation of the headlight leveling ECU*1 or headlight swivel ECU*2, replacement of the rear height control sensor LH, removal and reinstallation of the rear height control sensor LH, replacement of the suspension, etc.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Warning light comes on for approximately 3 seconds, and then turns off (bulb check function).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Static Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
</list1>
<atten4>
<ptxt>If the warning light does not come on when turning the ignition switch to ON, perform inspections and repairs according to "Problem Symptoms Table" (See page <xref label="Seep03" href="RM000002WKO02DX"/>).</ptxt>
</atten4>
</test1>
<results>
<result>NG</result>
<action-ci-right>GO TO PROBLEM SYMPTOMS TABLE (See page <xref label="Seep09" href="RM000002WKO02DX"/>)</action-ci-right>
<result-ci-down>OK</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INITIALIZATION</testtitle>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
<test1>
<ptxt>Connect terminals 4 (CG) and 8 (LVL) of the DLC3 using SST.</ptxt>
<figure>
<graphic graphicname="C166029E38" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Turn the low beam headlights on and off using the light control switch within 20 seconds after connecting the terminals.</ptxt>
<figure>
<graphic graphicname="E244505E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>The number of times the low beam headlights need to be turned on and off is determined by the present fuel level as shown below.</ptxt>
</atten4>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Fuel Level</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Number of Times to Turn Low Beam Headlights on and off</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Fuel level is within range A.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fuel level is within range B.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fuel level is within range C.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fuel level is within range D.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fuel level is within range E.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Turn the low beam headlights on and off at approximately 3-second intervals.</ptxt>
</item>
<item>
<ptxt>Be sure to operate the headlight dimmer switch from the outside of the vehicle.</ptxt>
</item>
</list1>
</atten3>
</test1>
<test1>
<ptxt>Check the warning light.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>The headlight leveling ECU*1 or headlight swivel ECU*2 is replaced with a new one.</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Static Headlight Auto Leveling:</ptxt>
<ptxt>Blinks 6 times at 2 Hz → Blinks N*3 times at 2 Hz</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ Dynamic Headlight Auto Leveling:</ptxt>
<ptxt>Blinks 2 times at 2 Hz → Blinks 3 times at 2 Hz, and then turns off.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Removal and reinstallation of the headlight leveling ECU*1 or headlight swivel ECU*2, replacement of the rear height control sensor LH, removal and reinstallation of the rear height control sensor LH, replacement of the suspension, etc.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off → Blinks N*3 times at 2 Hz</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Static Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
</list1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*3: The number of times that the indicator blinks is determined by the number of times the low beam headlights are turned on and off.</ptxt>
</item>
<item>
<ptxt>If the calculated vehicle height is not within the acceptable range, the rear height control sensor initialization will end as an abnormal termination, DTC B2452 is output and the warning indicator illuminates. Disconnect SST from the DLC3 and turn the ignition switch off. The restart from "Check Warning Light". (w/ Static Headlight Auto Leveling)</ptxt>
</item>
<item>
<ptxt>If the calculated vehicle height is not within the acceptable range, the rear height control sensor initialization will end as an abnormal termination, and the warning indicator continues to blink at 1 Hz. Disconnect SST from the DLC3 and turn the ignition switch off. Then restart from "Check Warning Light". (w/ Dynamic Headlight Auto Leveling)</ptxt>
</item>
<item>
<ptxt>If initialization cannot be finished normally, perform inspections and repairs according to "Problem Symptoms Table" (See page <xref label="Seep05" href="RM000002WKO02DX"/>).</ptxt>
</item>
</list1>
</atten4>
</test1>
<results>
<result>NG</result>
<action-ci-right>PERFORM TROUBLESHOOTING (See page <xref label="Seep11" href="RM000002WKL01CX"/>)</action-ci-right>
<result-ci-down>OK</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>