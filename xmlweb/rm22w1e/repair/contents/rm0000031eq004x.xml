<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000I" variety="S000I">
<name>1VD-FTV FUEL</name>
<ttl id="12008_S000I_7C3HB_T00AE" variety="T00AE">
<name>FUEL FILTER</name>
<para id="RM0000031EQ004X" category="A" type-id="30019" name-id="FU88N-01" from="201301">
<name>REPLACEMENT</name>
<subpara id="RM0000031EQ004X_02" type-id="11" category="10" proc-id="RM22W0E___00006AN00000">
<content3 releasenbr="1">
<atten4>
<ptxt>When replacing the filter, use TOYOTA genuine parts or equivalent parts.</ptxt>
</atten4>
</content3>
</subpara>
<subpara id="RM0000031EQ004X_01" type-id="01" category="01">
<s-1 id="RM0000031EQ004X_01_0001" proc-id="RM22W0E___00006AH00000">
<ptxt>DRAIN FUEL FROM FUEL FILTER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the fuel hose clamp.</ptxt>
<figure>
<graphic graphicname="A176401E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the clogging switch connector and level warning switch connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 nuts and lift up the fuel filter assembly.</ptxt>
<figure>
<graphic graphicname="A176368E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Connect a hose to the drain cock. Place the other end of the hose into a container under the drain cock.</ptxt>
</s2>
<s2>
<ptxt>Loosen the drain cock to drain fuel.</ptxt>
</s2>
<s2>
<ptxt>Tighten the drain cock by hand.</ptxt>
<atten3>
<ptxt>Do not use any tools in this procedure.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the fuel filter assembly with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>199</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A176401E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the clogging switch connector and level warning switch connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the fuel hose clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031EQ004X_01_0002" proc-id="RM22W0E___00006AI00000">
<ptxt>REMOVE FUEL FILTER ELEMENT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the level warning switch connector, and detach the level warning switch connector clamp.</ptxt>
<figure>
<graphic graphicname="A175108E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, remove the 3 bolts and disconnect the fuel filter cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the fuel filter case and element assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the O-ring and retainer.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the fuel filter element from the case.</ptxt>
<figure>
<graphic graphicname="A163549" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<ptxt>Do not damage the fuel filter element and case.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031EQ004X_01_0003" proc-id="RM22W0E___00006AJ00000">
<ptxt>INSTALL FUEL FILTER ELEMENT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the fuel filter case.</ptxt>
<atten3>
<ptxt>When replacing the fuel filter, clean the fuel filter case and remove dirt completely.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the fuel filter element to the case.</ptxt>
<figure>
<graphic graphicname="A168456E02" width="2.775699831in" height="5.787629434in"/>
</figure>
<atten3>
<ptxt>Make sure that there is no foreign matter on the areas of the case shown in the illustration.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the retainer.</ptxt>
</s2>
<s2>
<ptxt>Install a new O-ring.</ptxt>
<atten3>
<ptxt>Make sure the rounded side of the O-ring faces upward as shown in the illustration.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the fuel filter case and element assembly to the bracket.</ptxt>
</s2>
<s2>
<ptxt>Set the fuel filter cap on the case.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that there is no foreign matter on the areas of the cap shown in the illustration.</ptxt>
</item>
<item>
<ptxt>Do not turn the cap as the O-ring will be damaged.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, install the fuel filter cap with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>4.5</t-value1>
<t-value2>46</t-value2>
<t-value3>40</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A175108E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Further tighten the first bolt tightened above so that its torque value is slightly more than the torque specification.</ptxt>
</s2>
<s2>
<ptxt>Attach the level warning switch connector clamp and connect the level warning switch connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031EQ004X_01_0006" proc-id="RM22W0E___00006AM00000">
<ptxt>RESET FUEL SYSTEM WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the clogging switch connector.</ptxt>
<figure>
<graphic graphicname="A181376" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</s2>
<s2>
<ptxt>After turning the ignition switch to ON, connect the clogging switch connector within 3 to 60 seconds.</ptxt>
</s2>
<s2>
<ptxt>w/o Multi-information Display:</ptxt>
<ptxt>Check that the fuel system warning light on the combination meter turns off.</ptxt>
<figure>
<graphic graphicname="A181391" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Multi-information Display:</ptxt>
<ptxt>Check that the fuel system warning on the multi-information display turns off.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031EQ004X_01_0004" proc-id="RM22W0E___00006AK00000">
<ptxt>BLEED AIR FROM FUEL SYSTEM</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A175110" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using the hand pump, bleed air from the fuel system until pumping becomes difficult.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031EQ004X_01_0005" proc-id="RM22W0E___00006AL00000">
<ptxt>INSPECT FOR FUEL LEAK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start the engine. Check that leaking sounds, sucking sounds or other unusual sounds cannot be heard.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>