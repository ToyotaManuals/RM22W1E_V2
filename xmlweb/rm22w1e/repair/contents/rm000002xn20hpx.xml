<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000002XN20HPX" category="J" type-id="3009O" name-id="ES16DM-002" from="201308">
<dtccode/>
<dtcname>Starter Signal Circuit</dtcname>
<subpara id="RM000002XN20HPX_03" type-id="60" category="03" proc-id="RM22W0E___000018300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>While the engine is being cranked, current flows from terminal STAR of the main body ECU to the park/neutral position switch and also flows to terminal STA of the ECM (STA signal).</ptxt>
</content5>
</subpara>
<subpara id="RM000002XN20HPX_04" type-id="32" category="03" proc-id="RM22W0E___000018400001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0617 (See page <xref label="Seep01" href="RM000000TA01E9X_05"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000002XN20HPX_01" type-id="51" category="05" proc-id="RM22W0E___000018200001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>This chart is based on the premise that the engine can crank normally. If the engine cannot crank normally, proceed to Problem Symptoms Table (See page <xref label="Seep01" href="RM000000PDG0VTX"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002XN20HPX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002XN20HPX_05_0035" proc-id="RM22W0E___000018500001">
<testtitle>READ VALUE USING GTS (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Engine Switch Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>On (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>START</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002XN20HPX_05_0056" fin="true">OK</down>
<right ref="RM000002XN20HPX_05_0036" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0036" proc-id="RM22W0E___000018600001">
<testtitle>CHECK STARTER RELAY (ST) (POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the starter relay (ST) from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.37in"/>
<colspec colname="COL3" colwidth="2.34in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine cranking</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1* - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine cranking</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>*: URJ202L-GNTEKC</ptxt>
<atten4>
<ptxt>The engine does not crank because the relay is not installed.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002XN20HPX_05_0037" fin="false">OK</down>
<right ref="RM000002XN20HPX_05_0042" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0037" proc-id="RM22W0E___000018700001">
<testtitle>INSPECT STARTER RELAY (ST)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the starter relay (ST) (See page <xref label="Seep01" href="RM000003BLB02WX_01_0007"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002XN20HPX_05_0038" fin="false">OK</down>
<right ref="RM000002XN20HPX_05_0046" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0038" proc-id="RM22W0E___000018800001">
<testtitle>CHECK HARNESS AND CONNECTOR (STARTER RELAY (ST) - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the starter relay (ST) from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 2* - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>*: URJ202L-GNTEKC</ptxt>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002XN20HPX_05_0018" fin="true">OK</down>
<right ref="RM000002XN20HPX_05_0048" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0042" proc-id="RM22W0E___000018900001">
<testtitle>CHECK HARNESS AND CONNECTOR (STARTER RELAY (ST) - PARK/NEUTRAL POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the starter relay (ST) from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1* - C24-5 (L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 2 - C24-5 (L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1* or C24-5 (L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 2 or C24-5 (L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>*: URJ202L-GNTEKC</ptxt>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002XN20HPX_05_0043" fin="false">OK</down>
<right ref="RM000002XN20HPX_05_0052" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0043" proc-id="RM22W0E___000018A00001">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the park/neutral position switch assembly (See page <xref label="Seep01" href="RM000002BKX03MX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002XN20HPX_05_0044" fin="false">OK</down>
<right ref="RM000002XN20HPX_05_0053" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0044" proc-id="RM22W0E___000018B00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - MAIN BODY ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C24-4 (B) - E1-8 (STR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C24-4 (B) or E1-8 (STR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the main body ECU connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002XN20HPX_05_0058" fin="true">OK</down>
<right ref="RM000002XN20HPX_05_0054" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0046">
<testtitle>REPLACE STARTER RELAY (ST)</testtitle>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0048">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (STARTER RELAY (ST) - BODY GROUND)</testtitle>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0052">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0053">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC076X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0054">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0056">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000PDG0VTX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0018">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (ECM - PARK/NEUTRAL POSITION SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000002XN20HPX_05_0058">
<testtitle>GO TO ENTRY AND START SYSTEM<xref label="Seep01" href="RM000000YEF0JHX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>