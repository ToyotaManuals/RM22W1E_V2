<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SN_T00LQ" variety="T00LQ">
<name>POWER DOOR LOCK CONTROL SYSTEM</name>
<para id="RM000000W1Z051X" category="J" type-id="3050A" name-id="DL8CG-01" from="201301" to="201308">
<dtccode/>
<dtcname>All Doors LOCK/UNLOCK Functions do not Operate Via Door Control Switch</dtcname>
<subpara id="RM000000W1Z051X_01" type-id="60" category="03" proc-id="RM22W0E___0000E8300000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The main body ECU receives switch signals from the door control switch and activates the door lock motor on each door according to these signals.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W1Z051X_02" type-id="32" category="03" proc-id="RM22W0E___0000E8400000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B194715E05" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W1Z051X_03" type-id="51" category="05" proc-id="RM22W0E___0000E8500000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000000W1Z051X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W1Z051X_04_0012" proc-id="RM22W0E___0000E8800000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR LOCK AND UNLOCK SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the door lock and unlock switches are functioning properly. </ptxt>
<table pgwide="1">
<title>Main Body ECU</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Door Lock SW-Lock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door manual lock switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Front passenger side door control switch on (lock)</ptxt>
<ptxt>OFF: Front passenger side door control switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Unlock SW-Unlock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door manual unlock switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Front passenger side door control switch on (unlock)</ptxt>
<ptxt>OFF: Front passenger side door control switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, each item changes between ON and OFF according to above chart.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1Z051X_04_0014" fin="false">OK</down>
<right ref="RM000000W1Z051X_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1Z051X_04_0014" proc-id="RM22W0E___0000E8900000">
<testtitle>INSPECT FUSE (DOOR NO. 1, DOOR NO. 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the DOOR NO. 1 fuse from the main body ECU (cowl side junction block LH).</ptxt>
</test1>
<test1>
<ptxt>Remove the DOOR NO. 2 fuse from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.32in"/>
<colspec colname="COL2" colwidth="1.43in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DOOR NO. 1 fuse</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DOOR NO. 2 fuse</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1Z051X_04_0016" fin="false">OK</down>
<right ref="RM000000W1Z051X_04_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1Z051X_04_0016" proc-id="RM22W0E___0000E8A00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B190694E05" width="7.106578999in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the 2A, 2B, and 2D ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2A-1 (ALTB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2B-20 (BATB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2D-62 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1Z051X_04_0009" fin="true">OK</down>
<right ref="RM000000W1Z051X_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1Z051X_04_0003" proc-id="RM22W0E___0000E8600000">
<testtitle>INSPECT DOOR CONTROL SWITCH ASSEMBLY (DOOR LOCK AND UNLOCK SWITCH)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B180461E07" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Remove the door control switch (See page <xref label="Seep01" href="RM000002STU084X"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2 (UL) - 3 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unlock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (UL) - 3 (E)</ptxt>
<ptxt>4 (L) - 3 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>4 (L) - 3 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1Z051X_04_0004" fin="false">OK</down>
<right ref="RM000000W1Z051X_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1Z051X_04_0004" proc-id="RM22W0E___0000E8700000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - DOOR CONTROL SWITCH AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B315384E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the 2D ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the I33 switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2D-52 (UL1) - I33-2 (UL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-49 (L1) - I33-4 (L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I33-3 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I33-2 (UL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I33-4 (L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1Z051X_04_0009" fin="true">OK</down>
<right ref="RM000000W1Z051X_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1Z051X_04_0009">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000000W1Z051X_04_0008">
<testtitle>REPLACE DOOR CONTROL SWITCH ASSEMBLY<xref label="Seep01" href="RM000002STU084X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W1Z051X_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W1Z051X_04_0015">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>