<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3XD_T00QG" variety="T00QG">
<name>WINDOW DEFOGGER SYSTEM (for Double Swing Out Type)</name>
<para id="RM000004X6K001X" category="U" type-id="3001G" name-id="WS6DZ-01" from="201301">
<name>TERMINALS OF ECU</name>
<subpara id="RM000004X6K001X_z0" proc-id="RM22W0E___0000I5300000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK AIR CONDITIONING AMPLIFIER ASSEMBLY</ptxt>
<figure>
<graphic graphicname="B160781E03" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the E81 air conditioning amplifier assembly connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E81-1 (IG+) - E81-14 (GND)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Ignition power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E81-21 (+B1) - E81-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery power source</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E81-14 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Reconnect the E81 air conditioning amplifier assembly connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E81-38 (RDEF) - E81-14 (GND)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>G - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="left">
<ptxt>Defogger relay</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch ON and defogger switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Ignition switch ON and defogger switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, the air conditioning amplifier assembly or fuse may have a malfunction.</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>CHECK AIR CONDITIONING CONTROL ASSEMBLY</ptxt>
<figure>
<graphic graphicname="E139372E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the F10 air conditioning control assembly connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>F10-7 (IG+) - F10-1 (GND)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Ignition power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F10-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</item>
</list1>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>