<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SE_T00LH" variety="T00LH">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000002S8J03BX" category="C" type-id="801IC" name-id="NW2BB-03" from="201301" to="201308">
<dtccode>B2321</dtccode>
<dtcname>Driver Side Door ECU Communication Stop</dtcname>
<subpara id="RM000002S8J03BX_01" type-id="60" category="03" proc-id="RM22W0E___0000E2W00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when LIN communication between the front power window regulator motor LH*1 (RH*2) and main body ECU (cowl side junction block LH) stops for 10 seconds or more.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.61in"/>
<colspec colname="COL2" colwidth="3.11in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2321</ptxt>
</entry>
<entry valign="middle">
<ptxt>No communication between the front power window regulator motor LH*1 or RH*2 and main body ECU (cowl side junction block LH) for 10 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Front power window regulator motor LH*1</ptxt>
</item>
<item>
<ptxt>Front power window regulator motor RH*2</ptxt>
</item>
<item>
<ptxt>Main body ECU (cowl side junction block LH)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002S8J03BX_02" type-id="32" category="03" proc-id="RM22W0E___0000E2X00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B173719E22" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002S8J03BX_03" type-id="51" category="05" proc-id="RM22W0E___0000E2Y00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot:</ptxt>
<ptxt>Connect the intelligent tester to the vehicle, and turn a courtesy switch on and off at 1.5 second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</atten3>
<atten4>
<ptxt>DTC B2325 is stored when the communication between the front power window regulator motor and main body ECU (cowl side junction block LH) stops.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002S8J03BX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002S8J03BX_04_0001" proc-id="RM22W0E___0000E2Z00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8J03BX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8J03BX_04_0002" proc-id="RM22W0E___0000E3000000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2321 is output </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2321 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8J03BX_04_0003" fin="false">A</down>
<right ref="RM000002S8J03BX_04_0007" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8J03BX_04_0003" proc-id="RM22W0E___0000E3100000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - FRONT POWER WINDOW REGULATOR MOTOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E156403E05" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the E3 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the I12*1 or I4*2 motor connector.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E3-10 (LIN2) - I12-9 (LIN)*1 or I4-9 (LIN)*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8J03BX_04_0004" fin="false">OK</down>
<right ref="RM000002S8J03BX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8J03BX_04_0004" proc-id="RM22W0E___0000E3200000">
<testtitle>CHECK FRONT POWER WINDOW REGULATOR MOTOR ASSEMBLY (BATTERY VOLTAGE AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I12*1 or I4*2 motor connector.</ptxt>
<figure>
<graphic graphicname="B163167E17" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I12-1 (GND)*1 or I4-1 (GND)*2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I12-2 (B)*1 or I4-2 (B)*2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8J03BX_04_0005" fin="false">OK</down>
<right ref="RM000002S8J03BX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8J03BX_04_0005" proc-id="RM22W0E___0000E3300000">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the front power window regulator motor LH*1 or RH*2 with a new or normally functioning one (See page <xref label="Seep01" href="RM000002STX03BX"/>).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep02" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8J03BX_04_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8J03BX_04_0006" proc-id="RM22W0E___0000E3400000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2321 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2321 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8J03BX_04_0011" fin="true">A</down>
<right ref="RM000002S8J03BX_04_0010" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8J03BX_04_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000UZ30DCX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8J03BX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8J03BX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8J03BX_04_0010">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000002S8J03BX_04_0011">
<testtitle>END (FRONT POWER WINDOW REGULATOR MOTOR IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>