<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000187R07MX" category="C" type-id="302I8" name-id="ESRAS-03" from="201301" to="201308">
<dtccode>P0606</dtccode>
<dtcname>ECM / PCM Processor</dtcname>
<dtccode>P0607</dtccode>
<dtcname>Control Module Performance</dtcname>
<dtccode>P1611</dtccode>
<dtcname>IC Circuit Malfunction</dtcname>
<subpara id="RM00000187R07MX_01" type-id="60" category="03" proc-id="RM22W0E___00002MT00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<title>P0606, P0607, P1611</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>ECM internal error continues for 1 second (1 trip detection logic).</ptxt>
</entry>
<entry>
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000187R07MX_02" type-id="51" category="05" proc-id="RM22W0E___00002MU00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07YX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06NX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187R07MX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187R07MX_03_0003" proc-id="RM22W0E___00002MW00000">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0606, P0607 AND/OR P1611)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3. </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC. </ptxt>
</test1>
<test1>
<ptxt>Read the DTCs. </ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0606, P0607 or P1611 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0606, P0607 or P1611 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000187R07MX_03_0002" fin="false">A</down>
<right ref="RM00000187R07MX_03_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000187R07MX_03_0002" proc-id="RM22W0E___00002MV00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187R07MX_03_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187R07MX_03_0005" proc-id="RM22W0E___00002MX00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187R07MX_03_0006" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187R07MX_03_0004">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW05BX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000187R07MX_03_0006">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>