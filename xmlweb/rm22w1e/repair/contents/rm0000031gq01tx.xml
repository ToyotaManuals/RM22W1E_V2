<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000R" variety="S000R">
<name>3UR-FE INTAKE / EXHAUST</name>
<ttl id="12010_S000R_7C3J8_T00CB" variety="T00CB">
<name>INTAKE MANIFOLD</name>
<para id="RM0000031GQ01TX" category="A" type-id="80001" name-id="IE28O-03" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM0000031GQ01TX_01" type-id="01" category="01">
<s-1 id="RM0000031GQ01TX_01_0039" proc-id="RM22W0E___00006P200000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0037" proc-id="RM22W0E___00006P100000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0038" proc-id="RM22W0E___0000K7500000">
<ptxt>REMOVE COWL TOP VENTILATOR LOUVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Remove the cowl top ventilator louver sub-assembly (See page <xref label="Seep01" href="RM000002M66014X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0035" proc-id="RM22W0E___000011Z00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01TX_01_0036" proc-id="RM22W0E___000011Y00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01TX_01_0031" proc-id="RM22W0E___00001X600000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A238241" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 10 bolts and No. 1 engine under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01TX_01_0021" proc-id="RM22W0E___00001X300000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the radiator cap. Then drain the coolant from the radiator.</ptxt>
</s2>
<s2>
<ptxt>Loosen the 2 cylinder block drain cock plugs. Then drain the coolant from the engine.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 2 cylinder block drain cock plugs.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A174123E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01TX_01_0018" proc-id="RM22W0E___00001X400000">
<ptxt>REMOVE V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A174196E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Raise the front of the V-bank cover to detach the 3 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01TX_01_0014" proc-id="RM22W0E___00001X500000">
<ptxt>REMOVE AIR CLEANER HOSE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A243375" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the vacuum hose and No. 2 ventilation hose.</ptxt>
</s2>
<s2>
<ptxt>Loosen the 2 hose clamps.</ptxt>
</s2>
<s2>
<ptxt>Remove the air cleaner hose.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0006" proc-id="RM22W0E___00004TX00000">
<ptxt>REMOVE INTAKE MANIFOLD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the ventilation hose from the ventilation pipe of the cylinder head cover LH and RH.</ptxt>
<figure>
<graphic graphicname="A162091E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the 2 water by-pass hoses.</ptxt>
<figure>
<graphic graphicname="A162087" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the throttle body connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 1 ventilation hose.</ptxt>
<figure>
<graphic graphicname="A184056" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the purge VSV connector.</ptxt>
<figure>
<graphic graphicname="A162082" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the purge line hose from the purge VSV.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the vacuum switching valve connector (for ACIS).</ptxt>
</s2>
<s2>
<ptxt>Remove the No. 1 engine cover sub-assembly.</ptxt>
<figure>
<graphic graphicname="A162089" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 3 engine cover.</ptxt>
<figure>
<graphic graphicname="A238871" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the 3 wire clamps from the 3 wire brackets.</ptxt>
<figure>
<graphic graphicname="A238834" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and wire bracket from the intake manifold.</ptxt>
<figure>
<graphic graphicname="A238832" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts, 8 bolts, intake manifold and 2 gaskets.</ptxt>
<figure>
<graphic graphicname="A184053" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0017" proc-id="RM22W0E___00006OY00000">
<ptxt>REMOVE VENTILATION HOSE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and ventilation hose from the intake manifold.</ptxt>
<figure>
<graphic graphicname="A163820E02" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0010" proc-id="RM22W0E___00006OV00000">
<ptxt>REMOVE NO. 1 V-BANK COVER BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and bracket.</ptxt>
<figure>
<graphic graphicname="A177622" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0011" proc-id="RM22W0E___00006OW00000">
<ptxt>REMOVE V-BANK COVER BOLT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the cover bolt from the intake manifold.</ptxt>
<figure>
<graphic graphicname="A177623" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0030" proc-id="RM22W0E___00006P000000">
<ptxt>REMOVE NO. 2 V-BANK COVER BRACKET SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A177624" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and bracket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0007" proc-id="RM22W0E___00006OT00000">
<ptxt>REMOVE THROTTLE BODY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts, throttle body and gasket.</ptxt>
<figure>
<graphic graphicname="A162080E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0016" proc-id="RM22W0E___00006OX00000">
<ptxt>REMOVE VACUUM SWITCHING VALVE ASSEMBLY (for ACIS)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 vacuum hoses from the vacuum switching valve.</ptxt>
<figure>
<graphic graphicname="A184054" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and vacuum switching valve.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0008" proc-id="RM22W0E___00006OU00000">
<ptxt>REMOVE PURGE VSV</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the purge line hose from the intake manifold.</ptxt>
<figure>
<graphic graphicname="A168583" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and purge VSV.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01TX_01_0024" proc-id="RM22W0E___00006OZ00000">
<ptxt>REMOVE WIRE HARNESS CLAMP BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A177625" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and 2 wire harness clamp brackets.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>