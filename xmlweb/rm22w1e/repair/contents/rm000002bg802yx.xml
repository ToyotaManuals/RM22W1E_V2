<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000W" variety="S000W">
<name>3UR-FE COOLING</name>
<ttl id="12011_S000W_7C3K2_T00D5" variety="T00D5">
<name>THERMOSTAT</name>
<para id="RM000002BG802YX" category="A" type-id="80001" name-id="CO6D5-01" from="201301">
<name>REMOVAL</name>
<subpara id="RM000002BG802YX_01" type-id="11" category="10" proc-id="RM22W0E___00006W300000">
<content3 releasenbr="1">
<atten4>
<ptxt>If the thermostat is not installed, cooling efficiency decreases. Even if the engine tends to overheat, do not remove the thermostat.</ptxt>
</atten4>
</content3>
</subpara>
<subpara id="RM000002BG802YX_02" type-id="01" category="01">
<s-1 id="RM000002BG802YX_02_0018" proc-id="RM22W0E___000011Z00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802YX_02_0019" proc-id="RM22W0E___000011Y00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802YX_02_0013" proc-id="RM22W0E___00001X600000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A238241" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 10 bolts and No. 1 engine under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802YX_02_0005" proc-id="RM22W0E___00001X300000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the radiator cap. Then drain the coolant from the radiator.</ptxt>
</s2>
<s2>
<ptxt>Loosen the 2 cylinder block drain cock plugs. Then drain the coolant from the engine.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 2 cylinder block drain cock plugs.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A174123E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802YX_02_0006" proc-id="RM22W0E___00001X400000">
<ptxt>REMOVE V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A174196E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Raise the front of the V-bank cover to detach the 3 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802YX_02_0014" proc-id="RM22W0E___00001X500000">
<ptxt>REMOVE AIR CLEANER HOSE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A243375" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the vacuum hose and No. 2 ventilation hose.</ptxt>
</s2>
<s2>
<ptxt>Loosen the 2 hose clamps.</ptxt>
</s2>
<s2>
<ptxt>Remove the air cleaner hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802YX_02_0015" proc-id="RM22W0E___00001ZK00000">
<ptxt>REMOVE AIR CLEANER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A174195" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 bolts and air cleaner.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802YX_02_0016">
<ptxt>REMOVE NO. 1 RADIATOR HOSE</ptxt>
</s-1>
<s-1 id="RM000002BG802YX_02_0003">
<ptxt>REMOVE NO. 2 RADIATOR HOSE</ptxt>
</s-1>
<s-1 id="RM000002BG802YX_02_0017" proc-id="RM22W0E___00004TP00000">
<ptxt>REMOVE FAN SHROUD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 4 nuts holding the fluid coupling fan.</ptxt>
<figure>
<graphic graphicname="A151842" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the fan and generator V-belt (See page <xref label="Seep01" href="RM000002BOI024X_01_0001"/>).</ptxt>
</s2>
<s2>
<ptxt>Disconnect the reservoir hose from the upper radiator tank.</ptxt>
<figure>
<graphic graphicname="A238869" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw to open the flexible hose clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the oil cooler tube from the fan shroud.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts holding the fan shroud.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 nuts of the fluid coupling fan, and then remove the shroud together with the coupling fan.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the fan pulley.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802YX_02_0004" proc-id="RM22W0E___00006W400000">
<ptxt>REMOVE WATER INLET SUB-ASSEMBLY WITH THERMOSTAT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 5 water by-pass hose.</ptxt>
<figure>
<graphic graphicname="A238833E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 5 Water By-pass Hose</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 3 nuts, water inlet with thermostat and gasket.</ptxt>
<figure>
<graphic graphicname="A151841" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>