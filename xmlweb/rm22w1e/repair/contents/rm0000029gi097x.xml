<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12065_S002H" variety="S002H">
<name>SLIDING ROOF / CONVERTIBLE</name>
<ttl id="12065_S002H_7C3XI_T00QL" variety="T00QL">
<name>SLIDING ROOF SYSTEM</name>
<para id="RM0000029GI097X" category="C" type-id="8020Z" name-id="RF1HL-02" from="201308">
<dtccode>B2343</dtccode>
<dtcname>Position Initialization Incomplete</dtcname>
<subpara id="RM0000029GI097X_01" type-id="60" category="03" proc-id="RM22W0E___0000I9E00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is output when the sliding roof drive gear (sliding roof ECU) has not been initialized.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2343</ptxt>
</entry>
<entry valign="middle">
<ptxt>The sliding roof drive gear (sliding roof ECU) has not been initialized</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Sliding roof drive gear sub-assembly (Sliding roof ECU)</ptxt>
</item>
<item>
<ptxt>Map light assembly (Sliding roof switch)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000029GI097X_02" type-id="32" category="03" proc-id="RM22W0E___0000I9F00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B181150E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000029GI097X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000029GI097X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000029GI097X_04_0001" proc-id="RM22W0E___0000I9G00001">
<testtitle>INITIALIZE SLIDING ROOF DRIVE GEAR SUB-ASSEMBLY (SLIDING ROOF ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the sliding roof drive gear can be initialized (See page <xref label="Seep01" href="RM000001VJR06WX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Sliding roof drive gear can be initialized.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000029GI097X_04_0002" fin="false">OK</down>
<right ref="RM0000029GI097X_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000029GI097X_04_0002" proc-id="RM22W0E___0000I9H00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001YIS03EX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the DTCs (See page <xref label="Seep02" href="RM000001YIS03EX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2343 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000029GI097X_04_0006" fin="true">A</down>
<right ref="RM0000029GI097X_04_0005" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000029GI097X_04_0003" proc-id="RM22W0E___0000I9I00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SLIDING ROOF DRIVE GEAR - MAP LIGHT)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B181149E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Disconnect the R5 drive gear connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the R7 map light connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R5-10 (UP) - R7-19 (UP)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R5-8 (DWN) - R7-18 (DOWN)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R5-7 (OPN) - R7-8 (OPN)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R5-9 (CLS) - R7-7 (CLS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R7-17 (GND9) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R5-2 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R5-10 (UP) or R7-19 (UP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R5-8 (DWN) or R7-18 (DOWN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R5-7 (OPN) or R7-8 (OPN) -  Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R5-9 (CLS) or R7-7 (CLS) -  Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000029GI097X_04_0004" fin="false">OK</down>
<right ref="RM0000029GI097X_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000029GI097X_04_0004" proc-id="RM22W0E___0000I9J00001">
<testtitle>INSPECT MAP LIGHT ASSEMBLY (SLIDING ROOF SWITCH)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B237546E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Remove the map light (See page <xref label="Seep01" href="RM0000038VT00GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>19 (UP) - 17 (GND9)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>UP switch is pressed </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>19 (UP) - 17 (GND9)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>UP switch is not pressed </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>18 (DOWN) - 17 (GND9)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DOWN switch is pressed </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>18 (DOWN) - 17 (GND9)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DOWN switch is not pressed </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>8 (OPN) - 17 (GND9)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OPEN switch is pressed </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>8 (OPN) - 17 (GND9)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OPEN switch is not pressed </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7 (CLS) - 17 (GND9)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>CLOSE switch is pressed </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7 (CLS) - 17 (GND9)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>CLOSE switch is not pressed </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000029GI097X_04_0005" fin="true">OK</down>
<right ref="RM0000029GI097X_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000029GI097X_04_0005">
<testtitle>REPLACE SLIDING ROOF DRIVE GEAR SUB-ASSEMBLY (SLIDING ROOF ECU)<xref label="Seep01" href="RM000002LPC029X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000029GI097X_04_0006">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM0000029GI097X_04_0007">
<testtitle>REPLACE MAP LIGHT ASSEMBLY (SLIDING ROOF SWITCH)<xref label="Seep01" href="RM0000038VT00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000029GI097X_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>