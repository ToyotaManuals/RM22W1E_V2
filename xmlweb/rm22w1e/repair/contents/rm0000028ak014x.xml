<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CK_T005N" variety="T005N">
<name>CAMSHAFT POSITION SENSOR</name>
<para id="RM0000028AK014X" category="A" type-id="30014" name-id="ESL4I-08" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000028AK014X_01" type-id="01" category="01">
<s-1 id="RM0000028AK014X_01_0001" proc-id="RM22W0E___00000Z000000">
<ptxt>INSTALL VVT SENSOR (for Exhaust Side of Bank 2)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to the O-ring of the VVT sensor.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When reusing the VVT sensor, inspect the O-ring.</ptxt>
</item>
<item>
<ptxt>If the O-ring has scratches or cuts, replace the VVT sensor.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the VVT sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the VVT sensor connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000028AK014X_01_0017" proc-id="RM22W0E___00000Z300000">
<ptxt>INSTALL VVT SENSOR (for Intake Side of Bank 2)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to the O-ring of the VVT sensor.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When reusing the VVT sensor, inspect the O-ring.</ptxt>
</item>
<item>
<ptxt>If the O-ring has scratches or cuts, replace the VVT sensor.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the VVT sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the VVT sensor connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000028AK014X_01_0022" proc-id="RM22W0E___00000Z400000">
<ptxt>INSTALL VVT SENSOR (for Exhaust Side of Bank 1)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to the O-ring of the VVT sensor.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When reusing the VVT sensor, inspect the O-ring.</ptxt>
</item>
<item>
<ptxt>If the O-ring has scratches or cuts, replace the VVT sensor.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the VVT sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the VVT sensor connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000028AK014X_01_0023" proc-id="RM22W0E___00000Z500000">
<ptxt>INSTALL VVT SENSOR (for Intake Side of Bank 1)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to the O-ring of the VVT sensor.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When reusing the VVT sensor, inspect the O-ring.</ptxt>
</item>
<item>
<ptxt>If the O-ring has scratches or cuts, replace the VVT sensor.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the VVT sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the VVT sensor connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000028AK014X_01_0011" proc-id="RM22W0E___00000Z100000">
<ptxt>INSTALL NO. 1 AIR CLEANER HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 air cleaner hose with the 2 hose clamps.</ptxt>
<figure>
<graphic graphicname="A267635E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<torqueitem>
<t-value1>2.5</t-value1>
<t-value2>25</t-value2>
<t-value3>22</t-value3>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Groove</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Top</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the vacuum hose and No. 2 PCV hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000028AK014X_01_0012" proc-id="RM22W0E___00000Z200000">
<ptxt>INSTALL V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 2 V-bank cover grommets with the 2 pins and press down on the V-bank cover to attach the pins.</ptxt>
<figure>
<graphic graphicname="A271365E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>