<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000001DWV03MX" category="D" type-id="303F3" name-id="BC869-20" from="201308">
<name>DIAGNOSIS SYSTEM</name>
<subpara id="RM000001DWV03MX_z0" proc-id="RM22W0E___0000AD000001">
<content5 releasenbr="1">
<step1>
<ptxt>DIAGNOSIS</ptxt>
<step2>
<ptxt>If the skid control ECU (master cylinder solenoid) detects a malfunction, the ABS warning light and/or brake warning light and the slip indicator light come on in accordance with the trouble area to warn the driver.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The DTCs are simultaneously stored in memory. The DTCs can be read by connecting SST between terminals 13 (TC) and 4 (CG) of the DLC3 and observing the blinking pattern of the ABS warning light and slip indicator light, or by connecting the GTS.</ptxt>
</item>
<item>
<ptxt>This system has a Test Mode sensor signal check function. The test mode DTCs can be read by connecting the GTS and observing the blinking pattern of the ABS warning light and slip indicator light.</ptxt>
</item>
</list1>
</atten4>
<atten3>
<ptxt>If parts connected to the IG terminal circuit are damaged due to high voltage, DTCs may not be able to be output from the ABS warning light and slip indicator light by connecting SST to the DLC3. In this case, inspect the IG power source circuit.</ptxt>
</atten3>
</step2>
</step1>
<step1>
<ptxt>WARNING LIGHT AND INDICATOR LIGHT CHECK</ptxt>
<step2>
<ptxt>Release the parking brake.</ptxt>
<atten2>
<ptxt>When releasing the parking brake, set chocks to hold the vehicle for safety.</ptxt>
</atten2>
<atten4>
<ptxt>When the parking brake is applied or the level of the brake fluid is low, the brake warning light comes on.</ptxt>
</atten4>
</step2>
<step2>
<ptxt>Check that the ABS warning light, brake warning light and slip indicator light come on when the ignition switch is turned to ON, and go off in approximately 3 seconds.</ptxt>
<atten4>
<ptxt>If the indicator remains on or does not come on, proceed to troubleshooting for the light circuit below.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.95in"/>
<colspec colname="COL2" colwidth="2.13in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>See Procedure</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ABS warning light circuit (Remains on)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep01" href="RM000001DXE020X"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS warning light circuit (Does not come on)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep02" href="RM000001DXL020X"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake warning light circuit (Remains on)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep03" href="RM000001DXR022X"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake warning light circuit (Does not come on)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep04" href="RM000001DXY021X"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Slip indicator light circuit (Remains on)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep07" href="RM000001DXZ01UX"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Slip indicator light circuit (Does not come on)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep08" href="RM000001DY001SX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>