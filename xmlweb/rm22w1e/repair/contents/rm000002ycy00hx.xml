<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000D" variety="S000D">
<name>1VD-FTV ENGINE MECHANICAL</name>
<ttl id="12007_S000D_7C3FS_T008V" variety="T008V">
<name>CYLINDER HEAD</name>
<para id="RM000002YCY00HX" category="A" type-id="30019" name-id="EM35E-01" from="201301">
<name>REPLACEMENT</name>
<subpara id="RM000002YCY00HX_01" type-id="01" category="01">
<s-1 id="RM000002YCY00HX_01_0005" proc-id="RM22W0E___00005NV00000">
<ptxt>REPLACE VALVE GUIDE BUSH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A164872" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Gradually heat the cylinder head to 80 to 100°C (176 to 212°F).</ptxt>
</s2>
<s2>
<ptxt>Place the cylinder head on a wooden block.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A164547E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using SST, tap out the valve guide bush.</ptxt>
<sst>
<sstitem>
<s-number>09201-10000</s-number>
<s-subnumber>09201-01060</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<figure>
<graphic graphicname="A164581E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a caliper gauge, measure the bush bore diameter of the cylinder head.</ptxt>
<spec>
<title>Standard bush bore diameter</title>
<specitem>
<ptxt>10.985 to 11.006 mm (0.432 to 0.433 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the bush bore diameter of the cylinder head is more than 11.006 mm (0.433 in.), machine the bush bore to the dimension of 11.035 to 11.056 mm (0.434 to 0.435 in.).</ptxt>
<spec>
<title>Standard Valve Guide Bush Diameter</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="3.10in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>STD</ptxt>
</entry>
<entry align="center">
<ptxt>11.033 to 11.044 mm (0.434 to 0.435 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O/S 0.05</ptxt>
</entry>
<entry align="center">
<ptxt>11.083 to 11.094 mm (0.436 to 0.437 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the bush bore diameter of the cylinder head is more than 11.056 mm (0.435 in.), replace the cylinder head sub-assembly.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A164872" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Gradually heat the cylinder head to 80 to 100°C (176 to 212°F).</ptxt>
</s2>
<s2>
<ptxt>Place the cylinder head on a wooden block.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A164804E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using SST, tap in a new valve guide bush to the specified protrusion height A shown in the illustration.</ptxt>
<sst>
<sstitem>
<s-number>09201-10000</s-number>
<s-subnumber>09201-01060</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<spec>
<title>Standard protrusion height</title>
<specitem>
<ptxt>9.0 to 9.4 mm (0.354 to 0.370 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<figure>
<graphic graphicname="A164582" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a sharp 6.0 mm reamer, ream the valve guide bush to obtain the standard specified clearance between the valve guide bush and valve stem.</ptxt>
<spec>
<title>Standard Overall Length</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Intake</ptxt>
</entry>
<entry align="center">
<ptxt>0.025 to 0.060 mm (0.000984 to 0.00236 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Exhaust</ptxt>
</entry>
<entry align="center">
<ptxt>0.035 to 0.070 mm (0.00138 to 0.00276 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YCY00HX_01_0003" proc-id="RM22W0E___00005NU00000">
<ptxt>REPLACE HEAD STRAIGHT SCREW PLUG</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 6 mm hexagon wrench, remove the straight screw plug and gasket.</ptxt>
<figure>
<graphic graphicname="A164575E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Using a 6 mm hexagon wrench, install a new gasket and straight screw plug.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YCY00HX_01_0006" proc-id="RM22W0E___00005NW00000">
<ptxt>REPLACE RING PIN</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A164549E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<atten4>
<ptxt>It is not necessary to remove the ring pin unless it is being replaced.</ptxt>
</atten4>
<s2>
<ptxt>Remove the ring pins.</ptxt>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer, tap in new ring pins.</ptxt>
<spec>
<title>Standard protrusion</title>
<specitem>
<ptxt>4.4 to 5.6 mm (0.173 to 0.220 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YCY00HX_01_0008" proc-id="RM22W0E___00005NX00000">
<ptxt>REPLACE STUD BOLT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A164548" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>If a stud bolt is deformed or the threads are damaged, replace it.</ptxt>
</atten4>
<s2>
<ptxt>Using an E8 "TORX" wrench, remove the stud bolts.</ptxt>
</s2>
<s2>
<ptxt>Using an E8 "TORX" wrench, install new stud bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>