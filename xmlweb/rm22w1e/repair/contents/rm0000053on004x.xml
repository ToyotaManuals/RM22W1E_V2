<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM0000053ON004X" category="J" type-id="80658" name-id="BCEFI-01" from="201301" to="201308">
<dtccode/>
<dtcname>Turn Assist Indicator Light Circuit</dtcname>
<subpara id="RM0000053ON004X_01" type-id="60" category="03" proc-id="RM22W0E___0000ALR00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When any of the following conditions are met, the turn assist system becomes operable and the turn assist indicator light illuminates.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Crawl Control is operating.</ptxt>
</item>
<item>
<ptxt>The turn assist function switch (suspension control switch) is on.</ptxt>
</item>
<item>
<ptxt>The center differential is "free".</ptxt>
</item>
<item>
<ptxt>The vehicle is moving at a speed of 10 km/h (6 mph) or less.</ptxt>
</item>
<item>
<ptxt>The shift lever is not in P, R or N.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM0000053ON004X_02" type-id="32" category="03" proc-id="RM22W0E___0000ALS00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C256733E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000053ON004X_03" type-id="51" category="05" proc-id="RM22W0E___0000ALT00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000053ON004X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000053ON004X_04_0013" proc-id="RM22W0E___0000ABS00000">
<testtitle>CHECK CAN COMMUNICATION LINE
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Select "CAN Bus Check" from the System Selection Menu screen, and follow the prompts on the screen to inspect the CAN Bus.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>"CAN Bus Check" indicates no malfunctions in CAN communication.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM0000053ON004X_04_0014" fin="false">A</down>
<right ref="RM0000053ON004X_04_0006" fin="true">B</right>
<right ref="RM0000053ON004X_04_0011" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000053ON004X_04_0014" proc-id="RM22W0E___0000ABT00000">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (for LHD: See page <xref label="Seep01" href="RM000001RSW03JX"/>, for RHD: See page <xref label="Seep02" href="RM000001RSW03KX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>CAN system DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN system DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN system DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM0000053ON004X_04_0003" fin="false">A</down>
<right ref="RM0000053ON004X_04_0006" fin="true">B</right>
<right ref="RM0000053ON004X_04_0011" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000053ON004X_04_0003" proc-id="RM22W0E___0000ALU00000">
<testtitle>INSPECT TURN ASSIST FUNCTION SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the turn assist function switch (suspension control switch) (See page <xref label="Seep01" href="RM000003C4A00ZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the turn assist function switch (suspension control switch) (See page <xref label="Seep02" href="RM000003C4B00UX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000053ON004X_04_0004" fin="false">OK</down>
<right ref="RM0000053ON004X_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000053ON004X_04_0004" proc-id="RM22W0E___0000ALV00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU - TURN ASSIST FUNCTION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector. </ptxt>
</test1>
<test1>
<ptxt>Disconnect the V4 turn assist function switch (suspension control switch) connector. </ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-29 (EXI4) - V4-2 (OTA)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-29 (EXI4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>V4-16 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000053ON004X_04_0005" fin="false">OK</down>
<right ref="RM0000053ON004X_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000053ON004X_04_0005" proc-id="RM22W0E___0000ALW00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU EXI4 CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the skid control ECU (master cylinder solenoid) connector. </ptxt>
</test1>
<test1>
<ptxt>Disconnect the turn assist function switch (suspension control switch) connector. </ptxt>
<figure>
<graphic graphicname="E133658E10" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.63in"/>
<colspec colname="COL2" colwidth="1.50in"/>
<colspec colname="COL3" colwidth="1.00in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>V4-2 (OTA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Turn Assist Function Switch [Suspension Control Switch])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.47in"/>
<colspec colname="COL2" colwidth="1.66in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000053ON004X_04_0009" fin="true">A</down>
<right ref="RM0000053ON004X_04_0010" fin="true">B</right>
<right ref="RM0000053ON004X_04_0012" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000053ON004X_04_0006">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000053ON004X_04_0007">
<testtitle>REPLACE SUSPENSION CONTROL SWITCH<xref label="Seep01" href="RM000003C4A00ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000053ON004X_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000053ON004X_04_0009">
<testtitle>GO TO METER / GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L03NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000053ON004X_04_0010">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000053ON004X_04_0011">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000053ON004X_04_0012">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>