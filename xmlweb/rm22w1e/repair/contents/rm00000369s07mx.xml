<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM00000369S07MX" category="C" type-id="803LA" name-id="BCDWR-04" from="201308">
<dtccode>C1415</dtccode>
<dtcname>Rear Speed Sensor RH Output Malfunction</dtcname>
<dtccode>C1277</dtccode>
<dtcname>Abnormal Change in Output Signal of Rear Speed Sensor RH (Test Mode DTC)</dtcname>
<dtccode>C1278</dtccode>
<dtcname>Abnormal Change in Output Signal of Rear Speed Sensor LH (Test Mode DTC)</dtcname>
<dtccode>C1416</dtccode>
<dtcname>Rear Speed Sensor LH Output Malfunction</dtcname>
<subpara id="RM00000369S07MX_01" type-id="60" category="03" proc-id="RM22W0E___0000AFC00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1401 and C1402 (See page <xref label="Seep01" href="RM000000XI90WBX_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.93in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1415</ptxt>
<ptxt>C1416</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>An open in the sensor signal circuit of a malfunctioning area occurs 255 times or more.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 20 km/h (12 mph) or more, noise occurs in the sensor signals of a malfunctioning wheel 75 times or more within 5 seconds.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 10 km/h (6 mph) or more, noise occurs once per rotor rotation for 15 seconds or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Rear speed sensor RH/LH</ptxt>
</item>
<item>
<ptxt>Skid control sensor wire</ptxt>
</item>
<item>
<ptxt>Speed sensor circuit</ptxt>
</item>
<item>
<ptxt>Speed sensor rotor</ptxt>
</item>
<item>
<ptxt>Skid control ECU (Master cylinder solenoid)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1277</ptxt>
<ptxt>C1278</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stored only during test mode.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Rear speed sensor RH/LH</ptxt>
</item>
<item>
<ptxt>Speed sensor rotor</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTCs C1415 and C1277 are for the rear speed sensor RH.</ptxt>
</item>
<item>
<ptxt>DTCs C1416 and C1278 are for the rear speed sensor LH.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000369S07MX_02" type-id="32" category="03" proc-id="RM22W0E___0000AFD00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1407 and C1408 (See page <xref label="Seep01" href="RM00000369307IX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000369S07MX_03" type-id="51" category="05" proc-id="RM22W0E___0000AFE00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</item>
<item>
<ptxt>Check the speed sensor signal after cleaning or replacement (See page <xref label="Seep02" href="RM00000452K00NX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM00000369S07MX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000369S07MX_05_0025" proc-id="RM22W0E___0000AES00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MOMENTARY INTERRUPTION)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the GTS, check for any momentary interruption in the wire harness and connector corresponding to the DTC (See page <xref label="Seep01" href="RM000001DX1022X"/>).</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.17in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>RR Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor RH open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor LH open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Normal (there are no momentary interruptions).</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Perform the above inspection before removing the sensor and connector.</ptxt>
</atten4>
</test1>
</content6><res>
<down ref="RM00000369S07MX_05_0026" fin="false">OK</down>
<right ref="RM00000369S07MX_05_0028" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0026" proc-id="RM22W0E___0000AEC00001">
<testtitle>READ VALUE USING GTS (RR/RL WHEEL SPEED)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.16in"/>
<colspec colname="COL2" colwidth="2.33in"/>
<colspec colname="COL3" colwidth="1.63in"/>
<colspec colname="COLSPEC1" colwidth="1.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>RR Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor RH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor LH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that there is no difference between the speed value output from the speed sensor displayed on the GTS and the speed value displayed on the speedometer when driving the vehicle.</ptxt>
<atten4>
<ptxt>Factors that affect the indicated vehicle speed include tire size, tire inflation and tire wear. The speed indicated on the speedometer has an allowable margin of error. This can be tested using a speedometer tester (calibrated chassis dynamometer). For details about testing and the margin of error, see the reference chart (See page <xref label="Seep01" href="RM000002Z4Q02RX"/>).</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The speed value output from the speed sensor displayed on the GTS is the same as the actual vehicle speed measured using a speedometer tester (calibrated chassis dynamometer).</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM00000369S07MX_05_0020" fin="false">OK</down>
<right ref="RM00000369S07MX_05_0027" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0020" proc-id="RM22W0E___0000AFG00001">
<testtitle>PERFORM TEST MODE (SIGNAL CHECK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform the sensor check in the Test Mode Procedure (See page <xref label="Seep01" href="RM00000452K00NX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>All test mode DTCs are cleared.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000369S07MX_05_0003" fin="false">OK</down>
<right ref="RM00000369S07MX_05_0027" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0003" proc-id="RM22W0E___0000AFF00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 40 km/h (25 mph) or more for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000369S07MX_05_0010" fin="true">A</down>
<right ref="RM00000369S07MX_05_0039" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0027" proc-id="RM22W0E___0000AEB00001">
<testtitle>CHECK REAR SPEED SENSOR INSTALLATION
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Check the speed sensor installation (See page <xref label="Seep01" href="RM000001B2E01XX"/>).</ptxt>
<figure>
<graphic graphicname="C144929E49" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>There is no clearance between the sensor and the rear axle.</ptxt>
<ptxt>The installation nut is tightened properly.</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Speed Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>No clearance</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM00000369S07MX_05_0041" fin="false">OK</down>
<right ref="RM00000369S07MX_05_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0041" proc-id="RM22W0E___0000AED00001">
<testtitle>INSPECT REAR SPEED SENSOR TIP
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear speed sensor (See page <xref label="Seep01" href="RM000001B2G01TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the speed sensor tip.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No scratches, oil, or foreign matter on the sensor tip.</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>If no damage to the speed sensor tip is found during this inspection, do not replace the speed sensor.</ptxt>
</atten3>
</test1>
</content6><res>
<down ref="RM00000369S07MX_05_0040" fin="false">OK</down>
<right ref="RM00000369S07MX_05_0034" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0040" proc-id="RM22W0E___0000AEE00001">
<testtitle>INSPECT REAR SPEED SENSOR ROTOR
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear axle shaft together with the parking brake plate (See page <xref label="Seep01" href="RM00000270F014X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the speed sensor rotor.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No scratches, oil, or foreign matter on the rotors.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The rear speed sensor rotor is incorporated into the rear axle hub and bearing assembly.</ptxt>
<ptxt>If the rear speed sensor rotor needs to be replaced, replace it together with the rear axle hub and bearing assembly.</ptxt>
</atten4>
</test1>
</content6><res>
<down ref="RM00000369S07MX_05_0028" fin="false">OK</down>
<right ref="RM00000369S07MX_05_0035" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0028" proc-id="RM22W0E___0000AEW00001">
<testtitle>INSPECT SKID CONTROL SENSOR WIRE
</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C214499E11" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Skid Control Sensor Wire</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<test1>
<ptxt>Remove the skid control sensor wire (See page <xref label="Seep01" href="RM000001B2G01TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.05in"/>
<colspec colname="COL2" colwidth="1.68in"/>
<colspec colname="COL3" colwidth="2.35in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Nz1-4 - z19-2 (RR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Nz1-3 - z19-1 (RR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Nz1-4 - Nz1-3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Nz1-4 - Nz1-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Nz1-4 - Nz1-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.05in"/>
<colspec colname="COL2" colwidth="1.68in"/>
<colspec colname="COL3" colwidth="2.35in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Nz1-2 - z20-2 (RL+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Nz1-1 - z20-1 (RL-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Nz1-2 - Nz1-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Nz1-2 - Nz1-3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Nz1-2 - Nz1-4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM00000369S07MX_05_0029" fin="false">OK</down>
<right ref="RM00000369S07MX_05_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0029" proc-id="RM22W0E___0000AEU00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU - REAR SPEED SENSOR)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Install the skid control sensor wire (See page <xref label="Seep01" href="RM000001B2E01XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the z19 and/or z20 speed sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.02in"/>
<colspec colname="COL2" colwidth="1.71in"/>
<colspec colname="COL3" colwidth="2.35in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-5 (RR+) - z19-2 (RR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-5 (RR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-19 (RR-) - z19-1 (RR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-19 (RR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.05in"/>
<colspec colname="COL2" colwidth="1.68in"/>
<colspec colname="COL3" colwidth="2.35in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-20 (RL+) - z20-2 (RL+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-20 (RL+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-6 (RL-) - z20-1 (RL-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-6 (RL-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM00000369S07MX_05_0030" fin="false">OK</down>
<right ref="RM00000369S07MX_05_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0030" proc-id="RM22W0E___0000AEV00001">
<testtitle>CHECK HARNESS AND CONNECTOR (RR+/RL+ TERMINAL)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the speed sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C207217E28" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<title>for RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z19-2 (RR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z20-2 (RL+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Rear Speed Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM00000369S07MX_05_0021" fin="false">A</down>
<right ref="RM00000369S07MX_05_0013" fin="true">B</right>
<right ref="RM00000369S07MX_05_0042" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0021" proc-id="RM22W0E___0000AFH00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 40 km/h (25 mph) or more for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000369S07MX_05_0010" fin="true">A</down>
<right ref="RM00000369S07MX_05_0039" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0039" proc-id="RM22W0E___0000AEF00001">
<testtitle>REPLACE REAR SPEED SENSOR
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the rear speed sensor (See page <xref label="Seep01" href="RM000001B2G01TX"/>).</ptxt>
</test1>
</content6><res>
<down ref="RM00000369S07MX_05_0038" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0038" proc-id="RM22W0E___0000AFI00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 40 km/h (25 mph) or more for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000369S07MX_05_0023" fin="true">A</down>
<right ref="RM00000369S07MX_05_0031" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000369S07MX_05_0010">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000369S07MX_05_0023">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM00000369S07MX_05_0012">
<testtitle>INSTALL REAR SPEED SENSOR CORRECTLY</testtitle>
</testgrp>
<testgrp id="RM00000369S07MX_05_0011">
<testtitle>REPLACE SKID CONTROL SENSOR WIRE<xref label="Seep01" href="RM000001B2G01TX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000369S07MX_05_0017">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000369S07MX_05_0013">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000369S07MX_05_0031">
<testtitle>REPLACE REAR AXLE HUB AND BEARING ASSEMBLY<xref label="Seep01" href="RM00000271100WX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000369S07MX_05_0034">
<testtitle>CLEAN OR REPLACE REAR SPEED SENSOR</testtitle>
</testgrp>
<testgrp id="RM00000369S07MX_05_0035">
<testtitle>CLEAN OR REPLACE REAR SPEED SENSOR ROTOR</testtitle>
</testgrp>
<testgrp id="RM00000369S07MX_05_0042">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>