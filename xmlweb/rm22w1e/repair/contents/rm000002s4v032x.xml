<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S0011" variety="S0011">
<name>3UR-FE LUBRICATION</name>
<ttl id="12012_S0011_7C3KU_T00DX" variety="T00DX">
<name>OIL PUMP</name>
<para id="RM000002S4V032X" category="G" type-id="3001K" name-id="LU1D0-02" from="201301">
<name>INSPECTION</name>
<subpara id="RM000002S4V032X_01" type-id="01" category="01">
<s-1 id="RM000002S4V032X_01_0001" proc-id="RM22W0E___000070600000">
<ptxt>INSPECT OIL PUMP RELIEF VALVE</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A162535" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Coat the relief valve with engine oil.</ptxt>
</s2>
<s2>
<ptxt>Check that the relief valve falls smoothly into the valve hole by its own weight.</ptxt>
<ptxt>If the relief valve does not fall smoothly, replace it. If necessary, replace the timing chain cover sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4V032X_01_0002" proc-id="RM22W0E___000070700000">
<ptxt>INSPECT OIL PUMP ROTOR SET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rotors to the timing chain cover with the marks of the rotors facing outward. Check that the rotors revolve smoothly.</ptxt>
<figure>
<graphic graphicname="A162536E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Check the tip clearance.</ptxt>
<figure>
<graphic graphicname="A162537" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Using a feeler gauge, measure the clearance between the drive and driven rotor tips, as shown in the illustration.</ptxt>
<spec>
<title>Standard tip clearance</title>
<specitem>
<ptxt>0.180 to 0.300 mm (0.00709 to 0.0118 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum tip clearance</title>
<specitem>
<ptxt>0.300 mm (0.0118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the tip clearance is more than the maximum, replace the drive and driven rotors together.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the side clearance.</ptxt>
<figure>
<graphic graphicname="A162538" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Using a feeler gauge and steel square, measure the clearance between the rotors and steel square as shown in the illustration.</ptxt>
<spec>
<title>Standard side clearance</title>
<specitem>
<ptxt>0.030 to 0.090 mm (0.00118 to 0.00354 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum side clearance</title>
<specitem>
<ptxt>0.090 mm (0.00354 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the side clearance is more than the maximum, replace the timing chain cover sub-assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the body clearance.</ptxt>
<figure>
<graphic graphicname="A162539" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Using a feeler gauge, measure the clearance between the timing chain cover and driven rotor as shown in the illustration.</ptxt>
<spec>
<title>Standard body clearance</title>
<specitem>
<ptxt>0.175 to 0.250 mm (0.00689 to 0.00984 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum body clearance</title>
<specitem>
<ptxt>0.250 mm (0.00984 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the body clearance is more than the maximum, replace the timing chain cover sub-assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>