<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3U0_T00N3" variety="T00N3">
<name>SPIRAL CABLE</name>
<para id="RM000002O8U020X" category="A" type-id="30014" name-id="RSAVG-05" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000002O8U020X_01" type-id="01" category="01">
<s-1 id="RM000002O8U020X_01_0001" proc-id="RM22W0E___0000A8Z00001">
<ptxt>INSTALL SPIRAL CABLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the pins on the spiral cable with the installation holes on the steering sensor.</ptxt>
<figure>
<graphic graphicname="C110515E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 6 claws and install the spiral cable to the steering sensor.</ptxt>
</s2>
<s2>
<ptxt>Check that the front wheels are facing straight ahead. </ptxt>
</s2>
<s2>
<ptxt>Set the turn signal switch to the neutral position.</ptxt>
<atten2>
<ptxt>If it is not in the neutral position, the pin of the turn signal switch may be snapped.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Attach the 3 claws to install the spiral cable with steering sensor.</ptxt>
<figure>
<graphic graphicname="B181593" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten2>
<ptxt>When replacing the spiral cable with a new one, remove the lock pin before installing the steering wheel assembly.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Connect the connectors to the spiral cable with steering sensor.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002O8U020X_01_0013" proc-id="RM22W0E___0000A9200001">
<ptxt>INSTALL UPPER STEERING COLUMN COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the upper steering column cover.</ptxt>
<figure>
<graphic graphicname="C179482" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 4 clips to install the upper steering column cover onto the instrument cluster finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002O8U020X_01_0027" proc-id="RM22W0E___0000A9300001">
<ptxt>INSTALL LOWER STEERING COLUMN COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the lower steering column cover.</ptxt>
<figure>
<graphic graphicname="C179481" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not damage the tilt and telescopic switch.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>2.0</t-value1>
<t-value2>20</t-value2>
<t-value3>18</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002O8U020X_01_0004" proc-id="RM22W0E___0000A9000001">
<ptxt>ADJUST SPIRAL CABLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the ignition switch is off.</ptxt>
</s2>
<s2>
<ptxt>Check that the cable is disconnected from the battery negative (-) terminal.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Rotate the spiral cable with steering sensor counterclockwise slowly by hand until it feels firm.</ptxt>
<figure>
<graphic graphicname="B181594" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Do not turn the spiral cable with steering sensor by the airbag wire harness.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Rotate the spiral cable with steering sensor clockwise approximately 2.5 turns to align the marks.</ptxt>
<figure>
<graphic graphicname="B181595E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Do not turn the spiral cable with spiral sensor by the airbag wire harness.</ptxt>
</atten2>
<atten4>
<ptxt>The spiral cable with steering sensor will rotate approximately 2.5 turns to both the left and right from the center.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002O8U020X_01_0014" proc-id="RM22W0E___0000A9400001">
<ptxt>INSTALL STEERING WHEEL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the matchmarks on the steering wheel assembly and steering main shaft assembly.</ptxt>
<figure>
<graphic graphicname="C174823E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the steering wheel assembly set nut.</ptxt>
<torque>
<torqueitem>
<t-value1>50</t-value1>
<t-value2>510</t-value2>
<t-value4>37</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002O8U020X_01_0015" proc-id="RM22W0E___000077F00001">
<ptxt>INSTALL STEERING PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Support the steering pad with one hand.</ptxt>
<figure>
<graphic graphicname="B181568" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the 2 connectors to the steering pad.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the horn connector.</ptxt>
</s2>
<s2>
<ptxt>Confirm that the circumference groove of the "TORX" screw fits in the screw case, and place the steering pad onto the steering wheel.</ptxt>
</s2>
<s2>
<ptxt>Using a T30 "TORX" socket wrench, tighten the 2 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>8.8</t-value1>
<t-value2>90</t-value2>
<t-value3>78</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002O8U020X_01_0018" proc-id="RM22W0E___000077H00001">
<ptxt>INSTALL LOWER NO. 3 STEERING WHEEL COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
<figure>
<graphic graphicname="B181563" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002O8U020X_01_0019" proc-id="RM22W0E___000077G00001">
<ptxt>INSTALL LOWER NO. 2 STEERING WHEEL COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
<figure>
<graphic graphicname="B189186E01" width="2.775699831in" height="4.7836529in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002O8U020X_01_0009" proc-id="RM22W0E___0000FHB00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002O8U020X_01_0020" proc-id="RM22W0E___0000BFY00001">
<ptxt>INSPECT STEERING PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>With the steering pad installed on the vehicle, perform a visual check. If there are any defects as mentioned below, replace the steering pad with a new one:</ptxt>
<ptxt>Cuts, minute cracks or marked discoloration on the steering pad top surface or in the grooved portion.</ptxt>
</s2>
<s2>
<ptxt>Make sure that the horn sounds.</ptxt>
<atten4>
<ptxt>If the horn does not sound, inspect the horn system.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002O8U020X_01_0012" proc-id="RM22W0E___0000FHC00001">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0KMX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>