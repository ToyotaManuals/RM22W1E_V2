<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000000PDU0SKX" category="C" type-id="302GP" name-id="ES11G5-001" from="201301" to="201308">
<dtccode>P0011</dtccode>
<dtcname>Camshaft Position "A" - Timing Over-Advanced or System Performance (Bank 1)</dtcname>
<dtccode>P0012</dtccode>
<dtcname>Camshaft Position "A" - Timing Over-Retarded (Bank 1)</dtcname>
<dtccode>P0021</dtccode>
<dtcname>Camshaft Position "A" - Timing Over-Advanced or System Performance (Bank 2)</dtcname>
<dtccode>P0022</dtccode>
<dtcname>Camshaft Position "A" - Timing Over-Retarded (Bank 2)</dtcname>
<subpara id="RM000000PDU0SKX_01" type-id="60" category="03" proc-id="RM22W0E___00000HD00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The VVT system includes the ECM, camshaft timing oil control valve and VVT controller. The ECM sends a target duty-cycle control signal to the camshaft timing oil control valve. This control signal regulates the oil pressure supplied to the VVT controller. Camshaft timing control is performed according to engine operating conditions such as the intake air volume, throttle valve position and engine coolant temperature. The ECM controls the camshaft timing oil control valve based on the signals transmitted by several sensors. The VVT controller regulates the intake camshaft angle using oil pressure through the camshaft timing oil control valve. As a result, the relative positions of the camshaft and crankshaft are optimized, the engine torque and fuel economy improve and the exhaust emissions decrease under overall driving conditions. The ECM detects the actual intake valve timing using signals from the VVT and crankshaft position sensors and performs feedback control. This is how the target intake valve timing is verified by the ECM.</ptxt>
<figure>
<graphic graphicname="A192077E13" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0011</ptxt>
<ptxt>P0021</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve timing cannot be adjusted when in the advance range (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Valve timing</ptxt>
</item>
<item>
<ptxt>Camshaft timing oil control valve assembly (for intake side)</ptxt>
</item>
<item>
<ptxt>Oil control valve filter</ptxt>
</item>
<item>
<ptxt>Camshaft timing gear assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0012</ptxt>
<ptxt>P0022</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve timing cannot be adjusted when in the retard range (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Valve timing</ptxt>
</item>
<item>
<ptxt>Camshaft timing oil control valve assembly (for intake side)</ptxt>
</item>
<item>
<ptxt>Oil control valve filter</ptxt>
</item>
<item>
<ptxt>Camshaft timing gear assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000PDU0SKX_02" type-id="64" category="03" proc-id="RM22W0E___00000HE00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>The ECM optimizes the intake valve timing using the VVT (Variable Valve Timing) system to control the intake camshaft. The VVT system includes the ECM, camshaft timing oil control valve and VVT controller. The ECM sends a target duty cycle control signal to the camshaft timing oil control valve. This control signal regulates the oil pressure supplied to the VVT controller. The VVT controller can advance or retard the intake camshaft.</ptxt>
</item>
<item>
<ptxt>If the difference between the target and actual intake valve timing is large, and changes in the actual intake valve timing are small, the ECM interprets this as a VVT controller being stuck and stores a DTC.</ptxt>
</item>
</list1>
<list1 type="nonmark">
<item>
<ptxt>Example:</ptxt>
</item>
<item>
<ptxt>A DTC is stored when the following conditions 1 and 2 are met:</ptxt>
</item>
<list2 type="nonmark">
<item>
<ptxt>1. It takes 5 seconds or more to change the valve timing by 5°CA.</ptxt>
</item>
<item>
<ptxt>2. After the above condition 1 is met, the OCV is forcibly activated 63 times or more (when the vehicle is idling, this takes approximately 100 seconds).</ptxt>
</item>
</list2>
<item>
<ptxt>These DTCs indicate that the VVT controller cannot operate properly due to camshaft timing oil control valve malfunctions or the presence of foreign objects in the camshaft timing oil control valve.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000PDU0SKX_07" type-id="32" category="03" proc-id="RM22W0E___00000HF00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0010 (See page <xref label="Seep01" href="RM000000PDW0Q7X_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PDU0SKX_08" type-id="51" category="05" proc-id="RM22W0E___00000HG00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>DTC P0011, P0012, P0021 or P0022 may be stored when foreign objects in the engine oil are caught in some parts of the system. The DTC will remain stored even if the system returns to normal after a short time. Foreign objects are filtered out by the oil filter.</ptxt>
</atten4>
<atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Abnormal Bank</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Timing Over-advanced</ptxt>
<ptxt>(Valve Timing is Out of Specified Range)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Timing Over-retarded</ptxt>
<ptxt>(Valve Timing is Out of Specified Range)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Bank 1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P0011</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P0012</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Bank 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P0021</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P0022</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>If DTC P0011 or P0012 is output, check the right bank VVT system for the intake camshaft circuit (bank 1).</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>If DTC P0021 or P0022 is output, check the left bank VVT system for the intake camshaft circuit (bank 2).</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PDU0SKX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PDU0SKX_09_0001" proc-id="RM22W0E___00000HH00000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0011, P0012, P0021 OR P0022)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0011, P0012, P0021 or P0022 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0011, P0012, P0021 or P0022 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0011, P0012, P0021 or P0022 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SKX_09_0002" fin="false">A</down>
<right ref="RM000000PDU0SKX_09_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0002" proc-id="RM22W0E___00000HI00000">
<testtitle>PERFORM ACTIVE TEST USING GTS (OPERATE CAMSHAFT TIMING OIL CONTROL VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Warm up the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the A/C on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the VVT Linear (Bank 1) or Control the VVT Linear (Bank 2).</ptxt>
</test1>
<test1>
<ptxt>Check the engine speed while operating the camshaft timing oil control valve using the GTS.</ptxt>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry align="center">
<ptxt>GTS Operation</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>0%</ptxt>
</entry>
<entry align="center">
<ptxt>Normal engine speed</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>100%</ptxt>
</entry>
<entry align="center">
<ptxt>Engine idles roughly or stalls</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SKX_09_0003" fin="false">OK</down>
<right ref="RM000000PDU0SKX_09_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0003" proc-id="RM22W0E___00000HJ00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0011, P0012, P0021 OR P0022)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14FX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the GTS (See page <xref label="Seep02" href="RM000000PDL0SBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for more than 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the GTS.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTC output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SKX_09_0004" fin="true">OK</down>
<right ref="RM000000PDU0SKX_09_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0004">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0005" proc-id="RM22W0E___00000E600000">
<testtitle>CHECK VALVE TIMING (CHECK FOR LOOSE TEETH AND WHETHER TIMING CHAIN HAS JUMPED TOOTH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the cylinder head cover RH and LH.</ptxt>
<figure>
<graphic graphicname="A103826E11" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Timing Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Turn the crankshaft pulley and align its groove with the "0" timing mark on the timing chain cover.</ptxt>
</test1>
<test1>
<ptxt>Check that the timing marks on the camshaft timing gears are aligned with the timing marks of the bearing cap as shown in the illustration.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>If not, turn the crankshaft 1 revolution (360°), and then align the marks as above.</ptxt>
</item>
</list1>
<spec>
<title>OK</title>
<specitem>
<ptxt>Timing marks on camshaft timing gears are aligned as shown in the illustration.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Reinstall the cylinder head cover RH and LH.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SKX_09_0006" fin="false">OK</down>
<right ref="RM000000PDU0SKX_09_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0006" proc-id="RM22W0E___00000HK00000">
<testtitle>INSPECT CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the camshaft timing oil control valve assembly (See page <xref label="Seep01" href="RM000000Q74046X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SKX_09_0007" fin="false">OK</down>
<right ref="RM000000PDU0SKX_09_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0007" proc-id="RM22W0E___00000HL00000">
<testtitle>INSPECT OIL CONTROL VALVE FILTER AND OIL PIPE</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A225778E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Remove the No. 1 or No. 2 oil pipe.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>No. 1 Oil Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>No. 2 Oil Pipe</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Remove the oil control valve filter.</ptxt>
</test1>
<test1>
<ptxt>Check that the filter and pipe are not clogged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The filter and pipe are not clogged.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Reinstall the oil control valve filter.</ptxt>
</test1>
<test1>
<ptxt>Reinstall the No. 1 or No. 2 oil pipe.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SKX_09_0016" fin="false">OK</down>
<right ref="RM000000PDU0SKX_09_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0016" proc-id="RM22W0E___00000HN00000">
<testtitle>REPLACE CAMSHAFT TIMING GEAR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the camshaft timing gear assembly (See page <xref label="Seep01" href="RM000003B6X013X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SKX_09_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0009">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM000002ZSO00WX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0010">
<testtitle>ADJUST VALVE TIMING<xref label="Seep01" href="RM000002BK6043X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0011">
<testtitle>REPLACE CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY<xref label="Seep01" href="RM000000PWP05JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0012">
<testtitle>REPLACE OIL CONTROL VALVE FILTER AND OIL PIPE<xref label="Seep01" href="RM000002BK5043X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0013" proc-id="RM22W0E___00000HM00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0011, P0012, P0021 OR P0022)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14FX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the GTS (See page <xref label="Seep02" href="RM000000PDL0SBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for more than 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Read the output DTCs using the GTS.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTC output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>DTC P0011, P0012, P0021 or P0022 is stored when foreign objects in the engine oil are caught in some parts of the system. These codes will remain stored even if the system returns to normal after a short time. These foreign objects are then captured by the oil filter, thus eliminating the source of the problem.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SKX_09_0015" fin="true">OK</down>
<right ref="RM000000PDU0SKX_09_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0014">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SKX_09_0015">
<testtitle>SYSTEM OK</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>