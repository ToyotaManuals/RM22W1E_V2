<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S002F" variety="S002F">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S002F_7C3WZ_T00Q2" variety="T00Q2">
<name>FRONT CONSOLE BOX (w/o Console Box Lid)</name>
<para id="RM000004XLC001X" category="A" type-id="80001" name-id="IT31H-01" from="201301">
<name>REMOVAL</name>
<subpara id="RM000004XLC001X_01" type-id="11" category="10" proc-id="RM22W0E___0000HTT00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000004XLC001X_02" type-id="01" category="01">
<s-1 id="RM000004XLC001X_02_0012" proc-id="RM22W0E___0000HU000000">
<ptxt>REMOVE FRONT SEAT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000390S00TX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0007" proc-id="RM22W0E___00008UQ00000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292994E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the No. 2 instrument panel finish panel cushion.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the No. 2 instrument panel finish panel cushion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0008" proc-id="RM22W0E___00008UR00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292995" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the clip and screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 8 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors, detach the 2 clamps and remove the lower instrument panel pad sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0009" proc-id="RM22W0E___0000BBR00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL FINISH PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180004E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the No. 1 instrument panel finish panel cushion.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the No. 1 instrument panel finish panel cushion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0010" proc-id="RM22W0E___0000BBS00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180005" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the clip and screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 7 claws and remove the lower instrument panel pad sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0011" proc-id="RM22W0E___0000BBT00000">
<ptxt>REMOVE LOWER CENTER INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292996" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 7 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the lower center instrument cluster finish panel sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0001" proc-id="RM22W0E___0000HTU00000">
<ptxt>REMOVE UPPER NO. 2 CONSOLE PANEL GARNISH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the upper No. 2 console panel garnish.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 8 claws and remove the upper No. 2 console panel garnish.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<figure>
<graphic graphicname="B291236E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Separate Seat Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Bench Seat Type</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0002" proc-id="RM22W0E___0000HTV00000">
<ptxt>REMOVE UPPER NO. 1 CONSOLE PANEL GARNISH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B291238E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the upper No. 1 console panel garnish.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 8 claws and remove the upper No. 1 console panel garnish.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0003" proc-id="RM22W0E___0000HTW00000">
<ptxt>REMOVE REAR UPPER CONSOLE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the rear upper console panel sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 12 claws and remove the rear upper console panel sub-assembly.</ptxt>
<figure>
<graphic graphicname="B291240E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0004" proc-id="RM22W0E___0000HTX00000">
<ptxt>REMOVE SHIFT LEVER KNOB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B291232" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Twist the shift lever knob sub-assembly in the direction indicated by the arrow and remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0005" proc-id="RM22W0E___0000HTY00000">
<ptxt>REMOVE UPPER CONSOLE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B291234E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the upper console panel sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 8 claws and remove the upper console panel sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLC001X_02_0006" proc-id="RM22W0E___0000HTZ00000">
<ptxt>REMOVE LOWER CONSOLE BOX</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B291242" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws and 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the lower console box.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>