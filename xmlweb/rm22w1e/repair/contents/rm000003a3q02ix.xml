<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3VS_T00OV" variety="T00OV">
<name>REAR AIR CONDITIONING UNIT (w/o Heater)</name>
<para id="RM000003A3Q02IX" category="A" type-id="80001" name-id="ACBJM-02" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM000003A3Q02IX_01" type-id="01" category="01">
<s-1 id="RM000003A3Q02IX_01_0014" proc-id="RM22W0E___000049F00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 7 clips and radiator support seal.</ptxt>
<figure>
<graphic graphicname="B180890E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003A3Q02IX_01_0005" proc-id="RM22W0E___00004A400000">
<ptxt>RECOVER REFRIGERANT FROM REFRIGERATION SYSTEM
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>Turn the A/C switch on.</ptxt>
</s2>
<s2>
<ptxt>Operate the cooler compressor while the engine speed is approximately 1000 rpm for 5 to 6 minutes to circulate the refrigerant and collect the compressor oil remaining in each component into the cooler compressor.</ptxt>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Recover the refrigerant from the A/C system using a refrigerant recovery unit.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003A3Q02IX_01_0016" proc-id="RM22W0E___0000GPT00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="2">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003A3Q02IX_01_0007" proc-id="RM22W0E___0000GPO00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003A3Q02IX_01_0008" proc-id="RM22W0E___0000GPP00000">
<ptxt>REMOVE REAR FLOOR MAT REAR SUPPORT PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000038MO00QX_01_0007"/>)</ptxt>
</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000038MO00PX_01_0007"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A3Q02IX_01_0009" proc-id="RM22W0E___0000GPQ00000">
<ptxt>REMOVE REAR DOOR SCUFF PLATE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000038MO00QX_01_0005"/>)</ptxt>
</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000038MO00PX_01_0005"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A3Q02IX_01_0015" proc-id="RM22W0E___0000CGA00000">
<ptxt>REMOVE TONNEAU COVER ASSEMBLY (w/ Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the tonneau cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003A3Q02IX_01_0013" proc-id="RM22W0E___0000GPS00000">
<ptxt>REMOVE REAR SEAT COVER CAP (w/ Rear No. 2 Seat)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000038MO00QX_01_0086"/>)</ptxt>
</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000038MO00PX_01_0092"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A3Q02IX_01_0010" proc-id="RM22W0E___0000GPR00000">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000038MO00QX_01_0020"/>)</ptxt>
</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000038MO00PX_01_0020"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A3Q02IX_01_0001" proc-id="RM22W0E___0000GPL00000">
<ptxt>DISCONNECT AIR CONDITIONER TUBE AND ACCESSORY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154366" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the air conditioner tube and accessory assembly.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not use a screwdriver or similar tool to disconnect the tube.</ptxt>
</item>
<item>
<ptxt>Seal the openings of the disconnected parts using vinyl tape to prevent moisture and foreign matter from entering them.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the 2 O-rings from the air conditioner tube and accessory assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A3Q02IX_01_0003" proc-id="RM22W0E___0000GPM00000">
<ptxt>REMOVE REAR ROOF NO. 1 AIR DUCT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154869" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws and remove the cooler plate.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="E154870" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the clip and duct.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A3Q02IX_01_0004" proc-id="RM22W0E___0000GPN00000">
<ptxt>REMOVE REAR COOLING UNIT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154871" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 bolts and rear cooling unit.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>