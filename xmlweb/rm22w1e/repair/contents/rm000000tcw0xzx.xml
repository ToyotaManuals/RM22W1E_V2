<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000TCW0XZX" category="C" type-id="302B5" name-id="ES17MO-001" from="201308">
<dtccode>P0335</dtccode>
<dtcname>Crankshaft Position Sensor "A" Circuit</dtcname>
<dtccode>P0337</dtccode>
<dtcname>Crankshaft Position Sensor "A" Circuit Low Input</dtcname>
<dtccode>P0338</dtccode>
<dtcname>Crankshaft Position Sensor "A" Circuit High Input</dtcname>
<dtccode>P0339</dtccode>
<dtcname>Crankshaft Position Sensor "A" Circuit Intermittent</dtcname>
<subpara id="RM000000TCW0XZX_01" type-id="60" category="03" proc-id="RM22W0E___00001CS00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The crankshaft position sensor system consists of a crankshaft angle sensor rotor and Magnetoresistive Element (MRE) type sensor. The crankshaft angle sensor rotor has 34 teeth at 10° intervals (2 teeth are missing for detecting top dead center), and is installed to the rear end of the crankshaft. The crankshaft position sensor generates 34 signals per crankshaft revolution. The ECM uses the G2 signal to distinguish between the cylinders, and uses the NE signal to detect the crankshaft position and engine speed.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0335</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>No crankshaft position sensor signal is sent to the ECM while cranking (1 trip detection logic).</ptxt>
</item>
<item>
<ptxt>No crankshaft position sensor signal is sent to the ECM at an engine speed of 600 rpm or more (1 trip detection logic).</ptxt>
</item>
</list1>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Open or short in crankshaft position sensor circuit</ptxt>
</item>
<item>
<ptxt>Crankshaft position sensor</ptxt>
</item>
<item>
<ptxt>Crankshaft angle sensor rotor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0337</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output voltage of crankshaft position sensor is 0.3 V or less for 4 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Open or short in crankshaft position sensor circuit</ptxt>
</item>
<item>
<ptxt>Crankshaft position sensor</ptxt>
</item>
<item>
<ptxt>Crankshaft angle sensor rotor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0338</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output voltage of crankshaft position sensor is 4.7 V or higher for 4 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Open or short in crankshaft position sensor circuit</ptxt>
</item>
<item>
<ptxt>Crankshaft position sensor</ptxt>
</item>
<item>
<ptxt>Crankshaft angle sensor rotor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0339</ptxt>
</entry>
<entry valign="middle">
<ptxt>Under conditions (a), (b) and (c), no crankshaft position sensor signal is sent to the ECM for 0.05 seconds or more (1 trip detection logic):</ptxt>
<ptxt>(a) Engine speed is 1000 rpm or more.</ptxt>
<ptxt>(b) Starter signal is off.</ptxt>
<ptxt>(c) 3 seconds or more have elapsed since the starter signal switched from on to off.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in crankshaft position sensor circuit</ptxt>
</item>
<item>
<ptxt>Crankshaft position sensor</ptxt>
</item>
<item>
<ptxt>Crankshaft angle sensor rotor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>Reference: Inspection using an oscilloscope.</ptxt>
<figure>
<graphic graphicname="A211721E04" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C46-110 (NE+) - C46-111 (NE-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Cranking or idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-90 (G2) - C46-89 (G2-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Cranking or idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection </ptxt>
</entry>
<entry align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C45-110 (NE+) - C45-111 (NE-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Cranking or idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-90 (G2) - C45-89 (G2-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Cranking or idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>G2 is the camshaft position sensor signal, and NE is the crankshaft position sensor signal.</ptxt>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000TCW0XZX_02" type-id="64" category="03" proc-id="RM22W0E___00001CT00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If there is no signal from the crankshaft position sensor despite the crankshaft revolving, the ECM interprets this as a malfunction of the sensor.</ptxt>
<ptxt>When the sensor output voltage remains at below 0.3 V, or higher than 4.7 V for more than 4 seconds, the ECM stores a DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000TCW0XZX_14" type-id="73" category="03" proc-id="RM22W0E___00001D400001">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<list1 type="ordered">
<item>
<ptxt>Start the engine and run it at idle for 20 seconds or more.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000TCW0XZX_07" type-id="32" category="03" proc-id="RM22W0E___00001CU00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A232651E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TCW0XZX_08" type-id="51" category="05" proc-id="RM22W0E___00001CV00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>If no problem is found by this diagnostic troubleshooting procedure, check for problems by referring to the engine mechanical section.</ptxt>
</item>
<item>
<ptxt>The engine speed can be checked by using the GTS. To perform the check, follow the procedures below:</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Start the engine.</ptxt>
</item>
<item>
<ptxt>Turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Engine Speed.</ptxt>
</item>
</list2>
<list2 type="nonmark">
<item>
<ptxt>The engine speed may be indicated as zero despite the engine running normally. This is caused by a lack of NE signals from the crankshaft position sensor. Alternatively, the engine speed may be indicated as lower than the actual engine speed if the crankshaft position sensor output voltage is insufficient.</ptxt>
</item>
</list2>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine conditions when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TCW0XZX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TCW0XZX_09_0013" proc-id="RM22W0E___00001D200001">
<testtitle>READ VALUE USING GTS (ENGINE SPEED)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Engine Speed.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Read the values displayed on the GTS while the engine is running.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>Correct values are displayed.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To check the engine speed change, display the graph on the GTS.</ptxt>
</item>
<item>
<ptxt>If the engine does not start, check the engine speed while cranking.</ptxt>
</item>
<item>
<ptxt>If the engine speed indicated on the GTS remains zero (0), there may be an open or short in the Crankshaft Position sensor circuit.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000TCW0XZX_09_0014" fin="true">OK</down>
<right ref="RM000000TCW0XZX_09_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0001" proc-id="RM22W0E___00001CW00001">
<testtitle>INSPECT CRANKSHAFT POSITION SENSOR (SENSOR POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A186169E24" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the crankshaft position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C114-3 (VC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Crankshaft Position Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TCW0XZX_09_0002" fin="false">OK</down>
<right ref="RM000000TCW0XZX_09_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0002" proc-id="RM22W0E___00001CX00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CRANKSHAFT POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the crankshaft position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C114-1 (NE+) - C46-110 (NE+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C114-2 (NE-) - C46-111 (NE-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C114-1 (NE+) or C46-110 (NE+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C114-2 (NE-) or C46-111 (NE-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C114-1 (NE+) - C45-110 (NE+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C114-2 (NE-) - C45-111 (NE-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C114-1 (NE+) or C45-110 (NE+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C114-2 (NE-) or C45-111 (NE-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TCW0XZX_09_0003" fin="false">OK</down>
<right ref="RM000000TCW0XZX_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0003" proc-id="RM22W0E___00001CY00001">
<testtitle>CHECK SENSOR INSTALLATION (CRANKSHAFT POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="BR03795E25" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Check the crankshaft position sensor installation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Sensor is installed correctly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TCW0XZX_09_0004" fin="false">OK</down>
<right ref="RM000000TCW0XZX_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0004" proc-id="RM22W0E___00001CZ00001">
<testtitle>INSPECT CRANKSHAFT ANGLE SENSOR ROTOR (TEETH OF SENSOR ROTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the teeth of the sensor rotor.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Sensor rotor does not have any cracks or deformation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TCW0XZX_09_0008" fin="false">OK</down>
<right ref="RM000000TCW0XZX_09_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0008" proc-id="RM22W0E___00001D000001">
<testtitle>REPLACE CRANKSHAFT POSITION SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the crankshaft position sensor (See page <xref label="Seep01" href="RM000002PQ802WX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TCW0XZX_09_0015" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0015" proc-id="RM22W0E___00001D300001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0335, P0337, P0338 AND/OR P0339)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK187X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.59in"/>
<colspec colname="COL2" colwidth="2.49in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0335, P0337, P0338 or P0339 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the engine does not start, replace the ECM.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000TCW0XZX_09_0017" fin="true">A</down>
<right ref="RM000000TCW0XZX_09_0016" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0010" proc-id="RM22W0E___00001D100001">
<testtitle>CHECK HARNESS AND CONNECTOR (CRANKSHAFT POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the crankshaft position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C114-3 (VC) - C46-66 (VCV2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C114-3 (VC) or C46-66 (VCV2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C114-3 (VC) - C45-66 (VCV2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C114-3 (VC) or C45-66 (VCV2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TCW0XZX_09_0011" fin="true">OK</down>
<right ref="RM000000TCW0XZX_09_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0014">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ13TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0007">
<testtitle>SECURELY REINSTALL SENSOR<xref label="Seep01" href="RM000002PQ602WX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0005">
<testtitle>REPLACE CRANKSHAFT ANGLE SENSOR ROTOR<xref label="Seep01" href="RM000002PQ802WX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0016">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0012">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0017">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000TCW0XZX_09_0011">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>