<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WO_T00PR" variety="T00PR">
<name>AIR CONDITIONING SYSTEM (for Manual Air Conditioning System)</name>
<para id="RM000002LIR03VX" category="D" type-id="303F2" name-id="ACEN2-02" from="201301">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000002LIR03VX_z0" proc-id="RM22W0E___0000HB600000">
<content5 releasenbr="1">
<step1>
<ptxt>GENERAL</ptxt>
<step2>
<ptxt>The air conditioning system has the following control.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.86in"/>
<colspec colname="COLSPEC0" colwidth="1.86in"/>
<colspec colname="COL2" colwidth="3.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Diagnosis</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>A Diagnostic Trouble Code (DTC) is stored in memory when the air conditioning amplifier assembly detects a problem with the air conditioning system.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>MODE POSITION AND DAMPER OPERATION</ptxt>
<figure>
<graphic graphicname="E233993E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="center" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="center" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ PTC Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Blower with Fan Motor Sub-assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Cooler Evaporator Sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heater Radiator Unit Sub-assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Footwell Register Duct</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side Register</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Register</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Defroster</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side Defroster</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fresh Air</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Recirculation Air</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Function of Main Damper</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.30in"/>
<colspec colname="COL3" colwidth="1.51in"/>
<colspec colname="COL5" colwidth="0.85in"/>
<colspec colname="COL6" colwidth="3.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operation Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damper Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Air Inlet Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fresh</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Brings in fresh air.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Recirculation</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Recirculates internal air.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air Mix Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Max Cool to Max Hot</ptxt>
</entry>
<entry valign="middle">
<ptxt>K, L, M</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Varies the mixture ratio of cold air and hot air in order to regulate the temperature continuously from hot to cool.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Cool Air Bypass Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Max Cool to Max Hot</ptxt>
</entry>
<entry valign="middle">
<ptxt>H, I, J</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Cool air blows out from the front center register and side registers in order to adjust the temperature around the heads of the occupants during cooling or warming.</ptxt>
</entry>
</row>
<row>
<entry morerows="4" valign="middle">
<ptxt>Mode Control Damper</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E106660" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Def</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Defrosts the windshield through the center defroster and side defrosters.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106659" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot/Def</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Defrosts the windshield through the center defroster and side defrosters while air is also blown out from the footwell register duct.</ptxt>
<ptxt>In addition, air blows out slightly from the center register and side registers.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106658" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot</ptxt>
</entry>
<entry valign="middle">
<ptxt>E</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the footwell register ducts, center register and side registers.</ptxt>
<ptxt>In addition, air blows out slightly from the center defroster and side defrosters.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106657" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Bi-Level</ptxt>
</entry>
<entry valign="middle">
<ptxt>F</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the center register and side registers.</ptxt>
<ptxt>Air also blows out from the footwell register ducts.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Face</ptxt>
</entry>
<entry valign="middle">
<ptxt>G</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the center register and side registers.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>AIR OUTLETS AND AIRFLOW VOLUME</ptxt>
<figure>
<graphic graphicname="E233994E01" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<tgroup cols="6" align="center">
<colspec colname="COL1" colwidth="1.19in"/>
<colspec colname="COL2" colwidth="1.19in"/>
<colspec colname="COL5" colwidth="1.19in"/>
<colspec colname="COL6" colwidth="1.19in"/>
<colspec colname="COL7" colwidth="1.19in"/>
<colspec colname="COLSPEC0" colwidth="1.13in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" morerows="1" valign="middle">
<ptxt>Air Outlet Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Foot</ptxt>
</entry>
<entry>
<ptxt>Defroster</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>FACE</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106657" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>B/L</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry>
<graphic graphicname="E106658" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>FOOT</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106659" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>F/D</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106660" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>DEF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The size of the circle indicates the proportion of airflow volume.</ptxt>
</atten4>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>