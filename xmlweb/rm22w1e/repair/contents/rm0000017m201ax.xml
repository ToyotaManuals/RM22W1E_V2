<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S000Z" variety="S000Z">
<name>1GR-FE LUBRICATION</name>
<ttl id="12012_S000Z_7C3KH_T00DK" variety="T00DK">
<name>OIL PUMP</name>
<para id="RM0000017M201AX" category="G" type-id="3001K" name-id="LU3KS-02" from="201301">
<name>INSPECTION</name>
<subpara id="RM0000017M201AX_01" type-id="01" category="01">
<s-1 id="RM0000017M201AX_01_0001" proc-id="RM22W0E___00006XK00000">
<ptxt>INSPECT OIL PUMP RELIEF VALVE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the valve with engine oil, and then check that it falls smoothly into the valve hole by its own weight.</ptxt>
<figure>
<graphic graphicname="A223653" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If the valve does not fall smoothly, replace the timing chain cover sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017M201AX_01_0002" proc-id="RM22W0E___00006XL00000">
<ptxt>INSPECT OIL PUMP ROTOR SET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the drive and driven rotors into the timing chain cover with the marks facing upward.</ptxt>
<figure>
<graphic graphicname="A223654E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Check the rotor tip clearance.</ptxt>
<s3>
<ptxt>Using a feeler gauge, measure the clearance between the drive and driven rotor tips.</ptxt>
<figure>
<graphic graphicname="A223655" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard tip clearance</title>
<specitem>
<ptxt>0.06 to 0.16 mm (0.00236 to 0.00630 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum tip clearance</title>
<specitem>
<ptxt>0.16 mm (0.00630 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is more than the maximum, replace the timing chain cover sub-assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the rotor side clearance.</ptxt>
<s3>
<ptxt>Using a feeler gauge and steel square, measure the clearance between the rotors and precision straightedge.</ptxt>
<figure>
<graphic graphicname="A223656" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard side clearance</title>
<specitem>
<ptxt>0.030 to 0.075 mm (0.00118 to 0.00295 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum side clearance</title>
<specitem>
<ptxt>0.075 mm (0.00295 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is more than the maximum, replace the timing chain cover sub-assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the rotor body clearance.</ptxt>
<s3>
<ptxt>Using a feeler gauge, measure the clearance between the driven rotor and body.</ptxt>
<figure>
<graphic graphicname="A223657" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard body clearance</title>
<specitem>
<ptxt>0.250 to 0.325 mm (0.00984 to 0.0128 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum body clearance</title>
<specitem>
<ptxt>0.325 mm (0.0128 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is more than the maximum, replace the timing chain cover sub-assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>