<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3OC_T00HF" variety="T00HF">
<name>ACTIVE HEIGHT CONTROL SUSPENSION</name>
<para id="RM000001CTS00JX" category="C" type-id="803RC" name-id="SC0R0-06" from="201301" to="201308">
<dtccode>C1794</dtccode>
<dtcname>Height Control OFF Switch Circuit (Test Mode DTC)</dtcname>
<subpara id="RM000001CTS00JX_01" type-id="60" category="03" proc-id="RM22W0E___00009R000000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This is the height control system main switch. When the height control switch is pushed, the height control OFF indicator light lights up and height control turns off.</ptxt>
<ptxt>The off condition of the height control will not be canceled until the height control switch is pushed again or the vehicle is driven.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.07in"/>
<colspec colname="COL2" colwidth="2.80in"/>
<colspec colname="COL3" colwidth="3.21in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1794</ptxt>
</entry>
<entry valign="middle">
<ptxt>The test mode procedure is performed.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Suspension control switch (Height control OFF switch)</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001CTS00JX_02" type-id="32" category="03" proc-id="RM22W0E___00009R100000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C177227E08" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001CTS00JX_03" type-id="51" category="05" proc-id="RM22W0E___00009R200000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before performing troubleshooting, inspect the connectors of related circuits.</ptxt>
</item>
<item>
<ptxt>If the suspension control ECU or height control sensor is replaced, the vehicle height offset calibration must be performed (See page <xref label="Seep01" href="RM000003AG300EX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001CTS00JX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001CTS00JX_04_0012" proc-id="RM22W0E___00009R400000">
<testtitle>INSPECT SUSPENSION CONTROL SWITCH (HEIGHT CONTROL OFF SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the suspension control switch (See page <xref label="Seep01" href="RM000003A0H00MX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C261415E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>9 (N) - 16 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Height control OFF switch is pushed.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Height control OFF switch is not pushed.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001CTS00JX_04_0011" fin="false">OK</down>
<right ref="RM000001CTS00JX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001CTS00JX_04_0011" proc-id="RM22W0E___00009R300000">
<testtitle>CHECK HARNESS AND CONNECTOR (SUSPENSION CONTROL ECU - SWITCH AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C177225E06" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the K34 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the V4 switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.89in"/>
<colspec colname="COL2" colwidth="1.00in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt> Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K34-11 (TD) - V4-9 (N)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>K34-11 (TD) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>V4-16 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001CTS00JX_04_0009" fin="true">OK</down>
<right ref="RM000001CTS00JX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001CTS00JX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001CTS00JX_04_0009">
<testtitle>REPLACE SUSPENSION CONTROL ECU<xref label="Seep01" href="RM000003A0D00DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001CTS00JX_04_0010">
<testtitle>REPLACE SUSPENSION CONTROL SWITCH<xref label="Seep01" href="RM000003A0H00MX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>