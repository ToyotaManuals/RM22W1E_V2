<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SG_T00LJ" variety="T00LJ">
<name>REAR DOOR LOCK</name>
<para id="RM0000039H4024X" category="A" type-id="80001" name-id="DL889-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM0000039H4024X_03" type-id="11" category="10" proc-id="RM22W0E___0000E5Q00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the LH side and RH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039H4024X_01" type-id="01" category="01">
<s-1 id="RM0000039H4024X_01_0018" proc-id="RM22W0E___0000E5P00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039H4024X_01_0016" proc-id="RM22W0E___0000E5O00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039H4024X_01_0009" proc-id="RM22W0E___0000BPW00000">
<ptxt>REMOVE REAR DOOR ARMREST BASE PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a moulding remover, detach the 7 claws.</ptxt>
<figure>
<graphic graphicname="B180908" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the armrest base panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4024X_01_0010" proc-id="RM22W0E___0000BPV00000">
<ptxt>REMOVE REAR DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the rear door inside handle bezel LH.</ptxt>
<figure>
<graphic graphicname="B180907E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4024X_01_0011" proc-id="RM22W0E___0000BPZ00000">
<ptxt>REMOVE ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a moulding remover, detach the 9 claws and remove the assist grip cover LH.</ptxt>
<figure>
<graphic graphicname="B180910" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4024X_01_0012" proc-id="RM22W0E___0000E5K00000">
<ptxt>REMOVE COURTESY LIGHT ASSEMBLY (w/ Courtesy Light)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw.</ptxt>
<figure>
<graphic graphicname="E155902" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the courtesy light and then disconnect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4024X_01_0017" proc-id="RM22W0E___0000BPX00000">
<ptxt>REMOVE REAR DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B310802E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 9 clips.</ptxt>
</s2>
<s2>
<ptxt>Remove the rear inner door glass weatherstrip LH together with the rear door trim board sub-assembly LH by pulling them upward in the order shown in the illustration.</ptxt>
<atten4>
<ptxt>Make sure that the pin labeled A in the illustration is detached from the door panel.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the rear door trim board sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 cables from the rear door inside handle sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180913" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4024X_01_0013" proc-id="RM22W0E___0000E5L00000">
<ptxt>REMOVE REAR DOOR SERVICE HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, detach the 6 clamps.</ptxt>
<figure>
<graphic graphicname="B183933E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>Remove the rear door service hole cover LH.</ptxt>
<atten4>
<ptxt>Remove the remaining tape on the door.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4024X_01_0006" proc-id="RM22W0E___0000E5J00000">
<ptxt>REMOVE REAR DOOR LOCK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T30 ''TORX'' socket, remove the 3 screws and door lock.</ptxt>
<figure>
<graphic graphicname="B180926" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>Be careful when removing the screws as the door lock may fall and become damaged.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039H4024X_01_0014" proc-id="RM22W0E___0000E5M00000">
<ptxt>REMOVE REAR DOOR LOCK REMOTE CONTROL CABLE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the claw.</ptxt>
<figure>
<graphic graphicname="B180928E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the rear door lock remote control cable assembly LH.</ptxt>
<figure>
<graphic graphicname="B180929" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4024X_01_0015" proc-id="RM22W0E___0000E5N00000">
<ptxt>REMOVE REAR DOOR INSIDE LOCKING CABLE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 3 claws.</ptxt>
<figure>
<graphic graphicname="B180930E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the rear door inside locking cable assembly LH.</ptxt>
<figure>
<graphic graphicname="B188530E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Lock</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Lock</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>