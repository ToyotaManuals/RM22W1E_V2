<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SE_T00LH" variety="T00LH">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000002S8I03RX" category="C" type-id="801I2" name-id="NW3DP-03" from="201308">
<dtccode>B2325</dtccode>
<dtcname>LIN Communication Bus Malfunction</dtcname>
<subpara id="RM000002S8I03RX_01" type-id="60" category="03" proc-id="RM22W0E___0000E2700001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The main body ECU (cowl side junction block LH) intermittently monitors the LIN communication bus between the components related to the door, double lock control relay*2 and sliding roof*1. DTC B2325 is stored when a malfunction in the LIN communication bus between the components related to the doors, double lock control relay*2 and sliding roof*1 is detected consecutively 3 times.</ptxt>
<atten4>
<ptxt>*1: w/ Sliding Roof System</ptxt>
<ptxt>*2: w/ Double Locking System</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.56in"/>
<colspec colname="COL2" colwidth="2.93in"/>
<colspec colname="COL3" colwidth="2.59in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2325</ptxt>
</entry>
<entry valign="middle">
<ptxt>The main body ECU (cowl side junction block LH) detects a malfunction in the LIN communication bus between components related to the doors and sliding roof consecutively 3 times.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>for Models with jam protection function on 4 windows</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Master switch</ptxt>
</item>
<item>
<ptxt>Front power window regulator motor LH</ptxt>
</item>
<item>
<ptxt>Front power window regulator motor RH</ptxt>
</item>
<item>
<ptxt>Rear power window regulator motor LH</ptxt>
</item>
<item>
<ptxt>Rear power window regulator motor RH</ptxt>
</item>
<item>
<ptxt>Sliding roof ECU*1</ptxt>
</item>
<item>
<ptxt>Double lock door control relay*2</ptxt>
</item>
<item>
<ptxt>Main body ECU (cowl side junction block LH)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list2>
</list1>
<list1 type="unordered">
<item>
<ptxt>for Models with jam protection function on driver door window only</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Front power window regulator motor LH (for LHD)</ptxt>
</item>
<item>
<ptxt>Front power window regulator motor RH (for RHD)</ptxt>
</item>
<item>
<ptxt>Double lock door control relay*2</ptxt>
</item>
<item>
<ptxt>Main body ECU (cowl side junction block LH)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list2>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002S8I03RX_02" type-id="32" category="03" proc-id="RM22W0E___0000E2800001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E160479E04" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="E237261E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002S8I03RX_03" type-id="51" category="05" proc-id="RM22W0E___0000E2900001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot:</ptxt>
<ptxt>Connect the intelligent tester to the vehicle, and turn a courtesy switch on and off at 1.5 second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</atten3>
<atten4>
<ptxt>When DTC B2325 and a LIN communication stop DTC are output simultaneously, first perform the troubleshooting for the LIN communication stop DTC. Then perform the troubleshooting for DTC B2325.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002S8I03RX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002S8I03RX_04_0036" proc-id="RM22W0E___0000E2O00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0002" proc-id="RM22W0E___0000E2A00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (for Models with Jam Protection Function on 4 Windows)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (for Models with Jam Protection Function on Driver Door Window Only)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0003" fin="false">A</down>
<right ref="RM000002S8I03RX_04_0042" fin="false">B</right>
<right ref="RM000002S8I03RX_04_0016" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0003" proc-id="RM22W0E___0000E2B00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - MASTER SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E3 ECU connector.</ptxt>
<figure>
<graphic graphicname="E160502E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the I11*1 or I22*2 switch connector.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E3-10 (LIN2) - I11-17 (LIN1)*1 or I22-17 (LIN1)*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E3-10 (LIN2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E3-10 (LIN2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0004" fin="false">OK</down>
<right ref="RM000002S8I03RX_04_0037" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0004" proc-id="RM22W0E___0000E2C00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the E3 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the I11*1 or I22*2 switch connector.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0005" proc-id="RM22W0E___0000E2D00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0006" fin="false">A</down>
<right ref="RM000002S8I03RX_04_0017" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0006" proc-id="RM22W0E___0000E2E00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the I11*1 or I22*2 switch connector.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Disconnect the I12 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0007" proc-id="RM22W0E___0000E2F00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0008" fin="false">A</down>
<right ref="RM000002S8I03RX_04_0018" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0008" proc-id="RM22W0E___0000E2G00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the I12 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the I4 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0009" proc-id="RM22W0E___0000E2H00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0010" fin="false">A</down>
<right ref="RM000002S8I03RX_04_0019" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0010" proc-id="RM22W0E___0000E2I00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the I4 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the J4 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0011" proc-id="RM22W0E___0000E2J00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0012" fin="false">A</down>
<right ref="RM000002S8I03RX_04_0020" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0012" proc-id="RM22W0E___0000E2K00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the J4 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the J11 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0013" proc-id="RM22W0E___0000E2L00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">

<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/ Double Locking System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/o Double Locking System, w/ Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/o Double Locking System, w/o Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0014" fin="false">A</down>
<right ref="RM000002S8I03RX_04_0039" fin="false">B</right>
<right ref="RM000002S8I03RX_04_0048" fin="true">C</right>
<right ref="RM000002S8I03RX_04_0021" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0014" proc-id="RM22W0E___0000E2M00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the J11 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E86 relay connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0038" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0038" proc-id="RM22W0E___0000E2P00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/ Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/o Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0039" fin="false">A</down>
<right ref="RM000002S8I03RX_04_0049" fin="true">B</right>
<right ref="RM000002S8I03RX_04_0040" fin="true">C</right>
<right ref="RM000002S8I03RX_04_0041" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0039" proc-id="RM22W0E___0000E2Q00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the E86 relay connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the R5 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0015" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0015" proc-id="RM22W0E___0000E2N00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0024" fin="true">A</down>
<right ref="RM000002S8I03RX_04_0023" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0024">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0042" proc-id="RM22W0E___0000E2R00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - FRONT POWER WINDOW REGULATOR MOTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E3 ECU connector.</ptxt>
<figure>
<graphic graphicname="E156403E05" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the I12*1 or I4*2 motor connector.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E3-10 (LIN2) - I12-9 (LIN)*1 or I4-9 (LIN)*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0043" fin="false">OK</down>
<right ref="RM000002S8I03RX_04_0050" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0043" proc-id="RM22W0E___0000E2S00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the E3 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the I12*1 or I4*2 motor connector.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0044" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0044" proc-id="RM22W0E___0000E2T00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0045" fin="false">A</down>
<right ref="RM000002S8I03RX_04_0051" fin="true">B</right>
<right ref="RM000002S8I03RX_04_0052" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0045" proc-id="RM22W0E___0000E2U00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the I12*1 or I4*2 motor connector.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Disconnect the E86 relay connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0046" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0046" proc-id="RM22W0E___0000E2V00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I03RX_04_0047" fin="true">A</down>
<right ref="RM000002S8I03RX_04_0053" fin="true">B</right>
<right ref="RM000002S8I03RX_04_0054" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0047">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0016">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000UZ30DCX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0037">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0017">
<testtitle>REPLACE MASTER SWITCH</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0018">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR LH<xref label="Seep01" href="RM000002STX03UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0019">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR RH<xref label="Seep01" href="RM000002STX03UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0020">
<testtitle>REPLACE REAR POWER WINDOW REGULATOR MOTOR LH<xref label="Seep01" href="RM000002SU0027X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0048">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0021">
<testtitle>REPLACE REAR POWER WINDOW REGULATOR MOTOR RH<xref label="Seep01" href="RM000002STX03UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0049">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0040">
<testtitle>REPLACE DOUBLE LOCK DOOR CONTROL RELAY<xref label="Seep01" href="RM000003CM800DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0041">
<testtitle>REPLACE DOUBLE LOCK DOOR CONTROL RELAY<xref label="Seep01" href="RM000003CM500NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0023">
<testtitle>REPLACE SLIDING ROOF DRIVE GEAR SUB-ASSEMBLY<xref label="Seep01" href="RM000002LPB033X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0050">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0051">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR LH<xref label="Seep01" href="RM000002STX03UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0052">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR RH<xref label="Seep01" href="RM000002STX03UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0053">
<testtitle>REPLACE DOUBLE LOCK DOOR CONTROL RELAY<xref label="Seep01" href="RM000003CM800DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I03RX_04_0054">
<testtitle>REPLACE DOUBLE LOCK DOOR CONTROL RELAY<xref label="Seep01" href="RM000003CM500NX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>