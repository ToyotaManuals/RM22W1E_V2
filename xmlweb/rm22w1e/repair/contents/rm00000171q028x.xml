<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001M" variety="S001M">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001M_7C3PO_T00IR" variety="T00IR">
<name>HYDRAULIC BRAKE BOOSTER (for LHD)</name>
<para id="RM00000171Q028X" category="A" type-id="30014" name-id="BR5TH-02" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM00000171Q028X_01" type-id="01" category="01">
<s-1 id="RM00000171Q028X_01_0041" proc-id="RM22W0E___0000AUE00000">
<ptxt>INSTALL BRAKE BOOSTER GASKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new brake booster gasket to the hydraulic brake booster.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171Q028X_01_0042" proc-id="RM22W0E___0000AUF00000">
<ptxt>INSTALL HYDRAULIC BRAKE BOOSTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the hydraulic brake booster assembly with the 4 nuts.</ptxt>
<figure>
<graphic graphicname="C177106E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>145</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 4 brake lines to the correct positions of the hydraulic brake booster assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="F051200E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a union nut wrench, tighten the 4 brake lines.</ptxt>
<figure>
<graphic graphicname="C172201E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<subtitle>without union nut wrench</subtitle>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
<subtitle>with union nut wrench</subtitle>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>145</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use a torque wrench with a fulcrum length of 300 mm (11.8 in.).</ptxt>
</item>
<item>
<ptxt>The torque value for use with a union nut wrench is effective when the union nut wrench is parallel to the torque wrench.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Connect the 3 connectors to the hydraulic brake booster.</ptxt>
<figure>
<graphic graphicname="C172202" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171Q028X_01_0066" proc-id="RM22W0E___0000AT100000">
<ptxt>INSTALL PUSH ROD PIN
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of lithium soap base glycol grease to the inner surface of the hole on the brake pedal lever.</ptxt>
</s2>
<s2>
<ptxt>Set the master cylinder push rod clevis in place, insert the push rod pin from the outside of the vehicle and then install a new clip.</ptxt>
<figure>
<graphic graphicname="C177105E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q028X_01_0062" proc-id="RM22W0E___0000AT300000">
<ptxt>INSTALL DRIVER SIDE KNEE AIRBAG ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B189604E01" width="2.775699831in" height="6.791605969in"/>
</figure>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the driver side knee airbag with the 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q028X_01_0063" proc-id="RM22W0E___0000A9I00000">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Attach the 2 claws to install the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to connect the 2 control cables.</ptxt>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 16 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 9 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B182569" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to close the hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q028X_01_0065" proc-id="RM22W0E___0000A9J00000">
<ptxt>INSTALL COWL SIDE TRIM BOARD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181911" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 clips to install the cowl side trim board.</ptxt>
</s2>
<s2>
<ptxt>Install the cap nut.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q028X_01_0064" proc-id="RM22W0E___000014600000">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182553" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 3 claws to install the No. 1 instrument panel under cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q028X_01_0067" proc-id="RM22W0E___0000AT400000">
<ptxt>INSTALL FRONT DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181682" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 7 claws and 4 clips to install the scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q028X_01_0050" proc-id="RM22W0E___0000AUG00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000171Q028X_01_0068" proc-id="RM22W0E___0000ASR00000">
<ptxt>BLEED BRAKE SYSTEM
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>If air is bled without using the GTS, damage or accidents may result. Therefore, always use the GTS when bleeding air.</ptxt>
</atten2>
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</s2>
<s2>
<ptxt>Remove the brake master cylinder reservoir filler cap assembly.</ptxt>
</s2>
<s2>
<ptxt>Add brake fluid until the fluid level is between the MIN and MAX lines of the reservoir.</ptxt>
</s2>
<s2>
<ptxt>Repeatedly depress the brake pedal and bleed air from the bleeder plug of the front disc brake cylinder RH.</ptxt>
</s2>
<s2>
<ptxt>Repeat the step above until the air is completely bled, and then tighten the bleeder plug while depressing the brake pedal.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed the air from the bleeder plug of the front disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>With the brake pedal depressed, loosen the bleeder plug of the rear disc brake cylinder RH, continue to hold the brake pedal and allow brake fluid to be drained from the bleeder plug while the pump motor operates.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Air is bled as the pump motor operates while the brake pedal is being depressed.</ptxt>
</item>
<item>
<ptxt>Be sure to release the brake pedal to stop the motor after approximately 100 seconds of continuous operation.</ptxt>
</item>
<item>
<ptxt>As brake fluid is continuously drained while the pump operates, it is not necessary to repeatedly depress the brake pedal.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>When there is no more air in the brake fluid, tighten the bleeder plug, and then release the brake pedal.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed the air from the bleeder plug of the rear disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch off and connect the GTS to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</s2>
<s2>
<ptxt>Turn the GTS on.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Utility / Air Bleeding.</ptxt>
<atten3>
<ptxt>To protect the solenoid from overheating, the solenoid operation stops automatically in 4 seconds, and then the solenoid will not respond to commands for an additional 20 seconds.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Repeatedly depress the brake pedal several times, and then, with the brake pedal depressed, turn FR on and bleed air.</ptxt>
<atten4>
<ptxt>Air returns to the brake master cylinder reservoir together with the brake fluid and is bled from the brake system.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>As it is not possible to visually confirm that air is being bled, repeat this step 10 times.</ptxt>
</item>
<item>
<ptxt>Do not loosen the bleeder plug.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Turn FL on and bleed air using the same procedures as for FR.</ptxt>
</s2>
<s2>
<ptxt>Turn RR on, loosen the bleeder plug of the rear disc brake cylinder RH and drain brake fluid.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Do not depress the brake pedal.</ptxt>
</item>
<item>
<ptxt>As brake fluid is automatically drained while the pump and solenoid operate, it is not necessary to operate the brake pedal.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Repeat the step above until the air is completely bled, and then tighten the bleeder plug.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Turn FL Line on and bleed the air from the bleeder plug of the front disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>Turn RR Line on and loosen the bleeder plug of the rear disc brake cylinder RH.</ptxt>
<atten4>
<ptxt>Be sure to bleed air with the brake pedal depressed.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Repeat the step above until the air is completely bled, and then tighten the bleeder plug.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Turn RL Line on and bleed air from the bleeder plug of the rear disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>Turn the GTS off and turn the ignition switch off.</ptxt>
</s2>
<s2>
<ptxt>Inspect for brake fluid leaks.</ptxt>
</s2>
<s2>
<ptxt>Check and adjust the brake fluid level (See page <xref label="Seep01" href="RM0000012XQ03DX"/>).</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM0000046KV00MX"/>).</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q028X_01_0057" proc-id="RM22W0E___0000ASX00000">
<ptxt>CHECK BRAKE PEDAL HEIGHT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the brake pedal height.</ptxt>
<figure>
<graphic graphicname="C222463E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Pedal Height from No. 2 Dash Panel Insulator Pad</title>
<specitem>
<ptxt>147.1 to 157.1 mm (5.80 to 6.18 in.)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not adjust the pedal height. Doing so by changing the push rod length will structurally change the pedal ratio.</ptxt>
</atten3>
<ptxt>If the pedal height is incorrect, adjust the rod operating adapter length.</ptxt>
</s2>
<s2>
<ptxt>Adjust the rod operating adapter length.</ptxt>
<figure>
<graphic graphicname="C172191E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Remove the clip and clevis pin.</ptxt>
</s3>
<s3>
<ptxt>Loosen the clevis lock nut.</ptxt>
</s3>
<s3>
<ptxt>Adjust the rod operating adapter length by turning the pedal push rod clevis.</ptxt>
<spec>
<title>Standard Rod Operating Adapter Length "A"</title>
<specitem>
<ptxt>201.7 to 202.7 mm (7.94 to 7.98 in.)</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Tighten the clevis lock nut.</ptxt>
<torque>
<torqueitem>
<t-value1>26</t-value1>
<t-value2>260</t-value2>
<t-value4>19</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Set the master cylinder push rod clevis in place, insert the push rod pin from the outside of the vehicle and then install a new clip. </ptxt>
<ptxt>If the pedal height is incorrect even if the rod operating adapter is adjusted, check that there is no damage in the brake pedal, brake pedal lever, brake pedal support and dash panel.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Even if there is damage, there is no problem if the reserve distance is within the standard value.</ptxt>
</item>
<item>
<ptxt>If necessary, replace any damaged parts.</ptxt>
</item>
</list1>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q028X_01_0058" proc-id="RM22W0E___0000ASW00000">
<ptxt>CHECK PEDAL FREE PLAY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the pedal until the beginning of the resistance is felt. Measure the pedal free play.</ptxt>
<figure>
<graphic graphicname="C172192E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Pedal Free Play</title>
<specitem>
<ptxt>1 to 6 mm (0.0394 to 0.236 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q028X_01_0059" proc-id="RM22W0E___0000ASZ00000">
<ptxt>CHECK PEDAL RESERVE DISTANCE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Release the parking brake lever. With the engine running, depress the pedal and measure the pedal reserve distance as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C222462E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Pedal Reserve Distance from No. 2 Dash Panel Insulator Pad at 490 N (50 kgf, 110.2 lbf)</title>
<specitem>
<ptxt>More than 67 mm (2.64 in.)</ptxt>
</specitem>
</spec>
<ptxt>If incorrect, troubleshoot the brake system.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q028X_01_0040" proc-id="RM22W0E___0000AUD00000">
<ptxt>INSPECT BRAKE MASTER CYLINDER OPERATION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the brake master cylinder operation (See page <xref label="Seep01" href="RM00000171W029X_01_0004"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>