<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3XF_T00QI" variety="T00QI">
<name>BACK DOOR GLASS (for Double Swing Out Type)</name>
<para id="RM000004XI9001X" category="A" type-id="80001" name-id="WS6GT-01" from="201301">
<name>REMOVAL</name>
<subpara id="RM000004XI9001X_01" type-id="01" category="01">
<s-1 id="RM000004XI9001X_01_0012">
<ptxt>REMOVE SPARE TIRE (w/ Tire Carrier)</ptxt>
</s-1>
<s-1 id="RM000004XI9001X_01_0013" proc-id="RM22W0E___0000I7900000">
<ptxt>REMOVE SPARE WHEEL CARRIER HINGE BRACKET (w/ Tire Carrier)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the spare wheel carrier hinge bracket.</ptxt>
<figure>
<graphic graphicname="B292610" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0014" proc-id="RM22W0E___0000I7A00000">
<ptxt>REMOVE SPARE WHEEL COVER SUB-ASSEMBLY (w/ Tire Carrier)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts.</ptxt>
<figure>
<graphic graphicname="B292611" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the clip and remove the spare wheel cover.</ptxt>
<figure>
<graphic graphicname="B292612" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0015" proc-id="RM22W0E___0000I7B00000">
<ptxt>REMOVE SPARE WHEEL CARRIER SUB-ASSEMBLY (w/ Tire Carrier)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and spare tire.</ptxt>
</s2>
<s2>
<ptxt>Remove the 9 bolts and spare wheel carrier.</ptxt>
<figure>
<graphic graphicname="B292613" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0004" proc-id="RM22W0E___0000EBG00000">
<ptxt>REMOVE ASSIST GRIP LH (for LH Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the 2 covers.</ptxt>
<figure>
<graphic graphicname="B291735E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 2 bolts and assist grip.</ptxt>
<figure>
<graphic graphicname="B291736" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0005" proc-id="RM22W0E___0000EBH00000">
<ptxt>REMOVE DOOR INSIDE HANDLE BEZEL LH (for LH Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw and open the cover.</ptxt>
<figure>
<graphic graphicname="B295796" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw.</ptxt>
<figure>
<graphic graphicname="B291737" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws.</ptxt>
<figure>
<graphic graphicname="B291738E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the guide and remove the door inside handle bezel in the direction indicated by the arrow in the illustration.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0006" proc-id="RM22W0E___0000EBI00000">
<ptxt>REMOVE BACK DOOR TRIM PANEL ASSEMBLY LH (for LH Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 10 clips and remove the back door trim panel.</ptxt>
<figure>
<graphic graphicname="B291739" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0007" proc-id="RM22W0E___0000I7400000">
<ptxt>REMOVE ASSIST GRIP RH (for RH Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the 2 covers.</ptxt>
<figure>
<graphic graphicname="B291761E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 2 bolts and assist grip.</ptxt>
<figure>
<graphic graphicname="B291762" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0008" proc-id="RM22W0E___0000I7500000">
<ptxt>REMOVE BACK DOOR TRIM COVER RH (for RH Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 5 clips and 2 claws.</ptxt>
<figure>
<graphic graphicname="B291760" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the clamp and 2 claws and remove the back door trim cover.</ptxt>
<figure>
<graphic graphicname="B291796" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0009" proc-id="RM22W0E___0000I7600000">
<ptxt>REMOVE BACK DOOR TRIM PANEL ASSEMBLY RH (for RH Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 13 clips and remove the back door trim panel.</ptxt>
<figure>
<graphic graphicname="B291763" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0010" proc-id="RM22W0E___0000I7700000">
<ptxt>REMOVE REAR WIPER ARM AND BLADE ASSEMBLY (w/ Rear Wiper)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Open the wiper arm head cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and remove the rear wiper arm and blade assembly.</ptxt>
<figure>
<graphic graphicname="B283521" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0011" proc-id="RM22W0E___0000I7800000">
<ptxt>REMOVE BACK DOOR CENTER WEATHERSTRIP (for RH Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 5 clips and remove the back door center weatherstrip.</ptxt>
<figure>
<graphic graphicname="B291773" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XI9001X_01_0001" proc-id="RM22W0E___0000I7100000">
<ptxt>REMOVE BACK DOOR GLASS LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply protective tape to the outer surface of the vehicle body to prevent scratches.</ptxt>
<figure>
<graphic graphicname="B293866E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When separating the back door glass from the vehicle, be careful not to damage the vehicle paint or interior/exterior ornaments.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Disconnect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Place matchmarks on the glass and vehicle body at the locations indicated in the illustration.</ptxt>
<figure>
<graphic graphicname="B294129E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Matchmarks do not need to be placed if the glass is not going to be reused.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>From the interior, insert a piano wire between the vehicle body and back door glass as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B293868E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure not to damage the wire harness.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Piano Wire</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tie objects that can serve as handles (for example, wooden blocks) to both wire ends.</ptxt>
</s2>
<s2>
<ptxt>Cut through the adhesive by pulling the piano wire around the back door glass.</ptxt>
<atten3>
<ptxt>Leave as much adhesive on the vehicle body as possible when removing the back door glass.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using suction cups, remove the back door glass.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XI9001X_01_0002" proc-id="RM22W0E___0000I7200000">
<ptxt>REMOVE BACK DOOR GLASS RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply protective tape to the outer surface of the vehicle body to prevent scratches.</ptxt>
<figure>
<graphic graphicname="B293867E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When separating the back door glass from the vehicle, be careful not to damage the vehicle paint or interior/exterior ornaments.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Disconnect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Place matchmarks on the glass and vehicle body at the locations indicated in the illustration.</ptxt>
<figure>
<graphic graphicname="B294130E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Matchmarks do not need to be placed if the glass is not going to be reused.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>From the interior, insert a piano wire between the vehicle body and back door glass as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B293869E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure not to damage the wire harness.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Piano Wire</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tie objects that can serve as handles (for example, wooden blocks) to both wire ends.</ptxt>
</s2>
<s2>
<ptxt>Cut through the adhesive by pulling the piano wire around the back door glass.</ptxt>
<atten3>
<ptxt>Leave as much adhesive on the vehicle body as possible when removing the back door glass.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using suction cups, remove the back door glass.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XI9001X_01_0003" proc-id="RM22W0E___0000I7300000">
<ptxt>CLEAN VEHICLE BODY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean and shape the contact surface of the vehicle body.</ptxt>
<figure>
<graphic graphicname="B106081E18" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>On the contact surface of the vehicle body, use a knife to cut away excess adhesive as shown in the illustration.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
<atten4>
<ptxt>Leave as much adhesive on the vehicle body as possible.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Adhesive</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Vehicle Body</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>