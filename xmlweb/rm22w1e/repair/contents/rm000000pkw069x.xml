<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZC_T00SF" variety="T00SF">
<name>LIGHTING SYSTEM</name>
<para id="RM000000PKW069X" category="U" type-id="303FP" name-id="LI041-71" from="201301" to="201308">
<name>FAIL-SAFE CHART</name>
<subpara id="RM000000PKW069X_z0" proc-id="RM22W0E___0000J7Y00000">
<content5 releasenbr="1">
<step1>
<ptxt>Automatic Headlight Beam Level Control System (w/ Static Headlight Auto Leveling)</ptxt>
<step2>
<ptxt>The headlight leveling ECU operates in fail-safe mode if an abnormal condition such as those listed below has been detected.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Abnormality Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Return Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Warning Indicator</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Height control sensor power source voltage</ptxt>
</entry>
<entry valign="middle">
<ptxt>Abnormal height control sensor power source voltage (4.6 V or less, or 6.25 V or more) is detected.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Voltage of 4.61 to 6.24 V is detected</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Illuminated</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Height control sensor signal voltage</ptxt>
</entry>
<entry valign="middle">
<ptxt>Abnormal height control sensor signal voltage (0.25 V or less, or 4.75 V or more) is detected.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Voltage of 0.26 to 4.74 V is detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>High power source voltage</ptxt>
</entry>
<entry valign="middle">
<ptxt>Abnormal high power source voltage (18.5 V or more) is detected.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Voltage of 17.5 V or less is detected</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not illuminated</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Low power source voltage</ptxt>
</entry>
<entry valign="middle">
<ptxt>Abnormal low power source voltage (9.0 V or less) is detected.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Voltage of 9.3 V or more is detected</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not illuminated</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Abnormal height control sensor signal while initialization</ptxt>
</entry>
<entry valign="middle">
<ptxt>Initialization is performed outside the valid range of the voltage for initialization.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Initialization is successfully completed within the valid range of the voltage for initialization.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Illuminated</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>Automatic Headlight Beam Level Control System (w/ Dynamic Headlight Auto Leveling)</ptxt>
<ptxt>If the headlight swivel ECU detects a malfunction in the automatic headlight beam level control system , it will take the actions indicated in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Malfunctioning Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-safe Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Warning Indicator</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Speed Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Speed sensor signal malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Continues control.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Illuminated</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Rear Height Control Sensor LH (w/o Active Height Control Suspension)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear height control sensor LH signal malfunction</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Stops operation after returning the headlight leveling motor to the initial position (system fails when the headlight leveling motor is higher than the initial position).</ptxt>
</item>
<item>
<ptxt>Stops operation with the headlight leveling motor at the current position (system fails when the headlight leveling motor is lower than the initial position).</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Illuminated</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Source voltage malfunction</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Height Control Sensor Signal (w/ Active Height Control Suspension)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Height control sensor signal malfunction</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Stops operation after returning the headlight leveling motor to the initial position (system fails when the headlight leveling motor is higher than the initial position).</ptxt>
</item>
<item>
<ptxt>Stops operation with the headlight leveling motor at the current position (system fails when the headlight leveling motor is lower than the initial position).</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Illuminated</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Headlight Leveling Motor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight leveling motor communication error</ptxt>
</entry>
<entry valign="middle">
<ptxt>Abnormal side headlight leveling motor:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stops operation after returning the headlight leveling motor to the initial position (system fails when the headlight leveling motor is higher than the initial position).</ptxt>
</item>
<item>
<ptxt>Stops operation with the headlight leveling motor at the current position (system fails when the headlight leveling motor is lower than the initial position).</ptxt>
</item>
</list1>
<ptxt>Normal side headlight leveling motor:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stops operation after returning the headlight leveling motor to the initial position (system fails when the headlight leveling motor is higher than the initial position).</ptxt>
</item>
<item>
<ptxt>Stops operation with the headlight leveling motor at the current position (system fails when the headlight leveling motor is lower than the initial position).</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Illuminated</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Headlight leveling motor malfunction</ptxt>
<list1 type="unordered">
<item>
<ptxt>Open in circuit</ptxt>
</item>
<item>
<ptxt>Short in circuit</ptxt>
</item>
<item>
<ptxt>	Overheated</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Abnormal side headlight leveling motor:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stops at the current position.</ptxt>
</item>
</list1>
<ptxt>Normal side headlight leveling motor:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stops operation after returning the headlight leveling motor to the initial position (system fails when the headlight leveling motor is higher than the initial position).</ptxt>
</item>
<item>
<ptxt>Stops operation with the headlight leveling motor at the current position (system fails when the headlight leveling motor is lower than the initial position).</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Headlight Leveling Motor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Voltage malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Abnormal side headlight leveling motor:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stops at the current position.</ptxt>
</item>
</list1>
<ptxt>Normal side headlight leveling motor:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stops operation after returning the headlight leveling motor to the initial position (system fails when the headlight leveling motor is higher than the initial position).</ptxt>
</item>
<item>
<ptxt>Stops operation with the headlight leveling motor at the current position (system fails when the headlight leveling motor is lower than the initial position).</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Not illuminated</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle">
<ptxt>CAN Communication System</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle specifications unconfirmed</ptxt>
<ptxt>(Main body ECU)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Continues control.</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Illuminated</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Communication signal malfunction</ptxt>
<ptxt>(Main body ECU)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Communication signal malfunction</ptxt>
<ptxt>(Skid control ECU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Judges that the vehicle speed is 0 km/h (0 mph) and continues control.</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Headlight Swivel ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle specifications unconfirmed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Continues control.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Illuminated</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear height control sensor LH initialization unconfirmed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Both headlight leveling motors:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stop at the same position as when the vehicle is empty.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Blinks 6 times at 2 Hz after the ignition switch is turned from off to ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>