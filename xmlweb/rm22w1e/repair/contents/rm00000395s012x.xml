<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3V0_T00O3" variety="T00O3">
<name>REAR SEAT CUSHION HEATER (for 60/40 Split Seat Type 40 Side)</name>
<para id="RM00000395S012X" category="A" type-id="30014" name-id="SE6SV-04" from="201308">
<name>INSTALLATION</name>
<subpara id="RM00000395S012X_02" type-id="11" category="10" proc-id="RM22W0E___0000G8L00001">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000395S012X_01" type-id="01" category="01">
<s-1 id="RM00000395S012X_01_0001" proc-id="RM22W0E___0000G8J00001">
<ptxt>INSTALL REAR SEAT CUSHION HEATER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the seat cushion heater with the name stamp side facing the seat cushion cover.</ptxt>
</s2>
<s2>
<ptxt>Install the seat cushion heater with new tack pins.</ptxt>
<figure>
<graphic graphicname="B181209E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000395S012X_01_0018" proc-id="RM22W0E___0000FXW00001">
<ptxt>INSTALL REAR SEPARATE TYPE SEAT CUSHION COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182663E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using hog ring pliers, install the seat cushion cover to the seat cushion pad with new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Attach the 3 pieces of fastening tape.</ptxt>
<figure>
<graphic graphicname="B181184E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0019" proc-id="RM22W0E___0000FXX00001">
<ptxt>INSTALL SEAT CUSHION COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seat cushion cover with pad.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Pass the side airbag wire harness through the hole in the seat cushion.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<s3>
<ptxt>Pass the seatback heater wire harness through the hole in the seat cushion.</ptxt>
<figure>
<graphic graphicname="B184059" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Connect the 2 seat heater connectors.</ptxt>
<figure>
<graphic graphicname="B184061" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the side airbag wire harness clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0020" proc-id="RM22W0E___0000FXZ00001">
<ptxt>INSTALL REAR SEAT UNDER TRAY COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181181" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0021" proc-id="RM22W0E___0000FY100001">
<ptxt>INSTALL REAR UNDER SIDE COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181178" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws and clip to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 5 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0022" proc-id="RM22W0E___0000FY200001">
<ptxt>INSTALL REAR SEAT COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181179" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Attach the 12 claws.</ptxt>
<figure>
<graphic graphicname="B181177" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0023" proc-id="RM22W0E___0000FY300001">
<ptxt>INSTALL REAR NO. 1 SEAT HINGE SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the cable clamp to connect the cable. </ptxt>
<figure>
<graphic graphicname="B181176" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the hinge with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B181175" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0024" proc-id="RM22W0E___0000FY400001">
<ptxt>INSTALL REAR NO. 1 SEAT HINGE SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181174" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the hinge with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0025" proc-id="RM22W0E___0000FY500001">
<ptxt>INSTALL REAR NO. 2 SEAT PROTECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the wire harness, and attach the 2 claws to close the protector.</ptxt>
</s2>
<s2>
<ptxt>Attach the claw to install the protector to the seat hinge.</ptxt>
<figure>
<graphic graphicname="B184060" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the claw to install the wire harness connector.</ptxt>
<figure>
<graphic graphicname="B182658" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Attach the claw to install the wire harness connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0026" proc-id="RM22W0E___0000FY900001">
<ptxt>INSTALL REAR NO. 2 SEAT LEG SIDE COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Pass the fold seat stopper band through the seat leg side cover and attach it to the seat cushion frame with the bolt.</ptxt>
<figure>
<graphic graphicname="B181171" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 3 claws to install the seat leg side cover.</ptxt>
<figure>
<graphic graphicname="B181170" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 4 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0027" proc-id="RM22W0E___0000FYB00001">
<ptxt>INSTALL UPPER SEAT TRACK RAIL COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182664" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0028" proc-id="RM22W0E___0000FYC00001">
<ptxt>INSTALL UPPER SEAT TRACK RAIL COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182665" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0029" proc-id="RM22W0E___0000FYD00001">
<ptxt>INSTALL REAR NO. 1 SEAT RECLINING COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181166" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0030" proc-id="RM22W0E___0000FYE00001">
<ptxt>INSTALL REAR NO. 1 SEAT RECLINING COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182666E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws in the order shown in the illustration to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0031" proc-id="RM22W0E___0000FYF00001">
<ptxt>INSTALL RECLINING ADJUSTER RELEASE HANDLE RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181164" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the release handle with the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0032" proc-id="RM22W0E___0000FYG00001">
<ptxt>INSTALL SEAT ADJUSTER BOLT COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181163" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395S012X_01_0017" proc-id="RM22W0E___0000G8K00001">
<ptxt>INSTALL REAR NO. 1 SEAT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear No. 1 seat assembly (See page <xref label="Seep01" href="RM00000391200SX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>