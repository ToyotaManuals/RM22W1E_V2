<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000PFY0S3X" category="C" type-id="302IK" name-id="ESUB7-06" from="201301" to="201308">
<dtccode>P2120</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" Circuit</dtcname>
<dtccode>P2122</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" Circuit Low Input</dtcname>
<dtccode>P2123</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" Circuit High Input</dtcname>
<dtccode>P2125</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "E" Circuit</dtcname>
<dtccode>P2127</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "E" Circuit Low Input</dtcname>
<dtccode>P2128</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "E" Circuit High Input</dtcname>
<dtccode>P2138</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" / "E" Voltage Correlation</dtcname>
<subpara id="RM000000PFY0S3X_11" type-id="11" category="10">
<name>CAUTION / NOTICE / HINT</name>
</subpara>
<subpara id="RM000000PFY0S3X_01" type-id="60" category="03" proc-id="RM22W0E___00001HO00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The accelerator pedal position sensor assembly is mounted on the accelerator pedal bracket and has 2 sensor circuits: VPA (main) and VPA2 (sub). This sensor is a non-contact type and uses Hall-effect elements in order to yield accurate signals even in extreme driving conditions, such as at high speeds as well as very low speeds. The voltage, which is applied to terminals VPA and VPA2 of the ECM, varies between 0.5 V and 4.75 V in proportion to the position of the accelerator pedal (throttle valve). The signal from VPA indicates the actual accelerator pedal position (throttle valve opening angle) and is used for engine control. The signal from VPA2 conveys the status of the VPA circuit and is used to check the accelerator pedal position sensor itself.</ptxt>
<ptxt>The ECM monitors the actual accelerator pedal position (throttle valve opening angle) through the signals from VPA and VPA2, and controls the throttle actuator according to these signals.</ptxt>
<atten4>
<ptxt>This ETCS (Electronic Throttle Control System) does not use a throttle cable.</ptxt>
</atten4>
<figure>
<graphic graphicname="A112620E61" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2120</ptxt>
</entry>
<entry valign="middle">
<ptxt>VPA fluctuates rapidly beyond the upper and lower malfunction thresholds for 0.5 seconds or more (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor assembly</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2122</ptxt>
</entry>
<entry valign="middle">
<ptxt>VPA is 0.4 V or less for 0.5 seconds or more when the accelerator pedal is fully released (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor assembly</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Open in VCP1 circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Open or ground short in VPA circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2123</ptxt>
</entry>
<entry valign="middle">
<ptxt>VPA is 4.8 V or higher for 2.0 seconds or more (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor assembly</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Open in EPA circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2125</ptxt>
</entry>
<entry valign="middle">
<ptxt>VPA2 fluctuates rapidly beyond the upper and lower malfunction thresholds for 0.5 seconds or more (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor assembly</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2127</ptxt>
</entry>
<entry valign="middle">
<ptxt>VPA2 is 1.2 V or less for 0.5 seconds or more when the accelerator pedal is fully released (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor assembly</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Open in VCP2 circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Open or ground short in VPA2 circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2128</ptxt>
</entry>
<entry valign="middle">
<ptxt>Conditions (a) and (b) continue for 2.0 seconds or more (1 trip detection logic):</ptxt>
<ptxt>(a) VPA2 is 4.8 V or higher.</ptxt>
<ptxt>(b) VPA is between 0.4 V and 3.45 V.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor assembly</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Open in EPA2 circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2138</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition (a) or (b) continues for 2.0 seconds or more (1 trip detection logic):</ptxt>
<ptxt>(a) Difference between VPA and VPA2 is 0.02 V or less.</ptxt>
<ptxt>(b) VPA is 0.4 V or less and VPA2 is 1.2 V or less.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short between VPA and VPA2 circuits</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor assembly</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When any of these DTCs are stored, check the accelerator pedal position sensor voltage by entering the following menus: Powertrain / Engine and ECT / Data List / All Data / Accel Sensor Out No. 1 and Accel Sensor Out No. 2.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="2.27in"/>
<colspec colname="COL2" colwidth="1.20in"/>
<colspec colname="COL3" colwidth="1.20in"/>
<colspec colname="COL4" colwidth="1.20in"/>
<colspec colname="COL5" colwidth="1.21in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accel Sensor Out No. 1</ptxt>
<ptxt>When accelerator pedal Released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accel Sensor Out No. 2</ptxt>
<ptxt>When accelerator pedal Released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accel Sensor Out No. 1</ptxt>
<ptxt>When accelerator pedal Depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accel Sensor Out No. 2</ptxt>
<ptxt>When accelerator pedal Depressed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>VCP circuit open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Open or ground short in VPA circuit</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.2 to 2.0 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.4 to 4.7 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Open or ground short in VPA2 circuit</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.5 to 1.1 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.6 to 4.5 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>EPA circuit open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Normal condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.5 to 1.1 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.2 to 2.0 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.6 to 4.5 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.4 to 4.75 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
<atten4>
<ptxt>Accelerator pedal positions are expressed as voltages.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFY0S3X_02" type-id="64" category="03" proc-id="RM22W0E___00001HP00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the output voltage of either VPA or VPA2 deviates from the standard range, or the difference between the output voltages of the 2 sensor circuits is less than the threshold, the ECM determines that there is a malfunction in the accelerator pedal position sensor. The ECM then illuminates the MIL and stores a DTC.</ptxt>
<ptxt>Example:</ptxt>
<ptxt>When the output voltage of VPA drops below 0.4 V for more than 0.5 seconds when the accelerator pedal is fully depressed, DTC P2122 is stored.</ptxt>
<ptxt>If the malfunction is not repaired successfully, a DTC is stored 2 seconds after the engine is next started.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFY0S3X_12" type-id="73" category="03" proc-id="RM22W0E___00001HY00000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A199317E14" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on [A].</ptxt>
</item>
<item>
<ptxt>Fully depress and release the accelerator pedal [B].</ptxt>
</item>
<item>
<ptxt>Check that 5 seconds or more have elapsed since the engine switch was turned on (IG).</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes [C].</ptxt>
</item>
<item>
<ptxt>Read the pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a pending DTC is output, the system is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If a pending DTC is not output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P2120, P2122, P2123, P2125, P2127, P2128 or P2138.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows INCOMPLETE or N/A, perform steps [B] through [C] again.</ptxt>
</item>
</list1>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000PFY0S3X_07" type-id="62" category="03" proc-id="RM22W0E___00001HQ00000">
<name>FAIL-SAFE</name>
<content5 releasenbr="1">
<ptxt>When DTC P2120, P2121, P2122, P2123, P2125, P2127, P2128 or P2138 is stored, the ECM enters fail-safe mode. If either of the 2 sensor circuits malfunctions, the ECM uses the remaining circuit to calculate the accelerator pedal position to allow the vehicle to continue driving. If both of the circuits malfunction, the ECM regards the accelerator pedal as being released. As a result, the throttle valve is closed and the engine idles.</ptxt>
<ptxt>The ECM continues operating in fail-safe mode until a pass condition is detected, and the engine switch is then turned off.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFY0S3X_08" type-id="32" category="03" proc-id="RM22W0E___00001HR00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A162855E15" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000PFY0S3X_09" type-id="51" category="05" proc-id="RM22W0E___00001HS00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>These DTCs relate to the accelerator pedal position sensor.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFY0S3X_10" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PFY0S3X_10_0001" proc-id="RM22W0E___00001HT00000">
<testtitle>READ VALUE USING GTS (ACCELERATOR PEDAL POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A208621E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Accel Sensor Out No. 1 and Accel Sensor Out No. 2.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the GTS.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.04in"/>
<colspec colname="COL2" colwidth="1.04in"/>
<colspec colname="COL3" colwidth="1.02in"/>
<colspec colname="COLSPEC1" colwidth="1.03in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Accelerator Pedal Operation</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sensor Out No. 1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sensor Out No. 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Difference between Accel Sensor Out No. 1 and Accel Sensor Out No. 2</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Released</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.5 to 1.1 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.2 to 2.0 V</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>More than 0.02 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.6 to 4.5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.4 to 4.75 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Depressed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Released</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<right ref="RM000000PFY0S3X_10_0005" fin="false">OK</right>
<right ref="RM000000PFY0S3X_10_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFY0S3X_10_0002" proc-id="RM22W0E___00001HU00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ACCELERATOR PEDAL POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the accelerator pedal position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A31-6 (VPA) - A52-55 (VPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-5 (EPA) - A52-58 (EPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-4 (VCPA) - A52-57 (VCPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-3 (VPA2) - A52-56 (VPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-2 (EPA2) - A52-60 (EPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-1 (VCP2) - A52-59 (VCP2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-6 (VPA) or A52-55 (VPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-5 (EPA) or A52-58 (EPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-4 (VCPA) or A52-57 (VCPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-3 (VPA2) or A52-56 (VPA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-2 (EPA2) or A52-60 (EPA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-1 (VCP2) or A52-59 (VCP2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A31-6 (VPA) - A38-55 (VPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-5 (EPA) - A38-58 (EPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-4 (VCPA) - A38-57 (VCPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-3 (VPA2) - A38-56 (VPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-2 (EPA2) - A38-60 (EPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-1 (VCP2) - A38-59 (VCP2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-6 (VPA) or A38-55 (VPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-5 (EPA) or A38-58 (EPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-4 (VCPA) or A38-57 (VCPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-3 (VPA2) or A38-56 (VPA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-2 (EPA2) or A38-60 (EPA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-1 (VCP2) or A38-59 (VCP2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PFY0S3X_10_0003" fin="false">OK</down>
<right ref="RM000000PFY0S3X_10_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFY0S3X_10_0003" proc-id="RM22W0E___00001HV00000">
<testtitle>INSPECT ECM (VCPA AND VCP2 VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A276702E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the accelerator pedal position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry>
<ptxt>Tester Connection</ptxt>
</entry>
<entry>
<ptxt>Switch Condition</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A31-4 (VCPA) - A31-5 (EPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-1 (VCP2) - A31-2 (EPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Accelerator Pedal Position Sensor Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PFY0S3X_10_0010" fin="false">OK</down>
<right ref="RM000000PFY0S3X_10_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFY0S3X_10_0010" proc-id="RM22W0E___00001HX00000">
<testtitle>REPLACE ACCELERATOR PEDAL POSITION SENSOR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the accelerator pedal position sensor assembly (See page <xref label="Seep01" href="RM0000028B2015X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFY0S3X_10_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PFY0S3X_10_0005" proc-id="RM22W0E___00001HW00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (ACCELERATOR PEDAL POSITION SENSOR DTCS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle in accordance with the driving pattern described in Confirmation Driving Pattern.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P2120, P2122, P2123, P2125, P2127, P2128 and/or P2138 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PFY0S3X_10_0009" fin="true">A</down>
<right ref="RM000000PFY0S3X_10_0008" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PFY0S3X_10_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000PFY0S3X_10_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFY0S3X_10_0008">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000PFY0S3X_10_0009">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>