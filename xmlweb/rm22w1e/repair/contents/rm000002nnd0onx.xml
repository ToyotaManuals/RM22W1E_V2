<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM000002NND0ONX" category="J" type-id="802E3" name-id="NS8WT-03" from="201308">
<dtccode/>
<dtcname>Microphone Circuit between Microphone and Radio Receiver</dtcname>
<subpara id="RM000002NND0ONX_01" type-id="60" category="03" proc-id="RM22W0E___0000CJZ00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>The multi-media module receiver assembly and map light assembly (telephone microphone assembly) are connected to each other using the microphone connection detection signal lines.</ptxt>
</item>
<item>
<ptxt>Using this circuit, the multi-media module receiver assembly sends power to the map light assembly (telephone microphone assembly), and the map light assembly (telephone microphone assembly) sends microphone signals to the multi-media module receiver assembly.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002NND0ONX_02" type-id="32" category="03" proc-id="RM22W0E___0000CK000001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E248564E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002NND0ONX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002NND0ONX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002NND0ONX_05_0044" proc-id="RM22W0E___0000CK200001">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA MODULE RECEIVER ASSEMBLY - MAP LIGHT ASSEMBLY)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F77 multi-media module receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the R7 map light assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.23in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F77-49 (SNS2) - R7-20 (SNS2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F77-29 (MACC) - R7-25 (ACC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F77-27 (MIN+) - R7-27 (MI1+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F77-48 (MIN-) - R7-26 (MIC-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F77-49 (SNS2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F77-29 (MACC) - Body ground </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F77-27 (MIN+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F77-48 (MIN-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F77-28 (SGND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000002NND0ONX_05_0047" fin="false">OK</down>
<right ref="RM000002NND0ONX_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NND0ONX_05_0047" proc-id="RM22W0E___0000CK300001">
<testtitle>INSPECT MULTI-MEDIA MODULE RECEIVER ASSEMBLY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E239014E07" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.45in"/>
<colspec colname="COL2" colwidth="1.45in"/>
<colspec colname="COL3" colwidth="1.23in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F77-28 (SGND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F77-48 (MIN-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.45in"/>
<colspec colname="COL2" colwidth="1.45in"/>
<colspec colname="COL3" colwidth="1.23in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F77-29 (MACC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (ACC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4 to 6 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Multi-media Module Receiver Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6><res>
<down ref="RM000002NND0ONX_05_0004" fin="false">OK</down>
<right ref="RM000002NND0ONX_05_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NND0ONX_05_0049" proc-id="RM22W0E___0000CK400001">
<testtitle>INSPECT MAP LIGHT ASSEMBLY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the map light assembly (See page <xref label="Seep01" href="RM000002LJJ045X"/>).</ptxt>
<figure>
<graphic graphicname="E248565E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.45in"/>
<colspec colname="COL2" colwidth="1.45in"/>
<colspec colname="COL3" colwidth="1.23in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>26 (MIC-) - 20 (SNS2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000002NND0ONX_05_0020" fin="false">OK</down>
<right ref="RM000002NND0ONX_05_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NND0ONX_05_0050" proc-id="RM22W0E___0000CK500001">
<testtitle>CHECK MAP LIGHT ASSEMBLY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the engine switch on (ACC).</ptxt>
</test1>
<figure>
<graphic graphicname="E248566E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Connect an oscilloscope to terminals R7-27 (MI1+) and R7-26 (MIC-) of the map light assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Check the waveform of the telephone microphone assembly using the oscilloscope.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Map Light Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A waveform synchronized with the voice input to the map light assembly is not output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A waveform synchronized with the voice input to the map light assembly is output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000002NND0ONX_05_0019" fin="true">A</down>
<right ref="RM000002NND0ONX_05_0004" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002NND0ONX_05_0004" proc-id="RM22W0E___0000CK100001">
<testtitle>CHECK TELEPHONE MICROPHONE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the telephone microphone assembly with a new or normally functioning one (See page <xref label="Seep01" href="RM000002LJJ045X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check if the same malfunction recurs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Malfunction does not recur</ptxt>
<ptxt>(returns to normal).</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Malfunction recurs.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002NND0ONX_05_0011" fin="true">A</down>
<right ref="RM000002NND0ONX_05_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002NND0ONX_05_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002NND0ONX_05_0020">
<testtitle>MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY024X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002NND0ONX_05_0019">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000011BR0NBX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002NND0ONX_05_0012">
<testtitle>REPLACE MAP LIGHT ASSEMBLY<xref label="Seep01" href="RM000002LJJ045X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002NND0ONX_05_0011">
<testtitle>END (TELEPHONE MICROPHONE ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>