<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UP_T00NS" variety="T00NS">
<name>REAR NO. 1 SEAT ASSEMBLY (for 60/40 Split Seat Type 60 Side)</name>
<para id="RM00000391000IX" category="A" type-id="80002" name-id="SE6SA-03" from="201301" to="201308">
<name>DISASSEMBLY</name>
<subpara id="RM00000391000IX_02" type-id="11" category="10" proc-id="RM22W0E___0000G4400000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000391000IX_01" type-id="01" category="01">
<s-1 id="RM00000391000IX_01_0002" proc-id="RM22W0E___0000G2K00000">
<ptxt>REMOVE SEAT ADJUSTER BOLT COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184012" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0003" proc-id="RM22W0E___0000G2L00000">
<ptxt>REMOVE RECLINING ADJUSTER RELEASE HANDLE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184013" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw and release handle.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0004" proc-id="RM22W0E___0000G2M00000">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184014E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws in the order shown in the illustration, and then remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0005" proc-id="RM22W0E___0000G2N00000">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184015" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0006" proc-id="RM22W0E___0000G2O00000">
<ptxt>REMOVE UPPER SEAT TRACK RAIL COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181222" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0007" proc-id="RM22W0E___0000G2P00000">
<ptxt>REMOVE UPPER SEAT TRACK RAIL COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184035" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0008" proc-id="RM22W0E___0000G2Q00000">
<ptxt>REMOVE NO. 1 SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181223" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the 2 covers.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0009" proc-id="RM22W0E___0000G2R00000">
<ptxt>REMOVE REAR NO. 2 SEAT LEG SIDE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 screws and detach the 4 claws.</ptxt>
<figure>
<graphic graphicname="B181224" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt that is securing the fold seat stopper band and remove the seat leg side cover together with the fold seat stopper band.</ptxt>
<figure>
<graphic graphicname="B184044" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0064" proc-id="RM22W0E___0000G4000000">
<ptxt>REMOVE FOLD SEAT STOPPER BAND ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fold seat stopper band from the seat leg side cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0010" proc-id="RM22W0E___0000G2S00000">
<ptxt>REMOVE REAR SEAT SIDE HINGE FEMALE COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181172" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 6 claws and remove the 2 covers.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0011" proc-id="RM22W0E___0000G2T00000">
<ptxt>REMOVE REAR SEAT SIDE HINGE MALE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181173" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the clip and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0012" proc-id="RM22W0E___0000G2U00000">
<ptxt>REMOVE REAR SEAT SIDE HINGE MALE COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clip and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0001" proc-id="RM22W0E___0000G2J00000">
<ptxt>REMOVE REAR NO. 2 SEAT PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Detach the claw and remove the wire harness connector.</ptxt>
<figure>
<graphic graphicname="B184016" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Detach the claw and remove the wire harness connector.</ptxt>
</s2>
<s2>
<ptxt>Using a clip remover, remove the clip.</ptxt>
<figure>
<graphic graphicname="B182595" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and remove the seat protector from the seat hinge.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws, open the protector and remove wire harness.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0013" proc-id="RM22W0E___0000G2V00000">
<ptxt>REMOVE REAR NO. 1 SEAT HINGE SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="B184045" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 nuts and hinge.</ptxt>
</s2>
<s2>
<ptxt>Detach the cable clamp and disconnect the cable.</ptxt>
<figure>
<graphic graphicname="B181176" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0014" proc-id="RM22W0E___0000G2W00000">
<ptxt>REMOVE REAR NO. 1 SEAT HINGE SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184017" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 nuts and hinge.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0015" proc-id="RM22W0E___0000G2X00000">
<ptxt>REMOVE SEAT BELT ANCHOR COVER CAP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw and open the cap.</ptxt>
<figure>
<graphic graphicname="B181227" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw and anchor cover cap.</ptxt>
<figure>
<graphic graphicname="B181228" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0016" proc-id="RM22W0E___0000G2Y00000">
<ptxt>REMOVE REAR SEATBACK STAY COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181226" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0017" proc-id="RM22W0E___0000G2Z00000">
<ptxt>REMOVE REAR UNDER SIDE COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 10 claws.</ptxt>
<figure>
<graphic graphicname="B181229" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 5 screws.</ptxt>
<figure>
<graphic graphicname="B181230" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 clips and 2 claws, and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0018" proc-id="RM22W0E___0000G3000000">
<ptxt>REMOVE NO. 1 SEAT CUSHION PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 claws and remove the seat cushion plate.</ptxt>
<figure>
<graphic graphicname="B181180" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0019" proc-id="RM22W0E___0000G3100000">
<ptxt>REMOVE REAR SEAT CUSHION COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181231" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0020" proc-id="RM22W0E___0000G3200000">
<ptxt>REMOVE REAR SEAT UNDER TRAY COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181181" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0021" proc-id="RM22W0E___0000G3300000">
<ptxt>REMOVE REAR SEAT TRACK HANDLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and seat track handle.</ptxt>
<figure>
<graphic graphicname="B182656" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 cable clamps and disconnect the 2 cables.</ptxt>
<figure>
<graphic graphicname="B181182" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the cable clamp and disconnect the cable.</ptxt>
<figure>
<graphic graphicname="B181183" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0059">
<ptxt>REMOVE FOLD SEAT LOCK CONTROL CABLE SUB-ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM00000391000IX_01_0060" proc-id="RM22W0E___0000G6Z00000">
<ptxt>REMOVE REAR NO. 1 SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184531E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and belt.</ptxt>
<atten4>
<ptxt>The No. 1 seat 3 point type belt anchor is fixed with the same bolt as the rear No. 1 seat inner belt LH. Therefore, it becomes disconnected when the bolt is removed.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000391000IX_01_0065" proc-id="RM22W0E___0000G7000000">
<ptxt>DISCONNECT NO. 1 SEAT 3 POINT TYPE BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184533E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the belt anchor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391000IX_01_0023" proc-id="RM22W0E___0000G3400000">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<s3>
<ptxt>Detach the side airbag wire harness clamps.</ptxt>
</s3>
<s3>
<ptxt>Remove the side airbag wire harness through the hole in the seat cushion.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<s3>
<ptxt>Disconnect the 2 seat heater connectors.</ptxt>
<figure>
<graphic graphicname="B182596" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the seatback heater wire harness through the hole in the seat cushion. </ptxt>
<figure>
<graphic graphicname="B182597" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Remove the seat cushion cover with pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0024" proc-id="RM22W0E___0000G3500000">
<ptxt>REMOVE REAR SEPARATE TYPE SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 pieces of fastening tape.</ptxt>
<figure>
<graphic graphicname="B181234E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the hog rings and cover.</ptxt>
<figure>
<graphic graphicname="B181236" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0025" proc-id="RM22W0E___0000G3600000">
<ptxt>REMOVE REAR SEAT CUSHION HEATER ASSEMBLY (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181187" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Cut off the tack pins which fasten the seat heater to the seat cushion cover, and then remove the seat cushion heater from the seat cushion cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0026" proc-id="RM22W0E___0000G3700000">
<ptxt>REMOVE REAR SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181188E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0027" proc-id="RM22W0E___0000G3800000">
<ptxt>REMOVE REAR SEATBACK BOARD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181241" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<s3>
<ptxt>Detach the 2 claws on the top of the board.</ptxt>
</s3>
<s3>
<ptxt>Pull up the board to detach the 2 claws on the bottom of the board and remove it.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<figure>
<graphic graphicname="B181240" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Detach the 3 claws on the top of the board.</ptxt>
</s3>
<s3>
<ptxt>Pull up the board to detach the 3 claws on the bottom of the board and remove it.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0028">
<ptxt>REMOVE REAR SEAT HEADREST ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM00000391000IX_01_0029" proc-id="RM22W0E___0000G3900000">
<ptxt>REMOVE REAR NO. 1 SEAT HEADREST SUPPORT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181190" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Detach the 4 claws and remove the 2 supports.</ptxt>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Detach the 8 claws and remove the 4 supports.</ptxt>
<figure>
<graphic graphicname="B181243" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0030" proc-id="RM22W0E___0000G3A00000">
<ptxt>REMOVE SHOULDER BELT ANCHOR COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181238E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0031" proc-id="RM22W0E___0000G3B00000">
<ptxt>REMOVE REAR SEAT BELT HOLE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181239" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0032" proc-id="RM22W0E___0000G3C00000">
<ptxt>REMOVE SEATBACK COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Remove the hog rings.</ptxt>
<figure>
<graphic graphicname="B262667" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Remove the hog rings.</ptxt>
<figure>
<graphic graphicname="B262668" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<s3>
<ptxt>Remove the cap nut and seatback cover bracket from the seat frame.</ptxt>
<figure>
<graphic graphicname="B184020" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the side airbag wire harness through the hole in the seatback.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<s3>
<ptxt>Cut the cable tie and disconnect the seatback heater wire harness.</ptxt>
<figure>
<graphic graphicname="B182598E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the seatback heater wire harness through the hole in the seatback.</ptxt>
<figure>
<graphic graphicname="B182638" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Pull the seat belt into the inside of the seat.</ptxt>
<figure>
<graphic graphicname="B181245" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Pull the seat belt into the inside of the seat.</ptxt>
<figure>
<graphic graphicname="B181248" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the seatback cover with pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0033" proc-id="RM22W0E___0000G3D00000">
<ptxt>REMOVE REAR SEPARATE TYPE SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Disconnect the seatback cover bracket from the seatback pad.</ptxt>
<figure>
<graphic graphicname="B184021" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Remove the hog rings and rear seatback cover.</ptxt>
<figure>
<graphic graphicname="B181246" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Remove the hog rings and rear seatback cover.</ptxt>
<figure>
<graphic graphicname="B181249" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0034" proc-id="RM22W0E___0000G3E00000">
<ptxt>REMOVE REAR SEATBACK HEATER ASSEMBLY (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181195" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Cut off the tack pins which fasten the seatback heater to the seatback cover, and then remove the seatback heater from the seatback cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0035" proc-id="RM22W0E___0000G3F00000">
<ptxt>REMOVE SEATBACK STOPPER PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<figure>
<graphic graphicname="B181250" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Using a T40 "TORX" socket, remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Remove the bolt and stopper plate.</ptxt>
</s3>
<s3>
<ptxt>Remove the bush from the stopper plate.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<figure>
<graphic graphicname="B181251" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Using a T40 "TORX" socket, remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Remove the bolt and stopper plate.</ptxt>
</s3>
<s3>
<ptxt>Remove the bush from the stopper plate.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0036" proc-id="RM22W0E___0000G3G00000">
<ptxt>REMOVE REAR CENTER SEAT ARMREST ASSEMBLY (w/ Center Armrest)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181252" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T40 "TORX" socket, remove the 2 bolts and center seat armrest.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0037">
<ptxt>REMOVE REAR CENTER SEAT HEADREST ASSEMBLY (w/ Center Armrest)</ptxt>
</s-1>
<s-1 id="RM00000391000IX_01_0066" proc-id="RM22W0E___0000G4200000">
<ptxt>REMOVE NO. 1 SEAT ARMREST PLATE (w/ Center Armrest)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181253" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 4 clips and 2 claws, and then remove the 2 armrest plates.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0067" proc-id="RM22W0E___0000G4300000">
<ptxt>REMOVE CUP HOLDER ASSEMBLY (w/ Center Armrest)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws.</ptxt>
<figure>
<graphic graphicname="B190297" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 2 clips and remove the cup holder.</ptxt>
<figure>
<graphic graphicname="B181789" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0040" proc-id="RM22W0E___0000G3H00000">
<ptxt>REMOVE REAR SEAT RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181196" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0041" proc-id="RM22W0E___0000G3I00000">
<ptxt>REMOVE REAR NO. 2 SEAT RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181197" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the clamp and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0042" proc-id="RM22W0E___0000G3J00000">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181790" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0043" proc-id="RM22W0E___0000G3K00000">
<ptxt>REMOVE REAR SEATBACK HINGE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181237" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0044" proc-id="RM22W0E___0000G3L00000">
<ptxt>REMOVE REAR LOWER SEATBACK COVER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181791" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 4 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0062" proc-id="RM22W0E___0000GJK00000">
<ptxt>REMOVE REAR SEAT SHOULDER BELT HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185854" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 3 claws and remove the shoulder belt hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391000IX_01_0063" proc-id="RM22W0E___0000GJL00000">
<ptxt>REMOVE NO. 1 SEAT 3 POINT TYPE BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185876" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Remove the 3 nuts and belt sensor.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and then remove the outer belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391000IX_01_0047" proc-id="RM22W0E___0000G3M00000">
<ptxt>REMOVE RECLINING ADJUSTING CABLE ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181792" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 cable clamps and disconnect the cable.</ptxt>
</s2>
<s2>
<ptxt>Detach the clamp and remove the adjusting cable.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0048" proc-id="RM22W0E___0000G3N00000">
<ptxt>REMOVE NO. 2 FOLD SEAT LOCK CONTROL CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184052" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 cable clamps and disconnect the cable.</ptxt>
</s2>
<s2>
<ptxt>Detach the clamp and remove the lock control cable.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0049" proc-id="RM22W0E___0000G3O00000">
<ptxt>REMOVE REAR SEATBACK FRAME SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181793" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>LH Side:</ptxt>
<ptxt>Using a T40 "TORX" socket, remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>RH Side:</ptxt>
<ptxt>Using a T55 "TORX" socket, remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Remove the seatback frame sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the lower seatback cover.</ptxt>
<figure>
<graphic graphicname="B181794" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0050" proc-id="RM22W0E___0000G3P00000">
<ptxt>REMOVE FOLD SEAT LOCK CONTROL CABLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181795" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 cable clamps and disconnect the 2 cables.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 fold seat lock cables.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0051" proc-id="RM22W0E___0000G3Q00000">
<ptxt>REMOVE REAR NO. 1 SEAT LOCK CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and lower seat track bracket.</ptxt>
<figure>
<graphic graphicname="B181203" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Cut the cable tie and disconnect the cable.</ptxt>
<figure>
<graphic graphicname="B181796E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 3 cable clamps and disconnect the 3 cables.</ptxt>
</s2>
<s2>
<ptxt>Detach the clamp and remove the seat lock cable.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0052" proc-id="RM22W0E___0000G3R00000">
<ptxt>REMOVE REAR SEAT CUSHION HOLDER BRACKET LH (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182639" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the seat wire connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 screws and holder bracket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0053" proc-id="RM22W0E___0000G3S00000">
<ptxt>REMOVE SEAT HEATER CONTROL SUB-ASSEMBLY LH (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182640" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws and remove the seat heater control.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0061" proc-id="RM22W0E___0000GJ300000">
<ptxt>REMOVE REAR SEAT INNER BELT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185852E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391000IX_01_0056" proc-id="RM22W0E___0000G3V00000">
<ptxt>REMOVE NO. 1 SEAT LEG ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181799" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T40 "TORX" socket, remove the 2 bolts and seat leg assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0054" proc-id="RM22W0E___0000G3T00000">
<ptxt>REMOVE REAR SEAT CUSHION FRAME SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181798" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T40 "TORX" socket, remove the bolt.</ptxt>
</s2>
<s2>
<ptxt>Remove the 5 nuts and seat cushion frame.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391000IX_01_0055" proc-id="RM22W0E___0000G3U00000">
<ptxt>REMOVE REAR SEAT LOCK CONTROL LEVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181797" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and lock control lever.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>