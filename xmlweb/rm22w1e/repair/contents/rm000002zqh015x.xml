<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3U8_T00NB" variety="T00NB">
<name>SIDE AIRBAG SENSOR</name>
<para id="RM000002ZQH015X" category="A" type-id="30014" name-id="RSDVT-02" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM000002ZQH015X_02" type-id="11" category="10" proc-id="RM22W0E___0000FKD00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the RH and LH.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000002ZQH015X_01" type-id="01" category="01">
<s-1 id="RM000002ZQH015X_01_0017" proc-id="RM22W0E___0000FKC00000">
<ptxt>INSTALL SIDE AIRBAG SENSOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the ignition switch is off.</ptxt>
<figure>
<graphic graphicname="B181765" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Check that the cable is disconnected from the battery negative (-) terminal.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Install the side airbag sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>9.0</t-value1>
<t-value2>92</t-value2>
<t-value3>80</t-value3>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>If the side airbag sensor has been dropped, or there are any cracks, dents or other defects in the case, bracket or connector, replace it with a new one.</ptxt>
</item>
<item>
<ptxt>When installing the side airbag sensor, be careful that the SRS wiring does not interfere with other parts and that it is not pinched between other parts.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Check that there is no looseness in the installation parts of the side airbag sensor.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002ZQH015X_01_0012" proc-id="RM22W0E___0000BP000000">
<ptxt>INSTALL FRONT DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B183139" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the front door lock remote control cable assembly LH and front door inside locking cable assembly LH to the front door inside handle sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 claws and 13 clips to install the front door trim board sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180821" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002ZQH015X_01_0014" proc-id="RM22W0E___0000BP400000">
<ptxt>INSTALL ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 claws to install the door assist grip cover LH to the front door trim board sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002ZQH015X_01_0013" proc-id="RM22W0E___0000FKB00000">
<ptxt>INSTALL MULTIPLEX NETWORK MASTER SWITCH ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the master switch with the 3 screws.</ptxt>
<figure>
<graphic graphicname="B182892" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002ZQH015X_01_0015" proc-id="RM22W0E___0000BP200000">
<ptxt>INSTALL FRONT DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws to install the front door inside handle bezel LH.</ptxt>
<figure>
<graphic graphicname="B183136" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002ZQH015X_01_0018" proc-id="RM22W0E___0000BP500000">
<ptxt>INSTALL FRONT DOOR LOWER FRAME BRACKET GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and claw, and install the front door lower frame bracket garnish LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002ZQH015X_01_0006" proc-id="RM22W0E___0000FK900000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002ZQH015X_01_0007" proc-id="RM22W0E___0000FKA00000">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0IZX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>