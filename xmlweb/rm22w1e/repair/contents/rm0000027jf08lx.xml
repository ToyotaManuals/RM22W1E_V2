<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3X2_T00Q5" variety="T00Q5">
<name>POWER WINDOW CONTROL SYSTEM (for Models with Jam Protection Function on 4 Windows)</name>
<para id="RM0000027JF08LX" category="C" type-id="800LG" name-id="WS1PF-18" from="201301">
<dtccode>B2313</dtccode>
<dtcname>Glass Position Initialization Incomplete</dtcname>
<subpara id="RM0000027JF08LX_01" type-id="60" category="03" proc-id="RM22W0E___0000HX400000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The power window regulator motor assembly is driven by operating the power window switch. The power window regulator motor assembly has motor, regulator and ECU functions. </ptxt>
<ptxt>When the ECU determines that the power window regulator motor has not been initialized, DTC B2313 is stored.</ptxt>
<atten4>
<ptxt>This DTC may be stored for all the windows.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When the power window regulator motor is reinstalled or replaced, the power window control system must be initialized.</ptxt>
</item>
<item>
<ptxt>After a door glass or a door glass run has been replaced, the jam protection function may operate unexpectedly when the auto up function is used. In such cases, the auto up function can be resumed by repeating the following operation at least 5 times:</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Close the power window by fully pulling up the power window switch and holding it in the auto up position.</ptxt>
</item>
<item>
<ptxt>Open the power window by fully pushing down the switch.</ptxt>
</item>
</list2>
</list1>
</atten3>
<table pgwide="1">
<title>Front Power Window Regulator Motor LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.21in"/>
<colspec colname="COL2" colwidth="2.96in"/>
<colspec colname="COL3" colwidth="2.91in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2313</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>The power window regulator motor is not initialized.</ptxt>
</item>
<item>
<ptxt>The power window regulator motor malfunctions.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Front power window regulator motor assembly LH</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Front Power Window Regulator Motor RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.21in"/>
<colspec colname="COL2" colwidth="2.96in"/>
<colspec colname="COL3" colwidth="2.91in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2313</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>The power window regulator motor is not initialized.</ptxt>
</item>
<item>
<ptxt>The power window regulator motor malfunctions.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Front power window regulator motor assembly RH</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Rear Power Window Regulator Motor LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.21in"/>
<colspec colname="COL2" colwidth="2.96in"/>
<colspec colname="COL3" colwidth="2.91in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2313</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>The power window regulator motor is not initialized.</ptxt>
</item>
<item>
<ptxt>The power window regulator motor malfunctions.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Rear power window regulator motor assembly LH</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Rear Power Window Regulator Motor RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.21in"/>
<colspec colname="COL2" colwidth="2.96in"/>
<colspec colname="COL3" colwidth="2.91in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2313</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>The power window regulator motor is not initialized.</ptxt>
</item>
<item>
<ptxt>The power window regulator motor malfunctions.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Rear power window regulator motor assembly RH</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000027JF08LX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000027JF08LX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000027JF08LX_05_0007" proc-id="RM22W0E___0000HX600000">
<testtitle>PERFORM INITIALIZATION (APPLICABLE LOCATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Initialize the power window regulator motor (See page <xref label="Seep01" href="RM000000PYB02SX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000027JF08LX_05_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000027JF08LX_05_0001" proc-id="RM22W0E___0000HX500000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000000YCC0ABX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.93in"/>
<colspec colname="COL2" colwidth="1.20in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>B2313 is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>B2313 output from front power window regulator motor LH</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>B2313 output from front power window regulator motor RH</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>B2313 output from rear power window regulator motor LH</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>B2313 output from rear power window regulator motor RH</ptxt>
</entry>
<entry valign="middle">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000027JF08LX_05_0008" fin="true">A</down>
<right ref="RM0000027JF08LX_05_0002" fin="true">B</right>
<right ref="RM0000027JF08LX_05_0005" fin="true">C</right>
<right ref="RM0000027JF08LX_05_0004" fin="true">D</right>
<right ref="RM0000027JF08LX_05_0006" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM0000027JF08LX_05_0002">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR ASSEMBLY LH<xref label="Seep01" href="RM000002STX03BX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000027JF08LX_05_0005">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR ASSEMBLY RH<xref label="Seep01" href="RM000002STX03BX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000027JF08LX_05_0004">
<testtitle>REPLACE REAR POWER WINDOW REGULATOR MOTOR ASSEMBLY LH<xref label="Seep01" href="RM000002SU001ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000027JF08LX_05_0006">
<testtitle>REPLACE REAR POWER WINDOW REGULATOR MOTOR ASSEMBLY RH<xref label="Seep01" href="RM000002SU001ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000027JF08LX_05_0008">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>