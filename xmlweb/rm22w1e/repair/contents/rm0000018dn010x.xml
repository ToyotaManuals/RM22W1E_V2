<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12033_S001O" variety="S001O">
<name>BRAKE (REAR)</name>
<ttl id="12033_S001O_7C3PT_T00IW" variety="T00IW">
<name>REAR BRAKE</name>
<para id="RM0000018DN010X" category="G" type-id="3001K" name-id="BR1PI-02" from="201301">
<name>INSPECTION</name>
<subpara id="RM0000018DN010X_01" type-id="01" category="01">
<s-1 id="RM0000018DN010X_01_0002" proc-id="RM22W0E___0000AXX00000">
<ptxt>CHECK PAD LINING THICKNESS</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C155445" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a ruler, measure the pad lining thickness.</ptxt>
<spec>
<title>Standard Thickness</title>
<specitem>
<ptxt>12.0 mm (0.472 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum Thickness</title>
<specitem>
<ptxt>1.0 mm (0.0394 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the pad lining thickness is less than the minimum, replace the pad.</ptxt>
<atten4>
<ptxt>Be sure to check the wear on the rear disc after replacing the brake pad with a new one.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018DN010X_01_0004" proc-id="RM22W0E___0000AXY00000">
<ptxt>CHECK DISC THICKNESS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the disc with the 3 hub nuts.</ptxt>
</s2>
<s2>
<ptxt>Using a micrometer, measure the disc thickness.</ptxt>
<figure>
<graphic graphicname="C172163" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Thickness</title>
<specitem>
<ptxt>18.0 mm (0.709 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum Thickness</title>
<specitem>
<ptxt>16.0 mm (0.630 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the disc thickness is less than the minimum, replace the disc.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018DN010X_01_0005" proc-id="RM22W0E___0000AXZ00000">
<ptxt>CHECK DISC RUNOUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fix the disc in place with the 5 hub nuts.</ptxt>
<torque>
<subtitle>for Aluminum Wheel</subtitle>
<torqueitem>
<t-value1>131</t-value1>
<t-value2>1336</t-value2>
<t-value4>97</t-value4>
</torqueitem>
<subtitle>for Steel Wheel</subtitle>
<torqueitem>
<t-value1>209</t-value1>
<t-value2>2131</t-value2>
<t-value4>154</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a dial indicator, measure the disc runout at a position 10 mm (0.394 in.) from the outside edge.</ptxt>
<figure>
<graphic graphicname="C172164" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum Disc Runout</title>
<specitem>
<ptxt>0.15 mm (0.00590 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the disc runout is more than the maximum, check the rear axle shaft (See page <xref label="Seep01" href="RM00000270Y00WX_01_0001"/>). If the rear axle shaft is normal, adjust the disc runout or grind it on an on-vehicle brake lathe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018DN010X_01_0006" proc-id="RM22W0E___0000AY000000">
<ptxt>ADJUST DISC RUNOUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and rear disc brake cylinder mounting from the backing plate.</ptxt>
</s2>
<s2>
<ptxt>Remove the hub nuts and disc. Rotate the disc 1/5 of a turn from its original position on the hub, and install the disc with the hub nuts.</ptxt>
<torque>
<subtitle>for Aluminum Wheel</subtitle>
<torqueitem>
<t-value1>131</t-value1>
<t-value2>1336</t-value2>
<t-value4>97</t-value4>
</torqueitem>
<subtitle>for Steel Wheel</subtitle>
<torqueitem>
<t-value1>209</t-value1>
<t-value2>2131</t-value2>
<t-value4>154</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Measure the disc runout. Make a note of the runout and the disc position on the hub.</ptxt>
</s2>
<s2>
<ptxt>Repeat the 2 previous steps until the disc has been installed on the 3 remaining hub positions. If the minimum runout recorded above is less than 0.15 mm (0.00590 in.), install the disc in that position. If the minimum runout recorded above is more than 0.15 mm (0.00590 in.), replace the disc and repeat the "Check Disc Runout" procedure.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>