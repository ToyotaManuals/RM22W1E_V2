<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T7_T00MA" variety="T00MA">
<name>ENTRY AND START SYSTEM (for Start Function)</name>
<para id="RM000000XES07VX" category="C" type-id="304MY" name-id="TD4H7-03" from="201308">
<dtccode>B2275</dtccode>
<dtcname>STSW Monitor Malfunction</dtcname>
<subpara id="RM000000XES07VX_01" type-id="60" category="03" proc-id="RM22W0E___0000EH200001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when the engine start request signal circuit inside the main body ECU is open or shorted.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<colspec colname="COL3" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2275</ptxt>
</entry>
<entry valign="middle">
<ptxt>The engine start request signal circuit inside the main body ECU or other related circuit is malfunctioning.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Main body ECU</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XES07VX_02" type-id="32" category="03" proc-id="RM22W0E___0000EH300001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C162997E19" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XES07VX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000XES07VX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XES07VX_04_0006" proc-id="RM22W0E___0000EH500001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000YEH0DLX"/>).</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal (for A/T) or clutch pedal (for M/T) with the engine switch on (IG), wait at least 15 seconds, and check whether DTC B2275 is output.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2275 output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XES07VX_04_0001" fin="false">A</down>
<right ref="RM000000XES07VX_04_0008" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XES07VX_04_0001" proc-id="RM22W0E___0000EH400001">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - ECM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A175400E11" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E4 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A38 (for LHD) or A52 (for RHD) ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (except 1VD-FTV w/ DPF)</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E4-4 (STSW) - A38-14 (STSW)*1</ptxt>
<ptxt>E4-4 (STSW) - A52-14 (STSW)*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E4-4 (STSW) or A38-14 (STSW) - Body ground*1</ptxt>
<ptxt>E4-4 (STSW) or A52-14 (STSW) - Body ground*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
<spec>
<title>Standard Resistance (for 1VD-FTV w/ DPF)</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E4-4 (STSW) - A38-47 (STSW)*1</ptxt>
<ptxt>E4-4 (STSW) - A52-47 (STSW)*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E4-4 (STSW) or A38-47 (STSW) - Body ground*1</ptxt>
<ptxt>E4-4 (STSW) or A52-47 (STSW) - Body ground*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000XES07VX_04_0007" fin="false">OK</down>
<right ref="RM000000XES07VX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XES07VX_04_0007" proc-id="RM22W0E___0000EH600001">
<testtitle>INSPECT MAIN BODY ECU</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C164808E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<atten4>
<ptxt>The voltage is generated at terminal STSW for 0.3 seconds when the engine cranks.</ptxt>
</atten4>
<spec>
<title>Standard Voltage (for A/T)</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E4-4 (STSW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal depressed with shift lever in P, engine switch pushed once</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage (for M/T)</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E4-4 (STSW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clutch pedal depressed, engine switch pushed once</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.76in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Outside specified range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Within specified range (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Within specified range (for 1UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Within specified range (for 3UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Within specified range (for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XES07VX_04_0009" fin="true">A</down>
<right ref="RM000000XES07VX_04_0003" fin="true">B</right>
<right ref="RM000000XES07VX_04_0010" fin="true">C</right>
<right ref="RM000000XES07VX_04_0012" fin="true">D</right>
<right ref="RM000000XES07VX_04_0011" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000000XES07VX_04_0003">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292037X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XES07VX_04_0008">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XES07VX_04_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XES07VX_04_0009">
<testtitle>REPLACE MAIN BODY ECU</testtitle>
</testgrp>
<testgrp id="RM000000XES07VX_04_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XES07VX_04_0012">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292039X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XES07VX_04_0011">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329203AX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>