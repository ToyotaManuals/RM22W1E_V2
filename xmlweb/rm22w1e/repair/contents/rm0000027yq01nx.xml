<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T7_T00MA" variety="T00MA">
<name>ENTRY AND START SYSTEM (for Start Function)</name>
<para id="RM0000027YQ01NX" category="C" type-id="304MV" name-id="TD4H9-02" from="201301">
<dtccode>B2272</dtccode>
<dtcname>Ignition 1 Monitor Malfunction</dtcname>
<subpara id="RM0000027YQ01NX_01" type-id="60" category="03" proc-id="RM22W0E___0000EGL00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when a malfunction occurs in the IG1D output circuit, which is between the IG1 No. 3 relay actuation circuit inside the main body ECU and the IG1 No. 3 relay.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<colspec colname="COL3" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2272</ptxt>
</entry>
<entry valign="middle">
<ptxt>The IG1 No. 3 relay actuation circuit inside the main body ECU or other related circuit is malfunctioning.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Main body ECU</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000027YQ01NX_02" type-id="32" category="03" proc-id="RM22W0E___0000EGM00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C164716E04" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000027YQ01NX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000027YQ01NX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000027YQ01NX_04_0001" proc-id="RM22W0E___0000EGN00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (IG1 RELAY MONITOR (OUTSIDE))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the IG1 No. 3 relay is functioning properly.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="1.46in"/>
<colspec colname="COL2" colwidth="2.25in"/>
<colspec colname="COL3" colwidth="1.87in"/>
<colspec colname="COL4" colwidth="1.51in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>IG1 Relay Monitor (Outside)</ptxt>
</entry>
<entry valign="middle">
<ptxt>IG1 No. 3 relay outer relay monitor/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Engine switch on (IG)</ptxt>
<ptxt>OFF: Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>When the engine switch is turned on (IG), ON is displayed on the intelligent tester.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000027YQ01NX_04_0012" fin="false">OK</down>
<right ref="RM0000027YQ01NX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000027YQ01NX_04_0012" proc-id="RM22W0E___0000EGP00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000YEH0DLX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG), wait at least 10 seconds, and check whether DTC B2272 is output. </ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No DTC output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2272 output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000027YQ01NX_04_0007" fin="true">A</down>
<right ref="RM0000027YQ01NX_04_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000027YQ01NX_04_0003" proc-id="RM22W0E___0000EGO00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A175399E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E1, 2A and 2D main body ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2A-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E1-3 (IG1D) - 2D-43</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-62 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-3 (IG1D) or 2D-43 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000027YQ01NX_04_0009" fin="true">OK</down>
<right ref="RM0000027YQ01NX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000027YQ01NX_04_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000027YQ01NX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000027YQ01NX_04_0009">
<testtitle>REPLACE MAIN BODY ECU</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>