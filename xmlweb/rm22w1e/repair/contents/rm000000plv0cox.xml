<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LY_T00F1" variety="T00F1">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM000000PLV0COX" category="J" type-id="300WL" name-id="CC420-01" from="201301" to="201308">
<dtccode/>
<dtcname>Cruise Main Indicator Light Circuit</dtcname>
<subpara id="RM000000PLV0COX_01" type-id="60" category="03" proc-id="RM22W0E___00007A300000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>The ECM detects a cruise control switch signal and sends it to the combination meter via the CAN communication line. Then the cruise control indicator light comes on.</ptxt>
</item>
<item>
<ptxt>The cruise control indicator light circuit uses the CAN for communication. If there is a malfunction in this circuit, check for DTCs in the CAN communication system before troubleshooting this circuit.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000PLV0COX_04" type-id="32" category="03" proc-id="RM22W0E___00007A600000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E149011E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000PLV0COX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000PLV0COX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PLV0COX_03_0001" proc-id="RM22W0E___00007A400000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (CRUISE CONTROL INDICATOR LIGHT)</testtitle>
<content6 releasenbr="2">
<test1>
<ptxt>Using the intelligent tester, perform the Active Test (See page <xref label="Seep01" href="RM000002L7702ZX"/>).</ptxt>
<table pgwide="1">
<title>Combination Meter</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Indicat. Lamp Cruise</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise control indicator light operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF or ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Indicator light comes on/goes off.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PLV0COX_03_0002" fin="false">OK</down>
<right ref="RM000000PLV0COX_03_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PLV0COX_03_0002" proc-id="RM22W0E___00007A500000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CRUISE CONTROL INDICATOR LIGHT)</testtitle>
<content6 releasenbr="2">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000002L7702ZX"/>).</ptxt>
<table pgwide="1">
<title>Radar Cruise</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>CCS Indicator M-CPU</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Cruise control indicator light (Main CPU) / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Cruise control indicator light on</ptxt>
<ptxt>OFF: Cruise control indicator light off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The intelligent tester display changes according to condition of cruise control indicator light.</ptxt>
</specitem>
</spec>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 1UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 3UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000PLV0COX_03_0003" fin="true">A</down>
<right ref="RM000000PLV0COX_03_0007" fin="true">B</right>
<right ref="RM000000PLV0COX_03_0008" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000PLV0COX_03_0003">
<testtitle>PROCEED TO NEXT CIRCUIT INSPECTION SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000PLS0DBX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PLV0COX_03_0004">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM0000038ID00IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PLV0COX_03_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PLV0COX_03_0008">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>