<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MN_T00FQ" variety="T00FQ">
<name>AUTOMATIC TRANSMISSION ASSEMBLY (for 1VD-FTV)</name>
<para id="RM0000018ZD04WX" category="A" type-id="80001" name-id="AT9NW-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000018ZD04WX_01" type-id="01" category="01">
<s-1 id="RM0000018ZD04WX_01_0099" proc-id="RM22W0E___000089K00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>)</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0038" proc-id="RM22W0E___000089500001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0081" proc-id="RM22W0E___000011Z00002">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZD04WX_01_0082" proc-id="RM22W0E___000011Y00002">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZD04WX_01_0096" proc-id="RM22W0E___000032P00001">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts and No. 1 engine under cover.</ptxt>
<figure>
<graphic graphicname="A178461" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZD04WX_01_0097" proc-id="RM22W0E___000057Z00001">
<ptxt>REMOVE NO. 2 ENGINE UNDER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and No. 2 engine under cover.</ptxt>
<figure>
<graphic graphicname="A178462" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZD04WX_01_0064" proc-id="RM22W0E___000058000001">
<ptxt>REMOVE OIL PAN PROTECTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171483" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and oil pan protector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0091" proc-id="RM22W0E___000057200001">
<ptxt>REMOVE FRONT FENDER APRON TRIM PACKING A
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 clips and front fender apron seal front RH.</ptxt>
<figure>
<graphic graphicname="A177003" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZD04WX_01_0092" proc-id="RM22W0E___000056J00001">
<ptxt>REMOVE FRONT FENDER APRON TRIM PACKING C
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 clips and front fender apron seal rear RH.</ptxt>
<figure>
<graphic graphicname="A177002" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZD04WX_01_0057" proc-id="RM22W0E___00004KR00000">
<ptxt>DRAIN AUTOMATIC TRANSMISSION FLUID
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the drain plug and gasket, and drain the ATF.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket and the drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZD04WX_01_0024" proc-id="RM22W0E___000089300001">
<ptxt>DISCONNECT FLOOR SHIFT GEAR SHIFTING ROD SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C170828" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clip and pin, and disconnect the gear shifting rod from the transmission control shaft lever RH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0060" proc-id="RM22W0E___000089C00001">
<ptxt>REMOVE FRONT PROPELLER SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000018QS00NX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0028" proc-id="RM22W0E___000089400001">
<ptxt>REMOVE PROPELLER SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000001AMB00MX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0098" proc-id="RM22W0E___000089J00001">
<ptxt>REMOVE EXHAUST PIPE (w/ DPF)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000003B0N00HX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0095" proc-id="RM22W0E___000089I00001">
<ptxt>REMOVE EXHAUST PIPE (w/o DPF)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000003B0N00FX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0040" proc-id="RM22W0E___000089600001">
<ptxt>REMOVE DRIVE PLATE AND TORQUE CONVERTER CLUTCH SETTING BOLT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A170323" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and oil pan insulator.</ptxt>
</s2>
<s2>
<ptxt>Turn the crankshaft to gain access to the 6 bolts and remove the bolts while holding the crankshaft pulley setting bolt with a wrench.</ptxt>
<figure>
<graphic graphicname="C171531" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0078" proc-id="RM22W0E___000089D00001">
<ptxt>DISCONNECT OIL COOLER TUBE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C177423" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the 2 transmission oil cooler hoses from the oil cooler tube union.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and separate the oil cooler tube from the engine.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0041" proc-id="RM22W0E___000089700001">
<ptxt>SUPPORT AUTOMATIC TRANSMISSION ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161356" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Support the transmission with a transmission jack. Lift the transmission slightly from the crossmember.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0043" proc-id="RM22W0E___000089800001">
<ptxt>REMOVE NO. 2 FRAME CROSSMEMBER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171484" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the engine mounting hole cover.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 bolts of the rear engine mounting insulator.</ptxt>
<figure>
<graphic graphicname="C171489" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 nuts, 4 bolts and frame crossmember.</ptxt>
<figure>
<graphic graphicname="C171490E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0044" proc-id="RM22W0E___000089900001">
<ptxt>REMOVE REAR NO. 1 ENGINE MOUNTING INSULATOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172398" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and rear engine mounting insulator from the transmission.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0088" proc-id="RM22W0E___000089F00001">
<ptxt>DISCONNECT BREATHER PLUG HOSE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171532" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the 3 breather plug hoses from the wire harness.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0045" proc-id="RM22W0E___000089A00001">
<ptxt>DISCONNECT WIRE HARNESS AND CONNECTOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171503" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and disconnect the ground cable.</ptxt>
</s2>
<s2>
<ptxt>Tilt the transmission downward.</ptxt>
<figure>
<graphic graphicname="C162764E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure the cooling fan does not contact the fan shroud.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Disconnect the park/neutral position switch connector, 2 transmission wire connectors, 2 speed sensor connectors and 2 or 3 transfer control side connectors.</ptxt>
<figure>
<graphic graphicname="C172390E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>Detach the claw, press down the lever, and then disconnect the transmission wire connector.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Disconnect the 8 harness clamps.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the wire harness.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0090" proc-id="RM22W0E___000089H00001">
<ptxt>REMOVE OIL PAN COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172394" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and oil pan cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0089" proc-id="RM22W0E___000089G00001">
<ptxt>REMOVE TRANSMISSION HOUSING COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C175581" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 3 bolts and transmission housing cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0020" proc-id="RM22W0E___000089200001">
<ptxt>REMOVE AUTOMATIC TRANSMISSION ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C175537" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 10 bolts and transmission.</ptxt>
<atten3>
<ptxt>Do not use excess force to pry off the transmission assembly.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0079" proc-id="RM22W0E___000089E00001">
<ptxt>REMOVE TRANSFER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171493" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 7 bolts and transfer case lower protector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 8 bolts and transfer.</ptxt>
<figure>
<graphic graphicname="C171523" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0051">
<ptxt>REMOVE TORQUE CONVERTER CLUTCH ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM0000018ZD04WX_01_0052" proc-id="RM22W0E___000089B00001">
<ptxt>INSPECT TORQUE CONVERTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000013F204LX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>