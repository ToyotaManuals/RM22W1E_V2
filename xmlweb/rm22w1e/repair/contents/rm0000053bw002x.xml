<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12009_S000L" variety="S000L">
<name>1UR-FE EMISSION CONTROL</name>
<ttl id="12009_S000L_7C3ID_T00BG" variety="T00BG">
<name>EMISSION CONTROL SYSTEM (w/o Secondary Air Injection System)</name>
<para id="RM0000053BW002X" category="G" type-id="8000T" name-id="EC4RG-01" from="201301">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM0000053BW002X_01" type-id="01" category="01">
<s-1 id="RM0000053BW002X_01_0001" proc-id="RM22W0E___00006IJ00000">
<ptxt>INSPECT PURGE VALVE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Remove the V-bank cover (See page <xref label="Seep01" href="RM000002BOI023X_01_0005"/>).</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A266172" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the hose (connected to the canister) from the purge VSV.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and turn the GTS on.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Activate the VSV for EVAP Control.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Operation</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>EVAP VSV: OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Purge VSV has no suction</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>EVAP VSV: ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Purge VSV has suction</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Connect the hose (connected to the canister) to the purge VSV.</ptxt>
</s2>
<s2>
<ptxt>Install the V-bank cover (See page <xref label="Seep02" href="RM000002BOF023X_01_0009"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000053BW002X_01_0002" proc-id="RM22W0E___00006IK00000">
<ptxt>CHECK FUEL CUT RPM</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start and warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Open the throttle valve and maintain the engine speed at 3000 rpm.</ptxt>
</s2>
<s2>
<ptxt>Use a sound scope to check for injector operating sounds.</ptxt>
</s2>
<s2>
<ptxt>Check that when the accelerator pedal is released, injector operating sounds stop momentarily and then resume.</ptxt>
<ptxt>If the result is as not specified, check the injector, wiring and ECM.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000053BW002X_01_0003" proc-id="RM22W0E___00006IL00000">
<ptxt>VISUALLY INSPECT HOSES, CONNECTIONS AND GASKETS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that there are no cracks, leaks or damage.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Detachment or other problems with the engine oil dipstick, filler cap, ventilation hose and other components may cause the engine to run improperly.</ptxt>
</item>
<item>
<ptxt>Disconnection, looseness or cracks in the parts of the air induction system between the throttle body and cylinder head will allow air suction and cause an engine failure or engine failure malfunctions.</ptxt>
</item>
</list1>
</atten3>
<ptxt>If the result is not as specified, replace parts as necessary.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000053BW002X_01_0004" proc-id="RM22W0E___00006IM00000">
<ptxt>CHECK HOSES AND CONNECTORS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Visually check for loose connections, sharp bends or damage.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000053BW002X_01_0005" proc-id="RM22W0E___00006IN00000">
<ptxt>CHECK FUEL TANK ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Visually check for deformation, cracks or fuel leakage.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>