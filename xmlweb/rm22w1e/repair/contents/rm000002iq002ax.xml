<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R1_T00K4" variety="T00K4">
<name>REAR SEAT ENTERTAINMENT SYSTEM</name>
<para id="RM000002IQ002AX" category="J" type-id="802YR" name-id="AV5KC-14" from="201301" to="201308">
<dtccode/>
<dtcname>Sound Signal Circuit between Headphone Terminal and Television Display</dtcname>
<subpara id="RM000002IQ002AX_01" type-id="60" category="03" proc-id="RM22W0E___0000BT100000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This is the sound signal circuit from the television display assembly to the headphone terminal.</ptxt>
</content5>
</subpara>
<subpara id="RM000002IQ002AX_02" type-id="32" category="03" proc-id="RM22W0E___0000BT200000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E248577E02" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002IQ002AX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002IQ002AX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002IQ002AX_04_0001" proc-id="RM22W0E___0000BT300000">
<testtitle>CHECK HARNESS AND CONNECTOR (TELEVISION DISPLAY ASSEMBLY - HEADPHONE TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the L53 television display assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the P1 headphone terminal (video terminal) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>L53-7 (HP2R) - P1-1 (HP2R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-5 (HP2L) - P1-2 (HP2L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-6 (SLD6) - P1-3 (SGN2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-10 (HP1R) - P1-9 (HP1R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-8 (HP1L) - P1-10 (HP1L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-9 (SLD5) - P1-11 (SGN1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-7 (HP2R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-5 (HP2L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-6 (SLD6) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-10 (HP1R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-8 (HP1L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L53-9 (SLD5) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002IQ002AX_04_0002" fin="true">OK</down>
<right ref="RM000002IQ002AX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002IQ002AX_04_0002">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM00000158R02SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002IQ002AX_04_0003">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>