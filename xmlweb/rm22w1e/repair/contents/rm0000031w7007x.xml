<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000D" variety="S000D">
<name>1VD-FTV ENGINE MECHANICAL</name>
<ttl id="12007_S000D_7C3FQ_T008T" variety="T008T">
<name>REAR CRANKSHAFT OIL SEAL</name>
<para id="RM0000031W7007X" category="A" type-id="80001" name-id="EMBQW-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000031W7007X_01" type-id="01" category="01">
<s-1 id="RM0000031W7007X_01_0015" proc-id="RM22W0E___00005L100001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000031W7007X_01_0014" proc-id="RM22W0E___00005L000001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000031W7007X_01_0011" proc-id="RM22W0E___00005KZ00001">
<ptxt>REMOVE AUTOMATIC TRANSMISSION ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000018ZD04WX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000031W7007X_01_0006" proc-id="RM22W0E___00005KY00001">
<ptxt>REMOVE MANUAL TRANSMISSION UNIT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000031IH008X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000031W7007X_01_0008" proc-id="RM22W0E___000056U00001">
<ptxt>REMOVE CLUTCH COVER ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C173317E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put matchmarks on the clutch cover and flywheel.</ptxt>
</s2>
<s2>
<ptxt>Loosen each set bolt one turn at a time until spring tension is released.</ptxt>
</s2>
<s2>
<ptxt>Remove the 8 set bolts, and pull off the clutch cover.</ptxt>
<atten3>
<ptxt>Do not drop the clutch disc.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000031W7007X_01_0009" proc-id="RM22W0E___000056V00001">
<ptxt>REMOVE CLUTCH DISC ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Keep the lining part of the clutch disc, pressure plate and surface of the flywheel away from oil and foreign matter.</ptxt>
</atten3>
</content1></s-1>
<s-1 id="RM0000031W7007X_01_0013" proc-id="RM22W0E___000056F00001">
<ptxt>REMOVE DRIVE PLATE AND RING GEAR SUB-ASSEMBLY (for Automatic Transmission)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a wrench, hold the crankshaft.</ptxt>
<figure>
<graphic graphicname="A185177" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not turn the crankshaft counterclockwise.</ptxt>
<ptxt>If it is turned counterclockwise, check that the crankshaft pulley set bolt is not loose. If loose, tighten the bolt (See page <xref label="Seep01" href="RM0000032AA008X_01_0153"/>).</ptxt>
</atten3>
<torque>
<torqueitem>
<t-value1>108</t-value1>
<t-value2>1101</t-value2>
<t-value4>80</t-value4>
<ptxt>or more</ptxt>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Remove the 8 bolts.</ptxt>
<figure>
<graphic graphicname="A177363" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the rear drive plate spacer labeled A, drive plate and ring gear labeled B and rear crankshaft balancer weight labeled C.</ptxt>
<figure>
<graphic graphicname="A177364E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000031W7007X_01_0010" proc-id="RM22W0E___000056G00001">
<ptxt>REMOVE FLYWHEEL SUB-ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a wrench, hold the crankshaft.</ptxt>
<figure>
<graphic graphicname="A185177" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not turn the crankshaft counterclockwise.</ptxt>
<ptxt>If it is turned counterclockwise, check that the crankshaft pulley set bolt is not loose. If loose, tighten the bolt (See page <xref label="Seep01" href="RM0000032AA008X_01_0153"/>).</ptxt>
</atten3>
<torque>
<torqueitem>
<t-value1>108</t-value1>
<t-value2>1101</t-value2>
<t-value4>80</t-value4>
<ptxt>or more</ptxt>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Remove the 8 bolts and flywheel.</ptxt>
<figure>
<graphic graphicname="A177365" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000031W7007X_01_0004" proc-id="RM22W0E___00005KX00001">
<ptxt>REMOVE REAR CRANKSHAFT OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a knife, cut off the lip of the oil seal.</ptxt>
<figure>
<graphic graphicname="A177361E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Cut Position</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Oil Seal</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a screwdriver, pry out the oil seal.</ptxt>
<atten3>
<ptxt>Be careful not to damage the crankshaft.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>