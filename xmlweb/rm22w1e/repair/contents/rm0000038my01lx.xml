<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TZ_T00N2" variety="T00N2">
<name>STEERING PAD</name>
<para id="RM0000038MY01LX" category="N" type-id="3000N" name-id="RSGXS-01" from="201308">
<name>DISPOSAL</name>
<subpara id="RM0000038MY01LX_02" type-id="11" category="10" proc-id="RM22W0E___0000FH300001">
<content3 releasenbr="1">
<atten4>
<ptxt>When scrapping a vehicle equipped with the SRS or disposing of the steering pad, be sure to deploy the airbag first in accordance with the procedure described below. If any abnormality occurs with the airbag deployment, contact the SERVICE DEPT. of the DISTRIBUTOR..</ptxt>
</atten4>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Never dispose of a steering pad that has an unactivated airbag.</ptxt>
</item>
<item>
<ptxt>The airbag produces an exploding sound when it is deployed, so perform the operation outdoors and where it will not create a nuisance to nearby residents.</ptxt>
</item>
<item>
<ptxt>When deploying the airbag, always use the specified SST (SRS Airbag Deployment Tool). Perform the operation in a place away from electrical noise.</ptxt>
</item>
<item>
<ptxt>When deploying the airbag, perform the operation at least 10 m (32.8 ft.) away from the steering pad.</ptxt>
</item>
<item>
<ptxt>The steering pad becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a steering pad with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water, etc. to a steering pad with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
<item>
<ptxt>An airbag or pretensioner may be activated by static electricity. To prevent this, be sure to touch a metal surface with bare hands to discharge static electricity before performing this procedure.</ptxt>
</item>
</list1>
</atten2>
</content3>
</subpara>
<subpara id="RM0000038MY01LX_01" type-id="01" category="01">
<s-1 id="RM0000038MY01LX_01_0001" proc-id="RM22W0E___0000FH100001">
<ptxt>DISPOSE OF STEERING PAD (WHEN INSTALLED IN VEHICLE)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Prepare a battery as the power source to deploy the airbag.</ptxt>
</atten4>
<s2>
<ptxt>Check the function of SST.</ptxt>
<figure>
<graphic graphicname="C110371E12" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09082-00700</s-number>
</sstitem>
</sst>
<atten2>
<ptxt>When deploying the airbag, always use the specified SST (SRS Airbag Deployment Tool).</ptxt>
</atten2>
<s3>
<ptxt>Connect SST to the battery. Connect the red clip of SST to the battery positive (+) terminal and the black clip of SST to the battery negative (-) terminal.</ptxt>
<figure>
<graphic graphicname="C110372E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Check the function of SST. Press SST activation switch, and check that the LED of SST activation switch comes on. </ptxt>
<figure>
<graphic graphicname="C110475E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Do not connect SST connector (yellow colored) to the airbag.</ptxt>
</item>
<item>
<ptxt>If the LED comes on when the activation switch is not being pressed, SST malfunction is possible, so replace SST with a new one.</ptxt>
</item>
</list1>
</atten2>
</s3>
<s3>
<ptxt>Disconnect SST from the battery.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Read the precaution (See page <xref label="Seep02" href="RM000000KT10L4X"/>).</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the steering column cover lower.</ptxt>
<figure>
<graphic graphicname="B181570" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>While turning the steering wheel to the right and left, remove the 3 screws and steering column cover lower.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install SST.</ptxt>
<figure>
<graphic graphicname="B181572" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Check that there is no looseness in the steering wheel assembly and steering pad.</ptxt>
</atten2>
<s3>
<ptxt>Disconnect the airbag connector (yellow colored) from the spiral cable with steering sensor. </ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Connect SST connector to the airbag connector of the spiral cable with steering sensor.</ptxt>
<sst>
<sstitem>
<s-number>09082-00780</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>To avoid damaging SST connector and wire harness, do not lock the secondary lock of the twin lock.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Move SST at least 10 m (32.8 ft.) away from the vehicle front side window. </ptxt>
<figure>
<graphic graphicname="C110476E12" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Maintaining enough clearance for SST wire harness in the front side window, close all doors and windows of the vehicle.</ptxt>
<atten3>
<ptxt>Take care not to damage SST wire harness.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Connect the red clip of SST to the battery positive (+) terminal and the black clip of SST to the negative (-) terminal. </ptxt>
</s3>
</s2>
<s2>
<ptxt>Deploy the airbag.</ptxt>
<s3>
<ptxt>Check that no one is inside the vehicle or within a 10 m (32.8 ft.) radius of the vehicle.</ptxt>
</s3>
<s3>
<ptxt>Press SST activation switch to deploy the airbag.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>When deploying the airbag, make sure that no one is near the vehicle.</ptxt>
</item>
<item>
<ptxt>The steering pad becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a steering pad with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water to a steering pad with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
</list1>
</atten2>
<atten4>
<ptxt>The airbag is deployed as the LED of SST activation switch comes on.</ptxt>
</atten4>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038MY01LX_01_0002" proc-id="RM22W0E___0000FH200001">
<ptxt>DISPOSE OF STEERING PAD (WHEN NOT INSTALLED IN VEHICLE)</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Be sure to follow the procedure detailed below when deploying the airbag.</ptxt>
</atten2>
<atten4>
<ptxt>Prepare a battery as the power source to deploy the airbag.</ptxt>
</atten4>
<s2>
<ptxt>Check the function of SST.</ptxt>
<sst>
<sstitem>
<s-number>09082-00700</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Remove the steering pad (See page <xref label="Seep01" href="RM0000038N101IX"/>).</ptxt>
<figure>
<graphic graphicname="C110371E12" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<list1 type="unordered">
<item>
<ptxt>When removing the steering pad, wait at least 90 seconds after the ignition switch is turned off and the cable is disconnected from the negative (-) battery terminal before starting work.</ptxt>
</item>
<item>
<ptxt>When storing the steering pad, keep the airbag deployment side facing upward.</ptxt>
</item>
</list1>
</atten2>
</s2>
<s2>
<ptxt>Using a service-purpose wire harness for the vehicle, tie down the steering pad to a disc wheel.</ptxt>
<figure>
<graphic graphicname="B104882E14" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Cross-sectional area of stripped wire harness section</title>
<specitem>
<ptxt>1.25 mm<sup>2</sup> (0.00192 in.<sup>2</sup>)</ptxt>
</specitem>
</spec>
<atten2>
<ptxt>If the wire harness is too thin or an alternative object is used to tie down the steering pad, it may be snapped by the shock when the airbag is deployed. Always use a wire harness for vehicle use with a cross-sectional area of at least 1.25 mm<sup>2</sup> (0.00192 in.<sup>2</sup>).</ptxt>
</atten2>
<atten4>
<ptxt>To calculate the cross-sectional area of the stripped wire harness section: Area = 3.14 x (Diameter)<sup>2</sup> divided by 4</ptxt>
</atten4>
<s3>
<ptxt>Install 2 bolts with washers into the 2 bolt holes on the steering pad.</ptxt>
<figure>
<graphic graphicname="B181574E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Bolt</title>
<specitem>
<ptxt>L: 35.0 mm (1.38 in.)</ptxt>
<ptxt>M: 6.0 mm (0.236 in.)</ptxt>
<ptxt>Pitch: 1.0 mm (0.0393 in.) </ptxt>
</specitem>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Tighten the bolts by hand until they become difficult to turn.</ptxt>
</item>
<item>
<ptxt>Do not tighten the bolts excessively.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>After connecting SST to each other, connect them to the steering pad.</ptxt>
<figure>
<graphic graphicname="B181576E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09082-00820</s-number>
</sstitem>
</sst>
</s3>
<s3>
<ptxt>Wind 3 wire harnesses at least 2 times each around the bolts installed on the left and right sides of the steering pad.</ptxt>
<figure>
<graphic graphicname="B181578E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Tightly wind the wire harnesses around the bolts so that there is no slack.</ptxt>
</item>
<item>
<ptxt>Make sure that the wire harnesses are tight. If there is slack in the wire harnesses, the steering pad may become loose due to the shock when the airbag is deployed.</ptxt>
</item>
</list1>
</atten2>
</s3>
<s3>
<ptxt>Face the airbag deployment side of the steering pad upward. Separately tie the left and right sides of the steering pad to a disc wheel through the hub nut holes. Position SST connector so that it hangs downward through the hub hole of the disc wheel.</ptxt>
<figure>
<graphic graphicname="B181580" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Make sure that the wire harnesses are tight. If there is slack in the wire harnesses, the steering pad may become loose due to the shock when the airbag is deployed.</ptxt>
</item>
<item>
<ptxt>Always tie down the steering pad with the airbag deployment side facing upward.</ptxt>
</item>
</list1>
</atten2>
<atten3>
<ptxt>As the disc wheel may be damaged by the airbag deployment, use a disc wheel that you are planning to throw away.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Install SST.</ptxt>
<figure>
<graphic graphicname="B181582E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Place the disc wheel on level ground.</ptxt>
</atten2>
<s3>
<ptxt>Connect SST connector.</ptxt>
<sst>
<sstitem>
<s-number>09082-00700</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>To avoid damaging SST connector and wire harness, do not lock the secondary lock of the twin lock. Also, secure some slack for SST wire harness inside the disc wheel.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Move SST at least 10 m (32.8 ft.) away from the steering pad tied down to the disc wheel.</ptxt>
<figure>
<graphic graphicname="B181584E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Cover the steering pad with a cardboard box or tires.</ptxt>
<figure>
<graphic graphicname="B181586E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Covering method using a cardboard box: Cover the steering pad with the cardboard box and place weights on the cardboard box in 4 places with a total of at least 190 N (19 kgf, 41.8 lbf).</ptxt>
<spec>
<title>Cardboard box size</title>
<specitem>
<ptxt>Must exceed the following dimensions</ptxt>
<ptxt>X:  460 mm (18.1 in.)</ptxt>
<ptxt>Y:  650 mm (25.6 in.)</ptxt>
</specitem>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When dimension Y of the cardboard box exceeds the diameter of the tire with disc wheel which the steering pad is tied to, X should be the following size. X = 460 mm (18.1 in.) + width of tire </ptxt>
</item>
<item>
<ptxt>If a cardboard box which is smaller than the specified size is used, the cardboard box will be damaged by the shock from the airbag deployment.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Covering method using tires: Place at least 3 tires without disc wheels on the tire with disc wheel which the steering pad is tied to. Place another tire with disc wheel on them.</ptxt>
<figure>
<graphic graphicname="B181588E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Tire size</title>
<specitem>
<ptxt>Must exceed the following dimensions </ptxt>
<ptxt>Width: 185 mm (7.28 in.)</ptxt>
<ptxt>Inner diameter: 360 mm (14.2 in.)</ptxt>
</specitem>
</spec>
<atten2>
<ptxt>Do not use tires with disc wheels except on the top and bottom.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>As the tires may be damaged by the airbag deployment, use tires that you are planning to throw away.</ptxt>
</item>
<item>
<ptxt>Do not place SST connector under the tire because it could be damaged.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Tie the tires together with 2 wire harnesses.</ptxt>
<figure>
<graphic graphicname="C107322" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Make sure that the wire harnesses are tight. Looseness in the wire harnesses results in the tires coming free due to the shock when the airbag is deployed.</ptxt>
</atten2>
</s3>
</s2>
<s2>
<ptxt>Deploy the airbag.</ptxt>
<figure>
<graphic graphicname="B181590E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<s3>
<ptxt>Connect the red clip of SST to the battery positive (+) terminal and the black clip of SST to the battery negative (-) terminal. </ptxt>
</s3>
<s3>
<ptxt>Check that no one is within a 10 m (32.8 ft.) radius of the disc wheel which the steering pad is tied to.</ptxt>
</s3>
<s3>
<ptxt>Press SST activation switch to deploy the airbag. </ptxt>
<atten2>
<ptxt>When deploying the airbag, make sure that no one is near the tire.</ptxt>
</atten2>
<atten4>
<ptxt>The airbag is deployed as the LED of SST activation switch comes on. </ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Dispose of the steering pad.</ptxt>
<figure>
<graphic graphicname="B181592" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<list1 type="unordered">
<item>
<ptxt>The steering pad becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a steering pad with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water to a steering pad with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
</list1>
</atten2>
<s3>
<ptxt>Remove the steering pad from the disc wheel.</ptxt>
</s3>
<s3>
<ptxt>Place the steering pad in a plastic bag, tie it tightly and dispose of it in the same way as other general parts.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>