<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000D" variety="S000D">
<name>1VD-FTV ENGINE MECHANICAL</name>
<ttl id="12007_S000D_7C3FT_T008W" variety="T008W">
<name>CYLINDER BLOCK</name>
<para id="RM000002BK5045X" category="A" type-id="80002" name-id="EM35G-01" from="201301">
<name>DISASSEMBLY</name>
<subpara id="RM000002BK5045X_01" type-id="01" category="01">
<s-1 id="RM000002BK5045X_01_0041" proc-id="RM22W0E___00005BH00000">
<ptxt>INSPECT CONNECTING ROD THRUST CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the thrust clearance while moving the connecting rod back and forth.</ptxt>
<figure>
<graphic graphicname="A164562" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard thrust clearance</title>
<specitem>
<ptxt>0.14 to 0.54 mm (0.00551 to 0.0213 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum thrust clearance</title>
<specitem>
<ptxt>0.60 mm (0.0236 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the thrust clearance is more than the maximum, replace one or more connecting rods as necessary.</ptxt>
<ptxt>If necessary, replace the crankshaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0043" proc-id="RM22W0E___00005OG00000">
<ptxt>REMOVE PISTON SUB-ASSEMBLY WITH CONNECTING ROD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a ridge reamer, remove all the carbon from the top of the cylinder.</ptxt>
<figure>
<graphic graphicname="A164635" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Uniformly loosen the connecting rod bolts, and remove the connecting rod cap with bearing.</ptxt>
</s2>
<s2>
<ptxt>Push out the piston and connecting rod with bearing through the top of the cylinder block.</ptxt>
<atten4>
<ptxt>Arrange the piston and connecting rod assemblies in the correct order.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0044" proc-id="RM22W0E___00005OH00000">
<ptxt>REMOVE CONNECTING ROD BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the connecting rod and connecting rod cap bearings.</ptxt>
<figure>
<graphic graphicname="A164871" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Arrange the removed parts in the correct order.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0045" proc-id="RM22W0E___00005OI00000">
<ptxt>REMOVE PISTON RING SET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a piston ring expander, remove the No. 1 and No. 2 compression rings.</ptxt>
<figure>
<graphic graphicname="A164601" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a piston ring expander, remove the oil ring.</ptxt>
</s2>
<s2>
<ptxt>Remove the oil ring expander by hand.</ptxt>
<atten4>
<ptxt>Arrange the removed parts in the correct order.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0047" proc-id="RM22W0E___00005OJ00000">
<ptxt>REMOVE PISTON WITH PIN SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the fitting condition between the piston and piston pin by trying to move the piston back and forth on the piston pin.</ptxt>
<figure>
<graphic graphicname="A164623" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If any movement is felt, replace the piston and pin as a set.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, pry out the 2 snap rings.</ptxt>
<figure>
<graphic graphicname="A164602" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Gradually heat the piston to approximately 80°C (176°F).</ptxt>
<figure>
<graphic graphicname="A164823E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer and brass bar, lightly tap out the piston pin and remove the connecting rod.</ptxt>
<figure>
<graphic graphicname="A164603" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The piston and pin are a matched set.</ptxt>
</item>
<item>
<ptxt>Arrange the piston and connecting rod assemblies in the correct order.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0064" proc-id="RM22W0E___00005OO00000">
<ptxt>CLEAN PISTON</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a gasket scraper, remove the carbon from the piston top.</ptxt>
<figure>
<graphic graphicname="A164604" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a groove cleaning tool or broken ring, clean the piston ring grooves.</ptxt>
<figure>
<graphic graphicname="A164605" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using solvent and a brush, thoroughly clean the piston.</ptxt>
<figure>
<graphic graphicname="A164842" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not use a wire brush.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0048" proc-id="RM22W0E___00005BG00000">
<ptxt>INSPECT CRANKSHAFT THRUST CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the thrust clearance while prying the crankshaft back and forth with a screwdriver.</ptxt>
<figure>
<graphic graphicname="A164572" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard thrust clearance</title>
<specitem>
<ptxt>0.02 to 0.22 mm (0.000787 to 0.00866 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum thrust clearance</title>
<specitem>
<ptxt>0.30 mm (0.0118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the thrust clearance is more than the maximum, replace the thrust washers as a set.</ptxt>
<spec>
<title>Standard thrust washer thickness</title>
<specitem>
<ptxt>2.44 to 2.49 mm (0.0961 to 0.0980 in.)</ptxt>
</specitem>
</spec>
<ptxt>If necessary, replace the crankshaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0049" proc-id="RM22W0E___00005OK00000">
<ptxt>REMOVE CRANKSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using several steps, loosen and remove the 10 crankshaft bearing cap set bolts uniformly in the sequence shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A164796E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using several steps, loosen and remove the 20 crankshaft bearing cap set bolts uniformly in the sequence shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A164797E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 cylinder block stiffening plates.</ptxt>
<figure>
<graphic graphicname="A164799" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using 2 crankshaft bearing cap set bolts, pull out the 5 bearing caps.</ptxt>
<figure>
<graphic graphicname="A164847" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Take care not to damage the contact surfaces of the crankshaft bearing cap and cylinder block.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Keep the lower bearing and crankshaft bearing cap as a set.</ptxt>
</item>
<item>
<ptxt>Arrange the crankshaft bearing caps in the correct order.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Remove the crankshaft.</ptxt>
<figure>
<graphic graphicname="A164570" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0050" proc-id="RM22W0E___00005OL00000">
<ptxt>REMOVE CRANKSHAFT THRUST WASHER SET</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A164600" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0051" proc-id="RM22W0E___00005OM00000">
<ptxt>REMOVE CRANKSHAFT BEARING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A164798E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Keep the upper and lower crankshaft bearings and crankshaft bearing cap as a set.</ptxt>
</item>
<item>
<ptxt>Arrange the crankshaft bearing caps and bearings in the correct order.</ptxt>
</item>
</list1>
</atten4>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0052" proc-id="RM22W0E___00005ON00000">
<ptxt>REMOVE NO. 1 OIL NOZZLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 5 mm hexagon wrench, remove the 4 bolts and 4 oil nozzles.</ptxt>
<figure>
<graphic graphicname="A164571" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0066" proc-id="RM22W0E___00005OP00000">
<ptxt>INSPECT INPUT SHAFT BEARING (for Manual Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the bearing by hand.</ptxt>
<ptxt>If the bearing is stuck or difficult to turn, replace the input shaft bearing. For removal procedures, refer to the following (See page <xref label="Seep01" href="RM00000177U00ZX_02_0025"/>) and for installation procedures, refer to the following (See page <xref label="Seep02" href="RM00000177V00ZX_02_0025"/>).</ptxt>
<atten4>
<ptxt>The bearing is permanently lubricated and requires no cleaning or lubrication.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5045X_01_0067" proc-id="RM22W0E___00005OQ00000">
<ptxt>REMOVE CYLINDER BLOCK STRAIGHT SCREW PLUG</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A177329E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Remove the 2 straight screw plugs and 2 gaskets.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>