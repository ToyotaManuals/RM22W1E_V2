<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12060_S002C" variety="S002C">
<name>SEAT BELT</name>
<ttl id="12060_S002C_7C3VF_T00OI" variety="T00OI">
<name>REAR NO. 2 SEAT OUTER BELT ASSEMBLY</name>
<para id="RM0000039ES01EX" category="A" type-id="80001" name-id="SB6LM-01" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000039ES01EX_02" type-id="11" category="10" proc-id="RM22W0E___0000GIX00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039ES01EX_01" type-id="01" category="01">
<s-1 id="RM0000039ES01EX_01_0001" proc-id="RM22W0E___0000GIU00001">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 2 seat assembly (See page <xref label="Seep01" href="RM00000391S00ZX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ES01EX_01_0012" proc-id="RM22W0E___0000CG200001">
<ptxt>REMOVE REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 6 clips and remove the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039ES01EX_01_0018" proc-id="RM22W0E___0000CG300001">
<ptxt>REMOVE REAR STEP COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181673" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to remove the step cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 2 claws and remove the step cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039ES01EX_01_0013" proc-id="RM22W0E___0000FKY00001">
<ptxt>REMOVE REAR DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181674" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws and 4 clips, and remove the scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039ES01EX_01_0015" proc-id="RM22W0E___0000GIG00001">
<ptxt>REMOVE REAR NO. 2 SEAT COVER BEZEL
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181698" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 6 clips and remove the rear No. 2 seat cover bezel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039ES01EX_01_0006" proc-id="RM22W0E___0000GIV00001">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182644" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When installing the front quarter trim panel, operate the reclining adjuster release handle and move the No. 1 rear seat to the position shown in the illustration.</ptxt>
</atten4>
<figure>
<graphic graphicname="B181687" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 3 claws and remove the cover.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B181689" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and disconnect the rear seat outer belt floor anchor.</ptxt>
</s2>
<s2>
<ptxt>for LH Side:</ptxt>
<figure>
<graphic graphicname="B181691" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the bolt and disconnect the rear No. 2 seat outer belt floor anchor LH.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for RH Side:</ptxt>
<figure>
<graphic graphicname="B181692" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Detach the 3 claws and remove the cover.</ptxt>
</s3>
<s3>
<figure>
<graphic graphicname="B181693" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and disconnect the rear seat outer belt floor anchor RH.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Remove the clip and bolt.</ptxt>
</s2>
<s2>
<ptxt>Detach the 18 clips and 2 claws.</ptxt>
<figure>
<graphic graphicname="B186889" width="7.106578999in" height="4.7836529in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Rear Air Conditioning System:</ptxt>
<ptxt>Disconnect the rear seat lock control lever cable and then remove the quarter trim panel.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<ptxt>Disconnect the thermistor connector and rear seat lock control lever cable, and then remove the quarter trim panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ES01EX_01_0010" proc-id="RM22W0E___0000GIW00001">
<ptxt>REMOVE REAR NO. 2 SEAT OUTER BELT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the seat belt shoulder anchor.</ptxt>
<figure>
<graphic graphicname="B183808E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Using a screwdriver, detach the 2 claws and open the seat belt anchor cover as shown in the illustration.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s3>
<s3>
<figure>
<graphic graphicname="B183809" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and seat belt shoulder anchor.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B180638" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and rear No. 2 seat outer belt.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>