<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12067_S002J" variety="S002J">
<name>MIRROR (EXT)</name>
<ttl id="12067_S002J_7C3Y4_T00R7" variety="T00R7">
<name>OUTER MIRROR SWITCH</name>
<para id="RM000001DHF05PX" category="G" type-id="3001K" name-id="MX0XJ-02" from="201301">
<name>INSPECTION</name>
<subpara id="RM000001DHF05PX_01" type-id="01" category="01">
<s-1 id="RM000001DHF05PX_01_0005" proc-id="RM22W0E___0000IOI00000">
<ptxt>INSPECT OUTER MIRROR SWITCH ASSEMBLY (w/o Retract Mirror)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180592E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Inspect the outer mirror switch assembly.</ptxt>
<s3>
<ptxt>Select "L" on the left/right adjustment switch.</ptxt>
</s3>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COLSPEC0" colwidth="1.03in"/>
<colspec colname="COL3" colwidth="1.04in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry namest="COL2" nameend="COLSPEC0" valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 (MLV) - 4 (B)</ptxt>
<ptxt>7 (M+) - 8 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 (MLV) - 8 (E)</ptxt>
<ptxt>7 (M+) - 4 (B)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Down</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>9 (MLH) - 4 (B)</ptxt>
<ptxt>7 (M+) - 8 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Left</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>9 (MLH) - 8 (E)</ptxt>
<ptxt>7 (M+) - 4 (B)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Right</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the outer mirror switch assembly.</ptxt>
</s3>
<s3>
<ptxt>Select "R" on the left/right adjustment switch.</ptxt>
</s3>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COLSPEC0" colwidth="1.03in"/>
<colspec colname="COL3" colwidth="1.04in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry namest="COL2" nameend="COLSPEC0" valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>2 (MRV) - 4 (B)</ptxt>
<ptxt>7 (M+) - 8 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>2 (MRV) - 8 (E)</ptxt>
<ptxt>7 (M+) - 4 (B)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Down</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 (MRH) - 4 (B)</ptxt>
<ptxt>7 (M+) - 8 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Left</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 (MRH) - 8 (E)</ptxt>
<ptxt>7 (M+) - 4 (B)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Right</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the outer mirror switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the switch illumination.</ptxt>
<s3>
<ptxt>Apply battery voltage between the terminals of the light, and check the operation of the light.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 5 (ILL+)</ptxt>
<ptxt>Battery negative (-) → Terminal 6 (ILL-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Light comes on</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the outer mirror switch assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001DHF05PX_01_0002" proc-id="RM22W0E___0000IOG00000">
<ptxt>INSPECT OUTER MIRROR SWITCH ASSEMBLY (w/ Retract Mirror)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180592E05" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Inspect the outer mirror switch assembly.</ptxt>
<s3>
<ptxt>Select "L" on the left/right adjustment switch.</ptxt>
</s3>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COLSPEC2" colwidth="1.03in"/>
<colspec colname="COL3" colwidth="1.04in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry namest="COL2" nameend="COLSPEC2" valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>8 (MSW) - 9 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>L position</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>7 (M+) - 9 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>95 to 105 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>7 (M+) - 9 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Down</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>446 to 493 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>7 (M+) - 9 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Left</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>760 to 840 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>7 (M+) - 9 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Right</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>237 to 262 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the outer mirror switch assembly.</ptxt>
</s3>
<s3>
<ptxt>Select "R" on the left/right adjustment switch.</ptxt>
</s3>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COLSPEC2" colwidth="1.03in"/>
<colspec colname="COL3" colwidth="1.04in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry namest="COL2" nameend="COLSPEC2" valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>8 (MSW) - 9 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>R position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>7 (M+) - 9 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>95 to 105 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>7 (M+) - 9 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Down</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>446 to 493 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>7 (M+) - 9 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Left</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>760 to 840 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>7 (M+) - 9 (E)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Right</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>237 to 262 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the outer mirror switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the switch illumination.</ptxt>
<s3>
<ptxt>Apply battery voltage between the terminals of the light, and check the operation of the light.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 5 (ILL+)</ptxt>
<ptxt>Battery negative (-) → Terminal 6 (ILL-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Light comes on</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the outer mirror switch assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001DHF05PX_01_0004" proc-id="RM22W0E___0000IOH00000">
<ptxt>INSPECT RETRACTABLE OUTER MIRROR SWITCH (w/ Retract Mirror)</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="E194319E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Inspect the retractable outer mirror switch.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>5 (E) - 6 (MR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
<ptxt>(Retracts)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the retractable outer mirror switch.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the switch illumination.</ptxt>
<s3>
<ptxt>Apply battery voltage between the terminals of the light, and check the operation of the light.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 8 (ILL+)</ptxt>
<ptxt>Battery negative (-) → Terminal 7 (ILL-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Light comes on</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the retractable outer mirror switch.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>