<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T8_T00MB" variety="T00MB">
<name>THEFT DETERRENT SYSTEM (w/ Entry and Start System)</name>
<para id="RM000002TGC01HX" category="T" type-id="3001H" name-id="TD0K3-28" from="201301" to="201308">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM000002TGC01HX_z0" proc-id="RM22W0E___0000EK300000">
<content5 releasenbr="11">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</item>
<item>
<ptxt>Troubleshooting of the theft deterrent system is based on the premise that the door lock control system, the wireless door lock control system, the lighting system, the horn system, the engine immobiliser system and the entry and start system are operating normally. Check these systems first before troubleshooting the theft deterrent system.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Theft Deterrent System</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Theft deterrent system cannot be set</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Engine hood courtesy switch circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000W1002UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Door courtesy switch circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002ZS001WX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Certification ECU power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000W4603VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Security indicator is not blinking when theft deterrent system is set</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Security indicator light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000W1208LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Alarm sounding state cannot be canceled when engine switch is turned on (IG)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>IG power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000Y4B022X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="1" colsep="1" valign="middle" align="left">
<ptxt>Theft deterrent system can be set even when any door is open</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Engine hood courtesy switch circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000W1002UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Door courtesy switch circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002ZS001WX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Horns (low pitched, high pitched) do not sound while theft deterrent system is in alarm sounding state</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Horn system</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002W3D017X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Hazard warning lights do not flash while theft deterrent system is in warning operation</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Hazard warning switch circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002ZNJ01OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Security horn does not sound while theft deterrent system is set*1</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Security horn circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003BI401IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Headlights do not flash while theft deterrent system is in alarm sounding state*1</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Headlight signal circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002WKT01AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Taillights do not flash while theft deterrent system is in alarm sounding state*1</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Taillight circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002WKW011X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Map light does not illuminate while theft deterrent system is in alarm sounding state</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Illumination circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002WKY00TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Theft warning siren does not sound when theft deterrent system is in alarm sounding state*2</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Theft warning siren circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000012QB02LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Intrusion sensor cancel switch does not operate*3</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Intrusion sensor cancel switch circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001T49026X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Glass breakage sensor does not operate*4</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Glass breakage sensor circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000215L01YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: w/ Security Horn</ptxt>
</item>
<item>
<ptxt>*2: w/ Theft Warning Siren</ptxt>
</item>
<item>
<ptxt>*3: w/ Intrusion Sensor</ptxt>
</item>
<item>
<ptxt>*4: w/ Glass Breakage Sensor</ptxt>
</item>
</list1>
</atten4>
<list1 type="nonmark">
<item>
<ptxt>If the suspected areas operate normally, replace the certification ECU (smart key ECU assembly).</ptxt>
</item>
</list1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>