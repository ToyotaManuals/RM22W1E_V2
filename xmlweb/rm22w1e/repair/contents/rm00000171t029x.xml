<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001M" variety="S001M">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001M_7C3PP_T00IS" variety="T00IS">
<name>HYDRAULIC BRAKE BOOSTER (for RHD)</name>
<para id="RM00000171T029X" category="A" type-id="80001" name-id="BR6A1-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM00000171T029X_01" type-id="11" category="10" proc-id="RM22W0E___0000AVQ00000">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure not to allow the pump to contact the floor; support it with the booster body. If the pump contacts the floor, the red brake tube between the pump and booster body may be deformed, the operating sound of the pump may become loud enough to hear within the cabin, and fluid may leak.</ptxt>
</item>
<item>
<ptxt>When installing, coat the parts indicated by arrows with lithium soap base glycol grease (See page <xref label="Seep01" href="RM00000171S01RX"/>).</ptxt>
</item>
<item>
<ptxt>As high pressure is applied to the brake actuator tube No. 1, never deform it.</ptxt>
</item>
<item>
<ptxt>Until the work is over, do not turn the ignition switch on.</ptxt>
</item>
<item>
<ptxt>Before starting the work, make sure that the ignition switch is off and depress the brake pedal more than 20 times.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>When pressure in the power supply system is released, the reaction force becomes light and the stroke becomes longer.</ptxt>
</atten4>
</content3>
</subpara>
<subpara id="RM00000171T029X_02" type-id="01" category="01">
<s-1 id="RM00000171T029X_02_0031" proc-id="RM22W0E___0000AVT00000">
<ptxt>DRAIN BRAKE FLUID</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Wash off brake fluid immediately if it comes into contact with a painted surface.</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000171T029X_02_0043" proc-id="RM22W0E___0000AVV00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000171T029X_02_0001" proc-id="RM22W0E___0000AVR00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000171T029X_02_0023">
<ptxt>REMOVE FRONT DOOR SCUFF PLATE RH</ptxt>
</s-1>
<s-1 id="RM00000171T029X_02_0033" proc-id="RM22W0E___000014A00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180655" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the No. 1 instrument panel under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171T029X_02_0040" proc-id="RM22W0E___0000AU200000">
<ptxt>REMOVE COWL SIDE TRIM BOARD RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180022" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the cap nut.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clips and remove the cowl side trim board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171T029X_02_0034" proc-id="RM22W0E___0000A9S00000">
<ptxt>REMOVE LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180295E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the hole cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 16 claws.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 9 claws.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Detach the 2 claws and remove the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Detach the 2 claws and disconnect the 2 control cables.</ptxt>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the lower No. 1 instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171T029X_02_0041" proc-id="RM22W0E___0000ATC00000">
<ptxt>REMOVE DRIVER SIDE KNEE AIRBAG ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and driver side knee airbag.</ptxt>
<figure>
<graphic graphicname="B189602E01" width="2.775699831in" height="6.791605969in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000171T029X_02_0042" proc-id="RM22W0E___0000ATZ00000">
<ptxt>REMOVE PUSH ROD PIN
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip and push rod pin from the brake pedal lever.</ptxt>
<figure>
<graphic graphicname="C172862" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000171T029X_02_0030" proc-id="RM22W0E___0000AVS00000">
<ptxt>REMOVE HYDRAULIC BRAKE BOOSTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 3 connectors from the hydraulic brake booster.</ptxt>
<figure>
<graphic graphicname="C172863" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Use tags or make a memo to identify the place to reconnect.</ptxt>
<figure>
<graphic graphicname="F051200E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a union nut wrench, disconnect the 4 brake lines from the hydraulic brake booster.</ptxt>
<figure>
<graphic graphicname="C172864E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the clamp from the hydraulic brake booster assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 nuts and pull out the hydraulic brake booster assembly.</ptxt>
<figure>
<graphic graphicname="C172861" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171T029X_02_0032" proc-id="RM22W0E___0000AVU00000">
<ptxt>REMOVE BRAKE BOOSTER GASKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the brake booster gasket from the hydraulic brake booster assembly.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>