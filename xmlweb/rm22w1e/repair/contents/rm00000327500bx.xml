<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12073_S002O" variety="S002O">
<name>WINCH</name>
<ttl id="12073_S002O_7C40N_T00TQ" variety="T00TQ">
<name>REMOTE CONTROL SWITCH</name>
<para id="RM00000327500BX" category="G" type-id="8000T" name-id="WI00X-02" from="201308">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM00000327500BX_01" type-id="01" category="01">
<s-1 id="RM00000327500BX_01_0001" proc-id="RM22W0E___0000JTI00001">
<ptxt>INSPECT REMOTE CONTROL SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the remote control switch connector.</ptxt>
<figure>
<graphic graphicname="B180477" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the engine switch on (IG) and check that the power indicator light illuminates.</ptxt>
<figure>
<graphic graphicname="B167941" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Press the remote control switch, and check that the winch operates.</ptxt>
<atten3>
<ptxt>Be careful not to tighten or strain the winch wire.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Turn the engine switch off.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000327500BX_01_0015" proc-id="RM22W0E___0000JC800001">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000327500BX_01_0016" proc-id="RM22W0E___0000JC900001">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000327500BX_01_0017" proc-id="RM22W0E___0000JTK00001">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL</ptxt>
<content1 releasenbr="2">
<ptxt>(See page <xref label="Seep01" href="RM000003CJV003X_01_0001"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM00000327500BX_01_0027" proc-id="RM22W0E___000058700001">
<ptxt>REMOVE FRONT BUMPER WINCH COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws.</ptxt>
<figure>
<graphic graphicname="B302213" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 3 guides and remove front bumper winch cover sub-assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000327500BX_01_0019" proc-id="RM22W0E___000049G00000">
<ptxt>REMOVE RADIATOR GRILLE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the radiator grille assembly.</ptxt>
<figure>
<graphic graphicname="B302174E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clips and 8 claws, and remove the radiator grille assembly.</ptxt>
</s2>
<s2>
<ptxt>w/ Wide View Front Monitor System:</ptxt>
<s3>
<ptxt>Disconnect the connector.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM00000327500BX_01_0020" proc-id="RM22W0E___0000JCA00001">
<ptxt>REMOVE FRONT BUMPER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the bumper cover.</ptxt>
<figure>
<graphic graphicname="B302214E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a T30 "TORX" socket, remove the 6 screws.</ptxt>
<figure>
<graphic graphicname="B313279E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>LH Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>RH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 3 clips, 4 screws and 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 10 claws.</ptxt>
<figure>
<graphic graphicname="B302215" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the winch control wire connector and remove the front bumper cover.</ptxt>
</s2>
<s2>
<ptxt>w/ Fog Light:</ptxt>
<ptxt>Disconnect the No. 4 engine room wire connector and remove the front bumper cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000327500BX_01_0002" proc-id="RM22W0E___0000JTJ00001">
<ptxt>INSPECT OVERHEAT TEMPERATURE INDICATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 screws and cover.</ptxt>
<figure>
<graphic graphicname="B180478" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the motor relay connector.</ptxt>
<figure>
<graphic graphicname="B180479" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts.</ptxt>
<figure>
<graphic graphicname="B180480" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Pull the bumper side winch control wire out of the bumper.</ptxt>
</s2>
<s2>
<ptxt>Connect the bumper side winch control wire connector to the vehicle side winch control wire connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the bumper side winch control wire connector to the winch control switch connector.</ptxt>
</s2>
<s2>
<ptxt>Turn the engine switch on (IG) and check that the overheat temperature indicator light illuminates and the buzzer sounds.</ptxt>
<figure>
<graphic graphicname="B167943" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the engine switch off.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the bumper side winch control wire connector from the winch control switch connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the bumper side winch control wire connector from the vehicle side winch control wire connector.</ptxt>
</s2>
<s2>
<ptxt>Pass the bumper side winch control wire through the bumper.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 nuts.</ptxt>
<figure>
<graphic graphicname="B180480" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the motor relay connector.</ptxt>
<figure>
<graphic graphicname="B180479" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the cover with the 4 screws.</ptxt>
<figure>
<graphic graphicname="B180478" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000327500BX_01_0021" proc-id="RM22W0E___000055300001">
<ptxt>INSTALL FRONT BUMPER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Fog Light:</ptxt>
<ptxt>Connect the No. 4 engine room wire connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the winch control wire connector and attach the 10 claws to install the front bumper cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 clips, 4 screws and 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Using a T30 "TORX" socket, install the 6 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000327500BX_01_0022" proc-id="RM22W0E___000047X00000">
<ptxt>INSTALL RADIATOR GRILLE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Wide View Front Monitor System:</ptxt>
<s3>
<ptxt>Connect the connector.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Attach the 2 clips and 8 claws to install the radiator grille assembly.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000327500BX_01_0023">
<ptxt>INSTALL FRONT BUMPER WINCH COVER SUB-ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM00000327500BX_01_0024" proc-id="RM22W0E___0000JTL00001">
<ptxt>INSTALL UPPER RADIATOR SUPPORT SEAL</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>for Gasoline Engine:</ptxt>
<ptxt>Install the upper radiator support seal (See page <xref label="Seep01" href="RM000003CJT003X_01_0002"/>).</ptxt>
</s2>
<s2>
<ptxt>for Diesel Engine:</ptxt>
<ptxt>Install the upper radiator support seal (See page ).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000327500BX_01_0026" proc-id="RM22W0E___0000JBU00001">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000327500BX_01_0025" proc-id="RM22W0E___0000JBV00001">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly RH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and 2 screws.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>