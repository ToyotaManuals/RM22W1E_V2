<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002LLH01UX" category="C" type-id="803YC" name-id="ACBKW-02" from="201301" to="201308">
<dtccode>B14A2</dtccode>
<dtcname>Driver Side Solar Sensor Short Circuit</dtcname>
<subpara id="RM000002LLH01UX_01" type-id="60" category="03" proc-id="RM22W0E___0000H9300000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E132508E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>The automatic light control sensor (solar sensor), which is installed on the upper side of the instrument panel, detects sunlight and controls the air conditioning in auto mode. The output current from the solar sensor varies according to the amount of sunlight. When the sunlight increases, the output current increases. As the sunlight decreases, the output current decreases. The air conditioning amplifier assembly detects output current from the solar sensor.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B14A2</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>An open or short in the driver side solar sensor circuit.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Automatic light control sensor (solar sensor)</ptxt>
</item>
<item>
<ptxt>Harness or connector between solar sensor and air conditioning amplifier assembly</ptxt>
</item>
<item>
<ptxt>Harness or connector between solar sensor and main body ECU (cowl side junction block LH)</ptxt>
</item>
<item>
<ptxt>Main body ECU (cowl side junction block LH)</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTC B1244 is output at the same time, troubleshoot DTC B1244 first.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002LLH01UX_02" type-id="32" category="03" proc-id="RM22W0E___0000H9400000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E160577E10" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="E160577E11" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002LLH01UX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002LLH01UX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002LLH01UX_05_0001" proc-id="RM22W0E___0000H9500000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DRIVER SIDE SOLAR SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the driver side solar sensor is functioning properly. </ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Solar Sensor (D Side)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side solar sensor /</ptxt>
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side solar sensor voltage increases as brightness increases</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Open in the circuit: 0.</ptxt>
<ptxt>Short in the circuit: 255.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LLH01UX_05_0011" fin="true">OK</down>
<right ref="RM000002LLH01UX_05_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLH01UX_05_0004" proc-id="RM22W0E___0000H9700000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - AUTOMATIC LIGHT CONTROL SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E157179E04" width="2.775699831in" height="5.787629434in"/>
</figure>
<test1>
<ptxt>w/ Rear Heater</ptxt>
<test2>
<ptxt>Disconnect the F9 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E35 amplifier connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E35-7 (TSD) - F9-1 (TSL)*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-7 (TSD) - F9-2 (TSR)*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-7 (TSD) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test2>
</test1>
<test1>
<ptxt>w/o Rear Heater</ptxt>
<figure>
<graphic graphicname="E160313E02" width="2.775699831in" height="4.7836529in"/>
</figure>
<test2>
<ptxt>Disconnect the F9 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E81 amplifier connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E81-33 (TSD) - F9-1 (TSL)*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-33 (TSD) - F9-2 (TSR)*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-33 (TSD) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002LLH01UX_05_0005" fin="false">OK</down>
<right ref="RM000002LLH01UX_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLH01UX_05_0005" proc-id="RM22W0E___0000H9800000">
<testtitle>INSPECT AUTOMATIC LIGHT CONTROL SENSOR (TSL, TSR - CLTE VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E108193E23" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Remove the automatic light control sensor (See page <xref label="Seep01" href="RM0000024C2033X"/>).</ptxt>
</test1>
<test1>
<ptxt>Apply battery voltage between terminals 6 (CLTB) and 3 (CLTE) of the solar sensor.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 (TSL) - 3 (CLTE)*1</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Sensor exposed to electric light</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.8 to 4.3 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Sensor covered with a cloth</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 0.8 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>2 (TSR) - 3 (CLTE)*2</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Sensor exposed to electric light</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.8 to 4.3 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Sensor covered with a cloth</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 0.8 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>As the inspection light is moved away from the sensor, the voltage decreases.</ptxt>
</item>
<item>
<ptxt>Use an incandescent light for inspection. Bring it about 300 mm (11.8 in.) from the solar sensor.</ptxt>
</item>
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002LLH01UX_05_0002" fin="false">OK</down>
<right ref="RM000002LLH01UX_05_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLH01UX_05_0002" proc-id="RM22W0E___0000H9600000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - AUTOMATIC LIGHT CONTROL SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E157041E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the E2 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the F9 sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E2-20 (CLTB) - F9-6 (CLTB)</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-19 (CLTS) - F9-5 (CLTS)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-18 (CLTE) - F9-3 (CLTE)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-20 (CLTB) - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-19 (CLTS) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-18 (CLTE) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LLH01UX_05_0009" fin="true">OK</down>
<right ref="RM000002LLH01UX_05_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLH01UX_05_0011">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LLH01UX_05_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002LLH01UX_05_0010">
<testtitle>REPLACE AUTOMATIC LIGHT CONTROL SENSOR<xref label="Seep01" href="RM0000024C2033X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LLH01UX_05_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002LLH01UX_05_0009">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>