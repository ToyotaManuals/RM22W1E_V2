<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12037_S001R" variety="S001R">
<name>VARIABLE GEAR RATIO STEERING</name>
<ttl id="12037_S001R_7C3QA_T00JD" variety="T00JD">
<name>STEERING CONTROL ECU (for LHD)</name>
<para id="RM000003DZD00EX" category="A" type-id="80001" name-id="VG05T-02" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM000003DZD00EX_01" type-id="01" category="01">
<s-1 id="RM000003DZD00EX_01_0001" proc-id="RM22W0E___0000BBO00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After the engine switch is turned off, the navigation system requires approximately 90 seconds to record various types of memory and settings. As a result, after turning the engine switch off, wait 90 seconds or more before disconnecting the cable from the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003DZD00EX_01_0014" proc-id="RM22W0E___00008UQ00000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292994E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the No. 2 instrument panel finish panel cushion.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the No. 2 instrument panel finish panel cushion.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0015" proc-id="RM22W0E___00008UR00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292995" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the clip and screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 8 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors, detach the 2 clamps and remove the lower instrument panel pad sub-assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0016" proc-id="RM22W0E___0000BBR00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL FINISH PANEL CUSHION
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180004E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the No. 1 instrument panel finish panel cushion.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the No. 1 instrument panel finish panel cushion.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0017" proc-id="RM22W0E___0000BBS00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180005" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the clip and screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 7 claws and remove the lower instrument panel pad sub-assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0018" proc-id="RM22W0E___0000BBT00000">
<ptxt>REMOVE LOWER CENTER INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292996" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 7 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the lower center instrument cluster finish panel sub-assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0002" proc-id="RM22W0E___0000BB300000">
<ptxt>REMOVE INSTRUMENT SIDE PANEL LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154740E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 6 claws and remove the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0003" proc-id="RM22W0E___0000BB400000">
<ptxt>REMOVE NO. 1 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154744E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 3 claws and remove the No. 1 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0004" proc-id="RM22W0E___0000BB500000">
<ptxt>REMOVE NO. 2 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws and remove the No. 2 instrument cluster finish panel garnish.</ptxt>
<figure>
<graphic graphicname="B291251E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Entry and Start System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0006" proc-id="RM22W0E___0000BBP00000">
<ptxt>REMOVE FRONT DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0007" proc-id="RM22W0E___000014A00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY (w/ Floor Under Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180655" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the No. 1 instrument panel under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0008" proc-id="RM22W0E___0000A9R00000">
<ptxt>REMOVE COWL SIDE TRIM BOARD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181911" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the cap nut.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clips and remove the cowl side trim board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0009" proc-id="RM22W0E___0000A9S00000">
<ptxt>REMOVE LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180295E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the hole cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 16 claws.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 9 claws.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Detach the 2 claws and remove the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Detach the 2 claws and disconnect the 2 control cables.</ptxt>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the lower No. 1 instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0010" proc-id="RM22W0E___0000A9P00000">
<ptxt>REMOVE NO. 1 SWITCH HOLE BASE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180298" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the No. 1 switch hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0011" proc-id="RM22W0E___0000ATC00000">
<ptxt>REMOVE DRIVER SIDE KNEE AIRBAG ASSEMBLY (w/ Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and driver side knee airbag.</ptxt>
<figure>
<graphic graphicname="B189602E01" width="2.775699831in" height="6.791605969in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0012" proc-id="RM22W0E___0000BBC00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL SUB-ASSEMBLY (w/o Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180299" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws and disconnect the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Remove the 5 bolts and lower instrument panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DZD00EX_01_0013" proc-id="RM22W0E___0000BBQ00000">
<ptxt>REMOVE STEERING CONTROL ECU WITH JUNCTION BLOCK</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172815E01" width="2.775699831in" height="5.787629434in"/>
</figure>
<s2>
<ptxt>Rear Side:</ptxt>
<s3>
<ptxt>Disconnect the 7 connectors.</ptxt>
</s3>
<s3>
<ptxt>Detach the 2 clamps.</ptxt>
</s3>
<s3>
<ptxt>Remove the bolt and 2 nuts.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Front Side:</ptxt>
<s3>
<ptxt>Disconnect the 5 connectors.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Remove the steering control ECU with junction block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003DZD00EX_01_0019" proc-id="RM22W0E___0000BBU00000">
<ptxt>REMOVE STEERING CONTROL ECU</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C172816E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Remove the bolt, nut and steering control ECU from the junction block.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>