<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12065_S002H" variety="S002H">
<name>SLIDING ROOF / CONVERTIBLE</name>
<ttl id="12065_S002H_7C3XH_T00QK" variety="T00QK">
<name>SLIDING ROOF HOUSING</name>
<para id="RM000002LP9035X" category="A" type-id="30014" name-id="RF1CA-03" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000002LP9035X_01" type-id="01" category="01">
<s-1 id="RM000002LP9035X_01_0001" proc-id="RM22W0E___0000I7E00001">
<ptxt>INSTALL SLIDING ROOF HOUSING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the housing with the 8 bolts (vehicle body side) and 8 nuts.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 8 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the 8 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B178615" width="7.106578999in" height="4.7836529in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0019" proc-id="RM22W0E___0000I7O00001">
<ptxt>INSTALL SLIDING ROOF WEATHERSTRIP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the sliding roof weatherstrip as follows:</ptxt>
<s3>
<ptxt>Position the joint of the sliding roof weatherstrip at the rear center.</ptxt>
</s3>
<s3>
<ptxt>Align the alignment mark on the sliding roof weatherstrip with the middle marks at the corners of the sliding roof glass sub-assembly and install the sliding roof weatherstrip.</ptxt>
</s3>
<s3>
<ptxt>Install the lip of the sliding roof weatherstrip firmly.</ptxt>
<figure>
<graphic graphicname="B348976E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Correct</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Incorrect</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Joint</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle Rear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Alignment Mark</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*f</ptxt>
</entry>
<entry valign="middle">
<ptxt>Middle Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0003" proc-id="RM22W0E___0000I7F00001">
<ptxt>INSTALL SLIDING ROOF GLASS SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T25 "TORX" driver, temporarily install the glass with the 4 screws.</ptxt>
<figure>
<graphic graphicname="B178614" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Perform a level check.</ptxt>
<figure>
<graphic graphicname="B178629E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Check the difference in the level between the roof panel and the upper surface of the weatherstrip labeled "a" when the sliding roof glass is fully closed.</ptxt>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Area</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A - A</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>-1.5 to +1.5 mm (-0.0591 to +0.0591 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B - B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C - C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>D - D</ptxt>
</entry>
<entry>
<ptxt>-1.0 to +1.5 mm (-0.0394 to +0.0591 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>"+" represents the condition that the glass is above the panel level. "-" represents the condition that the glass is below the panel level.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Perform a gap check.</ptxt>
<ptxt>Check the gap between the roof panel and roof glass.</ptxt>
<atten3>
<ptxt>The gap must be even all around.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Tighten the 4 screws.</ptxt>
<figure>
<graphic graphicname="B178614" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0009" proc-id="RM22W0E___0000I7L00001">
<ptxt>CHECK FOR WATER LEAK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>After adjusting the sliding roof glass sub-assembly, check for water leakage into the vehicle interior.</ptxt>
</s2>
<s2>
<ptxt>If there are any leaks, readjust the sliding roof glass sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0005" proc-id="RM22W0E___0000I7H00001">
<ptxt>INSTALL SLIDING ROOF SIDE GARNISH LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B178613" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 5 claws to install the sliding roof side garnish.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0004" proc-id="RM22W0E___0000I7G00001">
<ptxt>INSTALL SLIDING ROOF SIDE GARNISH RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0020" proc-id="RM22W0E___0000I7P00001">
<ptxt>INSTALL REAR NO. 5 ROOF AIR DUCT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 clips to install the duct.</ptxt>
<figure>
<graphic graphicname="B178612" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0021" proc-id="RM22W0E___0000I7Q00001">
<ptxt>INSTALL REAR NO. 4 ROOF AIR DUCT</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the No. 5 roof air duct.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0007" proc-id="RM22W0E___0000I7J00001">
<ptxt>INSTALL CURTAIN SHIELD AIRBAG ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the curtain shield airbag (See page <xref label="Seep01" href="RM000003B7S005X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0006" proc-id="RM22W0E___0000I7I00001">
<ptxt>INSTALL CURTAIN SHIELD AIRBAG ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0008" proc-id="RM22W0E___0000I7K00001">
<ptxt>INSTALL ROOF HEADLINING ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the roof headlining (See page <xref label="Seep01" href="RM0000038MM00UX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0016" proc-id="RM22W0E___0000I7N00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002LP9035X_01_0013" proc-id="RM22W0E___0000I7M00001">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0KMX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>