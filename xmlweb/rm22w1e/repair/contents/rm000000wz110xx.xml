<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000000WZ110XX" category="J" type-id="3018M" name-id="ESRBA-03" from="201301" to="201308">
<dtccode/>
<dtcname>MIL Circuit</dtcname>
<subpara id="RM000000WZ110XX_01" type-id="60" category="03" proc-id="RM22W0E___000038J00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The MIL (Malfunction Indicator Lamp) is used to indicate vehicle malfunctions detected by the ECM. When the engine switch is turned on (IG), power is supplied to the MIL circuit, and the ECM provides the circuit ground which illuminates the MIL.</ptxt>
<ptxt>The MIL operation can be checked visually. When the engine switch is first turned on (IG), the MIL should be illuminated and should then turn off when the engine is started. If the MIL remains illuminated or is not illuminated, conduct the following troubleshooting procedure using the GTS.</ptxt>
</content5>
</subpara>
<subpara id="RM000000WZ110XX_02" type-id="32" category="03" proc-id="RM22W0E___000038K00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A182518E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000WZ110XX_03" type-id="51" category="05" proc-id="RM22W0E___000038L00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000WZ110XX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000WZ110XX_04_0011" proc-id="RM22W0E___000038Q00000">
<testtitle>CHECK THAT MIL IS ILLUMINATED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform troubleshooting in accordance with the table below.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MIL remains on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000WZ110XX_04_0001" fin="false">A</down>
<right ref="RM000000WZ110XX_04_0006" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0001" proc-id="RM22W0E___000038M00000">
<testtitle>CHECK WHETHER MIL TURNS OFF</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Check if DTCs have been stored (See page <xref label="Seep01" href="RM000000PDK14JX"/>). If DTCs are present, write them down.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep02" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check that the MIL turns off.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>MIL turns off.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ110XX_04_0003" fin="true">OK</down>
<right ref="RM000000WZ110XX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0002" proc-id="RM22W0E___000038N00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CHECK FOR SHORT IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A182543E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check that the MIL is not illuminated.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>MIL is not illuminated.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WZ110XX_04_0004" fin="true">OK</down>
<right ref="RM000000WZ110XX_04_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0012" proc-id="RM22W0E___000038R00000">
<testtitle>CHECK HARNESS AND CONNECTOR (COMBINATION METER - ECM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A276482E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E8-12 (CHK) - A38-24 (W)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E8-12 (CHK) or A38-24 (W) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E8-12 (CHK) - A52-24 (W)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E8-12 (CHK) or A52-24 (W) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the combination meter connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WZ110XX_04_0009" fin="true">OK</down>
<right ref="RM000000WZ110XX_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0006" proc-id="RM22W0E___000038O00000">
<testtitle>CHECK THAT MIL IS ILLUMINATED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the MIL is illuminated when the engine switch is turned on (IG).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>MIL is illuminated.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ110XX_04_0008" fin="true">OK</down>
<right ref="RM000000WZ110XX_04_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0013" proc-id="RM22W0E___000038S00000">
<testtitle>CHECK THAT ENGINE STARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Engine starts</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine does not start*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: The GTS cannot communicate with the ECM.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000WZ110XX_04_0007" fin="false">A</down>
<right ref="RM000000WZ110XX_04_0015" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0007" proc-id="RM22W0E___000038P00000">
<testtitle>INSPECT COMBINATION METER ASSEMBLY (MIL CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>See the combination meter troubleshooting procedure (See page <xref label="Seep01" href="RM000003BBU00DX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WZ110XX_04_0009" fin="true">OK</down>
<right ref="RM000000WZ110XX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0003">
<testtitle>REPAIR CIRCUITS INDICATED BY OUTPUT DTCS</testtitle>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0004">
<testtitle>REPLACE ECM</testtitle>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0008">
<testtitle>SYSTEM OK</testtitle>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0015">
<testtitle>GO TO VC OUTPUT CIRCUIT<xref label="Seep01" href="RM000001D6V0PVX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (COMBINATION METER - ECM)</testtitle>
</testgrp>
<testgrp id="RM000000WZ110XX_04_0009">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM0000038ID00IX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>