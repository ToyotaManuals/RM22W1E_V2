<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001Q" variety="S001Q">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001Q_7C3Q6_T00J9" variety="T00J9">
<name>POWER STEERING SYSTEM (for 1VD-FTV with DPF)</name>
<para id="RM0000027C008XX" category="C" type-id="804TC" name-id="PA1GH-02" from="201308">
<dtccode>C1544</dtccode>
<dtcname>High Engine Revolution Signal</dtcname>
<subpara id="RM0000027C008XX_01" type-id="60" category="03" proc-id="RM22W0E___0000B6H00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The power steering ECU receives engine speed signals from the ECM via CAN communication. The power steering ECU provides appropriate assisting force in accordance with the engine speed signal.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1544</ptxt>
</entry>
<entry valign="middle">
<ptxt>An abnormal engine speed signal is received.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ECD system</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>Power steering ECU assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000027C008XX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000027C008XX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000027C008XX_03_0001" proc-id="RM22W0E___0000B6I00001">
<testtitle>CHECK OTHER DTC OUTPUT (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if DTC U0100 is output (See page <xref label="Seep01" href="RM00000275006KX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC U0100 is not output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC U0100 is output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>DTC U0100 indicates a CAN communication malfunction in the power steering system.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000027C008XX_03_0002" fin="false">A</down>
<right ref="RM0000027C008XX_03_0003" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000027C008XX_03_0002" proc-id="RM22W0E___0000B6J00001">
<testtitle>CHECK OTHER DTC OUTPUT (ECD SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the GTS, check for DTCs and confirm that there are no problems in the ECD system (See page <xref label="Seep01" href="RM000000PDK189X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.23in"/>
<colspec colname="COL2" colwidth="1.9in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000027C008XX_03_0005" fin="true">A</down>
<right ref="RM0000027C008XX_03_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000027C008XX_03_0005">
<testtitle>REPLACE POWER STEERING ECU ASSEMBLY<xref label="Seep01" href="RM000003FC0012X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000027C008XX_03_0003">
<testtitle>REPAIR CIRCUIT INDICATED BY OUTPUT&#13;
CODE<xref label="Seep01" href="RM00000277M07TX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000027C008XX_03_0004">
<testtitle>GO TO ECD SYSTEM (DIAGNOSTIC TROUBLE CODE CHART)<xref label="Seep01" href="RM0000031HW065X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>