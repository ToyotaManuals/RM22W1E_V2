<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000004KMK010X" category="C" type-id="805AK" name-id="ESZKP-01" from="201301" to="201308">
<dtccode>P042E</dtccode>
<dtcname>Exhaust Gas Recirculation "A" Control Stuck Open</dtcname>
<dtccode>P045E</dtccode>
<dtcname>Exhaust Gas Recirculation "B" Control</dtcname>
<subpara id="RM000004KMK010X_01" type-id="60" category="03" proc-id="RM22W0E___00003YF00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The EGR system recirculates exhaust gases. The recirculated gas mingles with the intake air so that the EGR system can slow combustion speed and keep the combustion temperature down. This helps reduce NOx emissions.</ptxt>
<ptxt>In order to increase EGR circulation efficiency, the ECM adjusts the EGR valve lift amount and the throttle valve angle.</ptxt>
<table pgwide="1">
<title>P042E (No. 1), P045E (No. 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>After warming up engine and idling for 60 seconds, maintain engine speed at 2500 rpm for 40 seconds, drive vehicle, and perform engine brake deceleration by fully releasing accelerator when engine speed is 2000 rpm or more.</ptxt>
</entry>
<entry valign="middle">
<ptxt>The target and actual positions of the EGR valve are different for 20 seconds or more.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>EGR valve stuck</ptxt>
</item>
<item>
<ptxt>EGR valve does not move smoothly</ptxt>
</item>
<item>
<ptxt>EGR valve deposits</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>While the engine is running, the EGR activation duty exceeds the threshold for 0.5 seconds a certain number of times (40 seconds or more).</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P042E</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>MAF</ptxt>
</item>
<item>
<ptxt>MAP</ptxt>
</item>
<item>
<ptxt>EGR Close Lrn. Val.</ptxt>
</item>
<item>
<ptxt>Target EGR Valve Pos</ptxt>
</item>
<item>
<ptxt>Actual EGR Valve Pos</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P045E</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>MAF</ptxt>
</item>
<item>
<ptxt>MAP</ptxt>
</item>
<item>
<ptxt>EGR Close Lrn. Val #2</ptxt>
</item>
<item>
<ptxt>Target EGR Valve Pos #2</ptxt>
</item>
<item>
<ptxt>Actual EGR Valve Pos #2</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Actual EGR valve opening percentage: Fully closed = 0%, fully open = 100%.</ptxt>
</item>
<item>
<ptxt>If DTC P042E or P045E is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Stuck closed malfunction</ptxt>
</item>
<list3 type="unordered">
<item>
<ptxt>Lack of power: due to fail-safe operation</ptxt>
</item>
<item>
<ptxt>Intake booming noise</ptxt>
</item>
<item>
<ptxt>Slight combustion noise</ptxt>
</item>
</list3>
</list2>
<list2 type="unordered">
<item>
<ptxt>Stuck open malfunction</ptxt>
</item>
<list3 type="unordered">
<item>
<ptxt>Black smoke</ptxt>
</item>
<item>
<ptxt>Lack of power</ptxt>
</item>
<item>
<ptxt>Vibration at engine stop</ptxt>
</item>
<item>
<ptxt>Misfire</ptxt>
</item>
<item>
<ptxt>White smoke</ptxt>
</item>
<item>
<ptxt>Engine stall</ptxt>
</item>
</list3>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000004KMK010X_02" type-id="64" category="03" proc-id="RM22W0E___00003YG00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the target and actual positions of the EGR valve are different, the ECM interprets this as a malfunction of the electric EGR control valve assembly and illuminates the MIL (1 trip detection logic).</ptxt>
<figure>
<graphic graphicname="A208465E04" width="7.106578999in" height="6.791605969in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a large amount of carbon deposits exists in the EGR valve assembly, the following parts possibly also have deposits inside: The intake manifold, EGR cooler, exhaust fuel addition injector assemblies (if equipped), exhaust pipe assemblies (DPF catalytic converter), and other parts related to the exhaust gas.</ptxt>
</item>
<item>
<ptxt>Be sure to carefully examine "Actual EGR Valve Pos", "Target EGR Valve Pos" and "MAF" in the freeze frame data.</ptxt>
</item>
<item>
<ptxt>Be sure to carefully examine "Actual EGR Valve Pos #2", "Target EGR Valve Pos #2" and "MAF" in the freeze frame data.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000004KMK010X_03" type-id="32" category="03" proc-id="RM22W0E___00003YH00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0405 (See page <xref label="Seep01" href="RM00000187T07MX_03"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000004KMK010X_04" type-id="51" category="05" proc-id="RM22W0E___00003YI00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When cleaning the EGR valve assembly or diesel throttle body assembly, use a piece of cloth soaked with cleaning solvent. Spraying the solvent directly onto these parts or soaking the parts in solvent may damage the parts.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000004KMK010X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004KMK010X_05_0001" proc-id="RM22W0E___00003YJ00000">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P042E OR P045E)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P042E or P045E is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P042E or P045E and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P042E or P045E are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KMK010X_05_0002" fin="false">A</down>
<right ref="RM000004KMK010X_05_0008" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000004KMK010X_05_0002" proc-id="RM22W0E___00003YK00000">
<testtitle>INSPECT EGR VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Active Test / Control the EGR Step Position or Control the EGR Step Position #2.</ptxt>
</test1>
<test1>
<ptxt>While continuously changing the Active Test value to 0, 30, 60, 90, 60, 30 and 0%, check that Actual EGR Valve Pos or Actual EGR Valve Pos #2 smoothly changes to the set opening lift amount.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Value smoothly changes to within +/- 10% of set opening lift amount.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>EGR valve closed: 0%. </ptxt>
</item>
<item>
<ptxt>EGR valve open: 95.2%.</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Next, after the engine is warmed up, stop the engine and wait for 15 seconds. After that, start the engine again and idle it for 30 seconds. Then stop the engine and wait for 15 seconds. After starting the engine again, read the Data List values while idling.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Data List / EGR Close Lrn. Val. or EGR Close Lrn. Val. #2.</ptxt>
</test1>
<test1>
<ptxt>Read the values.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>3.5 to 4.5 V</ptxt>
</specitem>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004KMK010X_05_0003" fin="false">A</down>
<right ref="RM000004KMK010X_05_0005" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004KMK010X_05_0003" proc-id="RM22W0E___00003YL00000">
<testtitle>PERFORM ACTIVE TEST USING GTS (CONTROL THE EGR STEP POSITION OR CONTROL THE EGR STEP POSITION #2)</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>Visually inspect the movement of the EGR valve.</ptxt>
</atten4>
<test1>
<ptxt>Remove the No. 1 and No. 2 EGR valve assembly (See page <xref label="Seep02" href="RM0000031JM009X"/>).</ptxt>
</test1>
<test1>
<ptxt>Reconnect the No. 1 and No. 2 EGR valve assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Active Test / Control the EGR Step Position or Control the EGR Step Position #2.</ptxt>
</test1>
<test1>
<ptxt>Check if the valve of the EGR valve assembly opens/closes when an Active Test is performed.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The valve opens/closes.</ptxt>
</specitem>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>Remove the deposits if there is a large amount of deposits in the EGR valve assembly or the passage of the intake manifold.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KMK010X_05_0004" fin="false">A</down>
<right ref="RM000004KMK010X_05_0005" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004KMK010X_05_0004" proc-id="RM22W0E___00003YM00000">
<testtitle>REPLACE EGR VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>Before replacing the EGR valve assembly, check the connections of the wire harness and connectors. If there is any abnormality, replace or repair the wire harness or connector.</ptxt>
</atten4>
<ptxt>When DTC P042E is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace No. 1 EGR valve assembly (See page <xref label="Seep01" href="RM0000031JM009X"/>).</ptxt>
</item>
</list1>
<ptxt>When DTC P045E is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace No. 2 EGR valve assembly (See page <xref label="Seep02" href="RM0000031JM009X"/>).</ptxt>
</item>
</list1>
</content6>
<res>
<right ref="RM000004KMK010X_05_0006" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004KMK010X_05_0005" proc-id="RM22W0E___00003YN00000">
<testtitle>CHECK FOR DEPOSIT (EGR VALVE ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the EGR valve assembly (See page <xref label="Seep01" href="RM0000031JM009X"/>).</ptxt>
</test1>
<test1>
<ptxt>Visually check the EGR valve assembly for deposits.</ptxt>
<spec>
<title>Result</title>
<specitem>
<ptxt>If there are deposits, clean the EGR valve assembly.</ptxt>
</specitem>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When cleaning the EGR valve assembly, make sure the valve is completely closed.</ptxt>
</item>
<item>
<ptxt>When cleaning the EGR valve assembly, perform initialization procedure (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>Do not forcibly open the valve, as it may be damaged or deformed.</ptxt>
</item>
<item>
<ptxt>When cleaning the EGR valve assembly, use a piece of cloth soaked with cleaning solvent. Spraying the solvent directly onto these parts or soaking the parts in the solvent may damage the parts.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the EGR valve does not open properly or is stuck closed, the amount of intake air increases and combustion sounds and engine vibration may increase.</ptxt>
</item>
<item>
<ptxt>If the EGR valve does not close properly or is stuck open, EGR becomes excessive and combustion becomes unstable. Also, there may be a lack of power.</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Reinstall the EGR valve assembly (See page <xref label="Seep02" href="RM0000031JK009X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KMK010X_05_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KMK010X_05_0006" proc-id="RM22W0E___00003YO00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off and wait for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>After warming up the engine and idling for 60 seconds, the maintain engine speed at 2500 rpm for 40 seconds, the drive vehicle, and perform engine brake deceleration by fully releasing the accelerator when the engine speed is 2000 rpm or more.</ptxt>
<atten2>
<ptxt>When performing the confirmation driving pattern, obey all speed limits and traffic laws.</ptxt>
</atten2>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTCs is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KMK010X_05_0007" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KMK010X_05_0007">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000004KMK010X_05_0008">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW05CX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>