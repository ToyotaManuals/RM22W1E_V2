<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001S" variety="S001S">
<name>STEERING COLUMN</name>
<ttl id="12038_S001S_7C3QC_T00JF" variety="T00JF">
<name>STEERING LOCK SYSTEM</name>
<para id="RM000001T1I05QX" category="J" type-id="305HJ" name-id="SR2UJ-04" from="201308">
<dtccode/>
<dtcname>Unlock Position Sensor Signal Circuit</dtcname>
<subpara id="RM000001T1I05QX_01" type-id="60" category="03" proc-id="RM22W0E___0000BD500001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The steering lock ECU sends an unlock position signal to the main body ECU. On receiving the signal, the main body ECU permits an engine start. This prevents the engine from being started with the steering locked.</ptxt>
<ptxt>The diagnosis information of the steering lock ECU is transmitted to the intelligent tester via the certification ECU (smart key ECU assembly) as the steering lock ECU is not connected to the CAN communication system.</ptxt>
</content5>
</subpara>
<subpara id="RM000001T1I05QX_02" type-id="32" category="03" proc-id="RM22W0E___0000BD600001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C165587E05" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001T1I05QX_03" type-id="51" category="05" proc-id="RM22W0E___0000BD700001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>When the engine switch is off, the main body ECU may occasionally go into a non-active state called sleep mode. Therefore, before proceeding with the inspection, it is necessary to perform the following steps to wake up the ECU:</ptxt>
<ptxt>With the engine switch off, open the driver door. Then (with the engine switch still off) open and close any door several times at 1.5 second intervals.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001T1I05QX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001T1I05QX_04_0001" proc-id="RM22W0E___0000BD800001">
<testtitle>INSPECT STEERING LOCK ECU</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C165193E12" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.67in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.09in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition (Steering Lock Position)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E26-4 (SLP1) - E26-2 (SGND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E26-4 (SLP1) - E26-2 (SGND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Unlocked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T1I05QX_04_0006" fin="true">OK</down>
<right ref="RM000001T1I05QX_04_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T1I05QX_04_0006">
<testtitle>GO TO ENTRY AND START SYSTEM (FOR START FUNCTION) (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000000YEF0JHX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001T1I05QX_04_0004" proc-id="RM22W0E___0000BD900001">
<testtitle>CHECK HARNESS AND CONNECTOR (STEERING LOCK ECU - MAIN BODY ECU)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C164812E15" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E26 steering lock ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E1 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>E26-4 (SLP1) - E1-18 (SLP)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>E26-4 (SLP1) or E1-18 (SLP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.76in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Within specified range (for Power tilt and telescopic)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Within specified range (for Manual tilt and telescopic)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Outside specified range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001T1I05QX_04_0008" fin="true">A</down>
<right ref="RM000001T1I05QX_04_0010" fin="true">B</right>
<right ref="RM000001T1I05QX_04_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001T1I05QX_04_0008">
<testtitle>REPLACE STEERING LOCK ACTUATOR (STEERING LOCK ECU)<xref label="Seep01" href="RM0000039SJ014X_01_0001"/>
</testtitle>
</testgrp>
<testgrp id="RM000001T1I05QX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001T1I05QX_04_0010">
<testtitle>REPLACE STEERING LOCK ACTUATOR (STEERING LOCK ECU)<xref label="Seep01" href="RM000000MID008X_01_0055"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>