<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM00000451O011X" category="J" type-id="300E0" name-id="BCEHT-01" from="201301" to="201308">
<dtccode/>
<dtcname>ABS Warning Light Remains ON</dtcname>
<subpara id="RM00000451O011X_01" type-id="60" category="03" proc-id="RM22W0E___0000AM600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If any of the following is detected, the ABS warning light remains on.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The skid control ECU (master cylinder solenoid) connector is disconnected from the skid control ECU (master cylinder solenoid).</ptxt>
</item>
<item>
<ptxt>There is a malfunction in the skid control ECU (master cylinder solenoid) internal circuit.</ptxt>
</item>
<item>
<ptxt>There is an open in the harness between the combination meter and skid control ECU (master cylinder solenoid).</ptxt>
</item>
<item>
<ptxt>The anti-lock brake system is defective.</ptxt>
</item>
<item>
<ptxt>The voltage at terminal IG1 is high.</ptxt>
</item>
<item>
<ptxt>The rear differential is locked.*</ptxt>
</item>
</list1>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Rear Differential Lock</ptxt>
</item>
</list1>
<atten4>
<ptxt>It may not be possible to use the GTS when the skid control ECU (master cylinder solenoid) is abnormal.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000451O011X_02" type-id="32" category="03" proc-id="RM22W0E___0000AM700000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C258381E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000451O011X_03" type-id="51" category="05" proc-id="RM22W0E___0000AM800000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform calibration (See page <xref label="Seep01" href="RM000001DWZ01OX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000451O011X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000451O011X_04_0001" proc-id="RM22W0E___0000AM900000">
<testtitle>CHECK THAT SKID CONTROL ECU CONNECTOR IS SECURELY CONNECTED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the skid control ECU (master cylinder solenoid) connector connection.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The 2 connectors are securely connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000451O011X_04_0002" fin="false">OK</down>
<right ref="RM00000451O011X_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0002" proc-id="RM22W0E___0000AMA00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM0000046KV00NX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.84in"/>
<colspec colname="COL2" colwidth="3.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000451O011X_04_0003" fin="false">A</down>
<right ref="RM00000451O011X_04_0014" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0003" proc-id="RM22W0E___0000AMB00000">
<testtitle>CHECK CAN COMMUNICATION LINE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Select CAN Bus Check from the System Selection Menu screen and follow the prompts on the screen to inspect the CAN bus.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>CAN Bus Check indicates no malfunctions in CAN communication.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.02in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000451O011X_04_0030" fin="false">A</down>
<right ref="RM00000451O011X_04_0015" fin="true">B</right>
<right ref="RM00000451O011X_04_0031" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0030" proc-id="RM22W0E___0000AMK00000">
<testtitle>READ VALUE USING GTS (IG1 VOLTAGE VALUE)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3. </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>IG1 Voltage Value</ptxt>
</entry>
<entry valign="middle">
<ptxt>IG1 voltage value/ Min.: 0.00 V, Max.: 20.00 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON: 11 to 14 V </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check the voltage output from the skid control ECU (master cylinder solenoid) displayed on the GTS.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The output voltage displayed on the GTS is within 11 to 14 V.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM00000451O011X_04_0005" fin="false">OK</down>
<right ref="RM00000451O011X_04_0028" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0005" proc-id="RM22W0E___0000AMC00000">
<testtitle>CHECK HARNESS AND CONNECTOR (P TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214161E75" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.61in"/>
<colspec colname="COL3" colwidth="1.14in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-14 (P) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
<ptxt>w/ Rear Differential Lock: Rear differential free</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU [Master Cylinder Solenoid])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000451O011X_04_0006" fin="false">OK</down>
<right ref="RM00000451O011X_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0006" proc-id="RM22W0E___0000AMD00000">
<testtitle>PERFORM ACTIVE TEST USING GTS (ABS WARNING LIGHT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Active Test.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ABS Warning Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ABS warning light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Warning light ON/OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Observe the combination meter.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>When performing the ABS Warning Light Active Test, check ABS Warning Light in the Data List (See page <xref label="Seep01" href="RM000001DWY029X"/>).</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="2.16in"/>
<colspec colname="COL3" colwidth="1.70in"/>
<colspec colname="COL4" colwidth="1.45in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ABS Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS warning light/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Warning light on</ptxt>
<ptxt>OFF: Warning light off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.69in"/>
<colspec colname="COL2" colwidth="3.91in"/>
<colspec colname="COL3" colwidth="1.48in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Data List Display</ptxt>
</entry>
<entry>
<ptxt>Data List Display when Performing Active Test ON/OFF Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000451O011X_04_0017" fin="true">A</down>
<right ref="RM00000451O011X_04_0020" fin="true">B</right>
<right ref="RM00000451O011X_04_0023" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0028" proc-id="RM22W0E___0000AMI00000">
<testtitle>CHECK HARNESS AND CONNECTOR (IG1 TERMINAL)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the skid control ECU (master cylinder solenoid) connector. </ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214161E68" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-46 (IG1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU (Master Cylinder Solenoid))</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM00000451O011X_04_0029" fin="false">OK</down>
<right ref="RM00000451O011X_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0029" proc-id="RM22W0E___0000AMJ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (GND1, GND2 AND GND3 TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A24 and A25 skid control ECU (master cylinder solenoid) connectors.</ptxt>
<figure>
<graphic graphicname="C124799E58" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.89in"/>
<colspec colname="COL2" colwidth="1.05in"/>
<colspec colname="COL3" colwidth="1.19in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-1 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-32 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A25-4 (GND3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU [Master Cylinder Solenoid])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.46in"/>
<colspec colname="COL2" colwidth="1.67in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000451O011X_04_0018" fin="true">A</down>
<right ref="RM00000451O011X_04_0020" fin="true">B</right>
<right ref="RM00000451O011X_04_0023" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0009" proc-id="RM22W0E___0000AME00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU P CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A27 4 wheel drive control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>w/ Rear Differential Lock:</ptxt>
<ptxt>Disconnect the f2 rear differential lock detection switch (No. 1 transfer indicator switch) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.83in"/>
<colspec colname="COL2" colwidth="1.90in"/>
<colspec colname="COL3" colwidth="2.35in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-14 (P) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.51in"/>
<colspec colname="COL2" colwidth="2.57in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (w/o Rear Differential Lock)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (w/ Rear Differential Lock)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000451O011X_04_0010" fin="false">A</down>
<right ref="RM00000451O011X_04_0011" fin="false">B</right>
<right ref="RM00000451O011X_04_0019" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0010" proc-id="RM22W0E___0000AMF00000">
<testtitle>CHECK HARNESS AND CONNECTOR (P TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the A27 4 wheel drive control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214161E75" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-14 (P) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU [Master Cylinder Solenoid])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000451O011X_04_0022" fin="true">A</down>
<right ref="RM00000451O011X_04_0020" fin="true">B</right>
<right ref="RM00000451O011X_04_0023" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0011" proc-id="RM22W0E___0000AMG00000">
<testtitle>INSPECT REAR DIFFERENTIAL LOCK DETECTION SWITCH (NO. 1 TRANSFER INDICATOR SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear differential lock detection switch (No. 1 transfer indicator switch) (See page <xref label="Seep01" href="RM000003ARU005X"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the rear differential lock detection switch (No. 1 transfer indicator switch) (See page <xref label="Seep02" href="RM000003ARS003X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000451O011X_04_0012" fin="false">OK</down>
<right ref="RM00000451O011X_04_0024" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0012" proc-id="RM22W0E___0000AMH00000">
<testtitle>CHECK HARNESS AND CONNECTOR (P TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the A27 4 wheel drive control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214161E75" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-14 (P) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU [Master Cylinder Solenoid])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>

</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000451O011X_04_0022" fin="true">A</down>
<right ref="RM00000451O011X_04_0020" fin="true">B</right>
<right ref="RM00000451O011X_04_0023" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000451O011X_04_0013">
<testtitle>CONNECT CONNECTOR TO ECU CORRECTLY</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0014">
<testtitle>REPAIR CIRCUITS INDICATED BY OUTPUT DTCS<xref label="Seep01" href="RM000001DWQ01RX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0015">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0017">
<testtitle>GO TO METER / GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L03NX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0018">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0019">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0020">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0022">
<testtitle>REPLACE 4 WHEEL DRIVE CONTROL ECU<xref label="Seep01" href="RM0000030J200ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0023">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171V01VX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0024">
<testtitle>REPLACE NO. 1 TRANSFER INDICATOR SWITCH<xref label="Seep01" href="RM000003ARU005X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451O011X_04_0031">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>