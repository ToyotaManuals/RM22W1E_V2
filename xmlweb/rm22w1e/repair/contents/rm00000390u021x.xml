<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3U4_T00N7" variety="T00N7">
<name>FRONT SEAT SIDE AIRBAG ASSEMBLY (for Manual Seat)</name>
<para id="RM00000390U021X" category="A" type-id="30014" name-id="RSGI8-02" from="201308">
<name>INSTALLATION</name>
<subpara id="RM00000390U021X_02" type-id="11" category="10" proc-id="RM22W0E___0000FIY00001">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for LHD and RHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000390U021X_01" type-id="01" category="01">
<s-1 id="RM00000390U021X_01_0034" proc-id="RM22W0E___0000FIN00001">
<ptxt>INSTALL FRONT SEAT AIRBAG ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Make sure that the seat frame assembly is not deformed. If it is, replace it with a new one.</ptxt>
</atten2>
<s2>
<ptxt>Install the front seat airbag assembly with 2 nuts.</ptxt>
<figure>
<graphic graphicname="B102198" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 2 clamps to connect the wire harness.</ptxt>
</s2>
<s2>
<ptxt>Attach the 5 clamps to connect the wire harness.</ptxt>
<figure>
<graphic graphicname="B102194" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U021X_01_0036" proc-id="RM22W0E___0000FIP00001">
<ptxt>INSTALL SEATBACK COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the seatback cover with pad in place.</ptxt>
</s2>
<s2>
<ptxt>w/ Seatback Board:</ptxt>
<s3>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<ptxt>Install the seatback cover bracket to the seat frame with the nut.</ptxt>
<figure>
<graphic graphicname="B180716" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
<atten2>
<ptxt>After the seatback cover with pad is installed, make sure the seatback cover bracket is not twisted.</ptxt>
</atten2>
</s3>
<s3>
<figure>
<graphic graphicname="B191295E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Attach the 9 hooks.</ptxt>
</s3>
<s3>
<ptxt>Using hog ring pliers, install new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
<s2>
<ptxt>w/o Seatback Board:</ptxt>
<s3>
<ptxt>Attach the 4 claws to install the 2 headrest supports.</ptxt>
<figure>
<graphic graphicname="B180768" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<ptxt>Install the seatback cover bracket to the seat frame with the nut.</ptxt>
<figure>
<graphic graphicname="B180716" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
<atten2>
<ptxt>After the seatback cover with pad is installed, make sure the seatback cover bracket is not twisted.</ptxt>
</atten2>
</s3>
<s3>
<ptxt>Close the 2 fasteners.</ptxt>
<figure>
<graphic graphicname="B185566" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Using hog ring pliers, install new hog rings.</ptxt>
<figure>
<graphic graphicname="B180766" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM00000390U021X_01_0037" proc-id="RM22W0E___0000FIQ00001">
<ptxt>INSTALL FRONT SEAT HEADREST SUPPORT
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180712" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the 2 front seat headrest supports.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U021X_01_0038" proc-id="RM22W0E___0000FIR00001">
<ptxt>INSTALL FRONT SEATBACK BOARD SUB-ASSEMBLY LH (w/ Seatback Board)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182646" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Move the front seatback board sub-assembly in the direction of the arrow to attach the 2 hooks and install it.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U021X_01_0039" proc-id="RM22W0E___0000FIS00001">
<ptxt>INSTALL FRONT INNER SEAT CUSHION SHIELD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180761" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw and clip to install the front inner seat cushion shield LH.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U021X_01_0033" proc-id="RM22W0E___0000G9F00001">
<ptxt>INSTALL FRONT SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B189679E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install the front seat inner belt assembly with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not allow the anchor part of the front seat inner belt assembly to overlap the protruding part of the front seat adjuster.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the connectors and attach the clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U021X_01_0040" proc-id="RM22W0E___0000FIT00001">
<ptxt>INSTALL FRONT SEAT CUSHION SHIELD LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Lumbar Support:</ptxt>
<ptxt>Connect the lumbar switch connector.</ptxt>
</s2>
<s2>
<ptxt>for 4 Way Seat Type:</ptxt>
<s3>
<ptxt>Attach the 3 claws in the order shown in the illustration to install the front seat cushion shield LH.</ptxt>
<figure>
<graphic graphicname="B180780E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Install the 2 screws.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 8 Way Seat Type:</ptxt>
<s3>
<ptxt>Attach the 3 claws in the order shown in the illustration to install the front seat cushion shield LH.</ptxt>
<figure>
<graphic graphicname="B180786E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Install the 3 screws.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Seatback Board:</ptxt>
<ptxt>Connect the 2 rubber bands to the 2 hooks.</ptxt>
<figure>
<graphic graphicname="B180757" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Seatback Board:</ptxt>
<ptxt>Connect the rubber band to the 2 hooks.</ptxt>
<figure>
<graphic graphicname="B180756" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000390U021X_01_0041" proc-id="RM22W0E___0000FIU00001">
<ptxt>INSTALL RECLINING ADJUSTER RELEASE HANDLE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185571" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the reclining adjuster release handle LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U021X_01_0042" proc-id="RM22W0E___0000FIV00001">
<ptxt>INSTALL VERTICAL SEAT ADJUSTER KNOB (for 8 Way Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180781" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the snap ring to the vertical seat adjuster knob.</ptxt>
</s2>
<s2>
<ptxt>Install the vertical seat adjuster knob.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U021X_01_0043" proc-id="RM22W0E___0000FIW00001">
<ptxt>INSTALL VERTICAL ADJUSTING HANDLE LH (for 8 Way Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180753" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the vertical adjusting handle with the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U021X_01_0044" proc-id="RM22W0E___0000FIX00001">
<ptxt>INSTALL VERTICAL ADJUSTER COVER  LH (for 8 Way Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180752" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the vertical adjuster cover LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U021X_01_0035" proc-id="RM22W0E___0000FIO00001">
<ptxt>INSTALL FRONT SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front seat assembly LH (See page <xref label="Seep01" href="RM00000390Q013X"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>