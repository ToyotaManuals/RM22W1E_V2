<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3OC_T00HF" variety="T00HF">
<name>ACTIVE HEIGHT CONTROL SUSPENSION</name>
<para id="RM000002PE602HX" category="C" type-id="300ZX" name-id="SC1UA-03" from="201308">
<dtccode>C1715</dtccode>
<dtcname>Front Acceleration Sensor RH Malfunction</dtcname>
<dtccode>C1716</dtccode>
<dtcname>Front Acceleration Sensor LH Malfunction</dtcname>
<dtccode>C1717</dtccode>
<dtcname>Rear Acceleration Sensor Malfunction</dtcname>
<dtccode>C1796</dtccode>
<dtcname>Front Acceleration Sensor RH Malfunction (Test Mode DTC)</dtcname>
<dtccode>C1797</dtccode>
<dtcname>Front Acceleration Sensor LH Malfunction (Test Mode DTC)</dtcname>
<dtccode>C1798</dtccode>
<dtcname>Rear Acceleration Sensor Malfunction (Test Mode DTC)</dtcname>
<subpara id="RM000002PE602HX_01" type-id="60" category="03" proc-id="RM22W0E___00009N700001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The acceleration sensor (up and down G sensor) detects the upward and downward acceleration of the vehicle, and outputs it as a voltage to the suspension control ECU. Up and down G sensors are installed in 3 locations: 1) the suspension control ECU, 2) the driver side instrument panel, and 3) the passenger side instrument panel. Each up and down G sensor independently detects the upward and downward acceleration. During a test mode inspection, the suspension control ECU reads the fluctuations in the signal of each sensor. When the sensor cannot detect +/-1.96 m/s<sup>2</sup> for 1 second or more during test mode, DTCs C1796, C1797 and C1798 are stored.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.28in"/>
<colspec colname="COL2" colwidth="3.44in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1715</ptxt>
<ptxt>C1796</ptxt>
</entry>
<entry valign="middle">
<ptxt>When either of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the engine switch on (IG), a voltage of 4.7 V or higher, or 0.3 V or less at the front acceleration sensor RH is detected for 1 second.</ptxt>
</item>
<item>
<ptxt>When there is a front acceleration sensor RH power supply voltage malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Front acceleration sensor RH</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1716</ptxt>
<ptxt>C1797</ptxt>
</entry>
<entry valign="middle">
<ptxt>When either of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the engine switch on (IG), a voltage of 4.7 V or higher, or 0.3 V or less at the front acceleration sensor LH is detected for 1 second.</ptxt>
</item>
<item>
<ptxt>When there is a front acceleration sensor LH power supply voltage malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Front acceleration sensor LH</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1717</ptxt>
<ptxt>C1798</ptxt>
</entry>
<entry valign="middle">
<ptxt>When either of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the engine switch on (IG), a voltage of 4.7 V or higher, or 0.3 V or less at the rear acceleration sensor is detected for 1 second.</ptxt>
</item>
<item>
<ptxt>When there is a rear acceleration sensor power supply voltage malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Suspension control ECU (Houses rear acceleration sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002PE602HX_02" type-id="32" category="03" proc-id="RM22W0E___00009N800001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C177316E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002PE602HX_03" type-id="51" category="05" proc-id="RM22W0E___00009N900001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before performing troubleshooting, inspect the connectors of related circuits.</ptxt>
</item>
<item>
<ptxt>If the suspension control ECU or height control sensor is replaced, the vehicle height offset calibration must be performed (See page <xref label="Seep01" href="RM000003AG300EX"/>).</ptxt>
</item>
<item>
<ptxt>If DTCs C1717 and C1798 (Rear Acceleration Sensor Malfunction) are stored, replace the  suspension control ECU (houses rear acceleration sensor).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000002PE602HX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002PE602HX_04_0001" proc-id="RM22W0E___00009NA00001">
<testtitle>CHECK ACCELERATION SENSOR (DATA LIST)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / AHC / Data List.</ptxt>
</test1>
<test1>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
<table pgwide="1">
<title>AHC</title>
<tgroup cols="4">
<colspec colname="COLSPEC1" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.78in"/>
<colspec colname="COL3" colwidth="1.78in"/>
<colspec colname="COL4" colwidth="1.74in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>(Up &amp; Down)G Sensor FR</ptxt>
</entry>
<entry valign="middle">
<ptxt>G (up and down) front acceleration sensor RH reading/</ptxt>
<ptxt>min.: -1045.29 m/s<sup>2</sup>
</ptxt>
<ptxt>max.: 1045.26 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 +/-0.98 m/s<sup>2</sup> at still condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reading changes when vehicle (FR) is bounced</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>(Up &amp; Down)G Sensor FL</ptxt>
</entry>
<entry valign="middle">
<ptxt>G (up and down) front acceleration sensor LH reading/</ptxt>
<ptxt>min.: -1045.29 m/s<sup>2</sup>
</ptxt>
<ptxt>max.: 1045.26 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 +/-0.98 m/s<sup>2</sup> at still condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reading changes when vehicle (FL) is bounced</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>(Up &amp; Down)G Sensor Rear</ptxt>
</entry>
<entry valign="middle">
<ptxt>G (up and down) rear acceleration sensor reading/</ptxt>
<ptxt>min.: -1045.29 m/s<sup>2</sup>
</ptxt>
<ptxt>max.: 1045.26 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 +/-0.98 m/s<sup>2</sup> at still condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reading changes when vehicle (rear) is bounced</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Reading changes when vehicle is bounced.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002PE602HX_04_0007" fin="false">OK</down>
<right ref="RM000002PE602HX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PE602HX_04_0007" proc-id="RM22W0E___00009ND00001">
<testtitle>RECONFIRM DTC OUTPUT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001CU200OX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform a road test.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.51in"/>
<colspec colname="COL2" colwidth="1.62in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002PE602HX_04_0003" fin="false">A</down>
<right ref="RM000002PE602HX_04_0008" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002PE602HX_04_0003" proc-id="RM22W0E___00009NC00001">
<testtitle>CHECK HARNESS AND CONNECTOR (ACCELERATION SENSOR - SUSPENSION CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>When inspecting the wire harnesses between the suspension control ECU and acceleration sensors, it is difficult to determine the malfunctioning area if there is a sensor power supply malfunction. Therefore, disconnect the connectors for the acceleration sensors (front RH, front LH), the fluid temperature sensor, and fluid pressure sensor on the ECU side.</ptxt>
</atten3>
<test1>
<ptxt>Check the front acceleration sensor RH side: (C1715)</ptxt>
<figure>
<graphic graphicname="C174713E02" width="7.106578999in" height="2.775699831in"/>
</figure>
<test2>
<ptxt>Disconnect the K34 and K35 ECU connectors.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the L35 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.99in"/>
<colspec colname="COL2" colwidth="1.00in"/>
<colspec colname="COL3" colwidth="1.14in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>L35-1 (SGFR) - K34-24 (SGFR)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L35-2 (SGG) - K34-14 (SGG1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L35-3 (SGB) - K34-15 (SGB1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K34-24 (SGFR) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K34-14 (SGG1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K34-15 (SGB1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>Check the front acceleration sensor LH side: (C1716)</ptxt>
<test2>
<ptxt>Disconnect the K34 and K35 ECU connectors.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the K37 sensor connector.</ptxt>
<figure>
<graphic graphicname="C174713E03" width="7.106578999in" height="2.775699831in"/>
</figure>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.97in"/>
<colspec colname="COL2" colwidth="0.91in"/>
<colspec colname="COL3" colwidth="1.25in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K37-1 (SGFL) - K34-22 (SGFL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K37-2 (SGG) - K34-13 (SGG2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K37-3 (SGB) - K34-23 (SGB2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K34-22 (SGFL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K34-13 (SGG2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K34-23 (SGB2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002PE602HX_04_0002" fin="false">OK</down>
<right ref="RM000002PE602HX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PE602HX_04_0002" proc-id="RM22W0E___00009NB00001">
<testtitle>INSPECT ACCELERATION SENSOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C177935E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Remove the front acceleration sensor (See page <xref label="Seep01" href="RM000003CMM00GX"/>).</ptxt>
<atten3>
<ptxt>Do not drop the acceleration sensor. If it is dropped, replace it with a new one.</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Connect 3 1.5 V dry cell batteries in series.</ptxt>
</test1>
<test1>
<ptxt>Connect the positive (+) end of the batteries to terminal 3 (SGB) of the acceleration sensor and the negative (-) end of the batteries to terminal 2 (SGG). Then measure the voltage between terminal 1 (SGFR*1, SGFL*2) and terminal 2 (SGG).</ptxt>
<atten4>
<ptxt>*1: for RH</ptxt>
<ptxt>*2: for LH</ptxt>
</atten4>
<atten3>
<ptxt>Do not apply a voltage of more than 6 V.</ptxt>
</atten3>
<atten4>
<ptxt>The voltage may differ according to the amount that the sensor is tilted.</ptxt>
</atten4>
<spec>
<title>Standard Voltage</title>
<table>
<title>for RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 (SGFR) - 2 (SGG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sensor stationary</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approx. 2.0 to 2.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Sensor is tilted</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Change between approx. 0.9 to 2.3 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 (SGFL) - 2 (SGG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sensor stationary</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approx. 2.0 to 2.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Sensor is tilted</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Change between approx. 0.9 to 2.3 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002PE602HX_04_0004" fin="true">OK</down>
<right ref="RM000002PE602HX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PE602HX_04_0004">
<testtitle>REPLACE SUSPENSION CONTROL ECU<xref label="Seep01" href="RM000003A0D00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PE602HX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002PE602HX_04_0008">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PE602HX_04_0009">
<testtitle>REPLACE ACCELERATION SENSOR<xref label="Seep01" href="RM000003CMM00GX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>