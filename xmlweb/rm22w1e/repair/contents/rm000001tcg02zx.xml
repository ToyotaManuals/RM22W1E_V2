<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3TB_T00ME" variety="T00ME">
<name>ENGINE IMMOBILISER SYSTEM (w/o Entry and Start System)</name>
<para id="RM000001TCG02ZX" category="C" type-id="3010I" name-id="TD710-02" from="201308">
<dtccode>B2796</dtccode>
<dtcname>No Communication in Immobiliser System</dtcname>
<subpara id="RM000001TCG02ZX_01" type-id="60" category="03" proc-id="RM22W0E___0000EUM00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>DTC B2796 is stored when a key is inserted into the ignition key cylinder but no communication occurs between the key and transponder key ECU assembly.</ptxt>
</item>
</list1>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Output Confirmation Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2796</ptxt>
</entry>
<entry valign="middle">
<ptxt>The key code cannot be transmitted (1 trip detection logic*).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Key</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Transponder key ECU assembly</ptxt>
</item>
<item>
<ptxt>Transponder key amplifier</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Insert the key into the ignition key cylinder.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: Only output while a malfunction is present.</ptxt>
</item>
</list1>
<table pgwide="1">
<title>Vehicle Condition and Fail-safe Operation when Malfunction Detected</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Condition when Malfunction Detected</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-safe Operation when Malfunction Detected</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine cannot be started</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List and Active Test</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List and Active Test</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2796</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Response</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001TCG02ZX_02" type-id="32" category="03" proc-id="RM22W0E___0000EUN00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B193467E14" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001TCG02ZX_03" type-id="51" category="05" proc-id="RM22W0E___0000EUO00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the transponder key ECU assembly or key, refer to the Service Bulletin.</ptxt>
</item>
<item>
<ptxt>After performing repairs, perform the operation that fulfills the DTC output confirmation operation, and then confirm that no DTCs are output again.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001TCG02ZX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001TCG02ZX_08_0010" proc-id="RM22W0E___0000EUQ00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001TCQ02BX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0011" proc-id="RM22W0E___0000EUR00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Insert and remove each registered key one at a time, and check for DTCs each time (See page <xref label="Seep01" href="RM000001TCQ02BX"/>).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Before checking for DTCs, perform the "DTC Output Confirmation Operation" procedure.</ptxt>
</item>
<item>
<ptxt>If DTC B2796 is output, check for DTCs with the next key after clearing the DTCs.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2796 is not output.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (all keys)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (any key)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0015" fin="true">A</down>
<right ref="RM000001TCG02ZX_08_0035" fin="false">B</right>
<right ref="RM000001TCG02ZX_08_0026" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0035" proc-id="RM22W0E___0000EUY00001">
<testtitle>CHECK CONNECTION OF CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the connection of the transponder key ECU assembly and transponder key amplifier connectors.</ptxt>
</test1>
<test1>
<ptxt>Visually inspect the terminals of the transponder key ECU assembly and transponder key amplifier connectors.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Connectors are properly connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0036" fin="false">OK</down>
<right ref="RM000001TCG02ZX_08_0039" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0036" proc-id="RM22W0E___0000EUZ00001">
<testtitle>CHECK WHETHER ENGINE STARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the engine starts with an already registered vehicle key.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Engine starts normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0037" fin="false">OK</down>
<right ref="RM000001TCG02ZX_08_0020" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0037" proc-id="RM22W0E___0000EV000001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001TCQ02BX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0038" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0038" proc-id="RM22W0E___0000EV100001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000001TCQ02BX"/>).</ptxt>
<atten4>
<ptxt>Before checking for DTCs, perform the "DTC Output Confirmation Operation" procedure.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2796 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0027" fin="true">OK</down>
<right ref="RM000001TCG02ZX_08_0020" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0020" proc-id="RM22W0E___0000EUS00001">
<testtitle>CHECK TRANSPONDER KEY AMPLIFIER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B287569E05" width="2.775699831in" height="7.795582503in"/>
</figure>
<test2>
<ptxt>Waveform 1 (Reference)</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E95-1 (VC5) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
<test2>
<ptxt>Waveform 2 (Reference)</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E95-5 (TXCT) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
<test2>
<ptxt>Waveform 3 (Reference)</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E95-4 (CODE) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Transponder Key Amplifier)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform 3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Waveform 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Waveform 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Waveform 3)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0017" fin="true">A</down>
<right ref="RM000001TCG02ZX_08_0021" fin="false">B</right>
<right ref="RM000001TCG02ZX_08_0022" fin="false">C</right>
<right ref="RM000001TCG02ZX_08_0024" fin="false">D</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0021" proc-id="RM22W0E___0000EUT00001">
<testtitle>CHECK TRANSPONDER KEY AMPLIFIER (VC5)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B287949E07" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Waveform (Reference)</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E95-1 (VC5) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transponder Key Amplifier)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0005" fin="true">OK</down>
<right ref="RM000001TCG02ZX_08_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0004" proc-id="RM22W0E___0000EUP00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSPONDER KEY AMPLIFIER - TRANSPONDER KEY ECU ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E96 transponder key ECU assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" align="left" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E95-1 (VC5) - E96-14 (VC5)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E95-7 (AGND) - E96-5 (AGND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E95-1 (VC5) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0028" fin="true">OK</down>
<right ref="RM000001TCG02ZX_08_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0022" proc-id="RM22W0E___0000EUU00001">
<testtitle>CHECK TRANSPONDER KEY AMPLIFIER (TXCT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B287951E07" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Waveform (Reference)</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E95-5 (TXCT) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transponder Key Amplifier)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0029" fin="true">OK</down>
<right ref="RM000001TCG02ZX_08_0023" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0023" proc-id="RM22W0E___0000EUV00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSPONDER KEY AMPLIFIER - TRANSPONDER KEY ECU ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E96 transponder key ECU assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" align="left" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E95-5 (TXCT) - E96-4 (TXCT)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E95-5 (TXCT) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0031" fin="true">OK</down>
<right ref="RM000001TCG02ZX_08_0030" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0024" proc-id="RM22W0E___0000EUW00001">
<testtitle>CHECK TRANSPONDER KEY AMPLIFIER (CODE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B287950E10" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Waveform (Reference)</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E95-4 (CODE) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transponder Key Amplifier)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0032" fin="true">OK</down>
<right ref="RM000001TCG02ZX_08_0025" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0025" proc-id="RM22W0E___0000EUX00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSPONDER KEY AMPLIFIER - TRANSPONDER KEY ECU ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E96 transponder key ECU assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" align="left" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E95-4 (CODE) - E96-15 (CODE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E95-4 (CODE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCG02ZX_08_0034" fin="true">OK</down>
<right ref="RM000001TCG02ZX_08_0033" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0026">
<testtitle>REPLACE KEY THAT CANNOT START ENGINE</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0015">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0027">
<testtitle>END (CONNECTOR WAS NOT PROPERLY CONNECTED)</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0017">
<testtitle>REPLACE TRANSPONDER KEY ECU ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0005">
<testtitle>REPLACE TRANSPONDER KEY AMPLIFIER<xref label="Seep01" href="RM000003YO101VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0028">
<testtitle>REPLACE TRANSPONDER KEY ECU ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0029">
<testtitle>REPLACE TRANSPONDER KEY AMPLIFIER<xref label="Seep01" href="RM000003YO101VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0030">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0031">
<testtitle>REPLACE TRANSPONDER KEY ECU ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0032">
<testtitle>REPLACE TRANSPONDER KEY AMPLIFIER<xref label="Seep01" href="RM000003YO101VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0033">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0034">
<testtitle>REPLACE TRANSPONDER KEY ECU ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000001TCG02ZX_08_0039">
<testtitle>CONNECT CONNECTORS PROPERLY</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>