<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000PFB0TNX" category="C" type-id="3032S" name-id="ES11JS-001" from="201301" to="201308">
<dtccode>P0116</dtccode>
<dtcname>Engine Coolant Temperature Circuit Range / Performance Problem</dtcname>
<subpara id="RM000000PFB0TNX_01" type-id="60" category="03" proc-id="RM22W0E___00001JV00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0115 (See page <xref label="Seep01" href="RM000000PF50TBX_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>P0116</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met (2 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the engine is started cold and then warmed up, the engine coolant temperature sensor value does not change.</ptxt>
</item>
<item>
<ptxt>After the warmed up engine is stopped and the next cold engine start is performed, the engine coolant temperature sensor value does not change.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Thermostat</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature sensor</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine coolant temperature is between 35°C and 60°C (95°F and 140°F) when the engine is started and conditions (a) and (b) are met (2 trip detection logic):</ptxt>
<ptxt>(a) Vehicle is driven at varying speeds (accelerated and decelerated).</ptxt>
<ptxt>(b) Engine coolant temperature remains within 3°C (5.4°F) of the initial engine coolant temperature.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Thermostat</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature sensor</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000PFB0TNX_02" type-id="64" category="03" proc-id="RM22W0E___00001JW00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<topic>
<title>Engine coolant temperature sensor cold start monitor</title>
<ptxt>When a cold engine start is performed and then the engine is warmed up, if the engine coolant temperature sensor value does not change, it is determined that a malfunction has occurred. If this is detected in 2 consecutive driving cycles, the MIL is illuminated and a DTC is stored.</ptxt>
</topic>
<topic>
<title>Engine coolant temperature sensor soak monitor</title>
<ptxt>If the engine coolant temperature sensor value does not change after the warmed up engine is stopped and then the next cold engine start is performed, it is determined that a malfunction has occurred. If this is detected in 2 consecutive driving cycles, the MIL is illuminated and a DTC is stored.</ptxt>
</topic>
<topic>
<title>Engine coolant temperature sensor high side stuck monitor</title>
<ptxt>The ECM monitors the sensor voltage and uses this value to calculate the engine coolant temperature. If the sensor voltage output deviates from the normal operating range, the ECM interprets this deviation as a malfunction in the engine coolant temperature sensor and stores the DTC.</ptxt>
<ptxt>Example:</ptxt>
<ptxt>Upon starting the engine, the engine coolant temperature is between 35°C and 60°C (95°F and 140°F). If, after driving for 250 seconds, the engine coolant temperature remains within 3°C (5.4°F) of the starting temperature, the DTC is stored (2 trip detection logic).</ptxt>
</topic>
</content5>
</subpara>
<subpara id="RM000000PFB0TNX_07" type-id="51" category="05" proc-id="RM22W0E___00001JX00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTC P0115, P0117 or P0118 is stored simultaneously with DTC P0116, the engine coolant temperature sensor may have an open or short circuit. Troubleshoot those DTCs first.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFB0TNX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PFB0TNX_08_0001" proc-id="RM22W0E___00001JY00000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0116)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0116 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0116 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0116 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PFB0TNX_08_0006" fin="false">A</down>
<right ref="RM000000PFB0TNX_08_0003" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PFB0TNX_08_0006" proc-id="RM22W0E___00001JZ00000">
<testtitle>INSPECT THERMOSTAT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the thermostat (See page <xref label="Seep01" href="RM000002BG802XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the valve opening temperature of the thermostat.</ptxt>
<spec>
<title>Standard opening temperature</title>
<specitem>
<ptxt>80 to 84°C (176 to 183°F)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>In addition to the above check, confirm that the valve is completely closed when the temperature is below the standard.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Reinstall the thermostat (See page <xref label="Seep02" href="RM000002BG902YX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFB0TNX_08_0002" fin="true">OK</down>
<right ref="RM000000PFB0TNX_08_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFB0TNX_08_0003">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000032SF04AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFB0TNX_08_0005">
<testtitle>REPLACE THERMOSTAT<xref label="Seep01" href="RM000002BG802XX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFB0TNX_08_0002">
<testtitle>REPLACE ENGINE COOLANT TEMPERATURE SENSOR<xref label="Seep01" href="RM000002PQG035X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>