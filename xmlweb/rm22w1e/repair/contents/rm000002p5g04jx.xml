<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TW_T00MZ" variety="T00MZ">
<name>AIRBAG SYSTEM</name>
<para id="RM000002P5G04JX" category="C" type-id="305HV" name-id="RSGUC-01" from="201308">
<dtccode>B1655/37</dtccode>
<dtcname>Seat Belt Buckle Switch RH Circuit Malfunction</dtcname>
<dtccode>B1656/38</dtccode>
<dtcname>Seat Belt Buckle Switch LH Circuit Malfunction</dtcname>
<subpara id="RM000002P5G04JX_01" type-id="60" category="03" proc-id="RM22W0E___0000FB200001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>for RHD:</ptxt>
<ptxt>The seat belt buckle switch RH circuit consists of the center airbag sensor and the front seat inner belt RH. DTC B1655/37 is stored when a malfunction is detected in the seat belt buckle switch RH circuit.</ptxt>
<ptxt>for LHD:</ptxt>
<ptxt>The seat belt buckle switch LH circuit consists of the center airbag sensor and the front seat inner belt LH.</ptxt>
<ptxt>DTC B1656/38 is recorded when a malfunction is detected in the seat belt buckle switch LH circuit.</ptxt>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="left">
<colspec colname="COLSPEC0" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1655/37</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives a line short circuit signal, an open circuit signal, a short circuit to ground signal or a short circuit to B+ signal in the seat belt buckle switch RH circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A front seat inner belt RH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>No. 2 floor wire</ptxt>
</item>
<item>
<ptxt>Front seat inner belt assembly RH (Seat belt buckle switch RH)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="left">
<colspec colname="COLSPEC0" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1656/38</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives a line short circuit signal, an open circuit signal, a short circuit to ground signal or a short circuit to B+ signal in the seat belt buckle switch LH circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A front seat inner belt LH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Floor wire</ptxt>
</item>
<item>
<ptxt>Front seat inner belt assembly LH (Seat belt buckle switch LH)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002P5G04JX_02" type-id="32" category="03" proc-id="RM22W0E___0000FB300001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C253566E06" width="7.106578999in" height="4.7836529in"/>
</figure>
<figure>
<graphic graphicname="C174608E05" width="7.106578999in" height="4.7836529in"/>
</figure>
<figure>
<graphic graphicname="C253566E07" width="7.106578999in" height="4.7836529in"/>
</figure>
<figure>
<graphic graphicname="C174608E04" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002P5G04JX_03" type-id="51" category="05" proc-id="RM22W0E___0000FB400001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000002P5G04JX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002P5G04JX_04_0015" proc-id="RM22W0E___0000FBC00001">
<testtitle>CHECK VEHICLE CONDITION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check vehicle condition.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>for RHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0001" fin="false">A</down>
<right ref="RM000002P5G04JX_04_0016" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0001" proc-id="RM22W0E___0000FB500001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
<figure>
<graphic graphicname="C174605E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1656 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1656 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0008" fin="true">OK</down>
<right ref="RM000002P5G04JX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0002" proc-id="RM22W0E___0000FB600001">
<testtitle>CHECK CONNECTION OF CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors are properly connected to the center airbag sensor and front seat inner belt LH.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are properly connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0003" fin="false">OK</down>
<right ref="RM000002P5G04JX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0003" proc-id="RM22W0E___0000FB700001">
<testtitle>CHECK CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor and the front seat inner belt LH.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors (on the center airbag sensor side and front seat inner belt LH side) are not damaged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are not deformed or damaged.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0004" fin="false">OK</down>
<right ref="RM000002P5G04JX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0004" proc-id="RM22W0E___0000FB800001">
<testtitle>CHECK FLOOR WIRE (CENTER AIRBAG SENSOR - FRONT SEAT INNER BELT LH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
<figure>
<graphic graphicname="C263305E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K10-6 (LBE+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K10-7 (LBE-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>w/ Side Airbag:</ptxt>
<test2>
<ptxt>Turn the ignition switch off.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test2>
<test2>
<ptxt>Using a service wire, connect terminals 10 (LBE+) and 18 (LBE-) of connector B.</ptxt>
<atten3>
<ptxt>Do not forcibly insert the service wire into the terminals of the connector when connecting a service wire.</ptxt>
</atten3>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K10-6 (LBE+) - K10-7 (LBE-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
<test2>
<ptxt>Disconnect a service wire from connector B.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K10-6 (LBE+) - K10-7 (LBE-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K10-6 (LBE+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K10-7 (LBE-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>w/o Side airbag:</ptxt>
<test2>
<ptxt>Turn the ignition switch off.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test2>
<test2>
<ptxt>Using a service wire, connect terminals 11 (LBE+) and 12 (LBE-) of connector B.</ptxt>
<atten3>
<ptxt>Do not forcibly insert the service wire into the terminals of the connector when connecting a service wire.</ptxt>
</atten3>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K10-6 (LBE+) - K10-7 (LBE-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
<test2>
<ptxt>Disconnect a service wire from connector B.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K10-6 (LBE+) - K10-7 (LBE-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K10-6 (LBE+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K10-7 (LBE-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0005" fin="false">OK</down>
<right ref="RM000002P5G04JX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0005" proc-id="RM22W0E___0000FB900001">
<testtitle>CHECK FRONT SEAT INNER BELT LH (SEAT BELT BUCKLE SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the connector to the center airbag sensor and the front seat inner belt LH.</ptxt>
<figure>
<graphic graphicname="C174605E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1656 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1656 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0012" fin="true">OK</down>
<right ref="RM000002P5G04JX_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0006" proc-id="RM22W0E___0000FBA00001">
<testtitle>REPLACE FRONT SEAT INNER BELT LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Replace the front seat inner belt LH (See page <xref label="Seep01" href="RM000002LJ0036X"/>).</ptxt>
<atten4>
<ptxt>Perform the inspection using parts from a normally functioning vehicle if possible.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0007" proc-id="RM22W0E___0000FBB00001">
<testtitle>CHECK CENTER AIRBAG SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
<figure>
<graphic graphicname="C174605E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1656 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1656 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0014" fin="true">OK</down>
<right ref="RM000002P5G04JX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0016" proc-id="RM22W0E___0000FBD00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
<figure>
<graphic graphicname="C146230E14" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1655 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Code other than DTC B1655 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0023" fin="true">OK</down>
<right ref="RM000002P5G04JX_04_0017" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0017" proc-id="RM22W0E___0000FBE00001">
<testtitle>CHECK CONNECTION OF CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors are properly connected to the center airbag sensor and front seat inner belt RH.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are properly connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0018" fin="false">OK</down>
<right ref="RM000002P5G04JX_04_0024" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0018" proc-id="RM22W0E___0000FBF00001">
<testtitle>CHECK CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor and the front seat inner belt RH.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors (on the center airbag sensor side and front seat inner belt RH side) are not damaged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are not deformed or damaged.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0019" fin="false">OK</down>
<right ref="RM000002P5G04JX_04_0025" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0019" proc-id="RM22W0E___0000FBG00001">
<testtitle>CHECK NO. 2 FLOOR WIRE (CENTER AIRBAG SENSOR - FRONT SEAT INNER BELT RH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
<figure>
<graphic graphicname="C263306E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L33-6 (RBE+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L33-7 (RBE-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>w/ Side Airbag:</ptxt>
<test2>
<ptxt>Turn the ignition switch off.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test2>
<test2>
<ptxt>Using a service wire, connect terminals 15 (RBE+) and 23 (RBE-) of connector B.</ptxt>
<atten3>
<ptxt>Do not forcibly insert the service wire into the terminals of the connector when connecting a service wire.</ptxt>
</atten3>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L33-6 (RBE+) - L33-7 (RBE-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
<test2>
<ptxt>Disconnect a service wire from connector B.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L33-6 (RBE+) - L33-7 (RBE-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L33-6 (RBE+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L33-7 (RBE-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>w/o Side airbag:</ptxt>
<test2>
<ptxt>Turn the ignition switch off.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test2>
<test2>
<ptxt>Using a service wire, connect terminals 15 (RBE+) and 14 (RBE-) of connector B.</ptxt>
<atten3>
<ptxt>Do not forcibly insert the service wire into the terminals of the connector when connecting a service wire.</ptxt>
</atten3>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L33-6 (RBE+) - L33-7 (RBE-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
<test2>
<ptxt>Disconnect a service wire from connector B.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L33-6 (RBE+) - L33-7 (RBE-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L33-6 (RBE+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L33-7 (RBE-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0020" fin="false">OK</down>
<right ref="RM000002P5G04JX_04_0026" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0020" proc-id="RM22W0E___0000FBH00001">
<testtitle>CHECK FRONT SEAT INNER BELT RH (SEAT BELT BUCKLE SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the connector to the center airbag sensor and the front seat inner belt RH.</ptxt>
<figure>
<graphic graphicname="C146230E14" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1655 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1655 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0027" fin="true">OK</down>
<right ref="RM000002P5G04JX_04_0021" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0021" proc-id="RM22W0E___0000FBI00001">
<testtitle>REPLACE FRONT SEAT INNER BELT RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Replace the front seat inner belt RH (See page <xref label="Seep01" href="RM000002LJ0036X"/>).</ptxt>
<atten4>
<ptxt>Perform the inspection using parts from a normally functioning vehicle if possible.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0022" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0022" proc-id="RM22W0E___0000FBJ00001">
<testtitle>CHECK CENTER AIRBAG SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
<figure>
<graphic graphicname="C146230E14" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1655 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1655 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002P5G04JX_04_0029" fin="true">OK</down>
<right ref="RM000002P5G04JX_04_0028" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0008">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0KMX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0009">
<testtitle>CONNECT CONNECTORS PROPERLY</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0010">
<testtitle>REPLACE FLOOR WIRE</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0011">
<testtitle>REPLACE FLOOR WIRE</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0012">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0KMX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0013">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000002Y2N00ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0014">
<testtitle>REPLACE FRONT SEAT INNER BELT ASSEMBLY LH<xref label="Seep01" href="RM000002LJ0036X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0023">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0KMX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0024">
<testtitle>CONNECT CONNECTORS PROPERLY</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0025">
<testtitle>REPLACE NO. 2 FLOOR WIRE</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0026">
<testtitle>REPLACE NO. 2 FLOOR WIRE</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0027">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0KMX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0028">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000002Y2N00ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002P5G04JX_04_0029">
<testtitle>REPLACE FRONT SEAT INNER BELT ASSEMBLY RH</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>