<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000S" variety="S000S">
<name>1VD-FTV INTAKE / EXHAUST</name>
<ttl id="12010_S000S_7C3JF_T00CI" variety="T00CI">
<name>EXHAUST MANIFOLD W/  TURBOCHARGER</name>
<para id="RM000004BIU02EX" category="D" type-id="303F2" name-id="IT1OR-26" from="201301">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000004BIU02EX_z0" proc-id="RM22W0E___00006RN00000">
<content5 releasenbr="1">
<ptxt>This section should be used as a guide for troubleshooting when the turbocharger is suspected as the cause of a problem.</ptxt>
<step1>
<ptxt>OUTLINE OF TURBOCHARGER FAILURE</ptxt>
<step2>
<ptxt>State of Turbocharger Failure Repair:</ptxt>
<ptxt>It is well known that turbocharger malfunctions cause many symptoms as shown below. However, the mechanisms resulting in these symptoms that indicate turbocharger malfunctions are not well understood. As a result, many unnecessary turbocharger replacements and other repairs are being performed due to lack of knowledge about the turbocharger and turbocharger failure. Therefore, knowing the facts regarding turbocharger malfunctions is useful for making effective repairs and saving time.</ptxt>
</step2>
<step2>
<ptxt>Turbocharger Failure Classification</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Symptom Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Noise</ptxt>
</entry>
<entry valign="middle">
<ptxt>Whistling noise</ptxt>
</entry>
<entry valign="middle">
<ptxt>Continuous high-pitched noise proportional to engine speed</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>"NOISE" listed below</ptxt>
</item>
<item>
<ptxt>"Turbocharger Noise" Flowchart</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>w/ DPF: (See page <xref label="Seep01" href="RM00000416S089X"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF: (See page <xref label="Seep02" href="RM00000416S088X"/>)</ptxt>
</item>
</list2>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Whining noise</ptxt>
</entry>
<entry valign="middle">
<ptxt>Relatively low-pitched noise compared to whistling noise</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Oil leak</ptxt>
</entry>
<entry valign="middle">
<ptxt>External oil leak</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil leak on surface of turbocharger visible from outside of turbocharger</ptxt>
</entry>
<entry morerows="3" valign="middle">
<list1 type="unordered">
<item>
<ptxt>"OIL LEAK AND WHITE SMOKE" listed below</ptxt>
</item>
<item>
<ptxt>"Turbocharger Oil Leak and White Smoke" Flowchart</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>w/ DPF: (See page <xref label="Seep03" href="RM00000416R09FX"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF: (See page <xref label="Seep04" href="RM00000416R09EX"/>)</ptxt>
</item>
</list2>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Internal oil leak</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil leak from inside of bearing housing to inside of either compressor housing or turbine housing through seal ring</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>White smoke</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil smoke</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil smoke is emitted from exhaust pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Unburned fuel smoke</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unburned fuel smoke is emitted from exhaust pipe</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Black smoke</ptxt>
</entry>
<entry valign="middle">
<ptxt>Black smoke is emitted from exhaust pipe</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>"BLACK SMOKE" listed below</ptxt>
</item>
<item>
<ptxt>"Black Smoke Emitted" Flowchart</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>w/ DPF: (See page <xref label="Seep05" href="RM000000TIR0E7X"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF: (See page <xref label="Seep06" href="RM000000TIR0E6X"/>)</ptxt>
</item>
</list2>
</list1>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" morerows="2" valign="middle">
<ptxt>Lack of power or hesitation</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle does not reach target speed</ptxt>
</entry>
<entry morerows="2" valign="middle">
<list1 type="unordered">
<item>
<ptxt>"LACK OF POWER AND HESITATION" listed below</ptxt>
</item>
<item>
<ptxt>"Lack of Power or Hesitation" Flowchart</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>w/ DPF: (See page <xref label="Seep07" href="RM000000W0E0BTX"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF: (See page <xref label="Seep08" href="RM000000W0E0BSX"/>)</ptxt>
</item>
</list2>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Poor acceleration</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shock during acceleration</ptxt>
</entry>
</row>
<row>
<entry morerows="19" valign="middle">
<ptxt>MIL turns on</ptxt>
<ptxt>(DTC)</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0046</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control Solenoid Circuit Range / Performance</ptxt>
</entry>
<entry morerows="19" valign="middle">
<ptxt>Diagnostic Trouble Code Chart</ptxt>
<list1 type="unordered">
<item>
<ptxt>w/ DPF: (See page <xref label="Seep09" href="RM0000031HW05CX"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF: (See page <xref label="Seep10" href="RM0000031HW05BX"/>)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0047</ptxt>
</entry>
<entry valign="middle">
<ptxt>Boost Control "A" Circuit Low</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0048</ptxt>
</entry>
<entry valign="middle">
<ptxt>Boost Control "A" Circuit High</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P004B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Boost control solenoid "B" circuit Range / Performance</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P004C</ptxt>
</entry>
<entry valign="middle">
<ptxt>Boost control solenoid "B" circuit low</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P004D</ptxt>
</entry>
<entry valign="middle">
<ptxt>Boost control solenoid "B" circuit high</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P00AF</ptxt>
<ptxt>
<sup>*</sup>1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Boost control "A" module performance</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P00B0</ptxt>
<ptxt>
<sup>*</sup>1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Boost control "B" module performance</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0299</ptxt>
</entry>
<entry valign="middle">
<ptxt>Underboost</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P1251</ptxt>
</entry>
<entry valign="middle">
<ptxt>Overboost Condition (Too High)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P1258</ptxt>
<ptxt>
<sup>*</sup>1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position sensor circuit range/performance bank 2 sensor 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P1259</ptxt>
<ptxt>
<sup>*</sup>1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position sensor low bank 2 sensor 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P1260</ptxt>
<ptxt>
<sup>*</sup>1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position sensor high bank 2 sensor 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P1262</ptxt>
<ptxt>
<sup>*</sup>1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position sensor low bank 2 sensor 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P1263</ptxt>
<ptxt>
<sup>*</sup>1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position sensor high bank 2 sensor 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2563</ptxt>
<ptxt>
<sup>*</sup>1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position Sensor "A" Circuit Range/Performance</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2564</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position Sensor "A" Circuit Low</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2565</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position Sensor "A" Circuit High</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2588</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position Sensor "B" Circuit Low</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2589</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position Sensor "B" Circuit High</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>
<sup>*</sup>1: w/o DPF</ptxt>
</item>
<item>
<ptxt>This table shows only typical problems related to the turbocharger.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
<step1>
<ptxt>NOISE</ptxt>
<table pgwide="1">
<title>Description</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Probable Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Probable Failed Component</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Turbine shaft imbalance</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbocharger</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Leakage from intake line</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake line</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Gear noise</ptxt>
<ptxt>(Mistaken for turbocharger noise)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Gear inside engine</ptxt>
</item>
<item>
<ptxt>Transmission gear</ptxt>
</item>
<item>
<ptxt>Vacuum pump gear</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>It is easy to confirm whether the turbocharger is the cause of the noise or not, and confirming this before inspecting the turbocharger or removing it from the engine is an effective way to reduce troubleshooting time.</ptxt>
</atten4>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Start the engine and warm it up.</ptxt>
</step2>
<step2>
<ptxt>Turn the tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine / Active Test / Activate the VN Turbo Open.</ptxt>
<atten4>
<ptxt>This Active Test function operates both the turbocharger actuators (bank 1 and bank 2) at the same time.</ptxt>
</atten4>
</step2>
<step2>
<ptxt>Perform the Active Test and rev the engine up several times.</ptxt>
</step2>
<step2>
<ptxt>Check whether the noise is reduced or not compared with the noise heard when the Active Test is not performed.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Cause of Noise</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>The noise is reduced (or disappears)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Turbocharger</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>The noise does not change</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not turbocharger (other parts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Refer to the "Turbocharger Noise" flowchart.</ptxt>
<list1 type="unordered">
<item>
<ptxt>w/ DPF: (See page <xref label="Seep11" href="RM00000416S089X"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF: (See page <xref label="Seep12" href="RM00000416S088X"/>)</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
<step1>
<ptxt>OIL LEAK AND WHITE SMOKE</ptxt>
<table pgwide="1">
<title>Description</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Oil Leak Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Internal oil leak</ptxt>
<ptxt>(White smoke)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Oil leak from bearing housing to either compressor housing (intake side) or turbine housing (exhaust side) through seal rings.</ptxt>
</item>
<item>
<ptxt>This type of oil leak is not visible from outside of turbocharger.</ptxt>
</item>
<item>
<ptxt>If oil leak occurs from turbine side seal, large amount of white smoke is emitted from exhaust pipe.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Compressor side seal ring</ptxt>
</item>
<item>
<ptxt>Turbine side seal ring</ptxt>
</item>
<item>
<ptxt>Clogging of oil drain</ptxt>
</item>
<item>
<ptxt>Shaft breakage</ptxt>
</item>
<item>
<ptxt>Shaft or bearing seizure</ptxt>
</item>
<item>
<ptxt>Compressor impeller damage</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>External oil leak</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Oil leak from inside of turbocharger to outside of turbocharger.</ptxt>
</item>
<item>
<ptxt>Includes oil leaks visible from outside of turbocharger.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>FIPG sealing part</ptxt>
</item>
<item>
<ptxt>Oil pipe flange</ptxt>
</item>
<item>
<ptxt>Oil pipe union</ptxt>
</item>
<item>
<ptxt>Hose connection of intake pipe</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="A229639E03" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor Housing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seal Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor Inlet</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor Impeller</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bearing Housing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbine Shaft</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Drain (Outlet)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>FIPG Sealing Part</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbine Housing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbine Wheel</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>DC Motor Type Turbocharger (No. 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Internal oil leak to compressor housing</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Internal oil leak to turbine housing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The above illustration is an example.</ptxt>
</item>
<item>
<ptxt>When there is an internal oil leak, white smoke is emitted from the exhaust pipe and oil is consumed excessively. However, the cause of white smoke or excessive oil consumption can vary. Therefore, do not assume that the turbocharger is the cause of the failure when there is white smoke emission or excessive oil consumption.</ptxt>
</item>
<item>
<ptxt>When there is an external oil leak, the sources of the oil leak are limited to the points mentioned in the table above. If oil leaks from a FIPG sealing part, replace the turbocharger. If oil leaks from an oil pipe flange or a hose connection, do not replace the turbocharger, but confirm and repair the flange or hose.</ptxt>
</item>
<item>
<ptxt>Refer to the "Turbocharger Oil Leak and White Smoke" flowchart.</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>w/ DPF: (See page <xref label="Seep13" href="RM00000416R09FX"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF: (See page <xref label="Seep14" href="RM00000416R09EX"/>)</ptxt>
</item>
</list2>
</list1>
</atten4>
</step1>
<step1>
<ptxt>BLACK SMOKE</ptxt>
<step2>
<ptxt>Malfunctions are classified into 2 types as shown below.</ptxt>
<table pgwide="1">
<title>Description</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Fault</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Intake air volume shortage</ptxt>
</entry>
<entry valign="middle">
<ptxt>Insufficient mass airflow due to, for example, excessively low boost pressure, which results in fuel injection volume being relatively excessive with respect to mass airflow.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Excessive injection volume</ptxt>
</entry>
<entry valign="middle">
<ptxt>Excessive injection volume or incorrect injection timing due to fuel system trouble.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Main Components Related to Black Smoke:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Possible Faulty Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Fault</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Turbocharger</ptxt>
</entry>
<entry valign="middle">
<ptxt>Abnormally low boost pressure</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Intake system</ptxt>
</entry>
<entry valign="middle">
<ptxt>Leakage between turbocharger and intake manifold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fuel system</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Excessive fuel injection volume</ptxt>
</item>
<item>
<ptxt>Incorrect fuel injection timing</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>EGR valve</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stuck or does not close completely</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Diesel throttle</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stuck or does not move smoothly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The components listed above are only the main ones. Not all the components potentially related to black smoke are listed. For details regarding the troubleshooting of black smoke, refer to the "Black Smoke Emitted" flowchart.</ptxt>
<list1 type="unordered">
<item>
<ptxt>w/ DPF: (See page <xref label="Seep15" href="RM000000TIR0E7X"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF: (See page <xref label="Seep16" href="RM000000TIR0E6X"/>)</ptxt>
</item>
</list1>
</atten4>
</step2>
<step2>
<ptxt>Relation between Turbocharger and Black Smoke:</ptxt>
<ptxt>If the boost pressure is lower than normal due to a turbocharger failure, black smoke may occur due to a lack of mass airflow. However, abnormally low boost pressure can be caused by the failure of various components such as intake lines, the EGR valve, etc. Therefore, do not assume that the turbocharger is the cause of abnormally low boost pressure, but check all the components possibly related to abnormally low boost pressure. Components related to abnormal boost pressure are shown in a chart listed in the On-vehicle Inspection for the Intake System (See page <xref label="Seep17" href="RM0000031FH005X_01_0003"/>). For simple and effective troubleshooting, refer to the chart before starting troubleshooting.</ptxt>
</step2>
</step1>
<step1>
<ptxt>LACK OF POWER AND HESITATION</ptxt>
<step2>
<ptxt>Malfunctions are classified into 2 types as shown below.</ptxt>
<table pgwide="1">
<title>Description</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Fault</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Intake air volume shortage</ptxt>
</entry>
<entry valign="middle">
<ptxt>Insufficient mass airflow due to, for example, excessively low boost pressure, which results in fuel injection volume being restricted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Abnormal injection volume</ptxt>
</entry>
<entry valign="middle">
<ptxt>Abnormal injection volume or timing due to fuel system trouble.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Main Components Related to Lack of Power and Hesitation:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Possible Faulty Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Fault</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Turbocharger</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Abnormal boost pressure</ptxt>
</item>
<item>
<ptxt>VN does not move smoothly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Intake system</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Leakage between turbocharger and intake manifold</ptxt>
</item>
<item>
<ptxt>Clogging or blockage of intake line</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fuel system</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Abnormal injection volume</ptxt>
</item>
<item>
<ptxt>Incorrect fuel injection timing</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>EGR valve</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stuck or does not close completely</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Diesel throttle</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stuck or does not move smoothly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Exhaust system</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clogging of exhaust line</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The components listed above are only the main ones. Not all the components potentially related to lack of power and hesitation are listed. For details regarding the troubleshooting of lack of power and hesitation, refer to the "Lack of Power or Hesitation" flowchart.</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>w/ DPF: (See page <xref label="Seep18" href="RM000000W0E0BTX"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF: (See page <xref label="Seep19" href="RM000000W0E0BSX"/>)</ptxt>
</item>
</list2>
<item>
<ptxt>If an obvious malfunction (lack of power) has not been reproduced, test drive another vehicle of the same model which has the same engine, and compare the engine conditions and performance. If there is no great difference in engine performance, explain to the customer that the lack of power the customer mentioned is not abnormal.</ptxt>
</item>
</list1>
</atten4>
</step2>
<step2>
<ptxt>Relation between Turbocharger and Abnormal Boost Pressure:</ptxt>
<ptxt>If the boost pressure is lower than normal due to a turbocharger failure, a lack of power could occur due to an intake air volume shortage. However, abnormal boost pressure can be caused by the failure of various components such as intake lines, the EGR valve, etc. Therefore, do not assume that the turbocharger is the cause of abnormal boost pressure, but check all the components possibly related to abnormal boost pressure. Components related to abnormal boost pressure are shown in a chart listed in the On-vehicle Inspection for the Intake System (See page <xref label="Seep20" href="RM0000031FH005X_01_0003"/>). For simple and effective troubleshooting, refer to the chart before starting troubleshooting.</ptxt>
</step2>
</step1>
<step1>
<ptxt>MIL TURNS ON</ptxt>
<ptxt>If a DTC related to a turbocharger malfunction is stored, refer to the troubleshooting section for each DTC.</ptxt>
<list1 type="unordered">
<item>
<ptxt>w/ DPF: (See page <xref label="Seep21" href="RM0000031HW05CX"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF: (See page <xref label="Seep22" href="RM0000031HW05BX"/>)</ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>BRIEF OUTLINE OF TURBOCHARGER OPERATION AND CONSTRUCTION</ptxt>
<step2>
<ptxt>A turbocharger is a component used to supply a larger air volume to the cylinders by recovering exhaust gas energy using a turbine coaxially connected to a compressor.</ptxt>
</step2>
<step2>
<ptxt>Principle of Turbocharging:</ptxt>
<ptxt>Boost pressure is proportional to turbocharger speed, because the intake air is accelerated by centrifugal force generated by the rotation of the compressor and the increased kinetic energy, i.e. the velocity of the intake air, is converted to pressure energy by the diffuser located around the outlet of the compressor impeller. The compressor is driven by the turbine connected coaxially by the turbine shaft. The turbine is driven by exhaust gas energy. Therefore, when the turbocharger begins boosting the intake air, a larger air volume is supplied to the cylinders and more fuel can be injected. As a result, more exhaust energy will be available and the turbocharger boost increases.</ptxt>
<figure>
<graphic graphicname="A227153E01" width="7.106578999in" height="7.795582503in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" align="left" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbine</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Cleaner</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Manifold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Manifold</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intercooler (w/ Intercooler)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diffuser</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>See HINT below</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Airflow</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>
<sup>*</sup>a: If sufficient exhaust gas energy is not available, the turbocharger cannot generate the required boost pressure even when the turbocharger does not have a malfunction.</ptxt>
</item>
<item>
<ptxt>Considering the fact that the turbocharger is driven by exhaust gas energy, if sufficient exhaust gas is not available due to abnormal injection volume, etc., the required boost pressure will not be available even when the turbocharger does not have a malfunction. Therefore, when boost pressure is abnormally low, checking all the related components using the correct troubleshooting procedure is necessary to perform repairs simply and effectively.</ptxt>
</item>
</list1>
</atten4>
</step2>
<step2>
<ptxt>Boost Pressure Control:</ptxt>
<ptxt>The amount of energy the turbine can obtain from the exhaust gas is proportional to the expansion ratio, which is defined as the ratio of the turbine inlet exhaust gas pressure to the pressure at the turbine outlet.</ptxt>
<ptxt>To control boost pressure, a Variable Nozzle (VN) is used just upstream of the turbine wheel inlet, and controls the expansion ratio. If the VN is closed, the gap between neighboring vanes is narrowed and the turbine inlet exhaust gas pressure, and correspondingly the expansion ratio, increases. Therefore, when the VN is closed, the turbine receives more energy, and the turbine speed and boost pressure increase. On the other hand, if the VN is opened, the turbine inlet exhaust gas pressure decreases, and the turbine speed and boost pressure decrease. The VN is actuated by a DC motor.</ptxt>
<ptxt>w/o DPF: The ECM controls the VN opening angle through the turbo motor driver in accordance with the engine condition.</ptxt>
<ptxt>When a high engine power is required, the actuation rod is moved by the actuator to close the VN and boost pressure increases.</ptxt>
<figure>
<graphic graphicname="A211795E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN (Variable Nozzle)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbine Wheel</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Narrow Gap</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wide Gap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN Closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN Opened</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Gas Flow</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the VN becomes stuck open, the necessary boost pressure will not be available. If the VN becomes stuck closed, overboost will occur.</ptxt>
</atten4>
</step2>
<step2>
<ptxt>Mechanical Construction of Turbocharger:</ptxt>
<figure>
<graphic graphicname="A229645E02" width="7.106578999in" height="7.795582503in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN Actuator (DC Motor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN Actuating Rod</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bearing Housing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbine Shaft</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radial Bearing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbine Side Seal Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>VN (Variable Nozzle)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbine Housing</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbine Wheel</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Drain</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*11</ptxt>
</entry>
<entry valign="middle">
<ptxt>Thrust Bearing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*12</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor Impeller</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*13</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor Housing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*14</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor Side Seal Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>See HINT below</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>See HINT below</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>See HINT below</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>DC Motor Type Turbocharger (No. 1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Exhaust Gas Flow</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Intake Airflow</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The above illustration is an example.</ptxt>
</item>
<item>
<ptxt>
<sup>*</sup>a: The clearances of the radial bearing and thrust bearing are on the order of 100 μm, and for the accurate measurement of these clearances, an accurate process and accurate tools are essential.</ptxt>
</item>
<item>
<ptxt>
<sup>*</sup>b: A certain amount of oil mist from PCV gas is contained in the intake air. Therefore, a certain amount of oil at the inlet of the compressor is normal, and is not an oil leak.</ptxt>
</item>
<item>
<ptxt>
<sup>*</sup>c: The seal rings are C-shaped rings just like piston rings, and have a gap. Therefore, complete sealing is impossible by the seal rings alone. The oil is sealed in with the aid of the boost pressure in the compressor housing, and the exhaust gas pressure in the turbine housing. These pressures prevent oil from exiting the bearing housing through the gap of the seal rings. Therefore, if the turbine shaft is inclined from the horizontal, oil may flow out through the gap of a seal ring. This should not be interpreted as an oil leak due to seal ring failure.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>