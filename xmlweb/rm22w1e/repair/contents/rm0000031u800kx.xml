<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001F" variety="S001F">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001F_7C3NX_T00H0" variety="T00H0">
<name>DIFFERENTIAL SYSTEM</name>
<para id="RM0000031U800KX" category="T" type-id="3001H" name-id="DF0I5-08" from="201301">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM0000031U800KX_z0" proc-id="RM22W0E___00009C100000">
<content5 releasenbr="2">
<atten4>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Front Differential</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Oil leakage</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Oil (level too high or wrong grade)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318T00NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Carrier oil seal (worn or damaged)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000002HW9010X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Companion flange (loose or damaged)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000002HW9010X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="5" colsep="1" valign="middle" align="left">
<ptxt>Noise in front differential</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Oil level (low or wrong grade)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318T00NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Ring gear or drive pinion (worn or chipped)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000016GC02DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Backlash adjustment (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000016GC02DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Preload adjustment (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000016GC02DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Tooth contact between ring gear and drive pinion (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000016GC02DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Bearing (worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000016GC02DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Rear Differential (for Standard)</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Oil leakage</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Oil (level too high or wrong grade)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318T00NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Carrier oil seal (worn or damaged)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318H00OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Companion flange (loose or damaged)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318H00OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="5" colsep="1" valign="middle" align="left">
<ptxt>Noise in rear differential</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Oil level (low or wrong grade)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318T00NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Ring gear or drive pinion (worn or chipped)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318H00OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Backlash adjustment (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318H00OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Preload adjustment (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318H00OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Tooth contact between ring gear and drive pinion (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318H00OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Bearing (worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318H00OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Rear Differential (for LSD)</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Oil leakage</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Oil (level too high or wrong grade)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318T00NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Carrier oil seal (worn or damaged)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318400LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Companion flange (loose or damaged)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318400LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="5" colsep="1" valign="middle" align="left">
<ptxt>Noise in rear differential</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Oil level (low or wrong grade)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318T00NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Ring gear or drive pinion (worn or chipped)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318R003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Backlash adjustment (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318R003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Preload adjustment (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318R003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Tooth contact between ring gear and drive pinion (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318R003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Bearing (worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318R003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Rear Differential (w/ Differential Lock)</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Oil leakage</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Oil (level too high or wrong grade)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318T00NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Carrier oil seal (worn or damaged)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318400LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Companion flange (loose or damaged)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000000UYV0D1X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="5" colsep="1" valign="middle" align="left">
<ptxt>Noise in rear differential</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Oil level (low or wrong grade)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318T00NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Ring gear or drive pinion (worn or chipped)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318M003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Backlash adjustment (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318M003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Preload adjustment (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318M003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Tooth contact between ring gear and drive pinion (defective)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318M003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Bearing (worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318M003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="7" colsep="1" valign="middle" align="left">
<ptxt>Differential lock does not lock</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Harness and connector</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000003ARM005X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Transfer shift actuator</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000003ARU005X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Transfer position switch</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000003ARS003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Differential lock switch</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000003ARO003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Differential lock shift actuator</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000003CC3003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>CAN communication system</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001RSO08CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Four wheel drive control ECU</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000030J200ZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Rear Differential</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000318L003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>