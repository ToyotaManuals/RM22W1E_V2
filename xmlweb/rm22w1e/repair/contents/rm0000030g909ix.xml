<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MU_T00FX" variety="T00FX">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV with DPF)</name>
<para id="RM0000030G909IX" category="S" type-id="3001E" name-id="AT023O-268" from="201308">
<name>DIAGNOSTIC TROUBLE CODE CHART</name>
<subpara id="RM0000030G909IX_z0" proc-id="RM22W0E___00008N600001">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a DTC is output during the DTC check, check the parts listed in the table below and proceed to the "See page" given.</ptxt>
</item>
<item>
<ptxt>*1: "Comes on" means the Malfunction Indicator Lamp (MIL) illuminates.</ptxt>
</item>
<item>
<ptxt>*2: "DTC stored" means the ECM stores the trouble code if the ECM detects the DTC detection condition.</ptxt>
</item>
<item>
<ptxt>*3: The ATF temperature warning light blinks or a message is displayed in the combination meter.</ptxt>
</item>
<item>
<ptxt>These DTCs may be output when the clutch, brake, gear components, etc., inside the automatic transmission are damaged.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Automatic Transmission System</title>
<tgroup cols="5" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="1.42in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="1.42in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.42in" colsep="0"/>
<colspec colnum="4" colname="4" colwidth="1.42in" colsep="0"/>
<colspec colnum="5" colname="5" colwidth="1.4in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>MIL*1</ptxt>
</entry>
<entry colname="4" colsep="1" align="center">
<ptxt>Memory*2</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0705</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Range Sensor Circuit Malfunction (PRNDL Input)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8412JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0711</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Fluid Temperature Sensor "A" Performance</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W7Z0OTX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0712</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Fluid Temperature Sensor "A" Circuit Low Input</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W850Q9X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0713</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Fluid Temperature Sensor "A" Circuit High Input</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W850Q9X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0717</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Turbine Speed Sensor Circuit No Signal</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W7Y0B5X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0722</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Output Speed Sensor Circuit No Signal</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM0000012TD08LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0729</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Gear 6 Incorrect Ratio</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8I06SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0748</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Pressure Control Solenoid "A" Electrical (Shift Solenoid Valve SL1)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8F0I1X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0751</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "A" Performance (Shift Solenoid Valve S1)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8J0DCX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0756</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "B" Performance (Shift Solenoid Valve S2)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W800CYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0761</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "C" Performance (Shift Solenoid Valve S3)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8106SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0766</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "D" Performance (Shift Solenoid Valve S4)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W820BIX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0776</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Pressure Control Solenoid "B" Performance (Shift Solenoid Valve SL2)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W820BIX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0778</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Pressure Control Solenoid "B" Electrical (Shift Solenoid Valve SL2)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8G0H4X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0781</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>1-2 Shift (1-2 Shift Valve)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000XP10B0X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0894</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Component Slipping</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W830LYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0973</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "A" Control Circuit Low (Shift Solenoid Valve S1)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W860E0X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0974</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "A" Control Circuit High (Shift Solenoid Valve S1)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W860E0X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0976</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "B" Control Circuit Low (Shift Solenoid Valve S2)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W870DRX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0977</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "B" Control Circuit High (Shift Solenoid Valve S2)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W870DRX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0979</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "C" Control Circuit Low (Shift Solenoid Valve S3)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8B06NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0980</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "C" Control Circuit High (Shift Solenoid Valve S3)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8B06NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0982</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "D" Control Circuit Low (Shift Solenoid Valve S4)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8C08DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0983</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "D" Control Circuit High (Shift Solenoid Valve S4)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8C08DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0985</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "E" Control Circuit Low (Shift Solenoid Valve SR)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8H0BWX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0986</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "E" Control Circuit High (Shift Solenoid Valve SR)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8H0BWX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2714</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Pressure Control Solenoid "D" Performance (Shift Solenoid Valve SLT)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W830LYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2716</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Pressure Control Solenoid "D" Electrical (Shift Solenoid Valve SLT)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8A0KZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2742</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Fluid Temperature Sensor "B" Circuit Low Input</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-*3</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM0000012XR08UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2743</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Fluid Temperature Sensor "B" Circuit High Input</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-*3</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM0000012XR08UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2757</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Torque Converter Clutch Pressure Control Solenoid Performance (Shift Solenoid Valve SLU)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8D0H7X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2759</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Torque Converter Clutch Pressure Control Solenoid Control Circuit Electrical (Shift Solenoid Valve SLU)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W890IKX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2772</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Four Wheel Drive (4WD) Low Switch Circuit Range / Performance</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000002IR003YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>