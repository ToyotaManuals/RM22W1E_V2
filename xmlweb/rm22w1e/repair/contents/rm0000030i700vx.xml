<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3ML_T00FO" variety="T00FO">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV)</name>
<para id="RM0000030I700VX" category="C" type-id="303VB" name-id="AT2KQ-09" from="201308">
<dtccode>U0100</dtccode>
<dtcname>Lost Communication with ECM / PCM "A"</dtcname>
<subpara id="RM0000030I700VX_01" type-id="60" category="03" proc-id="RM22W0E___000085V00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The engine control unit and the transmission control unit are located inside the ECM. The engine control unit intercommunicates with the transmission control unit with the Controller Area Network (CAN). If there is a problem in this intercommunication, the ECM stores a DTC.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U0100</ptxt>
</entry>
<entry valign="middle">
<ptxt>No communication with the ECM continues (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000030I700VX_05" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000030I700VX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000030I700VX_06_0001" proc-id="RM22W0E___000085W00001">
<testtitle>CHECK DTC OUTPUT (IN ADDITION TO DTC P0100)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the tester.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only U0100 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U0100 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any other codes besides U0100 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000030I700VX_06_0002" fin="false">A</down>
<right ref="RM0000030I700VX_06_0005" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000030I700VX_06_0002" proc-id="RM22W0E___000085X00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329203AX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000030I700VX_06_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000030I700VX_06_0003" proc-id="RM22W0E___000085Y00001">
<testtitle>CHECK IF DTC RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000000W770Y6X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the tester.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Other DTCs output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000030I700VX_06_0004" fin="true">A</down>
<right ref="RM0000030I700VX_06_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000030I700VX_06_0004">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM0000030I700VX_06_0005">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000030G909HX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000030I700VX_06_0006">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000030G909HX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>