<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM0000018X207PX" category="C" type-id="3037I" name-id="ESRB0-03" from="201301" to="201308">
<dtccode>P2226</dtccode>
<dtcname>Barometric Pressure Circuit</dtcname>
<dtccode>P2228</dtccode>
<dtcname>Barometric Pressure Circuit Low Input</dtcname>
<dtccode>P2229</dtccode>
<dtcname>Barometric Pressure Circuit High Input</dtcname>
<subpara id="RM0000018X207PX_01" type-id="60" category="03" proc-id="RM22W0E___00002Q000000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>To detect atmospheric pressure, the built-in atmospheric pressure sensor in the ECM is used for the common rail system. Following the changes in the atmospheric pressure, the ECM corrects the injection volume, timing and duration, and adjusts the common rail internal fuel pressure in order to optimize engine combustion.</ptxt>
<table pgwide="1">
<title>P2226</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>Atmospheric pressure sensor  voltage is 1.4 V or less, or 4.0 V or more for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2228</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>Atmospheric pressure sensor  voltage is 1.4 V or less for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2229</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry>
<ptxt>Atmospheric pressure sensor  voltage is 4.0 V or more for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>DTC No.</ptxt>
</entry>
<entry>
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P2226</ptxt>
</entry>
<entry morerows="2">
<list1 type="unordered">
<item>
<ptxt>Atmosphere Pressure</ptxt>
</item>
<item>
<ptxt>MAP</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry>
<ptxt>P2228</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>P2229</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the ignition switch is ON, the intake air pipe pressure is approximately equal to atmospheric pressure.</ptxt>
</item>
<item>
<ptxt>Under standard atmospheric pressure conditions, the output of the atmospheric pressure sensor is 101 kPa. For every 100 m increase in altitude, pressure drops by 1 kPa. Varies by weather (high atmospheric pressure, low atmospheric pressure).</ptxt>
</item>
<item>
<ptxt>If DTC P2226, P2228 and/or P2229 is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Misfire</ptxt>
</item>
<item>
<ptxt>Combustion noise</ptxt>
</item>
<item>
<ptxt>Black smoke</ptxt>
</item>
<item>
<ptxt>White smoke</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM0000018X207PX_02" type-id="51" category="05" proc-id="RM22W0E___00002Q100000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07YX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06NX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000018X207PX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000018X207PX_03_0003" proc-id="RM22W0E___00002Q300000">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P2226, P2228 OR P2229)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3. </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC. </ptxt>
</test1>
<test1>
<ptxt>Read the DTCs. </ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P2226, P2228 or P2229 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2226, P2228 or P2229 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P2226, P2228 and P2229 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000018X207PX_03_0001" fin="false">A</down>
<right ref="RM0000018X207PX_03_0002" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000018X207PX_03_0001" proc-id="RM22W0E___00002Q200000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000018X207PX_03_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018X207PX_03_0004" proc-id="RM22W0E___00002Q400000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off and leave the vehicle for 15 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the tester to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Powertrain / Engine / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P2226, P2228 and/or P2229.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or UNKNOWN, idle the engine.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000018X207PX_03_0005" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018X207PX_03_0002">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW05BX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000018X207PX_03_0005">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>