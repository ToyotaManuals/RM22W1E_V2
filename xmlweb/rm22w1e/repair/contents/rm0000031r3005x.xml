<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S0012" variety="S0012">
<name>1VD-FTV LUBRICATION</name>
<ttl id="12012_S0012_7C3KX_T00E0" variety="T00E0">
<name>OIL AND OIL FILTER</name>
<para id="RM0000031R3005X" category="A" type-id="30019" name-id="LU3Q4-01" from="201301">
<name>REPLACEMENT</name>
<subpara id="RM0000031R3005X_01" type-id="11" category="10" proc-id="RM22W0E___000071900000">
<content3 releasenbr="1">
<atten2>
<list1 type="unordered">
<item>
<ptxt>Prolonged and repeated contact with engine oil will result in the removal of natural oils from the skin, leading to dryness, irritation and dermatitis. In addition, used engine oil contains potentially harmful contaminants which may cause skin cancer.</ptxt>
</item>
<item>
<ptxt>Precautions should be taken when replacing engine oil to minimize the risk of your skin making contact with used engine oil. Protective clothing and gloves that cannot be penetrated by oil should be worn. The skin should be washed with soap and water, or use waterless hand cleaner, to remove any used engine oil thoroughly. Do not use gasoline, thinners, or solvents.</ptxt>
</item>
<item>
<ptxt>In order to protect the environment, used oil and used oil filters must be disposed of at designated disposal sites.</ptxt>
</item>
</list1>
</atten2>
</content3>
</subpara>
<subpara id="RM0000031R3005X_02" type-id="01" category="01">
<s-1 id="RM0000031R3005X_02_0001" proc-id="RM22W0E___00005K600000">
<ptxt>DRAIN ENGINE OIL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and No. 2 engine under cover seal from the No. 2 engine under cover.</ptxt>
<figure>
<graphic graphicname="A177436" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the oil filler cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the oil pan drain plug and gasket, and then drain the engine oil into a container.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket and the oil pan drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>38</t-value1>
<t-value2>387</t-value2>
<t-value4>28</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031R3005X_02_0015" proc-id="RM22W0E___000011Y00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031R3005X_02_0016" proc-id="RM22W0E___000071A00000">
<ptxt>REMOVE ENGINE UNDER COVER SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and engine under cover sub-assembly RH.</ptxt>
<figure>
<graphic graphicname="A215101" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<figure>
<graphic graphicname="A208795E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>If the No. 1 cooler packing is not divided, remove both sides of the engine under cover assembly and cut the packing at the perforation indicated in the illustration.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031R3005X_02_0002" proc-id="RM22W0E___00005G700000">
<ptxt>REMOVE OIL FILTER ELEMENT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the oil filter drain plug.</ptxt>
<figure>
<graphic graphicname="A174852" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect a hose with an inside diameter of 15 mm (0.591 in.) to the pipe.</ptxt>
<figure>
<graphic graphicname="A094910E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the oil filter drain plug and install it to the drain pipe.</ptxt>
</s2>
<s2>
<ptxt>Install the oil filter drain pipe to the oil filter cap and drain the engine oil.</ptxt>
<figure>
<graphic graphicname="A174853E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the oil filter drain pipe.</ptxt>
</s2>
<s2>
<ptxt>Apply a light coat of engine oil to a new O-ring, and install it to the oil filter drain plug.</ptxt>
</s2>
<s2>
<ptxt>Install the oil filter drain plug to the oil filter cap.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A174852" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, remove the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A174854E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09228-06501</s-number>
</sstitem>
</sst>
<atten4>
<ptxt>Use a container to catch the draining oil. After the oil filter cap is loosened approximately 4 turns and the cap ribs are vertical, engine oil drains from the gap between the oil filter cap and oil pan.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the oil filter element and O-ring from the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A106167E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be sure to remove the cap O-ring by hand, without using any tools, to prevent damage to the cap O-ring groove.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031R3005X_02_0003" proc-id="RM22W0E___00005IL00000">
<ptxt>INSTALL OIL FILTER ELEMENT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the inside of the oil filter cap, its threads and its O-ring groove.</ptxt>
<figure>
<graphic graphicname="A106167E08" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Apply a small amount of engine oil to a new O-ring and install it to the oil filter cap.</ptxt>
</s2>
<s2>
<ptxt>Set a new oil filter element in the oil filter cap.</ptxt>
</s2>
<s2>
<ptxt>Remove any dirt or foreign matter from the installation surface of the engine.</ptxt>
</s2>
<s2>
<ptxt>Apply a small amount of engine oil to the O-ring again and temporarily install the oil filter cap by hand.</ptxt>
</s2>
<s2>
<ptxt>Using SST, tighten the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A174854E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09228-06501</s-number>
</sstitem>
</sst>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the oil filter is installed securely as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>Be careful that the O-ring does not get caught between any surrounding parts.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the No. 2 engine under cover seal to the No. 2 engine under cover with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A177436" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031R3005X_02_0004" proc-id="RM22W0E___000054B00000">
<ptxt>ADD ENGINE OIL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add fresh oil.</ptxt>
<spec>
<title>Standard Engine Oil</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Oil Grade</ptxt>
</entry>
<entry align="center">
<ptxt>Oil Viscosity (SAE)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/ DPF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ACEA C2</ptxt>
<ptxt>(Using engine oil other than ACEA C2 may damage catalytic converter)</ptxt>
</entry>
<entry>
<ptxt>- 0W-30</ptxt>
<ptxt>- 5W-30</ptxt>
<ptxt>(0W-30 is best choice for fuel economy and good starting in cold weather)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o DPF</ptxt>
</entry>
<entry valign="middle">
<ptxt>G-DLD-1, API CF-4, API CF or ACEA B1</ptxt>
<ptxt>(You may also use API CE or CD)</ptxt>
</entry>
<entry>
<ptxt>- 5W-30</ptxt>
<ptxt>- 10W-30</ptxt>
<ptxt>- 15W-40</ptxt>
<ptxt>- 20W-50</ptxt>
<ptxt>(5W-30 is best choice for fuel economy and good starting in cold weather).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Capacity</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Drain and refill with oil filter change</ptxt>
</entry>
<entry align="center">
<ptxt>9.2 liters (9.7 US qts, 8.1 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Drain and refill without oil filter change</ptxt>
</entry>
<entry align="center">
<ptxt>8.2 liters (8.7 US qts, 7.2 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Dry fill</ptxt>
</entry>
<entry align="center">
<ptxt>9.9 liters (10.5 US qts, 8.7 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Install the oil filler cap.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031R3005X_02_0005" proc-id="RM22W0E___000068W00000">
<ptxt>INSPECT FOR ENGINE OIL LEAK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Warm up the engine and check for an engine oil leak.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031R3005X_02_0007" proc-id="RM22W0E___000054J00000">
<ptxt>CHECK ENGINE OIL LEVEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Warm up the engine, stop the engine and wait 5 minutes. The engine oil level should be between the dipstick low level mark and full level mark.</ptxt>
<ptxt>If low, check for leakage and add oil up to the full level mark.</ptxt>
<atten3>
<ptxt>Do not fill engine oil above the full level mark.</ptxt>
</atten3>
<atten4>
<ptxt>A certain amount of engine oil will be consumed while driving. In the following situations, oil consumption may increase, and engine oil may need to be refilled in between oil maintenance intervals.</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the engine is new, for example directly after purchasing the vehicle or after replacing the engine.</ptxt>
</item>
<item>
<ptxt>If low quality oil or oil of an inappropriate viscosity is used.</ptxt>
</item>
<item>
<ptxt>When driving at high engine speed or with a heavy load, (when towing, or), when driving while accelerating or decelerating frequently.</ptxt>
</item>
<item>
<ptxt>When leaving the idling for a long time, or when driving frequently through heavy traffic.</ptxt>
</item>
</list1>
<ptxt>When judging the amount of oil consumption, keep in mind that the oil may have become diluted, making it difficult to judge the true level accurately.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000031R3005X_02_0017" proc-id="RM22W0E___000071B00000">
<ptxt>INSTALL ENGINE UNDER COVER SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the engine under cover sub-assembly RH with the 5 bolts.</ptxt>
<figure>
<graphic graphicname="A215101" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<figure>
<graphic graphicname="A209602E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Make sure there is no gap between the ends of the No. 1 cooler packing.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031R3005X_02_0018" proc-id="RM22W0E___000011Q00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly RH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and 2 screws.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>