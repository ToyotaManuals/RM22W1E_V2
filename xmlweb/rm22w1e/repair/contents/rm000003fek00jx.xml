<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LY_T00F1" variety="T00F1">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM000003FEK00JX" category="C" type-id="305G9" name-id="CC2MY-04" from="201308">
<dtccode>U0100</dtccode>
<dtcname>Communication Stop from ECM to Distance Control ECU, VSC</dtcname>
<dtccode>U0122</dtccode>
<dtcname>Lost Communication with Vehicle Dynamics Control Module</dtcname>
<dtccode>U0123</dtccode>
<dtcname>Lost Communication with Yaw Rate Sensor Module</dtcname>
<dtccode>U0126</dtccode>
<dtcname>Lost Communication with Steering Angle Sensor Module</dtcname>
<dtccode>U1101</dtccode>
<dtcname>Lost Communication with Distance Control ECU</dtcname>
<subpara id="RM000003FEK00JX_01" type-id="60" category="03" proc-id="RM22W0E___00007C300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is output when a communication malfunction occurs between the sensors and ECUs.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U0100</ptxt>
</entry>
<entry valign="middle">
<ptxt>The vehicle speed is 50 km/h (30 mph) or more with the cruise control main switch on (vehicle-to-vehicle distance control mode) and communication from the ECM to the distance control ECU stops.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Distance control ECU</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0122</ptxt>
</entry>
<entry valign="middle">
<ptxt>The vehicle speed is 50 km/h (30 mph) or more with the cruise control main switch on (vehicle-to-vehicle distance control mode) and communication from the master cylinder solenoid (skid control ECU) to the ECM stops.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Master cylinder solenoid (skid control ECU)</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0123</ptxt>
</entry>
<entry valign="middle">
<ptxt>While cruise control is in operation (vehicle-to-vehicle distance control mode), the data sent from the yaw rate sensor to the distance control ECU is abnormal.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Distance control ECU</ptxt>
</item>
<item>
<ptxt>Yaw rate sensor</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0126</ptxt>
</entry>
<entry valign="middle">
<ptxt>While cruise control is in operation (vehicle-to-vehicle distance control mode), the data sent from the steering angle sensor to the distance control ECU is abnormal.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Distance control ECU</ptxt>
</item>
<item>
<ptxt>Steering angle sensor</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U1101</ptxt>
</entry>
<entry valign="middle">
<ptxt>The vehicle speed is 50 km/h (30 mph) or more with the cruise control main switch on (vehicle-to-vehicle distance control mode) and communication from the distance control ECU to the ECM stops.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Distance control ECU</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003FEK00JX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003FEK00JX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003FEK00JX_03_0001" proc-id="RM22W0E___00007C400001">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally (See page <xref label="Seep01" href="RM000001RSO09SX"/> for LHD, <xref label="Seep02" href="RM000001RSO09TX"/> for RHD).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(CAN DTC is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003FEK00JX_03_0002" fin="true">A</down>
<right ref="RM000003FEK00JX_03_0003" fin="true">B</right>
<right ref="RM000003FEK00JX_03_0004" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003FEK00JX_03_0002">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FEK00JX_03_0003">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSW03ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FEK00JX_03_0004">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSW040X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>