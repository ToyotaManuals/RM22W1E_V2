<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000000PDV0R1X" category="C" type-id="302GR" name-id="ES17TJ-001" from="201308">
<dtccode>P0016</dtccode>
<dtcname>Crankshaft Position - Camshaft Position Correlation (Bank 1 Sensor A)</dtcname>
<dtccode>P0018</dtccode>
<dtcname>Crankshaft Position - Camshaft Position Correlation (Bank 2 Sensor A)</dtcname>
<subpara id="RM000000PDV0R1X_01" type-id="60" category="03" proc-id="RM22W0E___00000HO00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>In the VVT (Variable Valve Timing) system, the appropriate intake valve open and close timing is controlled by the ECM. The ECM performs intake valve control by performing the following: 1) controlling the camshaft and camshaft timing oil control valve, and operating the camshaft timing gear; and 2) changing the relative positions of the camshaft and crankshaft.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0016</ptxt>
</entry>
<entry valign="middle">
<ptxt>Deviations in the crankshaft position sensor and VVT sensor 1 (for intake camshaft) signals (2 trip detection logic).</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Valve timing</ptxt>
</item>
<item>
<ptxt>Camshaft timing oil control valve assembly (for intake side)</ptxt>
</item>
<item>
<ptxt>Oil control valve filter</ptxt>
</item>
<item>
<ptxt>Camshaft timing gear assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0018</ptxt>
</entry>
<entry valign="middle">
<ptxt>Deviations in the crankshaft position sensor and VVT sensor 2 (for intake camshaft) signals (2 trip detection logic).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000PDV0R1X_02" type-id="64" category="03" proc-id="RM22W0E___00000HP00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>To monitor the correlation of the intake camshaft position and crankshaft position, the ECM checks the VVT learned value while the engine is idling. The VVT learned value is calibrated based on the camshaft position and crankshaft position. The intake valve timing is set to the most retarded angle while the engine is idling. If the VVT learned value is out of the specified range in consecutive driving cycles, the ECM illuminates the MIL and stores DTC P0016 (Bank 1) or P0018 (Bank 2).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PDV0R1X_06" type-id="32" category="03" proc-id="RM22W0E___00000HQ00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0335 (See page <xref label="Seep01" href="RM000000TCW0V9X_07"/>).</ptxt>
<ptxt>Refer to DTC P0340 (See page <xref label="Seep02" href="RM000000WBZ0MSX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PDV0R1X_07" type-id="51" category="05" proc-id="RM22W0E___00000HR00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PDV0R1X_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PDV0R1X_09_0007" proc-id="RM22W0E___00000HS00001">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0016 OR P0018)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.25in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0016 or P0018 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0016 or P0018 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0016 or P0018 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDV0R1X_09_0008" fin="false">A</down>
<right ref="RM000000PDV0R1X_09_0015" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0008" proc-id="RM22W0E___00000HT00001">
<testtitle>PERFORM ACTIVE TEST USING GTS (OPERATE CAMSHAFT TIMING OIL CONTROL VALVE FOR INTAKE SIDE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Warm up the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the A/C on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the VVT Linear (Bank 1) or Control the VVT Linear (Bank 2).</ptxt>
</test1>
<test1>
<ptxt>Perform the Active Test. Check that the displacement angle varies.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Displacement angle varies.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDV0R1X_09_0009" fin="false">OK</down>
<right ref="RM000000PDV0R1X_09_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0009" proc-id="RM22W0E___00000HU00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0016 OR P0018)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK185X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the GTS (See page <xref label="Seep02" href="RM000000PDL0SBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for more than 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the GTS.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTC output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDV0R1X_09_0016" fin="true">OK</down>
<right ref="RM000000PDV0R1X_09_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0010" proc-id="RM22W0E___00000HV00001">
<testtitle>CHECK VALVE TIMING (CHECK FOR LOOSE TEETH AND WHETHER TIMING CHAIN HAS JUMPED TOOTH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the cylinder head cover RH and LH.</ptxt>
<figure>
<graphic graphicname="A103826E11" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Timing Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Turn the crankshaft pulley and align its groove with the "0" timing mark on the timing chain cover.</ptxt>
</test1>
<test1>
<ptxt>Check that the timing marks on the camshaft timing gears are aligned with the timing marks of the bearing cap as shown in the illustration.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>If not, turn the crankshaft 1 revolution (360°), and then align the marks as above.</ptxt>
</item>
</list1>
<spec>
<title>OK</title>
<specitem>
<ptxt>Timing marks on camshaft timing gears are aligned as shown in the illustration.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDV0R1X_09_0011" fin="false">OK</down>
<right ref="RM000000PDV0R1X_09_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0011" proc-id="RM22W0E___00000HW00001">
<testtitle>INSPECT CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY (FOR INTAKE SIDE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the camshaft timing oil control valve assembly (See page <xref label="Seep01" href="RM000000Q74046X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDV0R1X_09_0012" fin="false">OK</down>
<right ref="RM000000PDV0R1X_09_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0012" proc-id="RM22W0E___00000HX00001">
<testtitle>CHECK OIL PIPE AND OIL CONTROL VALVE FILTER</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A225778E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Remove the No. 1 or No. 2 oil pipe.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>No. 1 Oil Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>No. 2 Oil Pipe</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Remove the oil control valve filter.</ptxt>
</test1>
<test1>
<ptxt>Check that the filter and pipe are not clogged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The filter and pipe are not clogged.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDV0R1X_09_0013" fin="false">OK</down>
<right ref="RM000000PDV0R1X_09_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0013" proc-id="RM22W0E___00000HY00001">
<testtitle>REPLACE CAMSHAFT TIMING GEAR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the camshaft timing gear assembly (See page <xref label="Seep01" href="RM000003B6X01BX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDV0R1X_09_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0014" proc-id="RM22W0E___00000HZ00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0016 OR P0018)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK185X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the GTS (See page <xref label="Seep02" href="RM000000PDL0SBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for more than 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Confirm that no DTC is output using the GTS.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTC output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>DTC P0016 or P0018 is stored when foreign objects in the engine oil are caught in some parts of the system. These codes will stay stored even if the system returns to normal after a short time. These foreign objects are then captured by the oil filter, thus eliminating the source of the problem.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDV0R1X_09_0016" fin="true">OK</down>
<right ref="RM000000PDV0R1X_09_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0015">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM000002ZSO010X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0016">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0017">
<testtitle>ADJUST VALVE TIMING<xref label="Seep01" href="RM000002BK6043X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0018">
<testtitle>REPLACE CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY<xref label="Seep01" href="RM000000PWP05JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0019">
<testtitle>REPLACE OIL PIPE AND OIL CONTROL VALVE FILTER<xref label="Seep01" href="RM000002BK5043X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDV0R1X_09_0020">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292037X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>