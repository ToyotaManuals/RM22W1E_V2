<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12057_S002A" variety="S002A">
<name>PRE-CRASH SAFETY</name>
<ttl id="12057_S002A_7C3UG_T00NJ" variety="T00NJ">
<name>PRE-CRASH SAFETY SYSTEM</name>
<para id="RM000003B1000EX" category="C" type-id="802E1" name-id="PC0TO-03" from="201301">
<dtccode>B2071</dtccode>
<dtcname>Stop Light Control Relay Malfunction</dtcname>
<subpara id="RM000003B1000EX_01" type-id="60" category="03" proc-id="RM22W0E___0000FPJ00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when a malfunction in the stop light operation relay circuit of the vehicle stability control system is detected. This circuit consists of the master cylinder solenoid (skid control ECU) and seat belt control ECU. Signals are sent from the master cylinder solenoid (skid control ECU) via the CAN communication line.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2071</ptxt>
</entry>
<entry valign="middle">
<ptxt>Malfunction in the brake system is detected.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>Vehicle stability control system</ptxt>
</item>
<item>
<ptxt>Seat belt control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003B1000EX_04" type-id="32" category="03" proc-id="RM22W0E___0000FPN00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E149011E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003B1000EX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003B1000EX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003B1000EX_03_0001" proc-id="RM22W0E___0000FPK00000">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally (See page <xref label="Seep01" href="RM000001RSO08CX"/> for LHD, <xref label="Seep02" href="RM000001RSO08DX"/> for RHD).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(DTC U0122 is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>DTC U0122: Lost Communication with Skid Control ECU </ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003B1000EX_03_0002" fin="false">A</down>
<right ref="RM000003B1000EX_03_0005" fin="true">B</right>
<right ref="RM000003B1000EX_03_0008" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003B1000EX_03_0002" proc-id="RM22W0E___0000FPL00000">
<testtitle>CHECK DTC (VEHICLE STABILITY CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Chassis / ABS/VSC/TRAC / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000VVL03NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000VVL03NX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC C1380 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>DTC C1380: Stop Light Control Relay Malfunction</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003B1000EX_03_0003" fin="false">OK</down>
<right ref="RM000003B1000EX_03_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003B1000EX_03_0003" proc-id="RM22W0E___0000FPM00000">
<testtitle>CHECK DTC (PRE-CRASH SAFETY SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Body / Pre-Crash / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000VVL03NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000VVL03NX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(DTC B2071 is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003B1000EX_03_0004" fin="true">A</down>
<right ref="RM000003B1000EX_03_0007" fin="true">B</right>
<right ref="RM000003B1000EX_03_0009" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003B1000EX_03_0004">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003B1000EX_03_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSW03JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003B1000EX_03_0008">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSW03KX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003B1000EX_03_0006">
<testtitle>GO TO VEHICLE STABILITY CONTROL SYSTEM (DTC C1380)<xref label="Seep01" href="RM000001N3L07LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003B1000EX_03_0007">
<testtitle>REPLACE SEAT BELT CONTROL ECU<xref label="Seep01" href="RM0000039P900MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003B1000EX_03_0009">
<testtitle>REPLACE SEAT BELT CONTROL ECU<xref label="Seep01" href="RM000003A6M004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>