<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000000XI90T8X" category="C" type-id="803L0" name-id="BCEHD-01" from="201301" to="201308">
<dtccode>C1405</dtccode>
<dtcname>Open or Short in Front Speed Sensor RH Circuit</dtcname>
<dtccode>C1406</dtccode>
<dtcname>Open or Short in Front Speed Sensor LH Circuit</dtcname>
<subpara id="RM000000XI90T8X_01" type-id="60" category="03" proc-id="RM22W0E___0000AEH00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1401 and C1402 (See page <xref label="Seep01" href="RM000000XI90T7X_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.93in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1405</ptxt>
<ptxt>C1406</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>An open in the speed sensor signal circuit continues for 0.5 seconds or more.</ptxt>
</item>
<item>
<ptxt>With the IG1 terminal voltage at 9.5 V or higher, the sensor power supply voltage decreases for 0.5 seconds or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Front speed sensor RH/LH</ptxt>
</item>
<item>
<ptxt>Skid control sensor wire</ptxt>
</item>
<item>
<ptxt>Speed sensor circuit</ptxt>
</item>
<item>
<ptxt>Skid control ECU (Master cylinder solenoid)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTC C1405 is for the front speed sensor RH.</ptxt>
</item>
<item>
<ptxt>DTC C1406 is for the front speed sensor LH.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XI90T8X_02" type-id="32" category="03" proc-id="RM22W0E___0000AEI00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C217214E32" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XI90T8X_03" type-id="51" category="05" proc-id="RM22W0E___0000AEJ00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</item>
<item>
<ptxt>Check the speed sensor signal after replacement (See page <xref label="Seep02" href="RM00000452K00NX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XI90T8X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XI90T8X_04_0001" proc-id="RM22W0E___0000AEK00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MOMENTARY INTERRUPTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the GTS, check for any momentary interruption in the wire harness and connector corresponding to the DTC (See page <xref label="Seep01" href="RM000001DX1022X"/>).</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.17in"/>
<colspec colname="COL2" colwidth="3.09in"/>
<colspec colname="COL3" colwidth="1.43in"/>
<colspec colname="COLSPEC1" colwidth="1.39in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Normal (there are no momentary interruptions).</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Perform the above inspection before removing the sensor and connector.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000XI90T8X_04_0031" fin="false">OK</down>
<right ref="RM000000XI90T8X_04_0032" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0031" proc-id="RM22W0E___0000AE300000">
<testtitle>READ VALUE USING GTS (FR/FL WHEEL SPEED)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.16in"/>
<colspec colname="COL2" colwidth="2.22in"/>
<colspec colname="COL3" colwidth="1.74in"/>
<colspec colname="COLSPEC2" colwidth="1.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that there is no difference between the speed value output from the speed sensor displayed on the GTS and the speed value displayed on the speedometer when driving the vehicle.</ptxt>
<atten4>
<ptxt>Factors that affect the indicated vehicle speed include tire size, tire inflation and tire wear. The speed indicated on the speedometer has an allowable margin of error. This can be tested using a speedometer tester (calibrated chassis dynamometer). For details about testing and the margin of error, refer to the reference chart (See page <xref label="Seep01" href="RM000002Z4Q02RX"/>).</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The speed value output from the speed sensor displayed on the GTS is the same as the actual vehicle speed measured using a speedometer tester (calibrated chassis dynamometer).</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000XI90T8X_04_0004" fin="false">OK</down>
<right ref="RM000000XI90T8X_04_0032" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0004" proc-id="RM22W0E___0000AEL00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00MX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 40 km/h (25 mph) or more for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00MX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XI90T8X_04_0035" fin="true">A</down>
<right ref="RM000000XI90T8X_04_0033" fin="true">B</right>
<right ref="RM000000XI90T8X_04_0037" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0032" proc-id="RM22W0E___0000AEO00000">
<testtitle>INSPECT SKID CONTROL SENSOR WIRE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the skid control sensor wire (See page <xref label="Seep01" href="RM000001B2J01KX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C173118E04" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<title>for RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.76in"/>
<colspec colname="COL2" colwidth="1.00in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Az2-1 - z17-1 (FR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Az2-5 - z17-2 (FR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z17-1 (FR-) - z17-2 (FR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.76in"/>
<colspec colname="COL2" colwidth="1.00in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Az1-1 - z18-1 (FL-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Az1-5 - z18-2 (FL+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z18-1 (FL-) - z18-2 (FL+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Skid Control Sensor Wire</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XI90T8X_04_0019" fin="false">OK</down>
<right ref="RM000000XI90T8X_04_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0019" proc-id="RM22W0E___0000AEN00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU - FRONT SPEED SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Install the skid control sensor wire (See page <xref label="Seep01" href="RM000001B2H01PX"/>).</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the z17 and/or z18 speed sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.24in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="2.07in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-3 (FR+) - z17-2 (FR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-3 (FR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-17 (FR-) - z17-1 (FR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-17 (FR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.24in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="2.07in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-18 (FL+) - z18-2 (FL+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-18 (FL+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-4 (FL-) - z18-1 (FL-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-4 (FL-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XI90T8X_04_0008" fin="false">OK</down>
<right ref="RM000000XI90T8X_04_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0008" proc-id="RM22W0E___0000AEM00000">
<testtitle>CHECK HARNESS AND CONNECTOR (FR+/FL+ TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the speed sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C207217E27" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<title>for RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z17-2 (FR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z18-2 (FL+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Front Speed Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XI90T8X_04_0034" fin="true">A</down>
<right ref="RM000000XI90T8X_04_0033" fin="true">B</right>
<right ref="RM000000XI90T8X_04_0037" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0033">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0018">
<testtitle>REPLACE SKID CONTROL SENSOR WIRE<xref label="Seep01" href="RM000001B2J01KX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0017">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0034">
<testtitle>REPLACE FRONT SPEED SENSOR<xref label="Seep01" href="RM000001B2J01KX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0035">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI90T8X_04_0037">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>