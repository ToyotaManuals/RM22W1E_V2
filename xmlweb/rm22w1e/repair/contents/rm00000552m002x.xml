<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SC_T00LF" variety="T00LF">
<name>CAN COMMUNICATION SYSTEM (for LHD)</name>
<para id="RM00000552M002X" category="J" type-id="802V3" name-id="NW3XG-02" from="201301" to="201308">
<dtccode/>
<dtcname>AFS ECU Communication Stop Mode</dtcname>
<subpara id="RM00000552M002X_01" type-id="60" category="03" proc-id="RM22W0E___0000DIW00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>AFS ECU Communication Stop Mode</ptxt>
</entry>
<entry>
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>"Headlight swivel (AFS)" is not displayed on the "Bus Check" screen.</ptxt>
</item>
<item>
<ptxt>"AFS ECU Communication Stop Mode" in "DTC Communication Table" applies.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Power source or inside of headlight swivel ECU assembly</ptxt>
</item>
<item>
<ptxt>Headlight swivel ECU assembly CAN branch wire or connector</ptxt>
</item>
<item>
<ptxt>Headlight swivel ECU assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>For vehicles with a dynamic headlight auto leveling.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM00000552M002X_02" type-id="32" category="03" proc-id="RM22W0E___0000DIX00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E207358E06" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000552M002X_03" type-id="51" category="05" proc-id="RM22W0E___0000DIY00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Operating the ignition switch, any switches or any doors triggers related ECU and sensor communication with the CAN, which causes resistance variation.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000552M002X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000552M002X_04_0001" proc-id="RM22W0E___0000DIZ00000">
<testtitle>PRECAUTION</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content6>
<res>
<down ref="RM00000552M002X_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000552M002X_04_0002" proc-id="RM22W0E___0000DJ000000">
<testtitle>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal before measuring the resistances of the CAN main wire and the CAN branch wire.</ptxt>
<atten2>
<ptxt>For vehicles with an SRS system:</ptxt>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM00000552M002X_04_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000552M002X_04_0003" proc-id="RM22W0E___0000DJ100000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (HEADLIGHT SWIVEL ECU ASSEMBLY CAN BRANCH WIRE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E101 headlight swivel ECU assembly connector.</ptxt>
<figure>
<graphic graphicname="C240252E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E101-12 (MPX1) - E101-13 (MPX2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>54 to 69 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Headlight Swivel ECU Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000552M002X_04_0004" fin="false">OK</down>
<right ref="RM00000552M002X_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000552M002X_04_0004" proc-id="RM22W0E___0000DJ200000">
<testtitle>CHECK HARNESS AND CONNECTOR (HEADLIGHT SWIVEL ECU ASSEMBLY - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
<figure>
<graphic graphicname="C240252E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E101-22 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E101-15 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Headlight Swivel ECU Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM00000552M002X_04_0006" fin="true">OK</down>
<right ref="RM00000552M002X_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000552M002X_04_0005">
<testtitle>REPAIR OR REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY CAN BRANCH WIRE OR CONNECTOR (MPX1, MPX2)</testtitle>
</testgrp>
<testgrp id="RM00000552M002X_04_0006">
<testtitle>REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY<xref label="Seep01" href="RM000003A5S005X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000552M002X_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>