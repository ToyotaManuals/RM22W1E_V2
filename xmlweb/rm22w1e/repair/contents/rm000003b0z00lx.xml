<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12057_S002A" variety="S002A">
<name>PRE-CRASH SAFETY</name>
<ttl id="12057_S002A_7C3UG_T00NJ" variety="T00NJ">
<name>PRE-CRASH SAFETY SYSTEM</name>
<para id="RM000003B0Z00LX" category="C" type-id="80070" name-id="PC119-03" from="201301">
<dtccode>B2070</dtccode>
<dtcname>VSC Buzzer</dtcname>
<subpara id="RM000003B0Z00LX_01" type-id="60" category="03" proc-id="RM22W0E___0000FOY00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the master cylinder solenoid (skid control ECU) detects a malfunction in the VSC warning buzzer circuit, a malfunction signal is sent to the seat belt control ECU, and this DTC is stored.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B2070</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>While the ignition switch is ON, the skid control ECU buzzer circuit malfunction signal is received by the seat belt control ECU.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Vehicle stability control system</ptxt>
</item>
<item>
<ptxt>Seat belt control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003B0Z00LX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003B0Z00LX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003B0Z00LX_03_0001" proc-id="RM22W0E___0000FOZ00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (BUZZER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Chassis / ABS/VSC/TRAC / Active Test.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRAC</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Buzzer</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Skid control buzzer sounds</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF or ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Skid control buzzer sounds.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003B0Z00LX_03_0002" fin="true">A</down>
<right ref="RM000003B0Z00LX_03_0004" fin="true">B</right>
<right ref="RM000003B0Z00LX_03_0003" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003B0Z00LX_03_0002">
<testtitle>REPLACE SEAT BELT CONTROL ECU<xref label="Seep01" href="RM0000039P900MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003B0Z00LX_03_0004">
<testtitle>REPLACE SEAT BELT CONTROL ECU<xref label="Seep01" href="RM000003A6M004X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003B0Z00LX_03_0003">
<testtitle>GO TO VEHICLE STABILITY CONTROL SYSTEM (Skid Control Buzzer Circuit)<xref label="Seep01" href="RM000001DXP02DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>