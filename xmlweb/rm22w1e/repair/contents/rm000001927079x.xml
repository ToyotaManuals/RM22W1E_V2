<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3X0_T00Q3" variety="T00Q3">
<name>BACK WINDOW GLASS</name>
<para id="RM000001927079X" category="A" type-id="80001" name-id="WS74C-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM000001927079X_02" type-id="01" category="01">
<s-1 id="RM000001927079X_02_0149" proc-id="RM22W0E___0000HV100001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the   battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal   notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001927079X_02_0009" proc-id="RM22W0E___0000HUS00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001927079X_02_0134" proc-id="RM22W0E___0000CGP00001">
<ptxt>REMOVE CENTER BACK DOOR GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 5 clips and 4 claws, and remove the center back door garnish.</ptxt>
<figure>
<graphic graphicname="B313300" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0135" proc-id="RM22W0E___0000CGQ00001">
<ptxt>REMOVE BACK DOOR SIDE GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 clips and 2 claws, and remove the back door side garnish LH.</ptxt>
<figure>
<graphic graphicname="B313301" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0136" proc-id="RM22W0E___0000CGS00001">
<ptxt>REMOVE BACK DOOR SIDE GARNISH RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Power Back Door:</ptxt>
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>w/ Power Back Door:</ptxt>
<figure>
<graphic graphicname="B313302" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Detach the clip and 4 claws, and remove the back door side garnish RH.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0144" proc-id="RM22W0E___0000CGU00001">
<ptxt>REMOVE ASSIST GRIP (for Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and assist grip.</ptxt>
<figure>
<graphic graphicname="B188374" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0145" proc-id="RM22W0E___0000CGV00001">
<ptxt>REMOVE NO. 2 BACK DOOR SERVICE HOLE COVER (for Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the No. 2 back door service hole cover. </ptxt>
<figure>
<graphic graphicname="B186428" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0146" proc-id="RM22W0E___0000IHI00001">
<ptxt>REMOVE POWER BACK DOOR MAIN SWITCH (for Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the power back door main switch.</ptxt>
<figure>
<graphic graphicname="B309333" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0137" proc-id="RM22W0E___0000CUB00001">
<ptxt>REMOVE BACK DOOR GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 14 clips and remove the back door garnish.</ptxt>
<figure>
<graphic graphicname="B181329" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0147" proc-id="RM22W0E___0000HUZ00001">
<ptxt>REMOVE BACK DOOR GLASS CHANNEL LH (w/ Rear Spoiler)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B189693E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a clip remover, remove the clip.</ptxt>
</s2>
<s2>
<ptxt>Remove the back door glass channel LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001927079X_02_0148" proc-id="RM22W0E___0000HV000001">
<ptxt>REMOVE BACK DOOR GLASS CHANNEL RH (w/ Rear Spoiler)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B189693E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a clip remover, remove the clip.</ptxt>
</s2>
<s2>
<ptxt>Remove the back door glass channel RH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001927079X_02_0138" proc-id="RM22W0E___0000HUT00001">
<ptxt>REMOVE REAR SPOILER SUB-ASSEMBLY (w/ Rear Spoiler)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Power Back Door:</ptxt>
<figure>
<graphic graphicname="B193121" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the 2 hole plugs and 4 bolts.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/o Power Back Door:</ptxt>
<figure>
<graphic graphicname="B180983" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 clips and remove the rear spoiler sub-assembly.</ptxt>
<figure>
<graphic graphicname="B180984" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0139" proc-id="RM22W0E___0000HUU00001">
<ptxt>REMOVE REAR WIPER ARM (w/ Rear Wiper)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Open the cover.</ptxt>
<figure>
<graphic graphicname="B181332" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut and rear wiper arm.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0140" proc-id="RM22W0E___0000HUV00001">
<ptxt>REMOVE REAR WIPER MOTOR GROMMET (w/ Rear Wiper)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the rear wiper motor grommet.</ptxt>
<figure>
<graphic graphicname="E156014" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0141" proc-id="RM22W0E___0000HUW00000">
<ptxt>REMOVE REAR WIPER MOTOR ASSEMBLY (w/ Rear Wiper)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B181333" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and rear wiper motor assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001927079X_02_0142" proc-id="RM22W0E___0000HUX00001">
<ptxt>REMOVE BACK WINDOW MOULDING OUTSIDE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B187533E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a knife, cut off the moulding as shown in the illustration.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Pull the shaded area shown in the illustration by the hand to remove the back window moulding outside.</ptxt>
<atten4>
<ptxt>Make a partial cut in the moulding. Then pull and remove it by hand.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000001927079X_02_0143" proc-id="RM22W0E___0000HUY00001">
<ptxt>REMOVE BACK WINDOW MOULDING OUTSIDE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B187532E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a knife, cut off the moulding as shown in the illustration.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Pull the shaded area shown in the illustration by the hand to remove the back window moulding outside.</ptxt>
<atten4>
<ptxt>Make a partial cut in the moulding. Then pull and remove it by hand.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000001927079X_02_0006" proc-id="RM22W0E___0000HUQ00001">
<ptxt>REMOVE BACK WINDOW GLASS</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B180597E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Apply protective tape to the outer surface of the vehicle body to prevent scratches.</ptxt>
<atten3>
<ptxt>When separating the back window glass from the vehicle, be careful not to damage the vehicle paint or interior/exterior ornaments.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Disconnect the connectors.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B180598E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>From the interior, insert a piano wire between the vehicle body and back window glass as shown in the illustration.</ptxt>
<atten3>
<ptxt>Make sure not to damage the wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Tie objects that can serve as handles (for example, wooden blocks) to both wire ends.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B185856E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Place matchmarks over the glass and vehicle body on the locations indicated in the illustration.</ptxt>
<atten4>
<ptxt>Matchmarks do not need to be placed if the glass is not going to be reused.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Cut through the adhesive by pulling the piano wire around the back window glass.</ptxt>
<atten3>
<ptxt>Leave as much adhesive on the vehicle body as possible when removing the back window glass.</ptxt>
</atten3>
</s2>
<s2>
<figure>
<graphic graphicname="B185862E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using suction cups, remove the back window glass.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001927079X_02_0008" proc-id="RM22W0E___0000HUR00001">
<ptxt>CLEAN VEHICLE BODY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B106081E11" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a scraper, remove the moulding and adhesive from the back window glass.</ptxt>
</s2>
<s2>
<ptxt>Clean and shape the contact surface of the vehicle body.</ptxt>
<s3>
<ptxt>On the contact surface of the vehicle body, use a knife to cut away excess adhesive as shown in the illustration.</ptxt>
<atten4>
<ptxt>Leave as much adhesive on the vehicle body as possible.</ptxt>
</atten4>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Clean the contact surface of the vehicle body with cleaner.</ptxt>
<atten4>
<ptxt>Even if all the adhesive has been removed, clean the vehicle body.</ptxt>
</atten4>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>