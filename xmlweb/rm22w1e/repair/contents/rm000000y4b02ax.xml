<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T8_T00MB" variety="T00MB">
<name>THEFT DETERRENT SYSTEM (w/ Entry and Start System)</name>
<para id="RM000000Y4B02AX" category="J" type-id="300CF" name-id="TD4IQ-03" from="201308">
<dtccode/>
<dtcname>IG Power Source Circuit</dtcname>
<subpara id="RM000000Y4B02AX_01" type-id="60" category="03" proc-id="RM22W0E___0000EK400001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>When the engine switch is turned on (IG), positive (+) battery voltage is applied to the certification ECU (smart key ECU assembly).</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000Y4B02AX_02" type-id="32" category="03" proc-id="RM22W0E___0000EK500001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B159040E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000Y4B02AX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000Y4B02AX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000Y4B02AX_04_0001" proc-id="RM22W0E___0000EK600001">
<testtitle>CHECK BASIC FUNCTION (POWER SOURCE MODE FUNCTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the basic function of the power source mode function.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Basic function operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000Y4B02AX_04_0002" fin="false">OK</down>
<right ref="RM000000Y4B02AX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000Y4B02AX_04_0002" proc-id="RM22W0E___0000EK700001">
<testtitle>INSPECT FUSE (IGN)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the IGN fuse from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>IGN fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000Y4B02AX_04_0003" fin="false">OK</down>
<right ref="RM000000Y4B02AX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000Y4B02AX_04_0003" proc-id="RM22W0E___0000EK800001">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU [SMART KEY ECU ASSEMBLY] - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B188626E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E29 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-17 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-18 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000Y4B02AX_04_0011" fin="true">OK</down>
<right ref="RM000000Y4B02AX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000Y4B02AX_04_0007">
<testtitle>GO TO ENTRY AND START SYSTEM (for Start Function)<xref label="Seep01" href="RM000000XI60IDX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000Y4B02AX_04_0008">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000000Y4B02AX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000Y4B02AX_04_0011">
<testtitle>PROCEED TO NEXT CIRCUIT INSPECTION SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002TGC01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>