<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0019" variety="S0019">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0019_7C3ME_T00FH" variety="T00FH">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1GR-FE)</name>
<para id="RM000000WP10GWX" category="J" type-id="30128" name-id="AT2KR-05" from="201308">
<dtccode/>
<dtcname>Transmission Control Switch Circuit</dtcname>
<subpara id="RM000000WP10GWX_01" type-id="60" category="03" proc-id="RM22W0E___00007O500001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When moving the shift lever to S using the transmission control switch, it is possible to switch the shift range between "1" (1st range) and "5" (5th range).</ptxt>
<ptxt>Shifting to "+" once raises the shift range by one, and shifting to "-" lowers the shift range by one.</ptxt>
</content5>
</subpara>
<subpara id="RM000000WP10GWX_02" type-id="32" category="03" proc-id="RM22W0E___00007O600001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C176192E24" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000WP10GWX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000WP10GWX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000WP10GWX_04_0001" proc-id="RM22W0E___00007O700001">
<testtitle>INSPECT TRANSMISSION CONTROL SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the transmission control switch connector.</ptxt>
<figure>
<graphic graphicname="C203344E22" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>3 (IG) - 7 (S)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S, "+" or "-"</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (SFTU) - 5 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever held in "+"</ptxt>
<ptxt>(Up-shift)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 (SFTD) - 5 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever held in "-"</ptxt>
<ptxt>(Down-shift)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 (IG) - 7 (S)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in S, "+" or "-"</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (SFTU) - 5 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 (SFTD) - 5 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transmission Control Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000WP10GWX_04_0002" fin="false">OK</down>
<right ref="RM000000WP10GWX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WP10GWX_04_0002" proc-id="RM22W0E___00007O800001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSMISSION CONTROL SWITCH - BATTERY, BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the transmission control switch connector.</ptxt>
<figure>
<graphic graphicname="C203343E39" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E52-3 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E52-3 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E52-5 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transmission Control Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000WP10GWX_04_0003" fin="false">OK</down>
<right ref="RM000000WP10GWX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WP10GWX_04_0003" proc-id="RM22W0E___00007O900001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSMISSION CONTROL SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="C197708E74" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A38-25 (S) - Body ground </ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-25 (S) - Body ground </ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A38-38 (SFTU) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever held in "+"</ptxt>
<ptxt>(Up-shift)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-27 (SFTD) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever held in "-"</ptxt>
<ptxt>(Down-shift)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-38 (SFTU) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-27 (SFTD) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000WP10GWX_04_0008" fin="true">OK</down>
<right ref="RM000000WP10GWX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WP10GWX_04_0008">
<testtitle>PROCEED TO NEXT CIRCUIT INSPECTION SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000W730ZDX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WP10GWX_04_0009">
<testtitle>REPLACE TRANSMISSION CONTROL SWITCH (LOWER SHIFT LEVER ASSEMBLY)<xref label="Seep01" href="RM000002YBF03IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WP10GWX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000WP10GWX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>