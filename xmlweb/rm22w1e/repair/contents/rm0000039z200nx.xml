<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C40B_T00TE" variety="T00TE">
<name>BLACK OUT TAPE (for Front Door)</name>
<para id="RM0000039Z200NX" category="A" type-id="30014" name-id="ET8DP-01" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM0000039Z200NX_01" type-id="11" category="10" proc-id="RM22W0E___0000JNU00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>When installing the black out tape and window frame moulding, heat the vehicle body, black out tape and window frame moulding using a heat light.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>Standard Heating Temperature</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Body</ptxt>
</entry>
<entry valign="middle">
<ptxt>40 to 60°C (104 to 140°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Black Out Tape</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Window Frame Moulding</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not heat the vehicle body, black out tape and window frame moulding excessively.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM0000039Z200NX_02" type-id="01" category="01">
<s-1 id="RM0000039Z200NX_02_0001" proc-id="RM22W0E___0000JNV00000">
<ptxt>REPAIR INSTRUCTION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the vehicle body surface.</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body surface.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any tape adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Installation temperature</ptxt>
<s3>
<ptxt>When the ambient temperature is below 15°C (59°F), perform the installation procedure after warming the vehicle body surface (installation surface of the door frame) and tape up to between 20 and 30°C (68 and 86°F) using a heat light. When the ambient temperature is above 35°C (95°F), cool the vehicle body surface (installation surface of the door frame) and tape down to between 20 and 30°C (68 and 86°F) prior to installation.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The most appropriate temperature for installing the tape is 25°C (77°F).</ptxt>
</item>
<item>
<ptxt>When the temperature is low, the tape turns stiff and falls off easily. When the temperature is high, the tape loses elasticity.</ptxt>
</item>
</list1>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Before installation</ptxt>
<s3>
<ptxt>Make sure any dirt on and around the vehicle body surface where the tape will be installed (installation surface of the door frame) is removed, and that the surface is smooth. If the surface is rough or dirt remains when pressing the tape onto the surface, air will be trapped under the tape and result in a poor appearance.</ptxt>
<atten4>
<ptxt>Spray water on the shop floor to settle any dust.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Key points for handling the tape</ptxt>
<s3>
<ptxt>The tape bends and rolls up easily. Store the tape between flat pieces of cardboard or other similar objects and keep it dry and level.</ptxt>
<atten3>
<ptxt>Do not bend the tape or leave it in high temperature places.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Key points for the installation of the tape (how to use a squeegee and the installation procedure for flat surfaces)</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Position the tape with a high level of accuracy to achieve a neat finish and to avoid peeling.</ptxt>
</item>
<item>
<ptxt>The tape cannot be reused because it deforms and will not fit after removal.</ptxt>
</item>
</list1>
</atten3>
<s3>
<ptxt>To avoid air bubbles, slightly raise the part of the tape that is going to be applied so that its adhesive surface does not touch the vehicle body while applying the tape. Tilt the squeegee at 40 to 50° (for pressing forward) or 30 to 45° (for pulling) to the vehicle body surface and press the tape onto the vehicle body surface with a force of 20 to 30 N (2 to 3 kgf) at a constant slow speed of 3 to 7 cm (1.2 to 2.8 in.) per second.</ptxt>
<figure>
<graphic graphicname="B147252E04" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten3>
<ptxt>Be sure to observe the specified pressing speed, force, and angle of the squeegee to avoid wrinkles or air bubbles.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Either angle of the squeegee (for pressing forward or for pulling) is acceptable.</ptxt>
</item>
<item>
<ptxt>Be sure to apply the tape while removing the release paper 10 to 20 mm (0.393 to 0.787 in.) from the edge of the squeegee.</ptxt>
</item>
</list1>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Key points for the installation of the tape (how to use a squeegee and the installation procedure for hemming surfaces)</ptxt>
<s3>
<figure>
<graphic graphicname="B147253E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If it is difficult to press the tape, press it in several steps as shown in the illustration. Use your fingers or the padded surface of a squeegee to slowly apply the tape to the hem of the vehicle, especially for a small hem.</ptxt>
<atten4>
<ptxt>When applying tape to the backside of a hem, remove the release paper and use your fingers or the padded surface of a squeegee.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Key points for the installation of the tape (how to use a squeegee and the installation procedure for corners)</ptxt>
<s3>
<ptxt>Remove the release paper and apply the tape carefully with your fingers.</ptxt>
</s3>
<s3>
<ptxt>Before applying the tape to each corner, heat the tape using a heat light and gradually apply it to avoid wrinkles on the tape and achieve a neat finish.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check after installation</ptxt>
<s3>
<ptxt>After completing the application, check if the tape is applied neatly. If the tape is not applied neatly, apply new tape.</ptxt>
<atten3>
<ptxt>Do not reuse the tape.</ptxt>
</atten3>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039Z200NX_02_0002" proc-id="RM22W0E___0000JNW00000">
<ptxt>INSTALL NO. 4 BLACK OUT TAPE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182615" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Refer to the illustration to position a new No. 4 black out tape.</ptxt>
</s2>
<s2>
<ptxt>Remove the release paper and apply the tape.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039Z200NX_02_0003" proc-id="RM22W0E___0000JNX00000">
<ptxt>INSTALL NO. 3 BLACK OUT TAPE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182613" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Refer to the illustration to position a new No. 3 black out tape.</ptxt>
</s2>
<s2>
<ptxt>Remove the release paper and apply the tape.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039Z200NX_02_0004" proc-id="RM22W0E___0000JNY00000">
<ptxt>INSTALL NO. 1 BLACK OUT TAPE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182611E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Refer to the illustration to position a new No. 1 black out tape.</ptxt>
<spec>
<title>Standard Dimension</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Area</ptxt>
</entry>
<entry valign="middle">
<ptxt>Dimension</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle">
<ptxt>2 to 4 mm (0.078 to 0.158 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 to 6 mm (0.158 to 0.236 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Remove the release paper and apply the tape.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039Z200NX_02_0005" proc-id="RM22W0E___0000JNZ00000">
<ptxt>INSTALL FRONT DOOR WEATHERSTRIP LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184537" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 6 clips and install the upper part of the front door weatherstrip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039Z200NX_02_0023" proc-id="RM22W0E___0000IEA00000">
<ptxt>INSTALL LOWER DOOR FRAME GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and install a new lower door frame garnish LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0021" proc-id="RM22W0E___0000IEH00000">
<ptxt>INSTALL FRONT DOOR REAR WINDOW FRAME MOULDING LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the vehicle body surface.</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body surface.</ptxt>
</s3>
<s3>
<ptxt>Remove the double-sided tape from the vehicle body surface.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any tape adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install a new window frame moulding.</ptxt>
<s3>
<ptxt>Using a heat light, heat a new window frame moulding and the vehicle body surface.</ptxt>
</s3>
<s3>
<ptxt>Remove the peeling paper from the face of the window frame moulding.</ptxt>
<atten4>
<ptxt>After removing the peeling paper, keep the exposed adhesive free from foreign matter.</ptxt>
</atten4>
</s3>
<s3>
<figure>
<graphic graphicname="B182608E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the clip and double-sided tape to install the window frame moulding.</ptxt>
<atten4>
<ptxt>Press the window frame moulding firmly to install it.</ptxt>
</atten4>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0022" proc-id="RM22W0E___0000IEI00000">
<ptxt>INSTALL FRONT DOOR BELT MOULDING ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182604" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the belt moulding.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0024" proc-id="RM22W0E___0000IEB00000">
<ptxt>INSTALL FRONT DOOR GLASS RUN LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door glass run LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0025" proc-id="RM22W0E___0000I0Y00000">
<ptxt>INSTALL FRONT DOOR GLASS SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Insert the front door glass sub-assembly LH into the door panel along the glass run as indicated by the arrows in the illustration.</ptxt>
<figure>
<graphic graphicname="B183141" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to damage the glass.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the front door glass sub-assembly LH to the front door window regulator sub-assembly LH with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0026" proc-id="RM22W0E___0000IEC00000">
<ptxt>INSTALL FRONT INNER DOOR GLASS WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front inner door glass weatherstrip LH.</ptxt>
<figure>
<graphic graphicname="B180828" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0027" proc-id="RM22W0E___0000E4H00000">
<ptxt>INSTALL FRONT DOOR SERVICE HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply butyl tape to the door.</ptxt>
<figure>
<graphic graphicname="B180831" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Pass the front door lock remote control cable assembly LH and front door inside locking cable assembly LH through a new front door service hole cover LH.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When installing the front door service hole cover LH, pull the links and connectors through the front door service hole cover LH.</ptxt>
</item>
<item>
<ptxt>There should be no wrinkles or folds after attaching the front door service hole cover LH.</ptxt>
</item>
<item>
<ptxt>After attaching the front door service hole cover LH, check the sealing quality.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Connect the 2 connectors.</ptxt>
<figure>
<graphic graphicname="B186231E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 9 clamps.</ptxt>
</s2>
<s2>
<ptxt>Install the bolt as shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>8.4</t-value1>
<t-value2>86</t-value2>
<t-value3>74</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0028" proc-id="RM22W0E___0000IEG00000">
<ptxt>INSTALL OUTER REAR VIEW MIRROR ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180573E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the claw and Install the mirror with the 3 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>w/ Power Mirror Control System:</ptxt>
<s3>
<ptxt>Attach the clamp.</ptxt>
</s3>
<s3>
<ptxt>Connect the connector labeled A.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0029" proc-id="RM22W0E___0000BP000000">
<ptxt>INSTALL FRONT DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B183139" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the front door lock remote control cable assembly LH and front door inside locking cable assembly LH to the front door inside handle sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 claws and 13 clips to install the front door trim board sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180821" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0030" proc-id="RM22W0E___0000BP400000">
<ptxt>INSTALL DOOR ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 claws to install the door assist grip cover LH to the front door trim board sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0031" proc-id="RM22W0E___0000BP100000">
<ptxt>INSTALL FRONT DOOR ARMREST BASE PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector. </ptxt>
</s2>
<s2>
<ptxt>Attach the 5 claws to install the armrest base panel.</ptxt>
<figure>
<graphic graphicname="B183137" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0032" proc-id="RM22W0E___0000BP200000">
<ptxt>INSTALL FRONT DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws to install the front door inside handle bezel LH.</ptxt>
<figure>
<graphic graphicname="B183136" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0033" proc-id="RM22W0E___0000BP500000">
<ptxt>INSTALL FRONT LOWER DOOR FRAME BRACKET GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and claw, and install the front door lower frame bracket garnish LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z200NX_02_0020" proc-id="RM22W0E___0000JO000000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>