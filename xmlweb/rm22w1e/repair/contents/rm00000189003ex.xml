<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12048_S0020" variety="S0020">
<name>1UR-FE BATTERY / CHARGING</name>
<ttl id="12048_S0020_7C3S5_T00L8" variety="T00L8">
<name>GENERATOR (for 130 A, 150 A Type)</name>
<para id="RM00000189003EX" category="A" type-id="80002" name-id="BH0IA-05" from="201301">
<name>DISASSEMBLY</name>
<subpara id="RM00000189003EX_01" type-id="01" category="01">
<s-1 id="RM00000189003EX_01_0003" proc-id="RM22W0E___0000D3X00000">
<ptxt>REMOVE GENERATOR PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Mount the generator in a vise between aluminum plates.</ptxt>
</s2>
<s2>
<ptxt>Install SST 1-A to the pulley shaft.</ptxt>
<sst>
<sstitem>
<s-number>09820-63011</s-number>
<s-subnumber>09820-06010</s-subnumber>
<s-subnumber>09820-06021</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="A187035E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<tbody>
<row>
<entry align="center">
<ptxt>SST 1-A and B</ptxt>
</entry>
<entry align="center">
<ptxt>09820-06010</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>SST 2</ptxt>
</entry>
<entry align="center">
<ptxt>09820-06021</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</s2>
<s2>
<ptxt>Insert SST 1-B to SST 1-A.</ptxt>
</s2>
<s2>
<ptxt>Hold SST 1-A with a torque wrench and turn SST 1-B clockwise with the specified torque.</ptxt>
<torque>
<torqueitem>
<t-value1>39</t-value1>
<t-value2>398</t-value2>
<t-value4>29</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Check that SST is secured on the rotor shaft.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Insert SST 2 and attach it to the pulley nut.</ptxt>
<figure>
<graphic graphicname="A187036E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Insert</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>To loosen the pulley nut, turn SST 1-A in the direction shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A187037E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>To prevent damage to the rotor shaft, do not loosen the pulley nut more than one-half turn.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove SST 2 from the generator.</ptxt>
</s2>
<s2>
<ptxt>Turn SST 1-B in the direction shown in the illustration, and then remove SST 1-A and SST 1-B.</ptxt>
<figure>
<graphic graphicname="A187038E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the pulley nut and generator pulley.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000189003EX_01_0001" proc-id="RM22W0E___0000D3V00000">
<ptxt>REMOVE GENERATOR REAR END COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 nuts and generator rear end cover.</ptxt>
<figure>
<graphic graphicname="A230483" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000189003EX_01_0013" proc-id="RM22W0E___0000D4000000">
<ptxt>REMOVE TERMINAL INSULATOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A230484" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1>
</s-1>
<s-1 id="RM00000189003EX_01_0002" proc-id="RM22W0E___0000D3W00000">
<ptxt>REMOVE GENERATOR BRUSH HOLDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and brush holder.</ptxt>
<figure>
<graphic graphicname="A230485" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000189003EX_01_0004" proc-id="RM22W0E___0000D3Y00000">
<ptxt>REMOVE GENERATOR COIL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
<figure>
<graphic graphicname="A230486" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, remove the generator coil.</ptxt>
<sst>
<sstitem>
<s-number>09950-40011</s-number>
<s-subnumber>09951-04020</s-subnumber>
<s-subnumber>09952-04010</s-subnumber>
<s-subnumber>09953-04020</s-subnumber>
<s-subnumber>09954-04010</s-subnumber>
<s-subnumber>09955-04071</s-subnumber>
<s-subnumber>09957-04010</s-subnumber>
<s-subnumber>09958-04011</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="A156415E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000189003EX_01_0010" proc-id="RM22W0E___0000D3Z00000">
<ptxt>REMOVE GENERATOR ROTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the generator washer.</ptxt>
<figure>
<graphic graphicname="A156421" width="2.775699831in" height="1.771723296in"/>

</figure>
</s2>
<s2>
<ptxt>Remove the generator rotor.</ptxt>
<figure>
<graphic graphicname="A230487" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000189003EX_01_0014" proc-id="RM22W0E___0000D4100000">
<ptxt>INSPECT GENERATOR DRIVE END FRAME BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the drive end frame bearing is not rough or worn.</ptxt>
<figure>
<graphic graphicname="A156418" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If necessary, replace the generator drive end frame bearing.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000189003EX_01_0015" proc-id="RM22W0E___0000D4200000">
<ptxt>REMOVE GENERATOR DRIVE END FRAME BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 screws and retainer plate.</ptxt>
<figure>
<graphic graphicname="A156416" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST and a hammer, tap out the drive end frame bearing.</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00250</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="A156417E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>