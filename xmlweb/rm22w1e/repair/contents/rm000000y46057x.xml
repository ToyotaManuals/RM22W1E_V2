<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3XC_T00QF" variety="T00QF">
<name>POWER WINDOW CONTROL SYSTEM (for Models with Jam Protection Function on Driver Door Window Only)</name>
<para id="RM000000Y46057X" category="J" type-id="800PB" name-id="WS4YQ-04" from="201301">
<dtccode/>
<dtcname>Driver Side Power Window Auto Up / Down Function does not Operate with Power Window Master Switch</dtcname>
<subpara id="RM000000Y46057X_01" type-id="60" category="03" proc-id="RM22W0E___0000I4G00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>If the auto up/down function does not operate, the cause may be one or more of the following:</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>The ECU in the power window regulator motor determines that the power window regulator motor has not been initialized.</ptxt>
</item>
<item>
<ptxt>The master switch has a malfunction.</ptxt>
</item>
<item>
<ptxt>The Hall-IC in the driver side power window regulator motor has a malfunction.</ptxt>
</item>
<item>
<ptxt>There is an open or short in the wiring between the master switch and the driver side power window regulator motor.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000Y46057X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000Y46057X_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000Y46057X_06_0001" proc-id="RM22W0E___0000I4H00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000000YCC0ACX"/>).</ptxt>
<table>
<title>Result </title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.63in"/>
<colspec colname="COL2" colwidth="1.50in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>B2311 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>B2312 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>B2313 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>LIN communication system DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000Y46057X_06_0004" fin="false">A</down>
<right ref="RM000000Y46057X_06_0018" fin="true">B</right>
<right ref="RM000000Y46057X_06_0011" fin="true">C</right>
<right ref="RM000000Y46057X_06_0012" fin="true">D</right>
<right ref="RM000000Y46057X_06_0010" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000000Y46057X_06_0004" proc-id="RM22W0E___0000I4I00000">
<testtitle>CHECK MANUAL UP/DOWN FUNCTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the driver side door power window moves when the manual up/down function of the multiplex network master switch is operated (See page <xref label="Seep01" href="RM0000016810CQX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Driver side door power window moves.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000Y46057X_06_0015" fin="false">OK</down>
<right ref="RM000000Y46057X_06_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000Y46057X_06_0015" proc-id="RM22W0E___0000I4J00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (MULTIPLEX NETWORK MASTER SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the master switch is functioning properly (See page <xref label="Seep01" href="RM0000027QL03EX"/>).</ptxt>
<table pgwide="1">
<title>Master Switch</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.57in"/>
<colspec colname="COL2" colwidth="1.97in"/>
<colspec colname="COL3" colwidth="2.09in"/>
<colspec colname="COL4" colwidth="1.46in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>D Door P/W Auto SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side power window auto up/down signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side power window auto up/down switch operated</ptxt>
<ptxt>OFF: Driver side power window switch not operated</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display changes according to operation of the multiplex network master switch.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG</ptxt>
</entry>
<entry align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000Y46057X_06_0021" fin="true">A</down>
<right ref="RM000000Y46057X_06_0022" fin="true">B</right>
<right ref="RM000000Y46057X_06_0008" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000Y46057X_06_0018">
<testtitle>GO TO DTC B2311<xref label="Seep01" href="RM000002BQS08LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000Y46057X_06_0011">
<testtitle>GO TO DTC B2312<xref label="Seep01" href="RM000002BQR07RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000Y46057X_06_0012">
<testtitle>GO TO DTC B2313<xref label="Seep01" href="RM0000027JF08MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000Y46057X_06_0010">
<testtitle>GO TO LIN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000002S88023X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000Y46057X_06_0014">
<testtitle>OTHER PROBLEM (GO TO PROBLEM SYMPTOMS TABLE)<xref label="Seep01" href="RM000000XHI0AKX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000Y46057X_06_0022">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR ASSEMBLY RH<xref label="Seep01" href="RM000002STX03BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000Y46057X_06_0021">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR ASSEMBLY LH<xref label="Seep01" href="RM000002STX03BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000Y46057X_06_0008">
<testtitle>REPLACE MULTIPLEX NETWORK MASTER SWITCH ASSEMBLY<xref label="Seep01" href="RM000002STU087X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>