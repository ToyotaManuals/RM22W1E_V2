<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000PFU0VMX" category="C" type-id="302IE" name-id="ESUB3-07" from="201308">
<dtccode>P2102</dtccode>
<dtcname>Throttle Actuator Control Motor Circuit Low</dtcname>
<dtccode>P2103</dtccode>
<dtcname>Throttle Actuator Control Motor Circuit High</dtcname>
<subpara id="RM000000PFU0VMX_01" type-id="60" category="03" proc-id="RM22W0E___00001GI00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The throttle actuator is operated by the ECM and opens and closes the throttle valve using gears.</ptxt>
<ptxt>The opening angle of the throttle valve is detected by the throttle position sensor, which is mounted on the throttle body. The throttle position sensor provides feedback to the ECM. This feedback allows the ECM to appropriately control the throttle actuator and monitor the throttle opening angle as the ECM responds to driver inputs.</ptxt>
<atten4>
<ptxt>This ETCS (Electronic Throttle Control System) does not use a throttle cable.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2102</ptxt>
</entry>
<entry valign="middle">
<ptxt>Conditions (a) and (b) continue for 2.0 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) Throttle actuator duty ratio is 80% or more.</ptxt>
<ptxt>(b) Throttle actuator current is below 0.5 A.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in throttle actuator circuit</ptxt>
</item>
<item>
<ptxt>Throttle actuator</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2103</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>There is a hybrid IC diagnosis signal failure.</ptxt>
</item>
<item>
<ptxt>There is a hybrid IC current limiter port failure.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in throttle actuator circuit</ptxt>
</item>
<item>
<ptxt>Throttle actuator</ptxt>
</item>
<item>
<ptxt>Throttle valve</ptxt>
</item>
<item>
<ptxt>Throttle body assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFU0VMX_02" type-id="64" category="03" proc-id="RM22W0E___00001GJ00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM monitors the electrical current through the electronic actuator, and detects malfunctions and open circuits in the throttle actuator based on this value. If the current is outside the standard range, the ECM determines that there is a malfunction in the throttle actuator. In addition, if the throttle valve does not function properly (for example, if it is stuck on), the ECM determines that there is a malfunction. The ECM then illuminates the MIL and stores a DTC.</ptxt>
<ptxt>Example:</ptxt>
<ptxt>When the electrical current is less than 0.5 A and the throttle actuator duty ratio is 80% or more, the ECM interprets this as the current being outside the standard range, illuminates the MIL and stores a DTC.</ptxt>
<ptxt>If the malfunction is not repaired successfully, a DTC is stored when the engine is quickly revved to a high engine speed several times after the engine is started and has idled for 5 seconds.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFU0VMX_10" type-id="73" category="03" proc-id="RM22W0E___00001GR00001">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A199313E12" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on [A].</ptxt>
</item>
<item>
<ptxt>Start the engine.</ptxt>
</item>
<item>
<ptxt>With the vehicle stationary, fully depress the accelerator pedal and quickly release it [B].</ptxt>
</item>
<item>
<ptxt>Check that 16 seconds or more have elapsed from the instant when the accelerator pedal is first depressed.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes [C].</ptxt>
</item>
<item>
<ptxt>Read the pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a pending DTC is output, the system is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If a pending DTC is not output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P2102 or P2103.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows INCOMPLETE or N/A, perform steps [B] through [C] again and, if necessary, drive the vehicle for a period of time.</ptxt>
</item>
</list1>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000PFU0VMX_06" type-id="62" category="03" proc-id="RM22W0E___00001GK00001">
<name>FAIL-SAFE</name>
<content5 releasenbr="1">
<ptxt>When either of these DTCs, or other DTCs relating to ETCS (Electronic Throttle Control System) malfunctions are stored, the ECM enters fail-safe mode. During fail-safe mode, the ECM cuts the current to the throttle actuator and the throttle valve is returned to a 7° throttle angle by the return spring. The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel-cut) and ignition timing in accordance with the accelerator pedal position to allow the vehicle to continue at a minimal speed. If the accelerator pedal is depressed firmly and gently, the vehicle can be driven slowly.</ptxt>
<ptxt>The ECM continues operating in fail-safe mode until a pass condition is detected, and the engine switch is then turned off.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFU0VMX_07" type-id="32" category="03" proc-id="RM22W0E___00001GL00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A278624E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000PFU0VMX_08" type-id="51" category="05" proc-id="RM22W0E___00001GM00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>The throttle actuator current (Throttle Motor Current) and the throttle actuator duty ratio (Throttle Motor Open Duty/Throttle Motor Close Duty) can be read using the GTS. However, the ECM shuts off the throttle actuator current when the ETCS malfunctions.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFU0VMX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PFU0VMX_09_0001" proc-id="RM22W0E___00001GN00001">
<testtitle>INSPECT THROTTLE BODY ASSEMBLY (RESISTANCE OF THROTTLE ACTUATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the throttle body assembly (See page <xref label="Seep01" href="RM000002PRS02TX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFU0VMX_09_0002" fin="false">OK</down>
<right ref="RM000000PFU0VMX_09_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFU0VMX_09_0002" proc-id="RM22W0E___00001GO00001">
<testtitle>CHECK HARNESS AND CONNECTOR (THROTTLE ACTUATOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle body connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C3-2 (M+) - C46-19 (M+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-1 (M-) - C46-18 (M-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-2 (M+) or C46-19 (M+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-1 (M-) or C46-18 (M-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C3-2 (M+) - C45-19 (M+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-1 (M-) - C45-18 (M-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-2 (M+) or C45-19 (M+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-1 (M-) or C45-18 (M-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PFU0VMX_09_0003" fin="false">OK</down>
<right ref="RM000000PFU0VMX_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFU0VMX_09_0003" proc-id="RM22W0E___00001GP00001">
<testtitle>INSPECT THROTTLE BODY ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for foreign objects between the throttle valve and the housing.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No foreign objects between throttle valve and housing.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PFU0VMX_09_0004" fin="false">OK</down>
<right ref="RM000000PFU0VMX_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFU0VMX_09_0004" proc-id="RM22W0E___00001GQ00001">
<testtitle>INSPECT THROTTLE VALVE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the throttle valve opens and closes smoothly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Throttle valve opens and closes smoothly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PFU0VMX_09_0009" fin="true">OK</down>
<right ref="RM000000PFU0VMX_09_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFU0VMX_09_0005">
<testtitle>REPLACE THROTTLE BODY ASSEMBLY<xref label="Seep01" href="RM000002PQN034X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFU0VMX_09_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000PFU0VMX_09_0007">
<testtitle>REMOVE FOREIGN OBJECT AND CLEAN THROTTLE BODY</testtitle>
</testgrp>
<testgrp id="RM000000PFU0VMX_09_0008">
<testtitle>REPLACE THROTTLE BODY ASSEMBLY<xref label="Seep01" href="RM000002PQN034X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFU0VMX_09_0009">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>