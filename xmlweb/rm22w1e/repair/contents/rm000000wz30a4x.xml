<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000000WZ30A4X" category="J" type-id="3009O" name-id="ES11JG-002" from="201301">
<dtccode/>
<dtcname>Starter Signal Circuit</dtcname>
<subpara id="RM000000WZ30A4X_01" type-id="60" category="03" proc-id="RM22W0E___00000C700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>While the engine is being cranked, current flows from terminal ST1 of the ignition switch to clutch start switch and also flows to terminal STA of the ECM (STA Signal).</ptxt>
</content5>
</subpara>
<subpara id="RM000000WZ30A4X_02" type-id="32" category="03" proc-id="RM22W0E___00000C800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0617 (See page <xref label="Seep01" href="RM000000TA01A1X_05"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000WZ30A4X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000WZ30A4X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000WZ30A4X_05_0024" proc-id="RM22W0E___00000C900000">
<testtitle>READ VALUE USING GTS (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Read values.</ptxt>
</test1>
<test1>
<ptxt>Check the result when the ignition switch is turned to ON and when the engine is started.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Display (Starter Signal)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine is started</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0030" fin="false">OK</down>
<right ref="RM000000WZ30A4X_05_0028" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0030" proc-id="RM22W0E___00000CC00000">
<testtitle>INSPECT STARTER RELAY (ST)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the starter relay (ST) (See page <xref label="Seep01" href="RM000003BIA02EX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0029" fin="false">OK</down>
<right ref="RM000000WZ30A4X_05_0040" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0029" proc-id="RM22W0E___00000CB00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ST RELAY - CLUTCH START SWITCH)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A299801E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the clutch start switch.</ptxt>
</test1>
<test1>
<ptxt>Remove the ST relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ST relay (2) - A49-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ST relay (2) or A49-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ST relay (1)* - A49-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ST relay (1)* or A49-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ST relay (2) - e1-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ST relay (2) or e1-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>*: GRJ200L-GNANKC</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0032" fin="false">OK</down>
<right ref="RM000000WZ30A4X_05_0044" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0032" proc-id="RM22W0E___00000CD00000">
<testtitle>CHECK HARNESS AND CONNECTOR (POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A299802E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Remove the ST relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry>
<ptxt>Tester Connection</ptxt>
</entry>
<entry>
<ptxt>Condition</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay (1) - ST relay (5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ST relay (2)* - ST relay (5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>*: GRJ200L-GNANKC</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0033" fin="false">OK</down>
<right ref="RM000000WZ30A4X_05_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0033" proc-id="RM22W0E___00000CE00000">
<testtitle>INSPECT STARTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the starter (See page <xref label="Seep01" href="RM0000015XW01UX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0045" fin="true">OK</down>
<right ref="RM000000WZ30A4X_05_0043" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0028" proc-id="RM22W0E___00000CA00000">
<testtitle>INSPECT CLUTCH START SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the clutch start switch (for LHD) (See page <xref label="Seep01" href="RM0000032S3008X"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the clutch start switch (for RHD) (See page <xref label="Seep02" href="RM000003D8H002X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>OK</ptxt>
</entry>
<entry align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0060" fin="false">A</down>
<right ref="RM000000WZ30A4X_05_0039" fin="true">B</right>
<right ref="RM000000WZ30A4X_05_0069" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0060" proc-id="RM22W0E___00000CH00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CLUTCH START SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A266612E02" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Disconnect the clutch start switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>A49-2 - A38-46 (STA)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>A49-2 or A38-46 (STA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>e1-2 - A52-46 (STA)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>e1-2 or A52-46 (STA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0063" fin="false">OK</down>
<right ref="RM000000WZ30A4X_05_0048" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0063" proc-id="RM22W0E___00000CI00000">
<testtitle>CHECK IF VEHICLE IS EQUIPPED WITH ENTRY AND START SYSTEM</testtitle>
<content6 releasenbr="1">
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="3.54in"/>
<colspec colname="COL2" align="center" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>Result</ptxt>
</entry>
<entry>
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>w/o Entry and Start System</ptxt>
</entry>
<entry>
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0036" fin="false">A</down>
<right ref="RM000000WZ30A4X_05_0064" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0036" proc-id="RM22W0E___00000CF00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202PX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0057" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0057" proc-id="RM22W0E___00000CG00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the starter operation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction has been repaired successfully.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0059" fin="true">OK</down>
<right ref="RM000000WZ30A4X_05_0046" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0064" proc-id="RM22W0E___00000CJ00000">
<testtitle>INSPECT IGNITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the ignition switch assembly (See page <xref label="Seep01" href="RM000000YK004GX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0065" fin="false">OK</down>
<right ref="RM000000WZ30A4X_05_0066" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0065" proc-id="RM22W0E___00000CK00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CLUTCH START SWITCH - IGNITION SWITCH)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A157741E25" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Disconnect the clutch start switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ignition switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>A49-1 - E93-3 (ST1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>A49-1 or E93-3 (ST1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>e1-1 - E93-3 (ST1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>e1-1 or E93-3 (ST1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WZ30A4X_05_0068" fin="true">OK</down>
<right ref="RM000000WZ30A4X_05_0067" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0040">
<testtitle>REPLACE STARTER RELAY (ST)</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0044">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0031">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (STARTER RELAY (ST) - BATTERY, BODY GROUND)</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0043">
<testtitle>REPLACE STARTER<xref label="Seep01" href="RM0000022AC014X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0039">
<testtitle>REPLACE CLUTCH START SWITCH ASSEMBLY<xref label="Seep01" href="RM0000032S5004X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0046">
<testtitle>GO TO ENTRY AND START SYSTEM (ENGINE DOES NOT START)<xref label="Seep01" href="X39400000IL046J"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0066">
<testtitle>REPLACE IGNITION SWITCH ASSEMBLY<xref label="Seep01" href="RM000000YK102VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0067">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0045">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (STARTER - STARTER RELAY (ST), BATTERY)</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0048">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0059">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0068">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (IGNITION SWITCH - BATTERY)</testtitle>
</testgrp>
<testgrp id="RM000000WZ30A4X_05_0069">
<testtitle>REPLACE CLUTCH START SWITCH ASSEMBLY<xref label="Seep01" href="RM000003D8J002X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>