<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TW_T00MZ" variety="T00MZ">
<name>AIRBAG SYSTEM</name>
<para id="RM000002O2X01LX" category="C" type-id="303OF" name-id="RSARH-03" from="201301" to="201308">
<dtccode>B1925/78</dtccode>
<dtcname>Short in Rear Pretensioner Squib LH Circuit</dtcname>
<dtccode>B1926/78</dtccode>
<dtcname>Open in Rear Pretensioner Squib LH Circuit</dtcname>
<dtccode>B1927/78</dtccode>
<dtcname>Short to GND in Rear Pretensioner Squib LH Circuit</dtcname>
<dtccode>B1928/78</dtccode>
<dtcname>Short to B+ in Rear Pretensioner Squib LH Circuit</dtcname>
<subpara id="RM000002O2X01LX_01" type-id="60" category="03" proc-id="RM22W0E___0000FAS00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The rear pretensioner squib LH circuit consists of the center airbag sensor and the rear seat outer belt LH.</ptxt>
<ptxt>This circuit instructs the SRS to deploy when deployment conditions are met.</ptxt>
<ptxt>These DTCs are stored when a malfunction is detected in the rear pretensioner squib LH circuit.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COLSPEC0" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1925/78</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives a line short circuit signal 5 times in the rear pretensioner squib LH circuit during the primary check.</ptxt>
</item>
<item>
<ptxt>A rear pretensioner squib LH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Floor wire</ptxt>
</item>
<item>
<ptxt>Rear seat outer belt assembly LH (Rear pretensioner squib LH)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B1926/78</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives an open circuit signal in the rear pretensioner squib LH circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A rear pretensioner squib LH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Floor wire</ptxt>
</item>
<item>
<ptxt>Rear seat outer belt assembly LH (Rear pretensioner squib LH)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B1927/78</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives a short circuit to ground signal in the rear pretensioner squib LH circuit for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>A rear pretensioner squib LH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Floor wire</ptxt>
</item>
<item>
<ptxt>Rear seat outer belt assembly LH (Rear pretensioner squib LH)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B1928/78</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives a short circuit to B+ signal in the rear pretensioner squib LH circuit for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>A rear pretensioner squib LH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Floor wire</ptxt>
</item>
<item>
<ptxt>Rear seat outer belt assembly LH (rear pretensioner squib LH)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002O2X01LX_02" type-id="32" category="03" proc-id="RM22W0E___0000FAT00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C178002E12" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002O2X01LX_03" type-id="51" category="05" proc-id="RM22W0E___0000FAU00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="2">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep04" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep03" href="RM000003C32005X"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>To perform the simulation method, enter the check mode (signal check) with the intelligent tester (See page <xref label="Seep01" href="RM000000XFF0E9X"/>), and then wiggle each connector of the airbag system or drive the vehicle on various type of road (See page <xref label="Seep02" href="RM000000XFD0IZX"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002O2X01LX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002O2X01LX_04_0001" proc-id="RM22W0E___0000FAV00000">
<testtitle>CHECK REAR SEAT OUTER BELT LH (REAR PRETENSIONER SQUIB LH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C173791E05" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the connectors from the rear seat outer belt LH.</ptxt>
</test1>
<test1>
<ptxt>Connect the white wire side of SST (resistance: 2.1 Ω) to connector C.</ptxt>
<atten2>
<ptxt>Never connect the tester to the rear seat outer belt LH (rear pretensioner squib LH) for measurement, as this may lead to a serious injury due to airbag deployment.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not forcibly insert SST into the terminals of the connector when connecting SST.</ptxt>
</item>
<item>
<ptxt>Insert SST straight into the terminals of the connector.</ptxt>
</item>
</list1>
</atten3>
</test1>
<sst>
<sstitem>
<s-number>09843-18061</s-number>
</sstitem>
</sst>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1925, B1926, B1927 or B1928 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1925, B1926, B1927 and B1928 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002O2X01LX_04_0005" fin="true">OK</down>
<right ref="RM000002O2X01LX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002O2X01LX_04_0002" proc-id="RM22W0E___0000FAW00000">
<testtitle>CHECK CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Disconnect SST from connector C.</ptxt>
</test1>
<test1>
<ptxt>Check that the floor wire connector (on the rear seat outer belt LH side) is not damaged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The lock button is not disengaged, and the claw of the lock is not damaged or deformed.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002O2X01LX_04_0003" fin="false">OK</down>
<right ref="RM000002O2X01LX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002O2X01LX_04_0003" proc-id="RM22W0E___0000FAX00000">
<testtitle>CHECK FLOOR WIRE (REAR PRETENSIONER SQUIB LH CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor.</ptxt>
<figure>
<graphic graphicname="C173792E05" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" align="center" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K13-1 (RL+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K13-2 (RL-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" align="center" colwidth="1.37in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.39in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K13-1 (RL+) - K13-2 (RL-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Release the activation prevention mechanism built into connector B (See page <xref label="Seep01" href="RM000000XFD0IZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" align="center" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K13-1 (RL+) - K13-2 (RL-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K13-1 (RL+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K13-2 (RL-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002O2X01LX_04_0004" fin="false">OK</down>
<right ref="RM000002O2X01LX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002O2X01LX_04_0004" proc-id="RM22W0E___0000FAY00000">
<testtitle>CHECK CENTER AIRBAG SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the connectors to the rear seat outer belt LH and the center airbag sensor.</ptxt>
<figure>
<graphic graphicname="C148850E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1925, B1926, B1927 or B1928 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1925, B1926, B1927 or B1928 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002O2X01LX_04_0009" fin="true">OK</down>
<right ref="RM000002O2X01LX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002O2X01LX_04_0005">
<testtitle>REPLACE REAR SEAT OUTER BELT ASSEMBLY LH<xref label="Seep01" href="RM0000039ES015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002O2X01LX_04_0006">
<testtitle>REPLACE FLOOR WIRE</testtitle>
</testgrp>
<testgrp id="RM000002O2X01LX_04_0007">
<testtitle>REPLACE FLOOR WIRE</testtitle>
</testgrp>
<testgrp id="RM000002O2X01LX_04_0008">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000002Y2N00UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002O2X01LX_04_0009">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0IZX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>