<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TW_T00MZ" variety="T00MZ">
<name>AIRBAG SYSTEM</name>
<para id="RM0000038BD01GX" category="C" type-id="803SN" name-id="RSASC-03" from="201301" to="201308">
<dtccode>B1676/85</dtccode>
<dtcname>Lost Communication with Rear Floor Airbag Sensor RH</dtcname>
<dtccode>B1677/85</dtccode>
<dtcname>Rear Floor Airbag Sensor RH Initialization Incomplete</dtcname>
<subpara id="RM0000038BD01GX_01" type-id="60" category="03" proc-id="RM22W0E___0000FFI00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<ptxt>"Rear floor airbag sensor RH" refer to the rear floor airbag sensor.</ptxt>
</atten4>
<list1 type="unordered">
<item>
<ptxt>The rear floor airbag sensor circuit consists of the center airbag sensor and rear floor airbag sensor.</ptxt>
<ptxt>The rear floor airbag sensor detects impacts to the vehicle and sends signals to the center airbag sensor to determine if the airbag should be deployed.</ptxt>
<ptxt>DTC B1676/85 or B1677/85 is stored when a malfunction is detected in the rear floor airbag sensor circuit.</ptxt>
</item>
</list1>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1676/85</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives a line short circuit signal, an open circuit signal, a short circuit to ground signal or a short circuit to B+ signal in the rear floor airbag sensor circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A rear floor airbag sensor malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>No. 2 floor wire</ptxt>
</item>
<item>
<ptxt>Rear floor airbag sensor</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B1677/85</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives a line short circuit signal, an open circuit signal, a short circuit to ground signal or a short circuit to B+ signal in the rear floor airbag sensor circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A rear floor airbag sensor malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>No. 2 floor wire</ptxt>
</item>
<item>
<ptxt>Rear floor airbag sensor</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000038BD01GX_02" type-id="32" category="03" proc-id="RM22W0E___0000FFJ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C148443E36" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000038BD01GX_03" type-id="51" category="05" proc-id="RM22W0E___0000FFK00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM0000038BD01GX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000038BD01GX_04_0001" proc-id="RM22W0E___0000FFL00000">
<testtitle>CHECK CONNECTION OF CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors are properly connected to the center airbag sensor and the rear floor airbag sensor.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are properly connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000038BD01GX_04_0002" fin="false">OK</down>
<right ref="RM0000038BD01GX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000038BD01GX_04_0002" proc-id="RM22W0E___0000FFM00000">
<testtitle>CHECK CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor and rear floor airbag sensor.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors (on the center airbag sensor side and rear floor airbag sensor side) are not damaged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are not deformed or damaged.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000038BD01GX_04_0003" fin="false">OK</down>
<right ref="RM0000038BD01GX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000038BD01GX_04_0003" proc-id="RM22W0E___0000FFN00000">
<testtitle>CHECK REAR FLOOR AIRBAG SENSOR CIRCUIT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
<figure>
<graphic graphicname="C262845E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L19-2 (BER+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L19-1 (BER-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Using a service wire, connect terminals 21 (BDR+) and 22 (BDR-) of connector B.</ptxt>
<atten3>
<ptxt>Do not forcibly insert the service wire into the terminals of the connector when connecting a service wire.</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L19-2 (BER+) - L19-1 (BER-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Disconnect the service wire from connector B.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L19-2 (BER+) - L19-1 (BER-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L19-2 (BER+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L19-1 (BER-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000038BD01GX_04_0004" fin="false">OK</down>
<right ref="RM0000038BD01GX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000038BD01GX_04_0004" proc-id="RM22W0E___0000FFO00000">
<testtitle>CHECK REAR FLOOR AIRBAG SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the connectors to the center airbag sensor.</ptxt>
<figure>
<graphic graphicname="C171812E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Temporarily replace the rear floor airbag sensor with a new one.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs stored in the memory (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
<test2>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1676 and B1677are not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1676 and B1677 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000038BD01GX_04_0010" fin="true">C</down>
<right ref="RM0000038BD01GX_04_0008" fin="true">A</right>
<right ref="RM0000038BD01GX_04_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000038BD01GX_04_0005">
<testtitle>CONNECT CONNECTORS PROPERLY</testtitle>
</testgrp>
<testgrp id="RM0000038BD01GX_04_0006">
<testtitle>REPLACE HARNESS AND CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000038BD01GX_04_0007">
<testtitle>REPLACE NO. 2 FLOOR WIRE</testtitle>
</testgrp>
<testgrp id="RM0000038BD01GX_04_0008">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0IZX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000038BD01GX_04_0009">
<testtitle>REPLACE REAR FLOOR AIRBAG SENSOR<xref label="Seep01" href="RM0000038MW01AX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000038BD01GX_04_0010">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000002Y2N00UX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>