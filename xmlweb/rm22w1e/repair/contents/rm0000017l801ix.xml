<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000A" variety="S000A">
<name>1GR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000A_7C3EP_T007S" variety="T007S">
<name>ENGINE</name>
<para id="RM0000017L801IX" category="G" type-id="8000T" name-id="EMBSH-01" from="201301">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM0000017L801IX_01" type-id="01" category="01">
<s-1 id="RM0000017L801IX_01_0007" proc-id="RM22W0E___000042C00000">
<ptxt>INSPECT IGNITION TIMING</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Turn all electrical systems off.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Perform the inspection when the cooling fan motor is turned off.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>When using the GTS:</ptxt>
<s3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / IGN Advance.</ptxt>
</s3>
<s3>
<ptxt>Inspect the ignition timing during idling.</ptxt>
<spec>
<title>Standard ignition timing</title>
<specitem>
<ptxt>8 to 12° BTDC @ idle (transmission in neutral and A/C switch off) </ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Check that the ignition timing advances immediately when the engine speed is increased.</ptxt>
</s3>
</s2>
<s2>
<ptxt>When not using the GTS:</ptxt>
<figure>
<graphic graphicname="A206183E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Using SST, connect terminals 13 (TC) and 4 (CG) of the DLC3.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be sure not to improperly connect the terminals. This may damage the engine.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Connect the tester probe of a timing light to the wire of the ignition coil connector for the No. 1 cylinder.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Use a timing light that detects primary signals.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>After the inspection, be sure to wrap the wire harness with tape.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Inspect the ignition timing during idling.</ptxt>
<spec>
<title>Standard ignition timing</title>
<specitem>
<ptxt>8 to 12° BTDC @ idle (transmission in neutral and A/C switch off)</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Remove SST from the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Inspect the ignition timing during idling.</ptxt>
<spec>
<title>Standard ignition timing</title>
<specitem>
<ptxt>7 to 24° BTDC @ idle (transmission in neutral and A/C switch off)</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Disconnect the timing light from the engine.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017L801IX_01_0008" proc-id="RM22W0E___000042D00000">
<ptxt>INSPECT ENGINE IDLE SPEED</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Turn all the electrical systems off.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Perform the inspection when the cooling fan motor is turned off.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>When using the GTS:</ptxt>
<s3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Engine Speed.</ptxt>
</s3>
<s3>
<ptxt>Inspect the engine idle speed.</ptxt>
<spec>
<title>Standard idle speed</title>
<specitem>
<ptxt>690 to 790 rpm (transmission in neutral and A/C switch off)</ptxt>
</specitem>
</spec>
</s3>
</s2>
<s2>
<ptxt>When not using the GTS:</ptxt>
<figure>
<graphic graphicname="A206183E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Connect SST to terminal 9 (TAC) of the DLC3.</ptxt>
<sst>
<sstitem>
<s-number>09843-18030</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Race the engine at 2500 rpm for approximately 90 seconds.</ptxt>
</s3>
<s3>
<ptxt>Inspect the engine idle speed.</ptxt>
<spec>
<title>Standard idle speed</title>
<specitem>
<ptxt>690 to 790 rpm (transmission in neutral and A/C switch off)</ptxt>
</specitem>
</spec>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017L801IX_01_0009" proc-id="RM22W0E___000042E00000">
<ptxt>INSPECT COMPRESSION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Warm up and stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Remove the intake air surge tank (See page <xref label="Seep03" href="RM00000456D00PX"/>).</ptxt>
</s2>
<s2>
<ptxt>Remove the 6 spark plugs (See page <xref label="Seep01" href="RM000002I2Y02PX"/>).</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 6 fuel injector connectors.</ptxt>
</s2>
<s2>
<ptxt>Inspect the cylinder compression pressure.</ptxt>
<s3>
<ptxt>Insert a compression gauge into the spark plug hole. (Procedure A)</ptxt>
</s3>
<s3>
<ptxt>While cranking the engine, measure the compression pressure. (Procedure B)</ptxt>
<spec>
<title>Compression pressure</title>
<specitem>
<ptxt>1400 kPa (14.3 kgf/cm<sup>2</sup>, 203 psi) or higher</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum pressure</title>
<specitem>
<ptxt>1000 kPa (10.2 kgf/cm<sup>2</sup>, 145 psi)</ptxt>
</specitem>
</spec>
<spec>
<title>Difference between each cylinder</title>
<specitem>
<ptxt>100 kPa (1.0 kgf/cm<sup>2</sup>, 15 psi) or less</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use a fully-charged battery so that the engine speed can be increased to 250 rpm or more.</ptxt>
</item>
<item>
<ptxt>Measure the compression in as short a time as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>Repeat procedures A and B for each cylinder.</ptxt>
</s3>
<s3>
<ptxt>If the cylinder compression is low, pour a small amount of engine oil into the cylinder through the spark plug hole and repeat procedures A and B for cylinders with low compression.</ptxt>
<list1 type="unordered">
<item>
<ptxt>If adding oil increases the compression, the piston rings and/or cylinder bore may be worn or damaged.</ptxt>
</item>
<item>
<ptxt>If pressure stays low, a valve may be stuck or seated improperly, or there may be leakage from the gasket.</ptxt>
</item>
</list1>
</s3>
</s2>
<s2>
<ptxt>Connect the 6 injector connectors.</ptxt>
</s2>
<s2>
<ptxt>Install the 6 spark plugs (See page <xref label="Seep02" href="RM000002I2W02PX"/>).</ptxt>
</s2>
<s2>
<ptxt>Install the intake air surge tank (See page <xref label="Seep04" href="RM00000456B00PX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017L801IX_01_0010" proc-id="RM22W0E___000042F00000">
<ptxt>INSPECT CO/HC</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>This check is to determine whether or not the idle CO/HC concentration complies with regulations.</ptxt>
</atten4>
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>Run the engine at 2500 rpm for approximately 180 seconds.</ptxt>
</s2>
<s2>
<ptxt>Insert the CO/HC meter testing probe at least 40 cm (1.31 ft) into the tailpipe during idling.</ptxt>
</s2>
<s2>
<ptxt>Immediately check the CO/HC concentration during idling and/or at 2500 rpm.</ptxt>
<atten4>
<ptxt>When carrying out the 2 tests (idling and 2500 rpm), the measurement orders are prescribed by the applicable local regulations.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>If the CO/HC concentration does not comply with regulations, perform troubleshooting in the order given below.</ptxt>
<s3>
<ptxt>Check the air fuel ratio sensor operation (See page <xref label="Seep01" href="RM000002RV2026X"/>) and heated oxygen sensor operation (See page <xref label="Seep02" href="RM000002RV601RX"/>).</ptxt>
</s3>
<s3>
<ptxt>See the table below for possible causes, and then inspect and correct the corresponding causes if necessary.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.22in"/>
<colspec colname="COL3" colwidth="1.39in"/>
<colspec colname="COL4" colwidth="3.44in"/>
<thead>
<row>
<entry align="center">
<ptxt>CO</ptxt>
</entry>
<entry align="center">
<ptxt>HC</ptxt>
</entry>
<entry align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry align="center">
<ptxt>Causes</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>High</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rough idling</ptxt>
</entry>
<entry>
<list1 type="ordered">
<item>
<ptxt>Faulty ignition:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Incorrect timing</ptxt>
</item>
<item>
<ptxt>Plugs are contaminated or shorted, or plug gaps are incorrect</ptxt>
</item>
</list2>
<item>
<ptxt>Incorrect valve clearance</ptxt>
</item>
<item>
<ptxt>Leaky intake and exhaust valves</ptxt>
</item>
<item>
<ptxt>Leaky cylinders</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Low</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>High</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rough idling </ptxt>
<ptxt>(Fluctuating HC reading)</ptxt>
</entry>
<entry>
<list1 type="ordered">
<item>
<ptxt>Vacuum leaks:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Ventilation hoses</ptxt>
</item>
<item>
<ptxt>Intake manifold</ptxt>
</item>
<item>
<ptxt>Throttle body</ptxt>
</item>
</list2>
<item>
<ptxt>Lean mixture causing misfire</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>High</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>High</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rough idle </ptxt>
<ptxt>(Black smoke from exhaust)</ptxt>
</entry>
<entry>
<list1 type="ordered">
<item>
<ptxt>Restricted air filter</ptxt>
</item>
<item>
<ptxt>Plugged PCV valve</ptxt>
</item>
<item>
<ptxt>Faulty SFI system:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Faulty pressure regulator</ptxt>
</item>
<item>
<ptxt>Defective engine coolant temperature sensor</ptxt>
</item>
<item>
<ptxt>Faulty mass air flow meter</ptxt>
</item>
<item>
<ptxt>Faulty ECM</ptxt>
</item>
<item>
<ptxt>Faulty injectors</ptxt>
</item>
<item>
<ptxt>Faulty throttle position sensor</ptxt>
</item>
</list2>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>