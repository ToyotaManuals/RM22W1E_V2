<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3OC_T00HF" variety="T00HF">
<name>ACTIVE HEIGHT CONTROL SUSPENSION</name>
<para id="RM000003BBJ00FX" category="C" type-id="803R0" name-id="SC0QM-07" from="201301" to="201308">
<dtccode>C1736</dtccode>
<dtcname>Front Suspension Control Valve RH Malfunction</dtcname>
<dtccode>C1737</dtccode>
<dtcname>Front Suspension Control Valve LH Malfunction</dtcname>
<subpara id="RM000003BBJ00FX_01" type-id="60" category="03" proc-id="RM22W0E___00009PQ00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>In order to decrease roll angle/pitch angle, the suspension control ECU turns the spring rate switching valve in the front suspension control valve off/on to change the fluid lines when the acceleration sensor or yaw rate sensor signal is input into the suspension control ECU. Normally, the spring rate switching valve is open.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.00in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<colspec colname="COL3" colwidth="2.55in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1736</ptxt>
</entry>
<entry valign="middle">
<ptxt>When either of the following is detected:</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the spring rate switch valve RH not activated, an open signal of the spring rate switch valve RH is detected for 1 second or more.</ptxt>
</item>
<item>
<ptxt>With the spring rate switch valve RH activated, a short signal of the spring rate switch valve RH is detected 8 times successively.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Front suspension control valve RH (Front spring rate switching valve RH)</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
<item>
<ptxt>No. 1 height control valve</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1737</ptxt>
</entry>
<entry valign="middle">
<ptxt>When either of the following is detected:</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the spring rate switch valve LH not activated, an open signal of the spring rate switch valve LH is detected for 1 second or more.</ptxt>
</item>
<item>
<ptxt>With the spring rate switch valve LH activated, a short signal of the spring rate switch valve LH is detected 8 times successively.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Front suspension control valve LH (Front spring rate switching valve LH)</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
<item>
<ptxt>No. 1 height control valve</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003BBJ00FX_02" type-id="32" category="03" proc-id="RM22W0E___00009PR00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C176953E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003BBJ00FX_03" type-id="51" category="05" proc-id="RM22W0E___00009PS00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before performing troubleshooting, inspect the connectors of related circuits.</ptxt>
</item>
<item>
<ptxt>If the suspension control ECU or height control sensor is replaced, the vehicle height offset calibration must be performed (See page <xref label="Seep01" href="RM000003AG300EX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Power is supplied to the front suspension control valve and No. 1 height control valve from the suspension control ECU. If a short occurs in the power supply circuit in the No. 1 height control valve, DTC C1736 and/or C1737 will be stored. In this case, DTC C1741, C1742, C1743, C1744, C1753, C1754 and/or C1755 will also be stored.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000003BBJ00FX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003BBJ00FX_04_0010" proc-id="RM22W0E___00009PW00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (FRONT SPRING RATE SWITCHING VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Select the Active Test mode on the intelligent tester.</ptxt>
</test1>
<test1>
<ptxt>Check the operation of the front spring rate switching valve solenoid when operating the solenoid with the intelligent tester. </ptxt>
<table pgwide="1">
<title>AHC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.89in"/>
<colspec colname="COL2" colwidth="2.03in"/>
<colspec colname="COL3" colwidth="1.30in"/>
<colspec colname="COL4" colwidth="1.86in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Gas Spring Switch Valve</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front spring rate switching valve RH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the FR Gas Spring Switch Valve item of the Active Test is operated, the FR Gas Spring Switch Valve item of the Data List changes to ON/OFF.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Gas Spring Switch Valve</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front spring rate switching valve LH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the FL Gas Spring Switch Valve item of the Active Test is operated, the FL Gas Spring Switch Valve item of the Data List changes to ON/OFF.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The front spring rate switching valve operates.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003BBJ00FX_04_0011" fin="false">OK</down>
<right ref="RM000003BBJ00FX_04_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0011" proc-id="RM22W0E___00009PX00000">
<testtitle>RECONFIRM DTC OUTPUT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001CU200KX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform a road test.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs. </ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.44in"/>
<colspec colname="COL2" colwidth="1.69in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003BBJ00FX_04_0005" fin="true">A</down>
<right ref="RM000003BBJ00FX_04_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0001" proc-id="RM22W0E___00009OS00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SUSPENSION CONTROL ECU - FRONT SUSPENSION CONTROL VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the N10*1 and/or N12*2 front suspension control valve connector.</ptxt>
<atten4>
<ptxt>*1: for RH</ptxt>
<ptxt>*2: for LH</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Disconnect the K36 ECU connector.</ptxt>
<figure>
<graphic graphicname="C174926E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.86in"/>
<colspec colname="COL2" colwidth="0.97in"/>
<colspec colname="COL3" colwidth="1.30in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K36-11 (CRFR) - N10-1 (CRFR)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-11 (CRFR) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-23 (CRFL) - N12-1 (CRFL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-23 (CRFL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-1 (SLB) - N10-2 (SLB)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-1 (SLB) - N12-2 (SLB)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003BBJ00FX_04_0006" fin="false">OK</down>
<right ref="RM000003BBJ00FX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0006" proc-id="RM22W0E___00009PU00000">
<testtitle>CHECK HARNESS AND CONNECTOR (BODY GROUND BETWEEN ECU - FRONT SUSPENSION CONTROL VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the K36 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the N10*1 and/or N12*2 front suspension control valve connector.</ptxt>
<atten4>
<ptxt>*1: for RH</ptxt>
<ptxt>*2: for LH</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C174826E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.86in"/>
<colspec colname="COL2" colwidth="0.97in"/>
<colspec colname="COL3" colwidth="1.30in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K36-1 (SLB) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003BBJ00FX_04_0002" fin="false">OK</down>
<right ref="RM000003BBJ00FX_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0002" proc-id="RM22W0E___00009PT00000">
<testtitle>INSPECT FRONT SUSPENSION CONTROL VALVE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the N10 and/or N12 front suspension control valve connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the K36 ECU connector.</ptxt>
<figure>
<graphic graphicname="C174826E03" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.00in"/>
<colspec colname="COL2" colwidth="0.92in"/>
<colspec colname="COL3" colwidth="1.21in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K36-11 (CRFR) - K36-1 (SLB)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>at 20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.58 to 5.02 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-23 (CRFL) - K36-1 (SLB)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>at 20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.58 to 5.02 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003BBJ00FX_04_0005" fin="true">OK</down>
<right ref="RM000003BBJ00FX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0007" proc-id="RM22W0E___00009PV00000">
<testtitle>CHECK HARNESS AND CONNECTOR (BODY GROUND BETWEEN ECU - NO. 1 HEIGHT CONTROL VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the N4 No. 1 height control valve connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the K36 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C174826E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.86in"/>
<colspec colname="COL2" colwidth="0.97in"/>
<colspec colname="COL3" colwidth="1.30in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K36-1 (SLB) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003BBJ00FX_04_0009" fin="false">OK</down>
<right ref="RM000003BBJ00FX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0009" proc-id="RM22W0E___00009ON00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 HEIGHT CONTROL VALVE - SUSPENSION CONTROL ECU)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the N4 No. 1 height control valve connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the K36 ECU connector.</ptxt>
<figure>
<graphic graphicname="C174880E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.88in"/>
<colspec colname="COL2" colwidth="1.08in"/>
<colspec colname="COL3" colwidth="1.17in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K36-1 (SLB) - N4-6 (SLB)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-7 (SLFR) - N4-4 (SLFR)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-7 (SLFR) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-8 (SLFL) - N4-3 (SLFL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-8 (SLFL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-9 (SLRR) - N4-1 (SLRR)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-9 (SLRR) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-10 (SLAC) - N4-7 (AHC-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-10 (SLAC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-22 (SLRL) - N4-2 (SLRL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-22 (SLRL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-29 (SLRG) - N4-5 (SLRG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-29 (SLRG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-30 (SLFG) - N4-8 (SLFG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-30 (SLFG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000003BBJ00FX_04_0008" fin="true">OK</down>
<right ref="RM000003BBJ00FX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0003">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0004">
<testtitle>REPLACE FRONT SUSPENSION CONTROL VALVE<xref label="Seep01" href="RM000003A0200KX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0005">
<testtitle>REPLACE SUSPENSION CONTROL ECU<xref label="Seep01" href="RM000003A0D00DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0008">
<testtitle>REPLACE NO. 1 HEIGHT CONTROL VALVE<xref label="Seep01" href="RM0000039ZN00AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003BBJ00FX_04_0012">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>