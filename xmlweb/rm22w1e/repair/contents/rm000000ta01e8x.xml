<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000000TA01E8X" category="C" type-id="302IA" name-id="ES11HG-003" from="201308">
<dtccode>P0617</dtccode>
<dtcname>Starter Relay Circuit High</dtcname>
<subpara id="RM000000TA01E8X_08" type-id="60" category="03" proc-id="RM22W0E___00000JK00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>While the engine is being cranked, the positive battery voltage is applied to terminal STA of the ECM. If the ECM detects the Starter Control (STA) signal while the vehicle is being driven, it determines that there is a malfunction in the STA circuit. The ECM then illuminates the MIL and stores the DTC.</ptxt>
<ptxt>This monitor runs when the vehicle is driven at 20 km/h (12.4 mph) for over 20 seconds.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="3.19in"/>
<colspec colname="COL3" colwidth="3.18in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0617</ptxt>
</entry>
<entry valign="middle">
<ptxt>When conditions (a), (b) and (c) are met, a positive (+B) battery voltage of 10.5 V or higher is applied to the ECM for 20 seconds (1 trip detection logic):</ptxt>
<list1 type="nonmark">
<item>
<ptxt>(a) The vehicle speed is 20 km/h (12.4 mph) or more.</ptxt>
</item>
<item>
<ptxt>(b) The engine speed is 1000 rpm or more.</ptxt>
</item>
<item>
<ptxt>(c) The STA signal is ON.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Park/Neutral Position (PNP) switch</ptxt>
</item>
<item>
<ptxt>Clutch start switch</ptxt>
</item>
<item>
<ptxt>ST relay circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000TA01E8X_05" type-id="32" category="03" proc-id="RM22W0E___00000JD00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A180644E38" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="A180644E39" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="A264511E05" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="A264511E06" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TA01E8X_06" type-id="51" category="05" proc-id="RM22W0E___00000JE00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>The following troubleshooting flowchart is based on the premise that the engine can be cranked normally.</ptxt>
<ptxt>If the engine does not crank, proceed to Problem Symptoms Table (See page <xref label="Seep01" href="RM000000PDG0VPX"/>).</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TA01E8X_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TA01E8X_07_0001" proc-id="RM22W0E___00000JF00001">
<testtitle>READ VALUE USING GTS (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Check the value displayed on the GTS when the ignition switch is turned to the ON and START positions.</ptxt>
<spec>
<title>Standard</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Ignition Switch Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Close</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>START</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for Automatic transmission)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for Manual transmission)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01E8X_07_0012" fin="true">A</down>
<right ref="RM000000TA01E8X_07_0002" fin="false">B</right>
<right ref="RM000000TA01E8X_07_0004" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0002" proc-id="RM22W0E___00000JG00001">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the park/neutral position switch assembly (See page <xref label="Seep01" href="RM000002BKX03LX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA01E8X_07_0011" fin="false">OK</down>
<right ref="RM000000TA01E8X_07_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (PARK/NEUTRAL POSITION SWITCH ASSEMBLY - ECM)</testtitle>
<res>
<down ref="RM000000TA01E8X_07_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0017" proc-id="RM22W0E___00000JI00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK185X"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a vehicle speed of more than 24 km/h (15 mph) for more than 20 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0617 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01E8X_07_0019" fin="true">A</down>
<right ref="RM000000TA01E8X_07_0014" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0004" proc-id="RM22W0E___00000JH00001">
<testtitle>INSPECT CLUTCH START SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the clutch start switch assembly (for LHD) (See page <xref label="Seep01" href="RM0000032S3008X"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the clutch start switch assembly (for RHD) (See page <xref label="Seep02" href="RM000003D8H002X"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01E8X_07_0020" fin="false">A</down>
<right ref="RM000000TA01E8X_07_0015" fin="true">B</right>
<right ref="RM000000TA01E8X_07_0023" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0020">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (CLUTCH START SWITCH ASSEMBLY - ECM)</testtitle>
<res>
<down ref="RM000000TA01E8X_07_0021" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0021" proc-id="RM22W0E___00000JJ00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK185X"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a vehicle speed of more than 24 km/h (15 mph) for more than 20 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0617 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01E8X_07_0022" fin="true">A</down>
<right ref="RM000000TA01E8X_07_0018" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0013">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC075X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0014">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0015">
<testtitle>REPLACE CLUTCH START SWITCH ASSEMBLY<xref label="Seep01" href="RM0000032S5004X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0018">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0012">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ107X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0019">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292037X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0022">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292037X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TA01E8X_07_0023">
<testtitle>REPLACE CLUTCH START SWITCH ASSEMBLY<xref label="Seep01" href="RM000003D8J002X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>