<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LW_T00EZ" variety="T00EZ">
<name>CRUISE CONTROL SYSTEM</name>
<para id="RM000002U3L02RX" category="C" type-id="8034O" name-id="CC39X-03" from="201301" to="201308">
<dtccode>P0575</dtccode>
<dtcname>Cruise Control Input Circuit</dtcname>
<dtccode>P0607</dtccode>
<dtcname>Control Module Performance</dtcname>
<subpara id="RM000002U3L02RX_01" type-id="60" category="03" proc-id="RM22W0E___000079C00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates the internal abnormalities of the ECM.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0575</ptxt>
</entry>
<entry>
<ptxt>When either of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>STP signals input to the ECM supervisory CPU and control ECU are different for 0.15 seconds or more.</ptxt>
</item>
<item>
<ptxt>0.4 seconds have passed after cruise cancel input signal (STP input) is input to the ECM.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>ECM (for 1GR-FE, 1UR-FE, 3UR-FE)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0607</ptxt>
</entry>
<entry>
<ptxt>When the cruise control main switch is turned off, the ECM recognizes that the cruise control system is operating.</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM (for 1VD-FTV, w/o DPF)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The ECM receives signals from each sensor to control all functions of the cruise control system. When this trouble code is stored, fail-safe remains on until the ignition switch is turned off.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002U3L02RX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002U3L02RX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002U3L02RX_03_0001" proc-id="RM22W0E___000079D00000">
<testtitle>CHECK DTC (CRUISE CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Powertrain / Cruise Control / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002R6B01QX"/>).</ptxt>
</test1>
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep02" href="RM000002R6B01QX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(DTC is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(DTC P0575 is output for 1GR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(DTC P0575 is output for 1UR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(DTC P0575 is output for 3UR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(DTC P0607 is output for 1VD-FTV, w/o DPF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002U3L02RX_03_0002" fin="true">A</down>
<right ref="RM000002U3L02RX_03_0008" fin="true">B</right>
<right ref="RM000002U3L02RX_03_0003" fin="true">C</right>
<right ref="RM000002U3L02RX_03_0006" fin="true">D</right>
<right ref="RM000002U3L02RX_03_0009" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000002U3L02RX_03_0002">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U3L02RX_03_0008">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U3L02RX_03_0003">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U3L02RX_03_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U3L02RX_03_0009">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202SX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>