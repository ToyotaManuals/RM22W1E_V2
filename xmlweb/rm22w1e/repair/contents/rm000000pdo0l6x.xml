<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000000PDO0L6X" category="U" type-id="303FP" name-id="ES00D9-595" from="201301">
<name>FAIL-SAFE CHART</name>
<subpara id="RM000000PDO0L6X_z0" proc-id="RM22W0E___00000O900000">
<content5 releasenbr="1">
<ptxt>If any of the following DTCs are stored, the ECM enters fail-safe mode to allow the vehicle to be driven temporarily.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-Safe Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-Safe Deactivation Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0011, P0021, P0015 and P0025</ptxt>
</entry>
<entry valign="middle">
<ptxt>VVT system</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Idle up (Control of combustion worsening).</ptxt>
</item>
<item>
<ptxt>Stopping fuel-cut control.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0031, P0032, P0051 and P0052</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Fuel Ratio Sensor Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>The ECM turns off the air fuel ratio sensor heater.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0037, P0038, P0057 and P0058</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated Oxygen Sensor Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>The ECM turns off the heated oxygen sensor heater.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0102 and P0103</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mass Air Flow Meter</ptxt>
</entry>
<entry valign="middle">
<ptxt>The ECM calculates the ignition timing according to the engine speed and throttle valve position.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0112 and P0113</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Air Temperature Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>The ECM estimates the intake air temperature to be 20°C (68°F).</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0115, P0117 and P0118</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Coolant Temperature Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>The ECM estimates the engine coolant temperature to be 80°C (176°F).</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0120, P0121, P0122, P0123, P0220, P0222, P0223, P0604, P0606, P060A, P060B, P060D, P060E, P0657, P1607, P2102, P2103, P2111, P2112, P2118, P2119 and P2135</ptxt>
</entry>
<entry valign="middle">
<ptxt>Electronic Throttle Control System (ETCS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The ECM cuts off the throttle actuator current and the throttle valve returns to a throttle opening angle of 7° by the return spring.</ptxt>
<ptxt>The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel cut) and ignition timing in accordance with the accelerator pedal position to allow the vehicle to continue at a minimal speed.*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected and then ignition switch turned off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0300, P0301, P0302, P0303, P0304, P0305 and P0306*2</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Fuel Injector</ptxt>
</item>
<item>
<ptxt>Electronic Throttle Control System (ETCS)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When misfire occurs, a fuel-cut is performed to prevent malfunctions due to the catalyst overheating.</ptxt>
<list1 type="unordered">
<item>
<ptxt>During normal load and at a normal engine speed (MIL is blinking):</ptxt>
<ptxt>- Fuel-cut is performed on the malfunctioning cylinder.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>During high load and at a high engine speed (MIL is blinking):</ptxt>
<ptxt>- Throttle valve opening angle control is performed.</ptxt>
<ptxt>- Fuel cut for all cylinders or for the malfunctioning cylinder is performed.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected and then ignition switch turned off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0327, P0328, P0332 and P0333</ptxt>
</entry>
<entry valign="middle">
<ptxt>Knock Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>The ECM sets the ignition timing to maximum retard.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0351 to P0356</ptxt>
</entry>
<entry valign="middle">
<ptxt>Igniter</ptxt>
</entry>
<entry valign="middle">
<ptxt>The ECM cuts the fuel.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0504</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop Light Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>The accelerator pedal opening angle is fixed when there is a accelerator pedal malfunction or history of a malfunction.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2120, P2121, P2122, P2123, P2125, P2127, P2128 and P2138</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accelerator Pedal Position Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>The accelerator pedal position sensor has 2 sensor circuits: Main and Sub.</ptxt>
<ptxt>If either circuit malfunctions, the ECM limits the engine output.</ptxt>
<ptxt>If both of the circuits malfunction, the ECM regards the accelerator pedal as being released. As a result, the throttle valve is closed and the engine idles.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected and then ignition switch turned off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2237, P2238, P2239, P2240, P2241, P2242, P2252, P2253, P2255 and P2256</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Fuel Ratio Sensor</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air-fuel ratio feedback control is stopped.</ptxt>
</item>
<item>
<ptxt>Current to the air fuel ratio sensor heater is cut.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected and then ignition switch turned off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2432, P2433, P2437, P2438, P2440, P2442 and P2445</ptxt>
</entry>
<entry valign="middle">
<ptxt>Secondary Air Injection System</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle valve opening angle control is performed.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pass condition detected</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: The vehicle can be driven slowly when the accelerator pedal is depressed firmly and slowly. If the accelerator pedal is depressed quickly, the vehicle may speed up and slow down erratically.</ptxt>
</item>

<item>
<ptxt>*2: Misfire related fail-safe operations occur when catalyst overheat malfunctions occur.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>