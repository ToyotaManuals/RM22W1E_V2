<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000003CPO00JX" category="J" type-id="803TQ" name-id="ACILK-01" from="201301" to="201308">
<dtccode/>
<dtcname>Cooling Box Sensor Circuit</dtcname>
<subpara id="RM000003CPO00JX_01" type-id="60" category="03" proc-id="RM22W0E___0000H8W00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The No. 3 cooler thermistor is installed on the evaporator in the cooling box unit to detect the cooled air temperature that has passed through the evaporator and control the cooling box. It sends appropriate signals to the air conditioning amplifier assembly (for cool box). The resistance of the No. 3 cooler thermistor changes in accordance with the cooled air temperature that has passed through the evaporator. As the temperature decreases, the resistance increases. As the temperature increases, the resistance decreases.</ptxt>
<ptxt>The air conditioning amplifier assembly (for cool box) applies voltage (12 V) to the No. 3 cooler thermistor and reads voltage changes as the resistance of the No. 3 cooler thermistor changes.</ptxt>
</content5>
</subpara>
<subpara id="RM000003CPO00JX_02" type-id="32" category="03" proc-id="RM22W0E___0000H8X00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E160088E04" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003CPO00JX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003CPO00JX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003CPO00JX_04_0001" proc-id="RM22W0E___0000H8Y00000">
<testtitle>INSPECT FUSE (A/C IG, ECU-B1)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the A/C IG fuse from the cowl side junction block LH.</ptxt>
</test1>
<test1>
<ptxt>Remove the ECU-B1 fuse from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A/C IG fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ECU-B1 fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003CPO00JX_04_0003" fin="false">OK</down>
<right ref="RM000003CPO00JX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CPO00JX_04_0003" proc-id="RM22W0E___0000H8Z00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 COOLER WIRING HARNESS - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 cooler wiring harness sub-assembly connector.</ptxt>
<figure>
<graphic graphicname="E158385E20" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>4 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003CPO00JX_04_0004" fin="false">OK</down>
<right ref="RM000003CPO00JX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CPO00JX_04_0004" proc-id="RM22W0E___0000H9000000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 COOLER WIRING HARNESS - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 cooler wiring harness sub-assembly connector.</ptxt>
<figure>
<graphic graphicname="E158385E21" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>6 (L) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003CPO00JX_04_0005" fin="false">OK</down>
<right ref="RM000003CPO00JX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CPO00JX_04_0005" proc-id="RM22W0E___0000H9100000">
<testtitle>CHECK NO. 1 COOLER WIRING HARNESS SUB-ASSEMBLY (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the No. 1 cooler wiring harness sub-assembly with a normal one and check that the condition returns to normal.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Same problem does not occur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003CPO00JX_04_0010" fin="true">OK</down>
<right ref="RM000003CPO00JX_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CPO00JX_04_0007" proc-id="RM22W0E___0000H9200000">
<testtitle>CHECK NO. 3 COOLER THERMISTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the No. 3 cooler thermistor (See page <xref label="Seep01" href="RM000003B6T00JX"/>).</ptxt>
<figure>
<graphic graphicname="E161018E02" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.15in"/>
<colspec colname="COL2" colwidth="1.54in"/>
<colspec colname="COL3" colwidth="1.44in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="8" valign="middle" align="center">
<ptxt>1 (+) - 2 (-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-10°C (14°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>7.30 to 9.10 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>-5°C (23°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.65 to 6.95 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>0°C (32°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.40 to 5.35 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5°C (41°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.40 to 4.15 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>10°C (50°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.70 to 3.25 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>15°C (59°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.14 to 2.58 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.71 to 2.05 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>25°C (77°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.38 to 1.64 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>30°C (86°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.11 to 1.32 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Even slightly touching the sensor may change the resistance value. Be sure to hold the connector of the sensor.</ptxt>
</item>
<item>
<ptxt>When measuring, the sensor temperature must be the same as the ambient temperature.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>As the temperature increases, the resistance decreases (see the graph).</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003CPO00JX_04_0012" fin="true">OK</down>
<right ref="RM000003CPO00JX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CPO00JX_04_0008">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000003CPO00JX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003CPO00JX_04_0013">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003CPO00JX_04_0010">
<testtitle>REPLACE NO. 1 COOLER WIRING HARNESS SUB-ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000003CPO00JX_04_0011">
<testtitle>REPLACE NO. 3 COOLER THERMISTOR<xref label="Seep01" href="RM000003B6T00JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003CPO00JX_04_0012">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY (for Cool Box)<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>