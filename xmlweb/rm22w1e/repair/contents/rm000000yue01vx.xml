<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12037_S001R" variety="S001R">
<name>VARIABLE GEAR RATIO STEERING</name>
<ttl id="12037_S001R_7C3Q8_T00JB" variety="T00JB">
<name>VARIABLE GEAR RATIO STEERING SYSTEM</name>
<para id="RM000000YUE01VX" category="C" type-id="800V9" name-id="VG00C-05" from="201301" to="201308">
<dtccode>C15C7/78</dtccode>
<dtcname>DC Motor Power Source Voltage Malfunction</dtcname>
<dtccode>C15C8/79</dtccode>
<dtcname>Power Supply Relay Failure</dtcname>
<subpara id="RM000000YUE01VX_01" type-id="60" category="03" proc-id="RM22W0E___0000BA200000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The steering control ECU monitors PIG (motor power source voltage) and detects a malfunction in the power supply relay (located inside the steering control ECU).</ptxt>
<ptxt>If the steering control ECU detects a malfunction, it will turn on the master warning light, store these DTCs, and stop VGRS operation.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.22in"/>
<colspec colname="COL2" colwidth="3.36in"/>
<colspec colname="COL3" colwidth="2.50in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C15C7/78</ptxt>
</entry>
<entry valign="middle">
<ptxt>The steering control ECU detects that the IG terminal voltage is between 9 and 16 V, and the PIG terminal voltage is below 7 V or higher than 18.5 V for 2.4 seconds.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Steering control ECU</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C15C8/79</ptxt>
</entry>
<entry valign="middle">
<ptxt>The steering control ECU detects a malfunction in the power source relay or an open circuit.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Steering control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000YUE01VX_02" type-id="32" category="03" proc-id="RM22W0E___0000BA300000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C167706E03" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000YUE01VX_03" type-id="51" category="05" proc-id="RM22W0E___0000BA400000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>The fault may be intermittent. Check the harnesses and connectors thoroughly and retest.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000YUE01VX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YUE01VX_04_0007" proc-id="RM22W0E___0000BA600000">
<testtitle>CHECK HARNESS AND CONNECTOR (POWER SOURCE CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Record and clear the DTCs (See page <xref label="Seep01" href="RM000000XO701UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C149134E13" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.63in"/>
<colspec colname="COL2" colwidth="1.26in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E77-7 (PGND) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E78-8 (PGD2) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.64in"/>
<colspec colname="COLSPEC0" colwidth="1.25in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E77-8 (PIG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YUE01VX_04_0002" fin="false">OK</down>
<right ref="RM000000YUE01VX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YUE01VX_04_0002" proc-id="RM22W0E___0000BA500000">
<testtitle>RECONFIRM DTC OUTPUT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000000XO701UX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.79in"/>
<colspec colname="COL2" colwidth="1.34in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C15C7/78 or C15C8/79 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C15C7/78 or C15C8/79 is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C15C7/78 or C15C8/79 is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YUE01VX_04_0003" fin="true">A</down>
<right ref="RM000000YUE01VX_04_0006" fin="true">B</right>
<right ref="RM000000YUE01VX_04_0009" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000YUE01VX_04_0003">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUE01VX_04_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YUE01VX_04_0006">
<testtitle>REPLACE STEERING CONTROL ECU<xref label="Seep01" href="RM000003DZD00EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUE01VX_04_0009">
<testtitle>REPLACE STEERING CONTROL ECU<xref label="Seep01" href="RM000003EFD004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>