<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000187N070X" category="C" type-id="302AY" name-id="ESRAG-04" from="201301" to="201308">
<dtccode>P0122</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "A" Circuit Low Input</dtcname>
<dtccode>P0123</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "A" Circuit High Input</dtcname>
<dtccode>P0222</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "B" Circuit Low Input</dtcname>
<dtccode>P0223</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "B" Circuit High Input</dtcname>
<subpara id="RM00000187N070X_01" type-id="60" category="03" proc-id="RM22W0E___00002JP00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The throttle position sensor is mounted on the diesel throttle body and detects the opening angle of the throttle valve. This sensor is an electronic sensor and uses Hall-effect elements.</ptxt>
<ptxt>When the throttle valve is fully closed, a voltage of approximately 0.7 V is applied to terminal VLU of the ECM. The voltage applied to terminal VLU of the ECM increases in proportion to the opening angle of the throttle valve and becomes approximately 3.5 to 4.0 V when the throttle valve is fully opened.</ptxt>
<ptxt>The ECM determines the vehicle driving conditions from the signals input to its VLU terminal. The data is one of the inputs used for EGR control, etc.</ptxt>
<figure>
<graphic graphicname="A128639E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>P0122</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 3 seconds</ptxt>
</entry>
<entry>
<ptxt>Throttle position sensor (for Bank 1) output (VLU) is less than 0.2 V for 3 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Throttle position sensor (for Bank 1)</ptxt>
</item>
<item>
<ptxt>Open or short in VLU circuit</ptxt>
</item>
<item>
<ptxt>Open in VC circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0123</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 3 seconds</ptxt>
</entry>
<entry>
<ptxt>Throttle position sensor (for Bank 1) output (VLU) is more than 4.8 V for 3 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Throttle position sensor (for Bank 1)</ptxt>
</item>
<item>
<ptxt>Open in E2 circuit</ptxt>
</item>
<item>
<ptxt>VC and VLU circuits are short-circuited</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0222</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 3 seconds</ptxt>
</entry>
<entry>
<ptxt>Throttle position sensor (for Bank 2) output (VLU2) is less than 0.2 V for 3 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Throttle position sensor (for Bank 2)</ptxt>
</item>
<item>
<ptxt>Open or short in VLU2 circuit</ptxt>
</item>
<item>
<ptxt>Open in VC circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0223</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 3 seconds</ptxt>
</entry>
<entry>
<ptxt>Throttle position sensor (for Bank 2) output (VLU2) is more than 4.8 V for 3 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Throttle position sensor (for Bank 2)</ptxt>
</item>
<item>
<ptxt>Open in E2 circuit</ptxt>
</item>
<item>
<ptxt>VC and VLU2 circuits are short-circuited</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTC P0122, P0123, P0222 and/or P0223 is stored, the following symptoms may appear:</ptxt>
</atten4>
<list1 type="nonmark">
<item>
<ptxt>- Intake booming noise</ptxt>
</item>
<item>
<ptxt>- Black smoke</ptxt>
</item>
<item>
<ptxt>- Lack of power</ptxt>
</item>
<item>
<ptxt>- Vibration at engine stop</ptxt>
</item>
<item>
<ptxt>- Turbo overspeed, turbo damage</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM00000187N070X_02" type-id="64" category="03" proc-id="RM22W0E___00002JQ00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the output voltage of the throttle position sensor deviates from the normal operating range (between 0.2 V and 4.8 V) for more than 3 seconds, the ECM interprets this as a malfunction of the sensor circuit and illuminates the MIL.</ptxt>
<figure>
<graphic graphicname="C129051E04" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000187N070X_03" type-id="32" category="03" proc-id="RM22W0E___00002JR00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A165736E03" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000187N070X_04" type-id="51" category="05" proc-id="RM22W0E___00002JS00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07YX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06NX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187N070X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187N070X_05_0001" proc-id="RM22W0E___00002JT00000">
<testtitle>CHECK HARNESS AND CONNECTOR (THROTTLE POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A165991E06" width="2.775699831in" height="5.787629434in"/>
</figure>
<test1>
<ptxt>Disconnect the throttle position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for Bank 1 (LHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C83-1 (VC) - C45-91 (VCVL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C83-3 (VTA) - C45-93 (VLU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C83-2 (E2) - C45-92 (EVLU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2 (LHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C84-1 (VC) - C45-91 (VCVL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C84-3 (VTA2) - C45-103 (VLU2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C84-2 (E2) - C45-92 (EVLU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 1 (RHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C83-1 (VC) - C46-91 (VCVL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C83-3 (VTA) - C46-93 (VLU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C83-2 (E2) - C46-92 (EVLU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2 (RHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C84-1 (VC) - C46-91 (VCVL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C84-3 (VTA2) - C46-103 (VLU2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C84-2 (E2) - C46-92 (EVLU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for Bank 1 (LHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C83-1 (VC) or C45-91 (VCVL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C83-3 (VTA) or C45-93 (VLU) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C83-2 (E2) or C45-92 (EVLU) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2 (LHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C84-1 (VC) or C45-91 (VCVL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C84-3 (VTA2) or C45-103 (VLU2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C84-2 (E2) or C45-92 (EVLU) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 1 (RHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C83-1 (VC) or C46-91 (VCVL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C83-3 (VTA) or C46-93 (VLU) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C83-2 (E2) or C46-92 (EVLU) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2 (RHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C84-1 (VC) or C46-91 (VCVL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C84-3 (VTA2) or C46-103 (VLU2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C84-2 (E2) or C46-92 (EVLU) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187N070X_05_0002" fin="false">OK</down>
<right ref="RM00000187N070X_05_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187N070X_05_0002" proc-id="RM22W0E___00002JU00000">
<testtitle>CHECK ECM TERMINAL VOLTAGE (VC TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle position sensor connector.</ptxt>
<figure>
<graphic graphicname="A165737E03" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<title>for Bank 1</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C83-1 (VC) - C83-2 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C84-1 (VC) - C84-2 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187N070X_05_0003" fin="false">OK</down>
<right ref="RM00000187N070X_05_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187N070X_05_0003" proc-id="RM22W0E___00002JV00000">
<testtitle>REPLACE DIESEL THROTTLE BODY ASSEMBLY (for Bank 1 or Bank 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the diesel throttle body assembly (for Bank 1 or Bank 2) (See page <xref label="Seep01" href="RM00000316V005X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187N070X_05_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187N070X_05_0004" proc-id="RM22W0E___00002JW00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / Actual Throttle position or Actual Throttle position #2.</ptxt>
</test1>
<test1>
<ptxt>Check the movement of the throttle valve when idling the engine after starting it from ignition switch ON.</ptxt>
<table pgwide="1">
<title>OK</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>-5 to 5%</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 90%</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<right ref="RM00000187N070X_05_0008" fin="true">OK</right>
<right ref="RM00000187N070X_05_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187N070X_05_0005" proc-id="RM22W0E___00002JX00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187N070X_05_0007" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187N070X_05_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<res>
<down ref="RM00000187N070X_05_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187N070X_05_0007" proc-id="RM22W0E___00002JY00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 3 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187N070X_05_0008" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187N070X_05_0008">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>