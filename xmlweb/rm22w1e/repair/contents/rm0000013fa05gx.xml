<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MK_T00FN" variety="T00FN">
<name>OIL PUMP</name>
<para id="RM0000013FA05GX" category="A" type-id="8000E" name-id="AT2CI-03" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM0000013FA05GX_01" type-id="01" category="01">
<s-1 id="RM0000013FA05GX_01_0001" proc-id="RM22W0E___000080F00000">
<ptxt>INSTALL FRONT OIL PUMP OIL SEAL</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161807E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using SST and a hammer, tap in a new oil seal.</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00650</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<atten4>
<ptxt>The oil seal end should be flush with the outer edge of the pump body.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Coat the lip of the oil seal with MP grease.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05GX_01_0002" proc-id="RM22W0E___000080G00000">
<ptxt>FIX FRONT OIL PUMP BODY SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the oil pump body on the torque converter clutch.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05GX_01_0003" proc-id="RM22W0E___000080H00000">
<ptxt>INSTALL FRONT OIL PUMP DRIVEN GEAR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161799" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Coat the driven gear with ATF.</ptxt>
</s2>
<s2>
<ptxt>Install the driven gear to the oil pump body.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05GX_01_0004" proc-id="RM22W0E___000080I00000">
<ptxt>INSTALL FRONT OIL PUMP DRIVE GEAR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161798" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Coat the drive gear with ATF.</ptxt>
</s2>
<s2>
<ptxt>Install the drive gear to the oil pump body.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05GX_01_0005" proc-id="RM22W0E___000080J00000">
<ptxt>INSTALL FRONT OIL PUMP BODY O-RING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161797E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Coat a new O-ring with ATF and install it to the oil pump body.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05GX_01_0006" proc-id="RM22W0E___000080K00000">
<ptxt>INSTALL STATOR SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161796E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Align the stator shaft with the bolt holes.</ptxt>
</s2>
<s2>
<ptxt>Install the 17 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>112</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05GX_01_0007" proc-id="RM22W0E___000080L00000">
<ptxt>INSTALL CLUTCH DRUM OIL SEAL RING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161795E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Coat 3 new oil seal rings with ATF.</ptxt>
</s2>
<s2>
<ptxt>Squeeze the ends of the 3 oil seal rings together so that the overlap distance is 8 mm (0.314 in.) or less, and then install them to the starter shaft groove.</ptxt>
<atten3>
<ptxt>Do not excessively widen the rings.</ptxt>
</atten3>
<atten4>
<ptxt>After installing the oil seal rings, check that they rotate smoothly.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013FA05GX_01_0010" proc-id="RM22W0E___000080500000">
<ptxt>INSPECT OIL PUMP DRIVE GEAR ROTATION
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161806" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Place the oil pump body on the torque converter clutch.</ptxt>
</s2>
<s2>
<ptxt>Check that the drive gear rotates smoothly.</ptxt>
</s2>
<s2>
<ptxt>Remove the oil pump assembly from the torque converter.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000013FA05GX_01_0009" proc-id="RM22W0E___000080M00000">
<ptxt>INSTALL AUTOMATIC TRANSMISSION CASE O-RING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C162762E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Coat a new O-ring with ATF and install it to the oil pump.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>