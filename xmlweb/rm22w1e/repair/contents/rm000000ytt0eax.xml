<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TW_T00MZ" variety="T00MZ">
<name>AIRBAG SYSTEM</name>
<para id="RM000000YTT0EAX" category="C" type-id="303O3" name-id="RSARM-03" from="201301" to="201308">
<dtccode>B1860/64</dtccode>
<dtcname>Short in Driver Side Knee Airbag Squib Circuit</dtcname>
<dtccode>B1861/64</dtccode>
<dtcname>Open in Driver Side Knee Airbag Squib Circuit</dtcname>
<dtccode>B1862/64</dtccode>
<dtcname>Short to GND in Driver Side Knee Airbag Squib Circuit</dtcname>
<dtccode>B1863/64</dtccode>
<dtcname>Short to B+ in Driver Side Knee Airbag Squib Circuit</dtcname>
<subpara id="RM000000YTT0EAX_01" type-id="60" category="03" proc-id="RM22W0E___0000FA700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The driver side knee airbag squib circuit consists of the center airbag sensor and the driver side knee airbag.</ptxt>
<ptxt>This circuit instructs the SRS to deploy when deployment conditions are met.</ptxt>
<ptxt>These DTCs are stored when a malfunction is detected in the driver side knee airbag squib circuit.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1860/64</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives a line short circuit signal 5 times in the driver side knee airbag squib circuit during the primary check.</ptxt>
</item>
<item>
<ptxt>A driver side knee airbag squib malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>Driver side knee airbag assembly (Driver side knee squib)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B1861/64</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives an open circuit signal in the driver side knee airbag squib circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A driver side knee airbag squib malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>Driver side knee airbag assembly (Driver side knee squib)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B1862/64</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The airbag sensor center receives a short circuit to ground signal in the knee airbag (Driver side squib) circuit for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>A driver side knee airbag squib malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>Driver side knee airbag assembly (Driver side knee squib)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B1863/64</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The airbag sensor center receives a short circuit to B+ signal in the knee airbag (Driver side squib) circuit for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>A driver side knee airbag squib malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>Driver side knee airbag assembly (Driver side knee squib)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Vehicles that are not equipped with knee airbag still contain knee airbag circuits and use substitution parts in replace of the knee airbag. If a malfunction occurs in this knee airbag circuit, DTC B1860/64, B1861/64, B1862/64 or B1863/64 is stored.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000YTT0EAX_02" type-id="32" category="03" proc-id="RM22W0E___0000FA800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C146265E07" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000YTT0EAX_03" type-id="51" category="05" proc-id="RM22W0E___0000FA900000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="2">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep04" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep03" href="RM000003C32005X"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>To perform the simulation method, enter the check mode (signal check) with the intelligent tester (See page <xref label="Seep01" href="RM000000XFF0E9X"/>), and then wiggle each connector of the airbag system or drive the vehicle on various type of road (See page <xref label="Seep02" href="RM000000XFD0IZX"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000YTT0EAX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YTT0EAX_04_0001" proc-id="RM22W0E___0000FAA00000">
<testtitle>CHECK DRIVER SIDE KNEE AIRBAG (DRIVER SIDE KNEE SQUIB)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C146266E10" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the connector from the driver side knee airbag.</ptxt>
</test1>
<test1>
<ptxt>Connect the white wire side of SST (resistance: 2.1 Ω) to connector C.</ptxt>
<atten2>
<ptxt>Never connect the tester to the driver side knee airbag (driver side knee squib) for measurement, as this may lead to a serious injury due to airbag deployment.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not forcibly insert SST into the terminals of the connector when connecting SST.</ptxt>
</item>
<item>
<ptxt>Insert SST straight into the terminals of the connector.</ptxt>
</item>
</list1>
</atten3>
</test1>
<sst>
<sstitem>
<s-number>09843-18061</s-number>
</sstitem>
</sst>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1860, B1861, B1862 or B1863 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1860, B1861, B1862 and B1863 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000YTT0EAX_04_0006" fin="true">OK</down>
<right ref="RM000000YTT0EAX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTT0EAX_04_0002" proc-id="RM22W0E___0000FAB00000">
<testtitle>CHECK CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Disconnect SST from connector C.</ptxt>
</test1>
<test1>
<ptxt>Check that the instrument panel wire connector (on the driver side knee airbag side) is not damaged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The lock button is not disengaged, and the claw of the lock is not deformed or damaged.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YTT0EAX_04_0003" fin="false">OK</down>
<right ref="RM000000YTT0EAX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTT0EAX_04_0003" proc-id="RM22W0E___0000FAC00000">
<testtitle>CHECK INSTRUMENT PANEL WIRE (DRIVER SIDE KNEE AIRBAG CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor.</ptxt>
<figure>
<graphic graphicname="C146267E11" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" align="center" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E24-1 (DK+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E24-2 (DK-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" align="center" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E24-1 (DK+) - E24-2 (DK-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Release the activation prevention mechanism built into connector B (See page <xref label="Seep01" href="RM000000XFD0IZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" align="center" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E24-1 (DK+) - E24-2 (DK-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E24-1 (DK+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E24-2 (DK-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YTT0EAX_04_0004" fin="false">OK</down>
<right ref="RM000000YTT0EAX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTT0EAX_04_0004" proc-id="RM22W0E___0000FAD00000">
<testtitle>CHECK CENTER AIRBAG SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the connectors to the driver side knee airbag and the center airbag sensor.</ptxt>
<figure>
<graphic graphicname="C146268E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1860, B1861, B1862 or B1863 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1860, B1861, B1862 and B1863 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000YTT0EAX_04_0005" fin="true">OK</down>
<right ref="RM000000YTT0EAX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTT0EAX_04_0005">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0IZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTT0EAX_04_0006">
<testtitle>REPLACE DRIVER SIDE KNEE AIRBAG ASSEMBLY<xref label="Seep01" href="RM000002O91016X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTT0EAX_04_0007">
<testtitle>REPLACE INSTRUMENT PANEL WIRE</testtitle>
</testgrp>
<testgrp id="RM000000YTT0EAX_04_0008">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000002Y2N00UX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>