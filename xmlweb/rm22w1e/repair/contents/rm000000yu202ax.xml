<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12037_S001R" variety="S001R">
<name>VARIABLE GEAR RATIO STEERING</name>
<ttl id="12037_S001R_7C3Q8_T00JB" variety="T00JB">
<name>VARIABLE GEAR RATIO STEERING SYSTEM</name>
<para id="RM000000YU202AX" category="C" type-id="800UQ" name-id="VG05D-04" from="201308">
<dtccode>C1595/55</dtccode>
<dtcname>Lost Communication with Steering Angle Sensor Module</dtcname>
<dtccode>C15C4/68</dtccode>
<dtcname>Steering Angle Signal (Test Mode DTC)</dtcname>
<subpara id="RM000000YU202AX_01" type-id="60" category="03" proc-id="RM22W0E___0000B8O00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Signal transmission between the steering control ECU and steering angle sensor is performed via serial communication.</ptxt>
<ptxt>If an error occurs between them in serial communication, the ECU will store DTC C1595/55.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.22in"/>
<colspec colname="COL2" colwidth="2.92in"/>
<colspec colname="COL3" colwidth="2.94in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1595/55</ptxt>
</entry>
<entry valign="middle">
<ptxt>The steering control ECU detects an error in serial communication between the ECU and steering angle sensor.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Steering angle sensor</ptxt>
</item>
<item>
<ptxt>Steering control ECU</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C15C4/68</ptxt>
</entry>
<entry valign="middle">
<ptxt>A steering signal indicating a tire angle of 36° or more (to the left or right) is input after entering test mode.*</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Steering angle sensor</ptxt>
</item>
<item>
<ptxt>Steering control ECU</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: A tire angle of 36° and a motor rotation angle of 36° correspond to the steering wheel angle. The amount of change in tire angle and motor rotation angle is from when the mode is changed to test mode.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000YU202AX_02" type-id="32" category="03" proc-id="RM22W0E___0000B8P00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C163898E03" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000YU202AX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000YU202AX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YU202AX_04_0010" proc-id="RM22W0E___0000B8S00001">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs in the CAN communication system (for LHD: See page <xref label="Seep01" href="RM000001RSW03ZX"/>, for RHD: See page <xref label="Seep02" href="RM000001RSW040X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.79in"/>
<colspec colname="COL2" colwidth="1.34in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YU202AX_04_0011" fin="false">A</down>
<right ref="RM000000YU202AX_04_0012" fin="true">B</right>
<right ref="RM000000YU202AX_04_0013" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000YU202AX_04_0011" proc-id="RM22W0E___0000B8T00001">
<testtitle>CHECK STEERING ANGLE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if a steering angle sensor that complies with the vehicle specifications is installed.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>A steering angle sensor that complies with the vehicle specification is installed.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YU202AX_04_0002" fin="false">OK</down>
<right ref="RM000000YU202AX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YU202AX_04_0002" proc-id="RM22W0E___0000B8Q00001">
<testtitle>CHECK HARNESS AND CONNECTOR (STEERING CONTROL ECU - STEERING ANGLE SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C149363E03" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E79 steering control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E11 steering angle sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.75in"/>
<colspec colname="COLSPEC0" colwidth="1.08in"/>
<colspec colname="COL2" colwidth="1.30in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E79-10 (SS1-) - E11-7 (SS1-)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E79-19 (SS1+) - E11-8 (SS1+)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E79-19 (SS1+) - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E79-10 (SS1-) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E79-19 (SS1+) - E79-10 (SS1-)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YU202AX_04_0003" fin="false">OK</down>
<right ref="RM000000YU202AX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YU202AX_04_0003" proc-id="RM22W0E___0000B8R00001">
<testtitle>CHECK STEERING ANGLE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E79 steering control ECU connector.</ptxt>
<figure>
<graphic graphicname="C149142E03" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Move the shift lever to N.</ptxt>
</test1>
<test1>
<ptxt>Jack up the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Check the waveform of the steering angle sensor using an oscilloscope.</ptxt>
<atten4>
<ptxt>The voltage must be measured with the steering angle sensor connector connected.</ptxt>
</atten4>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.64in"/>
<colspec colname="COL2" colwidth="1.21in"/>
<colspec colname="COL3" colwidth="1.28in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E79-19 (SS1+) - E79-10 (SS1-) </ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG), steering wheel rotated slowly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation </ptxt>
<ptxt>(0 to 5 V)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>The output voltage should fluctuate up and down similarly to the diagram when the wheel is turned slowly.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<title>Result</title>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YU202AX_04_0008" fin="true">A</down>
<right ref="RM000000YU202AX_04_0004" fin="true">B</right>
<right ref="RM000000YU202AX_04_0009" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000YU202AX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YU202AX_04_0008">
<testtitle>REPLACE STEERING ANGLE SENSOR<xref label="Seep01" href="RM000000SS908KX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YU202AX_04_0004">
<testtitle>REPLACE STEERING CONTROL ECU<xref label="Seep01" href="RM000003DZD00JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YU202AX_04_0009">
<testtitle>REPLACE STEERING CONTROL ECU<xref label="Seep01" href="RM000003EFD005X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YU202AX_04_0012">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YU202AX_04_0013">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>