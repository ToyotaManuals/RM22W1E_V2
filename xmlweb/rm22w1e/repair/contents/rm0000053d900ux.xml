<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM0000053D900UX" category="C" type-id="8062L" name-id="NS8WI-01" from="201301" to="201308">
<dtccode>B1575</dtccode>
<dtcname>GVIF Disconnected (from EMV/MM Integrated Device to Multi Display)</dtcname>
<subpara id="RM0000053D900UX_01" type-id="60" category="03" proc-id="RM22W0E___0000COG00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1575</ptxt>
</entry>
<entry valign="middle">
<ptxt>GVIF disconnected (from Multi-media module receiver assembly to Multi-display assembly)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector (GVIF cable)</ptxt>
</item>
<item>
<ptxt>Multi-media module receiver assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000053D900UX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000053D900UX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000053D900UX_03_0001" proc-id="RM22W0E___0000COH00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0N7X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000053D900UX_03_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000053D900UX_03_0002" proc-id="RM22W0E___0000COI00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTC is output again (See page <xref label="Seep01" href="RM0000011BU0N7X"/>). </ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000053D900UX_03_0006" fin="true">OK</down>
<right ref="RM0000053D900UX_03_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000053D900UX_03_0009" proc-id="RM22W0E___0000COM00000">
<testtitle>CHECK HARNESS AND CONNECTOR (GVIF CABLE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the digital signal lines between the multi-display assembly and parking assist ECU are not sharply bent or pinched, and that the connectors are properly connected and there are no other installation problems.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There are no installation problems.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000053D900UX_03_0003" fin="false">OK</down>
<right ref="RM0000053D900UX_03_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000053D900UX_03_0003" proc-id="RM22W0E___0000COJ00000">
<testtitle>REPLACE HARNESS AND CONNECTOR (GVIF CABLE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the harness and connector (GVIF cable) with a new or normally functioning one.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000053D900UX_03_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000053D900UX_03_0004" proc-id="RM22W0E___0000COK00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0N7X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000053D900UX_03_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000053D900UX_03_0005" proc-id="RM22W0E___0000COL00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTC is output again (See page <xref label="Seep01" href="RM0000011BU0N7X"/>). </ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000053D900UX_03_0008" fin="true">OK</down>
<right ref="RM0000053D900UX_03_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000053D900UX_03_0006">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000053D900UX_03_0007">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000053D900UX_03_0008">
<testtitle>END (GVIF CABLE IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM0000053D900UX_03_0010">
<testtitle>REPLACE HARNESS AND CONNECTOR (GVIF CABLE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>