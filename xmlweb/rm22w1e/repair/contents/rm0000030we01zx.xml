<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12009_S000K" variety="S000K">
<name>1GR-FE EMISSION CONTROL</name>
<ttl id="12009_S000K_7C3I1_T00B4" variety="T00B4">
<name>AIR SWITCHING VALVE (for Bank 1)</name>
<para id="RM0000030WE01ZX" category="A" type-id="30014" name-id="EC4WD-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000030WE01ZX_02" type-id="01" category="01">
<s-1 id="RM0000030WE01ZX_02_0023" proc-id="RM22W0E___000011800000">
<ptxt>INSTALL NO. 1 EMISSION CONTROL VALVE SET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 emission control valve set with the 3 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Align the paint mark with the rib and connect the No. 1 air hose.</ptxt>
<figure>
<graphic graphicname="A267739E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Rib</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>RH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Top</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Make sure the direction of the hose clamp is as shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Connect the No. 1 emission control valve set connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030WE01ZX_02_0025" proc-id="RM22W0E___000011A00000">
<ptxt>INSTALL AIR TUBE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 2 new gaskets.</ptxt>
<figure>
<graphic graphicname="A226791E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Claw</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Air Tube</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure the gasket's claws are not caught between the No. 1 emission control valve set and air tube.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the air tube with the 2 bolts and 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value3>7</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030WE01ZX_02_0029" proc-id="RM22W0E___000047700000">
<ptxt>INSTALL AIR CLEANER CAP AND HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the air cleaner cap and hose.</ptxt>
<s3>
<ptxt>Install the air cleaner cap and hose with the bolt and fasten the 4 hook clamps.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Tighten the clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>2.5</t-value1>
<t-value2>25</t-value2>
<t-value3>22</t-value3>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Attach the clamp and connect the ventilation hose, vacuum hose and mass air flow meter connector.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000030WE01ZX_02_0030" proc-id="RM22W0E___00000Z200000">
<ptxt>INSTALL V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 2 V-bank cover grommets with the 2 pins and press down on the V-bank cover to attach the pins.</ptxt>
<figure>
<graphic graphicname="A271365E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>