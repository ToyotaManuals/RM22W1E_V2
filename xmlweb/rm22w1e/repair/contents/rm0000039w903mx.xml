<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000I" variety="S000I">
<name>1VD-FTV FUEL</name>
<ttl id="12008_S000I_7C3H7_T00AA" variety="T00AA">
<name>FUEL TANK</name>
<para id="RM0000039W903MX" category="A" type-id="80001" name-id="FUAO1-01" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000039W903MX_01" type-id="01" category="01">
<s-1 id="RM0000039W903MX_01_0060" proc-id="RM22W0E___000068S00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0003">
<ptxt>REMOVE FUEL TANK CAP ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM0000039W903MX_01_0004" proc-id="RM22W0E___000067V00001">
<ptxt>REMOVE NO. 1 FUEL TANK PROTECTOR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and fuel tank protector.</ptxt>
<figure>
<graphic graphicname="A175014" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0005" proc-id="RM22W0E___000067W00001">
<ptxt>DISCONNECT FUEL TANK MAIN TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
<figure>
<graphic graphicname="A175058" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the fuel tank main tube.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0043" proc-id="RM22W0E___000068C00001">
<ptxt>DRAIN FUEL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Double Tank Type:</ptxt>
<s3>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the ignition switch to ON.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Turn the intelligent tester on.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Activate the Intank Fuel Pump Relay.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Do not smoke or be near an open flame when working on the fuel system.</ptxt>
</item>
<item>
<ptxt>Secure good ventilation.</ptxt>
</item>
</list1>
</atten2>
<atten4>
<ptxt>for Single Tank Type:</ptxt>
<ptxt>Remove the fuel tube joint clip and disconnect the fuel tank return tube, and drain fuel from the port shown in the illustration.</ptxt>
<ptxt>for Double Tank Type:</ptxt>
<ptxt>If the fuel pump does not operate, remove the fuel tube joint clip and disconnect the fuel tank return tube, and drain fuel from the port shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A181435E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</atten4>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0002" proc-id="RM22W0E___000067U00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
<s2>
<ptxt>Disconnect the cables from the negative (-) main battery and sub-battery terminals.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0044" proc-id="RM22W0E___000068D00001">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY RH (for 60/40 Split Seat Type 40 Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat assembly RH (See page <xref label="Seep01" href="RM00000391400RX_01_0008"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0045" proc-id="RM22W0E___000068E00001">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY LH (for 60/40 Split Seat Type 60 Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat assembly LH (See page <xref label="Seep01" href="RM00000390Z00RX_01_0005"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0046" proc-id="RM22W0E___000068F00001">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Face to Face Seat Type:</ptxt>
<ptxt>Remove the rear No. 2 seat assembly (See page <xref label="Seep01" href="RM00000311C003X_02_0002"/>).</ptxt>
</s2>
<s2>
<ptxt>except Face to Face Seat Type:</ptxt>
<ptxt>Remove the rear No. 2 seat assembly (See page <xref label="Seep02" href="RM00000391S00ZX_01_0003"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0047" proc-id="RM22W0E___000068G00001">
<ptxt>REMOVE REAR NO. 1 SEAT PROTECTOR
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184032E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 10 claws and remove the 2 seat protectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039W903MX_01_0048" proc-id="RM22W0E___000068H00001">
<ptxt>REMOVE REAR NO. 2 SEAT PROTECTOR
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184031E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 10 claws and remove the 2 seat protectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039W903MX_01_0049" proc-id="RM22W0E___000068I00001">
<ptxt>REMOVE REAR STEP COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181673" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to remove the step cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 2 claws and remove the step cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039W903MX_01_0050" proc-id="RM22W0E___000068J00001">
<ptxt>REMOVE REAR DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181674" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws and 4 clips, and remove the scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039W903MX_01_0051" proc-id="RM22W0E___000068K00001">
<ptxt>REMOVE REAR DOOR SCUFF PLATE RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM0000039W903MX_01_0052" proc-id="RM22W0E___000068L00001">
<ptxt>REMOVE REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 6 clips and remove the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039W903MX_01_0053" proc-id="RM22W0E___000068M00001">
<ptxt>REMOVE REAR SEAT COVER CAP (w/ Rear No. 2 Seat, except Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190187E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to remove the rear seat cover cap on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 3 claws and remove the rear seat cover cap.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039W903MX_01_0054" proc-id="RM22W0E___000068N00001">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Remove the front quarter trim panel assembly LH (See page <xref label="Seep01" href="RM0000038MO00UX_01_0020"/>).</ptxt>
</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>Remove the front quarter trim panel assembly LH (See page <xref label="Seep02" href="RM0000038MO00VX_01_0020"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0056" proc-id="RM22W0E___000068O00001">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Remove the front quarter trim panel assembly RH (See page <xref label="Seep01" href="RM0000038MO00UX_01_0022"/>).</ptxt>

</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>Remove the front quarter trim panel assembly RH (See page <xref label="Seep02" href="RM0000038MO00VX_01_0022"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0057" proc-id="RM22W0E___00006E700001">
<ptxt>REMOVE AIR DUCT PLUG (w/ Rear Air Conditioning System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the plug.</ptxt>
<atten4>
<ptxt>Use the same procedures for both sides.</ptxt>
</atten4>
<figure>
<graphic graphicname="B183333" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039W903MX_01_0058" proc-id="RM22W0E___000068Q00001">
<ptxt>REMOVE REAR AIR DUCT GUIDE (w/ Rear Air Conditioning System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
<figure>
<graphic graphicname="B183334" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and remove the guide.</ptxt>
<atten4>
<ptxt>Use the same procedures for both sides.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039W903MX_01_0041" proc-id="RM22W0E___000068A00001">
<ptxt>REMOVE FRONT FLOOR CARPET ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E157065" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<ptxt>Fold back the floor carpet.</ptxt>
<atten4>
<ptxt>Fold back the floor carpet so that the air duct can be removed.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>w/o Rear Air Conditioning System:</ptxt>
<ptxt>Fold back the floor carpet.</ptxt>
<atten4>
<ptxt>Fold back the floor carpet so that the service hole cover can be removed.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0042" proc-id="RM22W0E___000068B00001">
<ptxt>REMOVE REAR FLOOR NO. 2 SERVICE HOLE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<ptxt>Remove the 2 screws and air duct.</ptxt>
<figure>
<graphic graphicname="A181426" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the service hole cover.</ptxt>
<figure>
<graphic graphicname="A181427" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the fuel pump and fuel sender gauge connector.</ptxt>
<figure>
<graphic graphicname="A181410E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0006" proc-id="RM22W0E___000067X00001">
<ptxt>DISCONNECT FUEL TANK RETURN TUBE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
<figure>
<graphic graphicname="A175056E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Pull up the retainer and disconnect both ends of the fuel tank return tube.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0019" proc-id="RM22W0E___000068700001">
<ptxt>DISCONNECT NO. 2 FUEL TANK BREATHER TUBE (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
<figure>
<graphic graphicname="A176372" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the No. 2 fuel tank breather tube from the body.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0015" proc-id="RM22W0E___000068500001">
<ptxt>DISCONNECT NO. 2 FUEL MAIN TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
<figure>
<graphic graphicname="A175061E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Pull up the retainer and disconnect the fuel main tube.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0007" proc-id="RM22W0E___000067Y00001">
<ptxt>DISCONNECT NO. 2 FUEL TANK BREATHER TUBE (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
<figure>
<graphic graphicname="A175063E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Pull up the retainer and disconnect the fuel tank breather tube.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0020" proc-id="RM22W0E___000068800001">
<ptxt>DISCONNECT FUEL TANK BREATHER TUBE (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
<figure>
<graphic graphicname="A176312E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Pull up the retainer and disconnect the fuel tank breather tube.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not use any tools in this procedure.</ptxt>
</item>
<item>
<ptxt>Check for any dirt and foreign matter contamination in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-rings or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0008" proc-id="RM22W0E___000067Z00001">
<ptxt>DISCONNECT FUEL TANK BREATHER TUBE (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
<figure>
<graphic graphicname="A175065E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Pull up the retainer and disconnect the fuel tank breather tube.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not use any tools in this procedure.</ptxt>
</item>
<item>
<ptxt>Check for any dirt and foreign matter contamination in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-rings or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0021" proc-id="RM22W0E___000068900001">
<ptxt>DISCONNECT FUEL TANK TO FILLER PIPE HOSE (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A176314" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0009" proc-id="RM22W0E___000068000001">
<ptxt>DISCONNECT FUEL TANK TO FILLER PIPE HOSE (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A175067" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0010" proc-id="RM22W0E___000068100001">
<ptxt>REMOVE FUEL TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place a transmission jack under the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts, 2 clips, 2 pins and 2 fuel tank bands.</ptxt>
<figure>
<graphic graphicname="A175024" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Lower the transmission jack to remove the fuel tank from the vehicle.</ptxt>
<atten3>
<ptxt>Do not allow the fuel tank to contact the vehicle, especially the differential.</ptxt>
</atten3>
<atten4>
<ptxt>Lower the transmission jack slowly.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0011" proc-id="RM22W0E___000068200001">
<ptxt>REMOVE FUEL TANK MAIN TUBE SUB-ASSEMBLY AND FUEL TANK RETURN TUBE (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 fuel tube joint clips and pull out the 2 fuel tubes.</ptxt>
<figure>
<graphic graphicname="A181404E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Remove any dirt and foreign matter on the fuel tube joint before performing this work.</ptxt>
</item>
<item>
<ptxt>Do not allow any scratches or foreign matter on the parts when disconnecting them, as the fuel tube joint contains the O-rings that seal the plug.</ptxt>
</item>
<item>
<ptxt>Perform this work by hand. Do not use any tools.</ptxt>
</item>
<item>
<ptxt>Do not forcibly bend, twist or turn the nylon tube.</ptxt>
</item>
<item>
<ptxt>Protect the disconnected part by covering it with a plastic bag and tape after disconnecting the fuel tubes.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the 2 fuel tubes from the fuel tank.</ptxt>
<figure>
<graphic graphicname="A175030" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0016" proc-id="RM22W0E___000068600001">
<ptxt>REMOVE FUEL TANK MAIN TUBE SUB-ASSEMBLY, FUEL TANK RETURN TUBE AND NO. 2 FUEL MAIN TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 fuel tube joint clips and pull out the 3 fuel tubes.</ptxt>
<figure>
<graphic graphicname="A175068E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Remove any dirt and foreign matter on the fuel tube joint before performing this work.</ptxt>
</item>
<item>
<ptxt>Do not allow any scratches or foreign matter on the parts when disconnecting them, as the fuel tube joint contains the O-rings that seal the plug.</ptxt>
</item>
<item>
<ptxt>Perform this work by hand. Do not use any tools.</ptxt>
</item>
<item>
<ptxt>Do not forcibly bend, twist or turn the nylon tube.</ptxt>
</item>
<item>
<ptxt>Protect the disconnected part by covering it with a plastic bag and tape after disconnecting the fuel tubes.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the 3 fuel tubes from the fuel tank.</ptxt>
<figure>
<graphic graphicname="A175070" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0059" proc-id="RM22W0E___000068R00000">
<ptxt>REMOVE FUEL SUCTION WITH PUMP AND GAUGE TUBE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set SST on the retainer.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A260062E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Securely attach the claws of SST to the protrusion of the retainer and fix SST in place.</ptxt>
</item>
<item>
<ptxt>Install SST while pressing the claws of SST against the retainer (towards the center of SST).</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Using SST, loosen the retainer.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A260063E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not use any tools other than those specified in this operation. Damage to the retainer or the fuel tank may result.</ptxt>
</item>
<item>
<ptxt>While pressing down on SST so that the claws of SST do not slip off the protrusion of the retainer, turn the retainer counterclockwise to loosen it.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>The protrusion of the retainer fit in the ends of SST.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the retainer.</ptxt>
</s2>
<s2>
<ptxt>Remove the retainer while holding the fuel suction tube assembly by hand.</ptxt>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the gasket from the fuel tank.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039W903MX_01_0013" proc-id="RM22W0E___000068300001">
<ptxt>REMOVE FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the hose from the fuel tank.</ptxt>
<figure>
<graphic graphicname="A176316" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903MX_01_0014" proc-id="RM22W0E___000068400001">
<ptxt>REMOVE NO. 1 FUEL TANK HEAT INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel tube clamp from the heat insulator.</ptxt>
<figure>
<graphic graphicname="A175071E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Using needle-nose pliers, remove the 4 clips shown in the illustration and then remove the heat insulator.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>