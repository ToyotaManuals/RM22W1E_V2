<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3X2_T00Q5" variety="T00Q5">
<name>POWER WINDOW CONTROL SYSTEM (for Models with Jam Protection Function on 4 Windows)</name>
<para id="RM000000XI103LX" category="J" type-id="800OK" name-id="WS4YO-02" from="201301">
<dtccode/>
<dtcname>Rear Power Window LH does not Operate with Rear Power Window Switch LH</dtcname>
<subpara id="RM000000XI103LX_01" type-id="60" category="03" proc-id="RM22W0E___0000HXQ00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>If the manual up/down function does not operate, there may be a malfunction in the rear power window regulator switch, power window rear regulator motor LH, harness or connector.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000XI103LX_02" type-id="32" category="03" proc-id="RM22W0E___0000HXR00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B184409E02" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XI103LX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000XI103LX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XI103LX_06_0022" proc-id="RM22W0E___0000HXV00000">
<testtitle>CHECK LIN COMMUNICATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for LIN communication system DTCs related to the power window control system (See page <xref label="Seep01" href="RM000002S88023X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>LIN communication system DTCs are not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XI103LX_06_0001" fin="false">OK</down>
<right ref="RM000000XI103LX_06_0026" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI103LX_06_0001" proc-id="RM22W0E___0000HXS00000">
<testtitle>CHECK FOR DTC (B2312)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if DTC B2312 is output (See page <xref label="Seep01" href="RM000002BQR07QX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2312 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XI103LX_06_0023" fin="false">OK</down>
<right ref="RM000000XI103LX_06_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI103LX_06_0023" proc-id="RM22W0E___0000HXW00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (REAR POWER WINDOW REGULATOR MOTOR LH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the power window regulator motor is functioning properly (See page <xref label="Seep01" href="RM0000027QL03DX"/>).</ptxt>
<table pgwide="1">
<title>RL-Door Motor</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.60in"/>
<colspec colname="COL2" colwidth="1.94in"/>
<colspec colname="COL3" colwidth="2.10in"/>
<colspec colname="COL4" colwidth="1.44in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>RL Door P/W Up SW</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear power window LH manual up signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Rear power window LH manual up switch operated</ptxt>
<ptxt>OFF: Rear power window LH switch not operated</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RL Door P/W Down SW</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear power window LH manual down signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Rear power window LH manual down switch operated</ptxt>
<ptxt>OFF: Rear power window LH switch not operated</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display changes according to operation of the rear power window switch LH.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XI103LX_06_0014" fin="true">OK</down>
<right ref="RM000000XI103LX_06_0024" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI103LX_06_0024" proc-id="RM22W0E___0000HXX00000">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR POWER WINDOW REGULATOR SWITCH LH - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the J10 switch connector.</ptxt>
<figure>
<graphic graphicname="B184452E02" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>J10-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XI103LX_06_0025" fin="false">OK</down>
<right ref="RM000000XI103LX_06_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI103LX_06_0025" proc-id="RM22W0E___0000HXY00000">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR POWER WINDOW REGULATOR MOTOR LH - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B180291E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the J11 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>J11-2 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>J11-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XI103LX_06_0018" fin="false">OK</down>
<right ref="RM000000XI103LX_06_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI103LX_06_0018" proc-id="RM22W0E___0000HXT00000">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR POWER WINDOW REGULATOR SWITCH LH - REAR POWER WINDOW REGULATOR MOTOR LH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the J10 switch connector.</ptxt>
<figure>
<graphic graphicname="B184455E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the J11 motor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>J10-6 (UP) - J11-10 (UP)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>J10-7 (DOWN) - J11-7 (DOWN)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>J10-8 (AUTO) - J11-4 (AUTO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>J10-6 (UP) or J11-10 (UP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>J10-7 (DOWN) or J11-7 (DOWN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>J10-8 (AUTO) or J11-4 (AUTO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XI103LX_06_0019" fin="false">OK</down>
<right ref="RM000000XI103LX_06_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI103LX_06_0019" proc-id="RM22W0E___0000HXU00000">
<testtitle>INSPECT REAR POWER WINDOW REGULATOR SWITCH ASSEMBLY LH</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B184456E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Remove the rear power window regulator switch (See page <xref label="Seep01" href="RM000002STU086X"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>J10-6 (UP) - J10-1 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Manual up operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>J10-7 (DOWN) - J10-1 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Manual down operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XI103LX_06_0014" fin="true">OK</down>
<right ref="RM000000XI103LX_06_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI103LX_06_0026">
<testtitle>GO TO LIN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO08EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI103LX_06_0017">
<testtitle>GO TO DTC B2312<xref label="Seep01" href="RM000002BQR07QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI103LX_06_0020">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XI103LX_06_0021">
<testtitle>REPLACE REAR POWER WINDOW REGULATOR SWITCH ASSEMBLY LH<xref label="Seep01" href="RM000002STU086X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI103LX_06_0014">
<testtitle>REPLACE REAR POWER WINDOW REGULATOR MOTOR ASSEMBLY LH<xref label="Seep01" href="RM000002SU001ZX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>