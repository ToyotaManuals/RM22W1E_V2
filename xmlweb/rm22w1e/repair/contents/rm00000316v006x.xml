<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3E4_T0077" variety="T0077">
<name>DIESEL THROTTLE BODY</name>
<para id="RM00000316V006X" category="A" type-id="80001" name-id="ES117V-002" from="201308">
<name>REMOVAL</name>
<subpara id="RM00000316V006X_01" type-id="01" category="01">
<s-1 id="RM00000316V006X_01_0016" proc-id="RM22W0E___000035800001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000316V006X_01_0013" proc-id="RM22W0E___000035700001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000316V006X_01_0001" proc-id="RM22W0E___000034Z00001">
<ptxt>REMOVE INTERCOOLER ASSEMBLY (w/ Intercooler)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000031FS003X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM00000316V006X_01_0014" proc-id="RM22W0E___000034E00001">
<ptxt>REMOVE NO. 1 COOL AIR INLET (w/o Intercooler)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the No. 1 air hose clamp.</ptxt>
<figure>
<graphic graphicname="A257026" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the turbo pressure sensor connector, intake air temperature sensor connector and vacuum hose.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 nuts, bolt and No. 1 cool air inlet.</ptxt>
<figure>
<graphic graphicname="A257028" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the gasket from the air tube RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000316V006X_01_0015" proc-id="RM22W0E___000034F00001">
<ptxt>REMOVE NO. 2 COOL AIR INLET (w/o Intercooler)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the No. 2 air hose clamp.</ptxt>
<figure>
<graphic graphicname="A257027" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 nuts, bolt and No. 2 cool air inlet.</ptxt>
<figure>
<graphic graphicname="A257029" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the gasket from the air tube LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000316V006X_01_0007" proc-id="RM22W0E___000035400001">
<ptxt>DISCONNECT NO. 2 ENGINE OIL LEVEL DIPSTICK GUIDE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the oil level dipstick guide.</ptxt>
<figure>
<graphic graphicname="A176580" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000316V006X_01_0002" proc-id="RM22W0E___000035000001">
<ptxt>REMOVE AIR TUBE SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the hose clamp.</ptxt>
<figure>
<graphic graphicname="A174729" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the air tube from the throttle body.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000316V006X_01_0008">
<ptxt>DRAIN CLUTCH FLUID (for Manual Transmission)</ptxt>
</s-1>
<s-1 id="RM00000316V006X_01_0011" proc-id="RM22W0E___000035500001">
<ptxt>REMOVE CLUTCH HOSE (for Manual Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 flexible hose tubes from the clutch hose with a 10 mm union nut wrench while holding the flexible hose with a wrench.</ptxt>
<figure>
<graphic graphicname="A177028" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not bend or damage the flexible hose tubes.</ptxt>
</item>
<item>
<ptxt>Do not allow any foreign matter such as dirt and dust to enter the flexible hose tubes from the connecting point.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the 2 clips.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and clutch hose.</ptxt>
<figure>
<graphic graphicname="A183845" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000316V006X_01_0003" proc-id="RM22W0E___000035100001">
<ptxt>REMOVE AIR TUBE SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the hose clamp.</ptxt>
<figure>
<graphic graphicname="A174728" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the air tube from the throttle body.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000316V006X_01_0012" proc-id="RM22W0E___000035600001">
<ptxt>REMOVE TUBE CONNECTOR TO FLEXIBLE HOSE TUBE (for Manual Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt> Using a 10 mm union nut wrench, disconnect the tube connector to flexible hose tube from the clutch tube to release cylinder 2 way.</ptxt>
<figure>
<graphic graphicname="A177030" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not bend or damage the flexible hose tube.</ptxt>
</item>
<item>
<ptxt>Do not allow any foreign matter such as dirt and dust to enter the flexible hose tube from the connecting point.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the 2 bolts and tube connector to flexible hose tube.</ptxt>
<figure>
<graphic graphicname="A177031" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000316V006X_01_0004" proc-id="RM22W0E___000035200001">
<ptxt>REMOVE DIESEL THROTTLE BODY ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the throttle position sensor connector.</ptxt>
<figure>
<graphic graphicname="A163132" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the throttle motor connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts, 2 nuts and throttle body.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the intake pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000316V006X_01_0006" proc-id="RM22W0E___000035300001">
<ptxt>REMOVE DIESEL THROTTLE BODY ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the throttle position sensor connector.</ptxt>
<figure>
<graphic graphicname="A163131" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the throttle motor connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts, 2 nuts and throttle body.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the intake pipe.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>