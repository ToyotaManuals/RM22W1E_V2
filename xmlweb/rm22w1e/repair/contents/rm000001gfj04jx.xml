<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WO_T00PR" variety="T00PR">
<name>AIR CONDITIONING SYSTEM (for Manual Air Conditioning System)</name>
<para id="RM000001GFJ04JX" category="J" type-id="301RT" name-id="ACEDK-02" from="201301" to="201308">
<dtccode/>
<dtcname>PTC Heater Circuit</dtcname>
<subpara id="RM000001GFJ04JX_01" type-id="60" category="03" proc-id="RM22W0E___0000HAW00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>PTC heater relays are closed in accordance with signals from the air conditioning amplifier assembly and power is supplied to the quick heater assembly installed on the radiator heater unit.</ptxt>
</content5>
</subpara>
<subpara id="RM000001GFJ04JX_02" type-id="32" category="03" proc-id="RM22W0E___0000HAX00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E179781E16" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001GFJ04JX_03" type-id="51" category="05" proc-id="RM22W0E___0000HAY00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses and relays for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001GFJ04JX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001GFJ04JX_05_0001" proc-id="RM22W0E___0000HAZ00000">
<testtitle>INSPECT PTC HEATER RELAY (PTC HTR1, PTC HTR2, PTC HTR3)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the PTC heater relays from the engine room relay block and junction block assembly.</ptxt>
<figure>
<graphic graphicname="A092673E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery voltage is not applied between terminals 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery voltage is applied between terminals 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04JX_05_0025" fin="false">OK</down>
<right ref="RM000001GFJ04JX_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0025" proc-id="RM22W0E___0000HB400000">
<testtitle>CHECK HARNESS AND CONNECTOR (PTC HEATER RELAY - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the PTC heater relays from the engine room relay block and junction block assembly.</ptxt>
<figure>
<graphic graphicname="E234132E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR1 relay terminal 5 - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR2 relay terminal 5 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR3 relay terminal 5 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without relay installed</ptxt>
<ptxt>(Engine Room Relay Block and Junction Block Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>PTC Heater Relay (PTC HTR3)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>PTC Heater Relay (PTC HTR2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>PTC Heater Relay (PTC HTR1)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04JX_05_0027" fin="false">OK</down>
<right ref="RM000001GFJ04JX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0027" proc-id="RM22W0E___0000HB500000">
<testtitle>CHECK HARNESS AND CONNECTOR (PTC HEATER RELAY - AIR CONDITIONING AMPLIFIER AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the PTC heater relays from the engine room relay block and junction block assembly.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E81 air conditioning amplifier assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR1 relay terminal 1 - E81-3 (PTC1)</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR2 relay terminal 1 - E81-22 (PTC2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR3 relay terminal 1 - E81-4 (PTC3)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR1 relay terminal 1 - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR2 relay terminal 1 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR3 relay terminal 1 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR1 relay terminal 2 - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR2 relay terminal 2 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR3 relay terminal 2 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04JX_05_0015" fin="false">OK</down>
<right ref="RM000001GFJ04JX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0015" proc-id="RM22W0E___0000HB100000">
<testtitle>CHECK HARNESS AND CONNECTOR (PTC HEATER RELAY - QUICK HEATER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the PTC heater relays from the engine room relay block and junction block assembly.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E33 quick heater assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR1 relay terminal 3 - E33-2 (B)</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR2 relay terminal 3 - E33-1 (B)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR3 relay terminal 3 - E33-3 (B)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR1 relay terminal 3 - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR2 relay terminal 3 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC HTR3 relay terminal 3 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04JX_05_0022" fin="false">OK</down>
<right ref="RM000001GFJ04JX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0022" proc-id="RM22W0E___0000HB300000">
<testtitle>CHECK HARNESS AND CONNECTOR (QUICK HEATER - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E34 quick heater assembly connector.</ptxt>
<figure>
<graphic graphicname="E179945E13" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E34-1 (E) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E34-2 (E) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Quick Heater Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04JX_05_0016" fin="false">OK</down>
<right ref="RM000001GFJ04JX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0016" proc-id="RM22W0E___0000HB200000">
<testtitle>INSPECT QUICK HEATER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the quick heater assembly (See page <xref label="Seep01" href="RM0000022AH01XX"/>).</ptxt>
<figure>
<graphic graphicname="E140255E08" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (B) - 1 (E)</ptxt>
</entry>
<entry morerows="3" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="3" valign="middle" align="center">
<ptxt>Below 1 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (B) - 1 (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (B) - 2 (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3 (B) - 2 (E)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04JX_05_0008" fin="false">OK</down>
<right ref="RM000001GFJ04JX_05_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0003" proc-id="RM22W0E___0000HB000000">
<testtitle>CHECK AIR CONDITIONING AMPLIFIER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the air conditioning amplifier assembly with its connectors still connected (See page <xref label="Seep01" href="RM0000039R501FX"/>).</ptxt>
<figure>
<graphic graphicname="E184002E14" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.25in"/>
<colspec colname="COLSPEC1" colwidth="1.92in"/>
<colspec colname="COL2" colwidth="0.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E81-3 (PTC1) - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Engine idling</ptxt>
<ptxt>Set temperature MAX. HOT</ptxt>
<ptxt>Engine coolant temperature below 65°C (149°F)</ptxt>
<ptxt>Ambient temperature below 10°C (50°F)</ptxt>
<ptxt>Blower switch off → LO level</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Below 1 V → 11 to 14 V*</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E81-22 (PTC2) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E81-4 (PTC3) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Air Conditioning Amplifier Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: After the measurement conditions are met, wait 30 seconds before performing measurements.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04JX_05_0026" fin="true">OK</down>
<right ref="RM000001GFJ04JX_05_0028" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0008">
<testtitle>REPLACE PTC HEATER RELAY (PTC HTR1, PTC HTR2, PTC HTR3)</testtitle>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0021">
<testtitle>REPLACE QUICK HEATER ASSEMBLY<xref label="Seep01" href="RM0000022AH01XX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0026">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002LIQ023X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001GFJ04JX_05_0028">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>