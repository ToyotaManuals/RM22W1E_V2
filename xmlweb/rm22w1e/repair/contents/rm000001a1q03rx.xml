<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001Q" variety="S001Q">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001Q_7C3Q2_T00J5" variety="T00J5">
<name>VANE PUMP (for 1GR-FE)</name>
<para id="RM000001A1Q03RX" category="G" type-id="3001K" name-id="PA1VG-01" from="201301">
<name>INSPECTION</name>
<subpara id="RM000001A1Q03RX_01" type-id="01" category="01">
<s-1 id="RM000001A1Q03RX_01_0001" proc-id="RM22W0E___0000B1N00000">
<ptxt>INSPECT VANE PUMP SHAFT AND BUSH IN VANE PUMP FRONT HOUSING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a micrometer, measure the outer diameter [a] of the vane pump shaft.</ptxt>
<figure>
<graphic graphicname="C132029E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a vernier caliper, measure the inner diameter [b] of the vane pump front housing bush.</ptxt>
</s2>
<s2>
<ptxt>Calculate the oil clearance.</ptxt>
<ptxt>Oil clearance = Inner diameter of the bush [b] - Outer diameter of the shaft [a]</ptxt>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt>0.07 mm (0.00276 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the oil clearance is more than the maximum, replace the vane pump assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1Q03RX_01_0002" proc-id="RM22W0E___0000B1O00000">
<ptxt>INSPECT VANE PUMP ROTOR AND VANE PUMP PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a micrometer, measure the thickness of the vane pump plates. </ptxt>
<figure>
<graphic graphicname="C101977E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard thickness</title>
<specitem>
<ptxt>1.405 to 1.411 mm (0.0553 to 0.0556 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the thickness is not as specified, replace the vane pump assembly.</ptxt>
</s2>
<s2>
<ptxt>Using a feeler gauge, measure the clearance between the side face of the vane pump rotor groove and the vane pump plate.</ptxt>
<figure>
<graphic graphicname="C101978E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum clearance</title>
<specitem>
<ptxt>0.03 mm (0.00118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is more than the maximum, replace the vane pump assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1Q03RX_01_0003" proc-id="RM22W0E___0000B1P00000">
<ptxt>INSPECT FLOW CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the flow control valve with power steering fluid and check that it falls smoothly into the valve hole by its own weight.</ptxt>
<figure>
<graphic graphicname="C228111" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If the control valve does not fall into the hole smoothly, replace the vane pump assembly.</ptxt>
</s2>
<s2>
<ptxt>Check the flow control valve for leakage. Close one of the holes and apply 390 to 490 kPa (4.0 to 5.0 kgf/cm<sup>2</sup>, 57 to 71 psi) of compressed air into the opposite side hole, and confirm that air does not come out from the end hole.</ptxt>
<figure>
<graphic graphicname="C114414" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If air leaks, replace the vane pump assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1Q03RX_01_0005" proc-id="RM22W0E___0000B1Q00000">
<ptxt>INSPECT FLOW CONTROL VALVE COMPRESSION SPRING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the free length of the compression spring.</ptxt>
<figure>
<graphic graphicname="C132032" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Minimum free length</title>
<specitem>
<ptxt>31.3 mm (1.23 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the length is less than the minimum, replace the vane pump assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1Q03RX_01_0006" proc-id="RM22W0E___0000B1R00000">
<ptxt>INSPECT PRESSURE PORT UNION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Visually check the pressure port union for fluid leaks. </ptxt>
<ptxt>If there is a leak, replace the vane pump assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1Q03RX_01_0007" proc-id="RM22W0E___0000B1S00000">
<ptxt>INSPECT TOTAL PRELOAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the pump rotates smoothly without abnormal noise.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install a service bolt.</ptxt>
<spec>
<title>Recommended service bolt</title>
<subtitle>Thread diameter</subtitle>
<specitem>
<ptxt>10 mm (0.394 in.)</ptxt>
</specitem>
<subtitle>Thread pitch</subtitle>
<specitem>
<ptxt>1.25 mm (0.0492 in.)</ptxt>
</specitem>
<subtitle>Bolt length</subtitle>
<specitem>
<ptxt>50 mm (1.97 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Using a torque wrench, measure the pump rotating torque.</ptxt>
<figure>
<graphic graphicname="C053369E10" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard rotating torque</title>
<specitem>
<ptxt>0.3 N*m (3 kgf*cm, 2 in.*lbf) or less</ptxt>
</specitem>
</spec>
<ptxt>If the rotating torque is not as specified, check the vane pump housing oil seal.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>