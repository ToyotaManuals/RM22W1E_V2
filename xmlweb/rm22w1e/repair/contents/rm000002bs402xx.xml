<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12048_S0020" variety="S0020">
<name>1UR-FE BATTERY / CHARGING</name>
<ttl id="12048_S0020_7C52R_T00U0" variety="T00U0">
<name>GENERATOR (for 180 A Type)</name>
<para id="RM000002BS402XX" category="A" type-id="80001" name-id="BH1K6-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM000002BS402XX_01" type-id="01" category="01">
<s-1 id="RM000002BS402XX_01_0015" proc-id="RM22W0E___000013G00000">
<ptxt>REMOVE V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Raise the front of the V-bank cover to detach the 3 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<figure>
<graphic graphicname="A274415E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002BS402XX_01_0035" proc-id="RM22W0E___0000D3U00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002BS402XX_01_0028" proc-id="RM22W0E___0000D3T00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002BS402XX_01_0026" proc-id="RM22W0E___000011Z00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BS402XX_01_0025" proc-id="RM22W0E___000011Y00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BS402XX_01_0022" proc-id="RM22W0E___000013S00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts and No. 1 engine under cover sub-assembly.</ptxt>
<figure>
<graphic graphicname="A178461" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BS402XX_01_0017" proc-id="RM22W0E___00004EN00000">
<ptxt>REMOVE FAN AND GENERATOR V BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>While turning the belt tensioner counterclockwise, align the service hole for the belt tensioner and the belt tensioner fixing position, and then insert a bar with a diameter of 5 mm (0.197 in.) into the service hole to fix the belt tensioner in place.</ptxt>
<figure>
<graphic graphicname="A163770E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bar</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Service Hole</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The pulley bolt for the belt tensioner has a left-hand thread.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the fan and generator V belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BS402XX_01_0027" proc-id="RM22W0E___000016K00000">
<ptxt>REMOVE FRONT FENDER APRON TRIM PACKING A
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 clips and front fender apron trim packing A.</ptxt>
<figure>
<graphic graphicname="A177003" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BS402XX_01_0020" proc-id="RM22W0E___000013H00000">
<ptxt>REMOVE AIR CLEANER CAP AND HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 PCV hose and No. 1 air hose.</ptxt>
<figure>
<graphic graphicname="A272589" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the mass air flow meter connector and detach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Loosen the hose clamp and remove the air cleaner cap and hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BS402XX_01_0018" proc-id="RM22W0E___00006Z500000">
<ptxt>DISCONNECT VANE PUMP ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and disconnect the vane pump.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BS402XX_01_0023" proc-id="RM22W0E___00004KO00000">
<ptxt>DISCONNECT OIL COOLER TUBE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and disconnect the oil cooler tube.</ptxt>
<figure>
<graphic graphicname="A272652" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BS402XX_01_0006" proc-id="RM22W0E___00004JT00001">
<ptxt>REMOVE GENERATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the generator connector.</ptxt>
<figure>
<graphic graphicname="A297261" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Open the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and disconnect the generator wire.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the wire harness bracket from the generator assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts, nut and generator assembly.</ptxt>
<figure>
<graphic graphicname="A297262" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the stud bolt.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>