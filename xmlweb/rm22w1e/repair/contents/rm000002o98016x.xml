<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TX_T00N0" variety="T00N0">
<name>CURTAIN SHIELD AIRBAG ASSEMBLY</name>
<para id="RM000002O98016X" category="N" type-id="3000N" name-id="RSAVR-04" from="201301" to="201308">
<name>DISPOSAL</name>
<subpara id="RM000002O98016X_01" type-id="11" category="10" proc-id="RM22W0E___0000FGB00000">
<content3 releasenbr="1">
<atten4>
<ptxt>When scrapping a vehicle equipped with the SRS or disposing of the steering pad, be sure to deploy the airbag first in accordance with the procedure described below. If any abnormality occurs with the airbag deployment, contact the SERVICE DEPT. of the DISTRIBUTOR..</ptxt>
</atten4>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Never dispose of a curtain shield airbag that has an unactivated airbag.</ptxt>
</item>
<item>
<ptxt>The airbag produces an exploding sound when it is deployed, so perform the operation outdoors and where it will not create a nuisance to nearby residents.</ptxt>
</item>
<item>
<ptxt>When deploying the airbag, always use the specified SST (SRS Airbag Deployment Tool). Perform the operation in a place away from electrical noise.</ptxt>
</item>
<item>
<ptxt>When deploying the airbag, perform the operation at least 10 m (32.8 ft.) away from the curtain shield airbag.</ptxt>
</item>
<item>
<ptxt>The curtain shield airbag becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a curtain shield airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water to a curtain shield airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
<item>
<ptxt>An airbag or pretensioner may be activated by static electricity. To prevent this, be sure to touch a metal surface with bare hands to discharge static electricity before performing this procedure.</ptxt>
</item>
</list1>
</atten2>
</content3>
</subpara>
<subpara id="RM000002O98016X_02" type-id="01" category="01">
<s-1 id="RM000002O98016X_02_0001" proc-id="RM22W0E___0000FGC00000">
<ptxt>DISPOSE OF CURTAIN SHIELD AIRBAG ASSEMBLY (WHEN INSTALLED IN VEHICLE)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Prepare a battery as the power source to deploy the airbag.</ptxt>
</atten4>
<s2>
<ptxt>Check the function of SST (See page <xref label="Seep01" href="RM0000038MY01DX_01_0001"/>).</ptxt>
<figure>
<graphic graphicname="C110371E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Read the precaution (See page <xref label="Seep02" href="RM000000KT10JGX"/>).</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep04" href="RM000003C32005X"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the roof headlining (w/ Sliding Roof) (See page <xref label="Seep03" href="RM0000038MO00QX"/>).</ptxt>
</s2>
<s2>
<ptxt>Remove the roof headlining (w/o Sliding Roof) (See page <xref label="Seep05" href="RM0000038MM00PX"/>).</ptxt>
</s2>
<s2>
<ptxt>Install SST.</ptxt>
<figure>
<graphic graphicname="B181626E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Disconnect the connector from the curtain shield airbag.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>After connecting SST to each other, connect them to the curtain shield airbag.</ptxt>
<sst>
<sstitem>
<s-number>09082-00700</s-number>
</sstitem>
<sstitem>
<s-number>09082-00802</s-number>
<s-subnumber>09082-10801</s-subnumber>
<s-subnumber>09082-40800</s-subnumber>
</sstitem>
</sst>
<atten3>
<ptxt>To avoid damaging SST connector and wire harness, do not lock the secondary lock of the twin lock.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Move SST at least 10 m (32.8 ft.) away from the vehicle rear side window.</ptxt>
<figure>
<graphic graphicname="C110476E12" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Maintaining enough clearance for SST wire harness in the rear side window, close all doors and windows of the vehicle. </ptxt>
<atten3>
<ptxt>Take care not to damage SST wire harness.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Connect the red clip of SST to the battery positive (+) terminal and black clip of SST to the battery negative (-) terminal. </ptxt>
</s3>
</s2>
<s2>
<ptxt>Deploy the airbag.</ptxt>
<s3>
<ptxt>Check that no one is inside the vehicle or within a 10 m (32.8 ft.) radius of the vehicle.</ptxt>
</s3>
<s3>
<ptxt>Press SST activation switch to deploy the airbag.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>When deploying the airbag, make sure that no one is near the vehicle.</ptxt>
</item>
<item>
<ptxt>The curtain shield airbag becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a curtain shield airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water to a curtain shield airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
</list1>
</atten2>
<atten4>
<ptxt>The airbag is deployed as the LED of SST activation switch comes on.</ptxt>
</atten4>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002O98016X_02_0002" proc-id="RM22W0E___0000FGD00000">
<ptxt>DISPOSE OF CURTAIN SHIELD AIRBAG ASSEMBLY (WHEN NOT INSTALLED IN VEHICLE)</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Be sure to follow the procedure detailed below when deploying the airbag.</ptxt>
</atten2>
<atten4>
<ptxt>Prepare a battery as the power source to deploy the airbag.</ptxt>
</atten4>
<s2>
<ptxt>Check the function of SST (See page <xref label="Seep01" href="RM0000038MY01DX_01_0001"/>).</ptxt>
<figure>
<graphic graphicname="C110371E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the curtain shield airbag (See page <xref label="Seep02" href="RM000003B7U004X"/>). </ptxt>
<atten2>
<ptxt>When removing the curtain shield airbag, wait at least 90 seconds after the ignition switch is turned off and the cable is disconnected from the negative (-) battery terminal before starting work.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Cut off the deployment section of the curtain shield airbag.</ptxt>
<figure>
<graphic graphicname="B181627" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a service-purpose wire harness for the vehicle, tie down the curtain shield airbag to a tire.</ptxt>
<figure>
<graphic graphicname="B104882E11" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Cross-sectional area of stripped wire harness section</title>
<specitem>
<ptxt>1.25 mm<sup>2</sup> (0.00192 in.<sup>2</sup>) or more</ptxt>
</specitem>
</spec>
<atten2>
<ptxt>If the wire harness is too thin or an alternative object is used to tie down the curtain shield airbag, it may be snapped by the shock when the airbag is deployed. Always use a wire harness for vehicle use with a cross-sectional area of at least 1.25 mm<sup>2</sup> (0.00192 in.<sup>2</sup>).</ptxt>
</atten2>
<atten4>
<ptxt>To calculate the cross-sectional area of the stripped wire harness section:</ptxt>
<ptxt>Cross-sectional area = 3.14 x (Diameter)<sup>2</sup> divided by 4</ptxt>
</atten4>
<s3>
<ptxt>Position the curtain shield airbag inside a tire as shown in the illustration. </ptxt>
<figure>
<graphic graphicname="B181628E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Tire size</title>
<specitem>
<ptxt>Must exceed the following dimensions</ptxt>
</specitem>
<subtitle>Width</subtitle>
<specitem>
<ptxt>185 mm (7.28 in.)</ptxt>
</specitem>
<subtitle>Inner diameter</subtitle>
<specitem>
<ptxt>360 mm (14.2 in.)</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Tie the curtain shield airbag to the tire with several wire harnesses. </ptxt>
<atten2>
<ptxt>Make sure that the wire harnesses are tight. If there is slack in the wire harnesses, the curtain shield airbag may become loose due to the shock when the airbag is deployed.</ptxt>
</atten2>
<atten3>
<ptxt>As the tire may be damaged by the airbag deployment, use a tire that you are planning to throw away.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Install SST.</ptxt>
<s3>
<ptxt>After connecting SST to each other, connect them to the curtain shield airbag.</ptxt>
<sst>
<sstitem>
<s-number>09082-00802</s-number>
<s-subnumber>09082-10801</s-subnumber>
<s-subnumber>09082-40800</s-subnumber>
</sstitem>
</sst>
</s3>
</s2>
<s2>
<ptxt>Place tires.</ptxt>
<figure>
<graphic graphicname="B181629E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not place the curtain shield airbag with the deployment direction facing the ground.</ptxt>
</atten3>
<s3>
<ptxt>Place at least 2 tires under the tire which the curtain shield airbag is tied to.</ptxt>
</s3>
<s3>
<ptxt>Place at least 2 tires over the tire which the curtain shield airbag is tied to. The top tire should have a disc wheel installed. </ptxt>
<figure>
<graphic graphicname="B181630E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not place SST connector under the tire because it could be damaged.</ptxt>
</item>
<item>
<ptxt>As the disc wheel may be damaged by the airbag deployment, use a disc wheel that you are planning to throw away.</ptxt>
</item>
<item>
<ptxt>As the tires may be damaged by the airbag deployment, use tires that you are planning to throw away.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Tie the tires together with 2 wire harnesses.</ptxt>
<figure>
<graphic graphicname="B105087" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure that the wire harnesses are tight. Looseness in the wire harnesses results in the tires coming free due to the shock when the airbag is deployed.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Install SST.</ptxt>
<figure>
<graphic graphicname="B181631E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Connect SST connector.</ptxt>
<sst>
<sstitem>
<s-number>09082-00700</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>To avoid damaging SST connector and wire harness, do not lock the secondary lock of the twin lock. Also, secure some slack for SST wire harness inside the tire. </ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Deploy the airbag.</ptxt>
<s3>
<ptxt>Connect the red clip of SST to the battery positive (+) terminal and the black clip of SST to the battery negative (-) terminal. </ptxt>
</s3>
<s3>
<ptxt>Check that no one is within a 10 m (32.8 ft.) radius of the tire which the curtain shield airbag is tied to. </ptxt>
</s3>
<s3>
<ptxt>Press SST activation switch to deploy the airbag.</ptxt>
<atten2>
<ptxt>When deploying the airbag, make sure that no one is near the tire.</ptxt>
</atten2>
<atten4>
<ptxt>The airbag is deployed as the LED of SST activation switch comes on.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Dispose of the curtain shield airbag.</ptxt>
<figure>
<graphic graphicname="B181632" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<list1 type="unordered">
<item>
<ptxt>The curtain shield airbag becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a curtain shield airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water to a curtain shield airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
</list1>
</atten2>
<s3>
<ptxt>Remove the curtain shield airbag from the tire.</ptxt>
</s3>
<s3>
<ptxt>Place the curtain shield airbag in a plastic bag, tie it tightly and dispose of it in the same way as other general parts.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>