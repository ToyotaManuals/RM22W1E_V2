<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM00000452J00LX" category="D" type-id="303FK" name-id="BCE75-01" from="201301">
<name>CALIBRATION</name>
<subpara id="RM00000452J00LX_z0" proc-id="RM22W0E___0000AD400000">
<content5 releasenbr="1">
<step1>
<ptxt>DESCRIPTION</ptxt>
<step2>
<ptxt>After replacing VSC-related components, clearing and reading the sensor calibration data and system information is necessary.</ptxt>
</step2>
<step2>
<ptxt>Follow the chart to perform calibration.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Replacement/Adjustment Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Necessary Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Skid Control ECU (Master Cylinder Solenoid)</ptxt>
</entry>
<entry valign="middle">
<list1 type="ordered">
<item>
<ptxt>Clearing zero point calibration data</ptxt>
</item>
<item>
<ptxt>Yaw rate and acceleration sensor zero point calibration</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Yaw Rate Sensor Assembly</ptxt>
</entry>
<entry valign="middle">
<list1 type="ordered">
<item>
<ptxt>Clearing zero point calibration data</ptxt>
</item>
<item>
<ptxt>Yaw rate and acceleration sensor zero point calibration</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle height adjustment</ptxt>
</entry>
<entry valign="middle">
<list1 type="ordered">
<item>
<ptxt>Clearing zero point calibration data</ptxt>
</item>
<item>
<ptxt>Yaw rate and acceleration sensor zero point calibration</ptxt>
</item>
<item>
<ptxt>Steering sensor initialization</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>PERFORM YAW RATE AND ACCELERATION SENSOR ZERO POINT CALIBRATION AND SYSTEM INFORMATION (WHETHER VGRS IS EQUIPPED) (When Using GTS)</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>While obtaining the zero points, keep the vehicle stationary and do not vibrate, tilt, move, or shake it (do not start the engine).</ptxt>
</item>
<item>
<ptxt>Be sure to perform this procedure on a level surface (with an inclination of less than 1%).</ptxt>
</item>
</list1>
</atten3>
<step2>
<ptxt>Clear the zero point calibration and system information data.</ptxt>
<step3>
<ptxt>Turn the ignition switch off.</ptxt>
</step3>
<step3>
<ptxt>Check that the steering wheel is centered.</ptxt>
</step3>
<step3>
<ptxt>Check that the shift lever is in P.</ptxt>
</step3>
<step3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step3>
<step3>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step3>
<step3>
<ptxt>Turn the GTS on.</ptxt>
</step3>
<step3>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Utility / Reset Memory.</ptxt>
</step3>
<step3>
<ptxt>Select the skid control ECU (master cylinder solenoid) to clear the zero point calibration data using the GTS.</ptxt>
</step3>
<step3>
<ptxt>Turn the ignition switch off.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Perform zero point calibration of the yaw rate and acceleration sensor and store the system information.</ptxt>
<step3>
<ptxt>Turn the ignition switch off.</ptxt>
</step3>
<step3>
<ptxt>Check that the steering wheel is centered.</ptxt>
</step3>
<step3>
<ptxt>Check that the shift lever is in P.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>DTCs C1210 (Zero Point Calibration of Yaw Rate Sensor Undone) and C1336 (Zero Point Calibration of Acceleration Sensor Undone) are stored if the shift lever is not in P.</ptxt>
</item>
<item>
<ptxt>If a DTC is output that indicates zero point calibration is incomplete, repeat the procedure starting at the step for clearing the zero point calibration data and system information.</ptxt>
</item>
</list1>
</atten3>
</step3>
<step3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step3>
<step3>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step3>
<step3>
<ptxt>Turn the GTS on.</ptxt>
</step3>
<step3>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Utility / Test Mode.</ptxt>
</step3>
<step3>
<ptxt>Keep the vehicle stationary on a level surface for 5 seconds or more.</ptxt>
</step3>
<step3>
<ptxt>Check that the slip indicator light comes on for several seconds and then blinks in the test mode pattern (0.125 seconds on and 0.125 seconds off).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the slip indicator light does not blink, perform zero point calibration again.</ptxt>
</item>
<item>
<ptxt>The zero point calibration is performed only once after the system enters test mode.</ptxt>
</item>
<item>
<ptxt>Calibration cannot be performed again until the stored data is cleared.</ptxt>
</item>
</list1>
</atten4>
</step3>
<step3>
<ptxt>Turn the ignition switch off and disconnect the GTS.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Check if DTC C120A (ECU Initial Setting Incomplete) is output.</ptxt>
<atten4>
<ptxt>If DTC C120A is not output, calibration was performed successfully.</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>PERFORM YAW RATE AND ACCELERATION SENSOR ZERO POINT CALIBRATION AND SYSTEM INFORMATION (WHETHER VGRS IS EQUIPPED) (When Using SST Check Wire)</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>While obtaining the zero points, keep the vehicle stationary and do not vibrate, tilt, move, or shake it (do not start the engine).</ptxt>
</item>
<item>
<ptxt>Be sure to perform this procedure on a level surface (with an inclination of less than 1%).</ptxt>
</item>
</list1>
</atten3>
<step2>
<ptxt>Clear the zero point calibration and system information data.</ptxt>
<step3>
<ptxt>Turn the ignition switch off.</ptxt>
</step3>
<step3>
<ptxt>Check that the steering wheel is centered.</ptxt>
</step3>
<step3>
<ptxt>Check that the shift lever is in P.</ptxt>
</step3>
<step3>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step3>
<step3>
<ptxt>Using SST, connect and disconnect terminals 12 (TS) and 4 (CG) of the DLC3 4 times or more within 8 seconds.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C146309E15" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Check that the slip indicator light comes on.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Perform zero point calibration of the yaw rate and acceleration sensor and store the system information.</ptxt>
<step3>
<ptxt>Turn the ignition switch off.</ptxt>
</step3>
<step3>
<ptxt>Check that the steering wheel is centered.</ptxt>
</step3>
<step3>
<ptxt>Check that the shift lever is in P.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>DTCs 36 (Zero Point Calibration of Yaw Rate Sensor Undone) and 98 (Zero Point Calibration of Acceleration Sensor Undone) are stored if the shift lever is not in P.</ptxt>
</item>
<item>
<ptxt>If a DTC is output that indicates zero point calibration is incomplete, repeat the procedure starting at the step for clearing the zero point calibration data and system information.</ptxt>
</item>
</list1>
</atten3>
</step3>
<step3>
<ptxt>Using SST, connect terminals 12 (TS) and 4 (CG) of the DLC3.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C146309E15" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step3>
<step3>
<ptxt>Keep the vehicle stationary on a level surface for 5 seconds or more.</ptxt>
</step3>
<step3>
<ptxt>Check that the slip indicator light comes on for several seconds and then blinks in the test mode pattern (0.125 seconds on and 0.125 seconds off).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the slip indicator light does not blink, perform zero point calibration again.</ptxt>
</item>
<item>
<ptxt>The zero point calibration is performed only once after the system enters test mode.</ptxt>
</item>
<item>
<ptxt>Calibration cannot be performed again until the stored data is cleared.</ptxt>
</item>
</list1>
</atten4>
</step3>
<step3>
<ptxt>Turn the ignition switch off and disconnect SST from the DLC3.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Check if DTC 13 (ECU Initial Setting Incomplete) is output.</ptxt>
<atten4>
<ptxt>If DTC 13 is not output, calibration was performed successfully.</ptxt>
</atten4>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>