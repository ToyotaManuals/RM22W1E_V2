<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SQ_T00LT" variety="T00LT">
<name>WIRELESS DOOR LOCK CONTROL SYSTEM (w/o Entry and Start System)</name>
<para id="RM000001WYH01PX" category="D" type-id="303FF" name-id="DL12I-19" from="201301">
<name>OPERATION CHECK</name>
<subpara id="RM000001WYH01PX_z0" proc-id="RM22W0E___0000EAI00000">
<content5 releasenbr="1">
<step1>
<ptxt>NOTICES WHEN CHECKING</ptxt>
<step2>
<ptxt>Wireless door lock/unlock function:</ptxt>
<ptxt>This function operates only when the vehicle is in its initial condition (the following 3 conditions are met).</ptxt>
<list1 type="unordered">
<item>
<ptxt>No key is inserted into the ignition key cylinder.</ptxt>
</item>
<item>
<ptxt>All the doors are closed.</ptxt>
<atten4>
<ptxt>The unlock function operates even when one of the doors is open.</ptxt>
</atten4>
</item>
<item>
<ptxt>The power door lock system is functioning normally.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>The operational range of the wireless transmitter differs depending on the situation.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The operating range differs depending on the user, the way the transmitter is held and the location.</ptxt>
</item>
<item>
<ptxt>In certain areas, the operating range will be reduced due to the vehicle body shape and the influence of the surrounding environment.</ptxt>
</item>
<item>
<ptxt>The weak electric waves from the transmitter may be affected if the area has strong electric waves or noise. The operating range of the transmitter may be reduced or the transmitter may not function.</ptxt>
</item>
<item>
<ptxt>When the battery weakens, the operating range is reduced and the transmitter may not function.</ptxt>
<atten4>
<ptxt>If the transmitter has had prolonged exposure to direct sunlight, such as being left on the instrument panel, the battery may weaken or other problems may occur.</ptxt>
</atten4>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>CHECK WIRELESS DOOR LOCK CONTROL FUNCTIONS</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The switches described below transmit signals and are built into the door control transmitter.</ptxt>
</item>
<item>
<ptxt>The operating range of the transmitter must be taken into account while checks are being made.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Make sure the vehicle is in a condition in which the wireless control functions can be operated (see above). </ptxt>
</step2>
<step2>
<ptxt>Check the basic functions:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Check that all the doors lock when the lock switch is pressed. </ptxt>
</item>
<item>
<ptxt>Check that all the doors unlock when the unlock switch is pressed.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Check the chattering prevention function:</ptxt>
<ptxt>When a switch is pressed, check that the corresponding operation occurs only once. When the switch is held down, check that the corresponding operation occurs only once and does not repeatedly activate. Lastly, when the switch is pressed at 1 second intervals, check that the corresponding operation activates once for each press of the switch.</ptxt>
</step2>
<step2>
<ptxt>Check the automatic locking function:</ptxt>
<ptxt>When all the doors are unlocked with the unlock switch and none of the doors are opened or locked within 30 seconds, check that the doors are relocked automatically.</ptxt>
</step2>
<step2>
<ptxt>Check the switch operation fail-safe function:</ptxt>
<ptxt>If the key is in the ignition key cylinder, check that the doors cannot be locked by the lock switch. However, this does not apply when the system is in recognition code registration mode.</ptxt>
</step2>
<step2>
<ptxt>Check the answer-back function:</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the lock switch is pressed, check that the hazard warning lights flash once and the buzzer sounds once simultaneously with the locking operation of all the doors.</ptxt>
</item>
<item>
<ptxt>When the unlock switch is pressed once, check that the hazard warning lights flash twice and the buzzer sounds twice simultaneously with the unlocking operation of all the doors.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Check the illuminated entry function:</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the map light door switch is on and all the doors are locked, pressing the unlock switch causes the interior light to illuminate and all the doors to unlock simultaneously.</ptxt>
</item>
<item>
<ptxt>If the doors have not been opened, the interior light turns off in approximately 15 seconds.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Check the door ajar warning function:</ptxt>
<list1 type="unordered">
<item>
<ptxt>If a door is open or not completely closed, check that the doors cannot be locked by the lock switch and the buzzer sounds for 5 seconds.</ptxt>
<atten4>
<ptxt>The buzzer stops sounding if one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The buzzer has sounded for 5 seconds.</ptxt>
</item>
<item>
<ptxt>The ignition switch is turned to ACC or to ON.</ptxt>
</item>
<item>
<ptxt>All the doors are closed.</ptxt>
</item>
</list1>
</atten4>
</item>
</list1>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>