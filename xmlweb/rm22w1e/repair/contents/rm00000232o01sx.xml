<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R4_T00K7" variety="T00K7">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM00000232O01SX" category="C" type-id="801O8" name-id="AV6XY-04" from="201308">
<dtccode>74-40</dtccode>
<dtcname>Short in Speaker Circuit</dtcname>
<subpara id="RM00000232O01SX_01" type-id="60" category="03" proc-id="RM22W0E___0000CAY00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>74-40</ptxt>
</entry>
<entry valign="middle">
<ptxt>Short is detected in the speaker output circuit.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Speaker</ptxt>
</item>
<item>
<ptxt>Stereo component amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000232O01SX_02" type-id="32" category="03" proc-id="RM22W0E___0000CAZ00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E155742E04" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000232O01SX_03" type-id="51" category="05" proc-id="RM22W0E___0000CB000001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>After the inspection is completed, clear the DTCs.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000232O01SX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000232O01SX_05_0004" proc-id="RM22W0E___0000CB100001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F3 and F52 amplifier connectors.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs.</ptxt>
</test1>
<test1>
<ptxt>Check if DTC 74-40 is output.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC 74-40 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000232O01SX_05_0005" fin="false">OK</down>
<right ref="RM00000232O01SX_05_0039" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000232O01SX_05_0005" proc-id="RM22W0E___0000CB200001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the F52 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs.</ptxt>
</test1>
<test1>
<ptxt>Check if DTC 74-40 is output.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC 74-40 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000232O01SX_05_0008" fin="false">OK</down>
<right ref="RM00000232O01SX_05_0024" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000232O01SX_05_0008" proc-id="RM22W0E___0000CB300001">
<testtitle>CHECK HARNESS AND CONNECTOR (STEREO COMPONENT AMPLIFIER - SPEAKER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F3 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F3-8 (CTR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F3-7 (CTR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F3-1 (WFL+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F3-5 (WFL-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F3-2 (WFR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F3-6 (WFR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000232O01SX_05_0025" fin="false">OK</down>
<right ref="RM00000232O01SX_05_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000232O01SX_05_0025" proc-id="RM22W0E___0000CB500001">
<testtitle>INSPECT FRONT NO. 1 SPEAKER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I10 and I1 speaker connectors.</ptxt>
<figure>
<graphic graphicname="E144966E35" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Front No. 1 Speaker)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000232O01SX_05_0043" fin="false">OK</down>
<right ref="RM00000232O01SX_05_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000232O01SX_05_0043" proc-id="RM22W0E___0000CB900001">
<testtitle>INSPECT FRONT NO. 4 SPEAKER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F6 speaker connector.</ptxt>
<figure>
<graphic graphicname="E118525E33" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Front No. 4 Speaker)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000232O01SX_05_0037" fin="true">OK</down>
<right ref="RM00000232O01SX_05_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000232O01SX_05_0024" proc-id="RM22W0E___0000CB400001">
<testtitle>CHECK HARNESS AND CONNECTOR (STEREO COMPONENT AMPLIFIER - SPEAKER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F52 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F52-8 (FL-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F52-3 (FL+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F52-9 (FR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F52-10 (FR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F52-5 (RR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F52-12 (RR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F52-4 (RL+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F52-11 (RL-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000232O01SX_05_0028" fin="false">OK</down>
<right ref="RM00000232O01SX_05_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000232O01SX_05_0028" proc-id="RM22W0E___0000CB700001">
<testtitle>INSPECT REAR SPEAKER SET</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the J17 and J15 speaker connectors.</ptxt>
<figure>
<graphic graphicname="E144966E35" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Rear Speaker Set)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000232O01SX_05_0029" fin="false">OK</down>
<right ref="RM00000232O01SX_05_0033" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000232O01SX_05_0027" proc-id="RM22W0E___0000CB600001">
<testtitle>INSPECT FRONT NO. 2 SPEAKER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F8 and F7 speaker connectors.</ptxt>
<figure>
<graphic graphicname="E144969E35" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3 - 4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 - 4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Front No. 2 Speaker)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000232O01SX_05_0046" fin="false">OK</down>
<right ref="RM00000232O01SX_05_0032" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000232O01SX_05_0029" proc-id="RM22W0E___0000CB800001">
<testtitle>INSPECT FRONT NO. 3 SPEAKER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F14 and F13 speaker connectors.</ptxt>
<figure>
<graphic graphicname="E197034E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Front No. 3 Speaker)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000232O01SX_05_0039" fin="false">OK</down>
<right ref="RM00000232O01SX_05_0048" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000232O01SX_05_0046" proc-id="RM22W0E___0000CBA00001">
<testtitle>CHECK HARNESS AND CONNECTOR (FRONT NO. 2 SPEAKER - FRONT NO. 3 SPEAKER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F8, F7, F14 and F13 speaker connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F8-2 (TWL+) - F14-1</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F8-4 (TWL-) - F14-2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F8-2 (TWL+) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F8-4 (TWL-) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F7-2 (TWR+) - F13-1</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F7-4 (TWR-) - F13-2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F7-2 (TWR+) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F7-4 (TWR-) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000232O01SX_05_0014" fin="true">OK</down>
<right ref="RM00000232O01SX_05_0049" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000232O01SX_05_0039">
<testtitle>REPLACE STEREO COMPONENT AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM000003AI901RX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000232O01SX_05_0017">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000232O01SX_05_0016">
<testtitle>REPLACE FRONT NO. 1 SPEAKER ASSEMBLY<xref label="Seep01" href="RM000002MK7024X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000232O01SX_05_0022">
<testtitle>REPLACE FRONT NO. 4 SPEAKER ASSEMBLY<xref label="Seep01" href="RM000000VEO03GX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000232O01SX_05_0037">
<testtitle>REPLACE STEREO COMPONENT AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM000003AI901RX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000232O01SX_05_0019">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000232O01SX_05_0033">
<testtitle>REPLACE REAR SPEAKER SET<xref label="Seep01" href="RM0000014T703SX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000232O01SX_05_0032">
<testtitle>REPLACE FRONT NO. 2 SPEAKER ASSEMBLY<xref label="Seep01" href="RM000000VEO03GX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000232O01SX_05_0048">
<testtitle>REPLACE FRONT NO. 3 SPEAKER ASSEMBLY<xref label="Seep01" href="RM000003AW8016X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000232O01SX_05_0049">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000232O01SX_05_0014">
<testtitle>REPLACE STEREO COMPONENT AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM000003AI901RX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>