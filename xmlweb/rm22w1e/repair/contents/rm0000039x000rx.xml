<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000H" variety="S000H">
<name>3UR-FE FUEL</name>
<ttl id="12008_S000H_7C3H3_T00A6" variety="T00A6">
<name>FUEL SUB TANK</name>
<para id="RM0000039X000RX" category="A" type-id="80001" name-id="FU934-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000039X000RX_01" type-id="01" category="01">
<s-1 id="RM0000039X000RX_01_0024" proc-id="RM22W0E___00004SR00001">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE
</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000028RU04QX"/>)</ptxt>
</content1></s-1>
<s-1 id="RM0000039X000RX_01_0026" proc-id="RM22W0E___000064W00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0025" proc-id="RM22W0E___000064V00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0020">
<ptxt>REMOVE SPARE TIRE</ptxt>
</s-1>
<s-1 id="RM0000039X000RX_01_0023" proc-id="RM22W0E___00004X100000">
<ptxt>REMOVE TAILPIPE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, clamp and gasket.</ptxt>
</s2>
<s2>
<ptxt>Remove the tailpipe from the 2 exhaust pipe supports.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and clamp, and then disconnect the tailpipe from the center exhaust pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039X000RX_01_0021" proc-id="RM22W0E___000064T00001">
<ptxt>REMOVE SPARE WHEEL CARRIER CROSSMEMBER AND SPARE WHEEL CARRIER BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt labeled A, and then loosen the 4 bolts labeled B.</ptxt>
<figure>
<graphic graphicname="A176384E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Slightly raise the spare wheel carrier crossmember, and then remove the 6 bolts labeled C, spare wheel carrier crossmember and 2 brackets.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0022" proc-id="RM22W0E___000064U00001">
<ptxt>REMOVE NO. 1 SPARE WHEEL STOPPER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and spare wheel stopper.</ptxt>
<figure>
<graphic graphicname="A176385" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0019">
<ptxt>REMOVE FUEL TANK CAP ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM0000039X000RX_01_0003" proc-id="RM22W0E___000064F00001">
<ptxt>DISCONNECT FUEL TANK EVAPORATION VENT TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>except G.C.C. Countries:</ptxt>
<ptxt>Disconnect the 2 fuel tank evaporation vent tubes.</ptxt>
<figure>
<graphic graphicname="A312453E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>except G.C.C. Countries</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>for G.C.C. Countries</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>for G.C.C. Countries:</ptxt>
<ptxt>Disconnect the fuel tank evaporation vent tube.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0004" proc-id="RM22W0E___000064G00001">
<ptxt>DISCONNECT FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel hose.</ptxt>
<figure>
<graphic graphicname="A312454E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>except G.C.C. Countries</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>for G.C.C. Countries</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0005" proc-id="RM22W0E___000064H00001">
<ptxt>DISCONNECT FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the breather hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A175076" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0006" proc-id="RM22W0E___000064I00001">
<ptxt>DISCONNECT NO. 3 FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A175077" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0007" proc-id="RM22W0E___000064J00001">
<ptxt>DISCONNECT FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A175078" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0009" proc-id="RM22W0E___000064K00001">
<ptxt>REMOVE FUEL SUB TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the floor No. 3 wire connector.</ptxt>
<figure>
<graphic graphicname="A181396" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a clip remover, detach the 3 wire harness clamps.</ptxt>
</s2>
<s2>
<ptxt>Set a transmission jack underneath the fuel sub tank.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the 2 fuel tank bands.</ptxt>
<figure>
<graphic graphicname="A175079" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the fuel sub tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0010" proc-id="RM22W0E___000064L00001">
<ptxt>REMOVE FLOOR NO. 3 WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the sender gauge connector.</ptxt>
<figure>
<graphic graphicname="A175080" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a clip remover, detach the wire harness clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0011" proc-id="RM22W0E___000064M00001">
<ptxt>REMOVE FUEL SENDER GAUGE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 screws and sender gauge.</ptxt>
<figure>
<graphic graphicname="A175087" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the gasket from the sender gauge.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0016" proc-id="RM22W0E___000064R00001">
<ptxt>REMOVE FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel hose.</ptxt>
<figure>
<graphic graphicname="A312455E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>except G.C.C. Countries</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>for G.C.C. Countries</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0017" proc-id="RM22W0E___000064S00001">
<ptxt>REMOVE FUEL TANK EVAPORATION VENT TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>except G.C.C. Countries:</ptxt>
<ptxt>Remove the 2 fuel tank evaporation vent tubes.</ptxt>
<figure>
<graphic graphicname="A312456E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>except G.C.C. Countries</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>for G.C.C. Countries</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>for G.C.C. Countries:</ptxt>
<ptxt>Remove the fuel tank evaporation vent tube.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0012" proc-id="RM22W0E___000064N00001">
<ptxt>REMOVE FUEL AND EVAPORATION VENT TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 6 bolts and fuel and evaporation vent tube.</ptxt>
<figure>
<graphic graphicname="A176321" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the gasket from the fuel and evaporation vent tube.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0013" proc-id="RM22W0E___000064O00001">
<ptxt>REMOVE FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the breather hose from the fuel sub tank.</ptxt>
<figure>
<graphic graphicname="A175083" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0014" proc-id="RM22W0E___000064P00001">
<ptxt>REMOVE NO. 3 FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel hose from the fuel sub tank.</ptxt>
<figure>
<graphic graphicname="A175084" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000RX_01_0015" proc-id="RM22W0E___000064Q00001">
<ptxt>REMOVE FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the hose from the fuel sub tank.</ptxt>
<figure>
<graphic graphicname="A175085" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>