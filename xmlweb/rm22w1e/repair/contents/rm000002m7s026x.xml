<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3XM_T00QP" variety="T00QP">
<name>HOOD LOCK CONTROL CABLE ASSEMBLY</name>
<para id="RM000002M7S026X" category="A" type-id="80001" name-id="DH3EV-01" from="201301">
<name>REMOVAL</name>
<subpara id="RM000002M7S026X_02" type-id="11" category="10"/>
<subpara id="RM000002M7S026X_01" type-id="01" category="01">
<s-1 id="RM000002M7S026X_01_0021" proc-id="RM22W0E___000049F00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 7 clips and radiator support seal.</ptxt>
<figure>
<graphic graphicname="B180890E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002M7S026X_01_0008" proc-id="RM22W0E___0000ICX00000">
<ptxt>REMOVE FRONT FENDER MAIN SEAL LH (for LHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 3 clips and remove the fender main seal.</ptxt>
<figure>
<graphic graphicname="B181061" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002M7S026X_01_0024" proc-id="RM22W0E___0000ID100000">
<ptxt>REMOVE FRONT FENDER MAIN SEAL RH (for RHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 3 clips and remove the fender main seal.</ptxt>
<figure>
<graphic graphicname="B182558" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002M7S026X_01_0027" proc-id="RM22W0E___000058700000">
<ptxt>REMOVE FRONT BUMPER WINCH COVER SUB-ASSEMBLY (w/ Winch)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws.</ptxt>
<figure>
<graphic graphicname="B302213" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 3 guides and remove front bumper winch cover sub-assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002M7S026X_01_0022" proc-id="RM22W0E___000049G00000">
<ptxt>REMOVE RADIATOR GRILLE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the radiator grille assembly.</ptxt>
<figure>
<graphic graphicname="B302174E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clips and 8 claws, and remove the radiator grille assembly.</ptxt>
</s2>
<s2>
<ptxt>w/ Wide View Front Monitor System:</ptxt>
<s3>
<ptxt>Disconnect the connector.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000002M7S026X_01_0026" proc-id="RM22W0E___00007DN00000">
<ptxt>REMOVE MILLIMETER WAVE RADAR SENSOR ASSEMBLY (w/ Dynamic Radar Cruise Control System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the sensor connector.</ptxt>
<figure>
<graphic graphicname="E239944" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and sensor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002M7S026X_01_0007" proc-id="RM22W0E___0000H0500000">
<ptxt>REMOVE HOOD LOCK CONTROL CABLE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B188474E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and remove the cable cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002M7S026X_01_0023" proc-id="RM22W0E___0000ID000000">
<ptxt>REMOVE HOOD LOCK NUT CAP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, remove the hood lock nut cap as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B188477E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002M7S026X_01_0009" proc-id="RM22W0E___0000H0600000">
<ptxt>REMOVE HOOD LOCK ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and nut.</ptxt>
<figure>
<graphic graphicname="B188475E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the hood lock.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the hood lock control cable.</ptxt>
<figure>
<graphic graphicname="B188476E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002M7S026X_01_0010" proc-id="RM22W0E___0000ICY00000">
<ptxt>REMOVE HOOD LOCK CONTROL LEVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws.</ptxt>
<figure>
<graphic graphicname="B310743" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the hood lock control cable and remove the hood lock lever.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002M7S026X_01_0012" proc-id="RM22W0E___0000ICZ00000">
<ptxt>REMOVE HOOD LOCK CONTROL CABLE ASSEMBLY (for LHD)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B309368" width="7.106578999in" height="4.7836529in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, disconnect the clamps shown in the illustration.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Pull the cable from the engine room and remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002M7S026X_01_0025" proc-id="RM22W0E___0000ID200000">
<ptxt>REMOVE HOOD LOCK CONTROL CABLE ASSEMBLY (for RHD)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B309369" width="7.106578999in" height="4.7836529in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, disconnect the clamps shown in the illustration.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Pull the cable from the engine room and remove it.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>