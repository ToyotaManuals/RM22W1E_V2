<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000001DX1023X" category="D" type-id="303I1" name-id="BCEHI-01" from="201301">
<name>CHECK FOR INTERMITTENT PROBLEMS</name>
<subpara id="RM000001DX1023X_z0" proc-id="RM22W0E___0000ANO00000">
<content5 releasenbr="1">
<step1>
<ptxt>DESCRIPTION</ptxt>
<atten4>
<ptxt>A momentary interruption (open circuit) in the connectors and/or wire harness between the sensors and ECUs can be detected through the ECU data monitor function of the GTS.</ptxt>
</atten4>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the GTS to the DLC3. </ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the GTS on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
</step2>
<step2>
<ptxt>Select areas where momentary interruptions should be monitored.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A momentary interruption (open circuit) cannot be detected for 3 seconds after the ignition switch is turned to ON (initial check).</ptxt>
</item>
<item>
<ptxt>If Error is displayed, check for continuity between the ECUs and the sensors or between ECUs.</ptxt>
</item>
<item>
<ptxt>Error remains displayed on the GTS for 1 second after the harness signal changes from a momentary interruption (open circuit) to normal.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="3.06in"/>
<colspec colname="COL3" colwidth="1.20in"/>
<colspec colname="COL4" colwidth="1.05in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH open circuit detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Speed Open </ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH open circuit detection/ </ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RR Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor RH open circuit detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor LH open circuit detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>EFI Communication Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM communication open circuit detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Deceleration Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Acceleration sensor open circuit detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Master Cylinder Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master cylinder pressure sensor open circuit detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>IG Voltage Value Decreased</ptxt>
</entry>
<entry valign="middle">
<ptxt>IG voltage value decreased detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>IG Voltage Value Increased</ptxt>
</entry>
<entry valign="middle">
<ptxt>IG voltage value increased detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FR Speed Sensor Voltage Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH voltage open circuit detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Speed Sensor Voltage Open </ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH voltage open circuit detection/ </ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RR Speed Sensor Voltage Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor RH voltage open circuit detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL Speed Sensor Voltage Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor LH voltage open circuit detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>M/C Pressure Sensor Noise</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master cylinder pressure sensor noise detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>EFI Communication Invalid</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM communication invalid detection/</ptxt>
<ptxt>Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>While observing the screen, gently jiggle the connectors or wire harnesses between the ECUs and sensors or between ECUs.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Normal display appears.</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>The connector and/or wire harness has a momentary interruption (open circuit) if the display changes. Repair or replace the connectors and/or wire harnesses as one of them is faulty.</ptxt>
</atten3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>