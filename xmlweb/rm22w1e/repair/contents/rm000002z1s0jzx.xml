<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000002Z1S0JZX" category="C" type-id="80380" name-id="ESUAS-04" from="201301" to="201308">
<dtccode>P060D</dtccode>
<dtcname>Internal Control Module Accelerator Pedal Position Performance</dtcname>
<subpara id="RM000002Z1S0JZX_01" type-id="60" category="03" proc-id="RM22W0E___00001V900000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM monitors the input signals of the accelerator pedal position sensor No. 1. When the input signals and control signals deviate, a DTC is stored.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P060D</ptxt>
</entry>
<entry>
<ptxt>Either condition is met (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>There is an ECM main CPU error.</ptxt>
</item>
<item>
<ptxt>There is an ECM sub CPU error.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002Z1S0JZX_07" type-id="73" category="03" proc-id="RM22W0E___00001VB00000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<list1 type="ordered">
<item>
<ptxt>Turn the engine switch off.</ptxt>
</item>
<item>
<ptxt>Disconnect the cable from the negative (-) battery terminal and wait for 1 minute.</ptxt>
</item>
<item>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and wait for 16 seconds or more.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002Z1S0JZX_05" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002Z1S0JZX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002Z1S0JZX_06_0001" proc-id="RM22W0E___00001VA00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the GTS.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal and wait for 1 minute.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Wait 16 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>P060D is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002Z1S0JZX_06_0002" fin="true">OK</down>
<right ref="RM000002Z1S0JZX_06_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Z1S0JZX_06_0002">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ108X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Z1S0JZX_06_0003">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>