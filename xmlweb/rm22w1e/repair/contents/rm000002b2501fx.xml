<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000002B2501FX" category="C" type-id="30361" name-id="ESO1K-07" from="201301" to="201308">
<dtccode>P0660</dtccode>
<dtcname>Intake Manifold Tuning Valve Control Circuit / Open (Bank 1)</dtcname>
<subpara id="RM000002B2501FX_01" type-id="60" category="03" proc-id="RM22W0E___00001K000000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit opens and closes the Intake Air Control Valve (IACV) in response to the engine load in order to increase the intake efficiency (ACIS: Acoustic Control Induction System).</ptxt>
<figure>
<graphic graphicname="A233285E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0660</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The following conditions are met simultaneously for 0.5 seconds or more (2 trip detection logic):</ptxt>
<list1 type="ordered">
<item>
<ptxt>Voltage of acoustic control induction system terminal of the ECM is low when the actuator is off.</ptxt>
</item>
<item>
<ptxt>The engine has been started.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Open or short in intake air control valve circuit</ptxt>
</item>
<item>
<ptxt>Intake air surge tank assembly (intake air control valve assembly)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002B2501FX_02" type-id="32" category="03" proc-id="RM22W0E___00001K100000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A276701E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002B2501FX_04" type-id="51" category="05" proc-id="RM22W0E___00001K600000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. The ECM records vehicle and driving condition information as freeze frame data the moment a DTC is stored. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002B2501FX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002B2501FX_03_0001" proc-id="RM22W0E___00001K200000">
<testtitle>PERFORM ACTIVE TEST USING GTS (OPERATE VSV FOR ACIS)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A233358E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the vacuum hose from port F on the vacuum switching valve (for ACIS).</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Activate the VSV for Intake Control.</ptxt>
</test1>
<test1>
<ptxt>Operate the VSV for ACIS.</ptxt>
</test1>
<test1>
<ptxt>Check the VSV air flow when switching the VSV.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry align="center">
<ptxt>Test Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>VSV is ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air from port E flows out through port F</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>VSV is OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air from port E flows out through air filter</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSV is ON</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSV is OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002B2501FX_03_0005" fin="true">OK</down>
<right ref="RM000002B2501FX_03_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002B2501FX_03_0002" proc-id="RM22W0E___00001K300000">
<testtitle>INSPECT VACUUM SWITCHING VALVE (FOR ACIS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the vacuum switching valve (for ACIS) (See page <xref label="Seep01" href="RM0000031GR01NX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002B2501FX_03_0008" fin="false">OK</down>
<right ref="RM000002B2501FX_03_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002B2501FX_03_0008" proc-id="RM22W0E___00001K500000">
<testtitle>CHECK HARNESS AND CONNECTOR (VACUUM SWITCHING VALVE VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the intake air control valve assembly connector.</ptxt>
<figure>
<graphic graphicname="A219139E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C129-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Intake Air Control Valve Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the intake air control valve assembly connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002B2501FX_03_0003" fin="false">OK</down>
<right ref="RM000002B2501FX_03_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002B2501FX_03_0003" proc-id="RM22W0E___00001K400000">
<testtitle>CHECK HARNESS AND CONNECTOR (VACUUM SWITCHING VALVE - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the intake air control valve assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C129-2 - C46-62 (ACIS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C129-2 or C46-62 (ACIS) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C129-2 - C45-62 (ACIS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C129-2 or C45-62 (ACIS) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the intake air control valve assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002B2501FX_03_0004" fin="true">OK</down>
<right ref="RM000002B2501FX_03_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002B2501FX_03_0005">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ108X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002B2501FX_03_0006">
<testtitle>REPLACE VACUUM SWITCHING VALVE&#13;
(FOR ACIS)<xref label="Seep01" href="RM0000031GQ01SX_01_0016"/>
</testtitle>
</testgrp>
<testgrp id="RM000002B2501FX_03_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (EFI MAIN RELAY - VACUUM SWITCHING VALVE)</testtitle>
</testgrp>
<testgrp id="RM000002B2501FX_03_0004">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002B2501FX_03_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (VACUUM SWITCHING VALVE - ECM)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>