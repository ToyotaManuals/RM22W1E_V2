<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000F" variety="S000F">
<name>1GR-FE FUEL</name>
<ttl id="12008_S000F_7C3GI_T009L" variety="T009L">
<name>FUEL TANK</name>
<para id="RM0000039W903CX" category="A" type-id="80001" name-id="FUAO7-01" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000039W903CX_01" type-id="01" category="01">
<s-1 id="RM0000039W903CX_01_0001" proc-id="RM22W0E___00005TW00001">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000028RU04PX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0003">
<ptxt>REMOVE FUEL TANK CAP ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM0000039W903CX_01_0004" proc-id="RM22W0E___00005TY00001">
<ptxt>REMOVE NO. 1 FUEL TANK PROTECTOR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and No. 1 fuel tank protector sub-assembly.</ptxt>
<figure>
<graphic graphicname="A175014" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0015" proc-id="RM22W0E___00005U200001">
<ptxt>DISCONNECT FUEL TANK MAIN TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A308904E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nylon Tube</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>except G.C.C. Countries</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>for G.C.C. Countries</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull Up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
</s2>
<s2>
<ptxt>Pull up the retainer and disconnect the fuel tank main tube sub-assembly.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0059" proc-id="RM22W0E___00005UF00001">
<ptxt>DRAIN FUEL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</s2>
<s2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Turn the GTS on.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Fuel Pump / Speed.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Do not smoke or be near an open flame when working on the fuel system.</ptxt>
</item>
<item>
<ptxt>Secure good ventilation.</ptxt>
</item>
<item>
<ptxt>Keep gasoline away from rubber or leather parts.</ptxt>
</item>
</list1>
</atten2>
<atten4>
<ptxt>If the fuel pump does not operate, remove the fuel tube joint clip and disconnect the fuel tank return tube, and drain fuel from the port shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A274194E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Port</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0063" proc-id="RM22W0E___00005UG00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0002" proc-id="RM22W0E___00005TX00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0041" proc-id="RM22W0E___00005U800001">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY RH (for 60/40 Split Seat Type 40 Side)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000391400RX_01_0008"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0042" proc-id="RM22W0E___00005U900001">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY LH (for 60/40 Split Seat Type 60 Side)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000390Z00RX_01_0005"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0043" proc-id="RM22W0E___00005UA00001">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>for Face to Face Seat Type (See page <xref label="Seep01" href="RM00000311C003X_02_0002"/>)</ptxt>
</item>
<item>
<ptxt>except Face to Face Seat Type (See page <xref label="Seep02" href="RM00000391S00ZX_01_0003"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0052" proc-id="RM22W0E___00005UB00001">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ Sliding Roof (See page <xref label="Seep01" href="RM0000038MO00UX_01_0020"/>)</ptxt>
</item>
<item>
<ptxt>w/o Sliding Roof (See page <xref label="Seep02" href="RM0000038MO00VX_01_0020"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0054" proc-id="RM22W0E___00005UC00001">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ Sliding Roof (See page <xref label="Seep01" href="RM0000038MO00UX_01_0022"/>)</ptxt>
</item>
<item>
<ptxt>w/o Sliding Roof (See page <xref label="Seep02" href="RM0000038MO00VX_01_0022"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0057" proc-id="RM22W0E___00005UD00001">
<ptxt>REMOVE FRONT FLOOR CARPET ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E157065" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Turn over the floor carpet.</ptxt>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<atten4>
<ptxt>Fold back the floor carpet so that the air duct can be removed.</ptxt>
</atten4>
<ptxt>w/o Rear Air Conditioning System:</ptxt>
<atten4>
<ptxt>Fold back the floor carpet so that the service hole cover can be removed.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0058" proc-id="RM22W0E___00005UE00001">
<ptxt>REMOVE REAR FLOOR NO. 2 SERVICE HOLE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<ptxt>Remove the 2 screws and air duct.</ptxt>
<figure>
<graphic graphicname="A181426" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the rear floor No. 2 service hole cover.</ptxt>
<figure>
<graphic graphicname="A181427" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the fuel pump and sender gauge connector.</ptxt>
<figure>
<graphic graphicname="A274193" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0064" proc-id="RM22W0E___0000K6I00000">
<ptxt>DISCONNECT NO. 2 FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for G.C.C. Countries:</ptxt>
<figure>
<graphic graphicname="A308892" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Detach the fuel tube clamp.</ptxt>
</s3>
<s3>
<ptxt>Slide the clip and disconnect the No. 2 fuel tank breather hose from the pipe.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0016" proc-id="RM22W0E___00005U300001">
<ptxt>DISCONNECT FUEL TANK RETURN TUBE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A308905E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nylon Tube</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>except G.C.C. Countries</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>for G.C.C. Countries</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull Up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
</s2>
<s2>
<ptxt>Pull up the retainer and disconnect the fuel tank return tube.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0065" proc-id="RM22W0E___0000K6J00000">
<ptxt>DISCONNECT NO. 1 FUEL EMISSION TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for G.C.C. Countries:</ptxt>
<figure>
<graphic graphicname="A308891E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Detach the fuel tube clamp.</ptxt>
</s3>
<s3>
<ptxt>Pull up the retainer and disconnect the No. 1 fuel emission tube sub-assembly.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull Up</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
<s2>
<ptxt>except G.C.C. Countries:</ptxt>
<figure>
<graphic graphicname="A310181E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Detach the fuel tube clamp.</ptxt>
</s3>
<s3>
<ptxt>Pull the retainer up while pinching it on both sides by hand as shown in the illustration, and disconnect the No. 1 fuel emission tube sub-assembly.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull Up</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0066" proc-id="RM22W0E___0000K6K00000">
<ptxt>DISCONNECT FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for G.C.C. Countries:</ptxt>
<figure>
<graphic graphicname="A308893" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Detach the fuel tube clamp.</ptxt>
</s3>
<s3>
<ptxt>Slide the clip and disconnect the fuel hose from the pipe.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0017" proc-id="RM22W0E___00005U400001">
<ptxt>DISCONNECT NO. 2 FUEL TANK MAIN TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A308906E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nylon Tube</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>except G.C.C. Countries</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>for G.C.C. Countries</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull Up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
</s2>
<s2>
<ptxt>Pull up the retainer and disconnect the No. 2 fuel tank main tube sub-assembly.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0018" proc-id="RM22W0E___00005U500001">
<ptxt>DISCONNECT NO. 2 FUEL TANK BREATHER TUBE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>except G.C.C. Countries:</ptxt>
<figure>
<graphic graphicname="A310185E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Detach the fuel tube clamp.</ptxt>
</s3>
<s3>
<ptxt>Pull up the retainer and disconnect the No. 2 fuel tank breather tube.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull Up</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0019" proc-id="RM22W0E___00005U600001">
<ptxt>DISCONNECT FUEL TANK BREATHER TUBE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the fuel tube clamp.</ptxt>
<figure>
<graphic graphicname="A175065E03" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Pull up the retainer and disconnect the fuel tank breather tube from the fuel tank to filler pipe sub-assembly.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not use any tools in this procedure.</ptxt>
</item>
<item>
<ptxt>Check for any dirt and foreign matter contamination in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-rings or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull Up</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0020" proc-id="RM22W0E___00005U700001">
<ptxt>DISCONNECT FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the hose clamp and disconnect the fuel tank to filler pipe hose from the fuel tank to filler pipe sub-assembly.</ptxt>
<figure>
<graphic graphicname="A175067" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0010" proc-id="RM22W0E___00005TZ00001">
<ptxt>REMOVE FUEL TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place an engine lifter under the fuel tank sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts, 2 clips, 2 pins and 2 fuel tank band sub-assemblies.</ptxt>
<figure>
<graphic graphicname="A175024" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Slowly lower the engine lifter slightly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0067" proc-id="RM22W0E___0000K6L00000">
<ptxt>REMOVE FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for G.C.C. Countries:</ptxt>
<ptxt>Slide the clip and remove the fuel hose from the fuel suction tube sub-assembly.</ptxt>
<figure>
<graphic graphicname="A308896" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0021" proc-id="RM22W0E___00005RS00001">
<ptxt>REMOVE FUEL TANK MAIN TUBE, FUEL TANK RETURN TUBE AND NO. 2 FUEL TANK MAIN TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 tube joint clips and pull out the 3 fuel tube joints.</ptxt>
<figure>
<graphic graphicname="A176319E05" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Remove any dirt and foreign matter on the fuel tube joint before performing this work.</ptxt>
</item>
<item>
<ptxt>Do not allow any scratches or foreign matter on the parts when disconnecting them, as the fuel tube joint contains the O-rings that seal the plug.</ptxt>
</item>
<item>
<ptxt>Perform this work by hand. Do not use any tools.</ptxt>
</item>
<item>
<ptxt>Do not forcibly bend, twist or turn the nylon tube.</ptxt>
</item>
<item>
<ptxt>Protect the disconnected part by covering it with a plastic bag and tape after disconnecting the fuel tubes.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.40in"/>
<colspec colname="COL2" colwidth="3.73in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tube Joint Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Suction Plate Sub-assembly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>except G.C.C. Countries:</ptxt>
<figure>
<graphic graphicname="A310189E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Detach the clamp and remove the fuel tank main tube sub-assembly.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Tank Main Tube Sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Fuel Tank Return Tube Sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>No. 2 Fuel Tank Main Tube Sub-assembly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Detach the clamp and remove the fuel tank return tube sub-assembly.</ptxt>
</s3>
<s3>
<ptxt>Detach the 2 clamps and remove the No. 2 fuel tank main tube sub-assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for G.C.C. Countries:</ptxt>
<figure>
<graphic graphicname="A308897E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Detach the clamp and remove the fuel tank main tube sub-assembly.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Tank Main Tube Sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Fuel Tank Return Tube Sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>No. 2 Fuel Tank Main Tube Sub-assembly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Detach the clamp and remove the fuel tank return tube sub-assembly.</ptxt>
</s3>
<s3>
<ptxt>Detach the 2 clamps and remove the No. 2 fuel tank main tube sub-assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0062" proc-id="RM22W0E___00005RQ00001">
<ptxt>REMOVE FUEL SUCTION WITH PUMP AND GAUGE TUBE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set SST on the retainer.</ptxt>
<figure>
<graphic graphicname="A260062E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Securely attach the claws of SST to the protrusion of the retainer and fix SST in place.</ptxt>
</item>
<item>
<ptxt>Install SST while pressing the claws of SST against the retainer (towards the center of SST).</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Using SST, loosen the retainer.</ptxt>
<figure>
<graphic graphicname="A260063E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not use any tools other than those specified in this operation. Damage to the retainer or the fuel tank may result.</ptxt>
</item>
<item>
<ptxt>While pressing down on SST so that the claws of SST do not slip off the protrusion of the retainer, turn the retainer counterclockwise to loosen it.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>The protrusion of the retainer fits in the ends of SST.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the retainer while holding the fuel suction tube assembly by hand.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the fuel tank.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039W903CX_01_0013" proc-id="RM22W0E___00005U000001">
<ptxt>REMOVE FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the hose from the fuel tank.</ptxt>
<figure>
<graphic graphicname="A176316" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W903CX_01_0014" proc-id="RM22W0E___00005U100001">
<ptxt>REMOVE NO. 1 FUEL TANK HEAT INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel tube clamp from the heat insulator.</ptxt>
<figure>
<graphic graphicname="A310190E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>for G.C.C. Countries</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using needle-nose pliers, remove the 4 clips shown in the illustration and then remove the heat insulator.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>