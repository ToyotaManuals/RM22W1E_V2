<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SE_T00LH" variety="T00LH">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000003DGA01BX" category="J" type-id="8038W" name-id="NW03S-09" from="201301">
<dtccode/>
<dtcname>Rain Sensor LIN Communication Malfunction</dtcname>
<subpara id="RM000003DGA01BX_01" type-id="60" category="03" proc-id="RM22W0E___0000E3W00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The LIN communication of the components related to the rain sensor occurs between the rain sensor and windshield wiper ECU.</ptxt>
</content5>
</subpara>
<subpara id="RM000003DGA01BX_02" type-id="32" category="03" proc-id="RM22W0E___0000E3X00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E234443E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003DGA01BX_03" type-id="51" category="05" proc-id="RM22W0E___0000E3Y00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot:</ptxt>
<ptxt>Connect the intelligent tester to the vehicle, and turn a courtesy switch on and off at 1.5 second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003DGA01BX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003DGA01BX_04_0001" proc-id="RM22W0E___0000E3Z00000">
<testtitle>CHECK HARNESS AND CONNECTOR (RAIN SENSOR - WIND SHIELD WIPER ECU AND OVERHEAD JUNCTION BLOCK)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B188527E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the E85 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the R11 sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the R7 junction block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R11-3 (MPX) - E85-7 (MPX1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R11-4 (SIG) - R7-29 (IG2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R11-1 (ES) - R7-4 (GND2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R11-3 (MPX) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R11-4 (SIG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R11-1 (ES) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the R7 junction block connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R11-4 (SIG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003DGA01BX_04_0002" fin="false">OK</down>
<right ref="RM000003DGA01BX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003DGA01BX_04_0002" proc-id="RM22W0E___0000E4000000">
<testtitle>REPLACE RAIN SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the rain sensor with a new one (See page <xref label="Seep01" href="RM000003CEV00HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the rain sensor function is normal.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Rain sensor function is normal</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003DGA01BX_04_0005" fin="true">OK</down>
<right ref="RM000003DGA01BX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003DGA01BX_04_0003">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003DGA01BX_04_0004">
<testtitle>REPLACE WINDSHIELD WIPER ECU<xref label="Seep01" href="RM000003CET006X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003DGA01BX_04_0005">
<testtitle>END (RAIN SENSOR IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>