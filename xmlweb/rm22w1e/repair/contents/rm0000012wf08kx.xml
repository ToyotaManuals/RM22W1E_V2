<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM0000012WF08KX" category="T" type-id="3001H" name-id="ES00MT-225" from="201308">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM0000012WF08KX_z0" proc-id="RM22W0E___00002IP00001">
<content5 releasenbr="2">
<atten4>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>ECD System</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="8" colsep="1" valign="middle" align="left">
<ptxt>Engine does not crank (does not start)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Battery</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000017VF03YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Starter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000032G500BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Starter relay</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000003BIA02HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Park/neutral position switch</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002BKX03MX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Clutch start switch (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000032S3008X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Clutch start switch (for RHD)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000003D8H002X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Immobiliser system (w/ Entry and start system)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000QY40DOX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Immobiliser system (w/o Entry and start system)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM000001TC903JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Entry and start system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="X39400000IL04KJ" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="13" colsep="1" valign="middle" align="left">
<ptxt>Difficult to start with cold engine</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Engine difficult to start or stalling</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000TJ50D0X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Cranking holding function circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WZ30ASX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Pre-heating control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000W0B097X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel filter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031ER003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Compression</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000RPP01UX_01_0008" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel supply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Diesel throttle body</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000032GC003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>EDU relay</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000003BLB02YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel with low cetane number used</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel frozen</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Air mixed into fuel pipe</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="11" colsep="1" valign="middle" align="left">
<ptxt>Difficult to start with hot engine</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Engine difficult to start or stalling</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000TJ50D0X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Cranking holding function circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WZ30ASX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel filter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031ER003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Compression</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000RPP01UX_01_0008" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel sypply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Diesel throttle body</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000032GC003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>EDU relay</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000003BLB02YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Air mixed into fuel pipe</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Battery deteriorated</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="9" colsep="1" valign="middle" align="left">
<ptxt>Engine stalls soon after starting</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Engine difficult to start or stalling</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000TJ50D0X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel filter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031ER003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001DN80A9X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel supply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Diesel throttle body</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000032GC003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Air mixed into fuel pipe</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Immobiliser system activated</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Engine stalls (not including the condition listed above)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Engine difficult to start or stalling</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000TJ50D0X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="8" colsep="1" valign="middle" align="left">
<ptxt>Abnormal initial idling (poor idling)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Rough idling or excessive engine vibration</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000W0C0D6X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel filter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031ER003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel supply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Mass air flow meter deteriorated</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel with low cetane number used</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>A/C signal circuit to ECM open and generator signal circuit open</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="8" colsep="1" valign="middle" align="left">
<ptxt>High engine idling speed</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Rough idling or excessive engine vibration</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000W0C0D6X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>A/C signal circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002LIP04HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Cranking holding function circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WZ30ASX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel supply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Speed sensor circuit open</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000012ME0M6X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Accelerator pedal</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000284B00JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="10" colsep="1" valign="middle" align="left">
<ptxt>Low engine idling speed (poor idling)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Rough idling or excessive engine vibration</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000W0C0D6X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>A/C signal circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002LIP04HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>EGR system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000141502YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Compression</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000RPP01UX_01_0008" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel line (air bleeding)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel supply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Diesel throttle body</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000032GC003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Mass air flow meter deteriorated</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Rough idling (poor idling)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Rough idling or excessive vibration</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000W0C0D6X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="7" colsep="1" valign="middle" align="left">
<ptxt>Hunting with hot engine (poor idling)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001DN80A9X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Compression</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000RPP01UX_01_0008" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel line (air bleeding)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel supply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Diesel throttle body</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000032GC003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="8" colsep="1" valign="middle" align="left">
<ptxt>Hunting with cold engine (poor idling)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001DN80A9X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Compression</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000RPP01UX_01_0008" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel line (air bleeding)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel supply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Diesel throttle body</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000032GC003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel with low cetane number used</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="14" colsep="1" valign="middle" align="left">
<ptxt>Hesitation/Poor acceleration (poor driveability)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Lack of power or hesitation</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000W0E0CWX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel filter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031ER003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>EGR system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000141502YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Compression</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000RPP01UX_01_0008" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel supply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Diesel throttle body</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000032GC003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Accelerator pedal</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000284B00JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Turbocharger system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031FN006X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Mass air flow meter deteriorated</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Intake system blockage</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Exhaust system blockage</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Brake override system</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM000004G8F0O9X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Knocking (poor driveability)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Engine knocking or rattling</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000W0D0CNX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="4" colsep="1" valign="middle" align="left">
<ptxt>Black smoke emitted (poor driveability)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Black smoke emitted</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000TIR0FYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>EGR system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000141502YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Accumulation in exhaust system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Exhaust system blockage</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="8" colsep="1" valign="middle" align="left">
<ptxt>White smoke emitted (poor driveability)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>EGR system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000141502YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel filter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031ER003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel supply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Diesel throttle body</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000032GC003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Pre-heating control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000W0B097X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Low quality fuel</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="5" colsep="1" valign="middle" align="left">
<ptxt>Surging/Hunting (poor driveability)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EF003X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000013FU06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Mass air flow meter deteriorated</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel supply pump</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EJ004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031EN004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Turbocharger system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000031FN006X" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>