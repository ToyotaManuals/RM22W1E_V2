<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3P7_T00IA" variety="T00IA">
<name>FRONT SPEED SENSOR</name>
<para id="RM000001B2J01UX" category="A" type-id="80001" name-id="BCGJC-01" from="201308">
<name>REMOVAL</name>
<subpara id="RM000001B2J01UX_02" type-id="01" category="01">
<s-1 id="RM000001B2J01UX_02_0011" proc-id="RM22W0E___0000A8200001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001B2J01UX_02_0008" proc-id="RM22W0E___0000A8100001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001B2J01UX_02_0007">
<ptxt>REMOVE FRONT WHEEL</ptxt>
</s-1>
<s-1 id="RM000001B2J01UX_02_0004" proc-id="RM22W0E___0000A7Y00001">
<ptxt>REMOVE FRONT SKID CONTROL SENSOR WIRE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector from the front speed sensor.</ptxt>
<figure>
<graphic graphicname="C177132" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and 2 harness clamps.</ptxt>
<figure>
<graphic graphicname="C170882" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>w/o AHC:</ptxt>
<ptxt>Disconnect the connector and then remove the bolt, harness clamp and sensor wire.</ptxt>
<figure>
<graphic graphicname="C170883E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>w/ AHC:</ptxt>
<ptxt>Disconnect the 2 connectors and then remove the bolt, harness clamp and sensor wire.</ptxt>
<figure>
<graphic graphicname="C170884E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001B2J01UX_02_0001" proc-id="RM22W0E___0000A7X00001">
<ptxt>REMOVE FRONT SPEED SENSOR LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and speed sensor from the knuckle.</ptxt>
<figure>
<graphic graphicname="C271342" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>Pull out the sensor while trying as much as possible not to rotate it.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001B2J01UX_02_0005" proc-id="RM22W0E___0000A7Z00001">
<ptxt>REMOVE FRONT SKID CONTROL SENSOR WIRE RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector from the front speed sensor.</ptxt>
<figure>
<graphic graphicname="C177133" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and harness clamp.</ptxt>
</s2>
<s2>
<ptxt>w/o AHC:</ptxt>
<s3>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="C172203" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Detach the connector from the skid control sensor clamp.</ptxt>
</s3>
<s3>
<ptxt>Remove the bolt and skid control sensor clamp.</ptxt>
</s3>
<s3>
<ptxt>Remove the 2 bolts, 2 harness clamps and sensor wire.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ AHC:</ptxt>
<s3>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="C170887" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Detach the connector from the skid control sensor clamp.</ptxt>
</s3>
<s3>
<ptxt>Remove the bolt and skid control sensor clamp.</ptxt>
</s3>
<s3>
<ptxt>Remove the 2 bolts and 2 harness clamps.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the connector and then remove the bolt, harness clamp and sensor wire.</ptxt>
<figure>
<graphic graphicname="C170888" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001B2J01UX_02_0006" proc-id="RM22W0E___0000A8000001">
<ptxt>REMOVE FRONT SPEED SENSOR RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and speed sensor from the knuckle.</ptxt>
<figure>
<graphic graphicname="C271343" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>Pull out the sensor while trying as much as possible not to rotate it.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>