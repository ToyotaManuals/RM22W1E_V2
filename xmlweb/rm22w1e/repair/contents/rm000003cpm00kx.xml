<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000003CPM00KX" category="J" type-id="803TN" name-id="ACEJ4-02" from="201301" to="201308">
<dtccode/>
<dtcname>Cooling Box Magnetic Valve Circuit</dtcname>
<subpara id="RM000003CPM00KX_01" type-id="60" category="03" proc-id="RM22W0E___0000H8B00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The cooler expansion valve (cooler magnetic valve) is controlled by the air conditioning amplifier assembly (for cool box).</ptxt>
</content5>
</subpara>
<subpara id="RM000003CPM00KX_02" type-id="32" category="03" proc-id="RM22W0E___0000H8C00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E160122E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003CPM00KX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003CPM00KX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003CPM00KX_04_0001" proc-id="RM22W0E___0000H8D00000">
<testtitle>INSPECT FUSE (A/C IG)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the A/C IG fuse from the cowl side junction block LH.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A/C IG fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003CPM00KX_04_0002" fin="false">OK</down>
<right ref="RM000003CPM00KX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CPM00KX_04_0002" proc-id="RM22W0E___0000H8E00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 COOLER WIRING HARNESS - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 cooler wiring harness sub-assembly connector.</ptxt>
<figure>
<graphic graphicname="E158385E14" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003CPM00KX_04_0003" fin="false">OK</down>
<right ref="RM000003CPM00KX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CPM00KX_04_0003" proc-id="RM22W0E___0000H8F00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 COOLER WIRING HARNESS - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 cooler wiring harness sub-assembly connector.</ptxt>
<figure>
<graphic graphicname="E158385E15" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>6 (L) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003CPM00KX_04_0004" fin="false">OK</down>
<right ref="RM000003CPM00KX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CPM00KX_04_0004" proc-id="RM22W0E___0000H8G00000">
<testtitle>CHECK NO. 1 COOLER WIRING HARNESS SUB-ASSEMBLY (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the No. 1 cooler wiring harness sub-assembly with a normal one and check that the condition returns to normal.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Same problem does not occur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003CPM00KX_04_0008" fin="true">OK</down>
<right ref="RM000003CPM00KX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CPM00KX_04_0005" proc-id="RM22W0E___0000H8H00000">
<testtitle>CHECK COOLER MAGNETIC VALVE (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the cooler magnetic valve (See page <xref label="Seep01" href="RM000003B6T00JX"/>).</ptxt>
<atten4>
<ptxt>Since the cooler magnetic valve cannot be inspected while it is removed from the vehicle, replace the cooler magnetic valve with a normal one and check that the condition returns to normal.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Same problem does not occur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003CPM00KX_04_0010" fin="true">OK</down>
<right ref="RM000003CPM00KX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003CPM00KX_04_0006">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000003CPM00KX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003CPM00KX_04_0008">
<testtitle>REPLACE NO. 1 COOLER WIRING HARNESS SUB-ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000003CPM00KX_04_0009">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY (for Cool Box)<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003CPM00KX_04_0010">
<testtitle>REPLACE COOLER MAGNETIC VALVE<xref label="Seep01" href="RM000003B6T00JX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>