<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000T8P1BJX" category="C" type-id="302I5" name-id="ES16DA-002" from="201308">
<dtccode>P0560</dtccode>
<dtcname>System Voltage</dtcname>
<subpara id="RM000000T8P1BJX_01" type-id="60" category="03" proc-id="RM22W0E___000026V00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The battery supplies electricity to the ECM even when the engine switch is off. This power allows the ECM to store data such as DTC history, freeze frame data and fuel trim values. If the battery voltage falls below a minimum level, the memory is cleared and the ECM determines that there is a malfunction in the power supply circuit. When the engine is next started, the ECM illuminates the MIL and stores the DTC.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0560</ptxt>
</entry>
<entry valign="middle">
<ptxt>An open in the ECM back up power source circuit (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in back up power source circuit</ptxt>
</item>
<item>
<ptxt>Battery</ptxt>
</item>
<item>
<ptxt>Battery terminals</ptxt>
</item>
<item>
<ptxt>EFI MAIN fuse</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTC P0560 is stored, the ECM does not store other DTCs or the data stored in the ECM is partly cleared.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000T8P1BJX_05" type-id="32" category="03" proc-id="RM22W0E___000026W00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A164743E17" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000T8P1BJX_06" type-id="51" category="05" proc-id="RM22W0E___000026X00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000T8P1BJX_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000T8P1BJX_07_0006" proc-id="RM22W0E___000027000001">
<testtitle>INSPECT BATTERY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the battery is not depleted (See page <xref label="Seep01" href="RM000001AR90APX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Battery is not depleted.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8P1BJX_07_0010" fin="false">OK</down>
<right ref="RM000000T8P1BJX_07_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8P1BJX_07_0010" proc-id="RM22W0E___000027100001">
<testtitle>CHECK BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the battery terminals are not loose or corroded.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Battery terminals are not loose or corroded.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8P1BJX_07_0001" fin="false">OK</down>
<right ref="RM000000T8P1BJX_07_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8P1BJX_07_0001" proc-id="RM22W0E___000026Y00001">
<testtitle>INSPECT ECM (BATT VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A208653E15" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the A38 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry>
<ptxt>Tester Connection</ptxt>
</entry>
<entry>
<ptxt>Condition</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="left">
<ptxt>A38-1 (BATT) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8P1BJX_07_0012" fin="false">OK</down>
<right ref="RM000000T8P1BJX_07_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8P1BJX_07_0012" proc-id="RM22W0E___000027200001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK17ZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off and turn the tester off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0560 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000T8P1BJX_07_0004" fin="true">A</down>
<right ref="RM000000T8P1BJX_07_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000T8P1BJX_07_0008">
<testtitle>REPLACE BATTERY</testtitle>
</testgrp>
<testgrp id="RM000000T8P1BJX_07_0011">
<testtitle>REPAIR BATTERY TERMINAL</testtitle>
</testgrp>
<testgrp id="RM000000T8P1BJX_07_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000T8P1BJX_07_0009">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ13NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000T8P1BJX_07_0004">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292039X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>