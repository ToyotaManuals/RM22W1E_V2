<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000B" variety="S000B">
<name>1UR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000B_7C3F1_T0084" variety="T0084">
<name>DRIVE BELT</name>
<para id="RM000002BOF023X" category="A" type-id="30014" name-id="EMBHI-02" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000002BOF023X_01" type-id="01" category="01">
<s-1 id="RM000002BOF023X_01_0001" proc-id="RM22W0E___00004EL00000">
<ptxt>INSTALL FAN AND GENERATOR V BELT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the fan and generator V belt onto every part.</ptxt>
<figure>
<graphic graphicname="A163771E04" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Water Pump Pulley</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fan Pulley</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Idler Pulley</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vane Pump Pulley</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Generator Pulley</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>V-ribbed Belt Tensioner</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Crankshaft Pulley</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cooler Compressor Pulley</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>While turning the belt tensioner counterclockwise, remove the pin.</ptxt>
<atten3>
<ptxt>Make sure that the fan and generator V belt is properly set on each pulley.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Check that the belt fits properly in the ribbed grooves.</ptxt>
<atten4>
<ptxt>Make sure to check by hand that the belt has not slipped out of the grooves on the bottom of the pulley.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BOF023X_01_0012" proc-id="RM22W0E___000013N00000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover sub-assembly with the 10 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002BOF023X_01_0013" proc-id="RM22W0E___000011S00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BOF023X_01_0014" proc-id="RM22W0E___000011Q00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly RH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BOF023X_01_0009" proc-id="RM22W0E___000013800000">
<ptxt>INSTALL V-BANK COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 3 V-bank cover grommets with the 3 pins, and press down on the V-bank cover to attach the pins.</ptxt>
<figure>
<graphic graphicname="A274416E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>