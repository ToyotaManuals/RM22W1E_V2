<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MR_T00FU" variety="T00FU">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 3UR-FE)</name>
<para id="RM000000W8B06LX" category="C" type-id="302GK" name-id="AT6HS-06" from="201308">
<dtccode>P0979</dtccode>
<dtcname>Shift Solenoid "C" Control Circuit Low (Shift Solenoid Valve S3)</dtcname>
<dtccode>P0980</dtccode>
<dtcname>Shift Solenoid "C" Control Circuit High (Shift Solenoid Valve S3)</dtcname>
<subpara id="RM000000W8B06LX_01" type-id="60" category="03" proc-id="RM22W0E___00008DT00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Shifting from 1st to 6th is performed in combination with ON and OFF operation of the shift solenoid valves SL1, SL2, S1, S2, S3, S4 and SR, which are controlled by the ECM. If an open or short circuit occurs in any of the shift solenoid valves, the ECM controls the remaining normal shift solenoid valves to allow the vehicle to be operated safely. Also, the ECM stops sending the current to the open or short circuited solenoid (See page <xref label="Seep01" href="RM000000O8L0P6X"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.19in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0979</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM detects short in solenoid valve S3 circuit 2 times when solenoid valve S3 is operated (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in shift solenoid valve S3 circuit</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve S3</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0980</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM detects open in solenoid valve S3 circuit 2 times when solenoid valve S3 is not operated (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in shift solenoid valve S3 circuit</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve S3</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W8B06LX_02" type-id="64" category="03" proc-id="RM22W0E___00008DU00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>These DTCs indicate an open or short in the shift solenoid valve S3 circuit. When there is an open or short circuit in any shift solenoid valve circuit, the ECM detects the problem, illuminates the MIL and stores the DTC. When the shift solenoid valve S3 is ON, if its resistance is 8 Ω or less, the ECM determines there is a short in the shift solenoid valve S3 circuit.</ptxt>
<ptxt>When the shift solenoid valve S3 is OFF, if its resistance is 100 kΩ or more, the ECM determines there is an open in the shift solenoid valve S3 circuit (See page <xref label="Seep01" href="RM000000O8L0P6X"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000W8B06LX_07" type-id="32" category="03" proc-id="RM22W0E___00008DV00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C214968E13" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W8B06LX_08" type-id="51" category="05" proc-id="RM22W0E___00008DW00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Shift solenoid valve S3 is turned ON/OFF normally when the shift lever is in D.</ptxt>
<table pgwide="1">
<tgroup cols="7">
<colspec colname="COL1" align="left" colwidth="1.13in"/>
<colspec colname="COL2" colwidth="0.99in"/>
<colspec colname="COL3" colwidth="0.99in"/>
<colspec colname="COL4" colwidth="0.99in"/>
<colspec colname="COL5" colwidth="0.99in"/>
<colspec colname="COL6" colwidth="0.99in"/>
<colspec colname="COL7" colwidth="1.00in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>ECM gear shift command</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6th</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shift solenoid valve S3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM000000W8B06LX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W8B06LX_09_0001" proc-id="RM22W0E___00008DX00001">
<testtitle>INSPECT NO. 1 TRANSMISSION WIRE (SHIFT SOLENOID VALVE S3)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 transmission wire connector.</ptxt>
<figure>
<graphic graphicname="C214326E35" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>7 (S3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 15 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(No. 1 Transmission Wire)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W8B06LX_09_0002" fin="false">OK</down>
<right ref="RM000000W8B06LX_09_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8B06LX_09_0002" proc-id="RM22W0E___00008DY00001">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 TRANSMISSION WIRE - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="C219456E23" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>C45-3 (S3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 15 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W8B06LX_09_0005" fin="true">OK</down>
<right ref="RM000000W8B06LX_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8B06LX_09_0008" proc-id="RM22W0E___00008C300001">
<testtitle>INSPECT SHIFT SOLENOID VALVE S3
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the shift solenoid valve S3.</ptxt>
<figure>
<graphic graphicname="C209925E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Shift solenoid valve S3 connector terminal - Shift solenoid valve S3 body</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F) </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 15 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Apply 12 V battery voltage to the shift solenoid valve and check that the valve moves and makes an operating noise.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery positive (+) → Shift solenoid valve S3 connector</ptxt>
</item>
<item>
<ptxt>Battery negative (-) → Shift solenoid valve S3 body</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Valve moves and makes an operating noise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Shift Solenoid Valve S3)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6><res>
<down ref="RM000000W8B06LX_09_0004" fin="true">OK</down>
<right ref="RM000000W8B06LX_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8B06LX_09_0004">
<testtitle>REPAIR OR REPLACE NO. 1 TRANSMISSION WIRE<xref label="Seep01" href="RM0000013C104UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8B06LX_09_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292039X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8B06LX_09_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W8B06LX_09_0007">
<testtitle>REPLACE SHIFT SOLENOID VALVE S3<xref label="Seep01" href="RM000000O9L06JX_02_0010"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>