<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3Y0_T00R3" variety="T00R3">
<name>TAIL GATE CLOSER SYSTEM</name>
<para id="RM000003G3100DX" category="J" type-id="803VT" name-id="DH3EX-02" from="201308">
<dtccode/>
<dtcname>Tail Gate Closer does not Operate</dtcname>
<subpara id="RM000003G3100DX_02" type-id="32" category="03" proc-id="RM22W0E___0000IL200001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B312263E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003G3100DX_03" type-id="51" category="05" proc-id="RM22W0E___0000IL300001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003G3100DX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003G3100DX_04_0002" proc-id="RM22W0E___0000IL400001">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 2 MAIN BODY ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B115211E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the No. 2 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>L51-5 (B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L51-6 (B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>L51-7 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 2 Main Body ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000003G3100DX_04_0004" fin="false">OK</down>
<right ref="RM000003G3100DX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003G3100DX_04_0004" proc-id="RM22W0E___0000IL500001">
<testtitle>INSPECT LOWER TAIL GATE LOCK ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LH:</ptxt>
<test2>
<ptxt>Remove the lower tail gate lock assembly LH (See page <xref label="Seep01" href="RM000001BLH07NX"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the lower tail gate lock assembly LH (See page <xref label="Seep03" href="RM000003GFE00AX"/>).</ptxt>
</test2>
</test1>
<test1>
<ptxt>for RH:</ptxt>
<test2>
<ptxt>Remove the lower tail gate lock assembly RH (See page <xref label="Seep02" href="RM000001BLH07NX"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the lower tail gate lock assembly RH (See page <xref label="Seep04" href="RM000003GFE00AX"/>).</ptxt>
</test2>
</test1>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for LH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for RH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000003G3100DX_04_0005" fin="false">A</down>
<right ref="RM000003G3100DX_04_0009" fin="true">B</right>
<right ref="RM000003G3100DX_04_0010" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003G3100DX_04_0005" proc-id="RM22W0E___0000IL600001">
<testtitle>CHECK HARNESS AND CONNECTOR (LOWER TAIL GATE LOCK ASSEMBLY - NO. 2 MAIN BODY ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LH:</ptxt>
<test2>
<ptxt>Disconnect the h5 lower tail gate lock assembly LH connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the L51 No. 2 main body ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>h5-5 (ACT+) - L51-3 (DC+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>h5-6 (ACT-) - L51-8 (DC-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>h5-5 (ACT+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>h5-6 (ACT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RH:</ptxt>
<test2>
<ptxt>Disconnect the h4 lower tail gate lock assembly RH connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the L51 No. 2 main body ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>h4-5 (ACT+) - L51-2 (DC+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>h4-6 (ACT-) - L51-1 (DC-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>h4-5 (ACT+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>h4-6 (ACT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000003G3100DX_04_0008" fin="true">OK</down>
<right ref="RM000003G3100DX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003G3100DX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003G3100DX_04_0008">
<testtitle>REPLACE NO. 2 MAIN BODY ECU<xref label="Seep01" href="RM0000038MO00UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003G3100DX_04_0009">
<testtitle>REPLACE LOWER TAIL GATE LOCK ASSEMBLY LH<xref label="Seep01" href="RM000001BLH07NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003G3100DX_04_0010">
<testtitle>REPLACE LOWER TAIL GATE LOCK ASSEMBLY RH<xref label="Seep01" href="RM000001BLH07NX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>