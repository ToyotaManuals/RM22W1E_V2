<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000G" variety="S000G">
<name>1UR-FE FUEL</name>
<ttl id="12008_S000G_7C3GQ_T009T" variety="T009T">
<name>FUEL TANK</name>
<para id="RM0000039W801EX" category="A" type-id="30014" name-id="FU959-01" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM0000039W801EX_01" type-id="01" category="01">
<s-1 id="RM0000039W801EX_01_0001" proc-id="RM22W0E___00005XI00000">
<ptxt>INSTALL NO. 1 FUEL TANK HEAT INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the heat insulator with the 4 clips.</ptxt>
<figure>
<graphic graphicname="A175034E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the fuel tube clamp to the heat insulator.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0002" proc-id="RM22W0E___00005XJ00000">
<ptxt>INSTALL FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the hose to the fuel tank as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A176439E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Tank Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Hose Side Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Align the fuel tank side mark with the hose side mark when installing the hose.</ptxt>
</item>
<item>
<ptxt>Tighten the hose clamp until the end of the hose clamp contacts the stopper as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A176441E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
</list1>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Stopper</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0063" proc-id="RM22W0E___00005WW00000">
<ptxt>INSTALL FUEL SUCTION WITH PUMP AND GAUGE TUBE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of gasoline or grease to a new gasket, and install it to the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Install the fuel suction with pump and gauge tube into the fuel tank.</ptxt>
<figure>
<graphic graphicname="A176324E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Groove</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
<atten4>
<ptxt>Align the protrusion of the fuel suction with pump and gauge tube with the groove of the fuel tank.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Put the retainer on the fuel tank. While holding the fuel suction with pump and gauge tube, tighten the retainer 1 complete turn by hand.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A242908E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Set SST on the retainer.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Hold the fuel suction tube assembly upright by hand to make sure that the fuel suction tube gasket is not moved out of position.</ptxt>
</item>
<item>
<ptxt>Engage the claws of SST securely with the fuel pump gauge retainer holes to secure SST.</ptxt>
</item>
<item>
<ptxt>Install SST while pressing the claws of SST against the fuel pump gauge retainer (toward the center of SST).</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Using SST, tighten the retainer until the mark on the retainer is within range A on the fuel tank as shown in the illustration.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A242906E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Tank Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Retainer Side Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Fit the tips of SST onto the ribs of the retainer.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039W801EX_01_0004" proc-id="RM22W0E___00005XK00000">
<ptxt>INSTALL FUEL TANK MAIN TUBE SUB-ASSEMBLY AND FUEL TANK RETURN TUBE SUB-ASSEMBLY (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 fuel tank tubes with the 2 tube joint clips.</ptxt>
<figure>
<graphic graphicname="A118171E12" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check that there are no scratches or foreign objects on the connecting parts.</ptxt>
</item>
<item>
<ptxt>Check that the fuel tube joints are inserted securely.</ptxt>
</item>
<item>
<ptxt>Check that the tube joint clips are on the collars of the fuel tube joints.</ptxt>
</item>
<item>
<ptxt>After installing the tube joint clips, check that the fuel tube joints cannot be pulled off.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Tube Joint</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Fuel Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Tube Joint Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the 2 fuel tank tubes to the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0015" proc-id="RM22W0E___00005XS00000">
<ptxt>INSTALL FUEL TANK MAIN TUBE, FUEL TANK RETURN TUBE AND NO. 2 FUEL TANK MAIN TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 3 fuel tank tubes with the 3 tube joint clips.</ptxt>
<figure>
<graphic graphicname="A118171E12" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check that there are no scratches or foreign objects on the connecting parts.</ptxt>
</item>
<item>
<ptxt>Check that the fuel tube joints are inserted securely.</ptxt>
</item>
<item>
<ptxt>Check that the tube joint clips are on the collars of the fuel tube joints.</ptxt>
</item>
<item>
<ptxt>After installing the tube joint clips, check that the fuel tube joints cannot be pulled off.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Tube Joint</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Fuel Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Tube Joint Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the 3 fuel tank tubes to the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0005" proc-id="RM22W0E___00005XL00000">
<ptxt>INSTALL FUEL TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the fuel tank on an engine lifter and raise the fuel tank.</ptxt>
<atten3>
<ptxt>Do not allow the fuel tank to contact the vehicle, especially the differential.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Raise the engine lifter.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 fuel tank bands with the 2 pins and 2 clips.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 fuel tank bands with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0006" proc-id="RM22W0E___00005XM00000">
<ptxt>CONNECT FUEL TANK TO FILLER PIPE HOSE (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the hose to the filler pipe as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A181359E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Hose Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pipe Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>0 to 0.3 mm</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Install the hose so that the distance between the fuel tank inlet pipe side mark and fuel tank to filler pipe hose side mark is 0 to 0.3 mm (0 to 0.0118 in.) as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>Tighten the hose clamp until the end of the hose clamp contacts the stopper as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A176441E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
</list1>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Stopper</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0016" proc-id="RM22W0E___00005XT00000">
<ptxt>CONNECT FUEL TANK TO FILLER PIPE HOSE (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the hose to the filler pipe as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A181358E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Hose Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pipe Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>0 to 0.3 mm</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Install the hose so that the distance between the fuel tank inlet pipe side mark and fuel tank to filler pipe hose side mark is 0 to 0.3 mm (0 to 0.0118 in.) as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>Tighten the hose clamp until the end of the hose clamp contacts the stopper as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A176441E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
</list1>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Stopper</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0007" proc-id="RM22W0E___00005XN00000">
<ptxt>CONNECT FUEL TANK BREATHER TUBE (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank breather tube to the filler pipe and push the retainer.</ptxt>
<figure>
<graphic graphicname="A176313E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the fuel tube clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0017" proc-id="RM22W0E___00005XU00000">
<ptxt>CONNECT FUEL TANK BREATHER TUBE (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank breather tube to the filler pipe and push the retainer.</ptxt>
<figure>
<graphic graphicname="A181492E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the fuel tube clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0019" proc-id="RM22W0E___00005XW00000">
<ptxt>CONNECT NO. 2 FUEL MAIN TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel main tube and push the retainer.</ptxt>
<figure>
<graphic graphicname="A175062E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the fuel tube clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0018" proc-id="RM22W0E___00005XV00000">
<ptxt>CONNECT NO. 2 FUEL TANK BREATHER TUBE (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank breather tube and push the retainer.</ptxt>
<figure>
<graphic graphicname="A175064E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the fuel tube clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0008" proc-id="RM22W0E___00005XO00000">
<ptxt>CONNECT FUEL TANK RETURN TUBE (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank return tube and push the retainer.</ptxt>
<figure>
<graphic graphicname="A176311E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the fuel tube clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0020" proc-id="RM22W0E___00005XX00000">
<ptxt>CONNECT FUEL TANK RETURN TUBE (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank return tube and push the retainer.</ptxt>
<figure>
<graphic graphicname="A176339E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the fuel tube clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0009" proc-id="RM22W0E___00005XP00000">
<ptxt>CONNECT NO. 2 FUEL TANK BREATHER TUBE (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank breather tube and push the retainer.</ptxt>
<figure>
<graphic graphicname="A176309E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the fuel tube clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0010" proc-id="RM22W0E___00005XQ00000">
<ptxt>CONNECT FUEL TANK MAIN TUBE SUB-ASSEMBLY (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank main tube and push the retainer.</ptxt>
<figure>
<graphic graphicname="A176307E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the fuel tube clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0021" proc-id="RM22W0E___00005XY00000">
<ptxt>CONNECT FUEL TANK MAIN TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank main tube and push the retainer.</ptxt>
<figure>
<graphic graphicname="A176337E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the fuel tube clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0011" proc-id="RM22W0E___00005XR00000">
<ptxt>INSTALL NO. 1 FUEL TANK PROTECTOR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel tank protector with the 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0012">
<ptxt>INSTALL FUEL TANK CAP ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM0000039W801EX_01_0022" proc-id="RM22W0E___00005XZ00000">
<ptxt>INSTALL REAR FLOOR NO. 2 SERVICE HOLE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel pump and fuel sender gauge connector.</ptxt>
</s2>
<s2>
<ptxt>Install the service hole cover with new butyl tape.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<ptxt>Install the air duct and 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0059">
<ptxt>INSTALL FRONT FLOOR CARPET ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM0000039W801EX_01_0066" proc-id="RM22W0E___00005Y100000">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ Sliding Roof (See page <xref label="Seep01" href="RM0000038MM00QX_02_0020"/>)</ptxt>
</item>
<item>
<ptxt>w/o Sliding Roof (See page <xref label="Seep02" href="RM0000038MM00PX_02_0020"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0067" proc-id="RM22W0E___00005Y200000">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ Sliding Roof (See page <xref label="Seep01" href="RM0000038MM00QX_02_0022"/>)</ptxt>
</item>
<item>
<ptxt>w/o Sliding Roof (See page <xref label="Seep02" href="RM0000038MM00PX_02_0022"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0068" proc-id="RM22W0E___00005Y300000">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>for Face to Face Seat Type (See page <xref label="Seep01" href="RM00000311A003X_01_0001"/>)</ptxt>
</item>
<item>
<ptxt>except Face to Face Seat Type (See page <xref label="Seep02" href="RM00000391Q00UX_01_0010"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0069" proc-id="RM22W0E___00005Y400000">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY RH (for 60/40 Split Seat Type 40 Side)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000391200OX_01_0004"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0070" proc-id="RM22W0E___00005Y500000">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY LH (for 60/40 Split Seat Type 60 Side)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000390X00OX_01_0004"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0062" proc-id="RM22W0E___00005Y000000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039W801EX_01_0058">
<ptxt>ADD FUEL</ptxt>
</s-1>
<s-1 id="RM0000039W801EX_01_0042" proc-id="RM22W0E___00004H300000">
<ptxt>INSPECT FOR FUEL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Make sure that there are no fuel leaks after performing maintenance on the fuel system.</ptxt>
<s3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Fuel Pump / Speed.</ptxt>
</s3>
<s3>
<ptxt>Check that there are no leaks from the fuel system.</ptxt>
<ptxt>If there are fuel leaks, repair or replace parts as necessary.</ptxt>
</s3>
<s3>
<ptxt>Turn the engine switch off.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the GTS from the DLC3.</ptxt>
</s3>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>