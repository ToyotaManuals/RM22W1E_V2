<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12021_S001D" variety="S001D">
<name>JF2A TRANSFER / 4WD / AWD</name>
<ttl id="12021_S001D_7C3NN_T00GQ" variety="T00GQ">
<name>TRANSFER ASSEMBLY</name>
<para id="RM000002HO7013X" category="G" type-id="3001K" name-id="TF18P-01" from="201301">
<name>INSPECTION</name>
<subpara id="RM000002HO7013X_02" type-id="01" category="01">
<s-1 id="RM000002HO7013X_02_0012" proc-id="RM22W0E___000097L00000">
<ptxt>INSPECT TRANSFER INPUT SHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a micrometer, measure the diameter of the input shaft journal surface.</ptxt>
<figure>
<graphic graphicname="C173633" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Minimum Diameter</title>
<specitem>
<ptxt>68 mm (2.68 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is less than the minimum, replace the input shaft.</ptxt>
</s2>
<s2>
<ptxt>Using a dial indicator, measure the inside diameter of the input shaft bush.</ptxt>
<figure>
<graphic graphicname="C173634" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Maximum Inside Diameter</title>
<specitem>
<ptxt>46 mm (1.81 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the inside diameter is more than the maximum, replace the input shaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO7013X_02_0013" proc-id="RM22W0E___000097M00000">
<ptxt>INSPECT TRANSFER LOW PLANETARY GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a feeler gauge, measure the thrust clearance of the pinion gear.</ptxt>
<figure>
<graphic graphicname="C173635" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Clearance</title>
<specitem>
<ptxt>0.11 to 0.85 mm (0.00434 to 0.0334 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum Clearance</title>
<specitem>
<ptxt>0.85 mm (0.0334 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is more than the maximum, replace the low planetary gear assembly.</ptxt>
</s2>
<s2>
<ptxt>Using a dial indicator, measure the radial clearance of the pinion gear.</ptxt>
<figure>
<graphic graphicname="C173636" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Clearance</title>
<specitem>
<ptxt>0.01 to 0.04 mm (0.000434 to 0.00150 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum Clearance</title>
<specitem>
<ptxt>0.04 mm (0.00150 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is more than the maximum, replace the low planetary gear assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO7013X_02_0015" proc-id="RM22W0E___000097N00000">
<ptxt>INSPECT REAR TRANSFER OUTPUT SHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a micrometer, measure the diameter of the output shaft journals.</ptxt>
<figure>
<graphic graphicname="C173637E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Minimum Diameter</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Journal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle">
<ptxt>38 mm (1.50 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>42 mm (1.65 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
<entry valign="middle">
<ptxt>49 mm (1.93 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the diameter is less than the minimum, replace the output shaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO7013X_02_0017" proc-id="RM22W0E___000097O00000">
<ptxt>INSPECT NO. 1 TRANSFER GEAR SHIFT FORK AND FRONT DRIVE CLUTCH SLEEVE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a feeler gauge, measure the clearance between the clutch sleeve and gear shift fork.</ptxt>
<figure>
<graphic graphicname="C173639" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum Clearance</title>
<specitem>
<ptxt>0.8 mm (0.0315 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is more than the maximum, replace the clutch sleeve or shift fork.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO7013X_02_0018" proc-id="RM22W0E___000097P00000">
<ptxt>INSPECT NO. 2 TRANSFER GEAR SHIFT FORK AND HIGH AND LOW CLUTCH SLEEVE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a feeler gauge, measure the clearance between the clutch sleeve and gear shift fork.</ptxt>
<figure>
<graphic graphicname="C173638" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum Clearance</title>
<specitem>
<ptxt>3.15 mm (0.124 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is more than the maximum, replace the clutch sleeve or shift fork.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO7013X_02_0020" proc-id="RM22W0E___000097Q00000">
<ptxt>INSPECT NO. 1 SYNCHRONIZER RING (for Manual Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply gear oil to the cone of the input shaft, and check that it does not turn in both directions while pushing the No. 1 synchronizer ring.</ptxt>
<figure>
<graphic graphicname="C174149" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If it can turn, replace the No. 1 synchronizer ring.</ptxt>
</s2>
<s2>
<ptxt>Push the No. 1 synchronizer ring to the cone of the input shaft. Measure the clearance between the No. 1 synchronizer ring and input shaft.</ptxt>
<figure>
<graphic graphicname="C174148" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Clearance</title>
<specitem>
<ptxt>0.86 to 1.54 mm (0.0339 to 0.0606 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum Clearance</title>
<specitem>
<ptxt>0.86 mm (0.0339 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is not as specified, replace the No. 1 synchronizer ring with a new one.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>