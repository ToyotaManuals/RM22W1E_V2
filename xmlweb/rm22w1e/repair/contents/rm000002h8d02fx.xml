<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000H" variety="S000H">
<name>3UR-FE FUEL</name>
<ttl id="12008_S000H_7C3GY_T00A1" variety="T00A1">
<name>FUEL PRESSURE REGULATOR</name>
<para id="RM000002H8D02FX" category="A" type-id="80001" name-id="FU930-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM000002H8D02FX_01" type-id="01" category="01">
<s-1 id="RM000002H8D02FX_01_0002" proc-id="RM22W0E___00004SR00001">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE
</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000028RU04QX"/>)</ptxt>
</content1></s-1>
<s-1 id="RM000002H8D02FX_01_0007" proc-id="RM22W0E___000061W00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002H8D02FX_01_0006" proc-id="RM22W0E___000061V00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002H8D02FX_01_0004" proc-id="RM22W0E___00001X400001">
<ptxt>REMOVE V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A174196E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Raise the front of the V-bank cover to detach the 3 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002H8D02FX_01_0005" proc-id="RM22W0E___00001X500001">
<ptxt>REMOVE AIR CLEANER HOSE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A243375" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the vacuum hose and No. 2 ventilation hose.</ptxt>
</s2>
<s2>
<ptxt>Loosen the 2 hose clamps.</ptxt>
</s2>
<s2>
<ptxt>Remove the air cleaner hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002H8D02FX_01_0001" proc-id="RM22W0E___000061U00001">
<ptxt>REMOVE FUEL PRESSURE REGULATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 fuel tube from the pressure regulator (See page <xref label="Seep01" href="RM0000028RU04QX"/>).</ptxt>
<figure>
<graphic graphicname="A161399" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Put a cloth or equivalent under the pressure regulator.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Disconnect the vacuum sensing hose from the pressure regulator.</ptxt>
<figure>
<graphic graphicname="A161400" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts, and pull out the pressure regulator.</ptxt>
<figure>
<graphic graphicname="A161401" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the pressure regulator.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>