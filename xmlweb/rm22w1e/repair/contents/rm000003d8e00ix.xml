<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000003D8E00IX" category="J" type-id="801IE" name-id="ACJ8A-01" from="201308">
<dtccode/>
<dtcname>Heater Control Switch Circuit</dtcname>
<subpara id="RM000003D8E00IX_01" type-id="60" category="03" proc-id="RM22W0E___0000H6400001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The temperature control switch assembly LH and RH sends the temperature up and down signal to the air conditioning amplifier assembly through the multi-display assembly*1 or the air conditioning control assembly*2.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Navigation System</ptxt>
</item>
<item>
<ptxt>*2: w/o Navigation System</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000003D8E00IX_02" type-id="32" category="03" proc-id="RM22W0E___0000H6500001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E251613E03" width="7.106578999in" height="5.787629434in"/>
</figure>
<figure>
<graphic graphicname="B144127E05" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003D8E00IX_04" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003D8E00IX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003D8E00IX_03_0001" proc-id="RM22W0E___0000H6600001">
<testtitle>CHECK VEHICLE TYPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the vehicle is equipped with the navigation system.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>w/ Navigation System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>w/o Navigation System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0002" fin="false">A</down>
<right ref="RM000003D8E00IX_03_0008" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0002" proc-id="RM22W0E___0000H6700001">
<testtitle>CHECK CAN COMMUNICATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the CAN communication system.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>CAN communication system has no problem.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0003" fin="false">A</down>
<right ref="RM000003D8E00IX_03_0014" fin="true">B</right>
<right ref="RM000003D8E00IX_03_0028" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0003" proc-id="RM22W0E___0000H6800001">
<testtitle>CHECK OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the temperature control switches operate.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Temperature control switch LH does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Temperature control switch RH does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Temperature control switch LH and RH do not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0004" fin="false">A</down>
<right ref="RM000003D8E00IX_03_0006" fin="false">B</right>
<right ref="RM000003D8E00IX_03_0029" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0004" proc-id="RM22W0E___0000H6900001">
<testtitle>INSPECT TEMPERATURE CONTROL SWITCH ASSEMBLY LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the temperature control switch assembly LH (See page <xref label="Seep01" href="RM000003B4601MX"/>).</ptxt>
<figure>
<graphic graphicname="E191987E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 (UP) - 4 (SE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>UP switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>UP switch not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>5 (DOWN) - 4 (SE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DOWN switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DOWN switch not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Temperature Control Switch Assembly LH)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0005" fin="false">OK</down>
<right ref="RM000003D8E00IX_03_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0005" proc-id="RM22W0E___0000H6A00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TEMPERATURE CONTROL SWITCH ASSEMBLY LH - MULTI-DISPLAY ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F16 temperature control switch assembly LH connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the F90 multi-display assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F16-3 (UP) - F90-2 (TEC+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F16-5 (DOWN) - F90-7 (TEC-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F16-4 (SE) - F90-6 (AGND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F16-3 (UP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F16-5 (DOWN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F16-4 (SE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0017" fin="true">OK</down>
<right ref="RM000003D8E00IX_03_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0006" proc-id="RM22W0E___0000H6B00001">
<testtitle>INSPECT TEMPERATURE CONTROL SWITCH ASSEMBLY RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the temperature control switch assembly RH (See page <xref label="Seep01" href="RM000003B4601MX"/>).</ptxt>
<figure>
<graphic graphicname="E191987E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>5 (UP) - 4 (SE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>UP switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>UP switch not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 (DOWN) - 4 (SE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DOWN switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DOWN switch not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Temperature Control Switch Assembly RH)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0007" fin="false">OK</down>
<right ref="RM000003D8E00IX_03_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0007" proc-id="RM22W0E___0000H6C00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TEMPERATURE CONTROL SWITCH ASSEMBLY RH - MULTI-DISPLAY ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F17 temperature control switch assembly RH connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the F90 multi-display assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F17-5 (UP) - F90-3 (TES+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F17-3 (DOWN) - F90-8 (TES-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F17-4 (SE) - F90-9 (BGND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F17-5 (UP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F17-3 (DOWN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F17-4 (SE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0020" fin="true">OK</down>
<right ref="RM000003D8E00IX_03_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0029" proc-id="RM22W0E___0000H6J00001">
<testtitle>CHECK MULTI-DISPLAY ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the multi-display assembly with a new or normally functioning one (See page <xref label="Seep01" href="RM000003B6H022X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check that the temperature control with temperature control switch LH and RH.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Temperature control with temperature control switch LH and RH.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0030" fin="true">OK</down>
<right ref="RM000003D8E00IX_03_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0008" proc-id="RM22W0E___0000H6D00001">
<testtitle>CHECK LIN COMMUNICATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the LIN communication system.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>LIN communication system has no problem.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0009" fin="false">OK</down>
<right ref="RM000003D8E00IX_03_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0009" proc-id="RM22W0E___0000H6E00001">
<testtitle>CHECK OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the temperature control switches operate.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Temperature control switch LH does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Temperature control switch RH does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Temperature control switch LH and RH do not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0010" fin="false">A</down>
<right ref="RM000003D8E00IX_03_0012" fin="false">B</right>
<right ref="RM000003D8E00IX_03_0032" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0010" proc-id="RM22W0E___0000H6F00001">
<testtitle>INSPECT TEMPERATURE CONTROL SWITCH ASSEMBLY LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the temperature control switch assembly LH (See page <xref label="Seep01" href="RM000003B6K002X"/>).</ptxt>
<figure>
<graphic graphicname="E191987E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 (UP) - 4 (SE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>UP switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>UP switch not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>5 (DOWN) - 4 (SE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DOWN switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DOWN switch not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Temperature Control Switch Assembly LH)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0011" fin="false">OK</down>
<right ref="RM000003D8E00IX_03_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0011" proc-id="RM22W0E___0000H6G00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TEMPERATURE CONTROL SWITCH ASSEMBLY LH - AIR CONDITIONING CONTROL ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F16 temperature control switch assembly LH connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the F10 air conditioning control assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F16-3 (UP) - F10-12 (DTP+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F16-5 (DOWN) - F10-11 (DTP-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F16-4 (SE) - F10-9 (S5)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F16-3 (UP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F16-5 (DOWN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F16-4 (SE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0024" fin="true">OK</down>
<right ref="RM000003D8E00IX_03_0023" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0012" proc-id="RM22W0E___0000H6H00001">
<testtitle>INSPECT TEMPERATURE CONTROL SWITCH ASSEMBLY RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the temperature control switch assembly RH (See page <xref label="Seep01" href="RM000003B6K002X"/>).</ptxt>
<figure>
<graphic graphicname="E191987E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>

<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>5 (UP) - 4 (SE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>UP switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>UP switch not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 (DOWN) - 4 (SE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DOWN switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DOWN switch not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Temperature Control Switch Assembly RH)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0013" fin="false">OK</down>
<right ref="RM000003D8E00IX_03_0025" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0013" proc-id="RM22W0E___0000H6I00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TEMPERATURE CONTROL SWITCH ASSEMBLY RH - AIR CONDITIONING CONTROL ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F17 temperature control switch assembly RH connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the F10 air conditioning control assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F17-5 (UP) - F10-10 (PTP+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F17-3 (DOWN) - F10-3 (PTP-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F17-4 (SE) - F10-2 (SG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F17-5 (UP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F17-3 (DOWN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F17-4 (SE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0027" fin="true">OK</down>
<right ref="RM000003D8E00IX_03_0026" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0032" proc-id="RM22W0E___0000H6K00001">
<testtitle>CHECK AIR CONDITIONING CONTROL ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air conditioning control assembly with a new or normally functioning one (See page <xref label="Seep01" href="RM000003B4601NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check that the temperature control with temperature control switch LH and RH.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Temperature control with temperature control switch LH and RH.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003D8E00IX_03_0033" fin="true">OK</down>
<right ref="RM000003D8E00IX_03_0034" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0014">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0015">
<testtitle>REPLACE TEMPERATURE CONTROL SWITCH ASSEMBLY LH<xref label="Seep01" href="RM000003B4601MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0017">
<testtitle>REPLACE MULTI-DISPLAY ASSEMBLY<xref label="Seep01" href="RM000003B4601MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0018">
<testtitle>REPLACE TEMPERATURE CONTROL SWITCH ASSEMBLY RH<xref label="Seep01" href="RM000003B4601MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0019">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0020">
<testtitle>REPLACE MULTI-DISPLAY ASSEMBLY<xref label="Seep01" href="RM000003B4601MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0021">
<testtitle>GO TO LIN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0022">
<testtitle>REPLACE TEMPERATURE CONTROL SWITCH ASSEMBLY LH<xref label="Seep01" href="RM000003B4601MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0023">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0024">
<testtitle>REPLACE AIR CONDITIONING CONTROL ASSEMBLY<xref label="Seep01" href="RM000003B4601NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0025">
<testtitle>REPLACE TEMPERATURE CONTROL SWITCH ASSEMBLY RH<xref label="Seep01" href="RM000003B4601MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0026">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0027">
<testtitle>REPLACE AIR CONDITIONING CONTROL ASSEMBLY<xref label="Seep01" href="RM000003B4601NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0028">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0030">
<testtitle>END (MULTI-DISPLAY ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0031">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002LIQ02HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0033">
<testtitle>END (AIR CONDITIONING CONTROL ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000003D8E00IX_03_0034">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501QX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>