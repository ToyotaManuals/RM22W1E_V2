<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S002F" variety="S002F">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S002F_7C3WU_T00PX" variety="T00PX">
<name>INSTRUMENT PANEL SAFETY PAD</name>
<para id="RM0000039OS00NX" category="A" type-id="8000E" name-id="IT3KM-02" from="201308">
<name>REASSEMBLY</name>
<subpara id="RM0000039OS00NX_02" type-id="11" category="10" proc-id="RM22W0E___0000HHK00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0DSX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039OS00NX_01" type-id="01" category="01">
<s-1 id="RM0000039OS00NX_01_0001" proc-id="RM22W0E___0000HHD00001">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL WIRE (for LHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clamps to install the No. 2 instrument panel wire to the instrument panel.</ptxt>
<figure>
<graphic graphicname="B180536" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OS00NX_01_0014" proc-id="RM22W0E___0000HHJ00001">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL WIRE (for RHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clamps to install the No. 2 instrument panel wire to the instrument panel.</ptxt>
<figure>
<graphic graphicname="B190100" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OS00NX_01_0011" proc-id="RM22W0E___0000BRZ00001">
<ptxt>INSTALL ANTENNA CORD SUB-ASSEMBLY (w/o Multi-display)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Attach the 7 clamps to install the antenna cord.</ptxt>
<figure>
<graphic graphicname="B181730" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<ptxt>Attach the 5 clamps to install the antenna cord.</ptxt>
<figure>
<graphic graphicname="B181732" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039OS00NX_01_0012" proc-id="RM22W0E___0000BRW00001">
<ptxt>INSTALL ANTENNA CORD SUB-ASSEMBLY (w/ Multi-display)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Attach the 8 clamps to install the antenna cord.</ptxt>
<figure>
<graphic graphicname="B181729" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<ptxt>Attach the 6 clamps to install the antenna cord.</ptxt>
<figure>
<graphic graphicname="B181731" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039OS00NX_01_0013" proc-id="RM22W0E___0000CH400001">
<ptxt>INSTALL NAVIGATION ANTENNA ASSEMBLY (w/ Multi-display)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<s3>
<ptxt>Install the navigation antenna assembly with the 2 screws.</ptxt>
</s3>
<s3>
<ptxt>Attach the 2 clamps.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<s3>
<ptxt>Install the navigation antenna assembly with the 2 screws.</ptxt>
</s3>
<s3>
<ptxt>Attach the 2 clamps.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000039OS00NX_01_0004" proc-id="RM22W0E___0000HHE00001">
<ptxt>INSTALL NO. 2 SIDE DEFROSTER NOZZLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws to install the No. 2 side defroster nozzle.</ptxt>
<figure>
<graphic graphicname="B180543" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OS00NX_01_0005" proc-id="RM22W0E___0000HHF00001">
<ptxt>INSTALL NO. 1 SIDE DEFROSTER NOZZLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the No. 1 side defroster nozzle.</ptxt>
<figure>
<graphic graphicname="B180541" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OS00NX_01_0006" proc-id="RM22W0E___0000HHG00001">
<ptxt>INSTALL DEFROSTER NOZZLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 11 claws to install the defroster nozzle.</ptxt>
<figure>
<graphic graphicname="B180544" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OS00NX_01_0010" proc-id="RM22W0E___0000FI700001">
<ptxt>INSTALL FRONT PASSENGER AIRBAG ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 5 hooks (A).</ptxt>
<figure>
<graphic graphicname="B189612E01" width="2.775699831in" height="4.7836529in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 5 hooks (B) to install the front passenger airbag on the instrument panel.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
<figure>
<graphic graphicname="B189610E01" width="2.775699831in" height="4.7836529in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the 2 connectors and install the instrument panel wire assembly to the front passenger airbag.</ptxt>
<figure>
<graphic graphicname="B183441" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Attach the claw and install the wire.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039OS00NX_01_0008" proc-id="RM22W0E___0000HHH00001">
<ptxt>INSTALL REAR NO. 1 AIR DUCT (w/ Rear Air Duct)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear No. 1 air duct with the 2 screws.</ptxt>
<figure>
<graphic graphicname="B180539" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OS00NX_01_0009" proc-id="RM22W0E___0000HHI00001">
<ptxt>INSTALL CENTER HEATER TO REGISTER DUCT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the center heater to register duct with the 3 screws.</ptxt>
<figure>
<graphic graphicname="B180538" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>