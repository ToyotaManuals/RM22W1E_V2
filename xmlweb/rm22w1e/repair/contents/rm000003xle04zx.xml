<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001X" variety="S001X">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001X_7C3RY_T00L1" variety="T00L1">
<name>PARKING ASSIST MONITOR SYSTEM (w/ Side Monitor System)</name>
<para id="RM000003XLE04ZX" category="J" type-id="805LC" name-id="PM73T-01" from="201308">
<dtccode/>
<dtcname>"Back Door is Open" is Displayed even after Back Door is Closed</dtcname>
<subpara id="RM000003XLE04ZX_01" type-id="60" category="03" proc-id="RM22W0E___0000D1E00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The parking assist ECU receives back door lock assembly open/close signals from the main body ECU (cowl side junction block LH) via CAN communication. When the back door is open, the camera aiming cannot be adjusted correctly because the rear television camera assembly is installed on the back door. Therefore, when adjusting the camera aiming calibration while the back door is open, a back door open warning message will be displayed on the screen and camera aiming adjustment will be canceled.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The back door lock assembly is connected to the main body ECU (cowl side junction block LH) by the vehicle wire harness. </ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000003XLE04ZX_02" type-id="32" category="03" proc-id="RM22W0E___0000D1F00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E246953E05" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003XLE04ZX_03" type-id="51" category="05" proc-id="RM22W0E___0000D1G00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When "System initializing" is displayed on the parking assist ECU after the battery terminal disconnected, correct the steering angle neutral point (See page <xref label="Seep01" href="RM0000035DE03CX"/>).</ptxt>
</item>
<item>
<ptxt>Depending on the parts that are replaced or operations that are performed during vehicle inspection or maintenance, calibration of other systems as well as the parking assist monitor system (w/ Side Monitor System) may be needed (See page <xref label="Seep02" href="RM0000035DD03SX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000003XLE04ZX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003XLE04ZX_05_0001" proc-id="RM22W0E___0000D1H00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (BACK DOOR COURTESY SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the back door courtesy switch is functioning properly (See page <xref label="Seep01" href="RM000003WW0039X"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Courtesy SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Back door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Back door open</ptxt>
<ptxt>OFF: Back door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The back door courtesy switch functions as specified in the normal condition column.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XLE04ZX_05_0006" fin="true">OK</down>
<right ref="RM000003XLE04ZX_05_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLE04ZX_05_0003" proc-id="RM22W0E___0000D1I00001">
<testtitle>CHECK HARNESS AND CONNECTOR (BACK DOOR LOCK ASSEMBLY - MAIN BODY ECU [COWL SIDE JUNCTION BLOCK LH] AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the S1*1 or S22*2 back door lock assembly connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/o Power Back Door System</ptxt>
</item>
<item>
<ptxt>*2: w/ Power Back Door System</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Disconnect the E2 main body ECU (cowl side junction block LH) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>w/o Power Back Door System</subtitle>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>S1-3 (+) - E2-25 (BCTY)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S1-3 (+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S1-1 (ACT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>w/ Power Back Door System</subtitle>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>S22-3 (CTY) - E2-25 (BCTY)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S22-3 (CTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S22-4 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XLE04ZX_05_0004" fin="false">OK</down>
<right ref="RM000003XLE04ZX_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLE04ZX_05_0004" proc-id="RM22W0E___0000D1J00001">
<testtitle>INSPECT BACK DOOR LOCK ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the back door lock assembly (See page <xref label="Seep01" href="RM0000039H800ZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the back door lock assembly (See page <xref label="Seep02" href="RM000001VYW00WX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003XLE04ZX_05_0005" fin="true">OK</down>
<right ref="RM000003XLE04ZX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLE04ZX_05_0006">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM0000039LH004X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLE04ZX_05_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003XLE04ZX_05_0005">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000003XLE04ZX_05_0009">
<testtitle>REPLACE BACK DOOR LOCK ASSEMBLY<xref label="Seep01" href="RM0000039H800ZX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>