<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12018_S001B" variety="S001B">
<name>CLUTCH</name>
<ttl id="12018_S001B_7C3N2_T00G5" variety="T00G5">
<name>CLUTCH ACCUMULATOR</name>
<para id="RM0000032RO003X" category="A" type-id="30014" name-id="CL2C0-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000032RO003X_01" type-id="01" category="01">
<s-1 id="RM0000032RO003X_01_0001" proc-id="RM22W0E___00008WU00000">
<ptxt>INSTALL CLUTCH ACCUMULATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C271135" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the clutch accumulator with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>120</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a union nut wrench, connect the flexible hose tube.</ptxt>
<figure>
<graphic graphicname="C271136" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032RO003X_01_0011" proc-id="RM22W0E___00008WK00000">
<ptxt>INSTALL CLUTCH RELEASE CYLINDER TO ACCUMULATOR TUBE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C271132" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a union nut wrench, connect the tube to the clutch accumulator and release cylinder.</ptxt>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>158</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the tube with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>120</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000032RO003X_01_0012" proc-id="RM22W0E___00008WL00000">
<ptxt>INSTALL NO. 1 CLUTCH HOUSING COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172725" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the housing cover with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>120</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000032RO003X_01_0008" proc-id="RM22W0E___000034P00000">
<ptxt>FILL RESERVOIR WITH BRAKE FLUID
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fill the reservoir with brake fluid.</ptxt>
<spec>
<title>Brake Fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT 3</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM0000032RO003X_01_0005" proc-id="RM22W0E___000034Q00000">
<ptxt>BLEED CLUTCH LINE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172957" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the release cylinder bleeder plug cap.</ptxt>
</s2>
<s2>
<ptxt>Connect a vinyl tube to the bleeder plug.</ptxt>
</s2>
<s2>
<ptxt>Depress the clutch pedal several times, and then loosen the bleeder plug while the pedal is depressed.</ptxt>
</s2>
<s2>
<ptxt>When fluid no longer comes out, tighten the bleeder plug, and then release the clutch pedal.</ptxt>
</s2>
<s2>
<ptxt>Repeat the previous 2 steps until all the air in the fluid is completely bled.</ptxt>
</s2>
<s2>
<ptxt>Tighten the bleeder plug. </ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the bleeder plug cap.</ptxt>
</s2>
<s2>
<ptxt>Check that all the air has been bled from the clutch line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032RO003X_01_0006" proc-id="RM22W0E___000034R00000">
<ptxt>CHECK FLUID LEVEL IN RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the fluid level.</ptxt>
<ptxt>If the brake fluid level is low, check for leaks and inspect the disc brake pad. If necessary, refill the reservoir with brake fluid after repair or replacement.</ptxt>
<spec>
<title>Brake fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT 3</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM0000032RO003X_01_0007">
<ptxt>INSPECT FOR CLUTCH FLUID LEAK</ptxt>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>