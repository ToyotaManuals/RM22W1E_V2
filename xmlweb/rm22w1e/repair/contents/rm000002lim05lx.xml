<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002LIM05LX" category="D" type-id="3001B" name-id="AC02TZ-106" from="201301" to="201308">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM000002LIM05LX_z0" proc-id="RM22W0E___0000H0G00000">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use these procedures to troubleshoot the air conditioning system.</ptxt>
</item>
<item>
<ptxt>*: Use the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<spec>
<title>Standard Voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding.</ptxt>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK COMMUNICATION FUNCTION OF CAN COMMUNICATION SYSTEM*</testtitle>
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally .</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: See page <xref label="Seep12" href="RM000001RSW03JX"/>.</ptxt>
</item>
<item>
<ptxt>for RHD: See page <xref label="Seep13" href="RM000001RSW03KX"/>.</ptxt>
</item>
</list1>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>CAN DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO CAN COMMUNICATION SYSTEM (See page <xref label="Seep07" href="RM000001RSO08CX"/>)</action-ci-right>
<result>C</result>
<action-ci-right>GO TO CAN COMMUNICATION SYSTEM (See page <xref label="Seep10" href="RM000001RSO08DX"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK COMMUNICATION FUNCTION OF LIN COMMUNICATION SYSTEM*</testtitle>
<test1>
<ptxt>Use the intelligent tester to check if the LIN communication system is functioning normally (See page <xref label="Seep14" href="RM000001RSO08EX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>LIN DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>LIN DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO LIN COMMUNICATION SYSTEM (See page <xref label="Seep01" href="RM000001RSO08EX"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK FOR DTC*</testtitle>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep08" href="RM000002LIT037X"/>).</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep09" href="RM000002LIT037X"/>).</ptxt>
</test1>
<test1>
<ptxt>Based on the DTCs output above, try to force output of the air conditioning system DTC by simulating the conditions indicated by the DTC. Then recheck for DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO DTC CHART (See page <xref label="Seep11" href="RM000002LIN04AX"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to the Problem Symptoms Table (See page <xref label="Seep06" href="RM000002LIQ022X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Fault is not listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fault is listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO STEP 8</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>OVERALL ANALYSIS AND TROUBLESHOOTING*</testtitle>
<test1>
<ptxt>Terminals of ECU (See page <xref label="Seep05" href="RM000002LIP04HX"/>)</ptxt>
</test1>
<test1>
<ptxt>Data List/Active Test (See page <xref label="Seep04" href="RM000002LIV02OX"/>)</ptxt>
</test1>
<test1>
<ptxt>Actuator Check (See page <xref label="Seep03" href="RM000002OI3010X"/>)</ptxt>
</test1>
<test1>
<ptxt>On-vehicle Inspection</ptxt>
</test1>
<test1>
<ptxt>Inspection</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>ADJUST, REPAIR OR REPLACE</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>