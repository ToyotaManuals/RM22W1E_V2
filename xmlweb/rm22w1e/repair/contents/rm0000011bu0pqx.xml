<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM0000011BU0PQX" category="D" type-id="303F4" name-id="NS8LA-14" from="201308">
<name>DTC CHECK / CLEAR</name>
<subpara id="RM0000011BU0PQX_z0" proc-id="RM22W0E___0000CIV00001">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK DTC (CHECK USING INTELLIGENT TESTER)</ptxt>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / Navigation System / DTCs.</ptxt>
</step2>
<step2>
<ptxt>Check for DTCs, and then write them down.</ptxt>
</step2>
<step2>
<ptxt>Check the details of the DTC(s) (See page <xref label="Seep01" href="RM0000011BO0JSX"/>).</ptxt>
<atten3>
<ptxt>The navigation system outputs DTCs for the following system. When DTCs other than those in Diagnostic Trouble Code Chart for the navigation system are output, refer to Diagnostic Trouble Code Chart for the relevant system.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.01in"/>
<colspec colname="COL2" colwidth="1.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Audio and Visual System (w/ Navigation System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep02" href="RM0000017YP0BPX"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear Seat Entertainment System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep16" href="RM000003WTP02WX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten3>
</step2>
</step1>
<step1>
<ptxt>CLEAR DTC (CHECK USING INTELLIGENT TESTER)</ptxt>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / Navigation System / DTCs.</ptxt>
</step2>
<step2>
<ptxt>Clear the DTCs.</ptxt>
</step2>
</step1>
<step1>
<ptxt>START DIAGNOSTIC MODE</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Illustrations may differ from the actual vehicle screen depending on the device settings and options. Therefore, some detailed areas may not be shown exactly the same as on the actual vehicle screen.</ptxt>
</item>
<item>
<ptxt>To help find the malfunction when the system cannot change to diagnostic mode, check for DTCs using the intelligent tester.</ptxt>
</item>
<item>
<ptxt>After the engine switch is turned on (IG), check that the map is displayed before starting diagnostic mode. Otherwise, some items cannot be checked.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Start the engine.</ptxt>
<figure>
<graphic graphicname="E196428E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>"INFO" Switch</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>While pressing and holding the "INFO" switch, operate the light control switch: Off → Tail → Off → Tail → Off → Tail → Off.</ptxt>
</step2>
<step2>
<ptxt>Diagnostic mode starts and the "Service Menu" screen will be displayed.</ptxt>
</step2>
</step1>
<step1>
<ptxt>FINISH DIAGNOSTIC MODE</ptxt>
<step2>
<ptxt>There are 2 methods to finish diagnostic mode. Start the mode by using one of them.</ptxt>
</step2>
<step2>
<ptxt>Method 1</ptxt>
<step3>
<ptxt>Turn the engine switch off.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Method 2</ptxt>
<step3>
<ptxt>Press and hold the "SETUP" switch for 3 seconds or more.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>FAILURE DIAGNOSIS</ptxt>
<step2>
<ptxt>Press the "Failure Diagnosis" switch on the "Service Menu" screen.</ptxt>
<figure>
<graphic graphicname="E245677" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>The "Failure Diagnosis" will be displayed.</ptxt>
<figure>
<graphic graphicname="E245678" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>SYSTEM CHECK</ptxt>
<step2>
<ptxt>Press the "System Check" switch on the "Failure Diagnosis" screen.</ptxt>
<figure>
<graphic graphicname="E245679" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>The "System Check Mode" screen will be displayed.</ptxt>
<figure>
<graphic graphicname="E251475" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>CHECK DTC (CHECK USING SYSTEM CHECK MODE SCREEN)</ptxt>
<step2>
<ptxt>System check mode screen description</ptxt>
<figure>
<graphic graphicname="E251475E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Screen Description</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.83in"/>
<colspec colname="COLSPEC0" colwidth="4.25in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a: Node position number for devices that use MOST communication</ptxt>
</entry>
<entry valign="middle">
<ptxt>MOST node position numbers are provided for devices connected to the MOST network.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*b: Device Name List No. 1</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Device Name List No. 1 displays some of the devices that make up the audio and visual system. </ptxt>
</item>
<item>
<ptxt>The names of the components from Device Name List No. 1 are shown in the following table.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*c: Check Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Result codes for all devices are displayed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*d: Memory Clear</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Present and history DTCs and registered connected device names are cleared.</ptxt>
</item>
<item>
<ptxt>Press the "Memory CLR" switch for 3 seconds.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*e: Recheck</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>A system check will be performed again after the memory is cleared.</ptxt>
</item>
<item>
<ptxt>The "Recheck" switch will dim during a system check.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*f: Device Name List No. 2</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Device Name List No. 2 displays some of the devices that make up the audio and visual system.</ptxt>
</item>
<item>
<ptxt>The names of the components from Device Name List No. 2 are shown in the following table.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>*b: Device Name List No. 1 Description</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COLSPEC0" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Name</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Connection Method</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>EMV-M</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-media module receiver assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AMP</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stereo component amplifier assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Communication line for MOST</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RSE</ptxt>
</entry>
<entry valign="middle">
<ptxt>Television display assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Communication line for MOST</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DISP</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-display assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Communication line for AVC-LAN</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>CAMERA-C</ptxt>
</entry>
<entry valign="middle">
<ptxt>Parking assist ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Communication line for AVC-LAN</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>*c: Check Result Description</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.40in"/>
<colspec colname="COL2" colwidth="2.85in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Meaning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Action</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>The device does not respond with a DTC.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MOST</ptxt>
</entry>
<entry valign="middle">
<ptxt>MOST communication error</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perform "MOST Line Check" to check connection of each device on the MOST network.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DETAIL</ptxt>
</entry>
<entry valign="middle">
<ptxt>The device responds with a DTC.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Look up the DTC in "Unit Check Mode".</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NCON</ptxt>
</entry>
<entry valign="middle">
<ptxt>The device was previously present, but does not respond in diagnostic mode.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Check power supply wire harness of the device.</ptxt>
</item>
<item>
<ptxt>Check the AVC-LAN of the device.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NRES</ptxt>
</entry>
<entry valign="middle">
<ptxt>The device responds in diagnostic mode, but gives no DTC information.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Check power supply wire harness of the device.</ptxt>
</item>
<item>
<ptxt>Check the AVC-LAN of the device.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>*f: Device Name List No. 2 Description</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COLSPEC0" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Name</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Connection Method</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>VTR/FR</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headphone terminal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle wire harness</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AUX</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 stereo jack adapter assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle wire harness</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Unit check mode screen description</ptxt>
<figure>
<graphic graphicname="E246895E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Screen Description</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.83in"/>
<colspec colname="COL2" colwidth="4.25in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a: Device name</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target device</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*b: History DTC</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic memory results and stored DTCs are displayed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*c: Current DTC</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTCs output in the service check are displayed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*d: DTC</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC (Diagnostic Trouble Code)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*e: Diagnosis clear switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pushing this switch for 3 seconds clears the diagnostic memory data of the target device. (Both response to diagnostic system check result and the displayed data are cleared.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>This screen is updated once per second.</ptxt>
</item>
<item>
<ptxt>A maximum of 6 DTCs can be displayed for history and present DTCs.</ptxt>
</item>
</list1>
</atten4>
</step2>
<step2>
<ptxt>Read the system check result.</ptxt>
<step3>
<ptxt>If the check result is "DETAIL", touch the displayed check result </ptxt>
<figure>
<graphic graphicname="E251475E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When all results are "OK", this means that no DTCs are stored.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>View the results on the "Unit Check Mode" screen and record them.</ptxt>
<figure>
<graphic graphicname="E246895" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>A maximum of 6 DTCs can be displayed for history and present DTCs on the "Unit check mode" screen. Therefore, when 6 DTCs are displayed, troubleshoot those DTCs first and then check the "Unit check mode" screen again to see if any other DTCs are displayed.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>When proceeding to view the results of another device, press the "Back" switch to return to the "System Check Mode" screen. Repeat the step above to view the results of other devices.</ptxt>
<figure>
<graphic graphicname="E246895E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>To check other item, repeat from the first step.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Check the details of the DTC(s) (See page <xref label="Seep08" href="RM0000011BO0JSX"/>).</ptxt>
<atten3>
<ptxt>The navigation system outputs DTCs for the following system. When DTCs other than the codes in Diagnostic Trouble Code Chart for the navigation system are output, refer to Diagnostic Trouble Code Chart for the relevant system.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>System</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Audio and Visual System (w/ Navigation System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep15" href="RM0000017YP0BPX"/>
</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Rear Seat Entertainment System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep17" href="RM000003WTP02WX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten3>
</step3>
</step2>
</step1>
<step1>
<ptxt>DTC CLEAR/RECHECK (CLEAR USING SYSTEM CHECK MODE SCREEN)</ptxt>
<step2>
<ptxt>Clear DTC</ptxt>
<step3>
<ptxt>Press the "Memory CLR" switch for 3 seconds.</ptxt>
<figure>
<graphic graphicname="E251475E08" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>Confirm that the check results are cleared.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To clear the DTC for a specific device, clear the DTC using the "Unit Check Mode" screen.</ptxt>
</item>
<item>
<ptxt>When clearing a DTC using the "Unit Check Mode" screen, press the "Code CLR" switch for 3 seconds.</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
<step2>
<ptxt>Recheck DTC</ptxt>
<figure>
<graphic graphicname="E251475E09" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Press the "Recheck" switch.</ptxt>
</step3>
<step3>
<ptxt>Confirm that all diagnostic codes are "OK" when the check results are displayed. If a code other than "OK" is displayed, troubleshoot again.</ptxt>
<figure>
<graphic graphicname="E251475E10" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When a DTC was cleared using the "Unit Check Mode" screen, press the "Back" switch to return to the "System Check Mode" screen and perform this operation.</ptxt>
</atten4>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>