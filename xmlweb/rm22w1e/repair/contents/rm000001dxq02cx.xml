<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000001DXQ02CX" category="C" type-id="8053D" name-id="BCGG7-01" from="201308">
<dtccode>C146E</dtccode>
<dtcname>Open in ABS Solenoid Relay Circuit</dtcname>
<dtccode>C146F</dtccode>
<dtcname>Short in ABS Solenoid Relay Circuit</dtcname>
<subpara id="RM000001DXQ02CX_01" type-id="60" category="03" proc-id="RM22W0E___0000ASG00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The solenoid relay is built into the master cylinder solenoid. </ptxt>
<ptxt>This relay supplies power to each solenoid. If the initial check is OK, after the ignition switch is turned to ON, the relay turns on.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C146E</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>Both of the following conditions continue for at least 0.2 seconds.</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>IG voltage is between 9.5 and 17 V. </ptxt>
</item>
<item>
<ptxt>The relay contact is open when the relay is on.</ptxt>
</item>
</list2>
<item>
<ptxt>Both of the following conditions continue for at least 0.2 seconds.</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>IG voltage is 9.5 V or less when the relay is on. </ptxt>
</item>
<item>
<ptxt>The relay contact remains open.</ptxt>
</item>
</list2>
</list1>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>ABS2 fuse</ptxt>
</item>
<item>
<ptxt>Solenoid relay circuit</ptxt>
</item>
<item>
<ptxt>Master cylinder solenoid</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C146F</ptxt>
</entry>
<entry valign="middle">
<ptxt>The following condition continues for at least 0.2 seconds.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The relay contact is closed immediately after turning the ignition switch ON when the relay is off.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001DXQ02CX_02" type-id="32" category="03" proc-id="RM22W0E___0000ASH00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1225, C1226, C1227, C1228, C1468, C1469, C146A and C146B (See page <xref label="Seep01" href="RM000001EA202OX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000001DXQ02CX_03" type-id="51" category="05" proc-id="RM22W0E___0000ASI00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration (See page <xref label="Seep01" href="RM000001DWZ01OX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001DXQ02CX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001DXQ02CX_04_0002" proc-id="RM22W0E___0000ASJ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (+BS TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214161E74" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-31 (+BS) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU [Master Cylinder Solenoid])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXQ02CX_04_0003" fin="false">OK</down>
<right ref="RM000001DXQ02CX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXQ02CX_04_0003" proc-id="RM22W0E___0000AO900001">
<testtitle>CHECK HARNESS AND CONNECTOR (GND1, GND2 AND GND3 TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<figure>
<graphic graphicname="C124799E58" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the skid control ECU (master cylinder solenoid) connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COLSPEC0" colwidth="1.11in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-1 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-32 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A25-4 (GND3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU [Master Cylinder Solenoid])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXQ02CX_04_0004" fin="false">OK</down>
<right ref="RM000001DXQ02CX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXQ02CX_04_0004" proc-id="RM22W0E___0000ASK00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>These codes are stored when a problem is detected in the master cylinder solenoid.</ptxt>
<ptxt>The solenoid relay is in the master cylinder solenoid.</ptxt>
<ptxt>Therefore, solenoid relay circuit inspections and relay unit inspections cannot be performed. Be sure to check if any DTC is output before replacing the master cylinder solenoid.</ptxt>
</atten4>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00VX"/>). </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of approximately 32 km/h (20 mph) or more for 60 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTCs are output (See page <xref label="Seep02" href="RM0000046KV00VX"/>).</ptxt>
<atten4>
<ptxt>Reinstall the sensors, connectors, etc. and restore the previous vehicle conditions before rechecking for DTCs.</ptxt>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXQ02CX_04_0009" fin="true">A</down>
<right ref="RM000001DXQ02CX_04_0005" fin="true">B</right>
<right ref="RM000001DXQ02CX_04_0011" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXQ02CX_04_0005">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXQ02CX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DXQ02CX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DXQ02CX_04_0009">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXQ02CX_04_0011">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>