<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R1_T00K4" variety="T00K4">
<name>REAR SEAT ENTERTAINMENT SYSTEM</name>
<para id="RM000003WQB03AX" category="C" type-id="803X9" name-id="AVF6C-01" from="201308">
<dtccode>B15D5</dtccode>
<dtcname>Rear Seat Entertainment System Disconnected</dtcname>
<subpara id="RM000003WQB03AX_01" type-id="60" category="03" proc-id="RM22W0E___0000BTK00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The television display assembly and multi-media module receiver assembly are connected by a MOST network.</ptxt>
<ptxt>When a MOST network error occurs between the display module board and multi-display assembly, these DTCs will be stored.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B15D5*</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A device that is listed in the MOST network connected device record of the master unit is missing.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Television display assembly power source circuit</ptxt>
</item>
<item>
<ptxt>MOST network</ptxt>
</item>
<item>
<ptxt>Multi-display assembly</ptxt>
</item>
<item>
<ptxt>Multi-media module receiver assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*: Even if no fault is present, this DTC may be stored depending on the battery condition or engine start voltage.</ptxt>
</item>
<item>
<ptxt>The multi-media module receiver assembly is the master unit.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000003WQB03AX_02" type-id="32" category="03" proc-id="RM22W0E___0000BTL00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E248578E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003WQB03AX_03" type-id="51" category="05" proc-id="RM22W0E___0000BTM00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003WQB03AX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003WQB03AX_05_0019" proc-id="RM22W0E___0000BTR00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000003XO002VX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WQB03AX_05_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WQB03AX_05_0003" proc-id="RM22W0E___0000BTN00001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTC is output again (See page <xref label="Seep01" href="RM000003XO002VX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WQB03AX_05_0020" fin="true">OK</down>
<right ref="RM000003WQB03AX_05_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQB03AX_05_0005" proc-id="RM22W0E___0000BTO00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TELEVISION DISPLAY ASSEMBLY - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the television display assembly connector.</ptxt>
<figure>
<graphic graphicname="E245657E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L53-3 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L53-4 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L53-1 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L53-11 (+B2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Television Display Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000003WQB03AX_05_0023" fin="false">OK</down>
<right ref="RM000003WQB03AX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQB03AX_05_0023" proc-id="RM22W0E___0000BTS00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA MODULE RECEIVER ASSEMBLY - TELEVISION DISPLAY ASSEMBLY)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F84 multi-media module receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the L56 television display assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>F84-2 (MI+) - L56-5 (MO+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>F84-3 (MI-) - L56-6 (MO-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>F84-4 (SLDI) - L56-7 (SLDO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>F84-2 (MI+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>F84-3 (MI-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>F84-4 (SLDI) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000003WQB03AX_05_0024" fin="false">OK</down>
<right ref="RM000003WQB03AX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQB03AX_05_0024" proc-id="RM22W0E___0000BTT00001">
<testtitle>REPLACE TELEVISION DISPLAY ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the television display assembly with a new or normally functioning one (See page <xref label="Seep01" href="RM000002BC801HX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WQB03AX_05_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WQB03AX_05_0012" proc-id="RM22W0E___0000BTP00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000003XO002VX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WQB03AX_05_0015" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WQB03AX_05_0015" proc-id="RM22W0E___0000BTQ00001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTC is output again (See page <xref label="Seep01" href="RM000003XO002VX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WQB03AX_05_0022" fin="true">OK</down>
<right ref="RM000003WQB03AX_05_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQB03AX_05_0020">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WQB03AX_05_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003WQB03AX_05_0016">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY024X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WQB03AX_05_0022">
<testtitle>END (TELEVISION DISPLAY ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>