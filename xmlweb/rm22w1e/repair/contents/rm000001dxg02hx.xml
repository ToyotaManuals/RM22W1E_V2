<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000001DXG02HX" category="C" type-id="803M2" name-id="BCE8W-01" from="201301">
<dtccode>C1203</dtccode>
<dtcname>ECM Communication Circuit Malfunction</dtcname>
<subpara id="RM000001DXG02HX_01" type-id="60" category="03" proc-id="RM22W0E___0000AHM00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The circuit is used to send control information from the skid control ECU (master cylinder solenoid) to the ECM, and engine control information from the ECM to the skid control ECU (master cylinder solenoid) via the CAN communication system.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1203</ptxt>
</entry>
<entry valign="middle">
<ptxt>The engine/drivetrain specification and/or destination information from the ECM does not match the skid control ECU data.</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001DXG02HX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001DXG02HX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001DXG02HX_04_0006" proc-id="RM22W0E___0000AHO00000">
<testtitle>CHECK ENGINE TYPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the engine type.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1VD-FTV</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1UR-FE</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3UR-FE</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXG02HX_04_0001" fin="false">A</down>
<right ref="RM000001DXG02HX_04_0007" fin="false">B</right>
<right ref="RM000001DXG02HX_04_0008" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXG02HX_04_0001" proc-id="RM22W0E___0000AHN00000">
<testtitle>CHECK DTC (FOR ENGINE CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the engine control system DTC is output (w/o DPF: See page <xref label="Seep01" href="RM000000PDK14IX"/>, w/ DPF: See page <xref label="Seep02" href="RM000000PDK14JX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output (w/o DPF)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output (w/ DPF)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXG02HX_04_0002" fin="true">A</down>
<right ref="RM000001DXG02HX_04_0003" fin="true">B</right>
<right ref="RM000001DXG02HX_04_0009" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXG02HX_04_0007" proc-id="RM22W0E___0000AHP00000">
<testtitle>CHECK DTC (FOR ENGINE CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the engine control system DTC is output (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXG02HX_04_0012" fin="true">A</down>
<right ref="RM000001DXG02HX_04_0010" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000001DXG02HX_04_0008" proc-id="RM22W0E___0000AHQ00000">
<testtitle>CHECK DTC (FOR ENGINE CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the engine control system DTC is output (See page <xref label="Seep01" href="RM000000PDK14HX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXG02HX_04_0013" fin="true">A</down>
<right ref="RM000001DXG02HX_04_0011" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000001DXG02HX_04_0003">
<testtitle>GO TO ENGINE CONTROL SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM0000012WB07FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXG02HX_04_0009">
<testtitle>GO TO ENGINE CONTROL SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM0000012WB07GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXG02HX_04_0010">
<testtitle>GO TO ENGINE CONTROL SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000000PD90Y2X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXG02HX_04_0011">
<testtitle>GO TO ENGINE CONTROL SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000000PD90Y3X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXG02HX_04_0002">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXG02HX_04_0012">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXG02HX_04_0013">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>