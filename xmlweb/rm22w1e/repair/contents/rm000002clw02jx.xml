<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T9_T00MC" variety="T00MC">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000002CLW02JX" category="J" type-id="800UJ" name-id="TD716-01" from="201301" to="201308">
<dtccode/>
<dtcname>Back Door Entry Unlock Function does not Operate</dtcname>
<subpara id="RM000002CLW02JX_01" type-id="60" category="03" proc-id="RM22W0E___0000ER400000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the back door opener switch is turned on, the certification ECU (smart key ECU assembly) uses the electrical key antenna (for outside luggage compartment) and antenna to output a request signal to the outside of the vehicle for a distance of approximately 1.0 m (3.28 ft.) from the back door. A key near the back door detects this request and transmits its ID code. The door control receiver receives the ID code and sends this ID code to the certification ECU (smart key ECU assembly). The certification ECU (smart key ECU assembly) checks if the ID code is valid. When the ID code is valid, the certification ECU (smart key ECU assembly) sends a back door open signal to the main body ECU using the CAN communication line and stops outputting request signals. The main body ECU then unlocks the back door.</ptxt>
</content5>
</subpara>
<subpara id="RM000002CLW02JX_02" type-id="32" category="03" proc-id="RM22W0E___0000ER500000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B194360E02" width="7.106578999in" height="4.7836529in"/>
</figure>
<figure>
<graphic graphicname="B187333E05" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002CLW02JX_03" type-id="51" category="05" proc-id="RM22W0E___0000ER600000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Before performing the inspection, check that there are no problems related to the "CAN Communication System" and "LIN Communication System".</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002CLW02JX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002CLW02JX_06_0001" proc-id="RM22W0E___0000ER700000">
<testtitle>CHECK POWER DOOR LOCK OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When the door control switch of the master switch assembly is operated, check that the locked doors unlock.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Door locks operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002CLW02JX_06_0026" fin="false">OK</down>
<right ref="RM000002CLW02JX_06_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0026" proc-id="RM22W0E___0000ERB00000">
<testtitle>CHECK VEHICLE EQUIPMENT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the power back door is equipped on the vehicle.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>w/ Power Back Door System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/o Power Back Door System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002CLW02JX_06_0027" fin="false">A</down>
<right ref="RM000002CLW02JX_06_0029" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0027" proc-id="RM22W0E___0000ERC00000">
<testtitle>CHECK POWER BACK DOOR OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When the power back door switch is operated, check that the power back door opens.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Power back door system is normal.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Power back door system is abnormal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Power back door system is normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002CLW02JX_06_0028" fin="true">A</down>
<right ref="RM000002CLW02JX_06_0002" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0029" proc-id="RM22W0E___0000ERD00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR LOCK POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Main Body / Data List.</ptxt>
</test1>
<test1>
<ptxt>Read the Data List according to the display on the intelligent tester.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item / Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Back Lock Pos switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Back door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Back door unlocked</ptxt>
<ptxt>OFF: Back door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between ON and OFF as shown in the chart above.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002CLW02JX_06_0002" fin="false">OK</down>
<right ref="RM000002CLW02JX_06_0030" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0002" proc-id="RM22W0E___0000ER800000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (BACK DOOR HANDLE SW)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Handle SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door opener switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Back door open switch is pushed</ptxt>
<ptxt>OFF: Back door open switch is not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>"ON" (switch is pushed) and "OFF" (switch is not pushed) appears on the screen.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002CLW02JX_06_0025" fin="true">OK</down>
<right ref="RM000002CLW02JX_06_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0003" proc-id="RM22W0E___0000ER900000">
<testtitle>CHECK BACK DOOR OPENER SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="B185531E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>3 (B) - 2 (L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 (B) - 2 (L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open switch pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002CLW02JX_06_0008" fin="false">OK</down>
<right ref="RM000002CLW02JX_06_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0008" proc-id="RM22W0E___0000ERA00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - BACK DOOR OPENER SWITCH)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B154889E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Disconnect the E1 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the S7 switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E1-26 (BDSU) - S7-3 (B)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-26 (BDSU) or S7-3 (B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002CLW02JX_06_0025" fin="true">OK</down>
<right ref="RM000002CLW02JX_06_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0010">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM<xref label="Seep01" href="RM000000TNQ04DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0028">
<testtitle>GO TO POWER BACK DOOR SYSTEM<xref label="Seep01" href="RM000003EBQ00BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0025">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0012">
<testtitle>REPLACE BACK DOOR OPENER SWITCH<xref label="Seep01" href="RM0000053CT001X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0017">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002CLW02JX_06_0030">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM (Proceed to Only Back Door LOCK/UNLOCK Functions do not Operate)<xref label="Seep01" href="RM000000TNU06PX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>