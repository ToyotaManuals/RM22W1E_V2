<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TW_T00MZ" variety="T00MZ">
<name>AIRBAG SYSTEM</name>
<para id="RM000002NNA06RX" category="C" type-id="303NJ" name-id="RSAS3-03" from="201301" to="201308">
<dtccode>B1635/24</dtccode>
<dtcname>Rear Airbag Sensor LH Circuit Malfunction</dtcname>
<dtccode>B1637/82</dtccode>
<dtcname>Lost Communication with Rear Airbag Sensor LH</dtcname>
<dtccode>B1690/15</dtccode>
<dtcname>Door Side Airbag Sensor RH Malfunction</dtcname>
<dtccode>B1692/81</dtccode>
<dtcname>Lost Communication with Door Side Airbag Sensor RH</dtcname>
<subpara id="RM000002NNA06RX_01" type-id="60" category="03" proc-id="RM22W0E___0000F9J00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<ptxt>"Door side airbag sensor RH" refer to the side airbag sensor RH.</ptxt>
</atten4>
<list1 type="unordered">
<item>
<ptxt>For DTC B1690 and B1635:</ptxt>
<ptxt>The rear airbag sensor LH and side airbag sensor RH consists of the safing sensor, the diagnostic circuit, the lateral deceleration sensor, etc.</ptxt>
<ptxt>If the center airbag sensor receives signals from the lateral deceleration sensor, it determines whether the SRS should be activated.</ptxt>
<ptxt>DTC B1690/15 or B1635/24 is stored when a malfunction is detected in the rear airbag sensor LH or side airbag sensor RH circuit.</ptxt>
</item>
<item>
<ptxt>For DTC B1692 and B1637:</ptxt>
<ptxt>The circuit for the side collision sensor RH or LH (to determine deployment of the front seat airbag RH or LH, rear seat side airbag RH or LH and curtain shield airbag RH or LH) is composed of the center airbag sensor, side airbag sensor RH or LH, and rear airbag sensor RH or LH.</ptxt>
<ptxt>The side airbag sensor RH or LH, and rear airbag sensor RH or LH detect impacts to the vehicle and send signals to the center airbag sensor to determine if the airbag should be deployed.</ptxt>
<ptxt>DTC B1692 or B1637 is stored when a malfunction is detected in the circuit for the side collision sensor RH or LH (to determine deployment of the front seat airbag RH or LH, rear seat side airbag RH or LH, and curtain shield airbag RH or LH).</ptxt>
</item>
</list1>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1635/24</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>A side airbag sensor LH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Rear airbag sensor LH</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B1637/82</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives a line short circuit signal, an open circuit signal, a short circuit to B+ signal in the circuit for the side collision sensor LH (to determine deployment of the front seat airbag LH, rear seat side airbag LH and curtain shield airbag LH) for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A side airbag sensor LH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Rear airbag sensor LH</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B1690/15</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>A side airbag sensor RH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Side airbag sensor RH</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B1692/81</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor receives a line short circuit signal, an open circuit signal, a short circuit to B+ signal in the circuit for the side collision sensor RH (to determine deployment of the front seat airbag RH, rear seat side airbag RH and curtain shield airbag RH) for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A side airbag sensor RH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Side airbag sensor RH</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002NNA06RX_02" type-id="32" category="03" proc-id="RM22W0E___0000F9K00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C175144E02" width="7.106578999in" height="5.787629434in"/>
</figure>
<figure>
<graphic graphicname="C175145E02" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002NNA06RX_03" type-id="51" category="05" proc-id="RM22W0E___0000F9L00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000002NNA06RX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002NNA06RX_04_0001" proc-id="RM22W0E___0000F9M00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs stored in the memory (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1635, B1637, B1690 and B1692 are not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1635 or B1637 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1690 or B1692 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Codes other than DTC B1635, B1637, B1690 and B1692 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002NNA06RX_04_0002" fin="false">C</down>
<right ref="RM000002NNA06RX_04_0004" fin="true">A</right>
<right ref="RM000002NNA06RX_04_0003" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002NNA06RX_04_0002" proc-id="RM22W0E___0000F9N00000">
<testtitle>CHECK SIDE AIRBAG SENSOR RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C171827E10" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Interchange the side airbag sensor RH with LH and connect the connectors to them.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs stored in the memory (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1695, B1697, B1690 and B1692 are not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1695 or B1697 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1690 or B1692 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Codes other than DTC B1690, B1692, B1695 and B1697 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Return the side airbag sensor RH and LH to their original positions and connect the connectors to them.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002NNA06RX_04_0007" fin="true">C</down>
<right ref="RM000002NNA06RX_04_0004" fin="true">A</right>
<right ref="RM000002NNA06RX_04_0005" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002NNA06RX_04_0003" proc-id="RM22W0E___0000F9O00000">
<testtitle>CHECK REAR AIRBAG SENSOR LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C146229E20" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Temporarily replace the rear airbag sensor LH with a new one.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs stored in the memory (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1635 and B1637 are not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1635 and B1637 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Return the side airbag sensor RH and LH to their original positions and connect the connectors to them.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002NNA06RX_04_0006" fin="true">OK</down>
<right ref="RM000002NNA06RX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NNA06RX_04_0004">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0IZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002NNA06RX_04_0007">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000002Y2N00UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002NNA06RX_04_0006">
<testtitle>REPLACE REAR AIRBAG SENSOR LH<xref label="Seep01" href="RM000002OF8015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002NNA06RX_04_0005">
<testtitle>REPLACE SIDE AIRBAG SENSOR RH<xref label="Seep01" href="RM000002OF8015X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>