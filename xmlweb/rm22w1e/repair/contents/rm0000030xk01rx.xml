<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000B" variety="S000B">
<name>1UR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000B_7C3F7_T008A" variety="T008A">
<name>ENGINE UNIT</name>
<para id="RM0000030XK01RX" category="A" type-id="30014" name-id="EMBVD-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000030XK01RX_01" type-id="01" category="01">
<s-1 id="RM0000030XK01RX_01_0041" proc-id="RM22W0E___00004L000000">
<ptxt>INSTALL NOISE FILTER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Bank 2:</ptxt>
<ptxt>Install the noise filter to the cylinder head cover with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>for Bank 1:</ptxt>
<ptxt>Install the noise filter to the cylinder head cover with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0032" proc-id="RM22W0E___00004KX00000">
<ptxt>INSTALL IGNITION COIL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 8 ignition coils with the 8 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0045" proc-id="RM22W0E___00004L100000">
<ptxt>INSTALL NO. 11 WATER BY-PASS HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 11 water by-pass hose.</ptxt>
<figure>
<graphic graphicname="A214840E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When connecting the hose, make sure the paint marks and clips are as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>The direction of the hose clamp is indicated in the illustration.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0010" proc-id="RM22W0E___00004KU00000">
<ptxt>INSTALL ENGINE WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 wire harness clamps to install the engine wire.</ptxt>
</s2>
<s2>
<ptxt>Connect the 4 knock sensor connectors.</ptxt>
</s2>
<s2>
<ptxt>Install the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0070">
<ptxt>INSTALL NO. 1 ENGINE COVER</ptxt>
</s-1>
<s-1 id="RM0000030XK01RX_01_0071">
<ptxt>INSTALL NO. 2 ENGINE COVER</ptxt>
</s-1>
<s-1 id="RM0000030XK01RX_01_0034" proc-id="RM22W0E___000015600000">
<ptxt>INSTALL SEPARATOR CASE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the separator case with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000030XK01RX_01_0006" proc-id="RM22W0E___00004KS00000">
<ptxt>INSTALL NO. 1 IDLER PULLEY SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 idler pulley with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>43</t-value1>
<t-value2>438</t-value2>
<t-value4>32</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000030XK01RX_01_0007" proc-id="RM22W0E___00004KT00000">
<ptxt>INSTALL WATER PUMP PULLEY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the water pump pulley with the 4 bolts.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A230493E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using SST, hold the water pump pulley and tighten the 4 bolts.</ptxt>
<sst>
<sstitem>
<s-number>09960-10010</s-number>
<s-subnumber>09962-01000</s-subnumber>
<s-subnumber>09963-01000</s-subnumber>
</sstitem>
</sst>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000030XK01RX_01_0053">
<ptxt>INSTALL NO. 1 WATER BY-PASS HOSE</ptxt>
</s-1>
<s-1 id="RM0000030XK01RX_01_0078" proc-id="RM22W0E___00004L700000">
<ptxt>INSTALL NO. 2 WATER BY-PASS PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 4 hoses.</ptxt>
</s2>
<s2>
<ptxt>Install the water by-pass pipe with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000030XK01RX_01_0015" proc-id="RM22W0E___000015D00000">
<ptxt>INSTALL NO. 3 ENGINE COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 3 engine cover.</ptxt>
<figure>
<graphic graphicname="A220035" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Engine Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Position the No. 3 engine cover so that the arrow mark faces the front of the engine and install it.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000030XK01RX_01_0016" proc-id="RM22W0E___000015E00000">
<ptxt>INSTALL NO. 4 ENGINE COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 4 engine cover.</ptxt>
<figure>
<graphic graphicname="A220036" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Engine Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Position the No. 4 engine cover so that the arrow mark faces the front of the engine and install it.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000030XK01RX_01_0038" proc-id="RM22W0E___00004KZ00000">
<ptxt>INSTALL NO. 2 FUEL DELIVERY PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 delivery pipe spacers and 4 insulators to the cylinder head LH.</ptxt>
</s2>
<s2>
<ptxt>Install the delivery pipe together with the injectors to the cylinder head LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<atten3>
<figure>
<graphic graphicname="A163659E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Make sure that the part of the injector labeled B is between the parts of the delivery pipe labeled A.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Delivery Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Fuel Injector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Connect the No. 7 wire harness connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030XK01RX_01_0037" proc-id="RM22W0E___00004KY00000">
<ptxt>INSTALL FUEL DELIVERY PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 delivery pipe spacers and 4 insulators to the cylinder head RH.</ptxt>
</s2>
<s2>
<ptxt>Install the delivery pipe together with the injectors to the cylinder head RH.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<atten3>
<figure>
<graphic graphicname="A163659E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Make sure that the part of the injector labeled B is between the parts of the delivery pipe labeled A.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Delivery Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Fuel Injector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Connect the No. 6 wire harness connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030XK01RX_01_0025" proc-id="RM22W0E___00004KV00000">
<ptxt>INSTALL FRONT NO. 1 ENGINE MOUNTING BRACKET LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front No. 1 engine mounting bracket LH with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0026" proc-id="RM22W0E___00004KW00000">
<ptxt>INSTALL FRONT NO. 1 ENGINE MOUNTING BRACKET RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front No. 1 engine mounting bracket RH with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0074" proc-id="RM22W0E___000015800000">
<ptxt>INSTALL NO. 1 EGR PIPE BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 EGR pipe bracket with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0075" proc-id="RM22W0E___000015900000">
<ptxt>INSTALL NO. 1 WATER OUTLET PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 water outlet pipe with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0056" proc-id="RM22W0E___000015A00000">
<ptxt>CONNECT NO. 11 WATER BY-PASS HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 11 water by-pass hose.</ptxt>
<figure>
<graphic graphicname="A239698E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upper Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The direction of the hose clamp is indicated in the illustration.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0063" proc-id="RM22W0E___000015B00000">
<ptxt>INSTALL NO. 8 WATER BY-PASS HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 8 water by-pass hose.</ptxt>
<figure>
<graphic graphicname="A239699E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upper Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When connecting the hose, make sure the paint marks and clips are as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>The direction of each hose clamp is indicated in the illustration.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0058" proc-id="RM22W0E___000015C00000">
<ptxt>INSTALL NO. 2 WATER BY-PASS PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 water by-pass pipe with the 2 bolts and connect the hose.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A239700E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upper Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When connecting the hose, make sure the paint marks and clips are as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>The direction of each hose clamp is indicated in the illustration.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0059" proc-id="RM22W0E___00004L200000">
<ptxt>INSTALL NO. 3 AIR TUBE (w/ Secondary Air Injection System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 3 air tube with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0060" proc-id="RM22W0E___00004L300000">
<ptxt>INSTALL NO. 4 AIR TUBE (w/ Secondary Air Injection System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 4 air tube with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0061" proc-id="RM22W0E___00004L400000">
<ptxt>INSTALL AIR SWITCHING VALVE ASSEMBLY (for Bank 2)</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>for Bank 1 (See page <xref label="Seep01" href="RM00000436200AX"/>)</ptxt>
</item>
<item>
<ptxt>for Bank 2 (See page <xref label="Seep02" href="RM00000436600AX"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM0000030XK01RX_01_0076" proc-id="RM22W0E___00004L500000">
<ptxt>INSTALL FUEL TUBE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel tube (See page <xref label="Seep01" href="RM0000028RU03WX"/>).</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000030XK01RX_01_0077" proc-id="RM22W0E___00004L600000">
<ptxt>INSTALL FUEL HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel hose (See page <xref label="Seep01" href="RM0000028RU03WX"/>).</ptxt>
</s2>
<s2>
<ptxt>Install the fuel pipe clamp.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030XK01RX_01_0067">
<ptxt>INSTALL ENGINE WIRE</ptxt>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>