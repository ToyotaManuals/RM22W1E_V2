<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000187G07QX" category="C" type-id="302B6" name-id="ESRAL-04" from="201308">
<dtccode>P0340</dtccode>
<dtcname>Camshaft Position Sensor "A" Circuit (Bank 1 or Single Sensor)</dtcname>
<subpara id="RM00000187G07QX_01" type-id="60" category="03" proc-id="RM22W0E___00002KB00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The camshaft position sensor (G signal) consists of a magnet, iron core and pickup coil.</ptxt>
<ptxt>The G signal plate (timing sprocket) has one tooth on its outer circumference and is installed on the camshaft timing chain gear. When the timing gear rotates, the protrusion on the signal plate and the air gap on the pickup coil change, causing fluctuations in the magnetic field that generate a voltage in the pickup coil.</ptxt>
<ptxt>The NE signal plate has 34 teeth and is mounted on the crank angle sensor plate. The NE signal sensor generates 34 signals for every revolution. The ECM detects the standard crankshaft angle based on the G signal, and detects the actual crankshaft angle and engine speed based on the NE signal.</ptxt>
<table pgwide="1">
<title>P0340</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Crank engine for 4 seconds (STA on)</ptxt>
</entry>
<entry>
<ptxt>STA on:</ptxt>
<ptxt>No camshaft position sensor signal is sent to the ECM while cranking for 4 seconds or more (2 trip detection logic).</ptxt>
</entry>
<entry morerows="1">
<list1 type="unordered">
<item>
<ptxt>Open or short in camshaft position sensor circuit</ptxt>
</item>
<item>
<ptxt>Camshaft position sensor</ptxt>
</item>
<item>
<ptxt>No. 2 camshaft timing sprocket</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry>
<ptxt>Idle engine for 1 second (STA off)</ptxt>
</entry>
<entry>
<ptxt>STA off:</ptxt>
<ptxt>When either condition below is met for 1 second (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>G signal is not input with an engine speed of 500 rpm or more.</ptxt>
</item>
<item>
<ptxt>No camshaft position sensor signal is sent to the ECM with an engine speed of 500 to 3000 rpm 20 times or more.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTC P0340 is stored, the following symptoms may appear:</ptxt>
<list1 type="nonmark">
<item>
<ptxt>- Difficult starting</ptxt>
</item>
<item>
<ptxt>- Misfire</ptxt>
</item>
<item>
<ptxt>- Combustion noise</ptxt>
</item>
<item>
<ptxt>- Black smoke</ptxt>
</item>
<item>
<ptxt>- White smoke</ptxt>
</item>
<item>
<ptxt>- Lack of power</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187G07QX_02" type-id="32" category="03" proc-id="RM22W0E___00002KC00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0335 (See page <xref label="Seep01" href="RM00000187F07MX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000187G07QX_03" type-id="51" category="05" proc-id="RM22W0E___00002KD00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08YX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06NX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187G07QX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187G07QX_04_0001" proc-id="RM22W0E___00002KE00001">
<testtitle>INSPECT CAMSHAFT POSITION SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the camshaft position sensor connector.</ptxt>
<figure>
<graphic graphicname="A165742E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cold</ptxt>
</entry>
<entry valign="middle">
<ptxt>835 to 1400 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hot</ptxt>
</entry>
<entry valign="middle">
<ptxt>1060 to 1645 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>In the table above, the terms "Cold" and "Hot" refer to the temperature of the coils in the sensor. "Cold" means approximately -10 to 50°C (14 to 122°F). "Hot" means approximately 50 to 100°C (122 to 212°F).</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000187G07QX_04_0002" fin="false">OK</down>
<right ref="RM00000187G07QX_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187G07QX_04_0002" proc-id="RM22W0E___00002KF00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CAMSHAFT POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the camshaft position sensor connector.</ptxt>
<figure>
<graphic graphicname="A182476E01" width="2.775699831in" height="5.787629434in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C66-1 - C45-80 (G+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C66-2 - C45-79 (G-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>

</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C66-1 - C46-80 (G+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C66-2 - C46-79 (G-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C66-1 or C45-80 (G+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C66-2 or C45-79 (G-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C66-1 or C46-80 (G+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C66-2 or C46-79 (G-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187G07QX_04_0003" fin="false">OK</down>
<right ref="RM00000187G07QX_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187G07QX_04_0003" proc-id="RM22W0E___00002KG00001">
<testtitle>CHECK CAMSHAFT POSITION SENSOR (SENSOR INSTALLATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the sensor installation.</ptxt>
<figure>
<graphic graphicname="BR03795E15" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>Sensor is installed correctly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187G07QX_04_0004" fin="false">OK</down>
<right ref="RM00000187G07QX_04_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187G07QX_04_0004" proc-id="RM22W0E___00002KH00001">
<testtitle>CHECK NO. 2 CAMSHAFT TIMING SPROCKET</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the condition of the timing sprocket.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Timing sprocket does not have any cracks or deformation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187G07QX_04_0005" fin="false">OK</down>
<right ref="RM00000187G07QX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187G07QX_04_0005" proc-id="RM22W0E___00002KI00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329203AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187G07QX_04_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187G07QX_04_0006" proc-id="RM22W0E___00002KJ00001">
<testtitle>REPLACE CAMSHAFT POSITION SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the camshaft position sensor (See page <xref label="Seep01" href="RM000002PQ402NX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187G07QX_04_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187G07QX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<res>
<right ref="RM00000187G07QX_04_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187G07QX_04_0008" proc-id="RM22W0E___00002KK00001">
<testtitle>SECURELY REINSTALL SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Securely reinstall the sensor (See page <xref label="Seep01" href="RM000002PQ202OX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187G07QX_04_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187G07QX_04_0009" proc-id="RM22W0E___00002KL00001">
<testtitle>REPLACE NO. 2 CAMSHAFT TIMING SPROCKET</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the No. 2 camshaft timing sprocket (See page <xref label="Seep01" href="RM0000032A9008X_01_0053"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187G07QX_04_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187G07QX_04_0010" proc-id="RM22W0E___00002KM00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK18AX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and idle it for 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187G07QX_04_0011" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187G07QX_04_0011">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>