<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000W880S1X" category="C" type-id="302GG" name-id="ESUAX-04" from="201301" to="201308">
<dtccode>P0724</dtccode>
<dtcname>Brake Switch "B" Circuit High</dtcname>
<subpara id="RM000000W880S1X_01" type-id="60" category="03" proc-id="RM22W0E___00001DX00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The stop light switch is part of a duplex system that transmits 2 signals: STP and ST1-. These 2 signals are used by the ECM to monitor whether or not the brake system is working properly. This DTC indicates that the stop light switch remains on. When the stop light switch remains on during GO and STOP driving, the ECM interprets this as a fault in the stop light switch. Then the MIL illuminates and the ECM stores the DTC.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0724</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch remains on even when the vehicle is driven in a GO (30 km/h (18.63 mph) or more) and STOP (less than 3 km/h (1.86 mph)) pattern 5 times (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in stop light switch signal circuit</ptxt>
</item>
<item>
<ptxt>Stop light switch assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W880S1X_02" type-id="64" category="03" proc-id="RM22W0E___00001DY00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates that the stop light switch remains on. When the stop light switch remains on during GO and STOP driving, the ECM interprets this as a fault in the stop light switch. Then the MIL illuminates and the ECM stores the DTC. The vehicle must GO (30 km/h (18.63 mph) or more) and STOP (less than 3 km/h (1.86 mph)) 5 times for 2 driving cycles in order for the DTC to be stored.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W880S1X_12" type-id="73" category="03" proc-id="RM22W0E___00001E400000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A199312E31" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on [A].</ptxt>
</item>
<item>
<ptxt>Wait 5 seconds.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes [B].</ptxt>
</item>
<item>
<ptxt>Read the pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a pending DTC is output, the system is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If a pending DTC is not output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P0724.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows INCOMPLETE or N/A, perform steps [C] through [E].</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Start the engine [C].</ptxt>
</item>
<item>
<ptxt>Accelerate the vehicle to 30 km/h (18.63 mph) or more, and then depress the brake pedal and decelerate the vehicle to 3 km/h (1.86 mph) or less [D]. Repeat step [D] 5 times.</ptxt>
<atten2>
<ptxt>When performing the confirmation driving pattern, obey all speed limits and traffic laws.</ptxt>
</atten2>
</item>
<item>
<ptxt>Check the DTC judgment result [E].</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000W880S1X_06" type-id="32" category="03" proc-id="RM22W0E___00001DZ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0504 (See page <xref label="Seep01" href="RM000000XCT0WXX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000W880S1X_07" type-id="51" category="05" proc-id="RM22W0E___00001E000000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Using the GTS, the Data List item "Stop Light Switch" can be read.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="1.77in"/>
<colspec colname="COL2" align="left" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/</ptxt>
<ptxt>Range (Display)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Brake pedal depressed: ON</ptxt>
</item>
<item>
<ptxt>Brake pedal released: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W880S1X_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W880S1X_08_0009" proc-id="RM22W0E___00001E300000">
<testtitle>CHECK STOP LIGHT SWITCH ASSEMBLY INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the stop light switch assembly installation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Stop light switch assembly is installed correctly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W880S1X_08_0003" fin="false">OK</down>
<right ref="RM000000W880S1X_08_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W880S1X_08_0003" proc-id="RM22W0E___00001E100000">
<testtitle>INSPECT STOP LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the stop light switch assembly (See page <xref label="Seep01" href="RM0000038XN00FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W880S1X_08_0004" fin="false">OK</down>
<right ref="RM000000W880S1X_08_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W880S1X_08_0004" proc-id="RM22W0E___00001E200000">
<testtitle>CHECK HARNESS AND CONNECTOR (ECM - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" align="right" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A52-36 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake pedal is depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>7.5 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A52-36 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake pedal is released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" align="right" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A38-36 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake pedal is depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>7.5 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A38-36 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake pedal is released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W880S1X_08_0005" fin="true">OK</down>
<right ref="RM000000W880S1X_08_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W880S1X_08_0010">
<testtitle>SECURELY REINSTALL STOP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM0000038XM00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W880S1X_08_0007">
<testtitle>REPLACE STOP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM0000038XP00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W880S1X_08_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W880S1X_08_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>