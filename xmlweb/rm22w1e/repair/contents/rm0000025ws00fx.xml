<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3Y1_T00R4" variety="T00R4">
<name>POWER BACK DOOR SYSTEM</name>
<para id="RM0000025WS00FX" category="J" type-id="80106" name-id="DH3EZ-01" from="201301" to="201308">
<dtccode/>
<dtcname>Power Back Door Opener / Closer Switch Circuit</dtcname>
<subpara id="RM0000025WS00FX_01" type-id="60" category="03" proc-id="RM22W0E___0000IN700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The door control switch only turns on while the switch is being pressed, and turns off when the switch is released.</ptxt>
<ptxt>When the switch is on, a power back door operation request signal is input to the main body ECU (cowl side junction block LH) to operate the back door.</ptxt>
</content5>
</subpara>
<subpara id="RM0000025WS00FX_02" type-id="32" category="03" proc-id="RM22W0E___0000IN800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B312261E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000025WS00FX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000025WS00FX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000025WS00FX_04_0007" proc-id="RM22W0E___0000INB00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (BACK DOOR OPEN SW)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the Data List for proper functioning of the door control switch (See page <xref label="Seep01" href="RM000003GK3002X"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Open SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door control switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Door control switch on</ptxt>
<ptxt>OFF: Door control switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000025WS00FX_04_0008" fin="true">OK</down>
<right ref="RM0000025WS00FX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000025WS00FX_04_0002" proc-id="RM22W0E___0000IN900000">
<testtitle>INSPECT DOOR CONTROL SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the door control switch (See page <xref label="Seep01" href="RM000003FAG00NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the door control switch (See page <xref label="Seep02" href="RM000003G9000HX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000025WS00FX_04_0003" fin="false">OK</down>
<right ref="RM0000025WS00FX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000025WS00FX_04_0003" proc-id="RM22W0E___0000INA00000">
<testtitle>CHECK HARNESS AND CONNECTOR (DOOR CONTROL SWITCH - MAIN BODY ECU [COWL SIDE JUNCTION BLOCK LH] AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E105 door control switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E2 main body ECU (cowl side junction block LH) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E105-6 (CSW) - E2-2 (PBDS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E105-3 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E105-6 (CSW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000025WS00FX_04_0004" fin="true">OK</down>
<right ref="RM0000025WS00FX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000025WS00FX_04_0004">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM0000025WS00FX_04_0005">
<testtitle>REPLACE DOOR CONTROL SWITCH<xref label="Seep01" href="RM000003FAG00NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000025WS00FX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000025WS00FX_04_0008">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000003EBU00BX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>