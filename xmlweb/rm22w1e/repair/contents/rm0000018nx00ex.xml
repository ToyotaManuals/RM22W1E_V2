<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001Q" variety="S001Q">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001Q_7C3Q0_T00J3" variety="T00J3">
<name>VANE PUMP (for 1VD-FTV without DPF)</name>
<para id="RM0000018NX00EX" category="G" type-id="3001K" name-id="PA1VH-01" from="201301">
<name>INSPECTION</name>
<subpara id="RM0000018NX00EX_01" type-id="01" category="01">
<s-1 id="RM0000018NX00EX_01_0001" proc-id="RM22W0E___0000B0I00000">
<ptxt>CHECK OIL CLEARANCE BETWEEN VANE PUMP SHAFT AND VANE PUMP REAR HOUSING BUSHING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a micrometer, measure the outer diameter [a] of the shaft.</ptxt>
<figure>
<graphic graphicname="C161394E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a vernier caliper, measure the inner diameter [b] of the rear housing bush.</ptxt>
</s2>
<s2>
<ptxt>Calculate the oil clearance.</ptxt>
<ptxt>Oil clearance = Inner diameter of the bush [b] - Outer diameter of the shaft [a]</ptxt>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt>0.08 mm (0.00315 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the oil clearance is more than the maximum, replace the vane pump assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018NX00EX_01_0002" proc-id="RM22W0E___0000B0J00000">
<ptxt>INSPECT VANE PUMP ROTOR AND VANE PUMP PLATES</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a micrometer, measure the thickness of the 10 vane pump plates.</ptxt>
<figure>
<graphic graphicname="C114411E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard thickness</title>
<specitem>
<ptxt>1.405 to 1.411 mm (0.0554 to 0.0556 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the thickness is not as specified, replace the vane pump assembly.</ptxt>
</s2>
<s2>
<ptxt>Using a feeler gauge, measure the clearance between the side face of the rotor groove and the plate.</ptxt>
<figure>
<graphic graphicname="C114412E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum clearance</title>
<specitem>
<ptxt>0.03 mm (0.00118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is more than the maximum, replace the vane pump assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018NX00EX_01_0003" proc-id="RM22W0E___0000B0K00000">
<ptxt>INSPECT FLOW CONTROL VALVE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the flow control valve with power steering fluid and check that it falls smoothly into the valve hole by its own weight.</ptxt>
<figure>
<graphic graphicname="C161395" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If the control valve does not fall into the hole smoothly, replace the vane pump assembly.</ptxt>
</s2>
<s2>
<ptxt>Check the flow control valve for leakage.</ptxt>
<figure>
<graphic graphicname="C114414E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Close one of the holes and apply 390 to 490 kPa (4.0 to 5.0 kgf/cm<sup>2</sup>, 57 to 71 psi) of compressed air into the opposite side hole, and confirm that air does not come out from the end holes.</ptxt>
<ptxt>If air leaks, replace the vane pump assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018NX00EX_01_0004" proc-id="RM22W0E___0000B0L00000">
<ptxt>INSPECT FLOW CONTROL VALVE COMPRESSION SPRING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the free length of the compression spring.</ptxt>
<figure>
<graphic graphicname="C114415E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Minimum free length</title>
<specitem>
<ptxt>31.3 mm (1.23 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the length is less than the minimum, replace the vane pump assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018NX00EX_01_0005" proc-id="RM22W0E___0000B0M00000">
<ptxt>INSPECT PRESSURE PORT UNION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Visually check the pressure port union for fluid leaks.</ptxt>
<ptxt>If there is a leak, replace the vane pump assembly.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>