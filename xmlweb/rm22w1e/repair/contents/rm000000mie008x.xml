<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001S" variety="S001S">
<name>STEERING COLUMN</name>
<ttl id="12038_S001S_7C3QI_T00JL" variety="T00JL">
<name>STEERING COLUMN ASSEMBLY (for Manual Tilt and Manual Telescopic Steering Column)</name>
<para id="RM000000MIE008X" category="A" type-id="8000E" name-id="SR3J7-01" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM000000MIE008X_02" type-id="11" category="10" proc-id="RM22W0E___0000BK000000">
<content3 releasenbr="1">
<atten3>
<ptxt>When using a vise, do not overtighten it.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM000000MIE008X_01" type-id="01" category="01">
<s-1 id="RM000000MIE008X_01_0028" proc-id="RM22W0E___0000BJG00000">
<ptxt>INSTALL STEERING MAIN SHAFT ASSEMBLY (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the steering main shaft assembly to the steering column upper tube.</ptxt>
</s2>
<s2>
<ptxt>Using a snap ring expander, install a new shaft snap ring to the steering main shaft assembly. </ptxt>
<figure>
<graphic graphicname="C104470E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure the steering main shaft snap ring is securely installed to the groove.</ptxt>
</item>
<item>
<ptxt>Do not damage the steering main shaft assembly.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0029" proc-id="RM22W0E___0000BJH00000">
<ptxt>INSTALL NO. 1 TILT STEERING SUPPORT COLLAR (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 No. 1 tilt steering support collars to the steering lower column tube assembly.</ptxt>
<figure>
<graphic graphicname="C104850E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When installing the No. 1 tilt steering support collar, make sure that the cutout portion is aligned as shown in the illustration.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0030" proc-id="RM22W0E___0000BJI00000">
<ptxt>INSTALL STEERING COLUMN BRACKET SPACER (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clamp the steering column bracket spacer and steering lower column tube assembly in a vise between a cloth and aluminum plates, and install the steering column bracket spacer to the steering lower column tube assembly using the vise. </ptxt>
<figure>
<graphic graphicname="C104853E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<ptxt>When installing the steering column bracket spacer, make sure that it is positioned so that the inner diameter is as shown in the illustration.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0031" proc-id="RM22W0E___0000BJJ00000">
<ptxt>INSTALL TELESCOPIC STEERING GUIDE (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the 3 protrusions of the telescopic steering guide with the 3 holes of the steering lower column tube assembly and install the telescopic steering guide.</ptxt>
<figure>
<graphic graphicname="C104854E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0032" proc-id="RM22W0E___0000BJK00000">
<ptxt>INSTALL STEERING LOWER COLUMN TUBE ASSEMBLY (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply MP grease to the area of the steering main shaft assembly indicated by the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="C214697E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the steering lower column tube assembly to the steering column upper tube.</ptxt>
<figure>
<graphic graphicname="C248949" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0051" proc-id="RM22W0E___0000BJY00000">
<ptxt>INSTALL TILT STEERING SUPPORT (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the tilt steering support with the bolt.</ptxt>
<figure>
<graphic graphicname="C247264" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>153</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0033" proc-id="RM22W0E___0000BJL00000">
<ptxt>INSTALL NO. 1 STEERING COLUMN RING (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a snap ring expander, install a new No. 1 steering column ring to the steering main shaft.</ptxt>
<figure>
<graphic graphicname="C214675E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure the No. 1 steering column ring is securely installed to the groove.</ptxt>
</item>
<item>
<ptxt>Do not damage the steering main shaft assembly.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0035" proc-id="RM22W0E___0000BJN00000">
<ptxt>INSTALL TILT STEERING PAWL ASSEMBLY (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the tilt steering pawl assembly to the steering column upper tube.</ptxt>
<figure>
<graphic graphicname="C104475" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When installing the tilt steering pawl assembly, position the opening as shown in the illustration.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0048" proc-id="RM22W0E___0000BJV00000">
<ptxt>INSTALL BREAK AWAY BRACKET (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply MP grease to the sliding areas of a new break away bracket and install it to the steering column upper tube.</ptxt>
<figure>
<graphic graphicname="C248948" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0046" proc-id="RM22W0E___0000BJT00000">
<ptxt>INSTALL STEERING COLUMN TUBE STOPPER (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply MP grease to the sliding surface of the steering column tube stopper (the one with the groove) and install it to the break away bracket.</ptxt>
<figure>
<graphic graphicname="C248950E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Align the flat edge of the steering column tube stopper and the raised sliding area of the break away bracket as shown in the illustration.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Apply MP grease to the sliding surface of the steering column tube stopper (the one without the groove) and install it to the break away bracket.</ptxt>
<figure>
<graphic graphicname="C248951E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Align the flat edge of the steering column tube stopper and the raised sliding area of the break away bracket as shown in the illustration.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0037" proc-id="RM22W0E___0000BJO00000">
<ptxt>INSTALL NO. 1 TILT LEVER LOCK BOLT (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply MP grease to the sliding surface of the steering tilt lever and install it to the No. 1 tilt lever lock bolt.</ptxt>
<figure>
<graphic graphicname="C104479E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the No. 1 tilt lever lock bolt to the break away bracket.</ptxt>
<figure>
<graphic graphicname="C248952" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not drop the steering column tube stopper.</ptxt>
</item>
<item>
<ptxt>Pass the No. 1 tilt lever lock bolt through the hole of the tilt steering pawl assembly.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Apply MP grease to the sliding surface of the thrust needle roller bearing and install it to the No. 1 tilt lever lock bolt.</ptxt>
<figure>
<graphic graphicname="C248953" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Temporarily install the tilt steering rotor to the No. 1 tilt lever lock bolt with a new No. 1 tilt steering adjusting nut.</ptxt>
</s2>
<s2>
<ptxt>Using an 8 mm socket hexagon wrench, tighten the No. 1 tilt lever lock bolt.</ptxt>
<figure>
<graphic graphicname="C248954" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>2.0</t-value1>
<t-value2>20</t-value2>
<t-value3>18</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>The tightening direction for the No. 1 tilt lever lock bolt is the tightening direction for bolts with a left-handed thread.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0038" proc-id="RM22W0E___0000BJP00000">
<ptxt>INSTALL NO. 1 TILT STEERING SUPPORT REINFORCEMENT (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 tilt steering support reinforcement to the steering tilt lever with the steering tilt lever bolt.</ptxt>
<figure>
<graphic graphicname="C248955" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.4</t-value1>
<t-value2>55</t-value2>
<t-value3>48</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>The tightening direction for the steering tilt lever bolt is the tightening direction for bolts with a left-handed thread.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0040" proc-id="RM22W0E___0000BJQ00000">
<ptxt>TIGHTEN NO. 1 TILT STEERING ADJUSTING NUT (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Lock the steering tilt lever.</ptxt>
<figure>
<graphic graphicname="C248956" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using an 8 mm socket hexagon wrench, hold the No. 1 tilt lever lock bolt in place and tighten the No. 1 tilt steering adjusting nut.</ptxt>
<figure>
<graphic graphicname="C248957E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>3.0</t-value1>
<t-value2>31</t-value2>
<t-value3>27</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using pliers, stake the No. 1 tilt steering adjusting nut as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C216017" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>After staking the No. 1 tilt steering adjusting nut, make sure it is not loose.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0041" proc-id="RM22W0E___0000BJR00000">
<ptxt>INSTALL TILT STEERING JUMP UP SPRING (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using pliers, move the tilt steering jump up spring in the direction indicated by the arrow in the illustration to install it to the break away bracket.</ptxt>
<figure>
<graphic graphicname="C104486" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>To avoid spatter, cover the area with a cloth.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0042" proc-id="RM22W0E___0000BJS00000">
<ptxt>INSTALL TILT LEVER RETURN SPRING (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, install the tilt lever return spring to the break away bracket and steering tilt lever.</ptxt>
<sst>
<sstitem>
<s-number>09921-00010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C248939E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>To avoid spatter, cover the area with a cloth.</ptxt>
</item>
<item>
<ptxt>Do not let the damper fall off of the tilt lever return spring.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0052" proc-id="RM22W0E___0000BJZ00000">
<ptxt>INSTALL TILT STEERING SUPPORT BOND CABLE (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the steering support bond cable to the steering column with the 2 screws.</ptxt>
<figure>
<graphic graphicname="C246796" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0034" proc-id="RM22W0E___0000BJM00000">
<ptxt>INSTALL BREAK AWAY COLLAR SUB-ASSEMBLY (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 new break away collars and 2 new break away capsules to the break away bracket.</ptxt>
<figure>
<graphic graphicname="C104462E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not compress the disc spring portion of the break away collar.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0047" proc-id="RM22W0E___0000BJU00000">
<ptxt>INSTALL IGNITION OR STARTER SWITCH ASSEMBLY (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the ignition or starter switch assembly to the steering column upper bracket assembly.</ptxt>
<figure>
<graphic graphicname="C104552E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0005" proc-id="RM22W0E___0000BJB00000">
<ptxt>INSTALL UNLOCK WARNING SWITCH ASSEMBLY (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the unlock warning switch assembly to the steering column upper bracket assembly.</ptxt>
<figure>
<graphic graphicname="C104837E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Move the unlock warning switch assembly in the direction indicated by the arrow in the illustration to install it.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0006" proc-id="RM22W0E___0000BJC00000">
<ptxt>INSTALL IGNITION SWITCH LOCK CYLINDER ASSEMBLY (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the ignition switch lock cylinder assembly to ACC.</ptxt>
</s2>
<s2>
<ptxt>Insert the ignition switch lock cylinder assembly into the steering column upper bracket assembly to install it.</ptxt>
<figure>
<graphic graphicname="C214540E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Check that the ignition switch lock cylinder assembly is securely fixed in place.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0007" proc-id="RM22W0E___0000BJD00000">
<ptxt>INSPECT STEERING LOCK OPERATION (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the key and check that the steering lock operates.</ptxt>
<figure>
<graphic graphicname="C103934" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Insert the key, turn the ignition switch to ACC and check that the steering lock disengages.</ptxt>
<figure>
<graphic graphicname="C214539E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0008" proc-id="RM22W0E___0000BJE00000">
<ptxt>INSTALL STEERING COLUMN UPPER WITH SWITCH BRACKET ASSEMBLY (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the steering column upper with switch bracket assembly and steering column upper clamp with 2 new steering lock set bolts, and tighten the bolts until the heads break off.</ptxt>
<figure>
<graphic graphicname="C103936" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0009" proc-id="RM22W0E___0000BJF00000">
<ptxt>INSTALL TRANSPONDER KEY AMPLIFIER (w/o Entry and Start System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the transponder key amplifier to the steering column upper with switch bracket assembly.</ptxt>
<figure>
<graphic graphicname="C103938E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0050" proc-id="RM22W0E___0000BJX00000">
<ptxt>INSTALL KEY INTERLOCK SOLENOID (for Automatic Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the key interlock solenoid to the steering column upper bracket with the 2 screws.</ptxt>
<figure>
<graphic graphicname="G034357E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000MIE008X_01_0049" proc-id="RM22W0E___0000BJW00000">
<ptxt>INSTALL STEERING LOCK ACTUATOR ASSEMBLY (w/ Entry and Start System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C178341" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Temporarily install the steering lock actuator assembly with 2 new tapered-head bolts.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 2 tapered-head bolts until the bolt heads break off.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>