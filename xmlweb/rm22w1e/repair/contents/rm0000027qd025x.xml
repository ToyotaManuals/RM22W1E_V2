<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3X2_T00Q5" variety="T00Q5">
<name>POWER WINDOW CONTROL SYSTEM (for Models with Jam Protection Function on 4 Windows)</name>
<para id="RM0000027QD025X" category="L" type-id="3001A" name-id="WS0YK-36" from="201301">
<name>PRECAUTION</name>
<subpara id="RM0000027QD025X_z0" proc-id="RM22W0E___0000HW400000">
<content5 releasenbr="1">
<step1>
<ptxt>POWER WINDOW CONTROL SYSTEM PRECAUTIONS</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>The power window control system prohibits the auto up/down function when the jam protection function malfunctions. Therefore, troubleshoot the power window control system after confirming that no trouble codes are output.</ptxt>
</item>
<item>
<ptxt>When a power window regulator or a power window regulator motor assembly is replaced, the power window control system must be initialized. </ptxt>
</item>
<item>
<ptxt>After a door glass or a door glass run has been replaced, the jam protection function may operate unexpectedly when the auto up function is used. In such cases, the auto up function can be resumed by repeating the following operation at least 5 times:</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Close the power window by fully pulling up the power window switch and holding it at the auto up position.</ptxt>
</item>
<item>
<ptxt>Open the power window by fully pushing down the power window switch.</ptxt>
</item>
</list2>
<item>
<ptxt>The jam protection function may operate unexpectedly even when the power window control system is normal, due to detection of a value different from the operation learned value when the door glass moving speed changes sharply under any of the following conditions:</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>A power window is being opened or closed while the vehicle is driving on a rough road.</ptxt>
</item>
<item>
<ptxt>A power window is being opened or closed while the door is being opened or closed.</ptxt>
</item>
<item>
<ptxt>A sharp voltage change.</ptxt>
</item>
</list2>
</list1>
</atten2>
<atten3>
<ptxt>Do not disconnect the cable from the negative (-) battery terminal while the ignition switch is on (IG). Otherwise, DTC B2311 will be output.</ptxt>
</atten3>
</step1>
<step1>
<ptxt>IGNITION SWITCH EXPRESSIONS</ptxt>
<atten4>
<ptxt>The type of ignition switch used on this model differs according to the specifications of the vehicle. The expressions listed in the table below are used in this section.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Expression</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition Switch (Position)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine Switch (Condition)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition Switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LOCK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition Switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On (IG)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition Switch ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On (ACC)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine Start</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>START</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Start</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>