<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZC_T00SF" variety="T00SF">
<name>LIGHTING SYSTEM</name>
<para id="RM000002WL601IX" category="J" type-id="303SY" name-id="LE7ZT-01" from="201308">
<dtccode/>
<dtcname>Headlight (HI-BEAM) Circuit</dtcname>
<subpara id="RM000002WL601IX_01" type-id="60" category="03" proc-id="RM22W0E___0000J7Z00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The main body ECU receives a headlight hi switch information signal from the light control switch, and illuminates the headlight high beam.</ptxt>
</content5>
</subpara>
<subpara id="RM000002WL601IX_02" type-id="32" category="03" proc-id="RM22W0E___0000J8000001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E286790E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002WL601IX_03" type-id="51" category="05" proc-id="RM22W0E___0000J8100001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses and bulbs for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002WL601IX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002WL601IX_05_0001" proc-id="RM22W0E___0000J8200001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (HEAD HI RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, perform the Active Test (See page <xref label="Seep01" href="RM000002WL502AX"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Head Light Hi</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight (Hi)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Headlight high beam turns on/turns off.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002WL601IX_05_0016" fin="true">OK</down>
<right ref="RM000002WL601IX_05_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WL601IX_05_0005" proc-id="RM22W0E___0000J8300001">
<testtitle>INSPECT HEADLIGHT RELAY (HEAD HI)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the headlight relay from the engine room relay block, junction block.</ptxt>
<figure>
<graphic graphicname="E247660E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery voltage not applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery voltage applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002WL601IX_05_0029" fin="false">OK</down>
<right ref="RM000002WL601IX_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WL601IX_05_0029" proc-id="RM22W0E___0000J8400001">
<testtitle>CHECK HARNESS AND CONNECTOR (BATTERY - HEADLIGHT RELAY [HEAD HI])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the headlight relay from the engine room relay block, junction block.</ptxt>
<figure>
<graphic graphicname="E160869E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Headlight relay terminal 2 - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Headlight relay terminal 3 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Headlight Relay [HEAD HI])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002WL601IX_05_0031" fin="false">OK</down>
<right ref="RM000002WL601IX_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WL601IX_05_0031" proc-id="RM22W0E___0000J8500001">
<testtitle>CHECK HARNESS AND CONNECTOR (HEADLIGHT RELAY [HEAD HI] - MAIN BODY ECU [COWL SIDE JUNCTION BLOCK LH])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the headlight relay from the engine room relay block, junction block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the 2B main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Headlight relay terminal 1 - 2B-16 (DIM)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Headlight relay terminal 1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002WL601IX_05_0002" fin="true">OK</down>
<right ref="RM000002WL601IX_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WL601IX_05_0002">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002WKO02QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002WL601IX_05_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002WL601IX_05_0006">
<testtitle>REPLACE HEADLIGHT RELAY (HEAD HI)</testtitle>
</testgrp>
<testgrp id="RM000002WL601IX_05_0016">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>