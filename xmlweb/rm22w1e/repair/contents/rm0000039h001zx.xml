<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SF_T00LI" variety="T00LI">
<name>FRONT DOOR LOCK</name>
<para id="RM0000039H001ZX" category="A" type-id="80001" name-id="DL9F4-01" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000039H001ZX_02" type-id="11" category="10" proc-id="RM22W0E___0000E5500001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the LH side and RH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039H001ZX_01" type-id="01" category="01">
<s-1 id="RM0000039H001ZX_01_0027" proc-id="RM22W0E___0000E5400001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039H001ZX_01_0025" proc-id="RM22W0E___0000E5200001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039H001ZX_01_0012" proc-id="RM22W0E___0000BPE00001">
<ptxt>REMOVE FRONT DOOR LOWER FRAME BRACKET GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clip and claw, and remove the front door lower frame bracket garnish LH.</ptxt>
<figure>
<graphic graphicname="B180820" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0014" proc-id="RM22W0E___0000IDQ00001">
<ptxt>REMOVE FRONT DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 7 screws and front door inside handle bezel LH.</ptxt>
<figure>
<graphic graphicname="B235655" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0013" proc-id="RM22W0E___0000BPB00000">
<ptxt>REMOVE FRONT DOOR ARMREST BASE PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180817" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 5 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the armrest base panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0015" proc-id="RM22W0E___0000BPF00001">
<ptxt>REMOVE DOOR ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a moulding remover, detach the 8 claws and remove the door assist grip cover LH.</ptxt>
<figure>
<graphic graphicname="B180819" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0016" proc-id="RM22W0E___0000E4W00000">
<ptxt>REMOVE COURTESY LIGHT ASSEMBLY (w/ Courtesy Light)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw.</ptxt>
<figure>
<graphic graphicname="E155940" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the courtesy light and then disconnect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0017" proc-id="RM22W0E___0000BPC00001">
<ptxt>REMOVE FRONT DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B310800E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 13 clips.</ptxt>
</s2>
<s2>
<ptxt>Remove the front inner door glass weatherstrip LH together with the front door trim board sub-assembly LH by pulling them upward in the order shown in the illustration.</ptxt>
<atten4>
<ptxt>Make sure that the pin labeled A in the illustration is detached from the door panel.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 cables from the front door inside handle sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180822" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0018" proc-id="RM22W0E___0000E4X00001">
<ptxt>REMOVE FRONT DOOR SERVICE HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, detach the 9 clamps. </ptxt>
<figure>
<graphic graphicname="B182874" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>Remove the front door service hole cover LH.</ptxt>
<atten4>
<ptxt>Remove the remaining tape on the door.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0026" proc-id="RM22W0E___0000E5300001">
<ptxt>REMOVE FRONT NO. 2 DOOR STIFFENER CUSHION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B183088" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 clamps and front No. 2 door stiffener cushion.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0019" proc-id="RM22W0E___0000E4Y00001">
<ptxt>REMOVE FRONT DOOR REAR LOWER FRAME SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and front door rear lower frame sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180840" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0024" proc-id="RM22W0E___0000E5100001">
<ptxt>REMOVE FRONT DOOR OUTSIDE HANDLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the hole plug.</ptxt>
<figure>
<graphic graphicname="B180841" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a T30 "TORX" wrench, loosen the screw and remove the front door outside handle cover LH with the door lock cylinder installed.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the front door outside handle cover LH.</ptxt>
<figure>
<graphic graphicname="B180845" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0009" proc-id="RM22W0E___0000E4V00001">
<ptxt>REMOVE FRONT DOOR LOCK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T30 "TORX" wrench, remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B180842" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Remove the door lock through the service hole.</ptxt>
</atten4>
<atten3>
<ptxt>Be careful when removing the screws as the door lock may fall and become damaged.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039H001ZX_01_0020" proc-id="RM22W0E___0000E4Z00001">
<ptxt>REMOVE FRONT DOOR LOCK REMOTE CONTROL CABLE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front door lock remote control cable assembly LH.</ptxt>
<figure>
<graphic graphicname="B180843" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H001ZX_01_0021" proc-id="RM22W0E___0000E5000001">
<ptxt>REMOVE FRONT DOOR INSIDE LOCKING LH CABLE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 3 claws.</ptxt>
<figure>
<graphic graphicname="B180846E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the front door inside locking cable assembly LH.</ptxt>
<figure>
<graphic graphicname="B180847" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>