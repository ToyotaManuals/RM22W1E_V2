<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000000TMD0D5X" category="C" type-id="300Y7" name-id="ES11H6-002" from="201308">
<dtccode>P0420</dtccode>
<dtcname>Catalyst System Efficiency Below Threshold (Bank 1)</dtcname>
<dtccode>P0430</dtccode>
<dtcname>Catalyst System Efficiency Below Threshold (Bank 2)</dtcname>
<subpara id="RM000000TMD0D5X_02" type-id="64" category="03" proc-id="RM22W0E___00000F000001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM uses sensors mounted in front of and behind the front catalyst to monitor its efficiency.</ptxt>
<ptxt>The first sensor, the air fuel ratio sensor, sends pre-catalyst information to the ECM. The second sensor, the heated oxygen sensor, sends post-catalyst information to the ECM.</ptxt>
<ptxt>In order to detect any deterioration in the three-way catalytic converter, the ECM calculates the Oxygen Storage Capacity (OSC) of the three-way catalytic converter. This calculation is based on the voltage output of the heated oxygen sensor while performing active air-fuel ratio control.</ptxt>
<ptxt>The OSC value is an indication of the oxygen storage capacity of the three-way catalytic converter. When the vehicle is being driven with a warm engine, active air-fuel ratio control is performed for approximately 15 to 20 seconds. When it is performed, the ECM deliberately sets the air-fuel ratio to lean or rich levels. If the rich-lean cycle of the heated oxygen sensor is long, the OSC becomes greater. There is a direct correlation between the OSCs of the heated oxygen sensor and three-way catalytic converter.</ptxt>
<ptxt>The ECM uses the OSC value to determine the state of the three-way catalytic converter. If any deterioration has occurred, it illuminates the MIL and stores the DTC.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0420</ptxt>
</entry>
<entry valign="middle">
<ptxt>OSC value is less than the standard value under active air-fuel ratio control (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Gas leakage from exhaust system</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (bank 1 sensor 1)</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor (bank 1 sensor 2)</ptxt>
</item>
<item>
<ptxt>Exhaust manifold sub-assembly RH (TWC: Front catalyst)</ptxt>
</item>
<item>
<ptxt>Front Exhaust Pipe Assembly (TWC: Rear catalyst)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0430</ptxt>
</entry>
<entry valign="middle">
<ptxt>OSC value is less than the standard value under active air-fuel ratio control (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Gas leakage from exhaust system</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (bank 2 sensor 1)</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor (bank 2 sensor 2)</ptxt>
</item>
<item>
<ptxt>Exhaust manifold sub-assembly LH (TWC: Front catalyst)</ptxt>
</item>
<item>
<ptxt>Front No. 2 Exhaust Pipe Assembly (TWC: Rear catalyst)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000TMD0D5X_16" type-id="87" category="03" proc-id="RM22W0E___00000FF00001">
<name>CATALYST LOCATION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A266659E01" width="7.106578999in" height="7.795582503in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Secondary Air Injection System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Secondary Air Injection System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Manifold Sub-assembly RH (TWC: Front catalyst)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Manifold Sub-assembly LH (TWC: Front catalyst)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Exhaust Pipe Assembly (TWC: Rear catalyst)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front No. 2 Exhaust Pipe Assembly (TWC: Rear catalyst)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Exhaust Pipe Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tailpipe Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Fuel Ratio Sensor (for Bank 1 Sensor 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Fuel Ratio Sensor (for Bank 2 Sensor 1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated Oxygen Sensor (for Bank 1 Sensor 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated Oxygen Sensor (for Bank 2 Sensor 2)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000TMD0D5X_15" type-id="73" category="03" proc-id="RM22W0E___00000FE00001">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Performing this confirmation driving pattern will activate the catalyst efficiency monitor. This is very useful for verifying the completion of a repair.</ptxt>
</atten4>
<figure>
<graphic graphicname="A231470E65" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the ignition switch to ON.</ptxt>
</item>
<item>
<ptxt>Turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</item>
<item>
<ptxt>Turn the ignition switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Start the engine and warm it up until the engine coolant temperature is 75°C (167°F) or higher [A].</ptxt>
</item>
<item>
<ptxt>Drive the vehicle at approximately 60 km/h (38 mph) for 10 minutes or more [B].</ptxt>
<atten4>
<ptxt>Drive the vehicle while keeping the engine load as constant as possible.</ptxt>
</atten4>
<atten2>
<ptxt>When performing the confirmation driving pattern, obey all speed limits and traffic laws.</ptxt>
</atten2>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes [C].</ptxt>
</item>
<item>
<ptxt>Read the pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a pending DTC is output, the system is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If a pending DTC is not output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P0420 or P0430.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows INCOMPLETE or N/A, perform steps [D] through [E].</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Drive the vehicle at a speed between 75 and 100 km/h (47 and 63 mph) for 10 minutes or more [D].</ptxt>
<atten4>
<ptxt>Drive the vehicle while keeping the engine load as constant as possible.</ptxt>
</atten4>
<atten2>
<ptxt>When performing the confirmation driving pattern, obey all speed limits and traffic laws.</ptxt>
</atten2>
</item>
<item>
<ptxt>Check the DTC judgment result again [E].</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000TMD0D5X_12" type-id="74" category="03" proc-id="RM22W0E___00000FD00001">
<name>CONDITIONING FOR SENSOR TESTING</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Perform the operation with the engine speeds and time durations described below prior to checking the waveforms of the air fuel ratio and heated oxygen sensors. This is in order to activate the sensors sufficiently to obtain appropriate inspection results.</ptxt>
</atten4>
<figure>
<graphic graphicname="A118003E64" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3 (a).</ptxt>
</item>
<item>
<ptxt>Start the engine and warm it up with all the accessories switched off until the engine coolant temperature stabilizes (b).</ptxt>
</item>
<item>
<ptxt>Run the engine at an engine speed between 2500 rpm and 3000 rpm for at least 3 minutes (c).</ptxt>
</item>
<item>
<ptxt>While running the engine at 3000 rpm for 2 seconds, and then 2000 rpm for 2 seconds, check the waveforms of the air fuel ratio and heated oxygen sensors using the GTS (d).</ptxt>
</item>
</list1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the voltage output of the air fuel ratio sensor or heated oxygen sensor does not fluctuate, or there is noise in the waveform of either sensor, the sensor which has those problems may be malfunctioning.</ptxt>
</item>
<item>
<ptxt>If the voltage outputs of both sensors remain lean or rich, the air-fuel ratio may be extremely lean or rich. In such cases, perform the Control the Injection Volume for A/F Sensor Active Test using the GTS.</ptxt>
</item>
<item>
<ptxt>If the three-way catalytic converter has deteriorated, the heated oxygen sensor (located behind the three-way catalytic converter) voltage output fluctuates up and down frequently, even under normal driving conditions (active air-fuel ratio control is not performed).</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="A121610E50" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TMD0D5X_09" type-id="51" category="05" proc-id="RM22W0E___00000F100001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a malfunction cannot be found when troubleshooting DTC P0420 or P0430, a lean or rich abnormality may be the cause. Perform troubleshooting by following the inspection procedure for P0171 or P0174 (System Too Lean) and P0172 or P0175 (System Too Rich).</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Sensor 1 refers to the sensor closest to the engine assembly.</ptxt>
</item>
<item>
<ptxt>Sensor 2 refers to the sensor farthest away from the engine assembly.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TMD0D5X_11" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TMD0D5X_11_0001" proc-id="RM22W0E___00000F200001">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0420 OR P0430)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0420 or P0430 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0420 or P0430 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0420 or P0430 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000TMD0D5X_11_0011" fin="false">A</down>
<right ref="RM000000TMD0D5X_11_0015" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0011" proc-id="RM22W0E___00000F500001">
<testtitle>PERFORM ACTIVE TEST USING GTS (INJECTION VOLUME)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Run the engine at an engine speed of 2500 rpm for approximately 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F Sensor.</ptxt>
</test1>
<test1>
<ptxt>Perform the Control the Injection Volume for A/F Sensor operation with the engine idling.</ptxt>
</test1>
<test1>
<ptxt>Monitor the voltage outputs of the air fuel ratio sensor and heated oxygen sensor (AFS Voltage B1S1 and O2S B1S2 or AFS Voltage B2S1 and O2S B2S2) displayed on the GTS.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Change the fuel injection volume within the range of - 12.5% to +12.5%.</ptxt>
</item>
<item>
<ptxt>Each sensor reacts in accordance with increases and decreases in the fuel injection volume.</ptxt>
</item>
<item>
<ptxt>The air fuel ratio sensor has an output delay of a few seconds and the heated oxygen sensor has a maximum output delay of approximately 20 seconds.</ptxt>
</item>
<item>
<ptxt>If the sensor output voltage does not change (almost no reaction) while performing the Active Test, the sensor may be malfunctioning.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>Standard</title>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
<ptxt>(Sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Injection Volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Voltage</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>AFS Voltage B1S1 or AFS Voltage B2S1</ptxt>
<ptxt>(Air fuel ratio)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 3.1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 3.4 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>O2S B1S2 or O2S B2S2</ptxt>
<ptxt>(Heated oxygen)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 0.55 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 0.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="6">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="0.71in"/>
<colspec colname="COL3" colwidth="1.98in"/>
<colspec colname="COL4" colwidth="0.71in"/>
<colspec colname="COL5" colwidth="2.41in"/>
<colspec colname="COL6" colwidth="0.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Status</ptxt>
<ptxt>AFS Voltage B1S1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status</ptxt>
<ptxt>O2S B1S2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Actual air fuel ratio, air fuel ratio sensor and</ptxt>
<ptxt>heated oxygen sensor condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Misfire</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Suspected Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Three-way catalytic converter</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated oxygen sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Heated oxygen sensor</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated oxygen sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Heated oxygen sensor</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual air fuel ratio lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>May occur</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Extremely rich or lean actual air fuel ratio</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual air fuel ratio rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Extremely rich or lean actual air fuel ratio</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Lean: During Control the Injection Volume for A/F Sensor, the air fuel ratio sensor output voltage (AFS Voltage B1S1 or AFS Voltage B2S1) is consistently higher than 3.4 V, and the heated oxygen sensor output voltage (O2S B1S2 or O2S B2S2) is consistently below 0.4 V.</ptxt>
<ptxt>Rich: During Control the Injection Volume for A/F Sensor, the AFS Voltage B1S1 or AFS Voltage B2S1 is consistently below 3.1 V, and the O2S B1S2 or O2S B2S2 is consistently higher than 0.55 V.</ptxt>
<ptxt>Lean/Rich: During Control the Injection Volume for A/F Sensor of the Active Test, the output voltage of the air fuel ratio sensor and heated oxygen sensor alternate</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TMD0D5X_11_0005" fin="false">A</down>
<right ref="RM000000TMD0D5X_11_0012" fin="false">B</right>
<right ref="RM000000TMD0D5X_11_0002" fin="false">C</right>
<right ref="RM000000TMD0D5X_11_0023" fin="false">D</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0005" proc-id="RM22W0E___00000F400001">
<testtitle>CHECK FOR EXHAUST GAS LEAK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for exhaust gas leaks.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No gas leaks.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TMD0D5X_11_0021" fin="false">OK</down>
<right ref="RM000000TMD0D5X_11_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0021" proc-id="RM22W0E___00000FA00001">
<testtitle>CHECK DTC OUTPUT (DTC P0420 AND/OR P0430)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0420</ptxt>
</entry>
<entry valign="middle">
<ptxt>A, B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0430</ptxt>
</entry>
<entry valign="middle">
<ptxt>C, D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0420 and P0430</ptxt>
</entry>
<entry valign="middle">
<ptxt>A, B, C, D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TMD0D5X_11_0019" fin="true">D</down>
<right ref="RM000000TMD0D5X_11_0020" fin="true">A</right>
<right ref="RM000000TMD0D5X_11_0022" fin="true">B</right>
<right ref="RM000000TMD0D5X_11_0018" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0012" proc-id="RM22W0E___00000F600001">
<testtitle>REPLACE AIR FUEL RATIO SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air fuel ratio sensor (See page <xref label="Seep01" href="RM000002W9Q00XX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TMD0D5X_11_0017" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0002" proc-id="RM22W0E___00000F300001">
<testtitle>CHECK FOR EXHAUST GAS LEAK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for exhaust gas leaks.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No gas leaks.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TMD0D5X_11_0013" fin="false">OK</down>
<right ref="RM000000TMD0D5X_11_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0013" proc-id="RM22W0E___00000F700001">
<testtitle>REPLACE HEATED OXYGEN SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the heated oxygen sensor (See page <xref label="Seep01" href="RM000002W9N00VX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TMD0D5X_11_0017" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0023" proc-id="RM22W0E___00000FB00001">
<testtitle>CHECK FOR EXHAUST GAS LEAK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for exhaust gas leaks.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No gas leaks.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TMD0D5X_11_0014" fin="false">OK</down>
<right ref="RM000000TMD0D5X_11_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0014" proc-id="RM22W0E___00000F800001">
<testtitle>CHECK CAUSE OF EXTREMELY RICH OR LEAN ACTUAL AIR-FUEL RATIO</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the cause of extremely rich or lean actual air fuel ratio, referring to the DTC P0171 Inspection Procedure (See page <xref label="Seep01" href="RM000000WC30SMX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TMD0D5X_11_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0017" proc-id="RM22W0E___00000F900001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS ON.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK185X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle in accordance with the driving pattern described in Confirmation Driving Pattern.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry>
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC P0420 or P0430 is output</ptxt>
</entry>
<entry align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC is not output</ptxt>
</entry>
<entry align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TMD0D5X_11_0024" fin="false">A</down>
<right ref="RM000000TMD0D5X_11_0025" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0024" proc-id="RM22W0E___00000FC00001">
<testtitle>CHECK DTC OUTPUT (DTC P0420 AND/OR P0430)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0420</ptxt>
</entry>
<entry valign="middle">
<ptxt>A, B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0430</ptxt>
</entry>
<entry valign="middle">
<ptxt>C, D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0420 and P0430</ptxt>
</entry>
<entry valign="middle">
<ptxt>A, B, C, D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TMD0D5X_11_0019" fin="true">D</down>
<right ref="RM000000TMD0D5X_11_0020" fin="true">A</right>
<right ref="RM000000TMD0D5X_11_0022" fin="true">B</right>
<right ref="RM000000TMD0D5X_11_0018" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0015">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM000002ZSO010X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0006">
<testtitle>REPAIR OR REPLACE EXHAUST GAS LEAK POINT</testtitle>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0020">
<testtitle>REPLACE FRONT EXHAUST PIPE ASSEMBLY (TWC: REAR CATALYST)<xref label="Seep01" href="RM000002HA700MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0022">
<testtitle>REPLACE EXHAUST MANIFOLD SUB-ASSEMBLY RH (TWC: FRONT CATALYST)<xref label="Seep01" href="RM00000456G00MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0018">
<testtitle>REPLACE FRONT NO. 2 EXHAUST PIPE ASSEMBLY (TWC: REAR CATALYST)<xref label="Seep01" href="RM000002HA700MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0019">
<testtitle>REPLACE EXHAUST MANIFOLD SUB-ASSEMBLY LH (TWC: FRONT CATALYST)<xref label="Seep01" href="RM00000456G00MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TMD0D5X_11_0025">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>