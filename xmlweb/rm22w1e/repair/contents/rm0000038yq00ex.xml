<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12054_S0027" variety="S0027">
<name>METER / GAUGE / DISPLAY</name>
<ttl id="12054_S0027_7C3TP_T00MS" variety="T00MS">
<name>CLOCK</name>
<para id="RM0000038YQ00EX" category="A" type-id="80001" name-id="ME3F8-03" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000038YQ00EX_02" type-id="11" category="10" proc-id="RM22W0E___0000F1S00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for LHD and RHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000038YQ00EX_01" type-id="01" category="01">
<s-1 id="RM0000038YQ00EX_01_0021" proc-id="RM22W0E___0000BMT00001">
<ptxt>REMOVE NO. 1 SPEAKER OPENING COVER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B183763" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 8 claws and remove the opening cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038YQ00EX_01_0011" proc-id="RM22W0E___0000BMU00001">
<ptxt>REMOVE NO. 3 INSTRUMENT PANEL REGISTER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180307E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 6 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the No. 3 instrument panel register.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038YQ00EX_01_0013" proc-id="RM22W0E___0000BMV00001">
<ptxt>REMOVE NO. 4 INSTRUMENT PANEL REGISTER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181944E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 6 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the No. 4 instrument panel register.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038YQ00EX_01_0022" proc-id="RM22W0E___0000F1R00001">
<ptxt>REMOVE NO. 1 CENTER INSTRUMENT CLUSTER FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180653" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Detach the 10 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the No. 1 center instrument cluster finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038YQ00EX_01_0020" proc-id="RM22W0E___0000F1Q00001">
<ptxt>REMOVE CLOCK ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and clock.</ptxt>
<figure>
<graphic graphicname="B179754" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038YQ00EX_01_0016" proc-id="RM22W0E___0000BN600001">
<ptxt>REMOVE RADIO RECEIVER ASSEMBLY (w/o Multi-display)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and 2 bolts.</ptxt>
<figure>
<graphic graphicname="B181484" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Pull the radio receiver to detach the 10 claws on the backside of the radio receiver.</ptxt>
<figure>
<graphic graphicname="B183147" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the radio receiver.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038YQ00EX_01_0017" proc-id="RM22W0E___0000BN800001">
<ptxt>REMOVE NO. 2 RADIO BRACKET (w/o Multi-display)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and bracket.</ptxt>
<figure>
<graphic graphicname="B181486" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038YQ00EX_01_0018" proc-id="RM22W0E___0000BN700001">
<ptxt>REMOVE NO. 1 RADIO BRACKET (w/o Multi-display)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and bracket.</ptxt>
<figure>
<graphic graphicname="B181485" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038YQ00EX_01_0009" proc-id="RM22W0E___0000BNA00001">
<ptxt>REMOVE CLOCK ASSEMBLY (w/o Multi-display)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and clock.</ptxt>
<figure>
<graphic graphicname="B179753" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>