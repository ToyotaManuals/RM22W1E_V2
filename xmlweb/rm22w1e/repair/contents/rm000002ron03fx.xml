<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002RON03FX" category="C" type-id="300BM" name-id="ACIKL-02" from="201308">
<dtccode>B1446/46</dtccode>
<dtcname>Air Mix Damper Control Servo Motor Circuit (Driver Side)</dtcname>
<subpara id="RM000002RON03FX_01" type-id="60" category="03" proc-id="RM22W0E___0000H3600001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The front air mix damper servo motor (for driver side) sends pulse signals to inform the air conditioning amplifier assembly of the damper position. The air conditioning amplifier assembly activates the motor (normal or reverse) based on the signals to move the air mix damper (for driver side) to any position. As a result, the amount of air that has passed through the evaporator and is passing through the heater core is adjusted, and the temperature of the air blowing toward the driver side is controlled.</ptxt>
<atten4>
<ptxt>Confirm that no mechanical problem is present because this trouble code can be output when either a damper link or damper is mechanically locked.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.40in"/>
<colspec colname="COL2" colwidth="2.85in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1446/46</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The air mix damper position does not change even if the air conditioning amplifier assembly operates the air mix damper servo motor.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Air mix damper servo motor (for driver side)</ptxt>
</item>
<item>
<ptxt>Air conditioning harness assembly</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002RON03FX_02" type-id="32" category="03" proc-id="RM22W0E___0000H3700001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E161749E09" width="7.106578999in" height="2.775699831in"/>
</figure>
<figure>
<graphic graphicname="E161749E10" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002RON03FX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002RON03FX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002RON03FX_04_0001" proc-id="RM22W0E___0000H3800001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (AIR MIX DAMPER SERVO MOTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the servo motor is functioning properly. </ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Air Mix Servo Targ Pulse(D)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air mix damper servo motor (for driver side) target pulse /</ptxt>
<ptxt>Min.: 8, Max.: 90 (for LHD)</ptxt>
<ptxt>Min.: 8, Max.: 90 (for RHD)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>for LHD</ptxt>
<list1 type="unordered">
<item>
<ptxt>MAX COOL: 90 (pulse)</ptxt>
</item>
<item>
<ptxt>MAX HOT: 8 (pulse)</ptxt>
</item>
</list1>
<ptxt>for RHD</ptxt>
<list1 type="unordered">
<item>
<ptxt>MAX COOL: 8 (pulse)</ptxt>
</item>
<item>
<ptxt>MAX HOT: 90 (pulse)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (Checking from the DTC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (Checking from the PROBLEM SYMPTOMS TABLE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002RON03FX_04_0002" fin="false">A</down>
<right ref="RM000002RON03FX_04_0011" fin="true">B</right>
<right ref="RM000002RON03FX_04_0012" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002RON03FX_04_0002" proc-id="RM22W0E___0000H3900001">
<testtitle>CHECK AIR MIX DAMPER SERVO MOTOR (FOR DRIVER SIDE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air mix damper servo motor (for driver side).</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: See page <xref label="Seep01" href="RM000003AXS02WX"/>
</ptxt>
</item>
<item>
<ptxt>for RHD: See page <xref label="Seep02" href="RM000003AXS02VX"/>
</ptxt>
</item>
</list1>
<atten4>
<ptxt>Since the servo motor cannot be inspected while it is removed from the vehicle, replace the servo motor with a normal one.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep03" href="RM000002LIT037X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep04" href="RM000002LIT037X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1446/46 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1446/46 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002RON03FX_04_0013" fin="true">A</down>
<right ref="RM000002RON03FX_04_0007" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002RON03FX_04_0007" proc-id="RM22W0E___0000H3A00001">
<testtitle>CHECK AIR CONDITIONING HARNESS ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air conditioning harness assembly.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: See page <xref label="Seep01" href="RM000003AXS02WX"/>
</ptxt>
</item>
<item>
<ptxt>for RHD: See page <xref label="Seep02" href="RM000003AXS02VX"/>
</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep03" href="RM000002LIT037X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep04" href="RM000002LIT037X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1446/46 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1446/46 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002RON03FX_04_0014" fin="true">A</down>
<right ref="RM000002RON03FX_04_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002RON03FX_04_0011">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002LIQ02HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002RON03FX_04_0012">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002RON03FX_04_0013">
<testtitle>END (AIR MIX DAMPER SERVO MOTOR [FOR DRIVER SIDE] IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002RON03FX_04_0014">
<testtitle>END (AIR CONDITIONING HARNESS ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>