<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000PFZ0VMX" category="C" type-id="302IL" name-id="ESUB8-07" from="201308">
<dtccode>P2121</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" Circuit Range / Performance</dtcname>
<subpara id="RM000000PFZ0VMX_01" type-id="60" category="03" proc-id="RM22W0E___00001HZ00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Refer to DTC P2120 (See page <xref label="Seep01" href="RM000000PFY0UBX_01"/>).</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P2121</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Either of following conditions 1 or 2 met for 0.5 seconds (1 trip detection logic)</ptxt>
<list1 type="nonmark">
<item>
<ptxt>1. Difference between VPA and VPA2 is less than 0.4 V, or more than 1.2 V. (learned value of accelerator off position)</ptxt>
</item>
<item>
<ptxt>2. Difference between VPA and VPA2 is greater than or equal to the specified value.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal sensor assembly</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000PFZ0VMX_02" type-id="64" category="03" proc-id="RM22W0E___00001I000001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The accelerator pedal position sensor is mounted on the accelerator pedal bracket. The accelerator pedal position sensor has 2 sensor elements and 2 signal outputs: VPA and VPA2. VPA is used to detect the actual accelerator pedal angle (used for engine control) and VPA2 is used to detect malfunctions in VPA.</ptxt>
<ptxt>When the difference between the output voltages of VPA and VPA2 deviates from the standard, the ECM determines that the accelerator pedal position sensor is malfunctioning. The ECM turns on the MIL and stores the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFZ0VMX_10" type-id="73" category="03" proc-id="RM22W0E___00001I800001">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A199317E15" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on [A].</ptxt>
</item>
<item>
<ptxt>Wait for 5 seconds after turning the engine switch to ON.</ptxt>
</item>
<item>
<ptxt>Operate the accelerator pedal in accordance with the following procedure [B].</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Gas Throttle / Accel Sens. No.1 Volt %, and Accel Sens. No.2 Volt %.</ptxt>
</item>
<item>
<ptxt>Slowly depress the accelerator pedal until Accel Sens. No.1 Volt % is approximately 30% and Accel Sens. No.2 Volt % is approximately 46%, then slowly release the accelerator pedal.</ptxt>
</item>
</list2>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble codes [C].</ptxt>
</item>
<item>
<ptxt>Read the pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a pending DTC is output, the system is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If a pending DTC is not output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P2121.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows INCOMPLETE or N/A, perform steps [B] through [C] again.</ptxt>
</item>
</list1>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000PFZ0VMX_06" type-id="62" category="03" proc-id="RM22W0E___00001I100001">
<name>FAIL-SAFE</name>
<content5 releasenbr="1">
<ptxt>The accelerator pedal position sensor has two (main and sub) sensor circuits. If a malfunction occurs in either of the sensor circuits, the ECM detects the abnormal signal voltage difference between the two sensor circuits and switches to limp mode. In limp mode, the functioning circuit is used to calculate the accelerator pedal position to allow the vehicle to continue driving. If both circuits malfunction, the ECM regards the accelerator pedal as being fully released. In this case, the throttle valve remains closed as if the engine is idling.</ptxt>
<ptxt>If a pass condition is detected and then the engine switch is turned off, the fail-safe operation stops and the system returns to normal.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFZ0VMX_07" type-id="32" category="03" proc-id="RM22W0E___00001I200001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2120 (See page <xref label="Seep01" href="RM000000PFY0UBX_08"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFZ0VMX_08" type-id="51" category="05" proc-id="RM22W0E___00001I300001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>This DTC relates to the accelerator pedal position sensor.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFZ0VMX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PFZ0VMX_09_0009" proc-id="RM22W0E___00001I700001">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P2121)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble codes.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2121 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2121 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P2121 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PFZ0VMX_09_0001" fin="false">A</down>
<right ref="RM000000PFZ0VMX_09_0010" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PFZ0VMX_09_0001" proc-id="RM22W0E___00001I400001">
<testtitle>READ VALUE USING GTS (ACCELERATOR PEDAL POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A208621E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Accel Sensor Out No. 1 and Accel Sensor Out No. 2.</ptxt>
</test1>
<test1>
<ptxt>Read the values displayed on the GTS.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.04in"/>
<colspec colname="COL2" colwidth="1.04in"/>
<colspec colname="COL3" colwidth="1.03in"/>
<colspec colname="COLSPEC0" colwidth="1.02in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Accelerator Pedal Operation</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sensor Out No. 1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sensor Out No. 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Difference between Accel Sensor Out No. 1 and Accel Sensor Out No. 2</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Released</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.5 to 1.1 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.2 to 2.0 V</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>0.4 to 1.2 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.6 to 4.5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.4 to 4.75 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry>
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Depressed</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Released</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PFZ0VMX_09_0011" fin="true">OK</down>
<right ref="RM000000PFZ0VMX_09_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFZ0VMX_09_0008" proc-id="RM22W0E___00001I600001">
<testtitle>REPLACE ACCELERATOR PEDAL POSITION SENSOR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the accelerator pedal position sensor assembly (See page <xref label="Seep01" href="RM0000028B2015X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFZ0VMX_09_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PFZ0VMX_09_0004" proc-id="RM22W0E___00001I500001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P2121)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK187X"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle in accordance with the driving pattern described in Confirmation Driving Pattern.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P2121 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PFZ0VMX_09_0007" fin="true">A</down>
<right ref="RM000000PFZ0VMX_09_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PFZ0VMX_09_0010">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000032SF055X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFZ0VMX_09_0011">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ13TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFZ0VMX_09_0006">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000PFZ0VMX_09_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>