<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3TB_T00ME" variety="T00ME">
<name>ENGINE IMMOBILISER SYSTEM (w/o Entry and Start System)</name>
<para id="RM000000W1208MX" category="J" type-id="303RQ" name-id="TD62S-02" from="201301" to="201308">
<dtccode/>
<dtcname>Security Indicator Light Does not Blink</dtcname>
<subpara id="RM000000W1208MX_01" type-id="60" category="03" proc-id="RM22W0E___0000EWB00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>When the engine immobiliser system is set, the security indicator light blinks continuously, but does not illuminate if the engine immobiliser system is not set.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000W1208MX_02" type-id="32" category="03" proc-id="RM22W0E___0000EWC00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B292706E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W1208MX_03" type-id="51" category="05" proc-id="RM22W0E___0000EWD00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>When replacing the transponder key ECU assembly, refer to Registration.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000W1208MX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W1208MX_04_0001" proc-id="RM22W0E___0000EWE00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000001TCQ02BX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1208MX_04_0026" fin="false">OK</down>
<right ref="RM000000W1208MX_04_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208MX_04_0026" proc-id="RM22W0E___0000EWJ00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (SECURITY INDICATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Immobiliser / Active Test.</ptxt>
</test1>
<test1>
<ptxt>According to the display on the intelligent tester, perform the Active Test.</ptxt>
<table pgwide="1">
<title>Immobiliser</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Security Indicator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Security indicator light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Security indicator light can be turned on and off using the intelligent tester.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1208MX_04_0002" fin="false">OK</down>
<right ref="RM000000W1208MX_04_0017" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208MX_04_0002" proc-id="RM22W0E___0000EWF00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (ENGINE IMMOBILISER SYSTEM STATUS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<atten4>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot: Connect the intelligent tester to the DLC3 and turn a courtesy light switch on and off at intervals of 1.5 seconds until communication between the intelligent tester and vehicle begins.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000001TCS026X"/>).</ptxt>
<table pgwide="1">
<title>Immobiliser</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Immobiliser</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine immobiliser system status determined by transponder key ECU assembly / Set or Unset</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Set: Engine immobiliser set (engine start prohibited [no key in ignition key cylinder])</ptxt>
<ptxt>Unset: Engine immobiliser unset (engine start permitted [key inserted in ignition key cylinder])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>When the engine immobiliser system does not change to the unset state, this item can be used to determine if the cause is the transponder key ECU assembly.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W1208MX_04_0022" fin="true">OK</down>
<right ref="RM000000W1208MX_04_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208MX_04_0017" proc-id="RM22W0E___0000EWI00000">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSPONDER KEY ECU - SECURITY INDICATOR LIGHT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E96 transponder key ECU assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the g1 clock assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E96-8 (IND) - g1-9 (LP)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>g1-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>g1-9 (LP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1208MX_04_0008" fin="false">OK</down>
<right ref="RM000000W1208MX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208MX_04_0008" proc-id="RM22W0E___0000EWG00000">
<testtitle>REPLACE CLOCK ASSEMBLY (SECURITY INDICATOR LIGHT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the clock assembly (security indicator light) with a new or normally functioning one (See page <xref label="Seep01" href="RM0000038YQ00CX"/>).</ptxt>
</test1>
<test1>
<ptxt>When the immobiliser is set or the theft deterrent system is in the arming preparation state, check that the security indicator light blinks.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Security indicator light blinks.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1208MX_04_0025" fin="true">OK</down>
<right ref="RM000000W1208MX_04_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208MX_04_0013" proc-id="RM22W0E___0000EWH00000">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSPONDER KEY ECU - BATTERY AND BODY&#13;
GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E96 transponder key ECU assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E96-16 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E96-1 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E96-2 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1208MX_04_0022" fin="true">OK</down>
<right ref="RM000000W1208MX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1208MX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W1208MX_04_0021">
<testtitle>GO TO DIAGNOSTIC TROUBLE CODE CHART<xref label="Seep01" href="RM000001TCA02PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W1208MX_04_0022">
<testtitle>REPLACE TRANSPONDER KEY ECU ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000000W1208MX_04_0025">
<testtitle>END (CLOCK ASSEMBLY WAS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>