<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UL_T00NO" variety="T00NO">
<name>FRONT SEAT ASSEMBLY (for Power Seat)</name>
<para id="RM00000390T01YX" category="A" type-id="80002" name-id="SE9E3-02" from="201308">
<name>DISASSEMBLY</name>
<subpara id="RM00000390T01YX_02" type-id="11" category="10" proc-id="RM22W0E___0000FUJ00001">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000390T01YX_01" type-id="01" category="01">
<s-1 id="RM00000390T01YX_01_0001" proc-id="RM22W0E___0000FMH00001">
<ptxt>REMOVE SLIDE AND VERTICAL POWER SEAT SWITCH KNOB</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180697E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Apply protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the slide and vertical power seat switch knob.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0002" proc-id="RM22W0E___0000FMI00001">
<ptxt>REMOVE RECLINING POWER SEAT SWITCH KNOB</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180698E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the reclining power seat switch knob.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0003" proc-id="RM22W0E___0000FMJ00001">
<ptxt>REMOVE FRONT SEAT CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180699" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 rubber bands.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B180700" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the front seat cushion shield LH together with the front inner No. 1 seat cushion shield LH.</ptxt>
</s2>
<s2>
<ptxt>for Driver Side:</ptxt>
<ptxt>Disconnect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0004" proc-id="RM22W0E___0000FTT00001">
<ptxt>REMOVE FRONT INNER NO. 1 SEAT CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180701" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the front inner No. 1 seat cushion shield LH from the front seat cushion shield LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0005" proc-id="RM22W0E___0000FTU00001">
<ptxt>REMOVE LUMBAR SWITCH (for Driver Side)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180702" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws and lumbar switch from the front seat cushion shield LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0030" proc-id="RM22W0E___0000FMF00001">
<ptxt>REMOVE FRONT SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connectors and detach the clamps.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B185874" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the nut and front seat inner belt assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01YX_01_0007" proc-id="RM22W0E___0000FMK00001">
<ptxt>REMOVE FRONT INNER SEAT CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180703" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and clip, and then remove the front inner seat cushion shield LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0008" proc-id="RM22W0E___0000FTV00001">
<ptxt>REMOVE FRONT POWER SEAT SWITCH LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Seat Position Memory System:</ptxt>
<ptxt>Disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>w/o Seat Position Memory System:</ptxt>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 screws and front power seat switch LH.</ptxt>
<figure>
<graphic graphicname="B180704" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0009" proc-id="RM22W0E___0000FTW00001">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, remove the 2 clips.</ptxt>
<figure>
<graphic graphicname="B180705" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Climate Control Seat System:</ptxt>
<s3>
<ptxt>Disconnect the connector and detach the 2 wire harness clamps.</ptxt>
<figure>
<graphic graphicname="B180706" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the connector and detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="B180707" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Climate Control Seat System:</ptxt>
<s3>
<ptxt>Disconnect the 2 connectors and detach the 4 wire harness clamps.</ptxt>
<figure>
<graphic graphicname="B299194" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="B300511" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>for Front Passenger Side:</ptxt>
<s3>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B309117" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Detach the 2 clamps and remove the connector holder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Bend the 2 seat frame claws upward.</ptxt>
<figure>
<graphic graphicname="B180708" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the hooks and remove the seat cushion cover with pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0010" proc-id="RM22W0E___0000FTX00001">
<ptxt>REMOVE FRONT SEPARATE TYPE SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the hog rings and front separate type seat cushion cover from the front separate type seat cushion pad.</ptxt>
<figure>
<graphic graphicname="B180709" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0011" proc-id="RM22W0E___0000FTY00001">
<ptxt>REMOVE FRONT SEAT CUSHION HEATER ASSEMBLY LH (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Climate Control Seat System:</ptxt>
<ptxt>Cut off the tack pins and remove the front seat cushion heater assembly LH from the front separate type seat cushion cover.</ptxt>
<figure>
<graphic graphicname="B180710" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Climate Control Seat System:</ptxt>
<ptxt>Cut off the tack pins and remove the front seat cushion heater assembly LH from the front separate type seat cushion cover.</ptxt>
<figure>
<graphic graphicname="B298406" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0012" proc-id="RM22W0E___0000FML00001">
<ptxt>REMOVE FRONT SEATBACK BOARD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180711" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Move the front seatback board sub-assembly LH in the direction of the arrow to detach the 2 hooks and remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0035" proc-id="RM22W0E___0000FUC00001">
<ptxt>REMOVE SEAT CLIMATE CONTROL CONTROLLER LH (w/ Climate Control Seat System, for Seatback)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B299415" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws and disconnect the seat climate control blower LH.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 hooks and remove the seat climate control controller LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0013" proc-id="RM22W0E___0000FMM00001">
<ptxt>REMOVE FRONT SEAT HEADREST SUPPORT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180712" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the 2 front seat headrest supports.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0014" proc-id="RM22W0E___0000FM200001">
<ptxt>REMOVE SEATBACK COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the hog rings.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B180713E02" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut and detach the seatback cover bracket.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Cover Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B180716E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the hooks and remove the seatback cover with pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0015" proc-id="RM22W0E___0000FTZ00001">
<ptxt>REMOVE FRONT SEPARATE TYPE SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Pass the seatback cover bracket through the hole of the front separate type seatback pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Cover Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B180717E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the hog rings.</ptxt>
<figure>
<graphic graphicname="B180718E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the fastening tape and remove the front separate type seatback cover from the front separate type seatback pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0044" proc-id="RM22W0E___0000FUI00001">
<ptxt>REMOVE FRONT SEPARATE TYPE SEATBACK COVER (w/ Climate Control Seat System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Pass the seatback cover bracket through the hole of the front separate type seatback pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Cover Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B180717E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the hog rings and front separate type seatback cover from the front separate type seatback pad.</ptxt>
<figure>
<graphic graphicname="B297983" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0016" proc-id="RM22W0E___0000FU000001">
<ptxt>REMOVE FRONT SEATBACK HEATER ASSEMBLY LH (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Climate Control Seat System:</ptxt>
<ptxt>Cut off the tack pins and remove the front seatback heater assembly LH from the front separate type seatback cover.</ptxt>
<figure>
<graphic graphicname="B180719" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Climate Control Seat System:</ptxt>
<ptxt>Cut off the tack pins and remove the front seatback heater assembly LH from the front separate type seatback cover.</ptxt>
<figure>
<graphic graphicname="B298407" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0042" proc-id="RM22W0E___0000FMO00001">
<ptxt>REMOVE FRONT SEATBACK PROTECTOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B299469E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the front seatback protector.</ptxt>
</s2>
<s2>
<ptxt>Remove any remaining double-sided tape.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double-sided Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0019" proc-id="RM22W0E___0000FU300001">
<ptxt>REMOVE OUTSIDE RECLINING ADJUSTER COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180720" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the outside reclining adjuster cover LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0020" proc-id="RM22W0E___0000FU400001">
<ptxt>REMOVE OUTSIDE RECLINING ADJUSTER COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the outside reclining adjuster cover RH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0017" proc-id="RM22W0E___0000FU100001">
<ptxt>REMOVE INSIDE RECLINING ADJUSTER COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180721" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the inside reclining adjuster cover LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0018" proc-id="RM22W0E___0000FU200001">
<ptxt>REMOVE INSIDE RECLINING ADJUSTER COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the inside reclining adjuster cover RH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0021" proc-id="RM22W0E___0000FU500001">
<ptxt>REMOVE LUMBAR SUPPORT ADJUSTER ASSEMBLY LH (for Driver Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the cable clamp and disconnect the active headrest cable from the seatback spring.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cable Clamp</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B299472E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 springs.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 hooks and remove the seatback spring.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B299473" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 screws and lumbar support adjuster assembly LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0037" proc-id="RM22W0E___0000FUD00001">
<ptxt>REMOVE BUSH (for Driver Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 claws and remove the bush.</ptxt>
<figure>
<graphic graphicname="B299474" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0022" proc-id="RM22W0E___0000FU600001">
<ptxt>REMOVE LOWER ACTIVE HEADREST UNIT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180727" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the nut and detach clamp, and then disconnect the active headrest cable from the upper active headrest unit.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 nuts and lower active headrest unit.</ptxt>
<figure>
<graphic graphicname="B299486" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0023" proc-id="RM22W0E___0000FU700001">
<ptxt>REMOVE UPPER ACTIVE HEADREST UNIT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B298293" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 nuts and upper active headrest unit.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0024" proc-id="RM22W0E___0000FU800001">
<ptxt>REMOVE FRONT LOWER SEAT CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180722" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the hook and remove the lower seat cushion shield LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0025" proc-id="RM22W0E___0000FU900001">
<ptxt>REMOVE FRONT LOWER SEAT CUSHION SHIELD RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the hook and remove the lower seat cushion shield RH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0026" proc-id="RM22W0E___0000FUA00001">
<ptxt>REMOVE SEAT HEATER CONTROL SUB-ASSEMBLY (w/ Seat Heater System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180728" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the connector and detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the seat heater control sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0038" proc-id="RM22W0E___0000FUE00001">
<ptxt>REMOVE SEAT CLIMATE CONTROL ECU (w/ Climate Control Seat System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B299479" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the seat climate control ECU.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0039" proc-id="RM22W0E___0000FMP00001">
<ptxt>REMOVE SEAT CLIMATE CONTROL CONTROLLER LH (w/ Climate Control Seat System, for Seat Cushion)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B299480" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and disconnect the seat climate control blower LH.</ptxt>
</s2>
<s2>
<ptxt>Detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="B299481" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 screws and seat climate control controller LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0040" proc-id="RM22W0E___0000FUF00001">
<ptxt>REMOVE SEAT CLIMATE CONTROL BLOWER LH (w/ Climate Control Seat System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B299483" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the connector and remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="B299484" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut and seat climate control blower LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0043" proc-id="RM22W0E___0000FUH00001">
<ptxt>REMOVE NO. 1 AIR FILTER (w/ Climate Control Seat System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B191967" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Rotate the No. 1 air filter counterclockwise to detach the claws and remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0041" proc-id="RM22W0E___0000FUG00001">
<ptxt>REMOVE SEAT FRAME SET BRACKET SUB-ASSEMBLY (w/ Climate Control Seat System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B299485" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 hooks and remove the seat frame set bracket sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01YX_01_0032" proc-id="RM22W0E___0000FLL00001">
<ptxt>REMOVE SEAT POSITION SENSOR (for Driver Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T30 "TORX" socket wrench, remove the screw and seat position sensor with sensor protector.</ptxt>
<figure>
<graphic graphicname="B181656" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the seat slide position sensor protector from the seat position sensor. </ptxt>
<figure>
<graphic graphicname="C108775E04" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01YX_01_0033" proc-id="RM22W0E___0000FMN00001">
<ptxt>REMOVE FRONT SEAT AIRBAG ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 5 clamps and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="B102196" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 clamps and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="B102199" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts and front seat airbag assembly LH.</ptxt>
<atten2>
<ptxt>Make sure that the seat frame assembly is not deformed. If it is, replace it with a new one.</ptxt>
</atten2>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01YX_01_0029" proc-id="RM22W0E___0000FUB00001">
<ptxt>REMOVE FRONT SEAT WIRE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connectors and detach the wire harness clamps.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the front seat wire LH.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>