<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000000XIM0U4X" category="C" type-id="803LR" name-id="BCE6S-03" from="201308">
<dtccode>C1433</dtccode>
<dtcname>Steering Angle Sensor Internal Circuit</dtcname>
<subpara id="RM000000XIM0U4X_01" type-id="60" category="03" proc-id="RM22W0E___0000AG400001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Steering angle sensor signals are sent to the skid control ECU (master cylinder solenoid) via the CAN communication system. When there is a malfunction in the CAN communication system, it is detected by the steering angle sensor zero point malfunction diagnostic function.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1433</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Either condition is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>With the IG1 terminal voltage 9.5 to 17.4 V, the skid control ECU receives an error signal from the steering sensor for 0.012 seconds.</ptxt>
</item>
<item>
<ptxt>An open in the sensor power supply circuit for 1 second or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<ptxt>Steering sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XIM0U4X_02" type-id="32" category="03" proc-id="RM22W0E___0000AG500001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC C1432 (See page <xref label="Seep01" href="RM000000XIM0U3X_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000XIM0U4X_03" type-id="51" category="05" proc-id="RM22W0E___0000AG600001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="nonmark">
<item>
<ptxt>When any of the speed sensors or the yaw rate sensor assembly has trouble, DTCs for the steering sensor may be stored even when the steering sensor is normal. When DTCs for the speed sensors or yaw rate sensor assembly are output together with DTCs for the steering sensor, inspect and repair the speed sensor and yaw rate sensor assembly first, and then inspect and repair the steering sensor.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XIM0U4X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XIM0U4X_04_0014" proc-id="RM22W0E___0000AG800001">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>CAN DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIM0U4X_04_0008" fin="false">A</down>
<right ref="RM000000XIM0U4X_04_0015" fin="true">B</right>
<right ref="RM000000XIM0U4X_04_0016" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000XIM0U4X_04_0008" proc-id="RM22W0E___0000AG700001">
<testtitle>REPLACE STEERING SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the steering sensor (See page <xref label="Seep01" href="RM000000SS908KX"/>).</ptxt>
<atten3>
<ptxt>When replacing the steering sensor, confirm that the replacement part is of the correct specification.</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000000XIM0U4X_04_0013" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XIM0U4X_04_0013">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000XIM0U4X_04_0015">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIM0U4X_04_0016">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>