<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000PGD0AWX" category="C" type-id="305GL" name-id="ES10SW-002" from="201301" to="201308">
<dtccode>P0630</dtccode>
<dtcname>VIN not Programmed or Mismatch - ECM / PCM</dtcname>
<subpara id="RM000000PGD0AWX_01" type-id="64" category="03" proc-id="RM22W0E___00001PR00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>DTC P0630 is stored when the Vehicle Identification Number (VIN) is not stored in the Engine Control Module (ECM) or the input VIN is not accurate. Input the VIN with the GTS.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0630</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>When either of the following conditions is met (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>The VIN is not stored in the ECM.</ptxt>
</item>
<item>
<ptxt>The VIN input into the ECM is not accurate.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000PGD0AWX_08" type-id="73" category="03" proc-id="RM22W0E___00001PV00000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC procedure).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Wait 5 seconds or more.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</item>
<item>
<ptxt>Read pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a pending DTC is output, the system is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If a pending DTC is not output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P0630.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
</list1>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000PGD0AWX_06" type-id="51" category="05" proc-id="RM22W0E___00001PS00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the GTS. The ECM records vehicle and driving condition information as freeze frame data the moment a DTC is stored. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PGD0AWX_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PGD0AWX_07_0001" proc-id="RM22W0E___00001PT00000">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0630)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC P0630 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC P0630 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If any DTCs other than P0630 are output, troubleshoot those DTCs first.</ptxt>
<atten3>
<ptxt>If P0630 is stored, the VIN must be input to the ECM using the GTS. However, all DTCs are cleared automatically by the GTS when the VIN is input. If DTCs other than P0630 are output, troubleshoot them first.</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000000PGD0AWX_07_0002" fin="false">A</down>
<right ref="RM000000PGD0AWX_07_0003" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PGD0AWX_07_0002" proc-id="RM22W0E___00001PU00000">
<testtitle>INPUT VIN</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Input the VIN (See page <xref label="Seep01" href="RM000000PDT0C5X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PGD0AWX_07_0004" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PGD0AWX_07_0003">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000032SF04AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PGD0AWX_07_0004">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>