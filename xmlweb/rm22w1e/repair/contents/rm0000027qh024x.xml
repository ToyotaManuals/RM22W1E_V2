<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3X2_T00Q5" variety="T00Q5">
<name>POWER WINDOW CONTROL SYSTEM (for Models with Jam Protection Function on 4 Windows)</name>
<para id="RM0000027QH024X" category="D" type-id="303F2" name-id="WS0YH-29" from="201301">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM0000027QH024X_z0" proc-id="RM22W0E___0000HWT00000">
<content5 releasenbr="1">
<step1>
<ptxt>POWER WINDOW CONTROL SYSTEM DESCRIPTION</ptxt>
<list1 type="unordered">
<item>
<ptxt>The power window control system controls the power window up/down function using the power window regulator motor. The main controls of this system are the multiplex network master switch which is built into the driver side door, and the power window regulator switches which are built into the front passenger side door and rear doors.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Pressing either of the power window switches on the power window regulator switches or either of the power window switches on the multiplex network master switch transmits an up/down signal to the corresponding power window regulator motor.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>An ECU is built into the power window regulator motor.</ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>FUNCTION OF MAIN COMPONENT</ptxt>
<ptxt>The power window control system consists of the following components:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="4.51in"/>
<thead>
<row>
<entry align="center">
<ptxt>Component</ptxt>
</entry>
<entry align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Multiplex network master switch</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Controls power window operation for all windows.</ptxt>
</item>
<item>
<ptxt>When window lock switch is on position, window operation will only be possible with multiplex network master switch.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Power window regulator switch</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Located on front passenger side door and rear doors.</ptxt>
</item>
<item>
<ptxt>Each power window switch controls power window operations for its respective window.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Power window regulator motor</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Receives power window switch signals and activates motor to change window position.</ptxt>
</item>
<item>
<ptxt>Uses Hall-IC in regulator motor with ECU to detect window position and jamming.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>SYSTEM FUNCTION</ptxt>
<ptxt>The power window control system has the following functions:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="4.51in"/>
<thead>
<row>
<entry align="center">
<ptxt>Function</ptxt>
</entry>
<entry align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Manual up/down function</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function that raises a window while a power window switch is pulled halfway up, and lowers the window while pushed halfway down (the window stops as soon as the power window switch is released).</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Auto up/down function</ptxt>
<ptxt>(All auto)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function that enables a window to be fully opened or closed by one full push or pull of the power window switch.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Remote up/down function</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function that allows the multiplex network master switch to control the front passenger side power window and rear power windows.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Key-off operation function</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Function that makes it possible to operate all power windows for approximately 45 sec. after the ignition switch is turned off when the driver side door or front passenger side door has not been opened.</ptxt>
</item>
<item>
<ptxt>If 45 sec. have passed or a front door is opened while the auto function is operating, the window operation stops.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Window lock function</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Function where the front passenger side power window regulator switch and rear power window regulator switch operations are disabled when the window lock switch is on.</ptxt>
</item>
<item>
<ptxt>The front passenger side power window regulator switch and rear power window regulator switches can be operated when the window lock switch is off.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Jam protection function</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Function that automatically stops a power window and moves it downward if an object is jammed in the power window.</ptxt>
</item>
<item>
<ptxt>The jam protection function works for the auto up and manual up operations.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Key-linked open and close</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>When the key is not inside the vehicle, the driver side door is locked, and the key inserted into the driver side door key cylinder is turned and maintained in the lock position for 2.3 sec. or more, the main body ECU activates the power window regulator motors to close all power windows while the key is turned..</ptxt>
</item>
<item>
<ptxt>Similarly, when the driver side door is unlocked, turning and maintaining the driver side door key cylinder in the unlock position for 2.3 seconds or more will cause all power windows to open.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless transmitter-linked open</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the certification ECU receives an unlock signal from the door control transmitter for 3 seconds or more, the main body ECU sends an operation permission signal to the ECU in the power window regulator motor. The ECU that receives the signal controls the power window regulator motor to open all the power windows.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Entry lock switch-linked close</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the door ECU receives a lock signal from the lock switch on the door outside handle for 3 seconds or more, the main body ECU sends an operation permission signal to the ECU in the power window regulator motor. The ECU that receives the signal controls the power window regulator motor to close all the power windows.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Diagnosis</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Function where the ECU in the power window regulator motor detects malfunctions in the power window system and makes a diagnosis.</ptxt>
</item>
<item>
<ptxt>The power window switch light blinks to inform the driver of the malfunction.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fail-safe</ptxt>
</entry>
<entry valign="middle">
<ptxt>If the pulse sensor(s) (Hall IC(s)) in the power window regulator motor malfunctions, the power window auto up/down function is disabled.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>