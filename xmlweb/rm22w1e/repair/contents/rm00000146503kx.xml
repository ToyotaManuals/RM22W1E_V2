<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000U" variety="S000U">
<name>1GR-FE COOLING</name>
<ttl id="12011_S000U_7C3JT_T00CW" variety="T00CW">
<name>RADIATOR</name>
<para id="RM00000146503KX" category="A" type-id="30014" name-id="CO6C6-02" from="201308">
<name>INSTALLATION</name>
<subpara id="RM00000146503KX_01" type-id="01" category="01">
<s-1 id="RM00000146503KX_01_0001" proc-id="RM22W0E___000047O00001">
<ptxt>INSTALL RADIATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the radiator bracket hooks into the radiator support holes.</ptxt>
<figure>
<graphic graphicname="A174324" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the radiator with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146503KX_01_0028" proc-id="RM22W0E___000047P00001">
<ptxt>CONNECT OUTLET OIL COOLER HOSE (for Automatic Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the cooler hose.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146503KX_01_0029" proc-id="RM22W0E___000047Q00001">
<ptxt>CONNECT INLET OIL COOLER HOSE (for Automatic Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the cooler hose.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146503KX_01_0004" proc-id="RM22W0E___000047R00001">
<ptxt>INSTALL FAN SHROUD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fan pulley to the water pump.</ptxt>
</s2>
<s2>
<ptxt>Place the shroud together with the coupling fan between the radiator and engine.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Temporarily install the fluid coupling fan to the water pump with the 4 nuts. Tighten the nuts as much as possible by hand.</ptxt>
<atten3>
<ptxt>Match the paint marks on the heads of the water pump studs with the paint marks on the flange of the fluid coupling and install the fluid coupling.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Attach the claws of the shroud to the radiator as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A273662" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the shroud with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the reservoir hose to the upper side of the radiator tank.</ptxt>
</s2>
<s2>
<ptxt>Install the fan and generator V-belt (See page <xref label="Seep01" href="RM0000017L901GX"/>).</ptxt>
</s2>
<s2>
<ptxt>Tighten the 4 nuts of the fluid coupling fan.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146503KX_01_0030" proc-id="RM22W0E___000047S00001">
<ptxt>CONNECT OIL COOLER TUBE (w/ Air Cooled Transmission Oil Cooler)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the oil cooler tube with the 2 bolts, and attach the claw to close the flexible hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146503KX_01_0045" proc-id="RM22W0E___000048F00001">
<ptxt>INSTALL NO. 2 RADIATOR HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 radiator hose and attach the clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146503KX_01_0044">
<ptxt>INSTALL NO. 1 RADIATOR HOSE</ptxt>
</s-1>
<s-1 id="RM00000146503KX_01_0013" proc-id="RM22W0E___000047T00001">
<ptxt>INSTALL RADIATOR SIDE DEFLECTOR LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the deflector with the 4 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146503KX_01_0012" proc-id="RM22W0E___000047U00001">
<ptxt>INSTALL RADIATOR SIDE DEFLECTOR RH (w/o Air Cooled Transmission Oil Cooler)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the deflector with the 4 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146503KX_01_0031" proc-id="RM22W0E___000047V00001">
<ptxt>INSTALL TRANSMISSION OIL COOLER AIR DUCT (w/ Air Cooled Transmission Oil Cooler)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the oil cooler air duct with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>4.9</t-value1>
<t-value2>50</t-value2>
<t-value3>43</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146503KX_01_0015" proc-id="RM22W0E___000011N00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add engine coolant.</ptxt>
<spec>
<title>Standard Capacity (for Manual transmission)</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>14.6 liters (15.4 US qts, 12.8 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>11.7 liters (12.4 US qts, 10.3 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard capacity (for Automatic transmission)</title>
<specitem>
<ptxt>11.2 liters (11.8 US qts, 9.9 Imp. qts)</ptxt>
</specitem>
</spec>
<spec>
<title>Standard Capacity (for China)</title>
<specitem>
<ptxt>14.4 liters (15.2 US qts, 12.7 Imp. qts)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Slowly pour coolant into the radiator reservoir until it reaches the F line.</ptxt>
</s2>
<s2>
<ptxt>Install the reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand, and then check the coolant level. If the coolant level is low, add coolant.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Set the air conditioning as follows while warming up the engine.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Fan speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Any setting except off</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Toward WARM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Air conditioning switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Start the engine and warm it up until the thermostat opens.</ptxt>
<atten4>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand, and checking when the engine coolant starts to flow inside the hose.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Maintain the engine speed at 2000 to 2500 rpm.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the radiator reservoir still has some coolant in it.</ptxt>
</item>
<item>
<ptxt>Pay attention to the needle of the water temperature meter. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
<item>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the F line.</ptxt>
</item>
<item>
<ptxt>Run the engine at 2000 rpm until the coolant level has stabilized.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand to bleed air.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves. Hot areas on the parts may injure your hands.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the fan.</ptxt>
</item>
</list1>
</atten2>
</s2>
<s2>
<ptxt>Stop the engine, and wait until the engine coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Check that the coolant level is between the F and L lines.</ptxt>
<ptxt>If the coolant level is below the L line, repeat all of the procedures above.</ptxt>
<ptxt>If the coolant level is above the F line, drain coolant so that the coolant level is between the F and L lines.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146503KX_01_0023" proc-id="RM22W0E___000047Z00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>To avoid being burned, do not remove the radiator reservoir cap while the engine and radiator are still hot. Thermal expansion may cause hot engine coolant and steam to blow out from the radiator.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with engine coolant, and then attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and then check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and engine water pump for leakage. If there are no signs or traces of external engine coolant leakage, check the heater core, cylinder block and head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146503KX_01_0043" proc-id="RM22W0E___00000Z200001">
<ptxt>INSTALL V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 2 V-bank cover grommets with the 2 pins and press down on the V-bank cover to attach the pins.</ptxt>
<figure>
<graphic graphicname="A271365E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000146503KX_01_0025" proc-id="RM22W0E___000011P00000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover with the 10 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146503KX_01_0038" proc-id="RM22W0E___00006UM00001">
<ptxt>INSTALL FRONT BUMPER COVER (for Standard)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000038JV01UX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM00000146503KX_01_0039" proc-id="RM22W0E___00006UN00001">
<ptxt>INSTALL FRONT BUMPER COVER (w/ Winch)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000038JV01VX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>