<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM00000187U08XX" category="C" type-id="3031S" name-id="ESTDW-08" from="201301" to="201308">
<dtccode>P0107</dtccode>
<dtcname>Manifold Absolute Pressure / Barometric Pressure Circuit Low Input</dtcname>
<dtccode>P0108</dtccode>
<dtcname>Manifold Absolute Pressure / Barometric Pressure Circuit High Input</dtcname>
<subpara id="RM00000187U08XX_01" type-id="60" category="03" proc-id="RM22W0E___00003G100000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A208461E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>The manifold absolute pressure sensor detects the intake manifold pressure. The ECM determines the basic injection volume and injection advance timing based on the voltage output by the manifold absolute pressure sensor.</ptxt>
<ptxt>The manifold absolute pressure sensor monitors the absolute pressure inside the intake manifold (default is 0 kPa [0 mmHg, 0 in.Hg]). As a result, the ECM controls the air-fuel ratio so that it reaches the proper level under any driving conditions.</ptxt>
<table pgwide="1">
<title>P0107</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2 seconds after engine is started, race engine for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>After the engine is started, condition (a) continues for more than 0.5 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) Manifold absolute pressure sensor voltage is 0.1 V or less.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in manifold absolute pressure sensor circuit</ptxt>
</item>
<item>
<ptxt>Manifold absolute pressure sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0108</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2 seconds after engine is started, race engine for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>After the engine is started, condition (a) continues for more than 0.5 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) Manifold absolute pressure sensor voltage is 4.8 V or higher.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in manifold absolute pressure sensor circuit</ptxt>
</item>
<item>
<ptxt>Manifold absolute pressure sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0107</ptxt>
<ptxt>P0108</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>MAP</ptxt>
</item>
<item>
<ptxt>Atmosphere Pressure</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTC P0107 and/or P0108 is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Misfire </ptxt>
</item>
<item>
<ptxt>Combustion noise</ptxt>
</item>
<item>
<ptxt>Black smoke</ptxt>
</item>
<item>
<ptxt>White smoke</ptxt>
</item>
<item>
<ptxt>Lack of power</ptxt>
</item>
</list2>
<item>
<ptxt>When DTC P0107 or P0108 is stored, check the intake manifold pressure by entering the following menus: Engine and ECT / Data List / MAP.</ptxt>
<table pgwide="1">
<title>Reference</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Intake Manifold Pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>Malfunction</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Approximately 0 kPa</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Open in VC circuit</ptxt>
</item>
<item>
<ptxt>Short in PIM circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>250 kPa (1875 mmHg, 73.8 in.Hg) or higher</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Open in PIM circuit</ptxt>
</item>
<item>
<ptxt>Open in E circuit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187U08XX_02" type-id="32" category="03" proc-id="RM22W0E___00003G200000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A165731E40" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000187U08XX_03" type-id="51" category="05" proc-id="RM22W0E___00003G300000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187U08XX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187U08XX_04_0001" proc-id="RM22W0E___00003G400000">
<testtitle>READ VALUE USING GTS (MANIFOLD ABSOLUTE PRESSURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / MAP.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Same as the actual atmospheric pressure.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Standard atmospheric pressure is 101 kPa. For every 100 m increase in elevation, pressure drops by 1 kPa. This varies by weather (high atmospheric pressure, low atmospheric pressure).</ptxt>
</item>
<item>
<ptxt>Also, check "Atmosphere Pressure" in the Data List.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<right ref="RM00000187U08XX_04_0032" fin="false">OK</right>
<right ref="RM00000187U08XX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187U08XX_04_0002" proc-id="RM22W0E___00003G500000">
<testtitle>CHECK HARNESS AND CONNECTOR (MANIFOLD ABSOLUTE PRESSURE SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the manifold absolute pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C80-2 (PIM) - C45-67 (PIM)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C80-3 (VC) - C45-66 (VCPM)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C80-1 (E) - C45-92 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C80-2 (PIM) or C45-67 (PIM) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C80-3 (VC) or C45-66 (VCPM) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C80-2 (PIM) - C46-67 (PIM)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C80-3 (VC) - C46-66 (VCPM)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C80-1 (E) - C46-92 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C80-2 (PIM) or C46-67 (PIM) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C80-3 (VC) or C46-66 (VCPM) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187U08XX_04_0004" fin="false">OK</down>
<right ref="RM00000187U08XX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187U08XX_04_0004" proc-id="RM22W0E___00003G600000">
<testtitle>CHECK ECM TERMINAL VOLTAGE (VC TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the manifold absolute pressure sensor connector.</ptxt>
<figure>
<graphic graphicname="A204467E31" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C80-3 (VC) - C80-1 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.75 to 5.25 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Absolute Pressure Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000187U08XX_04_0028" fin="false">OK</down>
<right ref="RM00000187U08XX_04_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187U08XX_04_0028" proc-id="RM22W0E___00003G900000">
<testtitle>REPLACE MANIFOLD ABSOLUTE PRESSURE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the manifold absolute pressure sensor (See page <xref label="Seep01" href="RM0000031DW003X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187U08XX_04_0029" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187U08XX_04_0029" proc-id="RM22W0E___00003GA00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>After the engine is started for 2 seconds, race the engine for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Utility / All Readiness.</ptxt>
</test1>
<test1>
<ptxt>Input DTC P0107 or P0108.</ptxt>
</test1>
<test1>
<ptxt>Check the DTC judgment result.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If STATUS is INCOMPLETE or N/A, race the engine for 1 minute, and then idle the engine for 5 minutes.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000187U08XX_04_0010" fin="false">A</down>
<right ref="RM00000187U08XX_04_0031" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000187U08XX_04_0010" proc-id="RM22W0E___00003G800000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187U08XX_04_0032" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187U08XX_04_0009" proc-id="RM22W0E___00003G700000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187U08XX_04_0032" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187U08XX_04_0032" proc-id="RM22W0E___00003GB00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) for 3 seconds and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>After the engine is started for 2 seconds, race the engine for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the GTS to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Engine and ECT / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P0107 or P0108.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or N/A, race the engine for 1 minute, and then idle the engine for 5 minutes.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000187U08XX_04_0031" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187U08XX_04_0031">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>