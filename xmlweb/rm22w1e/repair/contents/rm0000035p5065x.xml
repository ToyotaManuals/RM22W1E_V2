<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM0000035P5065X" category="C" type-id="803LX" name-id="BCE7I-01" from="201301" to="201308">
<dtccode>C1436</dtccode>
<dtcname>Yaw Rate Sensor Malfunction</dtcname>
<subpara id="RM0000035P5065X_01" type-id="60" category="03" proc-id="RM22W0E___0000AGM00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC C1419 and C1435 (See page <xref label="Seep01" href="RM0000035P406DX_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1436</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Either condition is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>With the vehicle stationary, the yaw rate sensor assembly output value is not 0°/s.</ptxt>
</item>
<item>
<ptxt>An abnormal comparison value.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<ptxt>Yaw rate sensor assembly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000035P5065X_02" type-id="32" category="03" proc-id="RM22W0E___0000AGN00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1232, C1243 and C1245 (See page <xref label="Seep01" href="RM000001EA3023X_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000035P5065X_03" type-id="51" category="05" proc-id="RM22W0E___0000AGO00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the yaw rate sensor assembly, perform calibration (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000035P5065X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000035P5065X_04_0006" proc-id="RM22W0E___0000AGR00000">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM0000046KV00MX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>CAN DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000035P5065X_04_0001" fin="false">A</down>
<right ref="RM0000035P5065X_04_0007" fin="true">B</right>
<right ref="RM0000035P5065X_04_0008" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000035P5065X_04_0001" proc-id="RM22W0E___0000AGP00000">
<testtitle>READ VALUE USING GTS (YAW RATE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.16in"/>
<colspec colname="COL2" colwidth="2.00in"/>
<colspec colname="COL3" colwidth="2.34in"/>
<colspec colname="COLSPEC2" colwidth="1.58in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Yaw Rate Sensor</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Yaw rate sensor/ Min.: -128°/s, Max.: 127°/s</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle stationary: 0°/s</ptxt>
<ptxt>Right turn: -128 to 0°/s</ptxt>
<ptxt>Left turn: 0 to 127°/s</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that the yaw rate sensor assembly output value is displayed on the GTS.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.86in"/>
<colspec colname="COL2" colwidth="5.01in"/>
<colspec colname="COL3" colwidth="1.21in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yaw rate sensor assembly output value is 0°/s when vehicle is stationary, but value fluctuates when turning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Yaw rate sensor assembly output value is not 0°/s when vehicle is stationary</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000035P5065X_04_0002" fin="false">A</down>
<right ref="RM0000035P5065X_04_0003" fin="true">B</right>
<right ref="RM0000035P5065X_04_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000035P5065X_04_0002" proc-id="RM22W0E___0000AGQ00000">
<testtitle>CHECK YAW RATE SENSOR ASSEMBLY INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Check that the yaw rate sensor assembly is installed properly (See page <xref label="Seep01" href="RM000000SS2063X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The sensor is tightened to the specified torque.</ptxt>
</specitem>
<specitem>
<ptxt>The sensor is not installed at an angle.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000035P5065X_04_0003" fin="true">OK</down>
<right ref="RM0000035P5065X_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000035P5065X_04_0003">
<testtitle>REPLACE YAW RATE SENSOR ASSEMBLY<xref label="Seep01" href="RM000000SS505TX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000035P5065X_04_0005">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000035P5065X_04_0004">
<testtitle>INSTALL YAW RATE SENSOR ASSEMBLY CORRECTLY<xref label="Seep01" href="RM000000SS2063X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000035P5065X_04_0007">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000035P5065X_04_0008">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>