<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001Q" variety="S001Q">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001Q_7C3Q3_T00J6" variety="T00J6">
<name>VANE PUMP (for 3UR-FE)</name>
<para id="RM000002H5E02MX" category="A" type-id="80002" name-id="PA0RY-01" from="201301">
<name>DISASSEMBLY</name>
<subpara id="RM000002H5E02MX_02" type-id="01" category="01">
<s-1 id="RM000002H5E02MX_02_0001" proc-id="RM22W0E___0000B3600000">
<ptxt>FIX VANE PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, fix the vane pump in a vise.</ptxt>
<sst>
<sstitem>
<s-number>09630-00014</s-number>
<s-subnumber>09631-00132</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C158820E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5E02MX_02_0002" proc-id="RM22W0E___0000B3700000">
<ptxt>REMOVE POWER STEERING SUCTION PORT UNION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and suction port union from the vane pump.</ptxt>
<figure>
<graphic graphicname="C158821" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the O-ring from the suction port union.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5E02MX_02_0003" proc-id="RM22W0E___0000B3800000">
<ptxt>REMOVE FLOW CONTROL VALVE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the pressure port union.</ptxt>
<figure>
<graphic graphicname="C228110" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the pressure port union.</ptxt>
</s2>
<s2>
<ptxt>Remove the flow control valve and compression spring.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5E02MX_02_0005" proc-id="RM22W0E___0000B3900000">
<ptxt>REMOVE VANE PUMP REAR HOUSING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and rear housing from the front housing.</ptxt>
<figure>
<graphic graphicname="C131230" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the O-ring from the rear housing.</ptxt>
<figure>
<graphic graphicname="C107025E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5E02MX_02_0006" proc-id="RM22W0E___0000B3A00000">
<ptxt>REMOVE VANE PUMP SHAFT WITH VANE PUMP PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using 2 screwdrivers, remove the snap ring from the vane pump shaft.</ptxt>
<figure>
<graphic graphicname="C131231" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the vane pump shaft with vane pump pulley.</ptxt>
<atten3>
<ptxt>Be careful not to drop or damage the vane pump shaft with vane pump pulley. If it is damaged, replace the vane pump assembly.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5E02MX_02_0007" proc-id="RM22W0E___0000B3B00000">
<ptxt>REMOVE VANE PUMP ROTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 vane pump plates.</ptxt>
<figure>
<graphic graphicname="C114697" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Take care not to drop the vane pump plate.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the vane pump rotor.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5E02MX_02_0008" proc-id="RM22W0E___0000B3C00000">
<ptxt>REMOVE VANE PUMP CAM RING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the cam ring from the front housing.</ptxt>
<figure>
<graphic graphicname="C114698" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5E02MX_02_0009" proc-id="RM22W0E___0000B3D00000">
<ptxt>REMOVE VANE PUMP FRONT SIDE PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front side plate from the front housing.</ptxt>
<figure>
<graphic graphicname="C114699" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the O-ring from the front side plate.</ptxt>
<figure>
<graphic graphicname="C107029E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the O-ring from the front housing.</ptxt>
<figure>
<graphic graphicname="C114700" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5E02MX_02_0010" proc-id="RM22W0E___0000B3E00000">
<ptxt>REMOVE VANE PUMP HOUSING OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver and piece of cloth, pry out the oil seal.</ptxt>
<figure>
<graphic graphicname="C114701E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to damage the front housing.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>