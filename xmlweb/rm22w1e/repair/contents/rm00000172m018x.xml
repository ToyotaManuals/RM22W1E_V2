<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S000Z" variety="S000Z">
<name>1GR-FE LUBRICATION</name>
<ttl id="12012_S000Z_7C3KI_T00DL" variety="T00DL">
<name>ENGINE OIL COOLER</name>
<para id="RM00000172M018X" category="A" type-id="30014" name-id="LU3VU-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM00000172M018X_01" type-id="01" category="01">
<s-1 id="RM00000172M018X_01_0001" proc-id="RM22W0E___00006XW00000">
<ptxt>INSTALL OIL COOLER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new seal washer to the bolt.</ptxt>
</s2>
<s2>
<ptxt>Install the oil cooler with the bolt and nut.</ptxt>
<torque>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>68</t-value1>
<t-value2>693</t-value2>
<t-value4>50</t-value4>
</torqueitem>
<subtitle>for nut</subtitle>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the 2 hoses.</ptxt>
<figure>
<graphic graphicname="A224741E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upward</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rearward</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The direction of the hose clamp is indicated in the illustration.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000172M018X_01_0027" proc-id="RM22W0E___000046200000">
<ptxt>INSTALL OIL FILTER BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the oil filter bracket and a new gasket with the 2 nuts and bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000172M018X_01_0028" proc-id="RM22W0E___000046300000">
<ptxt>CONNECT WATER BY-PASS PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 2 hoses and water by-pass pipe.</ptxt>
<figure>
<graphic graphicname="A224742E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upward</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rearward</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The direction of the hose clamp is indicated in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000172M018X_01_0009" proc-id="RM22W0E___00004CV00000">
<ptxt>INSTALL OIL FILTER ELEMENT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the inside of the oil filter cap, the threads and O-ring groove.</ptxt>
</s2>
<s2>
<ptxt>Apply a light coat of engine oil to a new O-ring for the cap, and then install the O-ring to the groove of the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A268595E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to install the O-ring in the proper location, otherwise oil may leak.</ptxt>
</item>
<item>
<ptxt>Do not twist the O-ring.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Set a new oil filter element in the oil filter cap.</ptxt>
</s2>
<s2>
<ptxt>Remove any dirt or foreign matter from the installation surface of the engine.</ptxt>
</s2>
<s2>
<ptxt>Apply a small amount of engine oil to the O-ring again and temporarily install the oil filter cap.</ptxt>
<atten3>
<ptxt>Do not pinch the O-ring for the cap.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using SST, tighten the oil filter cap.</ptxt>
<sst>
<sstitem>
<s-number>09228-06501</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A271363E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Filter Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Filter Bracket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>No Gap</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>After tightening the oil filter cap, make sure that there is no gap and that the O-ring is not protruding.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Apply a small amount of engine oil to a new drain plug O-ring and install the O-ring to the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A272718E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Before installing the O-ring, remove any dirt or foreign matter from the installation surface of the oil filter cap.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the oil filter drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Make sure that the O-ring does not get caught between the parts.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000172M018X_01_0011" proc-id="RM22W0E___000046R00000">
<ptxt>ADD ENGINE OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add fresh oil.</ptxt>
<atten3>
<ptxt>Do not allow engine oil to adhere to the moving parts of the belt tensioner, as this may cause malfunctions.</ptxt>
<figure>
<graphic graphicname="A308107" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If engine oil is on the location indicated by the arrow, replace the belt tensioner.</ptxt>
</atten3>
<spec>
<title>Standard Engine Oil</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Oil Grade</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Viscosity (SAE)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>API grade SL "energy-conserving", SM "energy-conserving", SN "resource-conserving" or ILSAC multigrade engine oil</ptxt>
</entry>
<entry valign="middle">
<ptxt>5W-30</ptxt>
<ptxt>10W-30</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>API grade SL, SM or SN multigrade engine oil</ptxt>
</entry>
<entry valign="middle">
<ptxt>15W-40</ptxt>
<ptxt>20W-50</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Engine Oil (for China)</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Oil Grade</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Viscosity (SAE)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>API grade SL "energy-conserving", SM "energy-conserving", SN "resource-conserving" or ILSAC multigrade engine oil</ptxt>
</entry>
<entry valign="middle">
<ptxt>0W-20</ptxt>
<ptxt>5W-20</ptxt>
<ptxt>5W-30</ptxt>
<ptxt>10W-30</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>API grade SL, SM or SN multigrade engine oil</ptxt>
</entry>
<entry valign="middle">
<ptxt>15W-40</ptxt>
<ptxt>20W-50</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Oil Capacity</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Drain and refill without oil filter change</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.6 liters (5.9 US qts, 4.9 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Drain and refill with oil filter change</ptxt>
</entry>
<entry valign="middle">
<ptxt>6.1 liters (6.4 US qts, 5.4 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Dry fill</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.1 liters (7.5 US qts, 6.2 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM00000172M018X_01_0015" proc-id="RM22W0E___000046400000">
<ptxt>INSPECT ENGINE OIL LEVEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Warm up the engine, and then stop the engine and wait for 5 minutes.</ptxt>
</s2>
<s2>
<ptxt>Check that the engine oil level is between the low level and full level marks on the dipstick.</ptxt>
<ptxt>If low, check for leakage and add oil up to the full level mark.</ptxt>
<atten3>
<ptxt>Do not fill engine oil above the full level mark.</ptxt>
</atten3>
<atten4>
<ptxt>A certain amount of engine oil will be consumed while driving. In the following situations, oil consumption may increase, and engine oil may need to be refilled in between oil maintenance intervals.</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the engine is new, for example directly after purchasing the vehicle or after replacing the engine.</ptxt>
</item>
<item>
<ptxt>If low quality oil or oil of an inappropriate viscosity is used.</ptxt>
</item>
<item>
<ptxt>When driving at high engine speed or with a heavy load, (when towing, or), when driving while accelerating or decelerating frequently.</ptxt>
</item>
<item>
<ptxt>When leaving the idling for a long time, or when driving frequently through heavy traffic.</ptxt>
</item>
</list1>
<ptxt>When judging the amount of oil consumption, keep in mind that the oil may have become diluted, making it difficult to judge the true level accurately.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000172M018X_01_0010" proc-id="RM22W0E___000011N00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add engine coolant.</ptxt>
<spec>
<title>Standard Capacity (for Manual transmission)</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>14.6 liters (15.4 US qts, 12.8 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>11.7 liters (12.4 US qts, 10.3 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard capacity (for Automatic transmission)</title>
<specitem>
<ptxt>11.2 liters (11.8 US qts, 9.9 Imp. qts)</ptxt>
</specitem>
</spec>
<spec>
<title>Standard Capacity (for China)</title>
<specitem>
<ptxt>14.4 liters (15.2 US qts, 12.7 Imp. qts)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Slowly pour coolant into the radiator reservoir until it reaches the F line.</ptxt>
</s2>
<s2>
<ptxt>Install the reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand, and then check the coolant level. If the coolant level is low, add coolant.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Set the air conditioning as follows while warming up the engine.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Fan speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Any setting except off</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Toward WARM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Air conditioning switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Start the engine and warm it up until the thermostat opens.</ptxt>
<atten4>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand, and checking when the engine coolant starts to flow inside the hose.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Maintain the engine speed at 2000 to 2500 rpm.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the radiator reservoir still has some coolant in it.</ptxt>
</item>
<item>
<ptxt>Pay attention to the needle of the water temperature meter. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
<item>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the F line.</ptxt>
</item>
<item>
<ptxt>Run the engine at 2000 rpm until the coolant level has stabilized.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand to bleed air.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves. Hot areas on the parts may injure your hands.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the fan.</ptxt>
</item>
</list1>
</atten2>
</s2>
<s2>
<ptxt>Stop the engine, and wait until the engine coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Check that the coolant level is between the F and L lines.</ptxt>
<ptxt>If the coolant level is below the L line, repeat all of the procedures above.</ptxt>
<ptxt>If the coolant level is above the F line, drain coolant so that the coolant level is between the F and L lines.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000172M018X_01_0021" proc-id="RM22W0E___000048000000">
<ptxt>INSPECT FOR OIL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start the engine. Make sure that there are no oil leaks from the area that was worked on.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000172M018X_01_0012" proc-id="RM22W0E___000011O00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>To avoid being burned, do not remove the radiator reservoir cap while the engine and radiator are still hot. Thermal expansion may cause hot engine coolant and steam to blow out from the radiator.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with engine coolant, and then attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and then check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and engine water pump for leakage. If there are no signs or traces of external engine coolant leakage, check the heater core, cylinder block and head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000172M018X_01_0023" proc-id="RM22W0E___000011P00000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover with the 10 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000172M018X_01_0029" proc-id="RM22W0E___000011Q00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly RH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000172M018X_01_0030" proc-id="RM22W0E___000011S00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and screw.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>