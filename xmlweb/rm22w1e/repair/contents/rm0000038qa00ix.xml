<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12027_S001J" variety="S001J">
<name>REAR SUSPENSION</name>
<ttl id="12027_S001J_7C3OY_T00I1" variety="T00I1">
<name>REAR SHOCK ABSORBER</name>
<para id="RM0000038QA00IX" category="A" type-id="80001" name-id="RP01U-03" from="201301">
<name>REMOVAL</name>
<subpara id="RM0000038QA00IX_01" type-id="11" category="10" proc-id="RM22W0E___0000A4J00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the RH side and LH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000038QA00IX_02" type-id="01" category="01">
<s-1 id="RM0000038QA00IX_02_0010" proc-id="RM22W0E___00009WH00000">
<ptxt>REMOVE STABILIZER CONTROL VALVE PROTECTOR (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp, and disconnect the connector from the protector.</ptxt>
<figure>
<graphic graphicname="C172095" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and protector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038QA00IX_02_0008" proc-id="RM22W0E___00009WN00000">
<ptxt>OPEN STABILIZER CONTROL WITH ACCUMULATOR HOUSING SHUTTER VALVE (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, loosen the lower and upper chamber shutter valves of the stabilizer control with accumulator housing 2.0 to 3.5 turns.</ptxt>
<figure>
<graphic graphicname="C175131E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When loosening a shutter valve, make sure that the end protrudes 2 to 3.5 mm (0.0787 to 0.137 in.) from the surface of the block, and do not turn the shutter valve any further.</ptxt>
</item>
<item>
<ptxt>Do not remove the shutter valves.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000038QA00IX_02_0001">
<ptxt>REMOVE REAR WHEEL</ptxt>
</s-1>
<s-1 id="RM0000038QA00IX_02_0004" proc-id="RM22W0E___0000A4M00000">
<ptxt>DISCONNECT NO. 3 PARKING BRAKE CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the No. 3 parking brake cable.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038QA00IX_02_0005" proc-id="RM22W0E___0000A4N00000">
<ptxt>DISCONNECT NO. 2 PARKING BRAKE CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the No. 2 parking brake cable.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038QA00IX_02_0007" proc-id="RM22W0E___0000A4P00000">
<ptxt>DISCONNECT REAR AXLE BREATHER HOSE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the rear axle breather hose from the rear axle housing assembly.</ptxt>
<figure>
<graphic graphicname="C174366" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038QA00IX_02_0011" proc-id="RM22W0E___00009RM00000">
<ptxt>DISCHARGE SUSPENSION FLUID PRESSURE (w/ Active Height Control)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C172938" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Connect a hose to the bleeder plug for the height control accumulator and loosen the bleeder plug.</ptxt>
</s2>
<s2>
<ptxt>Discharge the suspension fluid pressure.</ptxt>
</s2>
<s2>
<ptxt>After the fluid pressure has dropped and oil has drained out, tighten the bleeder plug and remove the hose.</ptxt>
<torque>
<torqueitem>
<t-value1>6.9</t-value1>
<t-value2>70</t-value2>
<t-value3>61</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000038QA00IX_02_0002" proc-id="RM22W0E___0000A4K00000">
<ptxt>SUPPORT REAR AXLE HOUSING ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Support the rear axle housing with a jack using a wooden block to avoid damage.</ptxt>
<figure>
<graphic graphicname="C174050" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038QA00IX_02_0003" proc-id="RM22W0E___0000A4L00000">
<ptxt>DISCONNECT REAR LATERAL CONTROL ROD ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and nut, and disconnect the rear lateral control rod from the frame.</ptxt>
<figure>
<graphic graphicname="C173671" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038QA00IX_02_0009" proc-id="RM22W0E___0000A4Q00000">
<ptxt>DISCONNECT REAR SHOCK ABSORBER ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt on the lower side of the shock absorber.</ptxt>
<figure>
<graphic graphicname="C178061" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the shock absorber from the axle housing.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038QA00IX_02_0012" proc-id="RM22W0E___0000A4R00000">
<ptxt>DISCONNECT NO. 4 SUSPENSION CONTROL PRESSURE HOSE (w/ Active Height Control)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and disconnect the pressure hose from the shock absorber.</ptxt>
<figure>
<graphic graphicname="C174042" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038QA00IX_02_0013" proc-id="RM22W0E___0000A4S00000">
<ptxt>REMOVE REAR SHOCK ABSORBER ASSEMBLY LH (w/ Active Height Control)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt on the lower side of the shock absorber.</ptxt>
<figure>
<graphic graphicname="C174043" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, hold the rear shock absorber in place.</ptxt>
<figure>
<graphic graphicname="C174051E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<sst>
<sstitem>
<s-number>09922-10010</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Remove the nut, upper bracket and shock absorber assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the lower bracket from the shock absorber.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038QA00IX_02_0006" proc-id="RM22W0E___0000A4O00000">
<ptxt>REMOVE REAR SHOCK ABSORBER ASSEMBLY LH (w/o Active Height Control)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt on the lower side of the shock absorber.</ptxt>
<figure>
<graphic graphicname="C174043" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, hold the rear shock absorber in place.</ptxt>
<sst>
<sstitem>
<s-number>09922-10010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C176903E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut, upper bracket and shock absorber assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the lower bracket from the shock absorber.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>