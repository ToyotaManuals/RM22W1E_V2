<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000000OS704BX" category="T" type-id="3001H" name-id="BC001L-104" from="201301" to="201308">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM000000OS704BX_z0" proc-id="RM22W0E___0000ABL00000">
<content5 releasenbr="2">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses and relays related to this system before inspecting the suspected areas below.</ptxt>
</item>
</list1>
</atten4>
<atten3>
<ptxt>When replacing the master cylinder or sensor, turn the ignition switch off.</ptxt>
</atten3>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Vehicle Stability Control System</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry morerows="8" colsep="1" valign="middle" align="left">
<ptxt>ABS, BA and/or EBD does not operate</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Check for DTCs again and make sure that no DTC is output</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000046KV00MX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>IG power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXW02FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Front speed sensor circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIA0KFX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369S06LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Master cylinder pressure sensor (check master cylinder pressure sensor by performing test mode inspection)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000452K00NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Check hydraulic brake booster with GTS. If abnormal, check hydraulic circuit for leakage.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171W029X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Check hydraulic brake booster with GTS. If abnormal, check hydraulic circuit for leakage.</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171W02AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: If symptoms still occur even after above circuits in suspected areas are inspected and proved to be normal, replace master cylinder solenoid</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: If symptoms still occur even after above circuits in suspected areas are inspected and proved to be normal, replace master cylinder solenoid</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="8" colsep="1" valign="middle" align="left">
<ptxt>ABS, BA and/or EBD does not operate efficiently</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Check for DTCs again and make sure that no DTC is output</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000046KV00MX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Front speed sensor circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIA0KFX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369S06LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Stop light switch circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIE0OHX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Master cylinder pressure sensor (check master cylinder pressure sensor by performing test mode inspection)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000452K00NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Check hydraulic brake booster with GTS. If abnormal, check hydraulic circuit for leakage.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171W029X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Check hydraulic brake booster with GTS. If abnormal, check hydraulic circuit for leakage.</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171W02AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: If symptoms still occur even after above circuits in suspected areas are inspected and proved to be normal, replace master cylinder solenoid</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: If symptoms still occur even after above circuits in suspected areas are inspected and proved to be normal, replace master cylinder solenoid</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="7" colsep="1" valign="middle" align="left">
<ptxt>VSC and/or TRC does not operate</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Check for DTCs again and make sure that no DTC is output</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000046KV00MX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>IG power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXW02FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front speed sensor circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIA0KFX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369S06LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Yaw rate sensor circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001EA3023X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Steering sensor circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIM0QLX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: If symptoms still occur even after above circuits in suspected areas are inspected and proved to be normal, replace master cylinder solenoid</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: If symptoms still occur even after above circuits in suspected areas are inspected and proved to be normal, replace master cylinder solenoid</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Sensor and switch signal check cannot be performed</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>TS and CG terminal circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXV02SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="4" colsep="1" valign="middle" align="left">
<ptxt>DTC check cannot be performed</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Check for DTCs again and make sure that no DTC is output</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000046KV00MX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>TC and CG terminal circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXU02JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>IG power source circuit (high power supply voltage malfunction)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000368W06ZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: If the symptoms still occur even after the above circuits in suspected areas are inspected and proved to be normal, replace the master cylinder solenoid</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: If the symptoms still occur even after the above circuits in suspected areas are inspected and proved to be normal, replace the master cylinder solenoid</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="3" colsep="1" valign="middle" align="left">
<ptxt>ABS warning light abnormal (Remains on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>ABS warning light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXE01WX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>IG power source circuit (high power supply voltage malfunction)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000368W06ZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="2" colsep="1" valign="middle" align="left">
<ptxt>ABS warning light abnormal (Does not come on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>ABS warning light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXL01WX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="3" colsep="1" valign="middle" align="left">
<ptxt>Brake warning light abnormal (Remains on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake warning light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXR01YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>IG power source circuit (high power supply voltage malfunction)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000368W06ZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Brake warning light abnormal (Does not come on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake warning light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXY01XX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="3" colsep="1" valign="middle" align="left">
<ptxt>Slip indicator light abnormal (Remains on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Slip indicator light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXZ01QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>IG power source circuit (high power supply voltage malfunction)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000368W06ZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Slip indicator light abnormal (Does not come on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Slip indicator light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DY001OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="2" colsep="1" valign="middle" align="left">
<ptxt>VSC OFF indicator light abnormal (Remains on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>VSC OFF indicator light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001FQO0G1X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="2" colsep="1" valign="middle" align="left">
<ptxt>VSC OFF indicator light abnormal (Does not come on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>VSC OFF indicator light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001FQP0FZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colsep="1" valign="middle" align="left">
<ptxt>Skid control buzzer abnormal*1</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Skid control buzzer circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXP02DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colsep="1" valign="middle">
<ptxt>Buzzer abnormal</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>VSC buzzer circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIS0B5X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="2" colsep="1" valign="middle">
<ptxt>TRC OFF indicator light abnormal (Remains on)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>TRC OFF indicator light circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003QXV01HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="2" colsep="1" valign="middle">
<ptxt>TRC OFF indicator light abnormal (Does not come on)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>TRC OFF indicator light circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003QXW01HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="2" colsep="1" valign="middle">
<ptxt>CRAWL indicator light abnormal (Remains on)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>CRAWL indicator light circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DY101IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="2" colsep="1" valign="middle">
<ptxt>CRAWL indicator light abnormal (Does not come on)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>CRAWL indicator light circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001FQN01GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for LHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>for RHD: Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000171U01TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Turn assist indicator light abnormal</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Turn assist indicator light circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000053ON004X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Multi-terrain select indicator light abnormal (Remains on)*2</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Multi-terrain select indicator light circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000045Z700PX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Multi-terrain select indicator light abnormal (Does not come on)*2</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Multi-terrain select indicator light circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000045Z800PX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>*1: w/ Dynamic Radar Cruise Control System</ptxt>
</item>
<item>
<ptxt>*2: w/ Multi-terrain Select</ptxt>
</item>
</list1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>