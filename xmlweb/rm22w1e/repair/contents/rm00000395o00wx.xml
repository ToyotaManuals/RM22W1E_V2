<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UZ_T00O2" variety="T00O2">
<name>REAR SEAT CUSHION HEATER (for 60/40 Split Seat Type 60 Side)</name>
<para id="RM00000395O00WX" category="A" type-id="30014" name-id="SE6SZ-03" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM00000395O00WX_02" type-id="11" category="10" proc-id="RM22W0E___0000G8C00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000395O00WX_01" type-id="01" category="01">
<s-1 id="RM00000395O00WX_01_0001" proc-id="RM22W0E___0000G8A00000">
<ptxt>INSTALL REAR SEAT CUSHION HEATER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the seat cushion heater with the name stamp side facing the seat cushion cover.</ptxt>
</s2>
<s2>
<ptxt>Install the seat cushion heater with new tack pins.</ptxt>
<figure>
<graphic graphicname="B181209E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000395O00WX_01_0021" proc-id="RM22W0E___0000G4V00000">
<ptxt>INSTALL REAR SEPARATE TYPE SEAT CUSHION COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184036E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using hog ring pliers, install the seat cushion cover to the seat cushion pad with new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Attach the 3 pieces of fastening tape.</ptxt>
<figure>
<graphic graphicname="B181234E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0022" proc-id="RM22W0E___0000G4W00000">
<ptxt>INSTALL SEAT CUSHION COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seat cushion cover with pad.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Pass the side airbag wire harness through the hole in the seat cushion.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<s3>
<ptxt>Pass the seatback heater wire harness through the hole in the seat cushion.</ptxt>
<figure>
<graphic graphicname="B184056" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Connect the 2 seat heater connectors.</ptxt>
<figure>
<graphic graphicname="B182596" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the side airbag wire harness clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0039" proc-id="RM22W0E___0000G6R00000">
<ptxt>CONNECT NO. 1 SEAT 3 POINT TYPE BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184533E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the belt anchor.</ptxt>
<atten3>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the vehicle body.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0038" proc-id="RM22W0E___0000G6Q00000">
<ptxt>INSTALL REAR NO. 1 SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184532E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the belt with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the vehicle body.</ptxt>
</atten3>
<atten4>
<ptxt>The No. 1 seat 3 point type belt anchor is fixed with the same bolt as the rear No. 1 seat inner belt LH. Therefore, connect it when installing the rear No. 1 seat inner belt LH.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0023" proc-id="RM22W0E___0000G4Y00000">
<ptxt>INSTALL REAR SEAT UNDER TRAY COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181181" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0024" proc-id="RM22W0E___0000G4Z00000">
<ptxt>INSTALL REAR SEAT CUSHION COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181231" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0025" proc-id="RM22W0E___0000G5100000">
<ptxt>INSTALL REAR UNDER SIDE COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181230" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws and 2 clips to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 5 screws.</ptxt>
</s2>
<s2>
<ptxt>Attach the 10 claws.</ptxt>
<figure>
<graphic graphicname="B181229" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0026" proc-id="RM22W0E___0000G5200000">
<ptxt>INSTALL REAR SEATBACK STAY COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181226" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0027" proc-id="RM22W0E___0000G5300000">
<ptxt>INSTALL SEAT BELT ANCHOR COVER CAP
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the anchor cover cap with the screw.</ptxt>
<figure>
<graphic graphicname="B181228" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the claw to close the cap.</ptxt>
<figure>
<graphic graphicname="B184029" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0028" proc-id="RM22W0E___0000G5400000">
<ptxt>INSTALL REAR NO. 1 SEAT HINGE SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184017" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the hinge with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0029" proc-id="RM22W0E___0000G5500000">
<ptxt>INSTALL REAR NO. 1 SEAT HINGE SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the cable clamp to connect the cable.</ptxt>
<figure>
<graphic graphicname="B181176" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the hinge with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B184045" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0030" proc-id="RM22W0E___0000G5600000">
<ptxt>INSTALL REAR NO. 2 SEAT PROTECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the wire harness, and attach 2 claws to close the protector.</ptxt>
</s2>
<s2>
<ptxt>Attach the claw to install the protector to the seat hinge.</ptxt>
<figure>
<graphic graphicname="B182595" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the claw to install the wire harness connector.</ptxt>
<figure>
<graphic graphicname="B184016" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Attach the claw to install the wire harness connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0031" proc-id="RM22W0E___0000G5A00000">
<ptxt>INSTALL REAR NO. 2 SEAT LEG SIDE COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Pass the fold seat stopper band through the seat leg side cover and attach it to the seat cushion frame with the bolt.</ptxt>
<figure>
<graphic graphicname="B184044" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 4 claws to install the seat leg side cover.</ptxt>
<figure>
<graphic graphicname="B181224" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 4 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0032" proc-id="RM22W0E___0000G5C00000">
<ptxt>INSTALL UPPER SEAT TRACK RAIL COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184041" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0033" proc-id="RM22W0E___0000G5D00000">
<ptxt>INSTALL UPPER SEAT TRACK RAIL COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184028" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0034" proc-id="RM22W0E___0000G5E00000">
<ptxt>INSTALL REAR NO. 1 SEAT RECLINING COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184015" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0035" proc-id="RM22W0E___0000G5F00000">
<ptxt>INSTALL REAR NO. 1 SEAT RECLINING COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184027E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws in the order shown in the illustration to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0036" proc-id="RM22W0E___0000G5G00000">
<ptxt>INSTALL RECLINING ADJUSTER RELEASE HANDLE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184013" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the release handle with the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0037" proc-id="RM22W0E___0000G5H00000">
<ptxt>INSTALL SEAT ADJUSTER BOLT COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184012" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395O00WX_01_0020" proc-id="RM22W0E___0000G8B00000">
<ptxt>INSTALL REAR NO. 1 SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear No. 1 seat assembly (See page <xref label="Seep01" href="RM00000390X00OX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>