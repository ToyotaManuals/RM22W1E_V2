<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000A" variety="S000A">
<name>1GR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000A_7C3ES_T007V" variety="T007V">
<name>CYLINDER HEAD GASKET</name>
<para id="RM000002BK5041X" category="A" type-id="80001" name-id="EM84K-04" from="201301">
<name>REMOVAL</name>
<subpara id="RM000002BK5041X_01" type-id="01" category="01">
<s-1 id="RM000002BK5041X_01_0016" proc-id="RM22W0E___000043G00000">
<ptxt>REMOVE TIMING CHAIN COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000002BZE023X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000002BK5041X_01_0097" proc-id="RM22W0E___000043J00000">
<ptxt>SET NO. 1 CYLINDER TO TDC/COMPRESSION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the pulley set bolt.</ptxt>
</s2>
<s2>
<ptxt>Turn the crankshaft clockwise to align the timing mark on the crank angle sensor plate with the RH block bore center line (TDC/compression).</ptxt>
<figure>
<graphic graphicname="A222172E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Timing Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sensor Plate</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Check that the timing marks of the camshaft timing gears are aligned with the timing marks of the bearing caps as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A225378E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<ptxt>If not, turn the crankshaft clockwise 1 revolution (360°) and align the timing marks as above.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Timing Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0098" proc-id="RM22W0E___000043K00000">
<ptxt>REMOVE NO. 1 CHAIN TENSIONER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Move the stopper plate upward to release the lock, and push the plunger deep into the tensioner.</ptxt>
<figure>
<graphic graphicname="A124589E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Plunger</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stopper Plate</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Move the stopper plate downward to set the lock, and insert a pin with a diameter of 1.27 mm (0.0500 in.) into the stopper plate hole.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and No. 1 chain tensioner assembly.</ptxt>
<figure>
<graphic graphicname="A095165E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0099" proc-id="RM22W0E___000043L00000">
<ptxt>REMOVE CHAIN TENSIONER SLIPPER
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223730" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the chain tensioner slipper.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0100" proc-id="RM22W0E___000043M00000">
<ptxt>REMOVE CHAIN SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the crankshaft counterclockwise 10° to loosen the chain of the crankshaft timing sprocket.</ptxt>
<figure>
<graphic graphicname="A221173E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Timing Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sensor Plate</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the pulley set bolt.</ptxt>
</s2>
<s2>
<ptxt>Remove the chain sub-assembly from the crankshaft timing sprocket and place it on the crankshaft.</ptxt>
<figure>
<graphic graphicname="A221174" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the camshaft timing gear assembly on bank 1 clockwise approximately 60° so that it is as shown in the illustration. Be sure to loosen the chain sub-assembly between the banks.</ptxt>
<figure>
<graphic graphicname="A095682E12" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the chain sub-assembly.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Bank 1</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0101" proc-id="RM22W0E___000043N00000">
<ptxt>REMOVE NO. 1 IDLE GEAR SHAFT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 10 mm hexagon wrench, remove the No. 2 idle gear shaft, No. 1 idle gear and No. 1 idle gear shaft.</ptxt>
<figure>
<graphic graphicname="A221175" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0104" proc-id="RM22W0E___000043Q00000">
<ptxt>REMOVE NO. 1 CHAIN VIBRATION DAMPER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and No. 1 chain vibration damper.</ptxt>
<figure>
<graphic graphicname="A223642" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0102" proc-id="RM22W0E___000043O00000">
<ptxt>REMOVE NO. 2 CHAIN VIBRATION DAMPER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 No. 2 chain vibration dampers.</ptxt>
<figure>
<graphic graphicname="A221176" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0103" proc-id="RM22W0E___000043P00000">
<ptxt>REMOVE CRANKSHAFT TIMING SPROCKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the crankshaft timing sprocket.</ptxt>
<figure>
<graphic graphicname="A158530E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0105" proc-id="RM22W0E___000043R00000">
<ptxt>REMOVE CAMSHAFT TIMING GEARS AND NO. 2 CHAIN (for Bank 1)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>While raising the No. 2 chain tensioner assembly, insert a pin with a diameter of 1.0 mm (0.0394 in.) into the hole to hold the No. 2 chain tensioner assembly.</ptxt>
<figure>
<graphic graphicname="A221132E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Plunger</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using SST to hold the hexagonal portion of each camshaft, loosen the bolts of the camshaft timing gear assembly and camshaft timing exhaust gear assembly.</ptxt>
<sst>
<sstitem>
<s-number>09922-10010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A221177E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hold</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not loosen the other 4 bolts. If any of the 4 bolts is loosened, replace the camshaft timing gear assembly and/or camshaft timing exhaust gear assembly with a new one.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the 2 bolts and camshaft timing gear assembly together with the No. 2 chain.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0106" proc-id="RM22W0E___000043S00000">
<ptxt>REMOVE NO. 2 CHAIN TENSIONER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and No. 2 chain tensioner assembly.</ptxt>
<figure>
<graphic graphicname="A221178" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0107" proc-id="RM22W0E___000043T00000">
<ptxt>REMOVE CAMSHAFT BEARING CAP (for Bank 1)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the camshafts are positioned as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A129700E06" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Knock Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front View</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Uniformly loosen and remove the 8 bearing cap bolts in several steps in the sequence shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A221179E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Uniformly loosen and remove the 12 bearing cap bolts in several steps in the sequence shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A221180E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Uniformly loosen the bolts while keeping the camshaft level.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the 5 camshaft bearing caps.</ptxt>
</s2>
<s2>
<ptxt>Remove the camshaft and No. 2 camshaft.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0108" proc-id="RM22W0E___000043U00000">
<ptxt>REMOVE CAMSHAFT HOUSING SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the camshaft housing sub-assembly RH by prying between the cylinder head and camshaft housing sub-assembly RH with a screwdriver.</ptxt>
<figure>
<graphic graphicname="A221181E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to damage the contact surfaces of the cylinder head and camshaft housing sub-assembly RH.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0109" proc-id="RM22W0E___000043V00000">
<ptxt>REMOVE CAMSHAFT TIMING GEARS AND NO. 2 CHAIN (for Bank 2)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>While pushing down the No. 3 chain tensioner assembly, insert a pin with a diameter of 1.0 mm (0.0394 in.) into the hole to hold the No. 3 chain tensioner assembly.</ptxt>
<figure>
<graphic graphicname="A221182E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Plunger</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using SST to hold the hexagonal portion of each camshaft, loosen the bolts of the camshaft timing gear assembly and camshaft timing exhaust gear assembly.</ptxt>
<sst>
<sstitem>
<s-number>09922-10010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A223637E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hold</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not loosen the other 4 bolts. If any of the 4 bolts is loosened, replace the camshaft timing gear assembly and/or camshaft timing exhaust gear assembly with a new one.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the 2 bolts and camshaft timing gear together with the No. 2 chain.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0110" proc-id="RM22W0E___000043W00000">
<ptxt>REMOVE NO. 3 CHAIN TENSIONER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and No. 3 chain tensioner assembly.</ptxt>
<figure>
<graphic graphicname="A221183" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0111" proc-id="RM22W0E___000043X00000">
<ptxt>REMOVE CAMSHAFT BEARING CAP (for Bank 2)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the camshafts are positioned as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A223636E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Knock Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front View</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Uniformly loosen and remove the 8 bearing cap bolts in several steps in the sequence shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A221184E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Uniformly loosen and remove the 13 bearing cap bolts in several steps in the sequence shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A221185E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Uniformly loosen the bolts while keeping the camshaft level.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the 5 camshaft bearing caps.</ptxt>
</s2>
<s2>
<ptxt>Remove the No. 3 camshaft and No. 4 camshaft.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0112" proc-id="RM22W0E___000043Y00000">
<ptxt>REMOVE CAMSHAFT HOUSING SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the camshaft housing sub-assembly LH by prying between the cylinder head and camshaft housing sub-assembly LH with a screwdriver.</ptxt>
<figure>
<graphic graphicname="A221186E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to damage the contact surfaces of the cylinder head and camshaft housing sub-assembly LH.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0113" proc-id="RM22W0E___000043Z00000">
<ptxt>REMOVE NO. 1 VALVE ROCKER ARM SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 24 valve rocker arms from the cylinder head.</ptxt>
<atten4>
<ptxt>Arrange the removed parts in the correct order.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0114" proc-id="RM22W0E___000044000000">
<ptxt>REMOVE VALVE LASH ADJUSTER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 24 valve lash adjusters from the cylinder head.</ptxt>
<atten4>
<ptxt>Arrange the removed parts in the correct order.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0115" proc-id="RM22W0E___000044100000">
<ptxt>REMOVE VALVE STEM CAP
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 24 valve stem caps from the cylinder head.</ptxt>
<atten4>
<ptxt>Arrange the removed parts in the correct order.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0116" proc-id="RM22W0E___000044200000">
<ptxt>REMOVE REAR WATER BY-PASS JOINT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts, 4 nuts, rear water by-pass joint and 2 gaskets.</ptxt>
<figure>
<graphic graphicname="A076534E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the No. 1 water outlet pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BK5041X_01_0095" proc-id="RM22W0E___000043I00000">
<ptxt>REMOVE CYLINDER HEAD SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 10 mm bi-hexagon wrench, uniformly loosen the 8 cylinder head bolts in the sequence shown in the illustration. Remove the 8 cylinder head bolts and plate washers.</ptxt>
<figure>
<graphic graphicname="A221092E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to drop washers into the cylinder head sub-assembly.</ptxt>
</item>
<item>
<ptxt>Cylinder head warpage or cracking could result from removing bolts in an incorrect order.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Arrange the removed parts in the correct order.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the cylinder head sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5041X_01_0094" proc-id="RM22W0E___000043H00000">
<ptxt>REMOVE CYLINDER HEAD LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Uniformly loosen and remove the 2 bolts in several steps in the sequence shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A221093E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a 10 mm bi-hexagon wrench, uniformly loosen the 8 bolts in the sequence shown in the illustration. Remove the 8 cylinder head bolts and plate washers.</ptxt>
<figure>
<graphic graphicname="A221094E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to drop washers into the cylinder head sub-assembly.</ptxt>
</item>
<item>
<ptxt>Cylinder head warpage or cracking could result from removing bolts in an incorrect order.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Be sure to keep the removed parts for each installation position separate.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the cylinder head LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK5041X_01_0081">
<ptxt>REMOVE CYLINDER HEAD GASKET</ptxt>
</s-1>
<s-1 id="RM000002BK5041X_01_0082">
<ptxt>REMOVE NO. 2 CYLINDER HEAD GASKET</ptxt>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>