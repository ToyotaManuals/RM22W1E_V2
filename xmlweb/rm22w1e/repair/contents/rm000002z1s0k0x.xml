<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000002Z1S0K0X" category="C" type-id="803DV" name-id="ESUAR-04" from="201301" to="201308">
<dtccode>P060B</dtccode>
<dtcname>Internal Control Module A/D Processing Performance</dtcname>
<subpara id="RM000002Z1S0K0X_01" type-id="60" category="03" proc-id="RM22W0E___00001VF00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when a communication error occurs in the ECM.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P060B</ptxt>
</entry>
<entry>
<ptxt>There is an ECM main CPU communication error (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Knock sensor</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002Z1S0K0X_07" type-id="73" category="03" proc-id="RM22W0E___00001VH00000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<list1 type="ordered">
<item>
<ptxt>Turn the engine switch off.</ptxt>
</item>
<item>
<ptxt>Disconnect the cable from the negative (-) battery terminal and wait for 1 minute.</ptxt>
</item>
<item>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and wait for 16 seconds or more.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002Z1S0K0X_05" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002Z1S0K0X_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002Z1S0K0X_06_0001" proc-id="RM22W0E___00001VG00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG). </ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS off. </ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the GTS.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal and wait for 1 minute.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Wait 16 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>DTC is not output</ptxt>
</entry>
<entry align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>DTC P060B is output</ptxt>
</entry>
<entry align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>DTC P060B and P0327, P0328, P032C, P032D, P0332, P0333, P033C or P033D are output</ptxt>
</entry>
<entry align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002Z1S0K0X_06_0002" fin="true">A</down>
<right ref="RM000002Z1S0K0X_06_0003" fin="true">B</right>
<right ref="RM000002Z1S0K0X_06_0004" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002Z1S0K0X_06_0002">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ108X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Z1S0K0X_06_0003">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Z1S0K0X_06_0004">
<testtitle>GO TO DTC P0327, P0328, P032C, P032D, P0332, P0333, P033C OR P033D<xref label="Seep01" href="RM000000SVT0RDX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>