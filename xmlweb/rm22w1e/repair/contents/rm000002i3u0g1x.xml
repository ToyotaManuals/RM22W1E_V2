<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000002I3U0G1X" category="C" type-id="302IB" name-id="ESDSC-07" from="201308">
<dtccode>P0657</dtccode>
<dtcname>Actuator Supply Voltage Circuit / Open</dtcname>
<subpara id="RM000002I3U0G1X_01" type-id="60" category="03" proc-id="RM22W0E___000027B00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM monitors the output voltage to the throttle actuator. This self-check ensures that the ECM is functioning properly. The output voltage usually is 0 V when the engine switch is turned off. If the output voltage is higher than 7 V when the engine switch is turned off, the ECM will illuminate the MIL and store DTC(s) when the engine switch is turned on (IG).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0657</ptxt>
</entry>
<entry>
<ptxt>A throttle actuator power supply error.</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002I3U0G1X_05" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002I3U0G1X_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002I3U0G1X_06_0001" proc-id="RM22W0E___000027C00001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear DTC (See page <xref label="Seep01" href="RM000000PDK17ZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal and wait for 1 minute.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) for 10 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check DTC.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>P0657 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002I3U0G1X_06_0002" fin="true">OK</down>
<right ref="RM000002I3U0G1X_06_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002I3U0G1X_06_0002">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ13NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002I3U0G1X_06_0003">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292039X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>