<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3D1_T0064" variety="T0064">
<name>THROTTLE BODY</name>
<para id="RM000002PQL03AX" category="A" type-id="30014" name-id="ES10NM-002" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000002PQL03AX_01" type-id="01" category="01">
<s-1 id="RM000002PQL03AX_01_0001" proc-id="RM22W0E___000013J00000">
<ptxt>INSTALL THROTTLE BODY WITH MOTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 4 water by-pass hose to the throttle body with motor assembly.</ptxt>
<figure>
<graphic graphicname="A230336E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When installing the hose, make sure the paint mark and clip are as shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the No. 12 water by-pass hose to the throttle body with motor assembly.</ptxt>
<figure>
<graphic graphicname="A230335E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When installing the hose, make sure the paint mark and clip are as shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Align the protrusion of a new gasket with the groove of the intake manifold and install the gasket to the intake manifold.</ptxt>
<figure>
<graphic graphicname="A271378E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Groove</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the throttle body with motor assembly with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the throttle position sensor and control motor connector.</ptxt>
<figure>
<graphic graphicname="A271379E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the No. 4 water by-pass hose and No. 12 water by-pass hose.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 mm (0.197 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>13 to 17 mm (0.511 to 0.669 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When connecting the No. 12 water by-pass hose, make sure the paint marks and clips are as shown in the illustration.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002PQL03AX_01_0018" proc-id="RM22W0E___000013700000">
<ptxt>INSTALL AIR CLEANER CAP AND HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the air cleaner cap and hose, and then tighten the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>2.5</t-value1>
<t-value2>25</t-value2>
<t-value3>22</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Connect the mass air flow meter connector and attach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 2 PCV hose and No. 1 air hose.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002PQL03AX_01_0011" proc-id="RM22W0E___000013K00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add engine coolant.</ptxt>
<spec>
<title>Standard Capacity (w/o ATF Warmer)</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL2" colwidth="2.07in"/>
<colspec colname="COL3" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>16.5 liters (17.4 US qts, 14.5 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>13.8 liters (14.6 US qts, 12.1 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Capacity (w/ ATF Warmer)</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL2" colwidth="2.07in"/>
<colspec colname="COL3" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>17.0 liters (18.0 US qts, 15.0 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>14.2 liters (15.0 US qts, 12.5 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</item>
<item>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand, and then check the coolant level. If the coolant level is low, add coolant.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Slowly pour coolant into the radiator reservoir until it reaches the F line.</ptxt>
</s2>
<s2>
<ptxt>Install the reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.*1</ptxt>
</s2>
<s2>
<ptxt>Start the engine and stop it immediately.*2</ptxt>
</s2>
<s2>
<ptxt>Allow approximately 10 seconds to pass. Then remove the radiator cap and check the coolant level. If the coolant level has decreased, add coolant.*3</ptxt>
</s2>
<s2>
<ptxt>Repeat steps *1, *2 and *3 until the coolant level does not decrease.</ptxt>
<atten4>
<ptxt>Be sure to perform this step while the engine is cold, as air in the No. 1 radiator hose will flow into the radiator if the engine is warmed up and the thermostat opens.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the radiator cap.*4</ptxt>
</s2>
<s2>
<ptxt>Set the air conditioning as follows.*5</ptxt>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Fan speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Any setting except off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
<entry valign="middle">
<ptxt>Toward WARM</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air conditioning switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Start the engine, warm it up until the thermostat opens, and then continue to run the engine for several minutes to circulate the coolant.*6</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves. Hot areas on the parts may injure your hands.</ptxt>
</item>
<item>
<ptxt>Be careful of the fan.</ptxt>
</item>
<item>
<ptxt>Be careful as the engine, radiator and radiator hoses are hot and can cause burns.</ptxt>
</item>
</list1>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the F line.</ptxt>
</item>
<item>
<ptxt>Do not start the engine when there is no coolant in the radiator reservoir.</ptxt>
</item>
<item>
<ptxt>Pay attention to the needle of the engine coolant temperature receiver gauge. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand to bleed air while warming up the engine.</ptxt>
</item>
<item>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand and checking when the engine coolant starts to flow inside the hose.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Stop the engine, wait until the engine coolant cools down to ambient temperature. Then remove the radiator cap and check the coolant level.*7</ptxt>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>If the coolant level has decreased, add coolant and warm up the engine until the thermostat opens.*8</ptxt>
</s2>
<s2>
<ptxt>If the coolant level has not decreased, check that the coolant level in the radiator reservoir is at the F line.</ptxt>
<ptxt>If the coolant level is below the F line, repeat steps *4 through *8.</ptxt>
<ptxt>If the coolant level is above the F line, drain coolant until the coolant level reaches the F line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03AX_01_0012" proc-id="RM22W0E___000013L00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>To avoid being burned, do not remove the radiator reservoir cap while the engine and radiator are still hot. Thermal expansion may cause hot engine coolant and steam to blow out from the radiator.</ptxt>
</atten2>
<s2>
<ptxt>Remove the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Check for excessive deposits of rust or scales around the radiator reservoir cap and radiator reservoir filler hole. Also, the engine coolant should be free of oil.</ptxt>
<ptxt>If excessively dirty, replace the engine coolant.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03AX_01_0021" proc-id="RM22W0E___000013N00000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover sub-assembly with the 10 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03AX_01_0022" proc-id="RM22W0E___000011Q00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly RH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03AX_01_0024" proc-id="RM22W0E___000011S00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03AX_01_0019" proc-id="RM22W0E___000013800000">
<ptxt>INSTALL V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 3 V-bank cover grommets with the 3 pins, and press down on the V-bank cover to attach the pins.</ptxt>
<figure>
<graphic graphicname="A274416E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03AX_01_0020" proc-id="RM22W0E___000013M00000">
<ptxt>PERFORM INITIALIZATION</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to perform this procedure after reassembling the throttle body with motor assembly or removing and reinstalling any throttle body component.</ptxt>
</item>
<item>
<ptxt>Perform the following procedure after replacing the ECM, throttle body with motor assembly or any throttle body components. The following procedure should also be performed if the throttle body is cleaned.</ptxt>
</item>
<item>
<ptxt>Be sure to perform this procedure after reconnecting the battery cable and replacing the ECM.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Disconnect the EFI fuse, wait at least 60 seconds, and then reconnect the fuse.</ptxt>
</s2>
<s2>
<ptxt>Turn the engine switch on (IG) without operating the accelerator pedal.</ptxt>
<atten3>
<ptxt>If the accelerator pedal is operated, perform the above steps again.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the GTS to the DLC3 and clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</s2>
<s2>
<ptxt>Start the engine and check that the MIL is not illuminated and that the idle speed is within the specified range when the A/C is switched off after the engine is warmed up.</ptxt>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" align="right" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Engine Idle Speed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A/C switched off</ptxt>
</entry>
<entry align="center">
<ptxt>650 to 750 rpm</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to perform this step with all accessories off.</ptxt>
</item>
<item>
<ptxt>Make sure that the shift lever is in N or P.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ETC / Data List / Throttle Sensor Volt %. Fully depress the accelerator pedal and check that the value is 60% or more.</ptxt>
</s2>
<s2>
<ptxt>Perform a road test and confirm that there are no abnormalities.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>