<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SN_T00LQ" variety="T00LQ">
<name>POWER DOOR LOCK CONTROL SYSTEM</name>
<para id="RM000001JW704YX" category="U" type-id="3001G" name-id="DL7PU-02" from="201301">
<name>TERMINALS OF ECU</name>
<subpara id="RM000001JW704YX_z0" proc-id="RM22W0E___0000E7W00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK MULTIPLEX NETWORK MASTER SWITCH ASSEMBLY</ptxt>
<figure>
<graphic graphicname="B158007E60" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>for Models with Jam Protection Function on 4 Windows</ptxt>
<step3>
<ptxt>Disconnect the I11*1 or I22*2 switch connector.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I11-12 (GND) - Body ground*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I11-11 (B) - I11-12 (GND)*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I22-12 (GND) - Body ground*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I22-11 (B) - I22-12 (GND)*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step3>
</step2>
<step2>
<ptxt>for Models with Jam Protection Function on Driver Door Window Only</ptxt>
<step3>
<ptxt>Disconnect the I31*1 or I26*2 switch connector.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I31-1 (E) - Body ground*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I31-6 (B) - I31-1 (E)*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I26-9 (E) - Body ground*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I26-6 (B) - I26-9 (E)*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</ptxt>
<figure>
<graphic graphicname="C172609E03" width="7.106578999in" height="8.799559038in"/>
</figure>
<step2>
<ptxt>Disconnect the 2A, 2B and 2D ECU connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2B-20 (BATB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2A-1 (ACC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ACC power supply</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2A-1 (ACC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ACC power supply</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-62 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Reconnect the 2A, 2B and 2D ECU connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.43in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E1-24 (DCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L*1 - Body ground</ptxt>
<ptxt>Y*2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door courtesy switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-24 (DCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L*1 - Body ground</ptxt>
<ptxt>Y*2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door courtesy switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, and driver side door courtesy switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-21 (PCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Y*1 - Body ground</ptxt>
<ptxt>L*2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front passenger side door courtesy switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front passenger side door open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-21 (PCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Y*1 - Body ground</ptxt>
<ptxt>L*2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front passenger side door courtesy switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, and passenger side door courtesy switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2C-2 (LCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door LH courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door LH open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2C-2 (LCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door LH courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, and rear LH side door courtesy switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-7 (RCTY) - Body ground</ptxt>
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door RH courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door RH open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-7 (RCTY) - Body ground</ptxt>
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door RH courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, and rear RH side door courtesy switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-17 (RCTY) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door RH courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door RH open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-17 (RCTY) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door RH courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, and rear RH side door courtesy switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-25 (BCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-25 (BCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door courtesy light switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, and back door closed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-3 (ACT+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door lock motor LOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-3 (ACT+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door lock motor LOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in lock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2C-24 (ACT+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door lock motor LOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2C-24 (ACT+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door lock motor LOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in lock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-5 (ACTD) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door lock motor UNLOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-5 (ACTD) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door lock motor UNLOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-2 (ACT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door lock motor UNLOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-2 (ACT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door lock motor UNLOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2C-23 (ACT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door lock motor UNLOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2C-23 (ACT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door lock motor UNLOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-10 (UL3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Y*1 - Body ground</ptxt>
<ptxt>L*2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door lock key switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door key cylinder in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-10 (UL3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Y*1 - Body ground</ptxt>
<ptxt>L*2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door lock key switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, all doors closed and driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-53 (L2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door lock key switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door key cylinder in lock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-53 (L2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door lock key switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, all doors closed and driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-52 (UL1) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-52 (UL1) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-52 (UL1) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door control switch assembly input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door control switch assembly in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-52 (UL1) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door control switch assembly input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door control switch assembly in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-49 (L1) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>BE - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-49 (L1) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>BE - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-49 (L1) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door control switch assembly input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door control switch assembly in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-49 (L1) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door control switch assembly input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Door control switch assembly in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-1 (TR+) - Body ground</ptxt>
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door lock motor UNLOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-1 (TR+) - Body ground</ptxt>
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door lock motor UNLOCK drive output</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Master switch (door control switch) or driver side door key cylinder in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-9 (LSWD) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR*5 - Body ground</ptxt>
<ptxt>BE*6 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door lock position switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door unlocked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-9 (LSWD) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR*5 - Body ground</ptxt>
<ptxt>BE*6 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door lock position switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, all doors closed and driver side door locked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-27 (LSWP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Y*1 - Body ground</ptxt>
<ptxt>BR*2*5 - Body ground</ptxt>
<ptxt>BE*6 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front passenger side door lock position switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front passenger side door unlocked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-27 (LSWP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Y*1 - Body ground</ptxt>
<ptxt>BR*2*5 - Body ground</ptxt>
<ptxt>BE*6 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front passenger side door lock position switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, all doors closed and passenger side door locked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2C-1 (LSWL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR*5 - Body ground</ptxt>
<ptxt>BE*6 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door LH lock position switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door LH unlocked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2C-1 (LSWL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR*5 - Body ground</ptxt>
<ptxt>BE*6 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door LH lock position switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, all doors closed and rear door LH locked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-5 (LSWR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR*5 - Body ground</ptxt>
<ptxt>BE*6 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door RH lock position switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door RH unlocked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-5 (LSWR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR*5 - Body ground</ptxt>
<ptxt>BE*6 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear door RH lock position switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, all doors closed and rear door RH locked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-2 (LSWB) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door lock position switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door unlocked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E2-2 (LSWB) - Body ground</ptxt>
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door lock position switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, all doors closed and back door locked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-26 (BDSU) - Body ground</ptxt>
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door opener switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door lock opener switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-26 (BDSU) - Body ground</ptxt>
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door opener switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Back door lock opener switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
<ptxt>*3: w/ Entry and Start System</ptxt>
<ptxt>*4: w/o Entry and Start System</ptxt>
<ptxt>*5: for GRJ200L-GNANKC, URJ202L-GNTEKC</ptxt>
<ptxt>*6: except GRJ200L-GNANKC, URJ202L-GNTEKC</ptxt>
</atten4>
<ptxt>If the result is not as specified, the ECU may have a malfunction.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CHECK DOUBLE LOCK DOOR CONTROL RELAY (w/ Double Locking Function)</ptxt>
<figure>
<graphic graphicname="E123928E02" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the E86 double lock door control relay connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt> Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E86-1 (+B) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-7 (CPUB) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECU power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-14 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Reconnect the E86 double lock door control relay connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt> Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E86-4 (ACTR) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>All door double lock motor off signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door lock set → unset</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V → 11 to 14 V → Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-3 (ACTS) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>All door double lock motor on signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door lock unset → set</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V → 11 to 14 V → Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-5 (DLPD) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>BR*1 - Body ground</ptxt>
<ptxt>BE*2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock set</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-5 (DLPD) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>BR*1 - Body ground</ptxt>
<ptxt>BE*2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock unset</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-6 (DLPP) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>GR - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock set</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-6 (DLPP) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>GR - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock unset</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-11 (DLPR) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Y*1 - W-B</ptxt>
<ptxt>SB*2 - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock set</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-11 (DLPR) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Y*1 - W-B</ptxt>
<ptxt>SB*2 - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock unset</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-12 (DLPL) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LG - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock set</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E86-12 (DLPL) - E86-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LG - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double lock unset</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: for GRJ200L-GNANKC, URJ202L-GNTEKC</ptxt>
<ptxt>*2: except GRJ200L-GNANKC, URJ202L-GNTEKC</ptxt>
</atten4>
<ptxt>If the result is not as specified, the ECU may have a malfunction.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>