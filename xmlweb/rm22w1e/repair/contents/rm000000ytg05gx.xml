<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MU_T00FX" variety="T00FX">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV with DPF)</name>
<para id="RM000000YTG05GX" category="J" type-id="801A2" name-id="AT31E-04" from="201301" to="201308">
<dtccode/>
<dtcname>Pattern Select Switch 2nd Start Mode Circuit</dtcname>
<subpara id="RM000000YTG05GX_01" type-id="60" category="03" proc-id="RM22W0E___00008S100000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When 2nd mode is selected with the pattern select (2ND) switch, the ECM controls the solenoid valves and the transmission starts from 2nd gear.</ptxt>
<ptxt>The 2nd mode is the system that operates the throttle motor to control engine output to reduce skidding of the driving wheels, and guarantee takeoff acceleration, driving straightness and turning stability.</ptxt>
<ptxt>When the shift lever is in D, the transmission automatically shifts up through 3rd to 6th as usual.</ptxt>
</content5>
</subpara>
<subpara id="RM000000YTG05GX_02" type-id="32" category="03" proc-id="RM22W0E___00008S200000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to Pattern Select Switch Power Mode Circuit (See page <xref label="Seep01" href="RM000000XP2072X_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000YTG05GX_03" type-id="51" category="05" proc-id="RM22W0E___00008S300000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<ptxt>When the pattern select (2ND) switch is pushed, switch contact is made and 2nd mode is selected. To cancel 2nd mode, push the pattern select (2ND) switch once again.</ptxt>
<atten4>
<ptxt>2nd mode is automatically canceled when the engine switch is turned off.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000YTG05GX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YTG05GX_04_0001" proc-id="RM22W0E___00008S400000">
<testtitle>DRIVING TEST</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>The driving test should be performed on a paved road (nonskid road).</ptxt>
</atten4>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the 2ND switch off (normal drive mode).</ptxt>
</test1>
<test1>
<ptxt>Confirm vehicle response by driving from a parked position to fully depressing the accelerator pedal.</ptxt>
</test1>
<test1>
<ptxt>Turn the 2ND switch on and perform the same check as above.</ptxt>
<ptxt>When driving the vehicle with the 2ND switch on and off, check that there is a difference in acceleration.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There is a difference in acceleration.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YTG05GX_04_0008" fin="true">OK</down>
<right ref="RM000000YTG05GX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTG05GX_04_0008">
<testtitle>PROCEED TO NEXT CIRCUIT INSPECTION SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000W730X5X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTG05GX_04_0002" proc-id="RM22W0E___00008S500000">
<testtitle>INSPECT PATTERN SELECT (2ND) SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the suspension control switch (See page <xref label="Seep01" href="RM000003C4A00ZX"/>).</ptxt>
<figure>
<graphic graphicname="C238423E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>4 (2ND) - 16 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (2ND) switch continuously pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>4 (2ND) - 16 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (2ND) switch released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Suspension Control Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YTG05GX_04_0003" fin="false">OK</down>
<right ref="RM000000YTG05GX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTG05GX_04_0003" proc-id="RM22W0E___00008S600000">
<testtitle>CHECK HARNESS AND CONNECTOR (PATTERN SELECT (2ND) SWITCH - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the suspension control switch connector.</ptxt>
<figure>
<graphic graphicname="C227138E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>V4-16 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="right" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Suspension Control Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YTG05GX_04_0004" fin="false">OK</down>
<right ref="RM000000YTG05GX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTG05GX_04_0004" proc-id="RM22W0E___00008S700000">
<testtitle>CHECK HARNESS AND CONNECTOR (PATTERN SELECT (2ND) SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C197708E57" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A38-17 (SNWI) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (2ND) switch continuously pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-17 (SNWI) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (2ND) switch released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A52-17 (SNWI) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (2ND) switch continuously pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A52-17 (SNWI) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (2ND) switch released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000YTG05GX_04_0005" fin="true">OK</down>
<right ref="RM000000YTG05GX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTG05GX_04_0005">
<testtitle>PROCEED TO NEXT CIRCUIT INSPECTION SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000W730X5X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTG05GX_04_0006">
<testtitle>REPLACE PATTERN SELECT (2ND) SWITCH (SUSPENSION CONTROL SWITCH)<xref label="Seep01" href="RM000003C4A00ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTG05GX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YTG05GX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>