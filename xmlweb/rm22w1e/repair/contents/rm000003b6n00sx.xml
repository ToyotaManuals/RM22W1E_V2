<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000F" variety="S000F">
<name>1GR-FE FUEL</name>
<ttl id="12008_S000F_7C3GH_T009K" variety="T009K">
<name>FUEL SENDER GAUGE ASSEMBLY (for Fuel Sub Tank)</name>
<para id="RM000003B6N00SX" category="A" type-id="30014" name-id="FU94N-02" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000003B6N00SX_01" type-id="01" category="01">
<s-1 id="RM000003B6N00SX_01_0001" proc-id="RM22W0E___00005SO00001">
<ptxt>INSTALL FUEL SENDER GAUGE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the sender gauge.</ptxt>
</s2>
<s2>
<ptxt>Install the sender gauge with the 5 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>1.5</t-value1>
<t-value2>15</t-value2>
<t-value3>13</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the sender gauge connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00SX_01_0002" proc-id="RM22W0E___00005SP00001">
<ptxt>INSTALL REAR FLOOR SERVICE HOLE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the service hole cover with new butyl tape.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00SX_01_0005" proc-id="RM22W0E___00005SR00001">
<ptxt>INSTALL REAR NO. 7 AIR DUCT (w/ Rear Air Conditioning System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 clips to install the air duct.</ptxt>
<atten4>
<ptxt>Attach the clips in the order shown in the illustration.</ptxt>
</atten4>
<figure>
<graphic graphicname="A176392E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00SX_01_0003" proc-id="RM22W0E___00005SQ00001">
<ptxt>INSTALL REAR FLOOR MAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 5 clips to install the floor mat.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00SX_01_0025" proc-id="RM22W0E___00005SU00001">
<ptxt>INSTALL FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ Sliding Roof (See page <xref label="Seep01" href="RM0000038MM00UX_02_0020"/>)</ptxt>
</item>
<item>
<ptxt>w/o Sliding Roof (See page <xref label="Seep02" href="RM0000038MM00VX_02_0020"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM000003B6N00SX_01_0027" proc-id="RM22W0E___00005SV00001">
<ptxt>INSTALL FRONT QUARTER TRIM PANEL ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ Sliding Roof (See page <xref label="Seep01" href="RM0000038MM00UX_02_0022"/>)</ptxt>
</item>
<item>
<ptxt>w/o Sliding Roof (See page <xref label="Seep02" href="RM0000038MM00VX_02_0022"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM000003B6N00SX_01_0035" proc-id="RM22W0E___00005SW00001">
<ptxt>INSTALL REAR NO. 2 SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>for Face to Face Seat Type (See page <xref label="Seep01" href="RM00000311A003X_01_0001"/>)</ptxt>
</item>
<item>
<ptxt>except Face to Face Seat Type (See page <xref label="Seep02" href="RM00000391Q00YX_01_0010"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM000003B6N00SX_01_0036" proc-id="RM22W0E___00005SX00001">
<ptxt>INSTALL REAR NO. 1 SEAT ASSEMBLY RH (for 60/40 Split Seat Type 40 Side)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000391200SX_01_0004"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000003B6N00SX_01_0037" proc-id="RM22W0E___00005SY00001">
<ptxt>INSTALL REAR NO. 1 SEAT ASSEMBLY LH (for 60/40 Split Seat Type 60 Side)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000390X00SX_01_0004"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000003B6N00SX_01_0021" proc-id="RM22W0E___00005SS00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003B6N00SX_01_0022" proc-id="RM22W0E___00005ST00001">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000000XFD0KMX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>