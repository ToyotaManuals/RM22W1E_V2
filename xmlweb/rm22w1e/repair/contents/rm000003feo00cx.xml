<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LY_T00F1" variety="T00F1">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM000003FEO00CX" category="C" type-id="305GD" name-id="CC41S-01" from="201301" to="201308">
<dtccode>P1575</dtccode>
<dtcname>Warning Buzzer Malfunction</dtcname>
<subpara id="RM000003FEO00CX_01" type-id="60" category="03" proc-id="RM22W0E___00007CH00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P1575</ptxt>
</entry>
<entry valign="middle">
<ptxt>The vehicle speed is 50 km/h (30 mph) or more with the cruise control main switch on (vehicle-to-vehicle distance control mode) and the approach warning buzzer malfunctions.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Vehicle stability control system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003FEO00CX_02" type-id="51" category="05" proc-id="RM22W0E___00007CI00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>This DTC code may be output under the vehicle stability control system at the same time. In that case, perform troubleshooting for the vehicle stability control system first.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003FEO00CX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003FEO00CX_03_0001" proc-id="RM22W0E___00007CJ00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (BUZZER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, perform the Active Test (See page <xref label="Seep01" href="RM000002L7702ZX"/>).</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRAC</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Buzzer</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Skid control buzzer sounds</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF or ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Skid control buzzer sounds.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003FEO00CX_03_0002" fin="false">OK</down>
<right ref="RM000003FEO00CX_03_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003FEO00CX_03_0002" proc-id="RM22W0E___00007CK00000">
<testtitle>CHECK DTC (DYNAMIC RADAR CRUISE CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Powertrain / Radar Cruise / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002R6B01RX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the following to make sure the DTC detection conditions are met.</ptxt>
<atten4>
<ptxt>If the detection conditions are not met, the system cannot detect the malfunction.</ptxt>
</atten4>
<test2>
<ptxt>Drive the vehicle at a speed of 50 km/h (30 mph) or more.</ptxt>
</test2>
<test2>
<ptxt>Turn the cruise control main switch on to set the vehicle-to-vehicle distance control mode to the standby state.</ptxt>
</test2>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000002R6B01RX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC P1575 is not output.</ptxt>
</specitem>
</spec>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 1UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 3UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000003FEO00CX_03_0003" fin="true">A</down>
<right ref="RM000003FEO00CX_03_0005" fin="true">B</right>
<right ref="RM000003FEO00CX_03_0006" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003FEO00CX_03_0003">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FEO00CX_03_0004">
<testtitle>GO TO VEHICLE STABILITY CONTROL SYSTEM (Skid Control Buzzer Circuit)<xref label="Seep01" href="RM000001DXP02DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FEO00CX_03_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FEO00CX_03_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>