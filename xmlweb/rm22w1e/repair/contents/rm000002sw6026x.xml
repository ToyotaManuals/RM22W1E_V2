<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SD_T00LG" variety="T00LG">
<name>CAN COMMUNICATION SYSTEM (for RHD)</name>
<para id="RM000002SW6026X" category="C" type-id="804QF" name-id="NW48N-01" from="201301" to="201308">
<dtccode>U0125</dtccode>
<dtcname>Lost Communication with Yaw Rate Sensor Module</dtcname>
<dtccode>U0124</dtccode>
<dtcname>Lost Communication with Lateral Acceleration Sensor Module</dtcname>
<subpara id="RM000002SW6026X_01" type-id="60" category="03" proc-id="RM22W0E___0000DZK00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U0125</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is no communication from the yaw rate sensor assembly.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Power source circuit of yaw rate sensor assembly</ptxt>
</item>
<item>
<ptxt>Yaw rate sensor assembly CAN branch wire or connector</ptxt>
</item>
<item>
<ptxt>Yaw rate sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0124</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is no communication from the yaw rate sensor assembly (lateral acceleration sensor).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Power source circuit of yaw rate sensor assembly</ptxt>
</item>
<item>
<ptxt>Yaw rate sensor assembly CAN branch wire or connector</ptxt>
</item>
<item>
<ptxt>Yaw rate sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>For vehicles with a vehicle stability control system.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002SW6026X_02" type-id="32" category="03" proc-id="RM22W0E___0000DZL00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C176042E02" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002SW6026X_03" type-id="51" category="05" proc-id="RM22W0E___0000DZM00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Operating the ignition switch, any switches or any doors triggers related ECU and sensor communication with the CAN, which causes resistance variation.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002SW6026X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002SW6026X_04_0009" proc-id="RM22W0E___0000DZQ00000">
<testtitle>PRECAUTION</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content6>
<res>
<down ref="RM000002SW6026X_04_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002SW6026X_04_0008" proc-id="RM22W0E___0000DZP00000">
<testtitle>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal before measuring the resistances of the CAN main wire and the CAN branch wire.</ptxt>
<atten2>
<ptxt>For vehicles with an SRS system:</ptxt>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000002SW6026X_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002SW6026X_04_0002" proc-id="RM22W0E___0000DZN00000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (YAW RATE SENSOR ASSEMBLY CAN BRANCH WIRE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E50 yaw rate sensor assembly connector.</ptxt>
<figure>
<graphic graphicname="C227504E07" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E50-3 (CANH) - E50-2 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>54 to 69 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Yaw Rate Sensor Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002SW6026X_04_0003" fin="false">OK</down>
<right ref="RM000002SW6026X_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002SW6026X_04_0003" proc-id="RM22W0E___0000DZO00000">
<testtitle>CHECK HARNESS AND CONNECTOR (YAW RATE SENSOR ASSEMBLY - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
<figure>
<graphic graphicname="C227504E09" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E50-4 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E50-6 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E50-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Yaw Rate Sensor Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000002SW6026X_04_0007" fin="true">OK</down>
<right ref="RM000002SW6026X_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002SW6026X_04_0005">
<testtitle>REPAIR OR REPLACE YAW RATE SENSOR ASSEMBLY CAN BRANCH WIRE OR CONNECTOR (CANH, CANL)</testtitle>
</testgrp>
<testgrp id="RM000002SW6026X_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002SW6026X_04_0007">
<testtitle>REPLACE YAW RATE SENSOR ASSEMBLY<xref label="Seep01" href="RM000000SS505TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>