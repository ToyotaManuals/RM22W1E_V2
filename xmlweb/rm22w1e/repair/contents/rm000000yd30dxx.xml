<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T7_T00MA" variety="T00MA">
<name>ENTRY AND START SYSTEM (for Start Function)</name>
<para id="RM000000YD30DXX" category="C" type-id="800SQ" name-id="TD61J-01" from="201301">
<dtccode>B2285</dtccode>
<dtcname>Steering Lock Position Signal Circuit Malfunction</dtcname>
<dtccode>B2288</dtccode>
<dtcname>Steering Lock Signal Circuit Malfunction</dtcname>
<subpara id="RM000000YD30DXX_01" type-id="60" category="03" proc-id="RM22W0E___0000EIW00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The main body ECU and the steering lock ECU are connected by a cable and the LIN communication line. These DTCs are stored when: 1) signals, transmitted through the cable and the communication line, do not match; or 2) a malfunction is detected in the circuit between the main body ECU and the steering lock ECU.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<colspec colname="COL3" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2285</ptxt>
</entry>
<entry valign="middle">
<ptxt>The cable and LIN information between the main body ECU and steering lock ECU is inconsistent.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Main body ECU</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Steering lock actuator assembly (steering lock ECU)</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B2288</ptxt>
</entry>
<entry valign="middle">
<ptxt>After turning the engine switch from off to on (IG), the steering wheel does not unlock for a certain period of time (the ECU unlocks the steering wheel only when it receives an unlock signal from LIN communication and the cable).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Main body ECU</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Steering lock actuator assembly (steering lock ECU)</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000YD30DXX_02" type-id="32" category="03" proc-id="RM22W0E___0000EIX00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C165201E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000YD30DXX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000YD30DXX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YD30DXX_04_0001" proc-id="RM22W0E___0000EIY00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STEERING UNLOCK SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the steering lock is functioning properly.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.44in"/>
<colspec colname="COL2" colwidth="2.27in"/>
<colspec colname="COL3" colwidth="1.75in"/>
<colspec colname="COL4" colwidth="1.62in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Steering Unlock Switch</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Steering lock condition/ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Steering unlocked</ptxt>
<ptxt>OFF: Steering locked</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.94in"/>
<colspec colname="COL2" colwidth="1.19in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Display on the intelligent tester does not change according to steering lock position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Display on the intelligent tester changes according to steering lock position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YD30DXX_04_0002" fin="false">A</down>
<right ref="RM000000YD30DXX_04_0005" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000YD30DXX_04_0002" proc-id="RM22W0E___0000EIZ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - STEERING LOCK ECU)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C164812E16" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E1 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E26 steering lock ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E1-18 (SLP) - E26-4 (SLP1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-18 (SLP) or E26-4 (SLP1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YD30DXX_04_0003" fin="false">OK</down>
<right ref="RM000000YD30DXX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YD30DXX_04_0003" proc-id="RM22W0E___0000EJ000000">
<testtitle>REPLACE MAIN BODY ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the main body ECU with a new or normally functioning one.</ptxt>
</test1>
<test1>
<ptxt>Check whether DTC B2285 and B2288 are output.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.76in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC output (for Power tilt and telescopic)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC output (for Manual tilt and telescopic)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YD30DXX_04_0004" fin="true">A</down>
<right ref="RM000000YD30DXX_04_0007" fin="true">B</right>
<right ref="RM000000YD30DXX_04_0008" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000YD30DXX_04_0004">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000YD30DXX_04_0005">
<testtitle>REPLACE MAIN BODY ECU</testtitle>
</testgrp>
<testgrp id="RM000000YD30DXX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YD30DXX_04_0007">
<testtitle>REPLACE STEERING LOCK ACTUATOR ASSEMBLY (STEERING LOCK ECU)<xref label="Seep01" href="RM0000039SJ014X_01_0001"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YD30DXX_04_0008">
<testtitle>REPLACE STEERING LOCK ACTUATOR ASSEMBLY (STEERING LOCK ECU)<xref label="Seep01" href="RM000000MID008X_01_0055"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>