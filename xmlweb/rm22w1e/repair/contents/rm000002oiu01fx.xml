<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12037_S001R" variety="S001R">
<name>VARIABLE GEAR RATIO STEERING</name>
<ttl id="12037_S001R_7C3Q8_T00JB" variety="T00JB">
<name>VARIABLE GEAR RATIO STEERING SYSTEM</name>
<para id="RM000002OIU01FX" category="C" type-id="800VB" name-id="VG00D-06" from="201301">
<dtccode>U0100/56</dtccode>
<dtcname>Lost Communication with ECM / PCM "A"</dtcname>
<dtccode>U0122/56</dtccode>
<dtcname>Lost Communication with Vehicle Dynamics Control Module</dtcname>
<dtccode>U0126/56</dtccode>
<dtcname>Lost Communication with Steering Angle Sensor Module</dtcname>
<subpara id="RM000002OIU01FX_01" type-id="60" category="03" proc-id="RM22W0E___0000BA700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The steering control ECU receives signals from the skid control ECU, ECM and steering angle sensor via CAN communication. When DTCs indicating a CAN communication system malfunction are output, repair the CAN communication system first.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.22in"/>
<colspec colname="COL2" colwidth="3.36in"/>
<colspec colname="COL3" colwidth="2.50in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U0100/56</ptxt>
</entry>
<entry valign="middle">
<ptxt>The steering control ECU detects an error in reception (from ECM) via CAN communication.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0122/56</ptxt>
</entry>
<entry valign="middle">
<ptxt>The steering control ECU detects an error in reception (from skid control ECU) via CAN communication.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>Skid control ECU</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0126/56</ptxt>
</entry>
<entry valign="middle">
<ptxt>The steering control ECU detects an error in reception (from steering angle sensor) via CAN communication.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>Steering angle sensor</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002OIU01FX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002OIU01FX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002OIU01FX_03_0001" proc-id="RM22W0E___0000BA800000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, check if the CAN communication system is functioning normally (See page <xref label="Seep01" href="RM000001RSW03JX"/> for LHD, <xref label="Seep02" href="RM000001RSW03KX"/> for RHD).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.54in"/>
<colspec colname="COL2" colwidth="1.59in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002OIU01FX_03_0002" fin="true">A</down>
<right ref="RM000002OIU01FX_03_0003" fin="true">B</right>
<right ref="RM000002OIU01FX_03_0004" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002OIU01FX_03_0002">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002OIU01FX_03_0003">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002OIU01FX_03_0004">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>