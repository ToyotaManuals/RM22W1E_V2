<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001X" variety="S001X">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001X_7C3RY_T00L1" variety="T00L1">
<name>PARKING ASSIST MONITOR SYSTEM (w/ Side Monitor System)</name>
<para id="RM0000035DH03WX" category="C" type-id="80394" name-id="PM2AI-53" from="201301" to="201308">
<dtccode>C1626</dtccode>
<dtcname>Steering Angle Sensor Failure</dtcname>
<subpara id="RM0000035DH03WX_01" type-id="60" category="03" proc-id="RM22W0E___0000D0S00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored if the parking assist ECU receives a signal via CAN communication from the steering sensor that indicates an internal malfunction.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1626</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A fail flag is transmitted from the steering angle sensor.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Steering sensor</ptxt>
</item>
<item>
<ptxt>Parking assist ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000035DH03WX_02" type-id="51" category="05" proc-id="RM22W0E___0000D0T00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When "System initializing" is displayed on the parking assist ECU after the battery terminal disconnected, correct the steering angle neutral point (See page <xref label="Seep01" href="RM0000035DE03CX"/>).</ptxt>
</item>
<item>
<ptxt>Depending on the parts that are replaced or operations that are performed during vehicle inspection or maintenance, calibration of other systems as well as the parking assist monitor system (w/ Side Monitor System) may be needed (See page <xref label="Seep02" href="RM0000035DD03SX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM0000035DH03WX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000035DH03WX_03_0001" proc-id="RM22W0E___0000D0U00000">
<testtitle>REPLACE STEERING SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the steering sensor with a normally functioning one (See page <xref label="Seep01" href="RM000000SS907PX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000035DH03WX_03_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000035DH03WX_03_0003" proc-id="RM22W0E___0000D0V00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000035DB03WX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000035DB03WX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC C1626 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000035DH03WX_03_0002" fin="true">OK</down>
<right ref="RM0000035DH03WX_03_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000035DH03WX_03_0002">
<testtitle>END (STEERING SENSOR IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM0000035DH03WX_03_0004">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM0000039LH004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>