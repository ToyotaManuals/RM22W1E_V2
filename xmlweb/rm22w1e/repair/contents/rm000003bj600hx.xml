<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3ON_T00HQ" variety="T00HQ">
<name>HEIGHT CONTROL SENSOR (for Rear Side)</name>
<para id="RM000003BJ600HX" category="A" type-id="30014" name-id="SC0SW-07" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000003BJ600HX_01" type-id="01" category="01">
<s-1 id="RM000003BJ600HX_01_0001" proc-id="RM22W0E___00009XS00001">
<ptxt>INSTALL REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C172823" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Install the sensor with the 2 bolts and nut.</ptxt>
<torque>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
<subtitle>for nut</subtitle>
<torqueitem>
<t-value1>5.6</t-value1>
<t-value2>57</t-value2>
<t-value3>50</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector and 2 clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BJ600HX_01_0007" proc-id="RM22W0E___00009XW00001">
<ptxt>INSTALL REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C172822" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Install the sensor with the 2 bolts and nut.</ptxt>
<torque>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
<subtitle>for nut</subtitle>
<torqueitem>
<t-value1>5.6</t-value1>
<t-value2>57</t-value2>
<t-value3>50</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector and 2 clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BJ600HX_01_0002" proc-id="RM22W0E___00009XT00001">
<ptxt>INSTALL REAR WHEEL</ptxt>
<content1 releasenbr="1">
<torque>
<subtitle>for Aluminum Wheel</subtitle>
<torqueitem>
<t-value1>131</t-value1>
<t-value2>1336</t-value2>
<t-value4>97</t-value4>
</torqueitem>
<subtitle>for Steel Wheel</subtitle>
<torqueitem>
<t-value1>209</t-value1>
<t-value2>2131</t-value2>
<t-value4>154</t-value4>
</torqueitem>
</torque>
</content1>
</s-1>
<s-1 id="RM000003BJ600HX_01_0003" proc-id="RM22W0E___00009XU00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003BJ600HX_01_0004" proc-id="RM22W0E___00009XV00001">
<ptxt>PERFORM VEHICLE HEIGHT OFFSET CALIBRATION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Perform the vehicle height offset calibration (See page <xref label="Seep01" href="RM000003AG300EX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BJ600HX_01_0009" proc-id="RM22W0E___00009XN00000">
<ptxt>ADJUST REAR HEIGHT SENSOR LINK SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make adjustments from the link that deviates the most from the specified vehicle height value.</ptxt>
</item>
<item>
<ptxt>When the front and rear are at the same level, make adjustments from the front first.</ptxt>
</item>
<item>
<ptxt>If adjustment cannot be completed through the vehicle height offset calibration, adjust the sensor link using the following procedure.</ptxt>
</item>
</list1>
</atten3>
<s2>
<figure>
<graphic graphicname="C176369" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Loosen the nut and adjust the link installation position by moving the height control sensor link up or down in the long hole of the bracket.</ptxt>
<atten4>
<ptxt>When the link is moved 1 mm (0.0394 in.), the vehicle height changes by approximately 2 mm (0.0787 in.).</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Tighten the nut of the height control sensor link.</ptxt>
<torque>
<torqueitem>
<t-value1>5.6</t-value1>
<t-value2>57</t-value2>
<t-value3>50</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003BJ600HX_01_0010" proc-id="RM22W0E___00009XY00001">
<ptxt>PERFORM ZERO POINT CALIBRATION OF G SENSOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Perform the zero point calibration of G sensor (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BJ600HX_01_0008" proc-id="RM22W0E___00009XX00001">
<ptxt>ADJUST HEADLIGHT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Standard:</ptxt>
<ptxt>Adjust the headlight (See page <xref label="Seep01" href="RM0000011MI096X"/>).</ptxt>
</s2>
<s2>
<ptxt>for HID Headlight:</ptxt>
<ptxt>Adjust the headlight (See page <xref label="Seep02" href="RM0000011MI095X"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>