<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001Q" variety="S001Q">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001Q_7C3Q4_T00J7" variety="T00J7">
<name>VANE PUMP (for 1VD-FTV with DPF)</name>
<para id="RM000003DKU008X" category="A" type-id="30014" name-id="PA1H0-02" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000003DKU008X_01" type-id="01" category="01">
<s-1 id="RM000003DKU008X_01_0001" proc-id="RM22W0E___0000B3Q00001">
<ptxt>INSTALL VANE PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat a new O-ring with power steering fluid and install it to the vane pump.</ptxt>
</s2>
<s2>
<ptxt>Install the vane pump with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003DKU008X_01_0017" proc-id="RM22W0E___0000B3S00001">
<ptxt>CONNECT PRESSURE FEED TUBE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the pressure feed tube.</ptxt>
</s2>
<s2>
<ptxt>Install the pressure feed tube and union with the union bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>50</t-value1>
<t-value2>510</t-value2>
<t-value4>37</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Make sure that the stopper of the tube touches the vane pump body as shown in the illustration before tightening the bolt.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000003DKU008X_01_0018" proc-id="RM22W0E___0000B3T00001">
<ptxt>CONNECT POWER STEERING OIL PRESSURE SWITCH CONNECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 2 connectors.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003DKU008X_01_0019" proc-id="RM22W0E___0000B3U00001">
<ptxt>CONNECT NO. 1 OIL RESERVOIR TO PUMP HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the pump hose to the vane pump with the clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003DKU008X_01_0002" proc-id="RM22W0E___000033K00000">
<ptxt>INSTALL V-RIBBED BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the V-ribbed belt as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A174851" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Temporarily install the lock nut, and turn the bolt clockwise.</ptxt>
<figure>
<graphic graphicname="A174848" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Using a belt tension gauge, inspect the belt tension.</ptxt>
<figure>
<graphic graphicname="A271744E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Belt Tension</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New belt</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 to 35°C (41 to 95°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>550 to 800 N (56 to 82 kgf, 123.6 to 179.8 lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Used belt</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 to 35°C (41 to 95°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>300 to 500 N (31 to 51 kgf, 67.4 to 112.4 lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Measuring Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When measuring the tension of a new belt, measure the tension immediately after installing it to the engine but before starting the engine.</ptxt>
</item>
<item>
<ptxt>A "new belt" is a belt which has been used for less than 5 minutes on a running engine.</ptxt>
</item>
<item>
<ptxt>A "used belt" is a belt which has been used on a running engine for 5 minutes or more.</ptxt>
</item>
<item>
<ptxt>After installing a new belt, run the engine for approximately 5 minutes and then recheck the tension.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Tighten the nut.</ptxt>
<figure>
<graphic graphicname="A181677" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Check that the belt fits properly in the ribbed grooves.</ptxt>
<figure>
<graphic graphicname="A185945E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Check with your hand to confirm that the belt has not slipped out of the groove on the bottom of the pulley.</ptxt>
<ptxt>If it has slipped out, replace the V-ribbed belt. Install a new V-ribbed belt.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0003" proc-id="RM22W0E___000033I00000">
<ptxt>INSTALL NO. 3 IDLER PULLEY (w/ Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the No. 3 idler pulley bracket knock pin and No. 1 idler pulley bracket knock pin hole and install the No. 3 idler pulley with the nut.</ptxt>
<figure>
<graphic graphicname="A177431" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>88</t-value1>
<t-value2>898</t-value2>
<t-value4>64</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0004" proc-id="RM22W0E___000033J00000">
<ptxt>INSTALL NO. 1 IDLER PULLEY (w/ Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the collar, No. 1 idler pulley and cover with the bolt.</ptxt>
<figure>
<graphic graphicname="A177430" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>49</t-value1>
<t-value2>495</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0005" proc-id="RM22W0E___000033K00000">
<ptxt>INSTALL V-RIBBED BELT (w/ Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the V-ribbed belt as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A174851" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Temporarily install the lock nut, and turn the bolt clockwise.</ptxt>
<figure>
<graphic graphicname="A174848" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Using a belt tension gauge, inspect the belt tension.</ptxt>
<figure>
<graphic graphicname="A271744E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Belt Tension</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New belt</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 to 35°C (41 to 95°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>550 to 800 N (56 to 82 kgf, 123.6 to 179.8 lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Used belt</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 to 35°C (41 to 95°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>300 to 500 N (31 to 51 kgf, 67.4 to 112.4 lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Measuring Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When measuring the tension of a new belt, measure the tension immediately after installing it to the engine but before starting the engine.</ptxt>
</item>
<item>
<ptxt>A "new belt" is a belt which has been used for less than 5 minutes on a running engine.</ptxt>
</item>
<item>
<ptxt>A "used belt" is a belt which has been used on a running engine for 5 minutes or more.</ptxt>
</item>
<item>
<ptxt>After installing a new belt, run the engine for approximately 5 minutes and then recheck the tension.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Tighten the nut.</ptxt>
<figure>
<graphic graphicname="A181677" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Check that the belt fits properly in the ribbed grooves.</ptxt>
<figure>
<graphic graphicname="A185945E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Check with your hand to confirm that the belt has not slipped out of the groove on the bottom of the pulley.</ptxt>
<ptxt>If it has slipped out, replace the V-ribbed belt. Install a new V-ribbed belt.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0006" proc-id="RM22W0E___000033N00001">
<ptxt>INSTALL RADIATOR RESERVOIR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radiator reservoir with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A174738E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the 2 hoses to the upper radiator tank and water inlet.</ptxt>
<atten4>
<ptxt>Make sure the directions of the hose clamps are as shown in the illustration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0007" proc-id="RM22W0E___000033E00001">
<ptxt>INSTALL INTAKE AIR CONNECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the intake air connector to the No. 1 and No. 2 air cleaner pipes.</ptxt>
</s2>
<s2>
<ptxt>Install the connector with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the 2 hose clamps.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 3 wire harness clamps.</ptxt>
</s2>
<s2>
<ptxt>w/o Viscous Heater:</ptxt>
<ptxt>Connect the connector to the water temperature sensor.</ptxt>
</s2>
<s2>
<ptxt>w/ Viscous Heater:</ptxt>
<ptxt>Connect the 2 connectors to the water temperature sensor and viscous with magnet clutch heater.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0008" proc-id="RM22W0E___000033F00001">
<ptxt>TEMPORARILY INSTALL NO. 1 AIR CLEANER HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the air cleaner hose to the intake air connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0009" proc-id="RM22W0E___000033G00001">
<ptxt>INSTALL AIR CLEANER CAP SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the air cleaner cap to the air cleaner hose, and install the air cleaner cap with the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Connect the mass air flow meter connector and attach the wire harness clamp to the air cleaner cap.</ptxt>
</s2>
<s2>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Align the protrusion of the air cleaner cap and the concave portion of the air cleaner hose.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 2 hose clamps.</ptxt>
<torque>
<torqueitem>
<t-value1>2.5</t-value1>
<t-value2>25</t-value2>
<t-value3>22</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0010" proc-id="RM22W0E___000032100000">
<ptxt>INSTALL NO. 1 ENGINE COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the engine cover with the 2 nuts.</ptxt>
<figure>
<graphic graphicname="A174701" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0011" proc-id="RM22W0E___000054P00000">
<ptxt>INSTALL UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the upper radiator support seal with the 7 clips.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0012" proc-id="RM22W0E___000053O00001">
<ptxt>INSTALL FRONT FENDER APRON TRIM PACKING A
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front fender apron seal front RH with the 3 clips.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003DKU008X_01_0013">
<ptxt>ADD POWER STEERING FLUID</ptxt>
</s-1>
<s-1 id="RM000003DKU008X_01_0014">
<ptxt>BLEED POWER STEERING FLUID</ptxt>
</s-1>
<s-1 id="RM000003DKU008X_01_0015">
<ptxt>INSPECT FOR POWER STEERING FLUID LEAK</ptxt>
</s-1>
<s-1 id="RM000003DKU008X_01_0016" proc-id="RM22W0E___0000B3R00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>