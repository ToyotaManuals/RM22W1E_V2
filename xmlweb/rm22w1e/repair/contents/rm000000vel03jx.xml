<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3QV_T00JY" variety="T00JY">
<name>INSTRUMENT PANEL SPEAKER</name>
<para id="RM000000VEL03JX" category="A" type-id="30014" name-id="AVF6U-01" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000000VEL03JX_02" type-id="11" category="10" proc-id="RM22W0E___0000BQH00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the LH side and RH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0DSX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000000VEL03JX_01" type-id="01" category="01">
<s-1 id="RM000000VEL03JX_01_0005" proc-id="RM22W0E___0000BQE00001">
<ptxt>INSTALL FRONT NO. 2 SPEAKER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 9 Speakers Models:</ptxt>
<figure>
<graphic graphicname="B192166" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Connect the connector.</ptxt>
</s3>
<s3>
<ptxt>Temporarily install the speaker by aligning the positioning pins of the speaker with the instrument panel.</ptxt>
</s3>
<s3>
<ptxt>Install the speaker with the 2 bolts.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not touch the cone part of the speaker.</ptxt>
</item>
<item>
<ptxt>When installing the speaker to the instrument panel be careful that the wires do not get caught between the parts.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
<s2>
<ptxt>except 9 Speakers Models:</ptxt>
<figure>
<graphic graphicname="B192167E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Connect the connector.</ptxt>
</s3>
<s3>
<ptxt>Temporarily install the speaker by aligning the positioning pins of the speaker with the instrument panel.</ptxt>
</s3>
<s3>
<ptxt>Install the speaker with the 2 bolts.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not touch the cone part of the speaker.</ptxt>
</item>
<item>
<ptxt>When installing the speaker to the instrument panel be careful that the wires do not get caught between the parts.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000VEL03JX_01_0006" proc-id="RM22W0E___0000BQF00001">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL SPEAKER PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181531" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000VEL03JX_01_0014" proc-id="RM22W0E___0000BQ400001">
<ptxt>INSTALL FRONT PILLAR GARNISH LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185447" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>for 9, 14 Speakers:</ptxt>
<ptxt>Connect the speaker connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the clip and 3 guides to install the front pillar garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEL03JX_01_0018" proc-id="RM22W0E___0000BQ500001">
<ptxt>INSTALL FRONT ASSIST GRIP SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181685" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the front assist grip on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the front assist grip.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B181684" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 4 claws to install the 2 assist grip plugs.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEL03JX_01_0001" proc-id="RM22W0E___0000BQD00001">
<ptxt>INSTALL FRONT NO. 4 SPEAKER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the speaker connector.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install the speaker by aligning the positioning pins of the speaker with the instrument panel.</ptxt>
</s2>
<s2>
<ptxt>Install the speaker with the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B186849" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not touch the cone part of the speaker.</ptxt>
</item>
<item>
<ptxt>When installing the speaker to the instrument panel be careful that the wires do not get caught between the parts.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000VEL03JX_01_0002" proc-id="RM22W0E___0000BMN00001">
<ptxt>INSTALL NO. 1 SPEAKER OPENING COVER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B183763" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 8 claws to install the opening cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000VEL03JX_01_0016" proc-id="RM22W0E___0000BQG00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>