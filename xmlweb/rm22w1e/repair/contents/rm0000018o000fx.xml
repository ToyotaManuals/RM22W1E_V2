<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001Q" variety="S001Q">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001Q_7C3Q0_T00J3" variety="T00J3">
<name>VANE PUMP (for 1VD-FTV without DPF)</name>
<para id="RM0000018O000FX" category="A" type-id="80002" name-id="PA01A-01" from="201301">
<name>DISASSEMBLY</name>
<subpara id="RM0000018O000FX_02" type-id="11" category="10" proc-id="RM22W0E___0000B1500000">
<content3 releasenbr="1">
<atten3>
<ptxt>When using a vise, do not overtighten it.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM0000018O000FX_01" type-id="01" category="01">
<s-1 id="RM0000018O000FX_01_0003" proc-id="RM22W0E___0000B0W00000">
<ptxt>REMOVE POWER STEERING SUCTION PORT UNION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and suction port union from the vane pump.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the O-ring from the suction port union.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018O000FX_01_0004" proc-id="RM22W0E___0000B0X00000">
<ptxt>REMOVE FLOW CONTROL VALVE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C162100" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the pressure port union.</ptxt>
</s2>
<s2>
<ptxt>Remove the O-ring from the pressure port union.</ptxt>
</s2>
<s2>
<ptxt>Remove the flow control valve and compression spring.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018O000FX_01_0009" proc-id="RM22W0E___0000B1200000">
<ptxt>REMOVE VANE PUMP GEAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using soft jaws on a vise, clamp the vane pump.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vane pump.</ptxt>
</atten3>
<figure>
<graphic graphicname="C162114E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, remove the gear.</ptxt>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03011</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018O000FX_01_0005" proc-id="RM22W0E___0000B0Y00000">
<ptxt>REMOVE REAR VANE PUMP HOUSING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and rear housing from the front housing.</ptxt>
<figure>
<graphic graphicname="C162098" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the O-ring from the rear housing.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<figure>
<graphic graphicname="C162099E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018O000FX_01_0006" proc-id="RM22W0E___0000B0Z00000">
<ptxt>REMOVE VANE PUMP ROTOR, 10 VANE PUMP PLATES AND VANE PUMP CAM RING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 vane pump plates.</ptxt>
<atten3>
<ptxt>Take care not to drop the vane pump plates.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the rotor.</ptxt>
</s2>
<s2>
<ptxt>Remove the cam ring from the front housing.   </ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018O000FX_01_0007" proc-id="RM22W0E___0000B1000000">
<ptxt>REMOVE VANE PUMP FRONT SIDE PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front side plate from the front housing.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the O-ring from the front side plate. </ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<figure>
<graphic graphicname="C162121E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the front housing.</ptxt>
<figure>
<graphic graphicname="C162123" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018O000FX_01_0008" proc-id="RM22W0E___0000B1100000">
<ptxt>REMOVE VANE PUMP SHAFT WITH VANE PUMP SHAFT BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, remove the snap ring.</ptxt>
<figure>
<graphic graphicname="C161392E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to damage the front housing.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Tape the serrated part of the vane pump shaft.</ptxt>
<figure>
<graphic graphicname="C164630E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a press, press out the shaft with bearing.</ptxt>
<figure>
<graphic graphicname="C161393" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018O000FX_01_0010" proc-id="RM22W0E___0000B1300000">
<ptxt>REMOVE VANE PUMP SHAFT BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using 2 screwdrivers, remove the snap ring from the shaft.</ptxt>
<atten3>
<ptxt>Be careful not to damage the shaft.</ptxt>
</atten3>
<figure>
<graphic graphicname="C162115" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a press, press out the bearing from the shaft.</ptxt>
<figure>
<graphic graphicname="C162117" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018O000FX_01_0011" proc-id="RM22W0E___0000B1400000">
<ptxt>REMOVE VANE PUMP HOUSING OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, pry out the oil seal.</ptxt>
<figure>
<graphic graphicname="C161396" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to damage the front housing.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>