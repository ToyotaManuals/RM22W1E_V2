<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MR_T00FU" variety="T00FU">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 3UR-FE)</name>
<para id="RM000000W8A0J1X" category="C" type-id="302GI" name-id="AT9MC-01" from="201301" to="201308">
<dtccode>P2716</dtccode>
<dtcname>Pressure Control Solenoid "D" Electrical (Shift Solenoid Valve SLT)</dtcname>
<subpara id="RM000000W8A0J1X_01" type-id="60" category="03" proc-id="RM22W0E___00008DO00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2714 (See page <xref label="Seep01" href="RM000000W830JVX_09"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.19in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2716</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short is detected in the shift solenoid valve SLT circuit for 1 second or more while driving (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in shift solenoid valve SLT circuit</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve SLT</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W8A0J1X_02" type-id="64" category="03" proc-id="RM22W0E___00008DP00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When an open or short in the shift solenoid valve SLT circuit is detected, the ECM interprets this as a fault. The ECM will turn on the MIL and store the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W8A0J1X_07" type-id="32" category="03" proc-id="RM22W0E___00008DQ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C219459E07" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W8A0J1X_08" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000W8A0J1X_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W8A0J1X_09_0001" proc-id="RM22W0E___00008DR00000">
<testtitle>INSPECT NO. 1 TRANSMISSION WIRE (SHIFT SOLENOID VALVE SLT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 transmission wire connector.</ptxt>
<figure>
<graphic graphicname="C214326E38" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>13 (SLT+) - 5 (SLT-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>13 (SLT+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>5 (SLT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(No. 1 Transmission Wire)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W8A0J1X_09_0002" fin="false">OK</down>
<right ref="RM000000W8A0J1X_09_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8A0J1X_09_0002" proc-id="RM22W0E___00008DS00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 TRANSMISSION WIRE - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="C219456E26" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>C45-9 (SLT+) - C45-8 (SLT-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>C45-9 (SLT+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>C45-8 (SLT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W8A0J1X_09_0005" fin="true">OK</down>
<right ref="RM000000W8A0J1X_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8A0J1X_09_0008" proc-id="RM22W0E___00008CG00000">
<testtitle>INSPECT SHIFT SOLENOID VALVE SLT
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove shift solenoid valve SLT.</ptxt>
<figure>
<graphic graphicname="C209928E06" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F) </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Apply 12 V battery voltage to the shift solenoid valve and check that the valve moves and makes an operating noise.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery positive (+) with a 21 W bulb → Terminal 2</ptxt>
</item>
<item>
<ptxt>Battery negative (-) → Terminal 1</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Valve moves and makes an operating noise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Shift Solenoid Valve SLT)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6><res>
<down ref="RM000000W8A0J1X_09_0004" fin="true">OK</down>
<right ref="RM000000W8A0J1X_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8A0J1X_09_0004">
<testtitle>REPAIR OR REPLACE NO. 1 TRANSMISSION WIRE<xref label="Seep01" href="RM0000013C104UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8A0J1X_09_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8A0J1X_09_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W8A0J1X_09_0007">
<testtitle>REPLACE SHIFT SOLENOID VALVE SLT<xref label="Seep01" href="RM000000O9L06JX_02_0004"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>