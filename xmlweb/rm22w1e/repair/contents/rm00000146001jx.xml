<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000U" variety="S000U">
<name>1GR-FE COOLING</name>
<ttl id="12011_S000U_7C3JR_T00CU" variety="T00CU">
<name>WATER PUMP</name>
<para id="RM00000146001JX" category="A" type-id="80001" name-id="CO6C1-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM00000146001JX_01" type-id="01" category="01">
<s-1 id="RM00000146001JX_01_0019" proc-id="RM22W0E___00006UC00000">
<ptxt>REMOVE GENERATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000001ONP03ZX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM00000146001JX_01_0040" proc-id="RM22W0E___00006UD00000">
<ptxt>REMOVE COOLER COMPRESSOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000180105BX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM00000146001JX_01_0066" proc-id="RM22W0E___000011Z00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146001JX_01_0067" proc-id="RM22W0E___000011Y00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146001JX_01_0060" proc-id="RM22W0E___000011X00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts and No. 1 engine under cover.</ptxt>
<figure>
<graphic graphicname="A178461" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000146001JX_01_0051" proc-id="RM22W0E___000011W00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<figure>
<graphic graphicname="A274035E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Water Drain Cock Plug</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>for Type A:</ptxt>
<ptxt>Loosen the radiator drain cock plug and 2 cylinder block water drain cock plugs.</ptxt>
</s2>
<s2>
<ptxt>for Type B:</ptxt>
<ptxt>Loosen the radiator drain cock plug and cylinder block water drain cock plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the radiator cap. Then drain the coolant.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>for Type A:</ptxt>
<ptxt>Tighten the 2 cylinder block water drain cock plugs.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>for Type B:</ptxt>
<ptxt>Tighten the cylinder block water drain cock plug.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146001JX_01_0068" proc-id="RM22W0E___00000Z800000">
<ptxt>REMOVE V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Raise the front of the V-bank cover to detach the 2 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<figure>
<graphic graphicname="A267633E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000146001JX_01_0053">
<ptxt>REMOVE NO. 1 RADIATOR HOSE</ptxt>
</s-1>
<s-1 id="RM00000146001JX_01_0054" proc-id="RM22W0E___00004A800000">
<ptxt>REMOVE NO. 2 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A273588" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the clamp and remove the No. 2 radiator hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146001JX_01_0057" proc-id="RM22W0E___000049L00000">
<ptxt>DISCONNECT OIL COOLER TUBE (w/ Air Cooled Transmission Oil Cooler)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw to open the flexible hose clamp, and then remove the 2 bolts to disconnect the oil cooler tube from the fan shroud.</ptxt>
<figure>
<graphic graphicname="A266158" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000146001JX_01_0058" proc-id="RM22W0E___000049M00000">
<ptxt>REMOVE FAN SHROUD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 4 nuts holding the fluid coupling fan.</ptxt>
</s2>
<s2>
<ptxt>Remove the fan and generator V-belt (See page <xref label="Seep01" href="RM0000017LA01GX"/>).</ptxt>
</s2>
<s2>
<ptxt>Disconnect the reservoir hose from the upper side of the radiator tank.</ptxt>
<figure>
<graphic graphicname="A273614" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts holding the fan shroud.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 nuts of the fluid coupling fan, and then remove the shroud together with the coupling fan.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the fan pulley from the water pump.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146001JX_01_0042" proc-id="RM22W0E___00006UE00000">
<ptxt>DISCONNECT AIR TUBE ASSEMBLY (w/ Secondary Air Injection System)</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223014" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and disconnect the air tube.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146001JX_01_0043" proc-id="RM22W0E___00004CF00000">
<ptxt>REMOVE WATER INLET HOUSING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the throttle body connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 5 water by-pass hoses.</ptxt>
<figure>
<graphic graphicname="A273503" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the oil cooler hose.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 2 oil cooler hose.</ptxt>
</s2>
<s2>
<ptxt>Remove the 5 bolts and water inlet.</ptxt>
<figure>
<graphic graphicname="A223012" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the water outlet pipe.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the water pump.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146001JX_01_0010" proc-id="RM22W0E___00004BQ00000">
<ptxt>REMOVE NO. 2 IDLER PULLEY SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223006" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and 2 No. 2 idler pulleys.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146001JX_01_0041" proc-id="RM22W0E___000049V00000">
<ptxt>DISCONNECT VANE PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A270981" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223015" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and disconnect the vane pump.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146001JX_01_0062" proc-id="RM22W0E___00004BM00000">
<ptxt>REMOVE V-RIBBED BELT TENSIONER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and V-ribbed belt tensioner.</ptxt>
<figure>
<graphic graphicname="A222180" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000146001JX_01_0014" proc-id="RM22W0E___00006UB00000">
<ptxt>REMOVE ENGINE WATER PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 17 bolts, engine water pump and gasket.</ptxt>
<figure>
<graphic graphicname="A223007" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>