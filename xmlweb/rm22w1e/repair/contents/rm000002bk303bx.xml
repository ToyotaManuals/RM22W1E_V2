<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000A" variety="S000A">
<name>1GR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000A_7C3EU_T007X" variety="T007X">
<name>CYLINDER BLOCK</name>
<para id="RM000002BK303BX" category="G" type-id="3001K" name-id="EMBT0-01" from="201301">
<name>INSPECTION</name>
<subpara id="RM000002BK303BX_01" type-id="01" category="01">
<s-1 id="RM000002BK303BX_01_0055" proc-id="RM22W0E___000045D00000">
<ptxt>CLEAN CYLINDER BLOCK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a gasket scraper, remove all the gasket material from the top surface of the cylinder block.</ptxt>
</s2>
<s2>
<ptxt>Using a soft brush and solvent, thoroughly clean the cylinder block.</ptxt>
<atten3>
<ptxt>If the cylinder is washed at high temperatures, the cylinder liner sticks out beyond the cylinder block. Always wash the cylinder block at a temperature of 45°C (113°F) or less.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0032" proc-id="RM22W0E___000045100000">
<ptxt>INSPECT CYLINDER BLOCK FOR WARPAGE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a precision straightedge and feeler gauge, measure the warpage of the surfaces which contact the cylinder head gaskets.</ptxt>
<figure>
<graphic graphicname="A221393" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Maximum warpage</title>
<specitem>
<ptxt>0.05 mm (0.00197 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the warpage is more than the maximum, replace the cylinder block.</ptxt>
</s2>
<s2>
<ptxt>Visually check the cylinder for vertical scratches. If deep scratches are present, rebore all 6 cylinders.</ptxt>
<ptxt>If necessary, replace the cylinder block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0033" proc-id="RM22W0E___000045200000">
<ptxt>INSPECT CYLINDER BORE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a cylinder gauge, measure the cylinder bore diameter at positions A and B in the thrust and axial directions.</ptxt>
<figure>
<graphic graphicname="A221394E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>94.000 to 94.012 mm (3.7008 to 3.7013 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum diameter</title>
<specitem>
<ptxt>94.132 mm (3.7060 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Measurement position (A)</title>
<specitem>
<ptxt>10 mm (0.394 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Axial Direction</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Thrust Direction</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Engine Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the diameter is more than the maximum, replace the cylinder block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0040" proc-id="RM22W0E___000045600000">
<ptxt>INSPECT RING GROOVE CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a feeler gauge, measure the clearance between a new piston ring and the wall of the ring groove.</ptxt>
<spec>
<title>Standard Ring Groove Clearance</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No. 1 compression ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.02 to 0.07 mm (0.000787 to 0.00276 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No. 2 compression ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.02 to 0.06 mm (0.000787 to 0.00236 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Oil ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.07 to 0.15 mm (0.00276 to 0.00590 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the clearance is not as specified, replace the piston with pin sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0041" proc-id="RM22W0E___000045700000">
<ptxt>INSPECT PISTON RING END GAP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Insert the piston ring into the cylinder bore.</ptxt>
</s2>
<s2>
<ptxt>Using a piston, push the piston ring a little beyond the bottom of the ring travel, 110 mm (4.33 in.) from the top of the cylinder block.</ptxt>
</s2>
<s2>
<ptxt>Using a feeler gauge, measure the end gap.</ptxt>
<spec>
<title>Standard End Gap</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No. 1 compression ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.22 to 0.32 mm (0.00866 to 0.0126 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No. 2 compression ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.35 to 0.45 mm (0.0138 to 0.0177 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Oil ring (side rail)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.10 to 0.40 mm (0.00394 to 0.0157 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Maximum End Gap</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No. 1 compression ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.0 mm (0.0394 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No. 2 compression ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.1 mm (0.0433 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Oil ring (side rail)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.0 mm (0.0394 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the end gap is more than the maximum, replace the piston ring. If the end gap is more than the maximum even with a new piston ring, rebore all 6 cylinders or replace the cylinder block sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0054" proc-id="RM22W0E___000045C00000">
<ptxt>CLEAN PISTON WITH PIN SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the piston.</ptxt>
<s3>
<ptxt>Using a gasket scraper, remove the carbon from the piston top.</ptxt>
</s3>
<s3>
<ptxt>Using a groove cleaning tool or broken ring, clean the piston ring grooves.</ptxt>
</s3>
<s3>
<ptxt>Using solvent and a brush, thoroughly clean the piston.</ptxt>
<atten3>
<ptxt>Do not use a wire brush.</ptxt>
</atten3>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0035" proc-id="RM22W0E___000045300000">
<ptxt>INSPECT PISTON OIL CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a micrometer, measure the piston diameter at a position that is 9.6 mm (0.378 in.) from the bottom of the piston (refer to the illustration).</ptxt>
<figure>
<graphic graphicname="A221395E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Piston diameter</title>
<specitem>
<ptxt>93.961 to 93.991 mm (3.6992 to 3.7004 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Subtract the piston diameter measurement from the cylinder bore diameter measurement.</ptxt>
<spec>
<title>Standard oil clearance</title>
<specitem>
<ptxt>0.009 to 0.051 mm (0.000354 to 0.00201 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt>0.110 mm (0.00433 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the oil clearance is more than the maximum, replace all 6 pistons. If necessary, replace the cylinder block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0037" proc-id="RM22W0E___000045500000">
<ptxt>INSPECT PISTON PIN OIL CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check each mark on the piston and connecting rod.</ptxt>
<figure>
<graphic graphicname="A221396E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position Pin Hole Inside Diameter Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connecting Rod Bush Inside Diameter Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a caliper gauge, measure the inside diameter of the piston pin hole.</ptxt>
<figure>
<graphic graphicname="A221397" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Piston Pin Hole Inside Diameter</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Mark A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>22.001 to 22.004 mm (0.86618 to 0.86630 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mark B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>22.005 to 22.007 mm (0.86634 to 0.86642 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mark C</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>22.008 to 22.010 mm (0.86645 to 0.86653 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Using a micrometer, measure the piston pin diameter.</ptxt>
<figure>
<graphic graphicname="A146331E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Measurement Position</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Piston Pin Position</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>a</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>28 mm (1.102 in.) from edge</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>b</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 mm (0.197 in.) from edge</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Piston Pin Diameter</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Mark A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>21.997 to 22.000 mm (0.86602 to 0.86614 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mark B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>22.001 to 22.003 mm (0.86618 to 0.86626 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mark C</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>22.004 to 22.006 mm (0.86630 to 0.86642 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Using a caliper gauge, measure the inside diameter of the connecting rod bush.</ptxt>
<spec>
<title>Standard Bush Inside Diameter</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Mark A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>22.005 to 22.008 mm (0.86634 to 0.86645 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mark B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>22.009 to 22.011 mm (0.86649 to 0.86657 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mark C</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>22.012 to 22.014 mm (0.86661 to 0.86669 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Subtract the piston pin diameter measurement from the piston pin hole diameter measurement.</ptxt>
<spec>
<title>Standard oil clearance</title>
<specitem>
<ptxt>0.001 to 0.007 mm (0.0000394 to 0.000276 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt>0.040 mm (0.00157 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the oil clearance is more than the maximum, replace the piston with pin sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Subtract the piston pin diameter measurement from the bush inside diameter measurement.</ptxt>
<spec>
<title>Standard oil clearance</title>
<specitem>
<ptxt>0.005 to 0.011 mm (0.000197 to 0.000433 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt>0.050 mm (0.00197 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the oil clearance is more than the maximum, replace the connecting rod sub-assembly. If necessary, replace the piston with pin sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0036" proc-id="RM22W0E___000045400000">
<ptxt>INSPECT CONNECTING ROD SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a rod aligner and feeler gauge, check the connecting rod alignment.</ptxt>
<figure>
<graphic graphicname="A132171" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Check for bend.</ptxt>
<spec>
<title>Maximum bend</title>
<specitem>
<ptxt>0.05 mm (0.00197 in.) per 100 mm (3.94 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the bend is more than the maximum, replace the connecting rod sub-assembly.</ptxt>
</s3>
<s3>
<ptxt>Check for twist.</ptxt>
<figure>
<graphic graphicname="A132172" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum twist</title>
<specitem>
<ptxt>0.15 mm (0.00591 in.) per 100 mm (3.94 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the twist is more than the maximum, replace the connecting rod sub-assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0044" proc-id="RM22W0E___000045A00000">
<ptxt>INSPECT CRANKSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the runout at the center journal.</ptxt>
<spec>
<title>Maximum circle runout</title>
<specitem>
<ptxt>0.06 mm (0.00236 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the circle runout is more than the maximum, replace the crankshaft.</ptxt>
</s2>
<s2>
<ptxt>Using a micrometer, measure the diameter of each main journal.</ptxt>
<figure>
<graphic graphicname="A076059E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>71.988 to 72.000 mm (2.8342 to 2.8346 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is not as specified, check the oil clearance. If necessary, replace the crankshaft.</ptxt>
</s2>
<s2>
<ptxt>Check each main journal for taper and out-of-round as shown in the illustration.</ptxt>
<spec>
<title>Maximum taper and out-of-round</title>
<specitem>
<ptxt>0.02 mm (0.000787 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the taper and out-of-round is more than the maximum, replace the crankshaft.</ptxt>
</s2>
<s2>
<ptxt>Using a micrometer, measure the diameter of each crank pin.</ptxt>
<figure>
<graphic graphicname="A076060E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>55.992 to 56.000 mm (2.2044 to 2.2047 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is not as specified, check the oil clearance. If necessary, replace the crankshaft.</ptxt>
</s2>
<s2>
<ptxt>Check each crank pin for taper and out-of-round as shown in the illustration.</ptxt>
<spec>
<title>Maximum taper and out-of-round</title>
<specitem>
<ptxt>0.02 mm (0.000787 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the taper and out-of-round is more than the maximum, replace the crankshaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0042" proc-id="RM22W0E___000045800000">
<ptxt>INSPECT CONNECTING ROD BOLT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the tension portion diameter of the bolt.</ptxt>
<figure>
<graphic graphicname="A109692E11" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>7.2 to 7.3 mm (0.283 to 0.287 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum diameter</title>
<specitem>
<ptxt>7.0 mm (0.276 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Measurement Area</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the diameter is less than the minimum, replace the connecting rod bolt.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0043" proc-id="RM22W0E___000045900000">
<ptxt>INSPECT CRANKSHAFT BEARING CAP SET BOLT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the thread outside diameter of the crankshaft bearing cap set bolt.</ptxt>
<figure>
<graphic graphicname="A223293E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>10.0 to 10.2 mm (0.394 to 0.402 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Measurement Area</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the crankshaft bearing cap set bolt.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK303BX_01_0053" proc-id="RM22W0E___000045B00000">
<ptxt>INSPECT NO. 1 OIL NOZZLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A188915" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Push the check valve with a pin to check if it is stuck. If stuck, replace the oil nozzle.</ptxt>
</s2>
<s2>
<ptxt>Push the check valve with a pin to check if it moves smoothly.</ptxt>
<ptxt>If it does not move smoothly, clean or replace the oil nozzle.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A190792E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>While covering A, blow air into B. Check that air does not leak through C. Perform the check again while covering B and blowing air into A.</ptxt>
<ptxt>If air leaks, clean or replace the oil nozzle.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A190793E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Push the check valve while covering A, and blow air into B. Check that air passes through C. Perform the check again while covering B, pushing the check valve and blowing air into A.</ptxt>
<ptxt>If air does not pass through C, clean or replace the oil nozzle.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>