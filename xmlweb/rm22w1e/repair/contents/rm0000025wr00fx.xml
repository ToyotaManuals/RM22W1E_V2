<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3Y1_T00R4" variety="T00R4">
<name>POWER BACK DOOR SYSTEM</name>
<para id="RM0000025WR00FX" category="J" type-id="305F6" name-id="DH06C-02" from="201301" to="201308">
<dtccode/>
<dtcname>Door Control Switch Circuit</dtcname>
<subpara id="RM0000025WR00FX_01" type-id="60" category="03" proc-id="RM22W0E___0000IMW00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The back door control switch only turns on while the switch is being pressed, and turns off when the back door control switch is released.</ptxt>
<ptxt>When the back door control switch is on, a power back door close request signal is input to the power back door unit assembly (power back door ECU) to close the back door.</ptxt>
</content5>
</subpara>
<subpara id="RM0000025WR00FX_02" type-id="32" category="03" proc-id="RM22W0E___0000IMX00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B312260E03" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000025WR00FX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000025WR00FX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000025WR00FX_04_0001" proc-id="RM22W0E___0000IMY00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (PBD CLOSE SW)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the Data List for proper functioning of the back door control switch (See page <xref label="Seep01" href="RM000003GK3002X"/>).</ptxt>
<table pgwide="1">
<title>Back Door</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PBD Close SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Back door control switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Back door control switch on</ptxt>
<ptxt>OFF: Back door control switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000025WR00FX_04_0008" fin="true">OK</down>
<right ref="RM0000025WR00FX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000025WR00FX_04_0002" proc-id="RM22W0E___0000IMZ00000">
<testtitle>INSPECT BACK DOOR CONTROL SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the back door control switch (See page <xref label="Seep01" href="RM000003FAG00NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the back door control switch (See page <xref label="Seep02" href="RM000003G9000HX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000025WR00FX_04_0003" fin="false">OK</down>
<right ref="RM0000025WR00FX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000025WR00FX_04_0003" proc-id="RM22W0E___0000IN000000">
<testtitle>CHECK HARNESS AND CONNECTOR (BACK DOOR CONTROL SWITCH - POWER BACK DOOR UNIT ASSEMBLY [POWER BACK DOOR ECU] AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the S25 back door control switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the L52 power back door unit assembly (power back door ECU) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance of the wire harness side connectors.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>S25-3 (MSW) - L52-4 (DS1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S25-4 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S25-3 (MSW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000025WR00FX_04_0004" fin="true">OK</down>
<right ref="RM0000025WR00FX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000025WR00FX_04_0004">
<testtitle>REPLACE POWER BACK DOOR UNIT ASSEMBLY (POWER BACK DOOR ECU)<xref label="Seep01" href="RM0000039HB017X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000025WR00FX_04_0005">
<testtitle>REPLACE BACK DOOR CONTROL SWITCH<xref label="Seep01" href="RM000003FAG00NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000025WR00FX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000025WR00FX_04_0008">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000003EBU00BX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>