<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R4_T00K7" variety="T00K7">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM000002EBY054X" category="J" type-id="802E3" name-id="AV6YB-04" from="201308">
<dtccode/>
<dtcname>Microphone Circuit between Microphone and Radio Receiver</dtcname>
<subpara id="RM000002EBY054X_01" type-id="60" category="03" proc-id="RM22W0E___0000CBP00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit sends the microphone signal from the map light assembly (telephone microphone assembly) to the radio receiver assembly.</ptxt>
<ptxt> It also supplies power source from the radio receiver assembly to the map light assembly (telephone microphone assembly).</ptxt>
</content5>
</subpara>
<subpara id="RM000002EBY054X_02" type-id="32" category="03" proc-id="RM22W0E___0000CBQ00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E177629E09" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002EBY054X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002EBY054X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002EBY054X_04_0001" proc-id="RM22W0E___0000CBR00001">
<testtitle>INSPECT RADIO RECEIVER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F54 radio receiver assembly connector.</ptxt>
<figure>
<graphic graphicname="E163028E18" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.45in"/>
<colspec colname="COL2" colwidth="1.45in"/>
<colspec colname="COL3" colwidth="1.23in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F54-3 (MACC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4 to 6 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.45in"/>
<colspec colname="COL2" colwidth="1.45in"/>
<colspec colname="COL3" colwidth="1.23in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F54-6 (SGND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F54-5 (MIN-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Component without harness connected</ptxt>
<ptxt>(Radio Receiver Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002EBY054X_04_0002" fin="false">OK</down>
<right ref="RM000002EBY054X_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002EBY054X_04_0002" proc-id="RM22W0E___0000CBS00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MAP LIGHT ASSEMBLY - RADIO RECEIVER ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the R7 map light assembly and F54 radio receiver assembly connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.45in"/>
<colspec colname="COLSPEC0" colwidth="1.45in"/>
<colspec colname="COL2" colwidth="1.23in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R7-20 (SNS2) - F54-1 (SNS2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R7-27 (MI1+) - F54-2 (MIN+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R7-26 (MIC-) - F54-5 (MIN-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R7-25 (ACC) - F54-3 (MACC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R7-20 (SNS2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R7-27 (MI1+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R7-26 (MIC-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R7-25 (ACC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F54-6 (SGND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002EBY054X_04_0006" fin="false">OK</down>
<right ref="RM000002EBY054X_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002EBY054X_04_0006" proc-id="RM22W0E___0000CBT00001">
<testtitle>INSPECT MAP LIGHT ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E187245E09" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.45in"/>
<colspec colname="COLSPEC0" colwidth="1.45in"/>
<colspec colname="COL2" colwidth="1.23in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R7-20 (SNS2) - R7-26 (MIC-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Component without harness connected</ptxt>
<ptxt>(Map Light Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002EBY054X_04_0009" fin="false">OK</down>
<right ref="RM000002EBY054X_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002EBY054X_04_0009" proc-id="RM22W0E___0000CBV00001">
<testtitle>INSPECT MAP LIGHT ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the radio receiver assembly connector.</ptxt>
<figure>
<graphic graphicname="E187250E07" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Reconnect the map light assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (ACC).</ptxt>
</test1>
<test1>
<ptxt>Connect an oscilloscope to terminals R7-27 (MI1+) and R7-26 (MIC-) of the map light assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Check the waveform of the telephone microphone assembly using an oscilloscope.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A waveform synchronized with the voice input to the telephone microphone assembly is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A waveform synchronized with the voice input to the telephone microphone assembly is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Map Light Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000002EBY054X_04_0003" fin="true">A</down>
<right ref="RM000002EBY054X_04_0007" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002EBY054X_04_0007" proc-id="RM22W0E___0000CBU00001">
<testtitle>REPLACE TELEPHONE MICROPHONE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the telephone microphone assembly (See page <xref label="Seep01" href="RM000002LJJ045X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check if the same malfunction recurs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Malfunction does not recur</ptxt>
<ptxt>(returns to normal)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Malfunction recurs</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002EBY054X_04_0010" fin="true">A</down>
<right ref="RM000002EBY054X_04_0008" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002EBY054X_04_0003">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000012A80GEX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002EBY054X_04_0004">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AI400DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002EBY054X_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002EBY054X_04_0008">
<testtitle>REPLACE MAP LIGHT ASSEMBLY<xref label="Seep01" href="RM000002LJJ045X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002EBY054X_04_0010">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>