<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MJ_T00FM" variety="T00FM">
<name>AUTOMATIC TRANSMISSION UNIT</name>
<para id="RM0000013EX05YX" category="G" type-id="3001K" name-id="AT31P-01" from="201301">
<name>INSPECTION</name>
<subpara id="RM0000013EX05YX_01" type-id="01" category="01">
<s-1 id="RM0000013EX05YX_01_0001" proc-id="RM22W0E___00007UX00000">
<ptxt>INSPECT AUTOMATIC TRANSMISSION OIL PAN SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="D001124E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the magnets, and use them to collect steel particles.</ptxt>
</s2>
<s2>
<ptxt>Carefully look at the foreign matter and particles in the pan and on the magnets to anticipate the type of wear you will find in the transmission.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Steel (magnetic): bearing, gear and clutch plate wear</ptxt>
</item>
<item>
<ptxt>Brass (non-magnetic): bush wear</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0002" proc-id="RM22W0E___00007UY00000">
<ptxt>INSPECT NO. 2 1-WAY CLUTCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161779E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Hold the reverse clutch hub and turn the 1-way clutch assembly. Check that the 1-way clutch turns freely clockwise and locks when turned counterclockwise.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0021" proc-id="RM22W0E___00007VE00000">
<ptxt>INSPECT REAR CLUTCH DISC</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C162758" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) grooves have even a little bit of damage.</ptxt>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 2 hours.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0022" proc-id="RM22W0E___00007VF00000">
<ptxt>INSPECT REVERSE CLUTCH HUB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161792" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a dial indicator, measure the inside diameter of the reverse clutch hub bush.</ptxt>
<spec>
<title>Standard inside diameter</title>
<specitem>
<ptxt>41.912 to 41.937 mm (1.410 to 1.411 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the inside diameter is more than the standard, replace the reverse clutch hub.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0023" proc-id="RM22W0E___00007VG00000">
<ptxt>INSPECT FORWARD CLUTCH HUB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161791" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a dial indicator, measure the inside diameter of the forward clutch hub bush.</ptxt>
<spec>
<title>Standard inside diameter</title>
<specitem>
<ptxt>30.200 to 30.225 mm (1.189 to 1.190 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the inside diameter is more than the standard, replace the forward clutch hub.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0032" proc-id="RM22W0E___00007VM00000">
<ptxt>INSPECT NO. 4 1-WAY CLUTCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161784E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Hold the coast clutch hub and turn the 1-way clutch assembly. Check that the 1-way clutch turns freely clockwise and locks when turned counterclockwise.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0024" proc-id="RM22W0E___00007VH00000">
<ptxt>INSPECT FORWARD MULTIPLE DISC CLUTCH DISC</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161790" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) grooves or printed numbers have even a little bit of damage.</ptxt>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 2 hours.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0033" proc-id="RM22W0E___00007VN00000">
<ptxt>INSPECT COAST CLUTCH DISC</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161789" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) grooves or printed numbers have even a little bit of damage.</ptxt>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 2 hours.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0025" proc-id="RM22W0E___00007VI00000">
<ptxt>INSPECT FORWARD CLUTCH RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161788" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>29.65 mm (1.17 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0026" proc-id="RM22W0E___00007VJ00000">
<ptxt>INSPECT DIRECT CLUTCH DISC</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161787" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) grooves or printed numbers have even a little bit of damage.</ptxt>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 2 hours.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0027" proc-id="RM22W0E___00007VK00000">
<ptxt>INSPECT REVERSE CLUTCH RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161786" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>21.24 mm (0.836 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0028" proc-id="RM22W0E___00007VL00000">
<ptxt>INSPECT DIRECT CLUTCH RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161785" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>19.46 mm (0.766 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0003" proc-id="RM22W0E___00007UZ00000">
<ptxt>INSPECT NO. 3 BRAKE DISC</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161778" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) grooves or printed numbers have even a little bit of damage.</ptxt>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 2 hours.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0004" proc-id="RM22W0E___00007V000000">
<ptxt>INSPECT NO. 3 BRAKE PISTON RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161777" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>15.72 mm (0.619 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0005" proc-id="RM22W0E___00007V100000">
<ptxt>INSPECT FRONT PLANETARY GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161773" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a feeler gauge, measure the front planetary pinion gear thrust clearance.</ptxt>
<spec>
<title>Standard clearance</title>
<specitem>
<ptxt>0.2 to 0.6 mm (0.00787 to 0.0236 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the clearance is more than the standard, replace the front planetary gear assembly.</ptxt>
</item>
</list1>
</s2>
<s2>
<ptxt>Using a cylinder gauge, measure the inside diameter of the front planetary gear bush.</ptxt>
<figure>
<graphic graphicname="C161774" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard inside diameter</title>
<specitem>
<ptxt>61.005 to 61.030 mm (2.402 to 2.403 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the inside diameter is more than the standard, replace the front planetary gear.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0006" proc-id="RM22W0E___00007V200000">
<ptxt>INSPECT 1-WAY CLUTCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161775E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the 1-way clutch to the 1-way clutch inner race.</ptxt>
</s2>
<s2>
<ptxt>Hold the 1-way clutch inner race and turn the 1-way clutch. Check that the 1-way clutch turns freely counterclockwise and locks when turned clockwise.</ptxt>
</s2>
<s2>
<ptxt>Remove the 1-way clutch from the 1-way clutch inner race.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0007" proc-id="RM22W0E___00007V300000">
<ptxt>INSPECT NO. 1 BRAKE DISC</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161776" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) grooves or printed numbers have even a little bit of damage.</ptxt>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 2 hours.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0008" proc-id="RM22W0E___00007V400000">
<ptxt>INSPECT BRAKE PISTON RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161772" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>17.05 mm (0.671 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0009" proc-id="RM22W0E___00007V500000">
<ptxt>INSPECT NO. 2 BRAKE DISC</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161770" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) grooves or printed numbers have even a little bit of damage.</ptxt>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 2 hours.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0020" proc-id="RM22W0E___00007VD00000">
<ptxt>INSPECT NO. 2 BRAKE PISTON RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161771" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>22.66 mm (0.892 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0010" proc-id="RM22W0E___00007V600000">
<ptxt>INSPECT CENTER PLANETARY GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161749" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a feeler gauge, measure the center planetary pinion gear thrust clearance.</ptxt>
<spec>
<title>Standard clearance</title>
<specitem>
<ptxt>0.20 to 0.60 mm (0.00787 to 0.0236 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the clearance is more than the standard, replace the center planetary gear assembly.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0011" proc-id="RM22W0E___00007V700000">
<ptxt>INSPECT NO. 3 1-WAY CLUTCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161748E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Hold the rear planetary ring gear flange and turn the 1-way clutch. Check that the 1-way clutch turns freely counterclockwise and locks when turned clockwise.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0013" proc-id="RM22W0E___00007V800000">
<ptxt>INSPECT INTERMEDIATE SHAFT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161746E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a dial indicator, check the intermediate shaft runout.</ptxt>
<spec>
<title>Standard runout</title>
<specitem>
<ptxt>0.03 mm (0.00118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the runout is more than the standard, replace the intermediate shaft with a new one.</ptxt>
</s2>
<s2>
<ptxt>Using a micrometer, check the diameter of the intermediate shaft at the positions shown in the diagram.</ptxt>
<figure>
<graphic graphicname="C161747E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard diameter</title>
<subtitle>A</subtitle>
<specitem>
<ptxt>19.963 to 19.976 mm (0.7859 to 0.7865 in.)</ptxt>
</specitem>
<subtitle>B, C</subtitle>
<specitem>
<ptxt>30.150 to 30.163 mm (1.187 to 1.188 in.)</ptxt>
</specitem>
<subtitle>D, E</subtitle>
<specitem>
<ptxt>36.404 to 36.420 mm (1.433 to 1.434 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is less than the standard, replace the intermediate shaft with a new one.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0014" proc-id="RM22W0E___00007V900000">
<ptxt>INSPECT NO. 4 BRAKE DISC</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) grooves or printed numbers have even a little bit of damage.</ptxt>
<figure>
<graphic graphicname="C161745" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 2 hours.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0015" proc-id="RM22W0E___00007VA00000">
<ptxt>INSPECT REAR PLANETARY GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a feeler gauge, measure the rear planetary pinion gear thrust clearance.</ptxt>
<figure>
<graphic graphicname="C161743" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard clearance</title>
<specitem>
<ptxt>0.2 to 0.6 mm (0.00787 to 0.0236 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the clearance is more than the standard, replace the planetary gear assembly.</ptxt>
</item>
</list1>
</s2>
<s2>
<ptxt>Using a dial indicator, measure the inside diameter of the rear planetary gear bush.</ptxt>
<figure>
<graphic graphicname="C161744" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard inside diameter</title>
<specitem>
<ptxt>20.0 to 20.025 mm (0.787 to 0.788 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the inside diameter is more than the standard, replace the rear planetary gear assembly.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0016" proc-id="RM22W0E___00007VB00000">
<ptxt>INSPECT 1ST AND REVERSE BRAKE RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161742" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>23.54 to 23.94 mm (0.927 to 0.943 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05YX_01_0019" proc-id="RM22W0E___00007VC00000">
<ptxt>INSPECT INDIVIDUAL PISTON OPERATION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C161780E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Check the operating sound while applying compressed air into the oil holes indicated in the illustration.</ptxt>
<atten4>
<ptxt>When inspecting the direct clutch, check with the accumulator piston holes indicated in the illustration.</ptxt>
<ptxt>If there is no sound, disassemble and check the installation condition of the parts.</ptxt>
</atten4>
<list1 type="ordered">
<item>
<ptxt>No. 2 clutch (C2)</ptxt>
</item>
<item>
<ptxt>No. 3 clutch (C3)</ptxt>
</item>
<item>
<ptxt>No. 4 clutch (C4)</ptxt>
</item>
<item>
<ptxt>No. 1 clutch (C1)</ptxt>
</item>
<item>
<ptxt>No. 3 brake (B3)</ptxt>
</item>
<item>
<ptxt>No. 1 brake (B1)</ptxt>
</item>
<item>
<ptxt>No. 2 brake (B2)</ptxt>
</item>
<item>
<ptxt>No. 4 brake (B4)</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>