<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3W4_T00P7" variety="T00P7">
<name>FRONT AIR CONDITIONING UNIT (for LHD)</name>
<para id="RM0000017ZP076X" category="A" type-id="30014" name-id="ACEO1-03" from="201308">
<name>INSTALLATION</name>
<subpara id="RM0000017ZP076X_01" type-id="01" category="01">
<s-1 id="RM0000017ZP076X_01_0001" proc-id="RM22W0E___0000GTZ00001">
<ptxt>INSTALL AIR CONDITIONING UNIT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the air conditioning unit with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0055" proc-id="RM22W0E___0000GUB00001">
<ptxt>INSTALL AIR CONDITIONING HOSE AND ACCESSORY (w/ Cool Box)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E112921E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Lubricate 2 new O-rings with compressor oil and install them to the air conditioning hose and accessory.</ptxt>
<spec>
<title>Compressor oil</title>
<specitem>
<ptxt>ND-OIL 8 or equivalent</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Correct</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Incorrect</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the air conditioning hose and accessory with the piping clamp.</ptxt>
<atten3>
<ptxt>After connection, check the claw engagement of the piping clamp.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0032" proc-id="RM22W0E___0000GU500001">
<ptxt>INSTALL INSTRUMENT PANEL REINFORCEMENT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the instrument panel reinforcement.</ptxt>
<s3>
<ptxt>Install the instrument panel reinforcement with the 7 bolts and screw.</ptxt>
<torque>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
<subtitle>for screw</subtitle>
<torqueitem>
<t-value1>6.0</t-value1>
<t-value2>61</t-value2>
<t-value3>53</t-value3>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>for Passenger Side:</ptxt>
<ptxt>Using a 12 mm hexagon wrench, install the 2 collars.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>for Passenger Side:</ptxt>
<ptxt>Using a T40 "TORX" socket, install the 2 "TORX" bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>for Passenger Side:</ptxt>
<ptxt>Install the 2 caps.</ptxt>
</s3>
<s3>
<ptxt>for Driver Side:</ptxt>
<ptxt>Install the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>for Driver Side:</ptxt>
<ptxt>Install the 3 caps.</ptxt>
<figure>
<graphic graphicname="E155489" width="7.106578999in" height="5.787629434in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Attach the 17 clamps.</ptxt>
</s2>
<s2>
<ptxt>Install the 8 nuts, 6 bolts, connectors to the wire harness.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0054" proc-id="RM22W0E___0000GUA00001">
<ptxt>INSTALL HEATER TO REGISTER DUCT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the duct.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0040" proc-id="RM22W0E___0000GU700001">
<ptxt>INSTALL STEERING COLUMN ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Power Tilt and Power Telescopic Steering Column:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000039SG02LX"/>)</ptxt>
</s2>
<s2>
<ptxt>for Manual Tilt and Manual Telescopic Steering Column:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000039SG02KX"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0056" proc-id="RM22W0E___0000BAL00001">
<ptxt>INSTALL NO. 3 AIR DUCT SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the duct.</ptxt>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0004" proc-id="RM22W0E___0000GU000001">
<ptxt>INSTALL INSTRUMENT PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000039ON00OX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0049" proc-id="RM22W0E___0000GU900001">
<ptxt>INSTALL FRONT WIPER MOTOR AND BRACKET</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000002M6301AX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0020" proc-id="RM22W0E___0000GU200001">
<ptxt>CONNECT HEATER WATER INLET HOSE AND HEATER WATER OUTLET HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 2 heater water hoses.</ptxt>
</s2>
<s2>
<ptxt>Using pliers, grip the claws of the clips and slide the 2 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0007" proc-id="RM22W0E___0000GU100001">
<ptxt>CONNECT AIR CONDITIONING TUBE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the attached vinyl tape from the tubes.</ptxt>
<figure>
<graphic graphicname="E154198E03" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Sufficiently apply compressor oil to 2 new O-rings and the fitting surface of the air conditioning tube assembly.</ptxt>
<spec>
<title>Compressor oil</title>
<specitem>
<ptxt>ND-OIL 8 or equivalent</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Plate</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the 2 O-rings to the air conditioning tube assembly.</ptxt>
</s2>
<s2>
<ptxt>Connect the air conditioning tube assembly.</ptxt>
</s2>
<s2>
<ptxt>Attach the plate as shown in the illustration and install the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0022" proc-id="RM22W0E___0000GU300001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0031" proc-id="RM22W0E___0000GU400001">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000000XFF0E9X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0041" proc-id="RM22W0E___0000GU800001">
<ptxt>ADD ENGINE COOLANT</ptxt>
<content1 releasenbr="3">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000017DS03SX_01_0002"/>)</ptxt>
</s2>
<s2>
<ptxt>for 1UR-FE:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM000003AAX00CX_01_0006"/>)</ptxt>
</s2>
<s2>
<ptxt>for 3UR-FE:</ptxt>
<ptxt>(See page <xref label="Seep03" href="RM0000017DS03TX_01_0002"/>)</ptxt>
</s2>
<s2>
<ptxt>for 1VD-FTV:</ptxt>
<ptxt>(See page <xref label="Seep04" href="RM000001X4H00AX_01_0003"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0036" proc-id="RM22W0E___000048900000">
<ptxt>CHARGE REFRIGERANT
</ptxt>
<content1 releasenbr="2">
<sst>
<sstitem>
<s-number>09985-20010</s-number>
<s-subnumber>09985-02130</s-subnumber>
<s-subnumber>09985-02150</s-subnumber>
<s-subnumber>09985-02090</s-subnumber>
<s-subnumber>09985-02110</s-subnumber>
<s-subnumber>09985-02010</s-subnumber>
<s-subnumber>09985-02050</s-subnumber>
<s-subnumber>09985-02060</s-subnumber>
<s-subnumber>09985-02070</s-subnumber>
</sstitem>
</sst>
<s2>
<ptxt>Perform vacuum purging using a vacuum pump.</ptxt>
</s2>
<s2>
<ptxt>Charge refrigerant HFC-134a (R134a).</ptxt>
<table pgwide="1">
<title>Standard:</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Condenser Core Thickness</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Conditioning Type</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>Refrigerant Charging Amount</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="3" valign="middle">
<ptxt>22 mm (0.866 in.)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>w/o Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>870 +/-30 g (30.7 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>870 +/-30 g (30.7 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>w/ Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>1010 +/-30 g (35.6 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>960 +/-30 g (33.9 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3" valign="middle">
<ptxt>16 mm (0.630 in.)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>w/o Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>770 +/-30 g (27.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>770 +/-30 g (27.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>w/ Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>970 +/-30 g (34.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>920 +/-30 g (32.5 +/-1.1 oz.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="I037365E19" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not operate the cooler compressor before charging refrigerant as the cooler compressor will not work properly without any refrigerant, and will overheat.</ptxt>
</item>
<item>
<ptxt>Approximately 200 g (7.05 oz.) of refrigerant may need to be charged after bubbles disappear. The refrigerant amount should be checked by measuring its quantity, and not with the sight glass.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000017ZP076X_01_0037" proc-id="RM22W0E___000048A00000">
<ptxt>WARM UP ENGINE
</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Warm up the engine at less than 1850 rpm for 2 minutes or more after charging the refrigerant.</ptxt>
<atten3>
<ptxt>Be sure to warm up the compressor when turning the A/C switch is on after removing and installing the cooler refrigerant lines (including the compressor), to prevent damage to the compressor.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000017ZP076X_01_0038" proc-id="RM22W0E___0000GU600001">
<ptxt>CHECK FOR ENGINE COOLANT LEAK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000017DS03SX_01_0005"/>)</ptxt>
</s2>
<s2>
<ptxt>for 1UR-FE:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM000003AAX00CX_01_0012"/>)</ptxt>
</s2>
<s2>
<ptxt>for 3UR-FE:</ptxt>
<ptxt>(See page <xref label="Seep03" href="RM0000017DS03TX_01_0005"/>)</ptxt>
</s2>
<s2>
<ptxt>for 1VD-FTV:</ptxt>
<ptxt>(See page <xref label="Seep04" href="RM000001X4H00AX_01_0008"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0061" proc-id="RM22W0E___000048B00000">
<ptxt>CHECK FOR REFRIGERANT GAS LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>After recharging the refrigerant gas, check for refrigerant gas leakage using a halogen leak detector.</ptxt>
</s2>
<s2>
<ptxt>Perform the operation under these conditions:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stop the engine.</ptxt>
</item>
<item>
<ptxt>Secure good ventilation (the halogen leak detector may react to volatile gases other than refrigerant, such as evaporated gasoline or exhaust gas).</ptxt>
</item>
<item>
<ptxt>Repeat the test 2 or 3 times.</ptxt>
</item>
<item>
<ptxt>Make sure that some refrigerant remains in the refrigeration system. When compressor is off: approximately 392 to 588 kPa (4.0 to 6.0 kgf/cm<sup>2</sup>, 57 to 85 psi).</ptxt>
</item>
</list1>
</s2>
<s2>
<ptxt>Using a halogen leak detector, check the refrigerant line for leakage.</ptxt>
<figure>
<graphic graphicname="I042222E21" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>If a gas leak is not detected on the drain hose, remove the blower motor control (blower resistor) from the cooling unit. Insert the halogen leak detector sensor into the unit and perform the test.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and wait for approximately 20 minutes. Bring the halogen leak detector close to the pressure switch and perform the test.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000017ZP076X_01_0062" proc-id="RM22W0E___000049F00000">
<ptxt>INSTALL UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 7 clips and radiator support seal.</ptxt>
<figure>
<graphic graphicname="B180890E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000017ZP076X_01_0064" proc-id="RM22W0E___0000GUE00001">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM000002B5J01UX_01_0260"/>)</ptxt>
</s2>
<s2>
<ptxt>for 1UR-FE:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM000002B5J01VX_01_0209"/>)</ptxt>
</s2>
<s2>
<ptxt>for 3UR-FE:</ptxt>
<ptxt>(See page <xref label="Seep03" href="RM0000031RQ01XX_01_0065"/>)</ptxt>
</s2>
<s2>
<ptxt>for 1VD-FTV:</ptxt>
<ptxt>(See page <xref label="Seep04" href="RM0000031FI00VX_01_0304"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0059" proc-id="RM22W0E___0000GUC00001">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Standard:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000038JV01UX_01_0013"/>)</ptxt>
</s2>
<s2>
<ptxt>w/ Winch:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000038JV01VX_01_0013"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZP076X_01_0060" proc-id="RM22W0E___0000GUD00001">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Standard:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000038JV01UX_01_0014"/>)</ptxt>
</s2>
<s2>
<ptxt>w/ Winch:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000038JV01VX_01_0014"/>)</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>