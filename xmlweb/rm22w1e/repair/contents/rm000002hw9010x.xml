<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001F" variety="S001F">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001F_7C3O0_T00H3" variety="T00H3">
<name>FRONT DIFFERENTIAL CARRIER OIL SEAL</name>
<para id="RM000002HW9010X" category="A" type-id="30019" name-id="AD1S6-01" from="201301">
<name>REPLACEMENT</name>
<subpara id="RM000002HW9010X_01" type-id="01" category="01">
<s-1 id="RM000002HW9010X_01_0051" proc-id="RM22W0E___00009A400000">
<ptxt>DRAIN DIFFERENTIAL OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Stop the vehicle on a level place.</ptxt>
</s2>
<s2>
<ptxt>for Front Differential:</ptxt>
<figure>
<graphic graphicname="C174596" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the engine under cover.</ptxt>
</s3>
<s3>
<ptxt>Using a 10 mm hexagon wrench, remove the filler plug and gasket.</ptxt>
</s3>
<s3>
<ptxt>Using a 10 mm hexagon wrench, remove the drain plug and gasket, and drain the oil.</ptxt>
</s3>
<s3>
<ptxt>Using a 10 mm hexagon wrench, install a new gasket and the drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>39</t-value1>
<t-value2>400</t-value2>
<t-value4>29</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
<s2>
<ptxt>for Rear Differential:</ptxt>
<s3>
<ptxt>Remove the filler plug and gasket.</ptxt>
</s3>
<s3>
<ptxt>Remove the drain plug and gasket, and drain the oil.</ptxt>
</s3>
<s3>
<ptxt>Install a new gasket and the drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>49</t-value1>
<t-value2>500</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0010" proc-id="RM22W0E___00009F600000">
<ptxt>REMOVE FRONT DIFFERENTIAL CARRIER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the differential carrier (See page <xref label="Seep01" href="RM0000017BM02CX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HW9010X_01_0052" proc-id="RM22W0E___00009FA00000">
<ptxt>REMOVE DIFFERENTIAL EXTENSION FLANGE TUBE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
<figure>
<graphic graphicname="C171952" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer, remove the differential tube.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0053" proc-id="RM22W0E___00009FB00000">
<ptxt>REMOVE FRONT DRIVE PINION COMPANION FLANGE FRONT NUT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, unstake the nut.</ptxt>
<sst>
<sstitem>
<s-number>09930-00010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C159261E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST to hold the companion flange, remove the nut.</ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
<s-subnumber>09330-00030</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C159262E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0054" proc-id="RM22W0E___00009FC00000">
<ptxt>REMOVE FRONT DRIVE PINION COMPANION FLANGE FRONT SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the companion flange from the differential carrier.</ptxt>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03020</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C159263E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0007" proc-id="RM22W0E___00009F500000">
<ptxt>REMOVE FRONT DIFFERENTIAL CARRIER OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the oil seal.</ptxt>
<sst>
<sstitem>
<s-number>09308-10010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C159264E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HW9010X_01_0014">
<ptxt>REMOVE FRONT DIFFERENTIAL DRIVE PINION OIL SLINGER</ptxt>
</s-1>
<s-1 id="RM000002HW9010X_01_0055" proc-id="RM22W0E___00009FD00000">
<ptxt>REMOVE FRONT DIFFERENTIAL CROSS SHAFT BEARING RETAINER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolts labeled B and front differential front support assembly LH.</ptxt>
<figure>
<graphic graphicname="C160133E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolts labeled A.</ptxt>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer, tap out the bearing retainer.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0056" proc-id="RM22W0E___00009FE00000">
<ptxt>REMOVE FRONT NO. 1 DIFFERENTIAL CASE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the differential case from the differential carrier.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0057" proc-id="RM22W0E___00009FF00000">
<ptxt>REMOVE DIFFERENTIAL DRIVE PINION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the differential carrier to an overhaul stand as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C159434E02" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Using a press, press out the drive pinion.</ptxt>
<atten3>
<ptxt>Do not drop the drive pinion.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0058" proc-id="RM22W0E___00009FG00000">
<ptxt>REMOVE FRONT DRIVE PINION FRONT RADIAL BALL BEARING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bearing (inner race) from the differential carrier.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0045" proc-id="RM22W0E___00009F900000">
<ptxt>REPLACE FRONT DIFFERENTIAL DRIVE PINION BEARING SPACER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Replace the bearing spacer with a new one.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HW9010X_01_0040">
<ptxt>TEMPORARILY INSTALL DIFFERENTIAL DRIVE PINION</ptxt>
</s-1>
<s-1 id="RM000002HW9010X_01_0041">
<ptxt>INSTALL FRONT NO. 1 DIFFERENTIAL CASE SUB-ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM000002HW9010X_01_0059" proc-id="RM22W0E___00009FH00000">
<ptxt>INSTALL FRONT DIFFERENTIAL CROSS SHAFT BEARING RETAINER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove any old FIPG material and be careful not to drop oil on the contact surfaces of the differential carrier and shaft bearing retainer.</ptxt>
</s2>
<s2>
<ptxt>Wipe off any residual FIPG material on the contact surface using gasoline or alcohol.</ptxt>
</s2>
<s2>
<ptxt>Apply seal packing to the differential carrier as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C214104E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Seal packing</title>
<specitem>
<ptxt>Toyota Genuine Seal Packing 1281, Three Bond 1281 or equivalent </ptxt>
</specitem>
</spec>
<spec>
<title>Seal Diameter (A)</title>
<specitem>
<ptxt>2.0 to 3.0 mm (0.0788 to 0.118 in.)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Install the shaft bearing retainer within 10 minutes of applying seal packing.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the front differential cross shaft bearing retainer and front differential front support assembly LH with the 11 bolts (A) and 3 new bolts (B).</ptxt>
<figure>
<graphic graphicname="C160133E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>65</t-value1>
<t-value2>633</t-value2>
<t-value4>48</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>155</t-value1>
<t-value2>1581</t-value2>
<t-value4>114</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not add oil or drive the vehicle immediately after installing the cover, and leave it as is for at least an hour. Also, for 12 hours or less, avoid rapid acceleration/deceleration.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0066" proc-id="RM22W0E___00009FN00000">
<ptxt>INSTALL FRONT DRIVE PINION FRONT RADIAL BALL BEARING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, lightly tap in the bearing (inner race) to install it.</ptxt>
<sst>
<sstitem>
<s-number>09316-60011</s-number>
<s-subnumber>09316-00041</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C159432E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0044">
<ptxt>INSTALL FRONT DIFFERENTIAL DRIVE PINION OIL SLINGER</ptxt>
</s-1>
<s-1 id="RM000002HW9010X_01_0026" proc-id="RM22W0E___00009F700000">
<ptxt>INSTALL FRONT DIFFERENTIAL CARRIER OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, tap in a new oil seal.</ptxt>
<sst>
<sstitem>
<s-number>09502-12010</s-number>
</sstitem>
<sstitem>
<s-number>09316-60011</s-number>
<s-subnumber>09316-00041</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C159273E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard oil seal depth</title>
<specitem>
<ptxt>5.6 to 6.6 mm (0.221 to 0.260 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Coat the lip of the oil seal with MP grease.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HW9010X_01_0060" proc-id="RM22W0E___00009FI00000">
<ptxt>INSTALL FRONT DRIVE PINION COMPANION FLANGE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, install the companion flange.</ptxt>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03020</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C159263E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0061" proc-id="RM22W0E___00009FJ00000">
<ptxt>INSPECT DIFFERENTIAL DRIVE PINION PRELOAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, hold the companion flange.</ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
<s-subnumber>09330-00030</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C159272E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Coat the threads of a new nut with hypoid gear oil.</ptxt>
</s2>
<s2>
<ptxt>Install the nut by tightening it until the standard preload is reached.</ptxt>
<torque>
<torqueitem>
<t-value1>451</t-value1>
<t-value2>4599</t-value2>
<t-value4>332</t-value4>
<ptxt>or less</ptxt>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a torque wrench, measure the preload of the backlash between the drive pinion and ring gear.</ptxt>
<figure>
<graphic graphicname="C159874" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.0 to 3.5 N*m (31 to 35 kgf*cm, 27 to 50 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.1 to 2.5 N*m (22 to 24 kgf*cm, 19 to 21 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the preload is more than the specification, replace the bearing spacer.</ptxt>
<ptxt>If the preload is less than the specification, tighten the nut with 13 N*m (130 kgf*cm, 9 ft.*lbf) of torque at a time until the specified preload is reached.</ptxt>
<torque>
<torqueitem>
<t-value1>451</t-value1>
<t-value2>4599</t-value2>
<t-value4>332</t-value4>
<ptxt>or less</ptxt>
</torqueitem>
</torque>
<ptxt>If the maximum torque is exceeded while tightening the nut, replace the bearing spacer and repeat the preload procedure.</ptxt>
<atten4>
<ptxt>Do not loosen the nut to reduce the preload.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0062" proc-id="RM22W0E___00009FK00000">
<ptxt>INSPECT TOTAL PRELOAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>With the drive pinion contacting the tooth side of the ring gear, use a torque wrench to measure the total preload.</ptxt>
<figure>
<graphic graphicname="C159874" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Total Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Standard drive pinion preload plus 1.6 to 2.4 N*m (17 to 24 kgf*cm, 15 to 21 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Standard drive pinion preload plus 1.3 to 2.1 N*m (14 to 21 kgf*cm, 12 to 18 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0063" proc-id="RM22W0E___00009FL00000">
<ptxt>STAKE FRONT DRIVE PINION COMPANION FLANGE FRONT NUT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a chisel and hammer, stake the nut.</ptxt>
<figure>
<graphic graphicname="C160139" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0064" proc-id="RM22W0E___00009FM00000">
<ptxt>INSTALL DIFFERENTIAL EXTENSION FLANGE TUBE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove any old FIPG material and be careful not to drop oil on the contact surfaces of the differential tube and bearing retainer.</ptxt>
</s2>
<s2>
<ptxt>Wipe off any residual FIPG material on the contact surface using gasoline or alcohol.</ptxt>
</s2>
<s2>
<ptxt>Apply seal packing to the bearing retainer as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C176060E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Seal packing</title>
<specitem>
<ptxt>Toyota Genuine Seal Packing 1281, Three Bond 1281 or equivalent</ptxt>
</specitem>
</spec>
<spec>
<title>Seal Diameter (A)</title>
<specitem>
<ptxt>2.0 to 3.0 mm (0.0788 to 0.118 in.)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Install the differential tube within 10 minutes of applying seal packing.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the differential tube to the bearing retainer.</ptxt>
</s2>
<s2>
<ptxt>Install 4 new bolts.</ptxt>
<figure>
<graphic graphicname="C171952" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>65</t-value1>
<t-value2>663</t-value2>
<t-value4>48</t-value4>
<ptxt>or less</ptxt>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not add oil or drive the vehicle immediately after installing the cover, and leave it as is for at least an hour. Also, for 12 hours or less, avoid rapid acceleration/deceleration.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002HW9010X_01_0033" proc-id="RM22W0E___00009F800000">
<ptxt>INSTALL FRONT DIFFERENTIAL CARRIER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front differential carrier (See page <xref label="Seep01" href="RM0000017BL01YX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HW9010X_01_0065" proc-id="RM22W0E___000099V00000">
<ptxt>ADD DIFFERENTIAL OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add differential oil so that the oil level is between 0 to 5 mm (0 to 0.197 in.) from the bottom lip of the differential filler plug hole.</ptxt>
<figure>
<graphic graphicname="D025304E27" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Too much or too little oil will lead to differential problems.</ptxt>
</item>
<item>
<ptxt>After changing the oil, drive the vehicle and then check the oil level again.</ptxt>
</item>
</list1>
</atten3>
<spec>
<title>Front differential oil type and viscosity</title>
<specitem>
<ptxt>Toyota Genuine Differential gear oil LT 75W-85 GL-5 or equivalent</ptxt>
</specitem>
</spec>
<spec>
<title>Rear Differential Oil Type and Viscosity</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Type and Viscosity</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/o LSD</ptxt>
</entry>
<entry valign="middle">
<ptxt>Toyota Genuine Differential gear oil LT 75W-85 GL-5 or equivalent</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ LSD</ptxt>
</entry>
<entry>
<ptxt>Toyota Genuine Differential gear oil LX 75W-85 GL-5 or equivalent</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Front differential capacity</title>
<specitem>
<ptxt>1.85 to 1.95 liters (1.96 to 2.06 US qts., 1.63 to 1.71 Imp. qts.)</ptxt>
</specitem>
</spec>
<spec>
<title>Rear Differential Capacity</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.15 to 4.25 liters (4.39 to 4.49 US qts., 3.66 to 3.74 Imp. qts.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ LSD</ptxt>
</entry>
<entry morerows="1">
<ptxt>4.10 to 4.20 liters (4.34 to 4.43 US qts., 3.60 to 3.69 Imp. qts.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ Differential lock</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>for Front Differential:</ptxt>
<s3>
<ptxt>Using a 10 mm hexagon wrench, install a new gasket and the filler plug.</ptxt>
<torque>
<torqueitem>
<t-value1>39</t-value1>
<t-value2>400</t-value2>
<t-value4>29</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Install the engine under cover.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for Rear Differential:</ptxt>
<s3>
<ptxt>Install a new gasket and the filler plug.</ptxt>
<torque>
<torqueitem>
<t-value1>49</t-value1>
<t-value2>500</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>