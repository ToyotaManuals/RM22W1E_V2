<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000001DXZ01UX" category="J" type-id="305RV" name-id="BCE6X-02" from="201308">
<dtccode/>
<dtcname>Slip Indicator Light Remains ON</dtcname>
<subpara id="RM000001DXZ01UX_01" type-id="60" category="03" proc-id="RM22W0E___0000ADE00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The slip indicator light blinks during VSC, A-TRC, Crawl Control and hill-start assist control operation.</ptxt>
<ptxt>When the system fails, the slip indicator light comes on to warn the driver.</ptxt>
</content5>
</subpara>
<subpara id="RM000001DXZ01UX_02" type-id="32" category="03" proc-id="RM22W0E___0000ADF00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C257192E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001DXZ01UX_03" type-id="51" category="05" proc-id="RM22W0E___0000ADG00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001DXZ01UX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001DXZ01UX_05_0012" proc-id="RM22W0E___0000ADH00001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXZ01UX_05_0023" fin="false">A</down>
<right ref="RM000001DXZ01UX_05_0017" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0023" proc-id="RM22W0E___0000ABP00001">
<testtitle>INSPECT IF SKID CONTROL ECU CONNECTOR IS SECURELY CONNECTED
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the skid control ECU (master cylinder solenoid) connector connection.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connector is securely connected.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000001DXZ01UX_05_0024" fin="false">OK</down>
<right ref="RM000001DXZ01UX_05_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0024" proc-id="RM22W0E___0000ABQ00001">
<testtitle>INSPECT BATTERY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the battery voltage.</ptxt>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 1UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 3UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001DXZ01UX_05_0025" fin="false">A</down>
<right ref="RM000001DXZ01UX_05_0010" fin="true">B</right>
<right ref="RM000001DXZ01UX_05_0019" fin="true">C</right>
<right ref="RM000001DXZ01UX_05_0020" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0025" proc-id="RM22W0E___0000ABS00001">
<testtitle>CHECK CAN COMMUNICATION LINE
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Select "CAN Bus Check" from the System Selection Menu screen, and follow the prompts on the screen to inspect the CAN Bus.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>"CAN Bus Check" indicates no malfunctions in CAN communication.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001DXZ01UX_05_0026" fin="false">A</down>
<right ref="RM000001DXZ01UX_05_0014" fin="true">B</right>
<right ref="RM000001DXZ01UX_05_0021" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0026" proc-id="RM22W0E___0000ABT00001">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (for LHD: See page <xref label="Seep01" href="RM000001RSW03ZX"/>, for RHD: See page <xref label="Seep02" href="RM000001RSW040X"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>CAN system DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN system DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN system DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001DXZ01UX_05_0013" fin="false">A</down>
<right ref="RM000001DXZ01UX_05_0014" fin="true">B</right>
<right ref="RM000001DXZ01UX_05_0021" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0013" proc-id="RM22W0E___0000ADI00001">
<testtitle>PERFORM ACTIVE TEST USING GTS (SLIP INDICATOR LIGHT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Active Test.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Slip Indicator Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Slip indicator light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Indicator light ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Observe the combination meter.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>When performing the Slip Indicator Light Active Test, check Slip Indicator Light in the Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRAC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.88in"/>
<colspec colname="COL3" colwidth="1.98in"/>
<colspec colname="COL4" colwidth="1.45in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Slip Indicator Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Slip indicator light/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Indicator light on</ptxt>
<ptxt>OFF: Indicator light off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.69in"/>
<colspec colname="COL2" colwidth="3.91in"/>
<colspec colname="COL3" colwidth="1.48in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Data List Display</ptxt>
</entry>
<entry>
<ptxt>Data List Display when Performing Active Test ON/OFF Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXZ01UX_05_0007" fin="true">A</down>
<right ref="RM000001DXZ01UX_05_0004" fin="true">B</right>
<right ref="RM000001DXZ01UX_05_0022" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0005">
<testtitle>CONNECT CONNECTOR TO ECU CORRECTLY</testtitle>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0010">
<testtitle>GO TO CHARGING SYSTEM (ON-VEHICLE INSPECTION)<xref label="Seep01" href="RM0000017VF03YX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0014">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0007">
<testtitle>GO TO METER / GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L048X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0004">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0017">
<testtitle>REPAIR CIRCUITS INDICATED BY OUTPUT DTC<xref label="Seep01" href="RM0000045Z6012X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0019">
<testtitle>GO TO CHARGING SYSTEM (ON-VEHICLE INSPECTION)<xref label="Seep01" href="RM000001AR90AOX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0020">
<testtitle>GO TO CHARGING SYSTEM (ON-VEHICLE INSPECTION)<xref label="Seep01" href="RM000001AR90APX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0021">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXZ01UX_05_0022">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>