<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001S" variety="S001S">
<name>STEERING COLUMN</name>
<ttl id="12038_S001S_7C3QD_T00JG" variety="T00JG">
<name>POWER TILT AND POWER TELESCOPIC STEERING COLUMN SYSTEM</name>
<para id="RM000000XZM04FX" category="D" type-id="304JI" name-id="SR0LN-45" from="201301">
<name>FREEZE FRAME DATA</name>
<subpara id="RM000000XZM04FX_z0" proc-id="RM22W0E___0000BFJ00000">
<content5 releasenbr="1">
<step1>
<ptxt>FREEZE FRAME DATA</ptxt>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG) and check the freeze frame data by following the prompts on the intelligent tester screen.</ptxt>
<table pgwide="1">
<title>Tilt &amp; Telescopic</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.47in"/>
<colspec colname="COL2" colwidth="2.07in"/>
<colspec colname="COL3" colwidth="2.02in"/>
<colspec colname="COL4" colwidth="1.52in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Tilt Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Current tilt position data/min.: 0000 H (0 edges), max.: FFFF H (65535 edges)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7000 to 9000 H</ptxt>
<ptxt>(28672 to 36864 edges)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Telesco Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Current telescopic position data/min.: 0000 H (0 edges), max.: FFFF H (65535 edges)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7000 to 9000 H</ptxt>
<ptxt>(28672 to 36864 edges)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tilt Return Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Memorized tilt return position data/min.: 0000 H (0 edges), max.: FFFF H (65535 edges)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7000 to 9000 H</ptxt>
<ptxt>(28672 to 36864 edges)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Telesco Return Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Memorized telescopic return position data/min.: 0000 H (0 edges), max.: FFFF H (65535 edges)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7000 to 9000 H</ptxt>
<ptxt>(28672 to 36864 edges)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tilt Up Limit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Uppermost tilt position/Not Mem or Mem</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not Mem: Position is not memorized</ptxt>
<ptxt>Mem: Position is memorized</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tilt Up Limit Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Uppermost tilt position data/min.: 0000 H (0 edges), max.: FFFF H (65535 edges)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7000 to 9000 H</ptxt>
<ptxt>(28672 to 36864 edges)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tilt Down Limit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lowermost tilt position/Not Mem or Mem</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not Mem: Position is not memorized</ptxt>
<ptxt>Mem: Position is memorized</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tilt Down Limit Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lowermost tilt position data/min.: 0000 H (0 edges), max.: FFFF H (65535 edges)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7000 to 9000 H</ptxt>
<ptxt>(28672 to 36864 edges)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Telesco Short Limit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shortest telescopic position/Not Mem or Mem</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not Mem: Position is not memorized</ptxt>
<ptxt>Mem: Position is memorized</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Telesco Short Limit Pos</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shortest telescopic position data/min.: 0000 H (0 edges), max.: FFFF H (65535 edges)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7000 to 9000 H</ptxt>
<ptxt>(28672 to 36864 edges)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Telesco Long Limit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Longest telescopic position/Not Mem or Mem</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not Mem: Position is not memorized</ptxt>
<ptxt>Mem: Position is memorized</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Telesco Long Limit Pos</ptxt>
</entry>
<entry valign="middle">
<ptxt>Longest telescopic position data/min.: 0000 H (0 edges), max.: FFFF H (65535 edges)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7000 to 9000 H</ptxt>
<ptxt>(28672 to 36864 edges)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tilt Up SW (CAN)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Input state of tilt up by manual switch/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Tilt up is activated by manual switch</ptxt>
<ptxt>OFF: Tilt up is not activated by manual switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tilt Down SW (CAN)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Input state of tilt down by manual switch/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Tilt down is activated by manual switch</ptxt>
<ptxt>OFF: Tilt down is not activated by manual switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Telesco Short SW (CAN)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Input state of telescopic short by manual switch/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Telescopic short is activated by manual switch</ptxt>
<ptxt>OFF: Telescopic short is not activated by manual switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Telesco Long SW (CAN)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Input state of telescopic long by manual switch/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Telescopic long is activated by manual switch</ptxt>
<ptxt>OFF: Telescopic long is not activated by manual switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S/B Mem 1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tilt and telescopic memory position 1/Not Mem or Mem</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not Mem: Position is not memorized</ptxt>
<ptxt>Mem: Position is memorized</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S/B Mem 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tilt and telescopic memory position 2/Not Mem or Mem</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not Mem: Position is not memorized</ptxt>
<ptxt>Mem: Position is memorized</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S/B Mem 3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tilt and telescopic memory position 3/Not Mem or Mem</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not Mem: Position is not memorized</ptxt>
<ptxt>Mem: Position is memorized</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>T&amp;T Autoaway Function</ptxt>
</entry>
<entry valign="middle">
<ptxt>Auto away/return function/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Auto away and return function is allowed</ptxt>
<ptxt>OFF: Auto away and return function is prohibited</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Power Source Voltage</ptxt>
</entry>
<entry valign="middle">
<ptxt>Voltage data of power supply/min.: 0 V, max.: 20 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual power supply voltage</ptxt>
<ptxt>11 to 14 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Freeze Speed Info</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle speed data/min: 0 km/h (0 mph), max.: 255 km/h (158 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual vehicle speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>IG Switch (CAN)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Communication state of engine switch/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Communication is normal</ptxt>
<ptxt>OFF: Communication is interrupted</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Key Code Confirm (CAN)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Communication state of key code confirmation signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Communication is normal</ptxt>
<ptxt>OFF: Communication is interrupted</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Driver Seat Buckle SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side buckle switch signal / Set, Unset or Unknown</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>The Number of DTCs</ptxt>
</entry>
<entry valign="middle">
<ptxt>Number of diagnostic trouble codes/min.: 0, max.: 255</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual number of diagnostic trouble codes</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>