<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000X" variety="S000X">
<name>1VD-FTV COOLING</name>
<ttl id="12011_S000X_7C3K5_T00D8" variety="T00D8">
<name>WATER PUMP</name>
<para id="RM0000032KL004X" category="G" type-id="8000T" name-id="CO78E-01" from="201301" to="201308">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM0000032KL004X_01" type-id="01" category="01">
<s-1 id="RM0000032KL004X_01_0049" proc-id="RM22W0E___00006WR00000">
<ptxt>INSPECT FOR COOLANT LEAK</ptxt>
<content1 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Before performing this inspection, check that there is no coolant leaking from any parts other than the water pump. If there are leaks, inspect those areas.</ptxt>
</item>
<item>
<ptxt>Perform this inspection when the engine is cold.</ptxt>
</item>
</list1>
</atten4>
<s2>
<ptxt>Visually inspect the engine compartment for the following conditions.</ptxt>
<s3>
<ptxt>There is a large amount of coolant in the engine compartment or on the underside of the hood near the V-ribbed belt.</ptxt>
</s3>
<s3>
<ptxt>Liquid coolant is dripping from around the water pump.</ptxt>
</s3>
<s3>
<ptxt>The coolant level in the reservoir is below the L line and there are traces of leakage around the water pump.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If any of the above conditions exist, replace the water pump assembly.</ptxt>
</item>
<item>
<ptxt>If none of the above conditions exist, perform the following inspection (inspection of the area around the water pump).</ptxt>
</item>
</list1>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Inspect the area around the water pump.</ptxt>
<atten4>
<ptxt>Check for deposits around the drain plug of the water pump.</ptxt>
</atten4>
<s3>
<ptxt>Remove the V-ribbed belt of viscous heater, No. 1 idler pulley and  No. 3 idler pulley (w/ viscous heater) (See page <xref label="Seep01" href="RM0000031FK00PX_02_0248"/>).</ptxt>
</s3>
<s3>
<ptxt>Turn the V-ribbed belt tensioner bracket clockwise and  loosen the V-ribbed belt.</ptxt>
<figure>
<graphic graphicname="A174849" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>With the V-ribbed belt loosened, rotate the water pump pulley and align the water pump pulley hole with the drain plug.</ptxt>
<figure>
<graphic graphicname="A280920E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The drain plug is at the bottom of the water pump.</ptxt>
</item>
<item>
<ptxt>Use a mirror to check that the pulley hole and drain plug are aligned.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>Wrap a thin paper towel around an L-shaped tool such as a hexagon wrench, and insert it into the water pump pulley hole.</ptxt>
<figure>
<graphic graphicname="A280922E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use a 6 mm (0.236 in.) hexagon wrench.</ptxt>
</item>
<item>
<ptxt>The diameter of the water pump pulley hole is 10 mm (0.394 in.).</ptxt>
</item>
<item>
<ptxt>The distance from the water pump pulley hole to the drain plug is 28 mm (1.102 in.).</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>Move the hexagon wrench as shown in the illustration, and press the paper towel against the coolant (solid matter) adhering to the drain plug or the edge of the drain plug, and check the condition of the paper towel.</ptxt>
<figure>
<graphic graphicname="A280923E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the paper towel is wet, replace the water pump assembly.</ptxt>
</item>
<item>
<ptxt>If the paper towel is dry, clean the area around the water pump.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>Install the V-ribbed belt of viscous heater, No. 1 idler pulley and No. 3 idler pulley (w/viscous heater) (See page <xref label="Seep02" href="RM0000031FI00PX_01_0277"/>).</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032KL004X_01_0013" proc-id="RM22W0E___00006WQ00000">
<ptxt>INSPECT WATER PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the V-ribbed belt (See page <xref label="Seep01" href="RM000003AT8008X_01_0008"/>).</ptxt>
</s2>
<s2>
<ptxt>Turn the pulley and check that the water pump bearing moves smoothly and quietly.</ptxt>
<figure>
<graphic graphicname="A280767" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If it moves roughly or noisily, replace the water pump assembly.</ptxt>
</s2>
<s2>
<ptxt>Install the V-ribbed belt (See page <xref label="Seep02" href="RM000003AT8007X_01_0009"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>