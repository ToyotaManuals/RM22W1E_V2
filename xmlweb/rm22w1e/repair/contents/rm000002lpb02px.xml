<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12065_S002H" variety="S002H">
<name>SLIDING ROOF / CONVERTIBLE</name>
<ttl id="12065_S002H_7C3XH_T00QK" variety="T00QK">
<name>SLIDING ROOF HOUSING</name>
<para id="RM000002LPB02PX" category="A" type-id="80001" name-id="RF1LO-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM000002LPB02PX_01" type-id="01" category="01">
<s-1 id="RM000002LPB02PX_01_0021" proc-id="RM22W0E___0000I8500000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="2">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0001" proc-id="RM22W0E___0000I7U00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000000UZ00A8X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0002" proc-id="RM22W0E___0000I7V00000">
<ptxt>REMOVE ROOF HEADLINING ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the roof headlining (See page <xref label="Seep01" href="RM0000038MO00QX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0012" proc-id="RM22W0E___0000I8000000">
<ptxt>REMOVE CURTAIN SHIELD AIRBAG ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the curtain shield airbag (See page <xref label="Seep01" href="RM000003B7U004X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0013" proc-id="RM22W0E___0000I8100000">
<ptxt>REMOVE CURTAIN SHIELD AIRBAG ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0019" proc-id="RM22W0E___0000I8300000">
<ptxt>REMOVE REAR NO. 5 ROOF AIR DUCT (w/ Rear Cooler)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B178612" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 clips and remove the No. 5 roof air duct.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0020" proc-id="RM22W0E___0000I8400000">
<ptxt>REMOVE REAR NO. 4 ROOF AIR DUCT (w/ Rear Cooler)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the No. 5 roof air duct.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0003" proc-id="RM22W0E___0000I7W00000">
<ptxt>REMOVE SLIDING ROOF SIDE GARNISH LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B178613" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 5 claws and remove the side garnish.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0004" proc-id="RM22W0E___0000I7X00000">
<ptxt>REMOVE SLIDING ROOF SIDE GARNISH RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0005" proc-id="RM22W0E___0000I7Y00000">
<ptxt>REMOVE SLIDING ROOF GLASS SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B178614" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T25 "TORX" driver, remove the 4 screws and glass.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0018" proc-id="RM22W0E___0000I8200000">
<ptxt>REMOVE SLIDING ROOF WEATHERSTRIP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the sliding roof weatherstrip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LPB02PX_01_0008" proc-id="RM22W0E___0000I7Z00000">
<ptxt>REMOVE SLIDING ROOF HOUSING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 4 sliding roof drain hoses.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 bolts and 4 nuts.</ptxt>
<figure>
<graphic graphicname="B250190" width="7.106578999in" height="4.7836529in"/>
</figure>
</s2>
<s2>
<ptxt>Loosen the 4 nuts as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B250191" width="7.106578999in" height="4.7836529in"/>
</figure>
<atten3>
<ptxt>Be sure that the nuts are fully threaded onto the bolts.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Pull the sliding roof housing sub-assembly downward and detach the 2 claws of the sliding roof lock catch plate LH form the stud bolt as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B246952E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bracket (Roof Panel)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sliding Roof Lock Catch Plate LH Claw</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sliding Roof Housing Sub-assembly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Performing this procedure breaks the claws of the sliding roof lock catch plate LH so that the sliding roof housing sub-assembly can be removed. However, do not use excessive force.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the 4 nuts and sliding roof housing subassembly.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the sliding roof lock catch plate LH from the sliding roof housing subassembly.</ptxt>
<figure>
<graphic graphicname="B246951" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>The sliding roof lock catch plate LH is used when the vehicle is assembled at the factory and is not needed for reinstallation.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>