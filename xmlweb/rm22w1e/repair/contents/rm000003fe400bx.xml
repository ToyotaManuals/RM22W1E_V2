<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12057_S002A" variety="S002A">
<name>PRE-CRASH SAFETY</name>
<ttl id="12057_S002A_7C3UG_T00NJ" variety="T00NJ">
<name>PRE-CRASH SAFETY SYSTEM</name>
<para id="RM000003FE400BX" category="C" type-id="3036W" name-id="PC18A-02" from="201301">
<dtccode>B2055</dtccode>
<dtcname>Radar Sensor Malfunction</dtcname>
<subpara id="RM000003FE400BX_01" type-id="60" category="03" proc-id="RM22W0E___0000FO600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when there is a malfunction in the communication from the millimeter wave radar sensor. This DTC is also stored when the vehicle information sent from the millimeter wave radar sensor is abnormal.</ptxt>
<ptxt>In these cases, perform initialization so that the millimeter wave radar sensor recognizes the vehicle information.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2055</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is a malfunction in the millimeter wave radar sensor or there is a problem with the initialization.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Millimeter wave radar sensor</ptxt>
</item>
<item>
<ptxt>Seat belt control ECU</ptxt>
</item>
<item>
<ptxt>Distance control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003FE400BX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003FE400BX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003FE400BX_03_0004" proc-id="RM22W0E___0000FO800000">
<testtitle>CHECK DTC (DYNAMIC RADAR CRUISE CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Powertrain / Radar Cruise / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00MX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the following to make sure the DTC detection conditions are met.</ptxt>
<atten4>
<ptxt>If the detection conditions are not met, the system cannot detect the malfunction.</ptxt>
</atten4>
<test2>
<ptxt>Drive the vehicle at a speed of 40 km/h (25 mph) or more.</ptxt>
</test2>
<test2>
<ptxt>Turn the cruise control main switch on to set the vehicle-to-vehicle distance control mode to the standby state.</ptxt>
</test2>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000046KV00MX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC P1570 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003FE400BX_03_0002" fin="false">OK</down>
<right ref="RM000003FE400BX_03_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003FE400BX_03_0002" proc-id="RM22W0E___0000FO700000">
<testtitle>CHECK DTC (PRE-CRASH SAFETY SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Body / Pre-Crash / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000VVL03NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000VVL03NX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(DTC B2055 is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003FE400BX_03_0005" fin="true">A</down>
<right ref="RM000003FE400BX_03_0006" fin="true">B</right>
<right ref="RM000003FE400BX_03_0009" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003FE400BX_03_0005">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FE400BX_03_0008">
<testtitle>GO TO DYNAMIC RADAR CRUISE CONTROL SYSTEM<xref label="Seep01" href="RM000003FEQ00BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FE400BX_03_0006">
<testtitle>REPLACE SEAT BELT CONTROL ECU<xref label="Seep01" href="RM0000039P900MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FE400BX_03_0009">
<testtitle>REPLACE SEAT BELT CONTROL ECU<xref label="Seep01" href="RM000003A6M004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>