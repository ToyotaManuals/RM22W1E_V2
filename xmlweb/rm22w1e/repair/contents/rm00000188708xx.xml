<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM00000188708XX" category="C" type-id="3038Y" name-id="ES17H5-001" from="201308">
<dtccode>P0488</dtccode>
<dtcname>Exhaust Gas Recirculation Throttle Position Control Range / Performance</dtcname>
<dtccode>P213B</dtccode>
<dtcname>Exhaust Gas Recirculation Throttle Position Control (Range / Performance)</dtcname>
<subpara id="RM00000188708XX_01" type-id="60" category="03" proc-id="RM22W0E___00003JT00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM opens and closes the throttle valve using a rotary solenoid type actuator. Due to the opening and closing of the valve, the exhaust gas recirculation volume can be properly controlled. Also, engine vibration and noise will be reduced by closing the valve when the engine is stopped.</ptxt>
<table pgwide="1">
<title>P0488 (Bank 1), P213B (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Perform the following repeatedly for 1 minute: gradually raise the engine speed to 3000 rpm from idling over approximately 10 seconds, and then release the accelerator pedal to return to an idling state.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target and actual opening angle of the throttle valve differs for approximately 20 seconds.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Throttle valve stuck</ptxt>
</item>
<item>
<ptxt>Throttle valve does not move smoothly</ptxt>
</item>
<item>
<ptxt>Open or short in throttle control motor circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>The throttle motor activation duty exceeds the threshold for 0.5 seconds a certain number of times (40 seconds or more).</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0488</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Throttle Motor Duty</ptxt>
</item>
<item>
<ptxt>Actual Throttle Position</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P213B</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Throttle Motor Duty #2</ptxt>
</item>
<item>
<ptxt>Actual Throttle Position #2</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Reference</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Throttle Valve Position</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Moment when accelerator pedal is depressed further or released at 3000 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle valve opening angle varies smoothly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000188708XX_02" type-id="64" category="03" proc-id="RM22W0E___00003JU00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM opens and closes the throttle valve by adjusting the current flowing to the rotary solenoid with a duty ratio. If the throttle valve does not move smoothly or is stuck, the duty ratio used during valve movement control increases or decreases greatly. The ECM will determine that the throttle valve is malfunctioning and illuminate the MIL.</ptxt>
<figure>
<graphic graphicname="A128641E11" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000188708XX_03" type-id="32" category="03" proc-id="RM22W0E___00003JV00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A165769E08" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000188708XX_04" type-id="51" category="05" proc-id="RM22W0E___00003JW00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08XX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK08XX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Although the DTC titles say "exhaust gas recirculation throttle position control range / performance", these DTCs relate to the throttle valve</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188708XX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000188708XX_05_0016" proc-id="RM22W0E___00003K600001">
<testtitle>PERFORM ACTIVE TEST USING GTS (DIESEL THROTTLE TARGET ANGLE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Active Test / Diesel Throttle Target Angle or Diesel Throttle Target Angle #2.</ptxt>
</test1>
<test1>
<ptxt>While continuously changing the Active Test value to 0, 30, 60, 90, 60, 30 and 0%, check that "Actual Throttle Position" or "Actual Throttle Position #2" smoothly changes to the set opening amount.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Value smoothly changes to set opening amount.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<right ref="RM00000188708XX_05_0017" fin="false">OK</right>
<right ref="RM00000188708XX_05_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0012" proc-id="RM22W0E___00003K200001">
<testtitle>CHECK HARNESS AND CONNECTOR (THROTTLE CONTROL MOTOR POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle control motor connector.</ptxt>
<figure>
<graphic graphicname="A129055E31" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-2 (VCR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C86-2 (VCR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Bank 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Bank 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Throttle Control Motor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the throttle control motor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188708XX_05_0015" fin="false">OK</down>
<right ref="RM00000188708XX_05_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0015" proc-id="RM22W0E___00003K500001">
<testtitle>CHECK HARNESS AND CONNECTOR (GROUND CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle control motor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-3 (E2R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C86-3 (E2R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the throttle control motor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188708XX_05_0004" fin="false">OK</down>
<right ref="RM00000188708XX_05_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0004" proc-id="RM22W0E___00003JX00001">
<testtitle>CHECK HARNESS AND CONNECTOR (THROTTLE CONTROL MOTOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle control motor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-1 (VTAR) - C45-57 (LUSL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C86-1 (VTAR) - C45-56 (LUS2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C85-1 (VTAR) or C45-57 (LUSL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C86-1 (VTAR) or C45-56 (LUS2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-1 (VTAR) - C46-57 (LUSL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C86-1 (VTAR) - C46-56 (LUS2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C85-1 (VTAR) or C46-57 (LUSL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C86-1 (VTAR) or C46-56 (LUS2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the throttle control motor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188708XX_05_0017" fin="false">OK</down>
<right ref="RM00000188708XX_05_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0017" proc-id="RM22W0E___00003K700001">
<testtitle>REMOVE DEPOSIT (DIESEL THROTTLE BODY ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Visually check for deposits in the diesel throttle body assembly. If there are deposits, clean the diesel throttle body assembly.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188708XX_05_0018" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0018" proc-id="RM22W0E___00003K800001">
<testtitle>PERFORM ACTIVE TEST USING GTS (DIESEL THROTTLE TARGET ANGLE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Active Test / Diesel Throttle Target Angle or Diesel Throttle Target Angle #2.</ptxt>
</test1>
<test1>
<ptxt>While continuously changing the Active Test value to 0, 30, 60, 90, 60, 30 and 0%, check that "Actual Throttle Position" or "Actual Throttle Position #2" smoothly changes to the set opening amount.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Value smoothly changes to set opening amount.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<right ref="RM00000188708XX_05_0010" fin="false">OK</right>
<right ref="RM00000188708XX_05_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0005" proc-id="RM22W0E___00003JY00001">
<testtitle>REPLACE DIESEL THROTTLE BODY ASSEMBLY</testtitle>
<content6 releasenbr="1">
<ptxt>When DTC P0488 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the diesel throttle body assembly (for bank 1) (See page <xref label="Seep01" href="RM00000316V006X"/>).</ptxt>
</item>
</list1>
<ptxt>When DTC P213B is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the diesel throttle body assembly (for bank 2) (See page <xref label="Seep02" href="RM00000316V006X"/>).</ptxt>
</item>
</list1>
</content6>
<res>
<down ref="RM00000188708XX_05_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0010" proc-id="RM22W0E___00003K100001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK189X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off and wait for 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Over a period of 10 seconds or more, slowly raise the engine speed to 3000 rpm and lower it.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0488 or P213B is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000188708XX_05_0008" fin="false">A</down>
<right ref="RM00000188708XX_05_0011" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0008" proc-id="RM22W0E___00003JZ00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329203AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188708XX_05_0014" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0013" proc-id="RM22W0E___00003K300001">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (THROTTLE CONTROL MOTOR - INTEGRATION RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188708XX_05_0014" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0009" proc-id="RM22W0E___00003K000001">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188708XX_05_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0014" proc-id="RM22W0E___00003K400001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK189X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off and wait for 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Over a period of 10 seconds or more, slowly raise the engine speed to 3000 rpm, and then lower it.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the GTS to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Engine and ECT / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P0488 and P213B.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or N/A, over a period of 10 seconds or more, slowly raise the engine speed to 3000 rpm again, and then idle the engine for 5 minutes.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000188708XX_05_0011" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188708XX_05_0011">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>