<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LW_T00EZ" variety="T00EZ">
<name>CRUISE CONTROL SYSTEM</name>
<para id="RM000003ANM044X" category="C" type-id="302I2" name-id="CC39Z-04" from="201308">
<dtccode>P0503</dtccode>
<dtcname>Vehicle Speed Sensor "A" Intermittent / Erratic / High</dtcname>
<subpara id="RM000003ANM044X_01" type-id="60" category="03" proc-id="RM22W0E___000078Q00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If a malfunction (a rapid change in vehicle speed) in the vehicle speed signal being output from the master cylinder solenoid (skid control ECU) is detected while the cruise control is in operation, the ECM determines there to be a momentary interruption and noise, and outputs a DTC.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0503</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A momentary interruption and noise are detected when a rapid change of the vehicle speed occurs while the cruise control is in operation.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Vehicle speed sensor</ptxt>
</item>
<item>
<ptxt>Vehicle speed sensor signal circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Combination meter</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003ANM044X_03" type-id="51" category="05" proc-id="RM22W0E___000078R00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<ptxt>This DTC code may be output under the vehicle stability control system at the same time. In that case, perform troubleshooting for the vehicle stability control system first.</ptxt>
</content5>
</subpara>
<subpara id="RM000003ANM044X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003ANM044X_04_0001" proc-id="RM22W0E___000078S00001">
<testtitle>CHECK DTC (VEHICLE STABILITY CONTROL SYSTEM OR ANTI-LOCK BRAKE SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Chassis / ABS/VSC/TRAC / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002R6B01QX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(DTC is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for Vehicle Stability Control System)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for Anti-lock Brake System)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003ANM044X_04_0002" fin="false">A</down>
<right ref="RM000003ANM044X_04_0004" fin="true">B</right>
<right ref="RM000003ANM044X_04_0008" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003ANM044X_04_0002" proc-id="RM22W0E___000078T00001">
<testtitle>CHECK DTC (CRUISE CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Powertrain / Cruise Control / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002R6B01QX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the following to make sure the DTC detection conditions are met.</ptxt>
<atten4>
<ptxt>If the detection conditions are not met, the system cannot detect the malfunction.</ptxt>
</atten4>
<test2>
<ptxt>Drive the vehicle at a speed of 45 km/h (28 mph) or more.</ptxt>
</test2>
<test2>
<ptxt>Turn the cruise control main switch on.</ptxt>
</test2>
<test2>
<ptxt>Push the -SET switch to activate the cruise control.</ptxt>
</test2>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000002R6B01QX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(DTC P0503 is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for 1GR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for 1UR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for 3UR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003ANM044X_04_0003" fin="true">A</down>
<right ref="RM000003ANM044X_04_0009" fin="true">B</right>
<right ref="RM000003ANM044X_04_0006" fin="true">C</right>
<right ref="RM000003ANM044X_04_0007" fin="true">D</right>
<right ref="RM000003ANM044X_04_0010" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000003ANM044X_04_0003">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003ANM044X_04_0004">
<testtitle>GO TO VEHICLE STABILITY CONTROL SYSTEM (DTC CHART)<xref label="Seep01" href="RM0000045Z6012X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003ANM044X_04_0008">
<testtitle>GO TO ANTI-LOCK BRAKE SYSTEM (DTC CHART)<xref label="Seep01" href="RM0000045Z6012X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003ANM044X_04_0009">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292037X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003ANM044X_04_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003ANM044X_04_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292039X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003ANM044X_04_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329203AX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>