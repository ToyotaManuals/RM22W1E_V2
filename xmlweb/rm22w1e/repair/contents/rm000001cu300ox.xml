<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3OC_T00HF" variety="T00HF">
<name>ACTIVE HEIGHT CONTROL SUSPENSION</name>
<para id="RM000001CU300OX" category="D" type-id="304SG" name-id="SC0QE-10" from="201308">
<name>TEST MODE PROCEDURE</name>
<subpara id="RM000001CU300OX_z0" proc-id="RM22W0E___00009NV00001">
<content5 releasenbr="1">
<step1>
<ptxt>TEST MODE PROCEDURE</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When entering the test mode, the suspension control ECU stores all the test mode DTCs first. After completing the test mode for each check item, the test mode DTCs that are determined to be normal by the suspension control ECU will be cleared. The test mode DTCs for other check items may not be cleared when only a certain signal is inspected.</ptxt>
</item>
<item>
<ptxt>When the test mode returns back to the normal mode, all the test mode DTCs will be cleared.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Turn the engine switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG) and the tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Chassis / AHC / Utility / Signal Check.</ptxt>
</step2>
<step2>
<ptxt>Read the test mode DTCs by following the prompts on the tester screen.</ptxt>
</step2>
<step2>
<ptxt>After completing the test mode, disconnect the tester and turn the engine switch off.</ptxt>
</step2>
</step1>
<step1>
<ptxt>TEST MODE DTC CHART</ptxt>
<atten4>
<ptxt>When entering test mode, the suspension control ECU outputs all the test mode DTCs. If the clear conditions for a test mode DTC are met, it is cleared.</ptxt>
</atten4>
<ptxt>If a trouble code is output during the test mode DTC check, check the circuit listed for that code.  For details of each code, refer to the "See page" for the respective DTC code in the chart.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.20in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COLSPEC0" colwidth="2.34in"/>
<colspec colname="COLSPEC1" colwidth="1.20in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Clear Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1791</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damping force control switch circuit malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>When each position (COMF/NORMAL/SPORT) of the damping force control switch is detected.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep01" href="RM000003BBK00JX"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1792</ptxt>
</entry>
<entry valign="middle">
<ptxt>Height control switch circuit malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the height control switch up and down operation is detected.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep02" href="RM000003BBL00JX"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1794</ptxt>
</entry>
<entry valign="middle">
<ptxt>Height control off switch circuit malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the signal of the height control OFF switch is detected.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep03" href="RM000001CTS00MX"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1796</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front acceleration sensor RH circuit malfunction</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>On a level surface, the G sensor input is +/-1.96 m/s<sup>2</sup> for 1 second.</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>
<xref label="Seep04" href="RM000002PE602HX"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1797</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front acceleration sensor LH circuit malfunction</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1798</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear acceleration sensor circuit malfunction</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>