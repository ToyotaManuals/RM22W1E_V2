<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000PFV0WRX" category="C" type-id="302IG" name-id="ESREO-05" from="201301" to="201308">
<dtccode>P2111</dtccode>
<dtcname>Throttle Actuator Control System - Stuck Open</dtcname>
<dtccode>P2112</dtccode>
<dtcname>Throttle Actuator Control System - Stuck Closed</dtcname>
<subpara id="RM000000PFV0WRX_01" type-id="60" category="03" proc-id="RM22W0E___000027M00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The throttle actuator is operated by the ECM, and opens and closes the throttle valve using gears. The opening angle of the throttle valve is detected by the Throttle Position (TP) sensor, which is mounted on the throttle body. The TP sensor provides feedback to the ECM so that it can control the throttle actuator (throttle valve) appropriately in response to driver inputs.</ptxt>
<atten4>
<ptxt>This ETCS (Electronic Throttle Control System) does not use a throttle cable.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2111</ptxt>
</entry>
<entry valign="middle">
<ptxt>The throttle actuator does not close when signaled by the ECM (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Throttle actuator</ptxt>
</item>
<item>
<ptxt>Throttle body assembly</ptxt>
</item>
<item>
<ptxt>Throttle valve</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2112</ptxt>
</entry>
<entry valign="middle">
<ptxt>The throttle actuator does not open when signaled by the ECM (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Throttle actuator</ptxt>
</item>
<item>
<ptxt>Throttle body assembly</ptxt>
</item>
<item>
<ptxt>Throttle valve</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFV0WRX_02" type-id="64" category="03" proc-id="RM22W0E___000027N00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM determines that there is a malfunction in the ETCS when the throttle valve remains at a fixed angle despite a high drive current from the ECM. The ECM illuminates the MIL and stores DTC(s).</ptxt>
<ptxt>If the malfunction is not repaired successfully, a DTC is stored when the accelerator pedal is fully depressed and released quickly (to fully open and close the throttle valve) after the engine is next started.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFV0WRX_06" type-id="62" category="03" proc-id="RM22W0E___000027O00000">
<name>FAIL-SAFE</name>
<content5 releasenbr="1">
<ptxt>When either of these DTCs, or other DTCs relating to ETCS (Electronic Throttle Control System) malfunctions are stored, the ECM enters fail-safe mode. During fail-safe mode, the ECM cuts the current to the throttle actuator, and the throttle valve is returned to a 7° throttle angle by the return spring. The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel cut) and ignition timing, in accordance with the accelerator pedal opening angle, to allow the vehicle to continue at a minimal speed. If the accelerator pedal is depressed firmly and gently, the vehicle can be driven slowly.</ptxt>
<ptxt>Fail-safe mode continues until a pass condition is detected, and the engine switch is then turned off.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFV0WRX_07" type-id="32" category="03" proc-id="RM22W0E___000027P00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2102 (See page <xref label="Seep01" href="RM000000PFU0SXX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFV0WRX_08" type-id="51" category="05" proc-id="RM22W0E___000027Q00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Refer to "Data List / Active Test" [Throttle Position Command, Throttle Position No. 1, Throttle Motor Current, Throttle Motor Duty (Open), Throttle Motor Duty (Close)] (See page <xref label="Seep01" href="RM000000SXS098X"/>).</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFV0WRX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PFV0WRX_09_0001" proc-id="RM22W0E___000027R00000">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P2111 OR P2112)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P2111 or P2112 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2111 or P2112 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P2111 or P2112 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PFV0WRX_09_0002" fin="false">A</down>
<right ref="RM000000PFV0WRX_09_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PFV0WRX_09_0002" proc-id="RM22W0E___000027S00000">
<testtitle>INSPECT THROTTLE BODY ASSEMBLY (VISUALLY CHECK THROTTLE VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for contamination between the throttle valve and the housing. If necessary, clean the throttle body. And check that the throttle valve moves smoothly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Throttle valve is not contaminated with foreign objects and moves smoothly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PFV0WRX_09_0008" fin="false">OK</down>
<right ref="RM000000PFV0WRX_09_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFV0WRX_09_0008" proc-id="RM22W0E___000027U00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (THROTTLE POSITION))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK14HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Throttle Position No. 1, Throttle Position No. 2 and Throttle Position Command.</ptxt>
</test1>
<test1>
<ptxt>Check the values displayed on the tester while wiggling the ECM wire harness.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Value in Data List changes when wire harness is wiggled, or DTC is output*</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Other than above</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>*: As the DTC was stored due to a change in the contact resistance of the connector, repair or replace the wire harness or connector (See page <xref label="Seep02" href="RM000000UZ30DCX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFV0WRX_09_0010" fin="true">A</down>
<right ref="RM000000PFV0WRX_09_0003" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000PFV0WRX_09_0009" proc-id="RM22W0E___000027V00000">
<testtitle>REPLACE THROTTLE BODY ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the throttle body assembly (See page <xref label="Seep01" href="RM000002PQN035X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFV0WRX_09_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PFV0WRX_09_0003" proc-id="RM22W0E___000027T00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P2111 OR P2112)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK14HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine, and fully depress and release the accelerator pedal quickly (to fully open and close the throttle valve).</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2111 or P2112 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PFV0WRX_09_0012" fin="true">A</down>
<right ref="RM000000PFV0WRX_09_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PFV0WRX_09_0004">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000032SF04BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFV0WRX_09_0010">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000PFV0WRX_09_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFV0WRX_09_0012">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>