<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3ML_T00FO" variety="T00FO">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV)</name>
<para id="RM000000WP10H0X" category="J" type-id="30128" name-id="AT71E-08" from="201308">
<dtccode/>
<dtcname>Transmission Control Switch Circuit</dtcname>
<subpara id="RM000000WP10H0X_01" type-id="60" category="03" proc-id="RM22W0E___000080X00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When moving the shift lever to S using the transmission control switch, it is possible to switch the shift range between "1" (1st range) and "6" (6th range).</ptxt>
<ptxt>Shifting to "+" once raises the shift range by one, and shifting to "-" lowers the shift range by one.</ptxt>
</content5>
</subpara>
<subpara id="RM000000WP10H0X_02" type-id="32" category="03" proc-id="RM22W0E___000080Y00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C176192E19" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000WP10H0X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000WP10H0X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000WP10H0X_04_0001" proc-id="RM22W0E___000080Z00001">
<testtitle>INSPECT TRANSMISSION CONTROL SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the transmission control switch connector.</ptxt>
<figure>
<graphic graphicname="C146679E06" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>3 (IG) - 7 (S)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S, "+" or "-"</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (SFTU) - 5 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever held in "+"</ptxt>
<ptxt>(Up-shift)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 (SFTD) - 5 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever held in "-"</ptxt>
<ptxt>(Down-shift)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 (IG) - 7 (S)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in S, "+" or "-"</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (SFTU) - 5 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 (SFTD) - 5 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WP10H0X_04_0002" fin="false">OK</down>
<right ref="RM000000WP10H0X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WP10H0X_04_0002" proc-id="RM22W0E___000081000001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSMISSION CONTROL SWITCH - BATTERY, BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the transmission control switch connector.</ptxt>
<figure>
<graphic graphicname="C146729E14" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E52-3 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E52-3 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E52-5 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WP10H0X_04_0003" fin="false">OK</down>
<right ref="RM000000WP10H0X_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WP10H0X_04_0003" proc-id="RM22W0E___000081100001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSMISSION CONTROL SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="C177782E14" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A38-25 (S) - Body ground </ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-25 (S) - Body ground </ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A52-25 (S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A52-25 (S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A38-60 (SFTU) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever held in "+"</ptxt>
<ptxt>(Up-shift)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-59 (SFTD) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever held in "-"</ptxt>
<ptxt>(Down-shift)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-60 (SFTU) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-59 (SFTD) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A52-60 (SFTU) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever held in "+"</ptxt>
<ptxt>(Up-shift)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A52-59 (SFTD) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever held in "-"</ptxt>
<ptxt>(Down-shift)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A52-60 (SFTU) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A52-59 (SFTD) - Body ground </ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000WP10H0X_04_0008" fin="true">OK</down>
<right ref="RM000000WP10H0X_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WP10H0X_04_0008">
<testtitle>PROCEED TO NEXT CIRCUIT INSPECTION SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000W730ZLX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WP10H0X_04_0009">
<testtitle>REPLACE TRANSMISSION CONTROL SWITCH (LOWER SHIFT LEVER ASSEMBLY)<xref label="Seep01" href="RM000002YBF03JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WP10H0X_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000WP10H0X_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>