<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3D2_T0065" variety="T0065">
<name>ECM</name>
<para id="RM000003292038X" category="A" type-id="80001" name-id="ES10NN-003" from="201308">
<name>REMOVAL</name>
<subpara id="RM000003292038X_01" type-id="01" category="01">
<s-1 id="RM000003292038X_01_0006" proc-id="RM22W0E___000014300001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="2">
<atten3>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003292038X_01_0007" proc-id="RM22W0E___000014400001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="2">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003292038X_01_0001" proc-id="RM22W0E___000014000001">
<ptxt>DISCONNECT CONNECTOR HOLDER BLOCK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and move the connector holder block so that the ECM can be removed in the next step.</ptxt>
<figure>
<graphic graphicname="A183089E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000003292038X_01_0002" proc-id="RM22W0E___000014100001">
<ptxt>REMOVE ECM</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Raise the 2 levers while pushing the locks on the 2 levers.</ptxt>
<figure>
<graphic graphicname="A182753E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Lock</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure that the lock levers are raised 90° as shown in the illustration before disconnecting the ECM connectors. Failure to do this may cause the ECM connectors to break.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 bolts and ECM.</ptxt>
<figure>
<graphic graphicname="A155993" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003292038X_01_0003" proc-id="RM22W0E___000014200001">
<ptxt>REMOVE GASKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Peel off the gasket.</ptxt>
<figure>
<graphic graphicname="A167150" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Spray gasket remover or equivalent on the remaining tape of the gasket.</ptxt>
<figure>
<graphic graphicname="A185949E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Cloth</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>When using gasket remover or equivalent, cover the ECM connectors with a cloth.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the tape of the gasket without using bladed objects.</ptxt>
<figure>
<graphic graphicname="A163829" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>