<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001Q" variety="S001Q">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001Q_7C3Q2_T00J5" variety="T00J5">
<name>VANE PUMP (for 1GR-FE)</name>
<para id="RM000002H5904NX" category="A" type-id="30014" name-id="PA1GQ-02" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000002H5904NX_01" type-id="01" category="01">
<s-1 id="RM000002H5904NX_01_0001" proc-id="RM22W0E___0000B1I00001">
<ptxt>INSTALL VANE PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the vane pump with the 2 bolts.</ptxt>
<figure>
<graphic graphicname="F051260E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5904NX_01_0002" proc-id="RM22W0E___0000B1J00001">
<ptxt>CONNECT PRESSURE FEED TUBE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172778E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install a new gasket to the pressure feed tube.</ptxt>
</s2>
<s2>
<ptxt>Connect the pressure feed tube and install the union bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>50</t-value1>
<t-value2>510</t-value2>
<t-value4>37</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5904NX_01_0003" proc-id="RM22W0E___0000B1K00001">
<ptxt>CONNECT NO. 1 OIL RESERVOIR TO PUMP HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No.1 oil reservoir to pump hose to the vane pump assembly with the clip.</ptxt>
<figure>
<graphic graphicname="C172780" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5904NX_01_0009" proc-id="RM22W0E___0000B1L00001">
<ptxt>CONNECT POWER STEERING OIL PRESSURE SWITCH CONNECTOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172779" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the wire harness clamp to the bracket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5904NX_01_0010" proc-id="RM22W0E___000042G00000">
<ptxt>INSTALL FAN AND GENERATOR V BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the V belt onto every part.</ptxt>
<figure>
<graphic graphicname="A225650E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Vane Pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Water Pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>No. 2 Idler</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Generator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Cooler Compressor or Idler Pulley</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry>
<ptxt>No. 1 Idler</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry>
<ptxt>Crankshaft</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry>
<ptxt>V-ribbed Belt Tensioner</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>While turning the belt tensioner counterclockwise, remove the pin.</ptxt>
<atten3>
<ptxt>Make sure that the V belt is properly installed to each pulley.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Check that the belt fits properly in the ribbed grooves.</ptxt>
<atten4>
<ptxt>Make sure to check by hand that the belt has not slipped out of the grooves on the bottom of the pulley.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002H5904NX_01_0022" proc-id="RM22W0E___00000Z200001">
<ptxt>INSTALL V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 2 V-bank cover grommets with the 2 pins and press down on the V-bank cover to attach the pins.</ptxt>
<figure>
<graphic graphicname="A271365E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002H5904NX_01_0024">
<ptxt>INSTALL FRONT WHEEL RH</ptxt>
</s-1>
<s-1 id="RM000002H5904NX_01_0005">
<ptxt>ADD POWER STEERING FLUID</ptxt>
</s-1>
<s-1 id="RM000002H5904NX_01_0015" proc-id="RM22W0E___000010H00000">
<ptxt>BLEED POWER STEERING FLUID
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the fluid level.</ptxt>
</s2>
<s2>
<ptxt>Jack up the front of the vehicle and support it with stands.</ptxt>
</s2>
<s2>
<ptxt>Turn the steering wheel.</ptxt>
<s3>
<ptxt>With the engine stopped, turn the wheel slowly from lock to lock several times.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Lower the vehicle.</ptxt>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>Idle the engine for a few minutes.</ptxt>
</s2>
<s2>
<ptxt>Turn the steering wheel.</ptxt>
<s3>
<ptxt>With the engine idling, turn the wheel left or right to the full lock position and keep it there for 2 to 3 seconds, then turn the wheel to the opposite full lock position and keep it there for 2 to 3 seconds. *1</ptxt>
</s3>
<s3>
<ptxt>Repeat *1 several times.</ptxt>
<atten3>
<ptxt>For vehicles with VGRS, if the steering wheel is turned from lock to lock repeatedly, the system may stop operating and the amount of rotation before the steering wheel locks may increase due to operation of the overheating prevention function. When the system temperature drops, the system operation automatically returns to normal.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Check for foaming or emulsification.</ptxt>
<figure>
<graphic graphicname="C124274E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Correct</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Incorrect</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the system has to be bled twice because of foaming or emulsification, check for fluid leaks in the system.</ptxt>
</s2>
<s2>
<ptxt>Check the fluid level.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002H5904NX_01_0007">
<ptxt>INSPECT FOR POWER STEERING FLUID LEAK</ptxt>
</s-1>
<s-1 id="RM000002H5904NX_01_0011" proc-id="RM22W0E___0000B1M00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>