<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0019" variety="S0019">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0019_7C3M2_T00F5" variety="T00F5">
<name>AUTOMATIC TRANSMISSION FLUID</name>
<para id="RM000002BL003UX" category="N" type-id="3000G" name-id="AT8T0-02" from="201301">
<name>ADJUSTMENT</name>
<subpara id="RM000002BL003UX_01" type-id="01" category="01">
<s-1 id="RM000002BL003UX_01_0001" proc-id="RM22W0E___00007DO00000">
<ptxt>BEFORE FILLING TRANSMISSION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C157019E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<list1 type="unordered">
<item>
<ptxt>This transmission requires Toyota Genuine ATF WS transmission fluid.</ptxt>
</item>
<item>
<ptxt>After servicing the transmission, you must refill the transmission with the correct amount of fluid.</ptxt>
</item>
<item>
<ptxt>Keep the vehicle level while adjusting the fluid level.</ptxt>
</item>
<item>
<ptxt>Proceed to the "Transmission Pan Fill" procedures if you replaced the entire transmission, transmission pan, drain plug, valve body and/or torque converter.</ptxt>
</item>
<item>
<ptxt>Proceed to the ''Transmission Fill'' procedures after removing the refill plug if you replaced the transmission hose and/or output shaft oil seal.</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM000002BL003UX_01_0002" proc-id="RM22W0E___00007DP00000">
<ptxt>TRANSMISSION PAN FILL</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C113723E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the refill plug and overflow plug.</ptxt>
</s2>
<s2>
<ptxt>Fill the transmission through the refill hole until fluid begins to trickle out of the overflow tube.</ptxt>
</s2>
<s2>
<ptxt>Reinstall the overflow plug.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BL003UX_01_0003" proc-id="RM22W0E___00007DQ00000">
<ptxt>TRANSMISSION FILL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fill the transmission with the amount of fluid listed in the table below.</ptxt>
<spec>
<title>Standard Capacity</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Repair</ptxt>
</entry>
<entry align="center">
<ptxt>Fill Amount</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Transmission pan and drain plug removal</ptxt>
</entry>
<entry>
<ptxt>1.7 liters (1.8 US qts, 1.5 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Transmission valve body removal</ptxt>
</entry>
<entry>
<ptxt>4.3 liters (4.5 US qts, 3.8 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Torque converter removal</ptxt>
</entry>
<entry>
<ptxt>5.4 liters (5.7 US qts, 4.8 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Reinstall the refill plug to prevent the fluid from splashing.</ptxt>
<atten4>
<ptxt>If you cannot add the listed amount of fluid, do the following:</ptxt>
</atten4>
<s3>
<ptxt>Install the refill plug.</ptxt>
</s3>
<s3>
<ptxt>Allow the engine to idle with the air conditioning off.</ptxt>
</s3>
<s3>
<ptxt>Move the shift lever through the entire gear range to circulate fluid.</ptxt>
</s3>
<s3>
<ptxt>Wait for 30 seconds with the engine idling.</ptxt>
</s3>
<s3>
<ptxt>Stop the engine.</ptxt>
</s3>
<s3>
<ptxt>Remove the refill plug and add fluid.</ptxt>
</s3>
<s3>
<ptxt>Reinstall the refill plug.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BL003UX_01_0005" proc-id="RM22W0E___00007DR00000">
<ptxt>ADJUST FLUID TEMPERATURE</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>The fluid temperature can be confirmed by checking the indicator light in the meter or by using the GTS. When using the GTS, it is necessary to change to temperature detection mode in order to idle the vehicle appropriately.</ptxt>
</atten3>
<s2>
<ptxt>When using the GTS:</ptxt>
<s3>
<ptxt>Turn the engine switch off.</ptxt>
</s3>
<s3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the engine switch on (IG).</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Connect the TC and TE1.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / ECT / Data List / A/T Oil Temperature 1.</ptxt>
</s3>
<s3>
<ptxt>Check the ATF temperature.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The ATF temperature must be between 38 to 45°C (100 to 113°F).</ptxt>
</item>
<item>
<ptxt>If the ATF temperature is not between 38 to 45°C (100 to 113°F), turn the engine switch off and wait until the fluid temperature drops between 38 to 45°C (100 to 113°F).</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>According to the display on the GTS, perform the Active Test "Connect the TC and TE1".</ptxt>
<atten4>
<ptxt>Indicator lights of the meter blink to output DTCs when TC and TE1 are connected.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Start the engine.</ptxt>
<atten3>
<ptxt>Check that electrical systems such as the air conditioning system, audio system and lighting system are off.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>When not using the GTS:</ptxt>
<figure>
<graphic graphicname="H102392E90" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Using SST, connect terminals 13 (TC) and 4 (CG) of the DLC3.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
</s3>
<s3>
<ptxt>Start the engine.</ptxt>
<atten3>
<ptxt>Check that electrical systems such as the air conditioning system, audio system and lighting system are off.</ptxt>
</atten3>
<atten4>
<ptxt>Indicator lights of the meter blink to output DTCs when terminals TC and CG are connected.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Slowly move the shift lever from P to S, then change the gears from 1st to 6th. Then return the shift lever to P.</ptxt>
<atten4>
<ptxt>Slowly move the shift lever to circulate the fluid through each part of the transmission.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Move the shift lever to D, and quickly move back and forth between N and D (once within 1.5 seconds) for at least 6 seconds. This will activate the fluid temperature detection mode.</ptxt>
<figure>
<graphic graphicname="C172430E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard condition</title>
<specitem>
<ptxt>Indicator light (D) remains illuminated for 2 seconds and then turns off.</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>When using the GTS:</ptxt>
<s3>
<ptxt>Return the shift lever to P and press OFF on the Active Test display.</ptxt>
</s3>
</s2>
<s2>
<ptxt>When not using the GTS:</ptxt>
<s3>
<ptxt>Return the shift lever to P and disconnect terminals 13 (TC) and 4 (CG).</ptxt>
</s3>
</s2>
<s2>
<ptxt>Allow the engine to idle until the fluid temperature reaches 38 to 45°C (100 to 113°F).</ptxt>
</s2>
<s2>
<ptxt>The indicator (D) will come on again when the fluid temperature reaches 38°C (100°F) and will blink when it exceeds 46°C (113°F).</ptxt>
<spec>
<title>Indicator Indication of ATF Temperature</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Below Proper Temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proper Temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than Proper Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Data List [A/T Oil Temperature 1]</ptxt>
<ptxt>38°C (100°F) or less</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List [A/T Oil Temperature 1]</ptxt>
<ptxt>38 to 45°C (100 to 113°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List [A/T Oil Temperature 1]</ptxt>
<ptxt>45°C (113°F) or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Indicator light (D)</ptxt>
<ptxt>Turns off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Indicator light (D)</ptxt>
<ptxt>Turns on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Indicator light (D)</ptxt>
<ptxt>Blinking</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Perform the fluid level inspection while the indicator light is on.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BL003UX_01_0006" proc-id="RM22W0E___00007DS00000">
<ptxt>FLUID LEVEL CHECK</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>The fluid temperature must be between 38 to 45°C (100 to 113°F) to accurately check the fluid level.</ptxt>
</atten3>
<s2>
<ptxt>Remove the overflow plug with the engine idling.</ptxt>
<figure>
<graphic graphicname="C113723E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Check that fluid comes out of the overflow tube.</ptxt>
<ptxt>If fluid does not come out, proceed to the "Transmission Refill" procedures.</ptxt>
<ptxt>If fluid comes out, wait until the overflow slows to a trickle and proceed to the "Complete" procedures.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BL003UX_01_0007" proc-id="RM22W0E___00007DT00000">
<ptxt>TRANSMISSION REFILL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the overflow plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the refill plug.</ptxt>
</s2>
<s2>
<ptxt>Add ATF into the refill hole until ATF flows from the overflow tube.</ptxt>
</s2>
<s2>
<ptxt>Wait until the overflow slows to a trickle and proceed to the ''COMPLETE'' procedures.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BL003UX_01_0008" proc-id="RM22W0E___00007DU00000">
<ptxt>COMPLETE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the overflow plug.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket and the refill plug.</ptxt>
<torque>
<torqueitem>
<t-value1>39</t-value1>
<t-value2>400</t-value2>
<t-value4>29</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>w/o Air Cooled Transmission Oil Cooler (w/ ATF Warmer):</ptxt>
<figure>
<graphic graphicname="C161813E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the pin.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure the shaft of the thermostat is protruding from the hole of the cap.</ptxt>
</item>
<item>
<ptxt>Check that there is no ATF leaking from the cap hole.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>