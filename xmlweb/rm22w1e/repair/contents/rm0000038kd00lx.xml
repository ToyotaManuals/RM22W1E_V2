<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C3ZW_T00SZ" variety="T00SZ">
<name>NAME PLATE (w/ Tire Carrier)</name>
<para id="RM0000038KD00LX" category="A" type-id="80001" name-id="ET22D-01" from="201301">
<name>REMOVAL</name>
<subpara id="RM0000038KD00LX_01" type-id="11" category="10" proc-id="RM22W0E___0000JJ900000">
<content3 releasenbr="1">
<atten4>
<ptxt>When removing the emblem, rear body name plate, back door name plate and rear license light cover, heat the vehicle body, emblem, rear body name plate, back door name plate and rear license light cover using a heat light.</ptxt>
</atten4>
<spec>
<title>Standard Heating Temperature</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Body</ptxt>
</entry>
<entry valign="middle">
<ptxt>40 to 60°C (104 to 140°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Emblem</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear Body Name Plate</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Back Door Name Plate</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear License Light Cover</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not heat the vehicle body, emblem, rear body name plate, back door name plate and rear license light cover excessively.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM0000038KD00LX_02" type-id="01" category="01">
<s-1 id="RM0000038KD00LX_02_0001" proc-id="RM22W0E___0000JJA00000">
<ptxt>REMOVE SYMBOL EMBLEM</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185937E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Put protective tape around the emblem.</ptxt>
</s2>
<s2>
<ptxt>Insert a piano wire between the vehicle body and emblem.</ptxt>
<atten4>
<ptxt>Be careful of the boss position.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Tie objects that can serve as handles (for example, wooden blocks) to both wire ends.</ptxt>
</s2>
<s2>
<ptxt>Pull the piano wire and scrape off the double-sided tape that holds the emblem to the vehicle body.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the emblem.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038KD00LX_02_0002" proc-id="RM22W0E___0000JJB00000">
<ptxt>REMOVE REAR NO. 3 BODY NAME PLATE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185938E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the rear body name plate.</ptxt>
</s2>
<s2>
<ptxt>Insert a piano wire between the vehicle body and rear body name plate.</ptxt>
</s2>
<s2>
<ptxt>Tie objects that can serve as handles (for example, wooden blocks) to both wire ends.</ptxt>
</s2>
<s2>
<ptxt>Pull the piano wire and scrape off the double-sided tape that holds the rear body name plate to the vehicle body.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the rear body name plate.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038KD00LX_02_0003" proc-id="RM22W0E___0000JJC00000">
<ptxt>REMOVE NO. 5 BACK DOOR NAME PLATE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185939E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the back door name plate.</ptxt>
</s2>
<s2>
<ptxt>Insert a piano wire between the vehicle body and back door name plate.</ptxt>
</s2>
<s2>
<ptxt>Tie objects that can serve as handles (for example, wooden blocks) to both wire ends.</ptxt>
</s2>
<s2>
<ptxt>Pull the piano wire and scrape off the double-sided tape that holds the back door name plate to the vehicle body.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the back door name plate.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038KD00LX_02_0004" proc-id="RM22W0E___0000IBA00000">
<ptxt>REMOVE REAR LICENSE LIGHT COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B188295E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the rear license light cover.</ptxt>
</s2>
<s2>
<ptxt>Detach the 7 claws and remove the double-sided tape to remove the rear license light cover.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>