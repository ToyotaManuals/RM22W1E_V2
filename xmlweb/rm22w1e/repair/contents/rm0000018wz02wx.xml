<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM0000018WZ02WX" category="C" type-id="302LP" name-id="ESVZZ-17" from="201308">
<dtccode>P2237</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit / Open (Bank 1 Sensor 1)</dtcname>
<dtccode>P2238</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P2239</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit High (Bank 1 Sensor 1)</dtcname>
<dtccode>P2240</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit / Open (Bank 2 Sensor 1)</dtcname>
<dtccode>P2241</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit Low (Bank 2 Sensor 1)</dtcname>
<dtccode>P2242</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit High (Bank 2 Sensor 1)</dtcname>
<dtccode>P2252</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P2253</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit High (Bank 1 Sensor 1)</dtcname>
<dtccode>P2255</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit Low (Bank 2 Sensor 1)</dtcname>
<dtccode>P2256</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit High (Bank 2 Sensor 1)</dtcname>
<subpara id="RM0000018WZ02WX_01" type-id="60" category="03" proc-id="RM22W0E___00003F100001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>For more information on the air fuel ratio sensor and Diesel Particulate Filter (DPF), refer to the following procedures (See page <xref label="Seep01" href="RM000000XSN037X"/>).</ptxt>
</item>
<item>
<ptxt>If P2237, P2238, P2239, P2240, P2441 or P2442 is present, refer to the DTC table for DPF system (See page <xref label="Seep02" href="RM000000XSN037X"/>).</ptxt>
</item>
<item>
<ptxt>These DTCs are recorded when the air fuel ratio sensor has a malfunction, although the title says oxygen sensor.</ptxt>
</item>
</list1>
</atten4>
<ptxt>The air fuel ratio sensor has the characteristic of providing output voltage which is proportional to the air-fuel ratio. The air fuel ratio sensor output voltage* is used by the ECM to control the air-fuel ratio.</ptxt>
<ptxt>The air fuel ratio sensor is located after the DPF catalytic converter. This sensor has been developed based on the structure and technology of a sensor that is used for gasoline engines. The cover portion of the sensor has been changed for use in the diesel engine with DPF in order to eliminate the influence of the sensor temperature and particulate matter (PM).</ptxt>
<ptxt>In order to reduce PM, the ECM adjusts the air-fuel ratio to a slightly rich value (but it is lean compared with the stoichiometric air-fuel ratio) based on signals from the air fuel ratio sensor. When the ECM performs PM forced regeneration using adding fuel by the exhaust fuel addition injector, the air-fuel ratio is also properly adjusted using the sensor.</ptxt>
<ptxt>*: The voltage value changes inside the ECM only.</ptxt>
<figure>
<graphic graphicname="A258975E04" width="7.106578999in" height="9.803535572in"/>
</figure>
<atten4>
<ptxt>The ECM provides a pulse width modulated control circuit to adjust current through the heater. The air fuel ratio sensor heater circuit uses a relay on the +B side of the circuit.</ptxt>
</atten4>
<figure>
<graphic graphicname="A210271E09" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>P2237 (Bank 1), P2240 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Drive the vehicle at 50 km/h (30 mph) or more for a total of 300 seconds or more.</ptxt>
</item>
<item>
<ptxt>Check that "Exhaust Temperature B1S1" displays 200°C (392°F) or higher using the GTS.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When either of the following conditions is met (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor circuit malfunction</ptxt>
</item>
<item>
<ptxt>The resistance of the air fuel ratio sensor element is 250 Ω or higher for 5 seconds or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Main trouble area: Open in air fuel ratio sensor circuit</ptxt>
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2238 (Bank 1), P2241 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 7 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor (for bank 1, 2) circuit low</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main trouble area: Open in air fuel ratio sensor circuit</ptxt>
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2239 (Bank 1), P2242 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 7 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor (for bank 1, 2) circuit high</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main trouble area: Short in air fuel ratio sensor circuit</ptxt>
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2252 (Bank 1), P2255 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 7 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor (for bank 1, 2) circuit low</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main trouble area: Open in air fuel ratio sensor circuit</ptxt>
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2253 (Bank 1), P2256 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 7 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor (for bank 1, 2) circuit high</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main trouble area: Short in air fuel ratio sensor circuit</ptxt>
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P2237</ptxt>
<ptxt>P2238</ptxt>
<ptxt>P2239</ptxt>
<ptxt>P2252</ptxt>
<ptxt>P2253</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>AFS Voltage B1S1</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B1S1</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2240</ptxt>
<ptxt>P2241</ptxt>
<ptxt>P2242</ptxt>
<ptxt>P2255</ptxt>
<ptxt>P2256</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>AFS Voltage B2S1</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B2S1</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000018WZ02WX_02" type-id="64" category="03" proc-id="RM22W0E___00003F200001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The air fuel ratio sensor outputs a voltage in proportion to the air-fuel ratio. If impedance (alternating current resistance) or voltage output of the sensor deviates greatly from the standard range, the ECM interprets this as an open or short malfunction of the air fuel ratio sensor circuit.</ptxt>
</content5>
</subpara>
<subpara id="RM0000018WZ02WX_03" type-id="32" category="03" proc-id="RM22W0E___00003F300001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0031 (See page <xref label="Seep01" href="RM00000187I03PX_03"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000018WZ02WX_04" type-id="51" category="05" proc-id="RM22W0E___00003F400001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08XX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK08XX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000018WZ02WX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000018WZ02WX_05_0004" proc-id="RM22W0E___00003F500001">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR FUEL RATIO SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C159-3 (AF1+) - C45-102 (A1A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C159-4 (AF1-) - C45-103 (A1A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-3 (AF2+) - C45-125 (A2A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-4 (AF2-) - C45-126 (A2A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C159-3 (AF1+) or C45-102 (A1A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C159-4 (AF1-) or C45-103 (A1A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-3 (AF2+) or C45-125 (A2A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-4 (AF2-) or C45-126 (A2A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C159-3 (AF1+) - C46-102 (A1A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C159-4 (AF1-) - C46-103 (A1A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-3 (AF2+) - C46-125 (A2A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-4 (AF2-) - C46-126 (A2A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C159-3 (AF1+) or C46-102 (A1A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C159-4 (AF1-) or C46-103 (A1A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-3 (AF2+) or C46-125 (A2A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-4 (AF2-) or C46-126 (A2A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000018WZ02WX_05_0006" fin="false">OK</down>
<right ref="RM0000018WZ02WX_05_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018WZ02WX_05_0006" proc-id="RM22W0E___00003F700001">
<testtitle>REPLACE AIR FUEL RATIO SENSOR</testtitle>
<content6 releasenbr="1">
<ptxt>When DTC P2237, P2238, P2239, P2252 or P2253 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the air fuel ratio sensor (for bank 1) (See page <xref label="Seep01" href="RM000004KGB00HX"/>).</ptxt>
</item>
</list1>
<ptxt>When DTC P2240, P2241, P2242, P2255 or P2256 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the air fuel ratio sensor (for bank 2) (See page <xref label="Seep02" href="RM000004KGB00HX"/>).</ptxt>
</item>
</list1>
</content6>
<res>
<down ref="RM0000018WZ02WX_05_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018WZ02WX_05_0011" proc-id="RM22W0E___00003FA00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Warm up the engine.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK189X"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at 50 km/h (30 mph) or more for a total of 300 seconds or more.</ptxt>
<atten4>
<ptxt>Make sure that the exhaust gas temperatures (Exhaust Temperature B1S1 and Exhaust Temperature B2S1) are 200°C (392°F) or higher.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000018WZ02WX_05_0010" fin="true">OK</down>
<right ref="RM0000018WZ02WX_05_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018WZ02WX_05_0005" proc-id="RM22W0E___00003F600001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329203AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM0000018WZ02WX_05_0009" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM0000018WZ02WX_05_0008" proc-id="RM22W0E___00003F800001">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000018WZ02WX_05_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018WZ02WX_05_0009" proc-id="RM22W0E___00003F900001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Warm up the engine.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK189X"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at 50 km/h (30 mph) or more for a total of 300 seconds or more.</ptxt>
<atten4>
<ptxt>Make sure that the exhaust gas temperatures (Exhaust Temperature B1S1 and Exhaust Temperature B2S1) are 200°C (392°F) or higher.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000018WZ02WX_05_0010" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018WZ02WX_05_0010">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>