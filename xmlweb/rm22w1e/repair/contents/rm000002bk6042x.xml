<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000A" variety="S000A">
<name>1GR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000A_7C3EU_T007X" variety="T007X">
<name>CYLINDER BLOCK</name>
<para id="RM000002BK6042X" category="A" type-id="8000E" name-id="EMBT1-01" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM000002BK6042X_01" type-id="01" category="01">
<s-1 id="RM000002BK6042X_01_0072" proc-id="RM22W0E___000045Y00000">
<ptxt>INSTALL STUD BOLT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install new stud bolts.</ptxt>
<torque>
<subtitle>for stud bolt A and C</subtitle>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
<subtitle>for stud bolt B and D</subtitle>
<torqueitem>
<t-value1>4.0</t-value1>
<t-value2>41</t-value2>
<t-value3>35</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A221128E03" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upper Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lower Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK6042X_01_0005" proc-id="RM22W0E___000045R00000">
<ptxt>INSTALL NO. 1 OIL NOZZLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, install the 3 oil nozzles with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A120734" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>9.0</t-value1>
<t-value2>92</t-value2>
<t-value3>80</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK6042X_01_0007" proc-id="RM22W0E___000045S00000">
<ptxt>INSTALL PISTON WITH PIN SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, install a new snap ring on one side of the piston pin hole.</ptxt>
<figure>
<graphic graphicname="A221130" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Be sure that the end gap of the snap ring is not aligned with the pin hole cutout portion of the piston.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Gradually heat the piston to approximately 80°C (176°F).</ptxt>
</s2>
<s2>
<ptxt>Coat the piston pin with engine oil.</ptxt>
<figure>
<graphic graphicname="A221131E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Align the front marks of the piston and connecting rod, insert the connecting rod into the piston, and then push in the piston pin with your thumb until the pin comes into contact with the snap ring.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The piston and pin are a matched set.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Check the fitting condition between the piston and piston pin by trying to move the piston back and forth on the piston pin.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, install a new snap ring at the other end of the piston pin hole.</ptxt>
<figure>
<graphic graphicname="A223711" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Be sure that the end gap of the snap ring is not aligned with the pin hole cutout portion of the piston.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK6042X_01_0009" proc-id="RM22W0E___000045T00000">
<ptxt>INSTALL PISTON RING SET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the oil ring (expander) and 2 side rails by hand.</ptxt>
</s2>
<s2>
<ptxt>Using a piston ring expander, install the 2 compression rings with the painted mark on the right side.</ptxt>
<figure>
<graphic graphicname="A221133E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Painted Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Compression Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Compression Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Right Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Position the piston rings so that the ring ends are as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A221134E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Compression Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Compression Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lower Side Rail</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upper Side Rail</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Expander</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK6042X_01_0011" proc-id="RM22W0E___000045V00000">
<ptxt>INSTALL CRANKSHAFT BEARING</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Main bearings come in widths of 17.3 mm (0.681 in.) and 19.9 mm (0.783 in.). Install the 19.9 mm (0.783 in.) bearings in the No. 1 and No. 4 cylinder block journal positions with the bearing caps. Install the 17.3 mm (0.681 in.) bearings in the No. 2 and No. 3 positions.</ptxt>
</atten4>
<figure>
<graphic graphicname="A223696E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 and No. 4 Journal Bearings</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 and No. 3 Journal Bearings</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>Clean each main journal and bearing.</ptxt>
</s2>
<s2>
<ptxt>Install the upper bearing.</ptxt>
<s3>
<ptxt>Install the upper bearing to the cylinder block as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A183116E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not apply engine oil to the bearings or contact surfaces.</ptxt>
</item>
<item>
<ptxt>Both sides of the oil groove in the cylinder block should be visible through the oil feed holes in the bearing. The amount visible on each side of the holes should be equal.</ptxt>
</item>
<item>
<ptxt>Do not allow coolant to come into contact with the inner surface of the bearing.</ptxt>
</item>
<item>
<ptxt>If any coolant comes into contact with the inner surface of the bearing, replace the bearing with a new one.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Install the lower bearing.</ptxt>
<figure>
<graphic graphicname="A183118E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Install the lower bearings to the crankshaft bearing caps.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mark 1, 2, 3 or 4</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vernier Caliper</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Using a vernier caliper, measure the distance between the edge of the crankshaft bearing cap and the edge of the lower bearing.</ptxt>
<spec>
<title>Dimension A - B or B - A</title>
<specitem>
<ptxt>0 to 0.7 mm (0 to 0.0276 in.)</ptxt>
</specitem>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not apply engine oil to the bearings or contact surfaces.</ptxt>
</item>
<item>
<ptxt>Do not allow coolant to come into contact with the inner surface of the bearing.</ptxt>
</item>
<item>
<ptxt>If any coolant comes into contact with the inner surface of the bearing, replace the bearing with a new one.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK6042X_01_0012" proc-id="RM22W0E___000045W00000">
<ptxt>INSTALL CRANKSHAFT</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Clean the contact surface of each main journal and crank pin.</ptxt>
</atten3>
<s2>
<ptxt>Apply new engine oil to the upper bearing and install the crankshaft to the cylinder block.</ptxt>
</s2>
<s2>
<ptxt>Push the crankshaft in the rear thrust direction to create clearance and install a thrust washer to the No. 2 journal position with the oil groove facing the front of the engine.</ptxt>
<figure>
<graphic graphicname="A229670" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Push the crankshaft in the forward thrust direction to create clearance and install a thrust washer to the No. 2 journal position with the oil groove facing the rear of the engine.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 lower thrust washers to the No. 2 bearing cap with the grooves facing outward.</ptxt>
<figure>
<graphic graphicname="A229671" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Examine the front marks and numbers and set the crankshaft bearing caps on the cylinder block.</ptxt>
<figure>
<graphic graphicname="A221135" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Apply a light coat of engine oil to the threads of the crankshaft bearing cap bolts.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install the 8 crankshaft bearing cap bolts to the inside positions.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 2 bolts for each bearing cap until the clearance between the crankshaft bearing cap and the cylinder block becomes less than 6 mm (0.236 in.).</ptxt>
<figure>
<graphic graphicname="A221136E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Less than 6 mm</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer, lightly tap the crankshaft bearing cap to ensure a proper fit.</ptxt>
<figure>
<graphic graphicname="A221137" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Apply a light coat of engine oil to the threads of the crankshaft bearing cap bolts and temporarily install the 8 crankshaft bearing bolts to the outside positions.</ptxt>
<figure>
<graphic graphicname="A184806E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Tighten the crankshaft bearing cap bolts.</ptxt>
<atten4>
<ptxt>The cap bolts are tightened in 2 progressive steps.</ptxt>
</atten4>
<s3>
<ptxt>Step 1:</ptxt>
<ptxt>Uniformly tighten the 16 bolts in several steps in the order shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A221138E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>61</t-value1>
<t-value2>622</t-value2>
<t-value4>45</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Mark the front side of the crankshaft bearing cap bolts with paint.</ptxt>
</s3>
<s3>
<ptxt>Step 2:</ptxt>
<ptxt>Tighten the bearing cap bolts 90° in the order shown in step 1.</ptxt>
</s3>
<s3>
<ptxt>Check that the paint marks are now at a 90° angle to the front.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check that the crankshaft turns smoothly.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A221139E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install and uniformly tighten the 8 main bearing cap bolts together with 8 new seal washers in several steps in the sequence shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>26</t-value1>
<t-value2>262</t-value2>
<t-value4>19</t-value4>
</torqueitem>
</torque>
<spec>
<title>Standard Bolt</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Length</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Bolt A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>45 mm (1.77 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Bolt B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>30 mm (1.18 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Bolt A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Bolt B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Check that the crankshaft turns smoothly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK6042X_01_0010" proc-id="RM22W0E___000045U00000">
<ptxt>INSTALL CONNECTING ROD BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the bearing to the connecting rod cap.</ptxt>
<figure>
<graphic graphicname="A177653E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Clean the contact surfaces of the bearing and connecting rod cap.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Do not apply engine oil to the bearing or the surface it contacts.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vernier Caliper</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connecting Rod Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connecting Rod Bearing</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a vernier caliper, measure the distance between the connecting rod cap edge and connecting rod bearing edge.</ptxt>
<spec>
<title>Dimension A - B or B - A</title>
<specitem>
<ptxt>0 to 0.7 mm (0 to 0.0276 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Install the bearing to the connecting rod.</ptxt>
<figure>
<graphic graphicname="A177654E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Clean the contact surface of the bearing and connecting rod.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Do not apply engine oil to the bearing or the surface it contacts.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vernier Caliper</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connecting Rod</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connecting Rod Bearing</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a vernier caliper, measure the distance between the connecting rod edge and connecting rod bearing edge.</ptxt>
<spec>
<title>Dimension A - B or B - A</title>
<specitem>
<ptxt>0 to 0.7 mm (0 to 0.0276 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK6042X_01_0069" proc-id="RM22W0E___000045J00000">
<ptxt>INSPECT CRANKSHAFT THRUST CLEARANCE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the thrust clearance while prying the crankshaft back and forth with a screwdriver.</ptxt>
<figure>
<graphic graphicname="A221124" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard thrust clearance</title>
<specitem>
<ptxt>0.04 to 0.24 mm (0.00157 to 0.00945 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum thrust clearance</title>
<specitem>
<ptxt>0.30 mm (0.0118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the thrust clearance is more than the maximum, replace the thrust washers as a set.</ptxt>
<spec>
<title>Standard thrust washer thickness</title>
<specitem>
<ptxt>1.93 to 1.98 mm (0.0760 to 0.0780 in.)</ptxt>
</specitem>
</spec>
<ptxt>If necessary, replace the crankshaft.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BK6042X_01_0013" proc-id="RM22W0E___000045X00000">
<ptxt>INSTALL PISTON SUB-ASSEMBLY WITH CONNECTING ROD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply engine oil to the cylinder walls, pistons and surfaces of the connecting rod bearings.</ptxt>
</s2>
<s2>
<ptxt>Check the positions of the piston ring ends.</ptxt>
<figure>
<graphic graphicname="A221134E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Compression Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Compression Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lower Side Rail</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upper Side Rail</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Expander</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a hammer handle and piston ring compressor, press a piston and connecting rod assembly into each cylinder with the front mark of the piston facing forward.</ptxt>
<figure>
<graphic graphicname="A221140E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install each connecting rod cap so that the protrusion is facing the correct direction.</ptxt>
<figure>
<graphic graphicname="A221141E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Match each numbered connecting rod cap with the correct connecting rod.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Apply a light coat of engine oil to the threads of the connecting rod cap bolts.</ptxt>
</s2>
<s2>
<ptxt>Install the connecting rod cap bolts.</ptxt>
<atten4>
<ptxt>The cap bolts are tightened in 2 progressive steps.</ptxt>
</atten4>
<s3>
<ptxt>Step 1:</ptxt>
<ptxt>Install and alternately tighten the bolts of each connecting rod cap in several steps.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>250</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A221142E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Mark the front side of each connecting rod cap bolt with paint.</ptxt>
</s3>
<s3>
<ptxt>Step 2:</ptxt>
<ptxt>Tighten the cap bolts 90°.</ptxt>
</s3>
<s3>
<ptxt>Check that the paint marks are now at a 90° angle to the front.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check that the crankshaft turns smoothly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BK6042X_01_0070" proc-id="RM22W0E___000045F00000">
<ptxt>INSPECT CONNECTING ROD THRUST CLEARANCE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the thrust clearance while moving the connecting rod back and forth.</ptxt>
<figure>
<graphic graphicname="A221120" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard thrust clearance</title>
<specitem>
<ptxt>0.15 to 0.30 mm (0.00591 to 0.0118 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum thrust clearance</title>
<specitem>
<ptxt>0.35 mm (0.0138 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the thrust clearance is more than the maximum, replace one or more connecting rods as necessary.</ptxt>
<spec>
<title>Standard connecting rod thickness</title>
<specitem>
<ptxt>20.80 to 20.85 mm (0.819 to 0.821 in.)</ptxt>
</specitem>
</spec>
<ptxt>If necessary, replace the crankshaft.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>