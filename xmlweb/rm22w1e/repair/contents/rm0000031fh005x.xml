<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000S" variety="S000S">
<name>1VD-FTV INTAKE / EXHAUST</name>
<ttl id="12010_S000S_7C3JD_T00CG" variety="T00CG">
<name>INTAKE SYSTEM</name>
<para id="RM0000031FH005X" category="G" type-id="8000T" name-id="IE1TR-02" from="201301">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM0000031FH005X_01" type-id="01" category="01">
<s-1 id="RM0000031FH005X_01_0001" proc-id="RM22W0E___00006Q200000">
<ptxt>CHECK INTAKE AIR CONTROL SYSTEM</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check for leakage or clogging between the air cleaner housing and inlet turbocharger and between the outlet turbocharger and cylinder head.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Clogged air cleaner</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Replace element</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hoses collapsed or deformed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Repair or replace</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Leakage from connections</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Check each connection and repair</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Cracks in components</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Check and replace</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031FH005X_01_0002" proc-id="RM22W0E___00006Q300000">
<ptxt>CHECK EXHAUST SYSTEM</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check for leakage or clogging between the cylinder head and turbocharger inlet and between the turbocharger outlet and exhaust pipe.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Deformed components</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Repair or replace</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Foreign material in passages</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Remove</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Leakage from components</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Repair or replace</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Cracks in components</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Check and replace</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031FH005X_01_0005" proc-id="RM22W0E___00006Q500000">
<ptxt>CHECK AIR INDUCTION SYSTEM</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>The illustration shows the areas that may draw in secondary air, which could lead to idling problems.</ptxt>
</atten4>
<s2>
<ptxt>Check that the hoses, gaskets, clamps and O-rings are installed correctly.</ptxt>
</s2>
<s2>
<ptxt>Check for cracks, etc. in the hoses, gaskets and O-rings.</ptxt>
<list1 type="unordered">
<item>
<ptxt>for V-bank Outside Area:</ptxt>
<figure>
<graphic graphicname="A267208E01" width="7.106578999in" height="7.795582503in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Intercooler</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="A257036E02" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Intercooler</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>for V-bank Inside Area:</ptxt>
<figure>
<graphic graphicname="A267209E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ EGR System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="A257037E02" width="7.106578999in" height="8.799559038in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o EGR System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Intercooler</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*C</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Intercooler</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031FH005X_01_0003" proc-id="RM22W0E___00006Q400000">
<ptxt>CHECK BOOST PRESSURE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and turn the tester on.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
<atten4>
<ptxt>Be sure to perform the inspection when the engine coolant temperature is between 75 and 90°C (167 and 194°F).</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / VN Turbo.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A176220E11" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Take a snapshot of the Data List items with the intelligent tester shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Snapshot Record Button</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Graphs can be displayed by transferring the stored snapshot from the tester to a PC. Intelligent Viewer must be installed on the PC.</ptxt>
</item>
<item>
<ptxt>The condition of the turbocharger can be determined by fully depressing the accelerator pedal while driving at 15 km/h (9 mph) in 2nd gear to accelerate the vehicle (obey all laws and regulations, and pay attention to driving conditions while driving the vehicle), and then comparing MAP with Target Booster Pressure at an engine speed of 3000 rpm.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Compare MAP with Target Booster Pressure.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>MAP is within 35 kPa of Target Booster Pressure when accelerating with accelerator pedal fully depressed.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The specification above denotes the difference between the absolute pressure values.</ptxt>
</item>
<item>
<ptxt>The inspection above is only for vehicles equipped with a manual transmission.</ptxt>
</item>
<item>
<ptxt>If the driving inspection using the intelligent tester above cannot be performed (e.g. due to road conditions), perform the following inspection.</ptxt>
</item>
</list1>
</atten4>
<s3>
<ptxt>Warm up the engine.</ptxt>
<atten4>
<ptxt>Be sure to perform the inspection when the engine coolant temperature is between 75 and 90°C (167 and 194°F).</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Using a 3-way connector, connect SST (turbocharger pressure gauge) between the diesel turbo pressure sensor (manifold absolute pressure sensor) and the gas filter.</ptxt>
<sst>
<sstitem>
<s-number>09992-00242</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A170342E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Gas Filter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Diesel Turbo Pressure Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>3-Way Connector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Fully apply the parking brake and chock the 4 wheels.</ptxt>
</s3>
<s3>
<ptxt>for Manual Transmission:</ptxt>
<ptxt>While depressing the clutch pedal, fully depress the accelerator pedal. Measure the boost pressure at maximum engine speed (4700 to 4900 rpm).</ptxt>
</s3>
<s3>
<ptxt>for Automatic Transmission:</ptxt>
<ptxt>Move the shift lever to P or N, and then fully depress the accelerator pedal. Measure the boost pressure at maximum engine speed (4700 to 4900 rpm).</ptxt>
<spec>
<title>Standard Pressure (Gauge Pressure)</title>
<specitem>
<ptxt>30 to 60 kPa (0.30 to 0.61 kgf/cm<sup>2</sup>, 4.3 to 8.7 psi)</ptxt>
</specitem>
</spec>
<ptxt>If the pressure is lower than the standard, the following problems may be present.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The intake system or exhaust system has leakage or blockage.</ptxt>
</item>
<item>
<ptxt>w/ DPF:</ptxt>
<ptxt>The turbocharger sub-assembly or ECM is malfunctioning.</ptxt>
</item>
<item>
<ptxt>w/o DPF:</ptxt>
<ptxt>The turbocharger sub-assembly or turbo motor driver is malfunctioning.</ptxt>
</item>
<item>
<ptxt>The EGR valve does not close.</ptxt>
</item>
<item>
<ptxt>The diesel throttle body does not open.</ptxt>
</item>
<item>
<ptxt>The vacuum hose connected to the diesel turbo pressure sensor (manifold absolute pressure sensor) is cracked or disconnected.</ptxt>
</item>
<item>
<ptxt>The mass air flow meter is malfunctioning.</ptxt>
</item>
<item>
<ptxt>A fuel injector is malfunctioning.</ptxt>
</item>
</list1>
<ptxt>If the pressure is higher than the standard, check the turbocharger and/or boost control components (turbo motor driver, pressure sensor, vacuum hose, wire harness, etc.).</ptxt>
</s3>
</s2>
<s2>
<ptxt>Chart showing the suspected trouble areas when the pressure is lower than the standard.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>○: If a problem listed in the leftmost column of the chart exists, or if the part in the leftmost column of the chart has a malfunction, the value of the Data List item in the uppermost row of the chart will meet the conditions shown in the row labeled "Value which represents a malfunction".</ptxt>
</item>
<item>
<ptxt>The values in the chart are applicable when the engine coolant temperature is between 75 and 90°C (167 and 194°F).</ptxt>
</item>
<item>
<ptxt>The values in the chart are for vehicles equipped with a manual transmission. For vehicles with an automatic transmission, use these values as a reference only as they may differ from the actual values.</ptxt>
</item>
<item>
<ptxt>The values in the chart are valid in an area with an absolute atmospheric pressure higher than 95 kPa. (Standard atmospheric pressure is 101 kPa. Atmospheric pressure decreases by 1 kPa for every 100 m increase in altitude, and is also affected by the current weather conditions.)</ptxt>
</item>
<item>
<ptxt>When the altitude increases, atmospheric pressure and MAP decrease.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="9">
<colspec colname="COL1" colwidth="0.79in"/>
<colspec colname="COL2" colwidth="0.79in"/>
<colspec colname="COL3" colwidth="0.79in"/>
<colspec colname="COL4" colwidth="0.79in"/>
<colspec colname="COL5" colwidth="0.79in"/>
<colspec colname="COL6" colwidth="0.79in"/>
<colspec colname="COL7" colwidth="0.79in"/>
<colspec colname="COL8" colwidth="0.79in"/>
<colspec colname="COL9" colwidth="0.76in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>MAP</ptxt>
<ptxt>(Absolute pressure inside intake manifold)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>MAF</ptxt>
<ptxt>(Intake airflow rate)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accel Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Actual Throttle Position (#1, #2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Actual EGR Valve Pos. (#1, #2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>EGR Close Lrn. Status (#1, #2)</ptxt>
<ptxt>(EGR valve fully closed position learning status)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/o DPF:</ptxt>
<ptxt>Fuel Press</ptxt>
<ptxt>w/ DPF:</ptxt>
<ptxt>Common Rail Pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Injection Feedback Val #1 (to #8)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Values taken from an actual normal vehicle</ptxt>
<ptxt>
<sup>*</sup>1</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>228 g/sec.</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>99% or more</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/o DPF:</ptxt>
<ptxt>0%</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>0%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/o DPF:</ptxt>
<ptxt>OK</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>-3 to +3 mm<sup>3</sup>/st</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/ DPF:</ptxt>
<ptxt>100%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/ DPF:</ptxt>
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Values which represent a malfunction</ptxt>
<ptxt>
<sup>*</sup>1</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>MAP is below Target Booster Pressure by 35 kPa or more</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>MAF is less than 190 g/sec.</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Accel Position is not fully depressed position</ptxt>
<ptxt>
<sup>*</sup>2</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Actual Throttle Position (#1, #2) is not within 10% of Target Throttle Position (#1, #2)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Actual EGR Valve Pos. (#1, #2) is more than 10%</ptxt>
<ptxt>
<sup>*</sup>3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/o DPF:</ptxt>
<ptxt>NG</ptxt>
<ptxt>(Determined after performing learning)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Fuel Pressure is below Target Common Rail Pressure by 10 MPa or more (Check while condition steady)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Outside of above range</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/ DPF:</ptxt>
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turbocharger</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>EGR valve does not close or has problem with movement</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
<ptxt>(Problem with EGR valve movement)</ptxt>
<ptxt>
<sup>*</sup>4, <sup>*</sup>5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
<ptxt>(EGR valve does not close)</ptxt>
<ptxt>
<sup>*</sup>4, <sup>*</sup>5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Problem with diesel throttle movement</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
<ptxt>(Intake airflow decreases)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Accelerator pedal cannot be fully depressed or problem with accelerator pedal position sensor exists</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Intake air system leakage or blockage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Exhaust gas leakage before turbocharger or blockage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Manifold absolute pressure sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Manifold absolute pressure sensor hose is disconnected</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mass air flow meter sub-assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fuel system (injector, supply pump or common rail)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>

</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
<ptxt>(Fuel injector leakage, decrease in pressure discharge valve relief pressure or valve is stuck)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
<ptxt>
<sup>*</sup>6</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>
<sup>*</sup>1: These values are measured when the transmission is in 2nd gear, the accelerator pedal is fully depressed, the vehicle is accelerating, and the engine speed is 3000 rpm.</ptxt>
</item>
<item>
<ptxt>
<sup>*</sup>2: The Accel Position is the accelerator opening angle (%) for engine control use. The value indicates around 100% when the accelerator pedal is fully depressed. If the value does not indicate around 100% when the accelerator pedal is fully depressed, the accelerator pedal position sensor circuit or the pedal itself is malfunctioning. When the MIL is illuminated, even with the accelerator pedal fully depressed and an Accel Position of below 70%, it means the fail-safe is restricting the accelerator.</ptxt>
</item>
<item>
<ptxt>
<sup>*</sup>3: Check that the Actual EGR Valve Pos. is always following the Target EGR Position value (e.g. the Actual EGR Valve Pos. is around the fully closed position when the Target EGR Position value indicates 0%). The EGR valve may be malfunctioning if the value is stuck or does not fluctuate smoothly. However, in some cases the EGR valve may be malfunctioning even if the Actual EGR Valve Pos. value is normal. Inspect the EGR valve for any defects (deposits, valve stuck, poor movement, etc.) if necessary.</ptxt>
</item>
<item>
<ptxt>
<sup>*</sup>4: w/ DPF</ptxt>
<ptxt>DTC P0400 (Bank 1, Bank 2) or P042E (for Bank 1) or P045E (for Bank 2) may be stored at this time. If the actual EGR valve position follows the target EGR valve position slowly, a feeling of hesitation may occur.</ptxt>
</item>
<item>
<ptxt>
<sup>*</sup>5: w/o DPF</ptxt>
<ptxt>DTC P0400 (for Bank 1) or P1248 (for Bank 2) may be stored at this time. If the actual EGR valve position follows the target EGR valve position slowly, a feeling of hesitation may occur.</ptxt>
</item>
<item>
<ptxt>
<sup>*</sup>6: If Injection Feedback Val # of a cylinder is not within -3 to +3 mm<sup>3</sup>/st, the corresponding cylinder may have a malfunction (injector or compression). However, in some cases the cylinder may be malfunctioning even if the value is within -3 to +3 mm<sup>3</sup>/st because these values are injection volume correction values calculated by the ECM during engine idling and not correction values for the high engine load condition related to boost pressure control and engine output performance.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031FH005X_01_0006" proc-id="RM22W0E___00006Q600000">
<ptxt>CHECK TURBOCHARGER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves to prevent injuries and burns when checking the turbocharger.</ptxt>
</item>
<item>
<ptxt>The engine compartment becomes hot when the engine is running.</ptxt>
</item>
</list1>
</atten2>
<s2>
<ptxt>Make sure that the DC motors connector is properly connected.</ptxt>
</s2>
<s2>
<ptxt>Check the motor movement when turning the ignition switch to ON, when starting the engine, and then when turning the ignition switch off.</ptxt>
<figure>
<graphic graphicname="A182668E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Ignition switch ON</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Engine idling</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry>
<ptxt>*b → *c → *d:</ptxt>
<ptxt>Turn the ignition switch off at idling</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry>
<ptxt>Ignition switch off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, check the ECM (w/ DPF: See page <xref label="Seep01" href="RM0000013FU06RX"/>, w/o DPF: See page <xref label="Seep02" href="RM0000013FU06QX"/>) and/or turbo motor driver (w/ DPF: See page <xref label="Seep03" href="RM000002NOW06KX"/>, w/o DPF: See page <xref label="Seep04" href="RM000002NOW06IX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>