<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12021_S001D" variety="S001D">
<name>JF2A TRANSFER / 4WD / AWD</name>
<ttl id="12021_S001D_7C3NN_T00GQ" variety="T00GQ">
<name>TRANSFER ASSEMBLY</name>
<para id="RM000002HO801BX" category="A" type-id="80002" name-id="TF1JS-01" from="201308">
<name>DISASSEMBLY</name>
<subpara id="RM000002HO801BX_02" type-id="01" category="01">
<s-1 id="RM000002HO801BX_02_0075" proc-id="RM22W0E___000098T00001">
<ptxt>REMOVE PROPELLER SHAFT HEAT INSULATOR BRACKET SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and bracket.</ptxt>
<figure>
<graphic graphicname="C174577" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0031" proc-id="RM22W0E___000097Z00001">
<ptxt>REMOVE TRANSFER OIL PUMP PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts, oil pump plate and oil pump driven rotor.</ptxt>
<figure>
<graphic graphicname="C159613" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not drop the oil pump driven rotor.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the O-ring from the oil pump plate.</ptxt>
</s2>
<s2>
<ptxt>Remove the transfer oil seal ring from the input shaft.</ptxt>
<figure>
<graphic graphicname="C173490" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the oil pump drive rotor from the input shaft.</ptxt>
<figure>
<graphic graphicname="C173598E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0060" proc-id="RM22W0E___000098H00001">
<ptxt>REMOVE TRANSFER CASE STRAIGHT PIN</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 straight pins.</ptxt>
<figure>
<graphic graphicname="C162213E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not drop the straight pins.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0032" proc-id="RM22W0E___000098000001">
<ptxt>REMOVE TRANSFER CASE PLUG</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 10 mm hexagon wrench, remove the case plug, compression spring, ball and relief valve seat.</ptxt>
<figure>
<graphic graphicname="C177450" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0033" proc-id="RM22W0E___000098100001">
<ptxt>REMOVE TRANSFER OIL PUMP PLATE OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, pry out the oil seal from the oil pump plate.</ptxt>
<figure>
<graphic graphicname="C173329" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to damage the oil seal and oil pump plate contact surfaces.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0034" proc-id="RM22W0E___000096800001">
<ptxt>REMOVE FRONT OUTPUT SHAFT COMPANION FLANGE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, release the staked part of the nut.</ptxt>
<sst>
<sstitem>
<s-number>09930-00010</s-number>
<s-subnumber>09931-00010</s-subnumber>
<s-subnumber>09931-00020</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C173330E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to use SST with the tapered surface facing the shaft.</ptxt>
</item>
<item>
<ptxt>Do not grind the tip of SST with a grinder, etc.</ptxt>
</item>
<item>
<ptxt>Completely loosen the staked part of the nut when removing it.</ptxt>
</item>
<item>
<ptxt>Do not damage the threads of the transfer driven sprocket.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Using SST to hold the companion flange, remove the lock nut.</ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C173331E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, remove the companion flange.</ptxt>
<figure>
<graphic graphicname="C173332E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-40011</s-number>
<s-subnumber>09951-04020</s-subnumber>
<s-subnumber>09952-04010</s-subnumber>
<s-subnumber>09953-04030</s-subnumber>
<s-subnumber>09954-04010</s-subnumber>
<s-subnumber>09955-04051</s-subnumber>
<s-subnumber>09957-04010</s-subnumber>
<s-subnumber>09958-04011</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Remove the O-ring from the companion flange.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0079" proc-id="RM22W0E___0000K7K00000">
<ptxt>REMOVE TRANSFER OUTPUT SHAFT DUST DEFLECTOR</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Using SST and a press, remove the transfer output shaft dust deflector.</ptxt>
<sst>
<sstitem>
<s-number>09950-00020</s-number>
</sstitem>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00340</s-subnumber>
<s-subnumber>09951-00470</s-subnumber>
<s-subnumber>09952-06010</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C284226E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not drop the front output shaft companion flange sub-assembly.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0076" proc-id="RM22W0E___000096400001">
<ptxt>REMOVE TRANSFER CASE FRONT OIL SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the front transfer case oil seal.</ptxt>
<sst>
<sstitem>
<s-number>09308-10010</s-number>
</sstitem>
<sstitem>
<s-number>09950-40011</s-number>
<s-subnumber>09957-04010</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C284223E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the front transfer case oil seal and front transfer case contact surface.</ptxt>
</item>
<item>
<ptxt>Apply grease to the tip of SST center bolt before use.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002HO801BX_02_0036" proc-id="RM22W0E___000096F00001">
<ptxt>REMOVE REAR OUTPUT SHAFT COMPANION FLANGE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, release the staked part of the nut.</ptxt>
<sst>
<sstitem>
<s-number>09930-00010</s-number>
<s-subnumber>09931-00010</s-subnumber>
<s-subnumber>09931-00020</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C173333E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to use SST with the tapered surface facing the shaft.</ptxt>
</item>
<item>
<ptxt>Do not grind the tip of SST with a grinder, etc.</ptxt>
</item>
<item>
<ptxt>Completely loosen the staked part of the nut when removing it.</ptxt>
</item>
<item>
<ptxt>Do not damage the threads of the rear transfer output shaft.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Using SST to hold the companion flange, remove the lock nut.</ptxt>
<figure>
<graphic graphicname="C173334E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using SST, remove the companion flange.</ptxt>
<sst>
<sstitem>
<s-number>09950-40011</s-number>
<s-subnumber>09951-04020</s-subnumber>
<s-subnumber>09952-04010</s-subnumber>
<s-subnumber>09953-04030</s-subnumber>
<s-subnumber>09954-04010</s-subnumber>
<s-subnumber>09955-04051</s-subnumber>
<s-subnumber>09957-04010</s-subnumber>
<s-subnumber>09958-04011</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C173335E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the companion flange.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0077" proc-id="RM22W0E___000096B00000">
<ptxt>REMOVE TRANSFER CASE REAR OIL SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, tap out the oil seal.</ptxt>
<figure>
<graphic graphicname="C172373E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09308-00010</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>Be careful not to damage the oil seal and rear transfer case contact surface.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002HO801BX_02_0059" proc-id="RM22W0E___000098G00001">
<ptxt>REMOVE NO. 1 TRANSFER CASE PLUG</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the plug, compression spring and pin.</ptxt>
<figure>
<graphic graphicname="C176791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0038" proc-id="RM22W0E___000098200001">
<ptxt>REMOVE TRANSFER CASE PLUG</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 6 mm hexagon wrench, remove the plug.</ptxt>
<figure>
<graphic graphicname="C173336" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0039" proc-id="RM22W0E___000098300001">
<ptxt>REMOVE REAR TRANSFER CASE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 14 bolts.</ptxt>
<figure>
<graphic graphicname="C173337" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the rear transfer case.</ptxt>
<atten4>
<ptxt>If necessary, tap the rear transfer case with a plastic-faced hammer to remove it.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0065" proc-id="RM22W0E___000098J00001">
<ptxt>REMOVE FRONT TRANSFER OUTPUT SHAFT NEEDLE ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C173595E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the needle roller bearing and spacer from the output shaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0040" proc-id="RM22W0E___000098400001">
<ptxt>REMOVE NO. 2 TRANSFER GEAR SHIFT FORK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using 2 screwdrivers and a hammer, tap out the 4 shift fork shaft snap rings.</ptxt>
<figure>
<graphic graphicname="C173338" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt. Then remove the No. 2 gear shift fork together with the high and low clutch sleeve from the shift actuator.</ptxt>
<figure>
<graphic graphicname="C173339" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0042" proc-id="RM22W0E___000098500001">
<ptxt>REMOVE TRANSFER SHIFT ACTUATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 1 gear shift fork bolt.</ptxt>
<figure>
<graphic graphicname="C173373" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts.</ptxt>
<figure>
<graphic graphicname="C173374" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the shift actuator and No. 1 gear shift fork from the rear transfer case.</ptxt>
<atten3>
<ptxt>Take care not to drop the No. 1 gear shift fork.</ptxt>
</atten3>
<figure>
<graphic graphicname="C241090E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the shift actuator.</ptxt>
<figure>
<graphic graphicname="C160356E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0043" proc-id="RM22W0E___000098600001">
<ptxt>REMOVE REAR TRANSFER OUTPUT SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a snap ring expander, expand the snap ring as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C173484" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer, carefully tap the rear transfer case, and remove the output shaft together with the drive chain and driven sprocket.</ptxt>
<figure>
<graphic graphicname="C175777" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the output shaft and driven sprocket from the drive chain.</ptxt>
<atten3>
<ptxt>Do not drop the output shaft washers.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a snap ring expander, remove the snap ring from the rear transfer case.</ptxt>
<figure>
<graphic graphicname="C174066" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0066" proc-id="RM22W0E___000098K00001">
<ptxt>REMOVE FRONT DRIVE CLUTCH SLEEVE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174057" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clutch sleeve from the output shaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0044" proc-id="RM22W0E___000098700001">
<ptxt>REMOVE SIDE GEAR SHAFT HOLDER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, remove the snap ring.</ptxt>
<figure>
<graphic graphicname="C160519" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, remove the bearing.</ptxt>
<sst>
<sstitem>
<s-number>09308-00010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C173485E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0045" proc-id="RM22W0E___000098800001">
<ptxt>REMOVE TRANSFER OIL SEPARATOR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and oil separator.</ptxt>
<figure>
<graphic graphicname="C173486" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the oil separator.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0056" proc-id="RM22W0E___000098E00001">
<ptxt>REMOVE TRANSFER CASE MAGNET</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174062" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the magnet from the oil separator.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0046" proc-id="RM22W0E___000098900001">
<ptxt>REMOVE TRANSFER INPUT SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 14 mm hexagon wrench, remove the 2 case plugs from the front transfer case.</ptxt>
<figure>
<graphic graphicname="C159719" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a snap ring expander, expand the snap ring as shown in the illustration. Then remove the input shaft from the front transfer case.</ptxt>
<figure>
<graphic graphicname="C176062" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0047" proc-id="RM22W0E___000098A00001">
<ptxt>REMOVE TRANSFER LOW PLANETARY RING GEAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, remove the snap ring. Then remove the low planetary ring gear from the front transfer case.</ptxt>
<figure>
<graphic graphicname="C173488" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a snap ring expander, remove the snap ring from the front transfer case.</ptxt>
<figure>
<graphic graphicname="C174065" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0048" proc-id="RM22W0E___000098B00001">
<ptxt>REMOVE TRANSFER DRIVEN SPROCKET BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, remove the snap ring. Then remove the bearing from the front transfer case.</ptxt>
<figure>
<graphic graphicname="C173489" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0067" proc-id="RM22W0E___000098L00001">
<ptxt>REMOVE NO. 1 SYNCHRONIZER RING (for Manual Transmission)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174150" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the synchronizer ring from the input shaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0062" proc-id="RM22W0E___000098I00001">
<ptxt>REMOVE NO. 1 TRANSFER INPUT SHAFT SEAL RING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the seal ring from the input shaft.</ptxt>
<figure>
<graphic graphicname="C173492E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0049" proc-id="RM22W0E___000098C00001">
<ptxt>REMOVE TRANSFER INPUT SHAFT BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a snap ring expander, remove the snap ring.</ptxt>
<figure>
<graphic graphicname="C173491" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST and a press, press out the bearing.</ptxt>
<figure>
<graphic graphicname="C173493E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<sst>
<sstitem>
<s-number>09555-55010</s-number>
</sstitem>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00490</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<atten3>
<ptxt>Be careful not to drop or damage the input shaft, low planetary gear and low planetary gear bearings.</ptxt>
</atten3>
<atten4>
<ptxt>When the bearing is removed, the input shaft, low planetary gear, and 2 low planetary gear bearings will come off.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0068" proc-id="RM22W0E___000098M00001">
<ptxt>REMOVE TRANSFER INPUT SHAFT BIMETAL FORMED BUSH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the bush.</ptxt>
<figure>
<graphic graphicname="C173631E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<sst>
<sstitem>
<s-number>09387-00041</s-number>
<s-subnumber>09387-01021</s-subnumber>
<s-subnumber>09387-01030</s-subnumber>
<s-subnumber>09387-01040</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0058" proc-id="RM22W0E___000098F00001">
<ptxt>REMOVE TRANSFER INPUT SHAFT PLUG</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, tap out the input shaft plug.</ptxt>
<figure>
<graphic graphicname="C173494E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00300</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0069" proc-id="RM22W0E___000098N00001">
<ptxt>REMOVE TRANSFER OUTPUT SHAFT WASHER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 output shaft washers from the output shaft.</ptxt>
<figure>
<graphic graphicname="C173600" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0051" proc-id="RM22W0E___000098D00001">
<ptxt>REMOVE NO. 1 TRANSFER OUTPUT SHAFT SPACER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using 2 screwdrivers and a hammer, tap out the snap ring. </ptxt>
<figure>
<graphic graphicname="C173602" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the output shaft spacer from the output shaft.</ptxt>
<figure>
<graphic graphicname="C173603" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the seal ring from the output shaft spacer.</ptxt>
<figure>
<graphic graphicname="C173604" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the straight pin from the output shaft.</ptxt>
<figure>
<graphic graphicname="C173605E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0070" proc-id="RM22W0E___000098O00001">
<ptxt>REMOVE CENTER DIFFERENTIAL CASE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the center differential case from the output shaft.</ptxt>
<figure>
<graphic graphicname="C173606" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for Manual Transmission:</ptxt>
<ptxt>Using a snap ring expander, remove the snap ring.</ptxt>
<figure>
<graphic graphicname="C173607" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for Manual Transmission:</ptxt>
<ptxt>Remove the synchromesh shifting key spring and No. 2 synchromesh shifting keys from the center differential case.</ptxt>
<figure>
<graphic graphicname="C173608" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, remove the needle roller bearing.</ptxt>
<sst>
<sstitem>
<s-number>09308-00010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C173609E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, remove the needle roller bearing.</ptxt>
<sst>
<sstitem>
<s-number>09387-00041</s-number>
<s-subnumber>09387-01021</s-subnumber>
<s-subnumber>09387-01030</s-subnumber>
<s-subnumber>09387-01040</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C173630E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0071" proc-id="RM22W0E___000098P00001">
<ptxt>REMOVE TRANSFER OUTPUT SHAFT SPACER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using 2 screwdrivers and a hammer, tap out the snap ring.</ptxt>
<figure>
<graphic graphicname="C173610" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the output shaft spacer from the output shaft.</ptxt>
<figure>
<graphic graphicname="C173611" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the straight pin from the output shaft.</ptxt>
<figure>
<graphic graphicname="C173612E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0072" proc-id="RM22W0E___000098Q00001">
<ptxt>REMOVE TRANSFER DRIVE SPROCKET SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the drive sprocket from the output shaft.</ptxt>
<figure>
<graphic graphicname="C173613" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0073" proc-id="RM22W0E___000098R00001">
<ptxt>REMOVE TRANSFER DRIVE SPROCKET BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bearing from the output shaft.</ptxt>
<figure>
<graphic graphicname="C173614" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002HO801BX_02_0074" proc-id="RM22W0E___000098S00001">
<ptxt>REMOVE REAR TRANSFER OUTPUT SHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a press, press out the bearing.</ptxt>
<sst>
<sstitem>
<s-number>09527-10011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C173616E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>Be careful not to drop or damage the output shaft.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the needle roller bearing from the output shaft.</ptxt>
<figure>
<graphic graphicname="C173615" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>