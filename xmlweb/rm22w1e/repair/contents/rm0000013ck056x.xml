<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0019" variety="S0019">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0019_7C3M6_T00F9" variety="T00F9">
<name>VALVE BODY ASSEMBLY</name>
<para id="RM0000013CK056X" category="A" type-id="30014" name-id="AT8T4-02" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000013CK056X_01" type-id="01" category="01">
<s-1 id="RM0000013CK056X_01_0001" proc-id="RM22W0E___00007EO00000">
<ptxt>INSTALL TRANSMISSION VALVE BODY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="D027914E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the spring and check ball body.</ptxt>
</s2>
<s2>
<ptxt>Insert the pin of the manual valve into the hole of the manual valve lever.</ptxt>
<figure>
<graphic graphicname="C157293E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the transmission valve body with the 19 bolts.</ptxt>
<figure>
<graphic graphicname="C157292E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>112</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Each bolt length is indicated below.</ptxt>
</atten4>
<ptxt>36 mm (1.42 in.) for bolt A</ptxt>
<ptxt>25 mm (0.984 in.) for bolt B</ptxt>
</s2>
<s2>
<ptxt>Install the detent spring and detent spring cover with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013CK056X_01_0013" proc-id="RM22W0E___00007E300000">
<ptxt>CONNECT TRANSMISSION WIRE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 7 connectors to the solenoid valves.</ptxt>
<figure>
<graphic graphicname="C125285E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the 2 ATF temperature sensors with the 2 clamps and 2 bolts.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>112</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Each bolt length is indicated below.</ptxt>
<ptxt>12 mm (0.472 in.) for bolt A</ptxt>
</atten4>
<ptxt>36 mm (1.42 in.) for bolt B</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000013CK056X_01_0014" proc-id="RM22W0E___00007E400000">
<ptxt>INSTALL VALVE BODY OIL STRAINER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="D028692E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Coat a new O-ring with ATF and install it to the oil strainer.</ptxt>
</s2>
<s2>
<ptxt>Install the oil strainer with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000013CK056X_01_0015" proc-id="RM22W0E___00007E500000">
<ptxt>INSTALL AUTOMATIC TRANSMISSION OIL PAN SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C153314E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Remove the gasket and be careful not to spill oil on the contacting surfaces of the transmission case and oil pan.</ptxt>
</atten3>
<s2>
<ptxt>Install a new gasket and the oil pan with the 20 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>4.4</t-value1>
<t-value2>45</t-value2>
<t-value3>39</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000013CK056X_01_0016" proc-id="RM22W0E___00007EQ00000">
<ptxt>RESET MEMORY</ptxt>
<content1 releasenbr="2">
<ptxt>(See page <xref label="Seep01" href="RM000000O7H0E2X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000013CK056X_01_0009" proc-id="RM22W0E___00007EP00000">
<ptxt>ADD AUTOMATIC TRANSMISSION FLUID</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add automatic transmission fluid (See page <xref label="Seep01" href="RM000002BL003UX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>