<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000R" variety="S000R">
<name>3UR-FE INTAKE / EXHAUST</name>
<ttl id="12010_S000R_7C3J9_T00CC" variety="T00CC">
<name>VACUUM SWITCHING VALVE (for ACIS)</name>
<para id="RM0000031GR01OX" category="G" type-id="8000T" name-id="IE014-08" from="201301">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM0000031GR01OX_01" type-id="01" category="01">
<s-1 id="RM0000031GR01OX_01_0002" proc-id="RM22W0E___00001X400000">
<ptxt>REMOVE V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A174196E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Raise the front of the V-bank cover to detach the 3 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000031GR01OX_01_0001" proc-id="RM22W0E___00006P300000">
<ptxt>INSPECT VACUUM SWITCHING VALVE ASSEMBLY (for ACIS)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the vacuum switching valve connector.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A162110E05" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Body Ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>37 to 44 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the vacuum switching valve assembly.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A162111E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>When applying vacuum to port E, check that air is sucked into the filter.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Filter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vacuum</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the vacuum switching valve assembly.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A162112E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Apply battery voltage to the connector, and check the VSV operation.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 1</ptxt>
<ptxt>Battery negative (-) → Terminal 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air is sucked into port E when a vacuum is applied to port F</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vacuum</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the vacuum switching valve assembly.</ptxt>
</s2>
<s2>
<ptxt>Connect the vacuum switching valve connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GR01OX_01_0004" proc-id="RM22W0E___00001WV00000">
<ptxt>INSTALL V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A177597E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 3 V-bank cover grommets with the 3 pins, and press down on the V-bank cover to attach the pins.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>