<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12009_S000N" variety="S000N">
<name>1VD-FTV EMISSION CONTROL</name>
<ttl id="12009_S000N_7C3IP_T00BS" variety="T00BS">
<name>EXHAUST FUEL ADDITION INJECTOR (for DPF)</name>
<para id="RM000003TI5013X" category="A" type-id="30014" name-id="EC5M9-01" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM000003TI5013X_02" type-id="11" category="10" proc-id="RM22W0E___00006LL00000">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing an injector (including interchanging injectors between cylinders) or common rail, replace the corresponding injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>When fuel lines are disconnected, air may enter the fuel lines, leading to engine starting trouble. Therefore, perform forced regeneration and bleed the air from the fuel lines (See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>).</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM000003TI5013X_01" type-id="01" category="01">
<s-1 id="RM000003TI5013X_01_0023" proc-id="RM22W0E___00005AD00000">
<ptxt>INSTALL FUEL INJECTOR SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 2 new fuel injector seals.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003TI5013X_01_0002" proc-id="RM22W0E___00005AE00000">
<ptxt>INSTALL EXHAUST FUEL ADDITION INJECTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>If there is foreign matter on the installation surface of the exhaust fuel addition injector, be sure to clean it before installation.</ptxt>
</atten3>
<s2>
<ptxt>Install 2 new gaskets, the 2 exhaust fuel addition injectors, 2 nozzle holder clamps and 2 new washers with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>28</t-value1>
<t-value2>286</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A201785E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Align the nozzle holder clamp with the cutouts of the injector as shown in the illustration.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nozzle Holder Clamp</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000003TI5013X_01_0019" proc-id="RM22W0E___00006LI00000">
<ptxt>CONNECT FUEL TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 2 fuel tube connectors to the exhaust fuel addition injector.</ptxt>
</s2>
<s2>
<ptxt>Turn the 2 retainers in the direction indicated by the arrow until they make a "click" sound.</ptxt>
<figure>
<graphic graphicname="A203842E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>If the fuel tube connector is not inserted to the correct position on the injector, the retainer cannot be turned far enough in the direction of the arrow.</ptxt>
</item>
<item>
<ptxt>Before connecting the fuel tube connector and fuel pipe, check that there is no damage or foreign matter on the connecting part of the fuel pipe.</ptxt>
</item>
<item>
<ptxt>After connecting the fuel tube connector and fuel pipe, check that they are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Fuel Addition Injector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000003TI5013X_01_0057" proc-id="RM22W0E___000054V00000">
<ptxt>INSTALL NO. 1 FUEL INJECTOR PROTECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 fuel injector protector with the bolt.</ptxt>
<figure>
<graphic graphicname="A271211" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Press the ventilation hose clip against the No. 1 fuel injector protector as shown in the illustration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0058" proc-id="RM22W0E___00005PB00000">
<ptxt>INSTALL CYLINDER HEAD COVER SILENCER RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the cylinder head cover silencer with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0059" proc-id="RM22W0E___000054W00000">
<ptxt>INSTALL NO. 2 FUEL INJECTOR PROTECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 fuel injector protector with the bolt.</ptxt>
<figure>
<graphic graphicname="A271212" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Press the ventilation hose clip against the No. 2 fuel injector protector as shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the hose clamp.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0060" proc-id="RM22W0E___00005PC00000">
<ptxt>INSTALL CYLINDER HEAD COVER SILENCER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the cylinder head cover silencer with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0061" proc-id="RM22W0E___000055100000">
<ptxt>INSTALL NO. 1 VACUUM TRANSMITTING PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the vacuum transmitting pipe with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>6.0</t-value1>
<t-value2>61</t-value2>
<t-value3>53</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 2 vacuum hoses.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0062" proc-id="RM22W0E___000055200000">
<ptxt>INSTALL NO. 1 VACUUM SWITCHING VALVE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the vacuum switching valve with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>6.0</t-value1>
<t-value2>61</t-value2>
<t-value3>53</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 2 vacuum hoses.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0063" proc-id="RM22W0E___000068V00000">
<ptxt>CONNECT ENGINE WIRE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>LH Side:</ptxt>
<s3>
<ptxt>Install the engine wire protector with the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Connect the 8 connectors.</ptxt>
</s3>
<s3>
<ptxt>Attach the wire harness clamp.</ptxt>
</s3>
<s3>
<ptxt>Install the engine wire harness bracket with the bolt.</ptxt>
</s3>
<s3>
<ptxt>for RHD:</ptxt>
<ptxt>Connect the wire harness with the wire harness clamp holder.</ptxt>
</s3>
<s3>
<ptxt>Attach the 3 wire harness clamps and connect the 2 connectors.</ptxt>
</s3>
<s3>
<ptxt>Attach the 3 wire harness clamps and connect the 4 connectors.</ptxt>
</s3>
</s2>
<s2>
<ptxt>RH Side:</ptxt>
<s3>
<ptxt>Install the engine wire harness protector with the 3 bolts.</ptxt>
</s3>
<s3>
<ptxt>Connect the 7 connectors.</ptxt>
</s3>
<s3>
<ptxt>Attach the wire harness clamp.</ptxt>
</s3>
<s3>
<ptxt>Install the wire harness bracket with the bolt.</ptxt>
</s3>
<s3>
<ptxt>Install the glow plug wire harness with the nut and screw grommet.</ptxt>
<torque>
<torqueitem>
<t-value1>4.0</t-value1>
<t-value2>41</t-value2>
<t-value3>35</t-value3>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>for RHD:</ptxt>
<ptxt>Install the wire harness clamp holder with the bolt.</ptxt>
</s3>
<s3>
<ptxt>for RHD:</ptxt>
<ptxt>Connect the wire harness with the wire harness clamp holder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Rear Side:</ptxt>
<s3>
<ptxt>Install the glow plug wire harness with the nut and screw grommet.</ptxt>
<torque>
<torqueitem>
<t-value1>4.0</t-value1>
<t-value2>41</t-value2>
<t-value3>35</t-value3>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>w/ DPF:</ptxt>
<ptxt>Connect the 3 connectors.</ptxt>
</s3>
<s3>
<ptxt>w/o DPF:</ptxt>
<ptxt>Connect the connector.</ptxt>
</s3>
<s3>
<ptxt>Install the engine wire harness protector with the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 ground wires with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.4</t-value1>
<t-value2>87</t-value2>
<t-value3>74</t-value3>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Attach the wire harness clamp.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0065" proc-id="RM22W0E___000053B00000">
<ptxt>INSTALL NO. 3 WATER BY-PASS PIPE (w/o Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 2 water hose ends, and install the No. 3 water by-pass pipe with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0033">
<ptxt>CONNECT FUEL HOSE</ptxt>
</s-1>
<s-1 id="RM000003TI5013X_01_0066" proc-id="RM22W0E___000033A00000">
<ptxt>INSTALL NO. 1 AIR CLEANER PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 1 air cleaner pipe to the No. 1 intake air connector pipe.</ptxt>
</s2>
<s2>
<ptxt>Install the pipe with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0067" proc-id="RM22W0E___000033B00000">
<ptxt>INSTALL HEATER WATER PIPE SUB-ASSEMBLY (w/ Viscous Heater)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 4 water hose ends, and install the water pipe with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0068" proc-id="RM22W0E___000053F00000">
<ptxt>INSTALL NO. 2 AIR CLEANER PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 2 air cleaner pipe to the No. 2 intake air connector pipe.</ptxt>
</s2>
<s2>
<ptxt>Connect the ventilation hose to the oil separator.</ptxt>
</s2>
<s2>
<ptxt>Install the pipe with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0069" proc-id="RM22W0E___000053G00000">
<ptxt>INSTALL NO. 4 AIR TUBE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 4 air tube with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A174723E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Top</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
<atten4>
<ptxt>Make sure the direction of the hose clamp is as shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the suction hose with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0070" proc-id="RM22W0E___000053H00000">
<ptxt>INSTALL NO. 2 AIR HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 2 air hose with the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A177379E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Top</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Align the paint mark of the air hose with the protrusion and push on the air hose so that distance B is 0 to 3 mm (0 to 0.118 in.).</ptxt>
</item>
<item>
<ptxt>Position the clamp so that distance A is 2 to 6 mm (0.0787 to 0.236 in.).</ptxt>
</item>
<item>
<ptxt>Make sure the direction of the hose clamp is as shown in the illustration.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0071" proc-id="RM22W0E___000033C00000">
<ptxt>INSTALL NO. 3 AIR TUBE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 3 air tube with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A174722E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Top</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
<atten4>
<ptxt>Make sure the direction of the hose clamp is as shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the wire harness bracket with the bolt.</ptxt>
</s2>
<s2>
<ptxt>Install the ground wire with the nut, and attach the wire harness clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>8.4</t-value1>
<t-value2>85</t-value2>
<t-value3>74</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0072" proc-id="RM22W0E___000033D00000">
<ptxt>INSTALL NO. 1 AIR HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 1 air hose with the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A177378E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Top</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Align the paint mark of the air hose with the protrusion and push on the air hose so that distance B is 0 to 3 mm (0 to 0.118 in.).</ptxt>
</item>
<item>
<ptxt>Position the clamp so that distance A is 2 to 6 mm (0.0787 to 0.236 in.).</ptxt>
</item>
<item>
<ptxt>Make sure the direction of the hose clamp is as shown in the illustration.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0073" proc-id="RM22W0E___000033E00000">
<ptxt>INSTALL INTAKE AIR CONNECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the intake air connector to the No. 1 and No. 2 air cleaner pipes.</ptxt>
</s2>
<s2>
<ptxt>Install the connector with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the 2 hose clamps.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 3 wire harness clamps.</ptxt>
</s2>
<s2>
<ptxt>w/o Viscous Heater:</ptxt>
<ptxt>Connect the connector to the water temperature sensor.</ptxt>
</s2>
<s2>
<ptxt>w/ Viscous Heater:</ptxt>
<ptxt>Connect the 2 connectors to the water temperature sensor and viscous with magnet clutch heater.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0074" proc-id="RM22W0E___000033F00000">
<ptxt>TEMPORARILY INSTALL NO. 1 AIR CLEANER HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the air cleaner hose to the intake air connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0075" proc-id="RM22W0E___000033G00000">
<ptxt>INSTALL AIR CLEANER CAP SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the air cleaner cap to the air cleaner hose, and install the air cleaner cap with the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Connect the mass air flow meter connector and attach the wire harness clamp to the air cleaner cap.</ptxt>
</s2>
<s2>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Align the protrusion of the air cleaner cap and the concave portion of the air cleaner hose.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 2 hose clamps.</ptxt>
<torque>
<torqueitem>
<t-value1>2.5</t-value1>
<t-value2>25</t-value2>
<t-value3>22</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0076" proc-id="RM22W0E___000034H00000">
<ptxt>INSTALL DIESEL THROTTLE BODY ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the intake pipe.</ptxt>
</s2>
<s2>
<ptxt>Install the throttle body with the 2 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="A163131" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the throttle position sensor connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the throttle motor connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0077" proc-id="RM22W0E___000054900000">
<ptxt>INSTALL NO. 3 INTERCOOLER SUPPORT BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 3 intercooler support bracket with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="A185145" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Manual Transmission:</ptxt>
<s3>
<ptxt>Connect the clutch tube to release cylinder 2 way with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Check that the union nut is tightened to the specified torque.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>154</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten4>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0078" proc-id="RM22W0E___000054A00000">
<ptxt>INSTALL NO. 1 GAS FILTER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 gas filter.</ptxt>
</s2>
<s2>
<ptxt>Connect the hose to the intake pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0079" proc-id="RM22W0E___000034I00000">
<ptxt>INSTALL AIR TUBE SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A181564E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install the air tube to the throttle body.</ptxt>
</s2>
<s2>
<ptxt>Align the embossed mark of the throttle body with the paint mark of the No. 4 air hose.</ptxt>
</s2>
<s2>
<ptxt>Tighten the hose clamp so that it is 7 to 11 mm (0.276 to 0.433 in.) from the end of the hose as shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A163828E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0080" proc-id="RM22W0E___000034L00000">
<ptxt>INSTALL DIESEL THROTTLE BODY ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the intake pipe.</ptxt>
</s2>
<s2>
<ptxt>Install the throttle body with the 2 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="A163132" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the throttle position sensor connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the throttle motor connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0081" proc-id="RM22W0E___000034J00000">
<ptxt>INSTALL AIR TUBE SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A181563E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install the air tube to the throttle body.</ptxt>
</s2>
<s2>
<ptxt>Align the embossed mark of the throttle body with the paint mark of the No. 4 air hose.</ptxt>
</s2>
<s2>
<ptxt>Tighten the hose clamp so that it is 7 to 11 mm (0.276 to 0.433 in.) from the end of the hose as shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A163828E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0082" proc-id="RM22W0E___000053I00000">
<ptxt>INSTALL NO. 2 ENGINE OIL LEVEL DIPSTICK GUIDE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to a new O-ring.</ptxt>
</s2>
<s2>
<ptxt>Install the O-ring to the No. 2 engine oil level dipstick guide.</ptxt>
</s2>
<s2>
<ptxt>Install the No. 2 engine oil level dipstick guide with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the ventilation hose to the cylinder head cover RH.</ptxt>
</s2>
<s2>
<ptxt>Connect the wire harness clamp to the No. 2 engine oil level dipstick guide bracket.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0050">
<ptxt>CONNECT WATER HOSE SUB-ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM000003TI5013X_01_0051" proc-id="RM22W0E___00006LJ00000">
<ptxt>INSTALL INTERCOOLER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>
<xref label="Seep01" href="RM0000031FQ003X"/>
</ptxt>
</content1>
</s-1>
<s-1 id="RM000003TI5013X_01_0011" proc-id="RM22W0E___00006LH00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003TI5013X_01_0017" proc-id="RM22W0E___000032D00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the engine air bleed cap.</ptxt>
<figure>
<graphic graphicname="A179103" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect a clear hose to the engine air bleed pipe.</ptxt>
<figure>
<graphic graphicname="A185226" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a wrench, remove the vent plug.</ptxt>
<figure>
<graphic graphicname="A177426" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Fill the radiator with TOYOTA SLLC to the radiator reservoir filler neck.</ptxt>
<atten4>
<ptxt>Pour TOYOTA SLLC until it spills out of the engine air bleed pipe.</ptxt>
</atten4>
<figure>
<graphic graphicname="A177425" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Capacity (for Automatic Transmission)</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>Front heater only</ptxt>
</entry>
<entry valign="middle">
<ptxt>14.8 liters (15.6 US qts, 13.0 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Front heater and rear heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>17.6 liters (18.6 US qts, 15.5 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Front heater with viscous heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>15.2 liters (16.1 US qts, 13.4 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Front heater and rear heater with viscous heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>18.0 liters (19.0 US qts, 15.4 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard capacity (for Manual Transmission)</title>
<specitem>
<ptxt>15.4 liters (16.3 US qts, 13.5 Imp. qts)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the vent plug.</ptxt>
<figure>
<graphic graphicname="A177426" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>2.0</t-value1>
<t-value2>20</t-value2>
<t-value3>18</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not tighten the plug to 5.0 N*m (51 kgf*cm, 44 in.*lbf) or more, as the plug will be damaged.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Disconnect the clear hose from the engine air bleed pipe.</ptxt>
<figure>
<graphic graphicname="A185226" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the engine air bleed cap when coolant comes out.</ptxt>
<figure>
<graphic graphicname="A179103" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the radiator reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
<atten3>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the FULL line.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Maintain an engine speed of 3000 rpm for approximately 10 minutes so that the thermostat opens and air bleeding is performed.</ptxt>
<atten2>
<list1 type="unordered">
<title>When pressing the radiator hoses:</title>
<item>
<ptxt>Wear protective gloves.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the radiator fan</ptxt>
</item>
</list1>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Pay attention to the needle of the water temperature meter. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand, and checking when the engine coolant starts to flow inside the hose.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Stop the engine, and wait until the engine coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Check that the coolant level is between the FULL and LOW lines.</ptxt>
<figure>
<graphic graphicname="A174740" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If the coolant level is above the FULL line, drain coolant so that the coolant level is between the FULL and LOW lines.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0022" proc-id="RM22W0E___00006EH00000">
<ptxt>BLEED AIR FROM FUEL SYSTEM
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using the hand pump mounted on the fuel filter cap, bleed air from the fuel system. Continue pumping until the pump resistance increases.</ptxt>
<figure>
<graphic graphicname="A175110" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The maximum hand pump pumping speed is 2 strokes per second.</ptxt>
</item>
<item>
<ptxt>The hand pump must be pushed with a full stroke during pumping.</ptxt>
</item>
<item>
<ptxt>When the fuel pressure at the supply pump inlet port reaches a saturated pressure, the hand pump resistance increases.</ptxt>
</item>
<item>
<ptxt>If pumping is interrupted during the air bleeding process, fuel in the fuel line may return to the fuel tank. Continue pumping until the hand pump resistance increases.</ptxt>
</item>
<item>
<ptxt>If the hand pump resistance does not increase despite consecutively pumping 200 times or more, there may be a fuel leak between the fuel tank and fuel filter, the hand pump may be malfunctioning, or the vehicle may have run out of fuel.</ptxt>
</item>
<item>
<ptxt>If air bleeding using the hand pump is incomplete, the common rail pressure does not rise to the pressure range necessary for normal use and the engine cannot be started.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Check if the engine starts.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Even if air bleeding using the hand pump has been completed, the starter may need to be cranked for 10 seconds or more to start the engine.</ptxt>
</item>
<item>
<ptxt>Do not crank the engine continuously for more than 20 seconds. The battery may be discharged.</ptxt>
</item>
<item>
<ptxt>Use a fully-charged battery.</ptxt>
</item>
</list1>
</atten3>
<s3>
<ptxt>When the engine can be started, proceed to the next step.</ptxt>
</s3>
<s3>
<ptxt>If the engine cannot be started, bleed air again using the hand pump until the hand pump resistance increases (refer to the procedures above). Then start the engine.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Turn the engine switch off.</ptxt>
</s2>
<s2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the engine switch ON (IG) and turn the GTS on.</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</s2>
<s2>
<ptxt>Start the engine.*1</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Test the Fuel Leak.*2</ptxt>
</s2>
<s2>
<ptxt>Perform the following test 5 times with on/off intervals of 10 seconds: Active Test / Test the Fuel Leak.*3</ptxt>
</s2>
<s2>
<ptxt>Allow the engine to idle for 3 minutes or more after performing the Active Test for the 5th time.</ptxt>
<figure>
<graphic graphicname="A192848E04" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>When the Active Test "Test the Fuel Leak" is used to change the pump control mode, the actual fuel pressure inside the common rail drops below the target fuel pressure when the Active Test is off, but this is normal and does not indicate a pump malfunction.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</s2>
<s2>
<ptxt>Read Current DTCs.</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK14JX"/>).</ptxt>
<atten4>
<ptxt>It is necessary to clear the DTCs as DTC P1604 or P1605 may be stored when air is bled from the fuel system after replacing or repairing fuel system parts.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Repeat steps *1 to *3.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</s2>
<s2>
<ptxt>Read Current DTCs.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0087" proc-id="RM22W0E___00006LK00000">
<ptxt>PERFORM DPF FORCIBLE REGENERATION PROCEDURE</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000003TI5013X_01_0016" proc-id="RM22W0E___000032E00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<figure>
<graphic graphicname="A174739" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester to the radiator reservoir.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 17.8 psi), and check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and water pump for leaks.</ptxt>
<ptxt>If no external leaks are found, check the cylinder block and cylinder head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0018" proc-id="RM22W0E___00006EF00000">
<ptxt>INSPECT FOR FUEL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Perform the Active Test.</ptxt>
<s3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the engine switch to ON (IG).</ptxt>
</s3>
<s3>
<ptxt>Start the engine.</ptxt>
</s3>
<s3>
<ptxt>Turn the GTS on.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</s3>
<s3>
<ptxt>Perform the Active Test.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Detail</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Test the Fuel Leak</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressurizes fuel inside common rail and checks for fuel leaks</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Stop/Start</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>The fuel inside the common rail is pressurized to the specified value and the engine speed increases to 2000 rpm when Start is selected.</ptxt>
</item>
<item>
<ptxt>The above conditions are preserved while Start is selected.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0083" proc-id="RM22W0E___000068W00000">
<ptxt>INSPECT FOR OIL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Warm up the engine and check for an engine oil leak.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0084" proc-id="RM22W0E___000054P00000">
<ptxt>INSTALL UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the upper radiator support seal with the 7 clips.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0020" proc-id="RM22W0E___000032F00000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover with the 10 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0085" proc-id="RM22W0E___000032G00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front fender splash shield RH with the clip, and then install the 3 bolts and 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003TI5013X_01_0086" proc-id="RM22W0E___000032H00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front fender splash shield LH with the clip, and then install the 3 bolts and screw.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>