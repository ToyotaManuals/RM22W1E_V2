<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000004ID3014X" category="C" type-id="8068S" name-id="ESZKK-01" from="201301" to="201308">
<dtccode>P200C</dtccode>
<dtcname>Diesel Particulate Filter Temperature Too High Bank 1</dtcname>
<dtccode>P200D</dtccode>
<dtcname>Diesel Particulate Filter Temperature Too High Bank 2</dtcname>
<dtccode>P200E</dtccode>
<dtcname>Catalyst System Temperature Too High Bank 1</dtcname>
<dtccode>P200F</dtccode>
<dtcname>Catalyst System Temperature Too High Bank 2</dtcname>
<dtccode>P2428</dtccode>
<dtcname>Exhaust Gas Temperature Too High Bank 1</dtcname>
<dtccode>P2429</dtccode>
<dtcname>Exhaust Gas Temperature Too High Bank 2</dtcname>
<subpara id="RM000004ID3014X_05" type-id="60" category="03" proc-id="RM22W0E___000042200000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A268071E03" width="7.106578999in" height="4.7836529in"/>
</figure>
<ptxt>The exhaust fuel addition injector assembly is mounted on the exhaust port of the cylinder head, and low pressure fuel is supplied to the exhaust fuel addition injector assembly by the feed pump in the fuel supply pump assembly. This exhaust fuel addition injector assembly adds fuel in response to a control signal from the ECM, in order to perform PM forced regeneration.</ptxt>
<ptxt>During PM forced regeneration, the exhaust fuel addition injector assembly adds fuel to raise the DPF* catalyst temperature.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>For more information on the exhaust fuel addition injector assembly and DPF, refer to the following procedures (See page <xref label="Seep01" href="RM000000XSN037X"/>).</ptxt>
</item>
<item>
<ptxt>If P200C, P200D, P200E, P200F, P2428 and P2429 are present, refer to the diagnostic trouble code (DTC) chart for Diesel Particulate Filter System (See page <xref label="Seep02" href="RM000000XSN037X"/>).</ptxt>
</item>
</list1>
<ptxt>*: Diesel Particulate Filter</ptxt>
</atten4>
<table pgwide="1">
<title>P200C (Bank 1), P200D (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>PM forced regeneration*</ptxt>
<ptxt>*: When P200C, P200D are detected due to a DPF catalyst problem such as thermal degradation, because the malfunction cannot be duplicated even if the vehicle is driven after PM forced regeneration is performed, perform troubleshooting by following the diagnosis procedure and checking the freeze frame data.</ptxt>
</entry>
<entry valign="middle">
<ptxt>The value of "Exhaust Temperature B1S3, B2S3" is higher than 1000°C (1832°F) for 5 seconds. </ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<title>Main Trouble Area</title>
<item>
<ptxt>Open or short in exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>Exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
</list1>
<list1 type="unordered">
<title>Sub Trouble Area</title>
<item>
<ptxt>No. 4 exhaust gas temperature sensor (B1S3, B2S3)</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>Mass air flow meter</ptxt>
</item>
<item>
<ptxt>Manifold absolute pressure sensor</ptxt>
</item>
<item>
<ptxt>Fuel leaks or blockages in exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>Suction control valve (fuel supply pump assembly)</ptxt>
</item>
<item>
<ptxt>Injector assembly</ptxt>
</item>
<item>
<ptxt>Blockages or leaks in air intake system</ptxt>
</item>
<item>
<ptxt>Blockages or leaks in exhaust system</ptxt>
</item>
<item>
<ptxt>Blockages or leaks in EGR system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
<list1 type="unordered">
<title>Common Inspection Item:</title>
<item>
<ptxt>Fuel injection system</ptxt>
</item>
<item>
<ptxt>Intake/exhaust system</ptxt>
</item>
<item>
<ptxt>After treatment control system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P200E (Bank 1), P200F (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>PM forced regeneration*</ptxt>
<ptxt>*: When P200E, P200F are detected due to a DPF catalyst problem such as thermal degradation, because the malfunction cannot be duplicated even if the vehicle is driven after PM forced regeneration is performed, perform troubleshooting by following the diagnosis procedure and checking the freeze frame data.</ptxt>
</entry>
<entry valign="middle">
<ptxt>The value of "Exhaust Temperature B1S2, B2S2" is higher than 1000°C (1832°F) for 5 seconds.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<title>Main Trouble Area</title>
<item>
<ptxt>Open or short in exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>Exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Monolithic converter assembly RH (for bank 1 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Monolithic converter assembly LH (for bank 2 CCo catalytic converter)</ptxt>
</item>
</list1>
<list1 type="unordered">
<title>Sub Trouble Area</title>
<item>
<ptxt>No. 2 exhaust gas temperature sensor (B1S2)</ptxt>
</item>
<item>
<ptxt>No. 3 exhaust gas temperature sensor (B2S2)</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>Mass air flow meter</ptxt>
</item>
<item>
<ptxt>Manifold absolute pressure sensor</ptxt>
</item>
<item>
<ptxt>Fuel leaks or blockages in exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>Suction control valve (fuel supply pump assembly)</ptxt>
</item>
<item>
<ptxt>Injector assembly</ptxt>
</item>
<item>
<ptxt>Blockages or leaks in air intake system</ptxt>
</item>
<item>
<ptxt>Blockages or leaks in exhaust system</ptxt>
</item>
<item>
<ptxt>Blockages or leaks in EGR system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
<list1 type="unordered">
<title>Common Inspection Item:</title>
<item>
<ptxt>Fuel injection system</ptxt>
</item>
<item>
<ptxt>Intake/exhaust system</ptxt>
</item>
<item>
<ptxt>After treatment control system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2428 (Bank 1), P2429 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>

<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>PM forced regeneration*</ptxt>
<ptxt>*: When P2428, P2429 are detected due to a DPF catalyst problem such as thermal degradation, because the malfunction cannot be duplicated even if the vehicle is driven after PM forced regeneration is performed, perform troubleshooting by following the diagnosis procedure and checking the freeze frame data.</ptxt>
</entry>
<entry valign="middle">
<ptxt>The value of "Exhaust Temperature B1S1, B2S1" is higher than 1000°C (1832°F) for 5 seconds.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<title>Main Trouble Area</title>
<item>
<ptxt>Open or short in exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>Exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>Monolithic converter assembly RH (for bank 1 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Monolithic converter assembly LH (for bank 2 CCo catalytic converter)</ptxt>
</item>
</list1>
<list1 type="unordered">
<title>Sub Trouble Area</title>
<item>
<ptxt>Exhaust gas temperature sensor (B1S1, B2S1)</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>Mass air flow meter</ptxt>
</item>
<item>
<ptxt>Manifold absolute pressure sensor</ptxt>
</item>
<item>
<ptxt>Fuel leaks or blockages in exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>Suction control valve (fuel supply pump assembly)</ptxt>
</item>
<item>
<ptxt>Injector assembly</ptxt>
</item>
<item>
<ptxt>Blockages or leaks in air intake system</ptxt>
</item>
<item>
<ptxt>Blockages or leaks in exhaust system</ptxt>
</item>
<item>
<ptxt>Blockages or leaks in EGR system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
<list1 type="unordered">
<title>Common Inspection Item:</title>
<item>
<ptxt>Fuel injection system</ptxt>
</item>
<item>
<ptxt>Intake/exhaust system</ptxt>
</item>
<item>
<ptxt>After treatment control system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Due to fail-safe operation when other DTCs (that involve the engine power limit) are stored, PM forced regeneration may be prohibited.</ptxt>
<ptxt>This DTC or P20CF, P20D5, P244B, P2465, P244C, P244E and P2463 may be stored due to the fact that PM forced regeneration is prohibited due to the fail-safe operation of other DTCs.</ptxt>
</item>
<item>
<ptxt>The air-fuel ratio and the exhaust gas temperature can be checked by entering the following menus on the GTS: Engine and ECT / Data List / AF Lambda B1S1 (AF Lambda B2S1), Exhaust Temperature B1S1 (Exhaust Temperature B2S1), Exhaust Temperature B1S2 (Exhaust Temperature B2S2) and Exhaust Temperature B1S3 (Exhaust Temperature B2S3).</ptxt>
</item>
<item>
<ptxt>Malfunctions in the engine itself may affect the DPF system control and cause storage of these DTCs. For example, malfunctions in the injector assemblies that cause a large amount of smoke emission: These affect the exhaust fuel addition injector assembly operation. Blockages or leaks in the air intake system, or an EGR system malfunction such as EGR passage blockages: These malfunctions affect the DPF catalyst temperature control. Therefore, the engine condition itself should also be checked, in addition to the exhaust fuel addition injector assembly.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P200C</ptxt>
<ptxt>P200D</ptxt>
<ptxt>P200E</ptxt>
<ptxt>P200F</ptxt>
<ptxt>P2428</ptxt>
<ptxt>P2429</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>MAF</ptxt>
</item>
<item>
<ptxt>MAP</ptxt>
</item>
<item>
<ptxt>Intake Air Temp (Turbo)</ptxt>
</item>
<item>
<ptxt>Target Booster Pressure</ptxt>
</item>
<item>
<ptxt>Atmosphere Pressure</ptxt>
</item>
<item>
<ptxt>Coolant Temp</ptxt>
</item>
<item>
<ptxt>Engine Speed</ptxt>
</item>
<item>
<ptxt>AFS Voltage B1S1</ptxt>
</item>
<item>
<ptxt>AFS Voltage B2S1</ptxt>
</item>
<item>
<ptxt>AF Lambda B1S1</ptxt>
</item>
<item>
<ptxt>AF Lambda B2S1</ptxt>
</item>
<item>
<ptxt>Exhaust Fuel Addition FB</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B1S1</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B2S1</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B1S2</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B2S2</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B1S3</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B2S3</ptxt>
</item>
<item>
<ptxt>Catalyst Differential Press</ptxt>
</item>
<item>
<ptxt>Catalyst Differential Press #2</ptxt>
</item>
<item>
<ptxt>Injection Feedback Val #1 to #8</ptxt>
</item>
<item>
<ptxt>Injection Volume</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000004ID3014X_06" type-id="64" category="03" proc-id="RM22W0E___000042300000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM monitors DPF catalyst converter temperature. When the temperature becomes high due to uncombusted fuel, engine oil or thermal degradation of the DPF catalyst converter, the ECM illuminates the MIL.</ptxt>
<figure>
<graphic graphicname="A244058E05" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000004ID3014X_03" type-id="51" category="05" proc-id="RM22W0E___000041R00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000004ID3014X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004ID3014X_04_0011" proc-id="RM22W0E___000041Z00000">
<testtitle>READ OUTPUT DTC (RECORD STORED DTC AND FREEZE FRAME DATA) (PROCEDURE 1)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Record the stored DTC and freeze frame data.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID3014X_04_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID3014X_04_0013" proc-id="RM22W0E___000042000000">
<testtitle>REPLACE EXHAUST GAS TEMPERATURE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Confirm "Exhaust Temperature B1S1", "Exhaust Temperature B1S2", "Exhaust Temperature B1S3", "Exhaust Temperature B2S1", "Exhaust Temperature B2S2" or "Exhaust Temperature B2S3" in the freeze frame data which was recorded in procedure 1 and determine which exhaust gas temperature sensor to replace.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.60in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Items to be Replaced</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC P2428 is output and the value of "Exhaust Temperature B1S1" is 850°C (1562°F) or higher</ptxt>
</entry>
<entry>
<ptxt>Exhaust gas temperature sensor (B1S1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC P2429 is output and the value of "Exhaust Temperature B2S1" is 850°C (1562°F) or higher</ptxt>
</entry>
<entry>
<ptxt>Exhaust gas temperature sensor (B2S1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC P200E is output and the value of "Exhaust Temperature B1S2" is 850°C (1562°F) or higher</ptxt>
</entry>
<entry>
<ptxt>No. 2 exhaust gas temperature sensor (B1S2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC P200F is output and the value of "Exhaust Temperature B2S2" is 850°C (1562°F) or higher</ptxt>
</entry>
<entry>
<ptxt>No. 3 exhaust gas temperature sensor (B2S2)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC P200C is output and the value of "Exhaust Temperature B1S3" is 850°C (1562°F) or higher</ptxt>
</entry>
<entry>
<ptxt>No. 4 exhaust gas temperature sensor (B1S3)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC P200D is output and the value of "Exhaust Temperature B2S3" is 850°C (1562°F) or higher</ptxt>
</entry>
<entry>
<ptxt>No. 4 exhaust gas temperature sensor (B2S3)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>If DTC P200C, P200D, P200E, P200F, P2428 or P2429 is output, the exhaust gas temperature sensors need to be replaced. However, make sure to perform all diagnostic procedures for P200C, P200D, P200E, P200F, P2428 or P2429 before replacing the exhaust gas temperature sensors.</ptxt>
<atten4>
<ptxt>If the exhaust gas temperature sensors are subjected to an abnormally high temperature, the internal resistance of the sensor increases. This causes incorrect temperature measurement.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID3014X_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID3014X_04_0002" proc-id="RM22W0E___000041S00000">
<testtitle>CHECK FOR BLACK SMOKE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Start the engine and drive the vehicle until the engine coolant temperature reaches 60°C (140°F) or higher.</ptxt>
</test1>
<test1>
<ptxt>Stop the vehicle and allow the engine to idle.</ptxt>
</test1>
<test1>
<ptxt>Fully depress the accelerator pedal for 5 seconds, and then release it [A].</ptxt>
</test1>
<test1>
<ptxt>Repeat the above procedure [A] 10 times [B].</ptxt>
</test1>
<test1>
<ptxt>Check for black smoke emission during procedure [A] and [B].</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Black smoke is emitted less than 5 times.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Even if the black smoke is very thin, count the number of black smoke emissions if there is any visible smoke.</ptxt>
</atten4>
</test1>
</content6>
<res>
<right ref="RM000004ID3014X_04_0004" fin="false">OK</right>
<right ref="RM000004ID3014X_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004ID3014X_04_0003" proc-id="RM22W0E___000041T00000">
<testtitle>REPLACE CATALYTIC CONVERTER (DPF, CCo  CATALYTIC CONVERTER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Determine the parts to replace according to the DTCs output in procedure 1.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Items to be Replaced</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P2428 is output</ptxt>
</entry>
<entry>
<ptxt>Monolithic converter assembly RH (for bank 1 CCo catalytic converter)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2429 is output</ptxt>
</entry>
<entry>
<ptxt>Monolithic converter assembly LH (for bank 2 CCo catalytic converter)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P200E is output</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Monolithic converter assembly RH (for bank 1 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P200F is output</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Monolithic converter assembly LH (for bank 2 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P200C is output</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P200D is output</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Replace the DPF catalytic converter for bank 1 and bank 2 at the same time.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Even though it has been determined that the exhaust pipe assemblies (DPF catalytic converter) (for bank 1 or bank 2) needs to be replaced, make sure to perform all diagnostic procedures for P200C, P200D, P200E, P200F, P2428 or P2429 before replacing it.</ptxt>
<atten4>
<ptxt>This DTC is stored because an excessive amount of PM has accumulated and resulted in abnormal combustion. It is necessary to diagnose the cause of abnormal PM accumulation and repair the problem. Therefore, do not perform replacement at this time.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID3014X_04_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID3014X_04_0004" proc-id="RM22W0E___000041U00000">
<testtitle>CHECK FUEL INJECTION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the fuel injection system (See page <xref label="Seep01" href="RM000004KNK012X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID3014X_04_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID3014X_04_0005" proc-id="RM22W0E___000041V00000">
<testtitle>CHECK INTAKE / EXHAUST SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the Intake/exhaust system (See page <xref label="Seep01" href="RM000004KNL00TX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID3014X_04_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID3014X_04_0007" proc-id="RM22W0E___000041W00000">
<testtitle>CHECK AFTER TREATMENT CONTROL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the after treatment control system (See page <xref label="Seep01" href="RM000004KNM00LX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID3014X_04_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID3014X_04_0008" proc-id="RM22W0E___000041X00000">
<testtitle>REPLACE MALFUNCTIONING PARTS</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>Replace the following items only when it has been determined necessary based on diagnosis for P200C, P200D, P200E, P200F, P2428 or P2429.</ptxt>
</atten3>
<test1>
<ptxt>Replace the exhaust gas temperature sensor (B1S1) (See page <xref label="Seep01" href="RM000003TEF00XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the exhaust gas temperature sensor (B2S1) (See page <xref label="Seep02" href="RM000003TEF00XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the No. 2 exhaust gas temperature sensor (B1S2) (See page <xref label="Seep03" href="RM000003TEF00XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the No. 3 exhaust gas temperature sensor (B2S2) (See page <xref label="Seep04" href="RM000003TEF00XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the No. 4 exhaust gas temperature sensor (B1S3) (See page <xref label="Seep05" href="RM000003TEF00XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the No. 4 exhaust gas temperature sensor (B2S3) (See page <xref label="Seep06" href="RM000003TEF00XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the exhaust pipe assembly (for bank 1 DPF catalytic converter) (See page <xref label="Seep07" href="RM000003B0N00HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter) (See page <xref label="Seep08" href="RM000003B0N00HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the monolithic converter assembly RH (for bank 1 CCo catalytic converter) (See page <xref label="Seep09" href="RM000003B0N00HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the monolithic converter assembly LH (for bank 2 CCo catalytic converter) (See page <xref label="Seep10" href="RM000003B0N00HX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID3014X_04_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID3014X_04_0014" proc-id="RM22W0E___000042100000">
<testtitle>CATALYST RECORD CLEAR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform the catalyst record clear (See page <xref label="Seep01" href="RM000000TIN06OX"/>).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the DPF catalytic converter or CCo catalytic converter is replaced, perform "catalyst record clear".</ptxt>
</item>
<item>
<ptxt>If the DPF catalytic converter and CCo catalytic converter were not replaced, proceed to the next step.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID3014X_04_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID3014X_04_0009" proc-id="RM22W0E___000041Y00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform PM forced regeneration (See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID3014X_04_0010" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID3014X_04_0010">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>