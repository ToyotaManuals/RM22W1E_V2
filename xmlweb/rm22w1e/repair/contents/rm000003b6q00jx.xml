<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S002F" variety="S002F">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S002F_7C3WY_T00Q1" variety="T00Q1">
<name>FRONT CONSOLE BOX (w/ Cool Box)</name>
<para id="RM000003B6Q00JX" category="A" type-id="30014" name-id="IT22Z-02" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM000003B6Q00JX_02" type-id="11" category="10" proc-id="RM22W0E___0000HRZ00000">
<content3 releasenbr="1">
<atten4>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0DSX"/>).</ptxt>
</atten4>
</content3>
</subpara>
<subpara id="RM000003B6Q00JX_01" type-id="01" category="01">
<s-1 id="RM000003B6Q00JX_01_0001" proc-id="RM22W0E___0000HRU00000">
<ptxt>INSTALL COOLING BOX ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182475" width="7.106578999in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Sufficiently apply compressor oil to 2 new O-rings.</ptxt>
<spec>
<title>Compressor oil</title>
<specitem>
<ptxt>ND-OIL 8 or equivalent</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Install the 2 O-rings on the 2 cooler pipes.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 cooler pipes with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the cooling box with the 4 bolts and 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0003" proc-id="RM22W0E___0000HRW00000">
<ptxt>INSTALL REAR CONSOLE END PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors and 2 clamps.</ptxt>
<figure>
<graphic graphicname="B182503" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 8 claws to install the end panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0002" proc-id="RM22W0E___0000HRV00000">
<ptxt>INSTALL NO. 1 COOLER COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw on the upper part of the cooler cover, and then attach the clamp on the lower part of the cooler cover to install it.</ptxt>
<figure>
<graphic graphicname="B183312" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0004" proc-id="RM22W0E___0000AAR00000">
<ptxt>INSTALL UPPER CONSOLE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
<figure>
<graphic graphicname="B180011" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 14 claws to install the console panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0005" proc-id="RM22W0E___0000AAS00000">
<ptxt>INSTALL CONSOLE CUP HOLDER BOX SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws to install the box.</ptxt>
<figure>
<graphic graphicname="B180010" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0006" proc-id="RM22W0E___0000GEP00000">
<ptxt>INSTALL REAR UPPER CONSOLE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 9 claws to install the panel.</ptxt>
<figure>
<graphic graphicname="B180009" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0007" proc-id="RM22W0E___0000AAT00000">
<ptxt>INSTALL LOWER CENTER INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
<figure>
<graphic graphicname="B180006" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 7 claws to install the panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0008" proc-id="RM22W0E___0000AAU00000">
<ptxt>INSTALL SHIFT LEVER KNOB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the shift lever knob and twist it in the direction indicated by the arrow.</ptxt>
<figure>
<graphic graphicname="B186291E01" width="7.106578999in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0009" proc-id="RM22W0E___0000AAV00000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 7 claws to install the panel pad.</ptxt>
<figure>
<graphic graphicname="B180005" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0010" proc-id="RM22W0E___0000GEQ00000">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL FINISH CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 7 claws to install the panel cushion.</ptxt>
<figure>
<graphic graphicname="B182501" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0011" proc-id="RM22W0E___0000AAX00000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors and 2 clamps.</ptxt>
<figure>
<graphic graphicname="B181910" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 8 claws to install the panel pad.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0012" proc-id="RM22W0E___0000AAY00000">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 7 claws to install the panel cushion.</ptxt>
<figure>
<graphic graphicname="E155134" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0017" proc-id="RM22W0E___0000HRX00000">
<ptxt>INSTALL FRONT SEAT ASSEMBLY LH (for Manual Seat)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front seat assembly LH (See page <xref label="Seep01" href="RM00000390Q00VX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0018" proc-id="RM22W0E___0000HRY00000">
<ptxt>INSTALL FRONT SEAT ASSEMBLY LH (for Power Seat)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front seat assembly LH (See page <xref label="Seep01" href="RM00000390Q00WX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6Q00JX_01_0014" proc-id="RM22W0E___000048900000">
<ptxt>CHARGE REFRIGERANT
</ptxt>
<content1 releasenbr="2">
<sst>
<sstitem>
<s-number>09985-20010</s-number>
<s-subnumber>09985-02130</s-subnumber>
<s-subnumber>09985-02150</s-subnumber>
<s-subnumber>09985-02090</s-subnumber>
<s-subnumber>09985-02110</s-subnumber>
<s-subnumber>09985-02010</s-subnumber>
<s-subnumber>09985-02050</s-subnumber>
<s-subnumber>09985-02060</s-subnumber>
<s-subnumber>09985-02070</s-subnumber>
</sstitem>
</sst>
<s2>
<ptxt>Perform vacuum purging using a vacuum pump.</ptxt>
</s2>
<s2>
<ptxt>Charge refrigerant HFC-134a (R134a).</ptxt>
<table pgwide="1">
<title>Standard:</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Condenser Core Thickness</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Conditioning Type</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>Refrigerant Charging Amount</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="3" valign="middle">
<ptxt>22 mm (0.866 in.)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>w/o Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>870 +/-30 g (30.7 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>870 +/-30 g (30.7 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>w/ Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>1010 +/-30 g (35.6 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>960 +/-30 g (33.9 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3" valign="middle">
<ptxt>16 mm (0.630 in.)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>w/o Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>770 +/-30 g (27.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>770 +/-30 g (27.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>w/ Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>970 +/-30 g (34.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>920 +/-30 g (32.5 +/-1.1 oz.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="I037365E19" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not operate the cooler compressor before charging refrigerant as the cooler compressor will not work properly without any refrigerant, and will overheat.</ptxt>
</item>
<item>
<ptxt>Approximately 200 g (7.05 oz.) of refrigerant may need to be charged after bubbles disappear. The refrigerant amount should be checked by measuring its quantity, and not with the sight glass.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000003B6Q00JX_01_0015" proc-id="RM22W0E___000048A00000">
<ptxt>WARM UP ENGINE
</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Warm up the engine at less than 1850 rpm for 2 minutes or more after charging the refrigerant.</ptxt>
<atten3>
<ptxt>Be sure to warm up the compressor when turning the A/C switch is on after removing and installing the cooler refrigerant lines (including the compressor), to prevent damage to the compressor.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000003B6Q00JX_01_0016" proc-id="RM22W0E___000048B00000">
<ptxt>CHECK FOR REFRIGERANT GAS LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>After recharging the refrigerant gas, check for refrigerant gas leakage using a halogen leak detector.</ptxt>
</s2>
<s2>
<ptxt>Perform the operation under these conditions:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stop the engine.</ptxt>
</item>
<item>
<ptxt>Secure good ventilation (the halogen leak detector may react to volatile gases other than refrigerant, such as evaporated gasoline or exhaust gas).</ptxt>
</item>
<item>
<ptxt>Repeat the test 2 or 3 times.</ptxt>
</item>
<item>
<ptxt>Make sure that some refrigerant remains in the refrigeration system. When compressor is off: approximately 392 to 588 kPa (4.0 to 6.0 kgf/cm<sup>2</sup>, 57 to 85 psi).</ptxt>
</item>
</list1>
</s2>
<s2>
<ptxt>Using a halogen leak detector, check the refrigerant line for leakage.</ptxt>
<figure>
<graphic graphicname="I042222E21" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>If a gas leak is not detected on the drain hose, remove the blower motor control (blower resistor) from the cooling unit. Insert the halogen leak detector sensor into the unit and perform the test.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and wait for approximately 20 minutes. Bring the halogen leak detector close to the pressure switch and perform the test.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>