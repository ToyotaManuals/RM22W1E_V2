<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000Q" variety="S000Q">
<name>1UR-FE INTAKE / EXHAUST</name>
<ttl id="12010_S000Q_7C3J6_T00C9" variety="T00C9">
<name>EXHAUST MANIFOLD</name>
<para id="RM0000031GX01WX" category="A" type-id="80001" name-id="IE28K-01" from="201301">
<name>REMOVAL</name>
<subpara id="RM0000031GX01WX_01" type-id="01" category="01">
<s-1 id="RM0000031GX01WX_01_0074" proc-id="RM22W0E___000013G00000">
<ptxt>REMOVE V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Raise the front of the V-bank cover to detach the 3 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<figure>
<graphic graphicname="A274415E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000031GX01WX_01_0075" proc-id="RM22W0E___000015000000">
<ptxt>REMOVE AIR CLEANER AND HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 PCV hose and No. 1 air hose.</ptxt>
<figure>
<graphic graphicname="A271401" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the mass air flow meter connector and detach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and loosen the hose clamp, and then remove the air cleaner and hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GX01WX_01_0059" proc-id="RM22W0E___000011Z00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GX01WX_01_0056" proc-id="RM22W0E___000011Y00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GX01WX_01_0057" proc-id="RM22W0E___000013S00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts and No. 1 engine under cover sub-assembly.</ptxt>
<figure>
<graphic graphicname="A178461" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000031GX01WX_01_0076" proc-id="RM22W0E___00004JA00000">
<ptxt>REMOVE NO. 2 ENGINE UNDER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and No. 2 engine under cover.</ptxt>
<figure>
<graphic graphicname="A178462" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000031GX01WX_01_0060" proc-id="RM22W0E___00004KC00000">
<ptxt>REMOVE FRONT FENDER APRON TRIM PACKING B</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ KDSS:</ptxt>
<ptxt>Remove the 3 clips and front fender apron trim packing B.</ptxt>
<figure>
<graphic graphicname="A177004" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o KDSS:</ptxt>
<ptxt>Remove the 4 clips and front fender apron trim packing B.</ptxt>
<figure>
<graphic graphicname="A176301" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0067" proc-id="RM22W0E___00004KD00000">
<ptxt>REMOVE FRONT FENDER APRON TRIM PACKING D</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 clips and front fender apron trim packing D.</ptxt>
<figure>
<graphic graphicname="A177005" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0061" proc-id="RM22W0E___000016K00000">
<ptxt>REMOVE FRONT FENDER APRON TRIM PACKING A</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 clips and front fender apron trim packing A.</ptxt>
<figure>
<graphic graphicname="A177003" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0062" proc-id="RM22W0E___00004KE00000">
<ptxt>REMOVE FRONT FENDER APRON TRIM PACKING C</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 clips and front fender apron trim packing C.</ptxt>
<figure>
<graphic graphicname="A177002" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0068" proc-id="RM22W0E___00006O500000">
<ptxt>REMOVE FRONT EXHAUST PIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000456J00JX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0070" proc-id="RM22W0E___00006O600000">
<ptxt>REMOVE FRONT PROPELLER SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000018QS00NX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0072" proc-id="RM22W0E___00006O700000">
<ptxt>REMOVE PROPELLER SHAFT HEAT INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and heat insulator.</ptxt>
<figure>
<graphic graphicname="A175007" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0054" proc-id="RM22W0E___00004K400000">
<ptxt>REMOVE NO. 2 MANIFOLD STAY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and No. 2 manifold stay.</ptxt>
<figure>
<graphic graphicname="A184508" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0022" proc-id="RM22W0E___00004JX00000">
<ptxt>REMOVE NO. 2 EXHAUST MANIFOLD HEAT INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and No. 2 exhaust manifold heat insulator.</ptxt>
<figure>
<graphic graphicname="A218610" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0023" proc-id="RM22W0E___00004JY00000">
<ptxt>REMOVE EXHAUST MANIFOLD ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the air fuel ratio sensor connector and detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="A218609" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Secondary Air Injection System:</ptxt>
<ptxt>Remove the 9 nuts, exhaust manifold and 2 gaskets.</ptxt>
<figure>
<graphic graphicname="A268566E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>w/ Secondary Air Injection System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>w/o Secondary Air Injection System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/o Secondary Air Injection System:</ptxt>
<ptxt>Remove the 7 nuts, exhaust manifold and gasket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0055" proc-id="RM22W0E___00004K500000">
<ptxt>REMOVE MANIFOLD STAY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and manifold stay.</ptxt>
<figure>
<graphic graphicname="A184511" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0073" proc-id="RM22W0E___00004JW00000">
<ptxt>REMOVE ENGINE OIL LEVEL DIPSTICK GUIDE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the engine oil level dipstick.</ptxt>
<figure>
<graphic graphicname="A218612" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the engine wire clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and engine oil level dipstick guide.</ptxt>
</s2>
<s2>
<ptxt>Remove the O-ring from the engine oil level dipstick guide.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0024" proc-id="RM22W0E___00004JZ00000">
<ptxt>REMOVE NO. 1 EXHAUST MANIFOLD HEAT INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and heat insulator.</ptxt>
<figure>
<graphic graphicname="A218614" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0025" proc-id="RM22W0E___00004K000000">
<ptxt>REMOVE EXHAUST MANIFOLD ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the air fuel ratio sensor connector and detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="A218613" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Secondary Air Injection System:</ptxt>
<ptxt>Remove the 9 nuts, exhaust manifold and 2 gaskets.</ptxt>
<figure>
<graphic graphicname="A268567E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>w/ Secondary Air Injection System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>w/o Secondary Air Injection System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/o Secondary Air Injection System:</ptxt>
<ptxt>Remove the 7 nuts, exhaust manifold and 2 gaskets.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GX01WX_01_0052" proc-id="RM22W0E___000016100000">
<ptxt>REMOVE AIR FUEL RATIO SENSOR (for Bank 1 Sensor 1)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the air fuel ratio sensor from the exhaust manifold LH.</ptxt>
<sst>
<sstitem>
<s-number>09224-00010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A218732E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000031GX01WX_01_0053" proc-id="RM22W0E___000016200000">
<ptxt>REMOVE AIR FUEL RATIO SENSOR (for Bank 2 Sensor 1)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the air fuel ratio sensor from the exhaust manifold RH.</ptxt>
<figure>
<graphic graphicname="A218733E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09224-00010</s-number>
</sstitem>
</sst>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>