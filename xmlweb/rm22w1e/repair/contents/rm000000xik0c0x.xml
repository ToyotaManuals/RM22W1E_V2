<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000000XIK0C0X" category="C" type-id="803LY" name-id="BCEHA-01" from="201301" to="201308">
<dtccode>C1210</dtccode>
<dtcname>Zero Point Calibration of Yaw Rate Sensor Undone</dtcname>
<dtccode>C1336</dtccode>
<dtcname>Zero Point Calibration of Acceleration Sensor Undone</dtcname>
<subpara id="RM000000XIK0C0X_01" type-id="60" category="03" proc-id="RM22W0E___0000AGS00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU (master cylinder solenoid) receives signals from the yaw rate sensor assembly via the CAN communication system.</ptxt>
<ptxt>The yaw rate sensor assembly has a built-in acceleration sensor and detects the vehicle's condition using 2 circuits (GL1, GL2). If there is trouble in the bus lines between the yaw rate sensor assembly and the CAN communication system, DTCs U0123 (yaw rate sensor communication trouble) and U0124 (acceleration sensor communication trouble) are stored.</ptxt>
<ptxt>The DTCs are also stored when the calibration has not been completed.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.97in"/>
<colspec colname="COL2" colwidth="2.97in"/>
<colspec colname="COL3" colwidth="3.14in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1210</ptxt>
</entry>
<entry valign="middle">
<ptxt>Zero point calibration of the yaw rate sensor is incomplete.</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Zero point calibration incomplete</ptxt>
</item>
<item>
<ptxt>Sensor installation</ptxt>
</item>
<item>
<ptxt>Yaw rate sensor assembly</ptxt>
</item>
<item>
<ptxt>Skid control ECU (Master cylinder solenoid)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1336</ptxt>
</entry>
<entry valign="middle">
<ptxt>Zero point calibration of the acceleration sensor is incomplete.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XIK0C0X_02" type-id="51" category="05" proc-id="RM22W0E___0000AGT00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the yaw rate sensor assembly, perform zero point calibration (See page <xref label="Seep02" href="RM00000452J00LX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XIK0C0X_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XIK0C0X_03_0012" proc-id="RM22W0E___0000AGY00000">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM0000046KV00MX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>CAN DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIK0C0X_03_0002" fin="false">A</down>
<right ref="RM000000XIK0C0X_03_0013" fin="true">B</right>
<right ref="RM000000XIK0C0X_03_0015" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0002" proc-id="RM22W0E___0000AGV00000">
<testtitle>PERFORM ZERO POINT CALIBRATION OF YAW RATE AND ACCELERATION SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform zero point calibration of the yaw rate and acceleration sensor (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XIK0C0X_03_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0007" proc-id="RM22W0E___0000AGW00000">
<testtitle>PERFORM TEST MODE (SIGNAL CHECK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Perform the signal check in the Test Mode Procedure (See page <xref label="Seep01" href="RM00000452K00NX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>All test mode DTCs are cleared.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XIK0C0X_03_0009" fin="true">OK</down>
<right ref="RM000000XIK0C0X_03_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0001" proc-id="RM22W0E___0000AGU00000">
<testtitle>CHECK YAW RATE SENSOR ASSEMBLY INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Check that the yaw rate sensor assembly has been installed properly (See page <xref label="Seep01" href="RM000000SS2063X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The sensor is tightened to the specified torque.</ptxt>
</specitem>
<specitem>
<ptxt>The sensor is not installed at an angle.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XIK0C0X_03_0008" fin="false">OK</down>
<right ref="RM000000XIK0C0X_03_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0008" proc-id="RM22W0E___0000AGX00000">
<testtitle>READ VALUE USING GTS (YAW RATE AND ACCELERATION SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.16in"/>
<colspec colname="COL2" colwidth="2.16in"/>
<colspec colname="COL3" colwidth="1.80in"/>
<colspec colname="COLSPEC2" colwidth="1.96in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Deceleration Sensor</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Acceleration sensor reading/ Min.: -18.525 m/s<sup>2</sup>, Max.: 18.387 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Changes continuously during acceleration/deceleration.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Deceleration Sensor2</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Acceleration sensor 2 reading/ Min.: -18.525 m/s<sup>2</sup>, Max.: 18.387 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Changes continuously during acceleration/deceleration.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Yaw Rate Sensor</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Yaw rate sensor/ Min.: -128°/s, Max.: 127°/s</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle stationary: 0°/s</ptxt>
<ptxt>Right turn: -128 to 0°/s</ptxt>
<ptxt>Left turn: 0 to 127°/s</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that the yaw rate and acceleration sensor output value is displayed on the GTS.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The yaw rate and acceleration sensor output value is normal.</ptxt>
</specitem>
</spec>
</test1>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000XIK0C0X_03_0010" fin="true">A</down>
<right ref="RM000000XIK0C0X_03_0006" fin="true">B</right>
<right ref="RM000000XIK0C0X_03_0014" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0009">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0006">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0005">
<testtitle>INSTALL YAW RATE SENSOR ASSEMBLY CORRECTLY<xref label="Seep01" href="RM000000SS2063X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0010">
<testtitle>REPLACE YAW RATE SENSOR ASSEMBLY<xref label="Seep01" href="RM000000SS505TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0013">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0014">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIK0C0X_03_0015">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>