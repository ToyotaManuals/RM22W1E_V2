<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002K" variety="S002K">
<name>WIPER / WASHER</name>
<ttl id="12068_S002K_7C3Y9_T00RC" variety="T00RC">
<name>WIPER AND WASHER SYSTEM (w/ Rain Sensor)</name>
<para id="RM000000TZ108KX" category="J" type-id="302UH" name-id="WW5ZN-02" from="201308">
<dtccode/>
<dtcname>Headlight Signal Circuit</dtcname>
<subpara id="RM000000TZ108KX_01" type-id="60" category="03" proc-id="RM22W0E___0000IRH00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The headlight cleaner control relay detects the low beam headlights status.</ptxt>
</content5>
</subpara>
<subpara id="RM000000TZ108KX_02" type-id="32" category="03" proc-id="RM22W0E___0000IRI00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E247721E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TZ108KX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000TZ108KX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TZ108KX_05_0021" proc-id="RM22W0E___0000IRL00001">
<testtitle>CHECK LOW BEAM HEADLIGHTS OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the low beam headlights operate normally (See page <xref label="Seep01" href="RM000002WL303MX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Low beam headlights operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TZ108KX_05_0019" fin="false">OK</down>
<right ref="RM000000TZ108KX_05_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TZ108KX_05_0019" proc-id="RM22W0E___0000IRK00001">
<testtitle>CHECK HARNESS AND CONNECTOR (HEADLIGHT CLEANER CONTROL RELAY - MAIN BODY ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E2 main body ECU (cowl side junction block LH) connector.</ptxt>
<figure>
<graphic graphicname="E247720E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the headlight cleaner control relay connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 (HDLO) - E2-13 (HDLO)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 (HDLO) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TZ108KX_05_0008" fin="false">OK</down>
<right ref="RM000000TZ108KX_05_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TZ108KX_05_0008" proc-id="RM22W0E___0000IRJ00001">
<testtitle>CHECK MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the headlight cleaner control relay connector.</ptxt>
<figure>
<graphic graphicname="E139345E22" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 (HDLO) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Headlight dimmer switch in HEAD</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 (HDLO) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Headlight dimmer switch not in HEAD</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TZ108KX_05_0017" fin="true">OK</down>
<right ref="RM000000TZ108KX_05_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TZ108KX_05_0015">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000TZ108KX_05_0017">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002M5V01HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TZ108KX_05_0020">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000000TZ108KX_05_0022">
<testtitle>GO TO LIGHTING SYSTEM<xref label="Seep01" href="RM000002WKL01IX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>