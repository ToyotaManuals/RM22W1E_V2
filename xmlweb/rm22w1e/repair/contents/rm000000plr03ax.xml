<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LY_T00F1" variety="T00F1">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM000000PLR03AX" category="U" type-id="3001G" name-id="CC41L-02" from="201301">
<name>TERMINALS OF ECU</name>
<subpara id="RM000000PLR03AX_z0" proc-id="RM22W0E___000079U00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK ECM (for 1UR-FE)</ptxt>
<figure>
<graphic graphicname="E158857E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>As the ECM connector is a waterproof connector, it is not possible to check the voltage of each terminal or check the waveform with an oscilloscope while the ECM is installed in the vehicle.</ptxt>
</atten4>
<step2>
<ptxt>Disconnect the A38*1 and C45*1 or A52*2 and C46*2 ECM connectors.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38*1-16 (TC) - Body ground</ptxt>
<ptxt>A52*2-16 (TC) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>V-W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Terminal TC of DLC3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45*1-81 (E1) - Body ground</ptxt>
<ptxt>C46*2-81 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>C45*1-27 (D) - Body ground</ptxt>
<ptxt>C46*2-27 (D) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in D</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever not in D</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38*1-38 (SFTU) - Body ground</ptxt>
<ptxt>A52*2-38 (SFTU) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Y - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in +</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38*1-25 (S) - Body ground</ptxt>
<ptxt>A52*2-25 (S) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever not in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38*1-27 (SFTD) - Body ground</ptxt>
<ptxt>A52*2-27 (SFTD) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in -</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38*1-36 (STP) - Body ground</ptxt>
<ptxt>A52*2-36 (STP) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Stop light signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38*1-35 (ST1-) - Body ground</ptxt>
<ptxt>A52*2-35 (ST1-) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R-W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Stop light signal (opposite to STP terminal)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="4" valign="middle">
<ptxt>A38*1-45 (CCS) - Body ground</ptxt>
<ptxt>A52*2-45 (CCS) - Body ground</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>Cruise control main switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Main switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>CANCEL switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>1510 to 1570 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>-SET switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>620 to 640 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>+RES switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>235 to 245 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Main switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 2.5 Ω</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38*1-37 (CCHG) - Body ground</ptxt>
<ptxt>A52*2-37 (CCHG) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>P-B - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Distance control switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Main switch on</ptxt>
<ptxt>MODE switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Main switch on</ptxt>
<ptxt>MODE switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CHECK ECM (for 3UR-FE)</ptxt>
<figure>
<graphic graphicname="E158857E04" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>As the ECM connector is a waterproof connector, it is not possible to check the voltage of each terminal or check the waveform with an oscilloscope while the ECM is installed in the vehicle.</ptxt>
</atten4>
<step2>
<ptxt>Disconnect the A38 and C45 ECM connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38-16 (TC) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>V-W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Terminal TC of DLC3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-81 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>C45-27 (D) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in D</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever not in D</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38-38 (SFTU) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Y - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in +</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38-25 (S) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever not in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38-27 (SFTD) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Shift lever in -</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38-36 (STP) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Stop light signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38-35 (ST1-) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R-W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Stop light signal (opposite to STP terminal)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="4" valign="middle">
<ptxt>A38-45 (CCS) - Body ground</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>Cruise control main switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Main switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>CANCEL switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>1510 to 1570 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>-SET switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>620 to 640 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>+RES switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>235 to 245 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Main switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 2.5 Ω</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38-37 (CCHG) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>P-B - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Distance control switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Main switch on</ptxt>
<ptxt>MODE switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Main switch on</ptxt>
<ptxt>MODE switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CHECK DISTANCE CONTROL ECU</ptxt>
<figure>
<graphic graphicname="E149022E02" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the E108 distance control ECU connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E108-1 (B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Distance control ECU power source line</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>E108-3 (WASH) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Windshield wiper switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Windshield wiper switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Windshield wiper switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E108-12 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>E108-13 (IGB) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Engine switch on (IG) signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>E108-16 (MODE) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Distance control switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Distance control switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
<ptxt>Distance control switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E108-22 (LRDD) - E108-10 (SGND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Millimeter wave radar sensor input signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
<ptxt>(See waveform 1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E108-23 (LRRD) - E108-10 (SGND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Millimeter wave radar sensor output signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
<ptxt>(See waveform 2)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 1.</ptxt>
<figure>
<graphic graphicname="E109730E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>E108-22 (LRDD) - E108-10 (SGND)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>2 V/DIV., 10 msec./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 2.</ptxt>
<figure>
<graphic graphicname="E109731E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>E108-23 (LRRD) - E108-10 (SGND)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>2 V/DIV., 5 msec./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>CHECK MILLIMETER WAVE RADAR SENSOR</ptxt>
<figure>
<graphic graphicname="E158915E02" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the A56 millimeter wave radar sensor connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A56-3 (LRDD) - A56-2 (SGND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Millimeter wave radar sensor output signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
<ptxt>(See waveform 3)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A56-4 (LRRD) - A56-2 (SGND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Millimeter wave radar sensor input signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
<ptxt>(See waveform 4)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A56-5 (IGB) - A56-2 (SGND)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R - R</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Engine switch on (IG) signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 3.</ptxt>
<figure>
<graphic graphicname="E109730E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A56-3 (LRDD) - A56-2 (SGND)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>2 V/DIV., 10 msec./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 4.</ptxt>
<figure>
<graphic graphicname="E109731E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A56-4 (LRRD) - A56-2 (SGND)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>2 V/DIV., 5 msec./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>