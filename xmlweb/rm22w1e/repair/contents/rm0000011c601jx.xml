<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12034_S001P" variety="S001P">
<name>PARKING BRAKE</name>
<ttl id="12034_S001P_7C3PW_T00IZ" variety="T00IZ">
<name>PARKING BRAKE CABLE</name>
<para id="RM0000011C601JX" category="A" type-id="30014" name-id="PB2JS-02" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM0000011C601JX_02" type-id="11" category="10" proc-id="RM22W0E___0000AZ800000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the LH side and RH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000011C601JX_01" type-id="01" category="01">
<s-1 id="RM0000011C601JX_01_0069" proc-id="RM22W0E___0000AZ300000">
<ptxt>INSTALL NO. 2 PARKING BRAKE CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 parking brake cable clamp.</ptxt>
</s2>
<s2>
<ptxt>Install the No. 2 parking brake cable with the 6 bolts.</ptxt>
<figure>
<graphic graphicname="C170911E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the fuel tank (See page <xref label="Seep01" href="RM0000039W801DX"/>).</ptxt>
</s2>
<s2>
<ptxt>Attach the claws of the No. 2 parking brake cable.</ptxt>
<figure>
<graphic graphicname="C172839" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the No. 2 parking brake cable to the No. 1 parking brake pull rod.</ptxt>
<figure>
<graphic graphicname="C226757" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011C601JX_01_0050" proc-id="RM22W0E___0000AZ000000">
<ptxt>INSTALL NO. 3 PARKING BRAKE CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 3 parking brake cable clamp.</ptxt>
</s2>
<s2>
<ptxt>Install the No. 3 parking brake cable with the 6 bolts.</ptxt>
<figure>
<graphic graphicname="C170910E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the claws of the No. 3 parking brake cable.</ptxt>
<figure>
<graphic graphicname="C170907" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the No. 3 parking brake cable to the No. 1 parking brake pull rod.</ptxt>
<figure>
<graphic graphicname="C226757" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011C601JX_01_0095" proc-id="RM22W0E___00009LS00000">
<ptxt>CONNECT NO. 3 PARKING BRAKE CABLE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 3 parking brake cable with the bolt.</ptxt>
<figure>
<graphic graphicname="C170909" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000011C601JX_01_0070" proc-id="RM22W0E___00009LC00000">
<ptxt>INSTALL PARKING BRAKE SHOE LEVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply high temperature grease to the parking brake anchor block.</ptxt>
<figure>
<graphic graphicname="C177128E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Install the parking brake shoe lever to the No. 3 parking brake cable.</ptxt>
<atten3>
<ptxt>Be carefully to distinguish between the parking brake shoe lever RH and LH.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000011C601JX_01_0071" proc-id="RM22W0E___00009LD00000">
<ptxt>INSTALL NO. 2 PARKING BRAKE SHOE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply high temperature grease to the areas of the backing plate that contact the shoe.</ptxt>
</s2>
<s2>
<ptxt>Using SST, install the No. 2 parking brake shoe with the shoe hold down spring pin, compression spring and shoe hold down spring cup.</ptxt>
<sst>
<sstitem>
<s-number>09718-00011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172175E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000011C601JX_01_0072" proc-id="RM22W0E___00009LE00000">
<ptxt>INSTALL NO. 1 PARKING BRAKE SHOE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply high temperature grease to the areas of the backing plate that contact the shoe.</ptxt>
</s2>
<s2>
<ptxt>Apply high temperature grease to the thread and all joining areas of the parking brake shoe adjuster screw set.</ptxt>
<figure>
<graphic graphicname="C172178E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Set the No. 1 parking brake shoe and shoe adjuster screw set in place.</ptxt>
<figure>
<graphic graphicname="C172179" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the tension spring.</ptxt>
<figure>
<graphic graphicname="C172180" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, install the shoe hold down spring pin, compression spring and shoe hold down spring cup.</ptxt>
<sst>
<sstitem>
<s-number>09718-00011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172174E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000011C601JX_01_0066" proc-id="RM22W0E___00009LF00000">
<ptxt>INSTALL PARKING BRAKE SHOE RETURN SPRING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, install the shoe return spring.</ptxt>
<sst>
<sstitem>
<s-number>09703-30011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172171E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000011C601JX_01_0067" proc-id="RM22W0E___00009LG00000">
<ptxt>CHECK PARKING BRAKE INSTALLATION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that each part is installed properly.</ptxt>
<figure>
<graphic graphicname="C172182E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000011C601JX_01_0094" proc-id="RM22W0E___00009LH00000">
<ptxt>INSTALL REAR DISC
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the matchmarks, and then install the rear disc.</ptxt>
<figure>
<graphic graphicname="C172168E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When replacing the rear disc with a new one, select the installation position where the rear disc has the minimum runout.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000011C601JX_01_0059" proc-id="RM22W0E___0000AZ100000">
<ptxt>CONNECT REAR DISC BRAKE CYLINDER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the rear disc brake cylinder with 2 new bolts.</ptxt>
<figure>
<graphic graphicname="C172183" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>95</t-value1>
<t-value2>969</t-value2>
<t-value4>70</t-value4>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not twist the brake hose.</ptxt>
</item>
<item>
<ptxt>Make sure that the bolts are free from damage and foreign matter.</ptxt>
</item>
<item>
<ptxt>Do not overtighten the bolts.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011C601JX_01_0113" proc-id="RM22W0E___00009Y900000">
<ptxt>INSTALL REAR NO. 2 SUSPENSION CONTROL ACCUMULATOR BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the bracket to the shock absorber control valve with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000011C601JX_01_0099" proc-id="RM22W0E___00009SS00000">
<ptxt>INSTALL HEIGHT CONTROL UNIT PROTECTOR PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the protector pipe with the 6 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>163</t-value2>
<t-value4>12</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="C179788" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000011C601JX_01_0074" proc-id="RM22W0E___0000AZ400000">
<ptxt>ADJUST PARKING BRAKE</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000011BK01GX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000011C601JX_01_0063" proc-id="RM22W0E___0000AZ200000">
<ptxt>INSTALL REAR WHEEL</ptxt>
<content1 releasenbr="1">
<torque>
<subtitle>for Aluminum Wheel</subtitle>
<torqueitem>
<t-value1>131</t-value1>
<t-value2>1336</t-value2>
<t-value4>97</t-value4>
</torqueitem>
<subtitle>for Steel Wheel</subtitle>
<torqueitem>
<t-value1>209</t-value1>
<t-value2>2131</t-value2>
<t-value4>154</t-value4>
</torqueitem>
</torque>
</content1>
</s-1>
<s-1 id="RM0000011C601JX_01_0114" proc-id="RM22W0E___0000AZ700000">
<ptxt>INSTALL LOWER CONSOLE BOX (w/o Console Box Lid)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000004XLA001X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000011C601JX_01_0083" proc-id="RM22W0E___0000AZ600000">
<ptxt>INSTALL COOLING BOX ASSEMBLY (w/ Cool Box)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000003B6Q00JX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000011C601JX_01_0082" proc-id="RM22W0E___0000AZ500000">
<ptxt>INSTALL REAR CONSOLE BOX SUB-ASSEMBLY (w/o Cool Box)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000039QD00GX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>