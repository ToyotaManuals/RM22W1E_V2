<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R4_T00K7" variety="T00K7">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM000002U3Q0EIX" category="J" type-id="804HC" name-id="AV96Y-06" from="201308">
<dtccode/>
<dtcname>Mute Signal Circuit between Radio Receiver and Multi-media Interface ECU</dtcname>
<subpara id="RM000002U3Q0EIX_01" type-id="60" category="03" proc-id="RM22W0E___0000CDM00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The multi-media interface ECU sends a mute signal to the radio receiver assembly.</ptxt>
<ptxt>The radio receiver assembly controls the volume according to the mute signal from the multi-media interface ECU.</ptxt>
<ptxt>The mute signal is sent to reduce noise and popping sounds generated when switching modes.</ptxt>
<ptxt>If there is an open in the circuit, noise can be heard from the speakers when changing the sound source.</ptxt>
<ptxt>If there is a short in the circuit, even though the radio receiver assembly is functioning, no sound, or only an extremely faint sound, can be heard.</ptxt>
</content5>
</subpara>
<subpara id="RM000002U3Q0EIX_02" type-id="32" category="03" proc-id="RM22W0E___0000CDN00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E175068E20" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002U3Q0EIX_04" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002U3Q0EIX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002U3Q0EIX_03_0001" proc-id="RM22W0E___0000CDO00001">
<testtitle>INSPECT RADIO RECEIVER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E152226E29" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F55-6 (MUTE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (ACC), USB audio system is playing → Mode is changing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Above 2.5 V → Below 0.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Component with harness connected</ptxt>
<ptxt>(Radio Receiver Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002U3Q0EIX_03_0004" fin="true">OK</down>
<right ref="RM000002U3Q0EIX_03_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002U3Q0EIX_03_0002" proc-id="RM22W0E___0000CDP00001">
<testtitle>CHECK HARNESS AND CONNECTOR (RADIO RECEIVER ASSEMBLY - MULTI-MEDIA INTERFACE ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F55 radio receiver assembly and L40 multi-media interface ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F55-6 (MUTE) - L40-6 (MUTI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F55-6 (MUTE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002U3Q0EIX_03_0003" fin="false">OK</down>
<right ref="RM000002U3Q0EIX_03_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002U3Q0EIX_03_0003" proc-id="RM22W0E___0000CDQ00001">
<testtitle>INSPECT MULTI-MEDIA INTERFACE ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the L40 multi-media interface ECU connector.</ptxt>
<figure>
<graphic graphicname="E152226E30" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L40-6 (MUTI) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (ACC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Above 2.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Component with harness connected</ptxt>
<ptxt>(Multi-media Interface ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002U3Q0EIX_03_0007" fin="true">OK</down>
<right ref="RM000002U3Q0EIX_03_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002U3Q0EIX_03_0004">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000012A80GEX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U3Q0EIX_03_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002U3Q0EIX_03_0006">
<testtitle>REPLACE MULTI-MEDIA INTERFACE ECU<xref label="Seep01" href="RM000003AWI017X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U3Q0EIX_03_0007">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AI400DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>