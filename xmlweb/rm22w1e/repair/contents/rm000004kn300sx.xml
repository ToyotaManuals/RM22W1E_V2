<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000004KN300SX" category="C" type-id="805CO" name-id="ESX47-04" from="201301" to="201308">
<dtccode>P244C</dtccode>
<dtcname>Exhaust Temperature Too Low for Particulate Filter Regeneration Bank 1</dtcname>
<dtccode>P244E</dtccode>
<dtcname>Exhaust Temperature Too Low for Particulate Filter Regeneration Bank 2</dtcname>
<dtccode>P2458</dtccode>
<dtcname>Diesel Particulate Filter Regeneration Duration</dtcname>
<dtccode>P2463</dtccode>
<dtcname>Diesel Particulate Filter Restriction - Soot Accumulation</dtcname>
<subpara id="RM000004KN300SX_01" type-id="60" category="03" proc-id="RM22W0E___00003ZY00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The DPF (Diesel Particulate Filter) is an emission control system which processes the particulate matter (PM) that is emitted from a diesel engine. The DPF catalyst converter has a porous ceramic structure which has a PM collecting function. The PM that is collected is continuously treated by oxidation due to catalytic action at high temperatures. However, however PM accumulation increases at low catalyst temperatures because the oxidation process slows down. The ECM performs PM forced regeneration so that the amount of accumulated PM does not exceed the threshold.</ptxt>
<figure>
<graphic graphicname="A254515E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>P244C (Bank 1), P244E (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>PM forced regeneration</ptxt>
</entry>
<entry valign="middle">
<ptxt>During PM forced regeneration, catalyst temperature does not increase sufficiently for 3 minutes.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Monolithic converter assembly RH (for bank 1 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Monolithic converter assembly LH (for bank 2 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>No. 2 exhaust gas temperature sensor (B1S2)</ptxt>
</item>
<item>
<ptxt>No. 3 exhaust gas temperature sensor (B2S2)</ptxt>
</item>
<item>
<ptxt>No. 4 exhaust gas temperature sensor (B1S3, B2S3)</ptxt>
</item>
</list1>
<ptxt>Common Inspection Item:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Fuel injection system</ptxt>
</item>
<item>
<ptxt>Intake/exhaust system</ptxt>
</item>
<item>
<ptxt>After treatment control system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2458</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>PM forced regeneration</ptxt>
</entry>
<entry valign="middle">
<ptxt>When PM forced regeneration is manually performed, PM forced regeneration does not finish even after 60 minutes or more elapse.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Monolithic converter assembly RH (for bank 1 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Monolithic converter assembly LH (for bank 2 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>No. 2 exhaust gas temperature sensor (B1S2)</ptxt>
</item>
<item>
<ptxt>No. 3 exhaust gas temperature sensor (B2S2)</ptxt>
</item>
<item>
<ptxt>No. 4 exhaust gas temperature sensor (B1S3, B2S3)</ptxt>
</item>
</list1>
<ptxt>Common Inspection Item:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Fuel injection system</ptxt>
</item>
<item>
<ptxt>Intake/exhaust system</ptxt>
</item>
<item>
<ptxt>After treatment control system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2463</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>PM forced regeneration</ptxt>
</entry>
<entry valign="middle">
<ptxt>While driving, PM accumulation amount exceeds the limit.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Monolithic converter assembly RH (for bank 1 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Monolithic converter assembly LH (for bank 2 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>No. 2 exhaust gas temperature sensor (B1S2)</ptxt>
</item>
<item>
<ptxt>No. 3 exhaust gas temperature sensor (B2S2)</ptxt>
</item>
<item>
<ptxt>No. 4 exhaust gas temperature sensor (B1S3, B2S3)</ptxt>
</item>
<item>
<ptxt>Differential pressure sensor assembly</ptxt>
</item>
</list1>
<ptxt>Common Inspection Item:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Fuel injection system</ptxt>
</item>
<item>
<ptxt>Intake/exhaust system</ptxt>
</item>
<item>
<ptxt>After treatment control system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>PM forced regeneration may be prohibited due to a fail-safe operation that is performed when another DTC is stored (including engine output limitation).</ptxt>
</atten4>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P244C</ptxt>
<ptxt>P244E</ptxt>
<ptxt>P2458</ptxt>
<ptxt>P2463</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Exhaust Fuel Addition FB</ptxt>
</item>
<item>
<ptxt>Exhaust Fuel Addition FB #2</ptxt>
</item>
<item>
<ptxt>Catalyst Differential Press</ptxt>
</item>
<item>
<ptxt>Catalyst Differential Press #2</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000004KN300SX_02" type-id="64" category="03" proc-id="RM22W0E___00003ZZ00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM monitors the differential pressure between the upstream and downstream of the DPF catalyst converter and also monitors exhaust gas temperature to detect a malfunction in the DPF catalyst converter.</ptxt>
<topic>
<title>Excessive PM accumulation:</title>
<ptxt>The ECM monitors the PM Accumulation Ratio and when the accumulated PM volume in the DPF catalyst converter exceeds the limit, the ECM illuminates the MIL.</ptxt>
</topic>
<topic>
<title>Abnormal catalyst temperature increase:</title>
<ptxt>The ECM monitors exhaust gas temperature increase while the exhaust fuel addition injector assemblies adds fuel during PM forced regeneration. Exhaust gas temperature increases up to approximately 300 to 500°C (572 to 932°F) during PM forced regeneration. If the temperature does not increase even after a certain amount of time has elapsed, the ECM determines that the DPF catalyst converter is abnormal and illuminates the MIL.</ptxt>
</topic>
<topic>
<title>Diesel particulate filter regeneration duration:</title>
<ptxt>When PM forced regeneration is manually performed and PM forced generation does not finish even after 60 minutes or more elapse, the ECM determines that there is problem with the DPF catalyst converter and illuminates the MIL.</ptxt>
<figure>
<graphic graphicname="A208470E06" width="7.106578999in" height="2.775699831in"/>
</figure>
</topic>
</content5>
</subpara>
<subpara id="RM000004KN300SX_03" type-id="51" category="05" proc-id="RM22W0E___000040000000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTC P244B, P244C, P244E, P2458, P2463 or P2465 is stored, the fail-safe controls the engine power output and PM forced regeneration is prohibited. In this case, PM forced regeneration can be performed after disconnecting the EFI MAIN fuse or battery terminal and waiting for 60 seconds or more before connecting it again.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000004KN300SX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004KN300SX_04_0001" proc-id="RM22W0E___000040100000">
<testtitle>READ OUTPUT DTC (RECORD STORED DTC AND FREEZE FRAME DATA)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Record the stored DTC and freeze frame data.</ptxt>
</test1>
<test1>
<ptxt>Check "Exhaust Fuel Addition FB", "Exhaust Fuel Addition FB #2", "Catalyst Differential Press" and "Catalyst Differential Press #2" in the freeze frame data.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the "Exhaust Fuel Addition FB" and "Exhaust Fuel Addition FB #2" values are 1.45 or more, it indicates a high possibility of insufficient temperature increase during PM regeneration control.</ptxt>
</item>
<item>
<ptxt>When the "Catalyst Differential Press" and "Catalyst Differential Press #2" values are 0.45 or more, it indicates a high possibility that DPF catalyst converter is clogged.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0002" proc-id="RM22W0E___000040200000">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P244C, P244E, P2458 AND P2463)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P244C, P244E, P2458 or P2463 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P244B or P2465 is output at the same time as P2463*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Only P2463 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Other relevant DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*: When P244B or P2465 is output at the same time as P2463, perform troubleshooting for P244B or P2465.</ptxt>
</item>
<item>
<ptxt>When DTC P1603, P1604 or P1608 is output in addition to P244C or P244E, perform troubleshooting for P1603, P1604 or P1608 first.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0003" fin="false">A</down>
<right ref="RM000004KN300SX_04_0013" fin="true">B</right>
<right ref="RM000004KN300SX_04_0007" fin="false">C</right>
<right ref="RM000004KN300SX_04_0004" fin="false">D</right>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0003" proc-id="RM22W0E___000040300000">
<testtitle>CHECK FOR BLACK SMOKE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Start the engine and drive the vehicle until the engine coolant temperature reaches 60°C (140°F) or higher.</ptxt>
</test1>
<test1>
<ptxt>Stop the vehicle and allow the engine to idle.</ptxt>
</test1>
<test1>
<ptxt>Fully depress the accelerator pedal for 5 seconds, and then release it [A].</ptxt>
</test1>
<test1>
<ptxt>Repeat the above procedure [A] 10 times [B].</ptxt>
</test1>
<test1>
<ptxt>Check for black smoke emission during procedures [A] and [B].</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Black smoke is emitted less than 5 times.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Even if the black smoke is very thin, count the number of black smoke emissions if there is any visible smoke.</ptxt>
</atten4>
</test1>
</content6>
<res>
<right ref="RM000004KN300SX_04_0005" fin="false">OK</right>
<right ref="RM000004KN300SX_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0004" proc-id="RM22W0E___000040400000">
<testtitle>GO TO DTC CHART</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Diagnose relevant DTCs (See page <xref label="Seep01" href="RM0000031HW05CX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0005" proc-id="RM22W0E___000040500000">
<testtitle>CHECK FOR WHITE SMOKE (DURING PM FORCED REGENERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up until the engine coolant temperature reaches 60°C (140°F) or higher.</ptxt>
</test1>
<test1>
<ptxt>Perform PM forced regeneration (See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>).</ptxt>
</test1>
<test1>
<ptxt>Check if excessive white smoke occurs when "Exhaust Temperature B1S3" and "Exhaust Temperature B2S3" are 300°C (572°F) or more.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Excessive smoke occurs during PM forced regeneration (Exhaust Temperature B1S3 and Exhaust Temperature B2S3 are 300°C (572°F))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When excessive white smoke occurs only during PM forced regeneration, the problem may be due to uncombusted fuel passing through due to the deterioration of the DPF catalyst converter.</ptxt>
</item>
<item>
<ptxt>When combustion control has a malfunction, deterioration or abnormality of driving performance can be felt despite PM forced regeneration being performed.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0006" fin="false">A</down>
<right ref="RM000004KN300SX_04_0007" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0006" proc-id="RM22W0E___000040600000">
<testtitle>REPLACE EXHAUST PIPE ASSEMBLIES (DPF CATALYTIC CONVERTERS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Even though it has been determined that the exhaust pipe assemblies (DPF catalyst converters) need to be replaced, make sure to perform all diagnostic procedures for P244C, P244E, P2458 and P2463 before replacing them.</ptxt>
<atten4>
<ptxt>These DTCs are stored because an excessive amount of PM has accumulated and resulted in abnormal combustion. It is necessary to diagnose the cause of abnormal PM accumulation and repair the problem. Therefore, do not perform replacement at this time.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0007" proc-id="RM22W0E___000040700000">
<testtitle>CHECK FUEL INJECTION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the fuel injection system (See page <xref label="Seep01" href="RM000004KNK012X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0008" proc-id="RM22W0E___000040800000">
<testtitle>CHECK INTAKE / EXHAUST SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the Intake/exhaust system (See page <xref label="Seep01" href="RM000004KNL00TX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0009" proc-id="RM22W0E___000040900000">
<testtitle>CHECK AFTER TREATMENT CONTROL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the after treatment control system (See page <xref label="Seep01" href="RM000004KNM00LX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0010" proc-id="RM22W0E___000040A00000">
<testtitle>REPAIR OR REPLACE MALFUNCTIONING PARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Determine the parts to replace according to the output DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Items to be Replaced</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P244C is output</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Monolithic converter assembly RH (for bank 1 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P244E is output</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Monolithic converter assembly LH (for bank 2 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2458 is output</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Monolithic converter assembly RH (for bank 1 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Monolithic converter assembly LH (for bank 2 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2463* is output</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Monolithic converter assembly RH (for bank 1 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Monolithic converter assembly LH (for bank 2 CCo catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Replace the following items only when it has been determined necessary based on diagnosis for P244C, P244E, P2458 and P2463.</ptxt>
</item>
<item>
<ptxt>Replace the DPF catalytic converter for bank 1 and bank 2 at the same time.</ptxt>
</item>
<item>
<ptxt>*: During the after treatment control system inspection performed when DTC P2463 is output, if Catalyst Differential Press and Catalyst Differential Press #2 decrease to 0.45 or less after forced regeneration is performed and there are no melted or clogged areas found when visually inspecting the CCo catalytic converter and DPF catalytic converter, then do not perform catalytic converter replacement.</ptxt>
</item>
</list1>
</atten4>
<test1>
<ptxt>Replace the front exhaust pipe assembly (for bank 1 DPF catalytic converter) (See page <xref label="Seep01" href="RM000003B0N00HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter) (See page <xref label="Seep02" href="RM000003B0N00HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the Monolithic converter assembly RH (for bank 1 CCo catalytic converter) (See page <xref label="Seep03" href="RM000003B0N00HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the Monolithic converter assembly LH (for bank 2 CCo catalytic converter) (See page <xref label="Seep04" href="RM000003B0N00HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Repair or replace the malfunctioning part confirmed in the diagnosis of the fuel injection system, Intake/exhaust system and after treatment control system.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0014" proc-id="RM22W0E___000040C00000">
<testtitle>CATALYST RECORD CLEAR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform the catalyst record clear (See page <xref label="Seep01" href="RM000000TIN06OX"/>).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the DPF catalytic converter or CCo catalytic converter is replaced, perform "catalyst record clear".</ptxt>
</item>
<item>
<ptxt>If the DPF catalytic converter and CCo catalytic converter were not replaced, proceed to the next step.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0011" proc-id="RM22W0E___000040B00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up until the engine coolant temperature reaches 60°C (140°F) or higher.</ptxt>
</test1>
<test1>
<ptxt>Perform PM forced regeneration (See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KN300SX_04_0012" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KN300SX_04_0012">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000004KN300SX_04_0013">
<testtitle>GO TO DTC P244B<xref label="Seep01" href="RM000004ID500VX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>