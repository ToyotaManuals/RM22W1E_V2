<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM0000045Z800PX" category="J" type-id="804RC" name-id="BCE77-02" from="201301" to="201308">
<dtccode/>
<dtcname>Multi-terrain Select Indicator Light does not Come ON</dtcname>
<subpara id="RM0000045Z800PX_01" type-id="60" category="03" proc-id="RM22W0E___0000AL600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the transfer gear position is L4, the multi-terrain select indicator light illuminates and control begins.</ptxt>
<ptxt>Under any of the following conditions, the multi-terrain select system does not begin control and the multi-terrain select indicator light does not illuminate.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The vehicle is in the VSC/TRC OFF state.</ptxt>
</item>
<item>
<ptxt>Crawl Control is operating.</ptxt>
</item>
<item>
<ptxt>There is a malfunction in the VSC system.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM0000045Z800PX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000045Z800PX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000045Z800PX_04_0014" proc-id="RM22W0E___0000ABS00000">
<testtitle>CHECK CAN COMMUNICATION LINE
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Select "CAN Bus Check" from the System Selection Menu screen, and follow the prompts on the screen to inspect the CAN Bus.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>"CAN Bus Check" indicates no malfunctions in CAN communication.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM0000045Z800PX_04_0015" fin="false">A</down>
<right ref="RM0000045Z800PX_04_0005" fin="true">B</right>
<right ref="RM0000045Z800PX_04_0013" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000045Z800PX_04_0015" proc-id="RM22W0E___0000ABT00000">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (for LHD: See page <xref label="Seep01" href="RM000001RSW03JX"/>, for RHD: See page <xref label="Seep02" href="RM000001RSW03KX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>CAN system DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN system DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN system DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM0000045Z800PX_04_0009" fin="true">A</down>
<right ref="RM0000045Z800PX_04_0005" fin="true">B</right>
<right ref="RM0000045Z800PX_04_0013" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000045Z800PX_04_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000045Z800PX_04_0009">
<testtitle>GO TO METER / GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L03NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000045Z800PX_04_0013">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>