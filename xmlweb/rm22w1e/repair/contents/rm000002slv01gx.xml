<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002SLV01GX" category="C" type-id="802M5" name-id="ACBLA-03" from="201308">
<dtccode>B1468/68</dtccode>
<dtcname>Rear Passenger Side Room Temperature Sensor Circuit</dtcname>
<subpara id="RM000002SLV01GX_01" type-id="60" category="03" proc-id="RM22W0E___0000H7A00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The rear room temperature sensor RH*1 or LH*2 (for front passenger side) is installed in the rear quarter trim board RH*1 or LH*2 to detect the rear room temperature and control the heater and air conditioner "auto" mode. The resistance of the rear room temperature sensor RH*1 or LH*2 changes in accordance with the rear room temperature for RH*1 or LH*2 seat. As the temperature decreases, the resistance increases. As the temperature increases, the resistance decreases.</ptxt>
<ptxt>The air conditioning amplifier applies a voltage (5 V) to the rear room temperature sensor RH*1 or LH*2 and reads voltage changes as changes in the resistance of the rear room temperature sensor RH*1 or LH*2.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1468/68</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>An open or short in the rear room temperature sensor RH*1 or LH*2 circuit.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Rear room temperature sensor RH*1</ptxt>
</item>
<item>
<ptxt>Rear room temperature sensor LH*2</ptxt>
</item>
<item>
<ptxt>Harness or connector between rear room temperature sensor RH and air conditioning amplifier assembly*1</ptxt>
</item>
<item>
<ptxt>Harness or connector between rear room temperature sensor LH and air conditioning amplifier assembly*2</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002SLV01GX_02" type-id="32" category="03" proc-id="RM22W0E___0000H7B00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E156584E42" width="7.106578999in" height="2.775699831in"/>
</figure>
<figure>
<graphic graphicname="E156584E43" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002SLV01GX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002SLV01GX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002SLV01GX_06_0001" proc-id="RM22W0E___0000H7C00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (AIR CONDITIONER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the rear room temperature sensor RH*1 or LH*2 is functioning properly. </ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Room Temperature Sensor (RP)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Rear room temperature sensor for RH*1 or LH*2 /</ptxt>
<ptxt>Min.: -6.5°C (20.3°F)</ptxt>
<ptxt>Max.: 57.25°C (135.05°F)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Actual rear room temperature for RH*1 or LH*2 displayed</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Open in the circuit: -6.5°C (20.3°F).</ptxt>
<ptxt>Short in the circuit: 57.25°C (135.05°F).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002SLV01GX_06_0004" fin="true">OK</down>
<right ref="RM000002SLV01GX_06_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002SLV01GX_06_0002" proc-id="RM22W0E___0000H7D00001">
<testtitle>INSPECT REAR ROOM TEMPERATURE SENSOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E160595E03" width="2.775699831in" height="5.787629434in"/>
</figure>
<test1>
<ptxt>for LHD</ptxt>
<ptxt>Remove the rear room temperature sensor RH (See page <xref label="Seep01" href="RM000001A3T01KX"/>).</ptxt>
</test1>
<test1>
<ptxt>for RHD</ptxt>
<ptxt>Remove the rear room temperature sensor LH (See page <xref label="Seep02" href="RM000001A3T01KX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="10" valign="middle">
<ptxt>1 (+) - 2 (-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10°C (50°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.00 to 3.73 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>15°C (59°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.45 to 2.88 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.95 to 2.30 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>25°C (77°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.60 to 1.80 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>30°C (86°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.28 to 1.47 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>35°C (95°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.00 to 1.22 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>40°C (104°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.80 to 1.00 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>45°C (113°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.65 to 0.85 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>50°C (122°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.50 to 0.70 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>55°C (131°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.44 to 0.60 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>60°C (140°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.36 to 0.50 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Even slightly touching the sensor may change the resistance value. Be sure to hold the connector of the sensor.</ptxt>
</atten3>
<atten4>
<ptxt>As the temperature increases, the resistance decreases (see the graph).</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002SLV01GX_06_0003" fin="false">A</down>
<right ref="RM000002SLV01GX_06_0007" fin="true">B</right>
<right ref="RM000002SLV01GX_06_0010" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002SLV01GX_06_0003" proc-id="RM22W0E___0000H7E00001">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR ROOM TEMPERATURE SENSOR - AIR CONDITIONING AMPLIFIER)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E160312E11" width="2.775699831in" height="5.787629434in"/>
</figure>
<test1>
<ptxt>for LHD</ptxt>
<test2>
<ptxt>Disconnect the z56 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E35 amplifier connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z56-2 (-)- E35-2 (SG-6)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z56-1 (+) - E35-19 (TR7)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-2 (SG-6) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-19 (TR7) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<figure>
<graphic graphicname="E160312E12" width="2.775699831in" height="5.787629434in"/>
</figure>
<test1>
<ptxt>for RHD</ptxt>
<test2>
<ptxt>Disconnect the z55 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E35 amplifier connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z55-2 (-) - E35-2 (SG-6)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z55-1 (+) - E35-18 (TR)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-2 (SG-6) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-18 (TR) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002SLV01GX_06_0004" fin="true">OK</down>
<right ref="RM000002SLV01GX_06_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002SLV01GX_06_0004">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002SLV01GX_06_0007">
<testtitle>REPLACE REAR ROOM TEMPERATURE SENSOR RH<xref label="Seep01" href="RM000001A3T01KX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002SLV01GX_06_0010">
<testtitle>REPLACE REAR ROOM TEMPERATURE SENSOR LH<xref label="Seep01" href="RM000001A3T01KX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002SLV01GX_06_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>