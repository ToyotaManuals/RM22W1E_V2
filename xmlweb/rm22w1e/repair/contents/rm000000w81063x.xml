<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MR_T00FU" variety="T00FU">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 3UR-FE)</name>
<para id="RM000000W81063X" category="C" type-id="302FQ" name-id="AT6HL-05" from="201301" to="201308">
<dtccode>P0761</dtccode>
<dtcname>Shift Solenoid "C" Performance (Shift Solenoid Valve S3)</dtcname>
<subpara id="RM000000W81063X_01" type-id="60" category="03" proc-id="RM22W0E___00008BZ00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM uses signals from the output shaft speed sensor and input speed sensor to detect the actual gear position (1st, 2nd, 3rd, 4th, 5th or 6th gear).</ptxt>
<ptxt>Then the ECM compares the actual gear with the shift schedule in the ECM memory to detect mechanical problems of the shift solenoid valves, valve body or automatic transmission (clutch, brake, gear, etc.).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.19in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0761</ptxt>
</entry>
<entry valign="middle">
<list1 type="nonmark">
<item>
<ptxt>S3 stuck ON malfunction*1:</ptxt>
</item>
<item>
<ptxt>When the ECM directs the gearshift to switch to 5th or 6th gear, the engine overruns (clutch slips).</ptxt>
</item>
<item>
<ptxt>The ECM determines there is a malfunction when either of the following conditions is met (1 trip detection logic).</ptxt>
</item>
</list1>
<ptxt>(a) When the ECM directs the transmission to switch to 4th gear, the actual gear is shifted to 3rd.</ptxt>
<ptxt>(b) When the ECM directs the transmission to switch to 5th gear, the engine overruns (clutch slips).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift solenoid valve S3 remains closed</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve SLT remains closed</ptxt>
</item>
<item>
<ptxt>Valve body is blocked</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake, gear, etc.)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0761</ptxt>
</entry>
<entry valign="middle">
<list1 type="nonmark">
<item>
<ptxt>S3 stuck OFF malfunction*2:</ptxt>
</item>
<item>
<ptxt>Shifting to 1st, 2nd, and 3rd gears is impossible.</ptxt>
</item>
<item>
<ptxt>The ECM determines there is a malfunction when the following conditions are both met (1 trip detection logic).</ptxt>
</item>
</list1>
<ptxt>(a) When the ECM directs the transmission to switch to 2nd gear, the actual gear is shifted to 4th.</ptxt>
<ptxt>(b) When the ECM directs the transmission to switch to 6th gear, the actual gear is shifted to 6th.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift solenoid valve S3 remains open</ptxt>
</item>
<item>
<ptxt>Valve body is blocked</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake, gear, etc.)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Gear positions in the event of a solenoid valve mechanical problem:</ptxt>
<table pgwide="1">
<tgroup cols="7">
<colspec colname="COL1" align="left" colwidth="1.13in"/>
<colspec colname="COL2" colwidth="0.99in"/>
<colspec colname="COL3" colwidth="0.99in"/>
<colspec colname="COL4" colwidth="0.99in"/>
<colspec colname="COL5" colwidth="0.99in"/>
<colspec colname="COL6" colwidth="0.99in"/>
<colspec colname="COL7" colwidth="1.00in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>ECM command gearshift</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6th</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*1: Actual gear position under S3 stuck ON malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>N*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>N*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2: Actual gear position under S3 stuck OFF malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6th</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>N*: Neutral</ptxt>
</item>
<item>
<ptxt>Gear position during fail-safe operation:</ptxt>
<ptxt>If any malfunction is detected, the ECM changes into the fail-safe mode to shift into the gear positions as shown in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="7">
<colspec colname="COL1" align="left" colwidth="1.01in"/>
<colspec colname="COL2" colwidth="1.01in"/>
<colspec colname="COL3" colwidth="1.01in"/>
<colspec colname="COL4" colwidth="1.01in"/>
<colspec colname="COL5" colwidth="1.01in"/>
<colspec colname="COL6" colwidth="1.01in"/>
<colspec colname="COL7" colwidth="1.02in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Gear position under normal conditions</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6th</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*1: Actual gear position under fail-safe mode when S3 stuck ON malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2: Actual gear position under fail-safe mode when S3 stuck OFF malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6th</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000W81063X_02" type-id="64" category="03" proc-id="RM22W0E___00008C000000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates stuck ON malfunction or stuck OFF malfunction of shift solenoid valve S3.</ptxt>
<ptxt>The ECM commands gear shifts by turning the shift solenoid valves ON/OFF. When the gear position commanded by the ECM and the actual gear position are not same, the ECM illuminates the MIL and stores the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W81063X_06" type-id="51" category="05" proc-id="RM22W0E___00008C100000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, perform the Active Test.</ptxt>
<atten4>
<ptxt>While driving, the shift position can be forcibly changed with the intelligent tester.</ptxt>
<ptxt>Comparing the shift position commanded by the Active Test with the actual shift position enables you to confirm the problem (See page <xref label="Seep01" href="RM000000O8L0P6X"/>).</ptxt>
</atten4>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Control the Shift Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valves and set each shift position by yourself</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Press "→" button: Shift up</ptxt>
</item>
<item>
<ptxt>Press "←" button: Shift down</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Possible to check operation of the shift solenoid valves.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<ptxt>50 km/h (30 mph) or less</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>This test can be conducted when the vehicle speed is 50 km/h (30 mph) or less.</ptxt>
</item>
<item>
<ptxt>The 4th to 5th and 5th to 6th up-shift must be performed with the accelerator pedal released.</ptxt>
</item>
<item>
<ptxt>The 6th to 5th and 5th to 4th down-shift must be performed with the accelerator pedal released.</ptxt>
</item>
<item>
<ptxt>Do not operate the accelerator pedal for at least 2 seconds after shifting and do not shift successively.</ptxt>
</item>
<item>
<ptxt>The shift position commanded by the ECM is shown in the Data List display on the intelligent tester.</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve SL2 turns ON/OFF normally when the shift lever is in D.</ptxt>
<table pgwide="1">
<tgroup cols="7">
<colspec colname="COL1" align="left" colwidth="1.13in"/>
<colspec colname="COL2" colwidth="0.99in"/>
<colspec colname="COL3" colwidth="0.99in"/>
<colspec colname="COL4" colwidth="0.99in"/>
<colspec colname="COL5" colwidth="0.99in"/>
<colspec colname="COL6" colwidth="0.99in"/>
<colspec colname="COL7" colwidth="1.00in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>ECM command gearshift</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6th</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shift solenoid valve S3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000000W81063X_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W81063X_07_0001" proc-id="RM22W0E___00008C200000">
<testtitle>CHECK DTC OUTPUT (IN ADDITION TO DTC P0761)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the intelligent tester.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only P0761 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0761 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any other codes besides P0761 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W81063X_07_0010" fin="false">A</down>
<right ref="RM000000W81063X_07_0005" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000W81063X_07_0010" proc-id="RM22W0E___00008C400000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (SHIFT SOLENOID VALVE SLT)
</testtitle>
<content6 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the test while the ATF temperature is between 50 and 80°C (122 and 176°F).</ptxt>
</item>
<item>
<ptxt>Be careful to prevent the hose of SST from interfering with the exhaust pipe.</ptxt>
</item>
<item>
<ptxt>Perform the test with the A/C off.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<test1>
<ptxt>Remove the test plug on the transmission case and connect SST.</ptxt>
<figure>
<graphic graphicname="C219461E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<sst>
<sstitem>
<s-number>09992-00095</s-number>
<s-subnumber>09992-00231</s-subnumber>
<s-subnumber>09992-00271</s-subnumber>
</sstitem>
</sst>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Measure the line pressure with SST.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</test1>
<test1>
<ptxt>According to the display on the intelligent tester, perform the Active Test.</ptxt>
</test1>
<test1>
<ptxt>Measure the line pressure.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SLT)*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid SLT and raise line pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>OFF: Line pressure up (when Active Test "Activate the Solenoid (SLT)" is performed, ECM commands SLT solenoid to turn OFF)</ptxt>
</item>
<item>
<ptxt>ON: No action (normal operation)</ptxt>
</item>
</list1>
</atten4>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Vehicle stopped</ptxt>
</item>
<item>
<ptxt>Engine idling</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: Activate the Solenoid (SLT) in the Active Test is performed to check the line pressure changes by connecting SST to the automatic transmission, which is used in the Hydraulic Test (See page <xref label="Seep01" href="RM000000W7B0PPX"/>) as well. Please note that the pressure values in the Active Test and Hydraulic Test are different.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The line pressure changes as specified when performing the Active Test.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000W81063X_07_0002" fin="false">OK</down>
<right ref="RM000000W81063X_07_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W81063X_07_0002" proc-id="RM22W0E___00008C300000">
<testtitle>INSPECT SHIFT SOLENOID VALVE S3</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the shift solenoid valve S3.</ptxt>
<figure>
<graphic graphicname="C209925E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Shift solenoid valve S3 connector terminal - Shift solenoid valve S3 body</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F) </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 15 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Apply 12 V battery voltage to the shift solenoid valve and check that the valve moves and makes an operating noise.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery positive (+) → Shift solenoid valve S3 connector</ptxt>
</item>
<item>
<ptxt>Battery negative (-) → Shift solenoid valve S3 body</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Valve moves and makes an operating noise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Shift Solenoid Valve S3)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000W81063X_07_0011" fin="false">OK</down>
<right ref="RM000000W81063X_07_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W81063X_07_0011" proc-id="RM22W0E___00008C500000">
<testtitle>INSPECT TRANSMISSION VALVE BODY ASSEMBLY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the transmission valve body assembly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There are no foreign objects on each valve.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000W81063X_07_0004" fin="true">OK</down>
<right ref="RM000000W81063X_07_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W81063X_07_0004">
<testtitle>REPAIR OR REPLACE AUTOMATIC TRANSMISSION ASSEMBLY<xref label="Seep01" href="RM0000018ZD049X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W81063X_07_0005">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000030G908AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W81063X_07_0009">
<testtitle>REPLACE SHIFT SOLENOID VALVE SLT<xref label="Seep01" href="RM000000O9L06JX_02_0004"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W81063X_07_0006">
<testtitle>REPLACE SHIFT SOLENOID VALVE S3<xref label="Seep01" href="RM000000O9L06JX_02_0010"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W81063X_07_0007">
<testtitle>REPAIR OR REPLACE TRANSMISSION VALVE BODY ASSEMBLY<xref label="Seep01" href="RM0000013CM04WX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>