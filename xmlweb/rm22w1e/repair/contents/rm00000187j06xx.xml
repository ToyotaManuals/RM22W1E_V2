<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM00000187J06XX" category="C" type-id="300XS" name-id="ESVWM-06" from="201301" to="201308">
<dtccode>P0100</dtccode>
<dtcname>Mass Air Flow Circuit Malfunction</dtcname>
<subpara id="RM00000187J06XX_01" type-id="60" category="03" proc-id="RM22W0E___000037M00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The mass air flow meter consists of a platinum hot wire, a temperature sensor and a control circuit installed in a plastic housing. The temperature sensor (located in the intake air by-pass of the housing) detects the intake air temperature.</ptxt>
<ptxt>There is little variation in the output from the mass air flow meter, as the output voltage is converted into a periodic signal which minimizes the effect of wire harness resistance.</ptxt>
<ptxt>The internal circuit of the mass air flow meter contains a hot wire and cold wire. A voltage is output through a bridge circuit (refer to the illustration) indicating the cooling of the hot wire according to the intake air amount. This voltage is then converted into a periodic signal by the periodic signal conversion circuit and output to the ECM. The frequency varies between approximately 250 to 11000 Hz depending on the air flow rate. The ECM calculates the mass air flow rate based on this frequency.</ptxt>
<figure>
<graphic graphicname="A207644E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>P0100</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>3 seconds after engine is started</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mass air flow meter signal frequency is less than 850 Hz with the engine speed at 400 rpm or more for 3 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in mass air flow meter circuit</ptxt>
</item>
<item>
<ptxt>Mass air flow meter</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P0100</ptxt>
</entry>
<entry valign="middle">
<ptxt>MAF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="nonmark">
<item>
<ptxt>If DTC P0100 is stored, the following symptoms may appear (as the ECM mistakenly determines that there is less air than the actual intake air amount, EGR is decreased according to the target EGR):</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Combustion noise worsens</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187J06XX_02" type-id="32" category="03" proc-id="RM22W0E___000037N00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A275248E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000187J06XX_03" type-id="51" category="05" proc-id="RM22W0E___000037O00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187J06XX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187J06XX_04_0002" proc-id="RM22W0E___000037P00000">
<testtitle>INSPECT MASS AIR FLOW METER (POWER SOURCE CIRCUIT)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A127724E93" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C50-3 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Mass Air Flow Meter)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187J06XX_04_0004" fin="false">OK</down>
<right ref="RM00000187J06XX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0004" proc-id="RM22W0E___000037Q00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MASS AIR FLOW METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C50-1 (FG) - C45-71 (VG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-2 (E2G) - C45-94 (EVG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-1 (FG) or C45-71 (VG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C50-1 (FG) - C46-71 (VG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-2 (E2G) - C46-94 (EVG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-1 (FG) or C46-71 (VG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187J06XX_04_0010" fin="false">OK</right>
<right ref="RM00000187J06XX_04_0015" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0005" proc-id="RM22W0E___000037R00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MASS AIR FLOW METER - INTEGRATION RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Remove the integration relay from the engine room relay block and junction block assembly.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C50-3 (+B) - 1A-4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-3 (+B) or 1A-4 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187J06XX_04_0026" fin="false">OK</down>
<right ref="RM00000187J06XX_04_0015" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0026" proc-id="RM22W0E___000037Y00000">
<testtitle>INSPECT INTEGRATION RELAY (EFI MAIN 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the integration relay (EFI MAIN 2) (See page <xref label="Seep01" href="RM000003BLB02YX_01_0003"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187J06XX_04_0027" fin="false">OK</down>
<right ref="RM00000187J06XX_04_0028" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0027" proc-id="RM22W0E___000037Z00000">
<testtitle>CHECK HARNESS AND CONNECTOR (INTEGRATION RELAY - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the integration relay from the engine room relay block and junction block assembly.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.53in"/>
<colspec colname="COLSPEC3" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1A-2 - A38-44 (MREL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1A-2 or A38-44 (MREL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.53in"/>
<colspec colname="COLSPEC3" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1A-2 - A52-44 (MREL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1A-2 or A52-44 (MREL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187J06XX_04_0012" fin="false">OK</down>
<right ref="RM00000187J06XX_04_0015" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0012" proc-id="RM22W0E___000037T00000">
<testtitle>CHECK ECM POWER SOURCE CIRCUIT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the ECM power source circuit (See page <xref label="Seep01" href="RM000001DN809BX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187J06XX_04_0017" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0015" proc-id="RM22W0E___000037U00000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187J06XX_04_0017" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0028" proc-id="RM22W0E___000038000000">
<testtitle>REPLACE INTEGRATION RELAY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the integration relay (EFI MAIN 2).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187J06XX_04_0017" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0010" proc-id="RM22W0E___000037S00000">
<testtitle>REPLACE MASS AIR FLOW METER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the mass air flow meter (See page <xref label="Seep01" href="RM000002DFB00HX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187J06XX_04_0025" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0025" proc-id="RM22W0E___000037X00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and wait for 3 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0100 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000187J06XX_04_0022" fin="false">A</down>
<right ref="RM00000187J06XX_04_0024" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0022" proc-id="RM22W0E___000037W00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187J06XX_04_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0017" proc-id="RM22W0E___000037V00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and wait for 3 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187J06XX_04_0024" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187J06XX_04_0024">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>