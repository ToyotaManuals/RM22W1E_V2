<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3TA_T00MD" variety="T00MD">
<name>ENGINE IMMOBILISER SYSTEM (w/ Entry and Start System)</name>
<para id="RM000000QY40DOX" category="D" type-id="3001B" name-id="EI000N-311" from="201308">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM000000QY40DOX_z0" proc-id="RM22W0E___0000ESN00001">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use these procedures to troubleshoot the engine immobiliser system.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>*: Use the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding.</ptxt>
</item>
</list1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK FOR DTC*</testtitle>
<test1>
<ptxt>Check for DTCs and make a note of any codes that are output (See page <xref label="Seep01" href="RM000000QYC05XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep08" href="RM000000QYC05XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep09" href="RM000000QYC05XX"/>). Based on the DTCs output above, try to cause the output of the same SFI system or ECD system DTC, LIN communication system DTC, steering lock system DTC or engine immobiliser system DTC by simulating the symptoms indicated by the DTC.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>SFI system (for 1GR-FE) DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>SFI system (for 1UR-FE) DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ECD system (for 1VD-FTV) DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>SFI system (for 3UR-FE) DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>LIN communication system DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>F</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Steering lock system DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine immobiliser system DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>H</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to DIAGNOSTIC TROUBLE CODE CHART (See page <xref label="Seep04" href="RM000002ZSO010X"/>)</action-ci-right>
<result>C</result>
<action-ci-right>Go to DIAGNOSTIC TROUBLE CODE CHART (See page <xref label="Seep05" href="RM0000032SF055X"/>)</action-ci-right>
<result>D</result>
<action-ci-right>Go to DIAGNOSTIC TROUBLE CODE CHART (See page <xref label="Seep06" href="RM0000031HW066X"/>)</action-ci-right>
<result>E</result>
<action-ci-right>Go to DIAGNOSTIC TROUBLE CODE CHART (See page <xref label="Seep13" href="RM0000032SF053X"/>)</action-ci-right>
<result>F</result>
<action-ci-right>Go to DIAGNOSTIC TROUBLE CODE CHART (See page <xref label="Seep07" href="RM000002S8802GX"/>)</action-ci-right>
<result>G</result>
<action-ci-right>Go to DIAGNOSTIC TROUBLE CODE CHART (See page <xref label="Seep11" href="RM00000287G045X"/>)</action-ci-right>
<result>H</result>
<action-ci-right>Go to DIAGNOSTIC TROUBLE CODE CHART (See page <xref label="Seep12" href="RM000002M6W04UX"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to the problem symptoms table (See page <xref label="Seep10" href="RM000002M6U04YX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Fault is not listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fault is listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to step 6</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>OVERALL ANALYSIS AND TROUBLESHOOTING*</testtitle>
<test1>
<ptxt>Data List/Active Test (See page <xref label="Seep02" href="RM000002M6V03QX"/>)</ptxt>
</test1>
<test1>
<ptxt>Terminals of ECU (See page <xref label="Seep03" href="RM000000QY70A9X"/>)</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>ADJUST, REPAIR OR REPLACE</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>