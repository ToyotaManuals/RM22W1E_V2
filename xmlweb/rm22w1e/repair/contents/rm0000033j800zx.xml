<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM0000033J800ZX" category="C" type-id="300B7" name-id="ACBKS-02" from="201301" to="201308">
<dtccode>B1417/17</dtccode>
<dtcname>Rear Evaporator Temperature Sensor Circuit</dtcname>
<subpara id="RM0000033J800ZX_01" type-id="60" category="03" proc-id="RM22W0E___0000H1U00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The No. 2 cooler thermistor is installed on the evaporator in the rear air conditioning unit to detect the cooled air temperature that has passed through the evaporator and control the air conditioning. It sends appropriate signals to the air conditioning amplifier assembly. The resistance of the No. 2 cooler thermistor changes in accordance with the cooled air temperature that has passed through the rear evaporator. As the temperature decreases, the resistance increases. As the temperature increases, the resistance decreases.</ptxt>
<ptxt>The air conditioning amplifier assembly applies voltage (5 V) to the No. 2 cooler thermistor and reads voltage changes as the resistance of the No. 2 cooler thermistor changes. This sensor is used for frost prevention.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1417/17</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>An open or short in the No. 2 cooler thermistor circuit.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Harness or connector between No. 2 air conditioning harness assembly and air conditioning amplifier assembly</ptxt>
</item>
<item>
<ptxt>No. 2 air conditioning harness assembly (No. 2 cooler thermistor)</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000033J800ZX_02" type-id="32" category="03" proc-id="RM22W0E___0000H1V00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E160576E04" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000033J800ZX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000033J800ZX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000033J800ZX_05_0001" proc-id="RM22W0E___0000H1W00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (NO. 2 COOLER THERMISTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the No. 2 cooler thermistor is functioning properly.</ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ambient Temp Sens (Rear)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No. 2 cooler thermistor /</ptxt>
<ptxt>Min.: -29.7°C (-21.46°F)</ptxt>
<ptxt>Max.: 59.55°C (139.19°F)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Actual rear evaporator temperature displayed</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Open in the circuit: -29.7°C (-21.46°F).</ptxt>
<ptxt>Short in the circuit: 59.55°C (139.19°F).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000033J800ZX_05_0012" fin="true">OK</down>
<right ref="RM0000033J800ZX_05_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000033J800ZX_05_0002" proc-id="RM22W0E___0000H1X00000">
<testtitle>INSPECT NO. 2 COOLER THERMISTOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E156641E03" width="2.775699831in" height="6.791605969in"/>
</figure>
<test1>
<ptxt>Remove the No. 2 air conditioning harness assembly (See page <xref label="Seep01" href="RM000001JR302PX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="8" valign="middle">
<ptxt>7 (-) - 8 (+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>-10°C (14°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.40 to 9.20 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>-5°C (23°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.65 to 7.00 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>0°C (32°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.35 to 5.40 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>5°C (41°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.40 to 4.20 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>10°C (50°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.68 to 3.30 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>15°C (59°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.10 to 2.60 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.66 to 2.10 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>25°C (77°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.32 to 1.66 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>30°C (86°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.05 to 1.35 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Even slightly touching the sensor may change the resistance value. Be sure to hold the connector of the sensor.</ptxt>
</item>
<item>
<ptxt>When measuring, the sensor temperature must be the same as the ambient temperature.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>As the temperature increases, the resistance decreases (see the graph).</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000033J800ZX_05_0007" fin="false">OK</down>
<right ref="RM0000033J800ZX_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000033J800ZX_05_0007" proc-id="RM22W0E___0000H1Z00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 2 AIR CONDITIONING HARNESS - AIR CONDITIONING AMPLIFIER)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E156642E03" width="2.775699831in" height="5.787629434in"/>
</figure>
<test1>
<ptxt>Disconnect the K19 No. 2 air conditioning harness connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E35 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>K19-8 (+) - E35-17 (TEC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>K19-7 (-) - E35-1 (SG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E35-17 (TEC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E35-1 (SG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000033J800ZX_05_0003" fin="false">OK</down>
<right ref="RM0000033J800ZX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000033J800ZX_05_0003" proc-id="RM22W0E___0000H1Y00000">
<testtitle>CHECK NO. 2 AIR CONDITIONING HARNESS ASSEMBLY (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the No. 2 air conditioning harness assembly with a normal one and check that the condition returns to normal (See page <xref label="Seep01" href="RM000001JR302PX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000033J800ZX_05_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000033J800ZX_05_0011" proc-id="RM22W0E___0000H2000000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002LIT037X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000002LIT037X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1417 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000033J800ZX_05_0015" fin="true">OK</down>
<right ref="RM0000033J800ZX_05_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000033J800ZX_05_0012">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000033J800ZX_05_0006">
<testtitle>REPLACE NO. 2 AIR CONDITIONING HARNESS ASSEMBLY<xref label="Seep01" href="RM000001JR302PX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000033J800ZX_05_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000033J800ZX_05_0014">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000033J800ZX_05_0015">
<testtitle>REPLACE NO. 2 AIR CONDITIONING HARNESS ASSEMBLY<xref label="Seep01" href="RM000001JR302PX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>