<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000000XCT0X0X" category="C" type-id="302GG" name-id="ESZKH-01" from="201301" to="201308">
<dtccode>P0724</dtccode>
<dtcname>Brake Switch "B" Circuit High</dtcname>
<subpara id="RM000000XCT0X0X_01" type-id="60" category="03" proc-id="RM22W0E___00003AW00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates that the stop light switch remains on. When the stop light switch remains on during GO and STOP driving, the ECM interprets this as a fault in the stop light switch. Then the MIL illuminates and the ECM stores the DTC.</ptxt>
<table pgwide="1">
<title>P0724</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Accelerate the vehicle to 30 km/h (19 mph) or more, depress the brake pedal and decelerate the vehicle to 3 km/h (1.8 mph) or less 5 times.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch remains on even when vehicle is driven in GO (30 km/h (19 mph) or more) and STOP (less than 3 km/h (1.8 mph)) pattern 5 times.</ptxt>
<ptxt>(2 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in stop light switch signal circuit</ptxt>
</item>
<item>
<ptxt>Stop light switch assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P0724</ptxt>
</entry>
<entry>
<ptxt>Stop Light Switch</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XCT0X0X_02" type-id="32" category="03" proc-id="RM22W0E___00003AX00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0504 (See page <xref label="Seep01" href="RM000000XCT0X1X_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000XCT0X0X_03" type-id="51" category="05" proc-id="RM22W0E___00003AY00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XCT0X0X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XCT0X0X_04_0011" proc-id="RM22W0E___00003B400000">
<testtitle>READ VALUE USING GTS (STOP LIGHT SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / Stop Light Switch.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the GTS when the brake pedal is depressed and released.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL3" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Brake Pedal</ptxt>
</entry>
<entry>
<ptxt>Display</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Released</ptxt>
</entry>
<entry>
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Depressed</ptxt>
</entry>
<entry>
<ptxt>ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<right ref="RM000000XCT0X0X_04_0012" fin="false">OK</right>
<right ref="RM000000XCT0X0X_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XCT0X0X_04_0002" proc-id="RM22W0E___00003AZ00000">
<testtitle>INSPECT STOP LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the stop light switch assembly (See page <xref label="Seep01" href="RM0000038XN00FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XCT0X0X_04_0003" fin="false">OK</down>
<right ref="RM000000XCT0X0X_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XCT0X0X_04_0003" proc-id="RM22W0E___00003B000000">
<testtitle>CHECK ECM (STP VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A275087E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake Pedal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38-13 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Released</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 3 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake Pedal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>A52-13 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Released</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 3 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake Pedal Depressed</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake Pedal Released</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XCT0X0X_04_0007" fin="false">OK</down>
<right ref="RM000000XCT0X0X_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XCT0X0X_04_0007" proc-id="RM22W0E___00003B300000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000XCT0X0X_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000XCT0X0X_04_0005" proc-id="RM22W0E___00003B100000">
<testtitle>REPLACE STOP LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the stop light switch assembly (See page <xref label="Seep01" href="RM0000038XP00HX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000XCT0X0X_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000XCT0X0X_04_0006" proc-id="RM22W0E___00003B200000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XCT0X0X_04_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XCT0X0X_04_0012" proc-id="RM22W0E___00003B500000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Accelerate the vehicle to 30 km/h (19 mph) or more, depress the brake pedal and decelerate the vehicle to 3 km/h (1.8 mph) or less 5 times or more.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the GTS to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Engine and ECT / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P0724.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If STATUS is NORMAL, DTC judgment is complete and it can be determined that the system is normal.</ptxt>
</item>
<item>
<ptxt>If STATUS is INCOMPLETE or N/A, DTC judgment is incomplete. Perform the following procedure again 5 times: accelerate the vehicle to 30 km/h (19 mph) or more, depress the brake pedal and decelerate the vehicle to 3 km/h (1.8 mph) or less.</ptxt>
</item>
</list1>
</atten4>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000XCT0X0X_04_0001" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XCT0X0X_04_0001">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>