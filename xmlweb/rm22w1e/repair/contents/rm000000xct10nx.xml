<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000XCT10NX" category="C" type-id="302I3" name-id="ES17MR-001" from="201308">
<dtccode>P0504</dtccode>
<dtcname>Brake Switch "A" / "B" Correlation</dtcname>
<subpara id="RM000000XCT10NX_01" type-id="60" category="03" proc-id="RM22W0E___00001FQ00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The stop light switch is a duplex system that transmits two signals: STP and ST1-. These two signals are used by the ECM to monitor whether or not the brake system is working properly. If signals which indicate the brake pedal is being depressed and released are detected simultaneously, the ECM interprets this as a malfunction in the stop light switch and stores the DTC.</ptxt>
<atten4>
<ptxt>The normal signal conditions are as shown in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Signal (ECM Terminal)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake Pedal Released</ptxt>
</entry>
<entry valign="middle">
<ptxt>In Transition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake Pedal Depressed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>STP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ST1-</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>[OFF] denotes ground potential.</ptxt>
</item>
<item>
<ptxt>[ON] denotes battery potential (+B).</ptxt>
</item>
<item>
<ptxt>On the GTS, the Data List items Stop Light Switch and ST1 are both ON when the brake pedal is depressed because the characteristics of ST1 indication are the opposite of the characteristics of Stop Light Switch indication.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0504</ptxt>
</entry>
<entry valign="middle">
<ptxt>Conditions (a), (b) and (c) continue for 0.5 seconds or more</ptxt>
<ptxt>(1 trip detection logic):</ptxt>
<ptxt>(a) Engine switch is on (IG).</ptxt>
<ptxt>(b) Brake pedal is released.</ptxt>
<ptxt>(c) STP signal is OFF when the ST1- signal is OFF.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in stop light switch signal circuit</ptxt>
</item>
<item>
<ptxt>STOP fuse</ptxt>
</item>
<item>
<ptxt>IGN fuse</ptxt>
</item>
<item>
<ptxt>Stop light switch assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XCT10NX_02" type-id="32" category="03" proc-id="RM22W0E___00001FR00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A179962E05" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XCT10NX_03" type-id="51" category="05" proc-id="RM22W0E___00001FS00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>STP signal conditions can be checked using the GTS.</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG).</ptxt>
</item>
<item>
<ptxt>Turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / All Data / Stop Light Switch.</ptxt>
</item>
<item>
<ptxt>Check the STP signal when the brake pedal is depressed and released.</ptxt>
</item>
</list2>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Brake Pedal Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>STP signal ON</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>STP signal OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XCT10NX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XCT10NX_04_0010" proc-id="RM22W0E___00001FW00001">
<testtitle>READ VALUE USING GTS (STOP LIGHT SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / All Data / Stop Light Switch.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the GTS.</ptxt>
<spec>
<title>Standard</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry align="center">
<ptxt>STP signal ON</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry align="center">
<ptxt>STP signal OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XCT10NX_04_0011" fin="true">OK</down>
<right ref="RM000000XCT10NX_04_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XCT10NX_04_0001" proc-id="RM22W0E___00001FT00001">
<testtitle>INSPECT STOP LIGHT SWITCH ASSEMBLY (B+ VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A204535E23" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the stop light switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A30-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A30-4 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Stop Light Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XCT10NX_04_0002" fin="false">OK</down>
<right ref="RM000000XCT10NX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XCT10NX_04_0002" proc-id="RM22W0E___00001FU00001">
<testtitle>INSPECT STOP LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the stop light switch assembly (See page <xref label="Seep01" href="RM0000038XN00FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XCT10NX_04_0003" fin="false">OK</down>
<right ref="RM000000XCT10NX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XCT10NX_04_0003" proc-id="RM22W0E___00001FV00001">
<testtitle>INSPECT ECM (STP AND ST1 - VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A276700E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>A52-35 (ST1-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.5 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A52-36 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.5 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38-35 (ST1-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.5 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A38-36 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.5 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Depressed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Released</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XCT10NX_04_0007" fin="true">OK</down>
<right ref="RM000000XCT10NX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XCT10NX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (BATTERY - STOP LIGHT SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000000XCT10NX_04_0005">
<testtitle>REPLACE STOP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM0000038XP00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XCT10NX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XCT10NX_04_0011">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ13TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XCT10NX_04_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>