<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000PD911BX" category="D" type-id="3001B" name-id="ES17MJ-001" from="201308">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM000000PD911BX_z0" proc-id="RM22W0E___000016Y00001">
<content5 releasenbr="1">
<atten4>
<ptxt>*: Use the GTS.</ptxt>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CUSTOMER PROBLEM ANALYSIS</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONNECT GTS TO DLC3*</testtitle>
<atten4>
<ptxt>If the display indicates a communication fault in the GTS, inspect the DLC3.</ptxt>
</atten4>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK DTC AND FREEZE FRAME DATA*</testtitle>
<test1>
<ptxt>Check DTC (See page <xref label="Seep01" href="RM000000PDK187X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check freeze frame data (See page <xref label="Seep02" href="RM000000PDS10WX"/>).</ptxt>
<atten4>
<ptxt>Record or print DTCs and freeze frame data, if necessary.</ptxt>
</atten4>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CLEAR DTC AND FREEZE FRAME DATA*</testtitle>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep03" href="RM000000PDK187X"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONDUCT VISUAL INSPECTION</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>SET CHECK MODE DIAGNOSIS*</testtitle>
<test1>
<ptxt>Switch the ECM from normal mode to check mode (See page <xref label="Seep04" href="RM000000PDL0SCX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONFIRM PROBLEM SYMPTOMS</testtitle>
<test1>
<ptxt>Refer to "Problem Symptoms Table" (See page <xref label="Seep05" href="RM000000PDG0VTX"/>).</ptxt>
<atten4>
<ptxt>If the engine does not start, first perform the "Check DTC" procedures and "Conduct Basic Inspection" procedures below.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Malfunction does not occur</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Malfunction occurs</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO STEP 10</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>SIMULATE SYMPTOMS</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK DTC*</testtitle>
<test1>
<ptxt>Check DTC (See page <xref label="Seep06" href="RM000000PDK187X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Trouble code output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No code output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO STEP 12</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>REFER TO DTC CHART</testtitle>
<test1>
<ptxt>Refer to "Diagnostic Trouble Code Chart" (See page <xref label="Seep07" href="RM0000032SF055X"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>GO TO STEP 14</action-ci-fin>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONDUCT BASIC INSPECTION</testtitle>
<test1>
<ptxt>Refer to "Basic Inspection" (See page <xref label="Seep08" href="RM000000PDM0RTX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Malfunctioning parts not confirmed</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Malfunctioning parts confirmed</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO STEP 17</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>REFER TO PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to "Problem Symptoms Table" (See page <xref label="Seep09" href="RM000000PDG0VTX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Malfunctioning circuit confirmed</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Malfunctioning parts confirmed</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO STEP 17</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK ECM POWER SOURCE CIRCUIT</testtitle>
<test1>
<ptxt>Refer to "ECM Power Source Circuit" (See page <xref label="Seep10" href="RM000001DN80A7X"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONDUCT CIRCUIT INSPECTION</testtitle>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Result</ptxt>
</entry>
<entry>
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Malfunction not confirmed</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Malfunction confirmed</ptxt>
</entry>
<entry>
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<results>
<result>B</result>
<action-ci-right>GO TO STEP 18</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK FOR INTERMITTENT PROBLEMS</testtitle>
<test1>
<ptxt>Refer to "Check for Intermittent Problems" (See page <xref label="Seep11" href="RM000000PDQ13TX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>GO TO STEP 18</action-ci-fin>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONDUCT PARTS INSPECTION</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>IDENTIFY PROBLEM</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>ADJUST AND/OR REPAIR</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONDUCT CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>