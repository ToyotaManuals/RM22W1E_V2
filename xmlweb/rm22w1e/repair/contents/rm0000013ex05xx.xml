<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0019" variety="S0019">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0019_7C3MA_T00FD" variety="T00FD">
<name>AUTOMATIC TRANSMISSION UNIT</name>
<para id="RM0000013EX05XX" category="G" type-id="3001K" name-id="AT34R-03" from="201301">
<name>INSPECTION</name>
<subpara id="RM0000013EX05XX_01" type-id="01" category="01">
<s-1 id="RM0000013EX05XX_01_0001" proc-id="RM22W0E___00007H600000">
<ptxt>INSPECT AUTOMATIC TRANSMISSION OIL PAN SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the magnets, and use them to collect steel particles.</ptxt>
<figure>
<graphic graphicname="D001124E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Carefully look at the foreign matter and particles in the pan and on the magnets to anticipate the type of wear you will find in the transmission.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Steel (magnetic): bearing, gear and clutch plate wear</ptxt>
</item>
<item>
<ptxt>Brass (non-magnetic): bushing wear</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0002" proc-id="RM22W0E___00007H700000">
<ptxt>INSPECT NO. 2 1-WAY CLUTCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Hold the reverse clutch hub and turn the 1-way clutch assembly.</ptxt>
<figure>
<graphic graphicname="D027993E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Check that the 1-way clutch turns freely clockwise and locks when turned counterclockwise.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0021" proc-id="RM22W0E___00007HQ00000">
<ptxt>INSPECT REAR CLUTCH DISC</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) the grooves or printed numbers have even a little bit of damage.</ptxt>
<figure>
<graphic graphicname="C171511" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 15 minutes.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0022" proc-id="RM22W0E___00007HR00000">
<ptxt>INSPECT REVERSE CLUTCH HUB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the inside diameter of the reverse clutch hub bush.</ptxt>
<figure>
<graphic graphicname="D028000E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard inside diameter</title>
<specitem>
<ptxt>35.812 to 35.837 mm (1.410 to 1.411 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum inside diameter</title>
<specitem>
<ptxt>35.887 mm (1.413 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the inside diameter is more than the maximum, replace the reverse clutch hub.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0023" proc-id="RM22W0E___00007HS00000">
<ptxt>INSPECT FORWARD CLUTCH HUB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the inside diameter of the forward clutch hub bush.</ptxt>
<figure>
<graphic graphicname="D028501E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard inside diameter</title>
<specitem>
<ptxt>26.037 to 26.062 mm (1.025 to 1.026 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum inside diameter</title>
<specitem>
<ptxt>26.112 mm (1.028 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the inside diameter is more than the maximum, replace the forward clutch hub.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0024" proc-id="RM22W0E___00007HT00000">
<ptxt>INSPECT FORWARD MULTIPLE DISC CLUTCH DISC</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) the grooves or printed numbers have even a little bit of damage.</ptxt>
<figure>
<graphic graphicname="C171513" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 15 minutes.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0025" proc-id="RM22W0E___00007HU00000">
<ptxt>INSPECT FORWARD CLUTCH RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<figure>
<graphic graphicname="C171515" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>26.74 mm (1.05 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0026" proc-id="RM22W0E___00007HV00000">
<ptxt>INSPECT DIRECT CLUTCH DISC</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) the grooves or printed numbers have even a little bit of damage.</ptxt>
<figure>
<graphic graphicname="C171512" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 15 minutes.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0027" proc-id="RM22W0E___00007HW00000">
<ptxt>INSPECT REVERSE CLUTCH RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<figure>
<graphic graphicname="C171517" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>21.04 mm (0.828 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0028" proc-id="RM22W0E___00007HX00000">
<ptxt>INSPECT DIRECT CLUTCH RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<figure>
<graphic graphicname="C171516" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>19.51 mm (0.768 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0003" proc-id="RM22W0E___00007H800000">
<ptxt>INSPECT NO. 3 BRAKE DISC</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) the grooves or printed numbers have even a little bit of damage.</ptxt>
<figure>
<graphic graphicname="C161778" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 15 minutes.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0004" proc-id="RM22W0E___00007H900000">
<ptxt>INSPECT NO. 3 BRAKE PISTON RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<figure>
<graphic graphicname="C171518" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>15.72 mm (0.619 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0005" proc-id="RM22W0E___00007HA00000">
<ptxt>INSPECT FRONT PLANETARY GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a feeler gauge, measure the front planetary pinion gear thrust clearance.</ptxt>
<figure>
<graphic graphicname="D028544E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard clearance</title>
<specitem>
<ptxt>0.2 to 0.6 mm (0.00787 to 0.0236 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum clearance</title>
<specitem>
<ptxt>0.65 mm (0.0256 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the clearance is more than the maximum, replace the front planetary gear assembly.</ptxt>
</item>
</list1>
</s2>
<s2>
<ptxt>Using a cylinder gauge, measure the inside diameter of the front planetary gear bush.</ptxt>
<figure>
<graphic graphicname="D028543E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum inside diameter</title>
<specitem>
<ptxt>57.48 mm (2.26 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the inside diameter is more than the maximum, replace the front planetary gear.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0006" proc-id="RM22W0E___00007HB00000">
<ptxt>INSPECT 1-WAY CLUTCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="D028542E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the 1-way clutch to the 1-way clutch inner race.</ptxt>
</s2>
<s2>
<ptxt>Hold the 1-way clutch inner race and turn the 1-way clutch assembly. Check that the 1-way clutch turns freely counterclockwise and locks when turned clockwise.</ptxt>
</s2>
<s2>
<ptxt>Remove the 1-way clutch from the 1-way clutch inner race.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0007" proc-id="RM22W0E___00007HC00000">
<ptxt>INSPECT NO. 1 BRAKE DISC</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) the grooves or printed numbers have even a little bit of damage.</ptxt>
<figure>
<graphic graphicname="C171514" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 15 minutes.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0008" proc-id="RM22W0E___00007HD00000">
<ptxt>INSPECT BRAKE PISTON RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<figure>
<graphic graphicname="C171519" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>17.05 mm (0.671 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0009" proc-id="RM22W0E___00007HE00000">
<ptxt>INSPECT NO. 2 BRAKE DISC</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) the grooves or printed numbers have even a little bit of damage.</ptxt>
<figure>
<graphic graphicname="C161770" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 15 minutes.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0020" proc-id="RM22W0E___00007HP00000">
<ptxt>INSPECT NO. 2 BRAKE PISTON RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<figure>
<graphic graphicname="C171520" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>17.45 mm (0.687 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0010" proc-id="RM22W0E___00007HF00000">
<ptxt>INSPECT CENTER PLANETARY GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="D028533E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a feeler gauge, measure the center planetary pinion gear thrust clearance.</ptxt>
<spec>
<title>Standard clearance</title>
<specitem>
<ptxt>0.12 to 0.68 mm (0.00472 to 0.0268 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum clearance</title>
<specitem>
<ptxt>0.73 mm (0.0287 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the clearance is more than the maximum, replace the center planetary gear assembly.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0011" proc-id="RM22W0E___00007HG00000">
<ptxt>INSPECT NO. 3 1-WAY CLUTCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Hold the rear planetary ring gear flange and turn the 1-way clutch. Check that the 1-way clutch turns freely counterclockwise and locks when turned clockwise.</ptxt>
<figure>
<graphic graphicname="D028532E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0012" proc-id="RM22W0E___00007HH00000">
<ptxt>INSPECT REAR PLANETARY RING GEAR FLANGE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="D028531E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a dial indicator, measure the inside diameter of the rear planetary ring gear bush.</ptxt>
<spec>
<title>Standard inside diameter</title>
<specitem>
<ptxt>32.176 to 32.201 (1.267 to 1.268 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum inside diameter</title>
<specitem>
<ptxt>32.251 mm (1.270 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the inside diameter is more than the maximum, replace the rear planetary ring gear.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0013" proc-id="RM22W0E___00007HI00000">
<ptxt>INSPECT INTERMEDIATE SHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the intermediate shaft runout.</ptxt>
<figure>
<graphic graphicname="D028529E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard runout</title>
<specitem>
<ptxt>0.03 mm (0.00118 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum runout</title>
<specitem>
<ptxt>0.08 mm (0.00315 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the runout is more than the maximum, replace the intermediate shaft with a new one.</ptxt>
</s2>
<s2>
<ptxt>Using a micrometer, measure the diameter of the intermediate shaft at the positions shown in the diagram.</ptxt>
<figure>
<graphic graphicname="D028530E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard diameter</title>
<subtitle>A, B</subtitle>
<specitem>
<ptxt>25.962 to 25.975 mm (1.022 to 1.023 in.)</ptxt>
</specitem>
<subtitle>C, D</subtitle>
<specitem>
<ptxt>32.062 to 32.075 mm (1.262 to 1.263 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum diameter</title>
<subtitle>A, B</subtitle>
<specitem>
<ptxt>25.912 mm (1.02 in.)</ptxt>
</specitem>
<subtitle>C, D</subtitle>
<specitem>
<ptxt>32.012 mm (1.26 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is less than the minimum, replace the intermediate shaft with a new one.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0014" proc-id="RM22W0E___00007HJ00000">
<ptxt>INSPECT NO. 4 BRAKE DISC</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Replace all discs if one of the following problems is present: 1) a disc, plate or flange is worn or burnt, 2) the lining of a disc is peeled off or discolored, or 3) the grooves or printed numbers have even a little bit of damage.</ptxt>
<figure>
<graphic graphicname="C161745" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Before assembling new discs, soak them in ATF for at least 15 minutes.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0015" proc-id="RM22W0E___00007HK00000">
<ptxt>INSPECT REAR PLANETARY GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a feeler gauge, measure the rear planetary pinion gear thrust clearance.</ptxt>
<figure>
<graphic graphicname="D028527E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard clearance</title>
<specitem>
<ptxt>0.2 to 0.6 mm (0.00787 to 0.0236 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum clearance</title>
<specitem>
<ptxt>0.65 mm (0.0256 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the clearance is more than the maximum, replace the planetary gear assembly.</ptxt>
</item>
</list1>
</s2>
<s2>
<ptxt>Using a dial indicator, measure the inside diameter of the rear planetary gear bush.</ptxt>
<figure>
<graphic graphicname="D028528E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum inside diameter</title>
<specitem>
<ptxt>20.075 mm (0.790 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the inside diameter is more than the maximum, replace the rear planetary gear assembly.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0016" proc-id="RM22W0E___00007HL00000">
<ptxt>INSPECT 1ST AND REVERSE BRAKE RETURN SPRING SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the free length of the spring together with the spring seat.</ptxt>
<figure>
<graphic graphicname="C171521" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard free length</title>
<specitem>
<ptxt>23.74 mm (0.935 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0017" proc-id="RM22W0E___00007HM00000">
<ptxt>INSPECT PACK CLEARANCE OF 1ST AND REVERSE BRAKE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Make sure the 1st and reverse brake piston moves smoothly when applying and releasing compressed air into the transmission case.</ptxt>
<figure>
<graphic graphicname="D027966E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0018" proc-id="RM22W0E___00007HN00000">
<ptxt>INSPECT PISTON STROKE OF NO. 1 BRAKE PISTON</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Make sure the No. 1 brake piston moves smoothly when applying and releasing compressed air into the transmission case.</ptxt>
<figure>
<graphic graphicname="D028814E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a feeler gauge, measure the B3 bearing pack clearance between the snap ring and flange.</ptxt>
<figure>
<graphic graphicname="D028815E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard piston stroke</title>
<specitem>
<ptxt>0.42 to 0.72 mm (0.0165 to 0.0283 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the piston stroke is outside the specification, parts may have been assembled incorrectly. Perform the reassembly again. If the piston stroke is still outside the specification, select another flange.</ptxt>
<atten4>
<ptxt>There are 4 different thicknesses for the flange.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0029" proc-id="RM22W0E___00007HY00000">
<ptxt>INSPECT PACK CLEARANCE OF DIRECT CLUTCH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the moving distance (distance A) of the clutch flange at both ends across a diameter while applying air to the oil hole as shown in the illustration, and calculate the average.</ptxt>
<figure>
<graphic graphicname="D029841E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard pack clearance</title>
<specitem>
<ptxt>0.5 to 0.8 mm (0.0197 to 0.0315 in.)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>When measuring the moving distance, install a standard flange (thickness: 3.4 mm (0.134 in.)) to the position of the shaded area in the illustration.</ptxt>
</atten3>
<atten4>
<ptxt>Flange moving distance A = 0.26 to 1.14 mm (0.0102 to 0.0449 in.)</ptxt>
<ptxt>Pack clearance = Flange moving distance A - 0.05 mm (0.00197 in.)</ptxt>
</atten4>
<ptxt>If the pack clearance is outside the standard, select and install a clutch flange that makes the pack clearance within the standard.</ptxt>
<spec>
<title>Flange Thickness</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.24in"/>
<colspec colname="COL2" colwidth="2.89in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Mark</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Thickness</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.95 to 3.05 mm (0.116 to 0.120 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.05 to 3.15 mm (0.120 to 0.124 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.15 to 3.25 mm (0.124 to 0.128 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.25 to 3.35 mm (0.128 to 0.132 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>6</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.35 to 3.45 mm (0.132 to 0.136 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.45 to 3.55 mm (0.136 to 0.140 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>8</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.55 to 3.65 mm (0.140 to 0.144 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>9</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.65 to 3.75 mm (0.144 to 0.148 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.75 to 3.85 mm (0.148 to 0.152 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0030" proc-id="RM22W0E___00007HZ00000">
<ptxt>INSPECT PACK CLEARANCE OF REVERSE CLUTCH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the reverse clutch piston stroke (distance A) and the moving distance (distance B) of the reverse flange at both ends across a diameter while applying air (392 kPa (4.0 kgf/cm<sup>2</sup>, 57 psi)) to the oil hole as shown in the illustration, and calculate the average.</ptxt>
<spec>
<title>Standard pack clearance</title>
<specitem>
<ptxt>0.5 to 0.8 mm (0.0197 to 0.0315 in.)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>When measuring the moving distance, install a standard flange (thickness: 3.3 mm (0.130 in.)) to the position of the shaded area in the illustration.</ptxt>
</atten3>
<atten4>
<ptxt>Piston stroke A = 1.05 to 2.15 mm (0.0413 to 0.0846 in.)</ptxt>
<ptxt>Flange moving distance B = 0.72 to 1.08 mm (0.0283 to 0.0425 in.)</ptxt>
<ptxt>Pack clearance = Piston stroke A - Flange moving distance B - 0.06 mm (0.00236 in.)</ptxt>
</atten4>
<figure>
<graphic graphicname="D028538E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<ptxt>If the pack clearance is outside the standard, select and install a clutch flange that makes the pack clearance within the standard.</ptxt>
<spec>
<title>Flange Thickness</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.24in"/>
<colspec colname="COL2" colwidth="2.89in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Mark</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Thickness</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>0</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.75 to 2.85 mm (0.108 to 0.112 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.85 to 2.95 mm (0.112 to 0.116 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.95 to 3.05 mm (0.116 to 0.120 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.05 to 3.15 mm (0.120 to 0.124 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.15 to 3.25 mm (0.124 to 0.128 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.25 to 3.35 mm (0.128 to 0.132 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>6</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.35 to 3.45 mm (0.132 to 0.136 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.45 to 3.55 mm (0.136 to 0.140 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>8</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.55 to 3.65 mm (0.140 to 0.144 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>9</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.65 to 3.75 mm (0.144 to 0.148 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.75 to 3.85 mm (0.148 to 0.152 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0031" proc-id="RM22W0E___00007I000000">
<ptxt>INSPECT PACK CLEARANCE OF FORWARD CLUTCH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the moving distance (distance A) of the clutch flange at both ends across a diameter while applying air to the oil hole as shown in the illustration, and calculate the average.</ptxt>
<figure>
<graphic graphicname="D028537E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard pack clearance</title>
<specitem>
<ptxt>0.6 to 0.9 mm (0.0236 to 0.0354 in.)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>When measuring the moving distance, install a standard flange (thickness: 3.4 mm (0.134 in.)) to the position of the shaded area in the illustration.</ptxt>
</atten3>
<atten4>
<ptxt>Flange moving distance A = 0.26 to 1.36 mm (0.0102 to 0.0535 in.)</ptxt>
<ptxt>Pack clearance = Flange moving distance A - 0.01 mm (0.000394 in.)</ptxt>
</atten4>
<ptxt>If the pack clearance is outside the standard, select and install a clutch flange that makes the pack clearance within the standard.</ptxt>
<spec>
<title>Flange Thickness</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.24in"/>
<colspec colname="COL2" colwidth="2.89in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Mark</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Thickness</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>0</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.95 to 3.05 mm (0.116 to 0.120 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.05 to 3.15 mm (0.120 to 0.124 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.15 to 3.25 mm (0.124 to 0.128 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.25 to 3.35 mm (0.128 to 0.132 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.35 to 3.45 mm (0.132 to 0.136 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.45 to 3.55 mm (0.136 to 0.140 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>6</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.55 to 3.65 mm (0.140 to 0.144 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.65 to 3.75 mm (0.144 to 0.148 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>8</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.75 to 3.85 mm (0.148 to 0.152 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>9</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.85 to 3.95 mm (0.148 to 0.152 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.95 to 4.05 mm (0.152 to 0.159 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013EX05XX_01_0019" proc-id="RM22W0E___00007HO00000">
<ptxt>INSPECT INDIVIDUAL PISTON OPERATION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the operating sound while applying compressed air into the oil holes indicated in the illustration.</ptxt>
<figure>
<graphic graphicname="D028547E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>When inspecting the O/D direct clutch, check with the accumulator piston holes indicated in the illustration.</ptxt>
<ptxt>If there is no sound, disassemble and check the installation condition of the parts.</ptxt>
</atten4>
<list1 type="ordered">
<item>
<ptxt>No. 2 clutch (C2)</ptxt>
</item>
<item>
<ptxt>No. 3 clutch (C3)</ptxt>
</item>
<item>
<ptxt>No. 1 clutch (C1)</ptxt>
</item>
<item>
<ptxt>No. 3 brake (B3)</ptxt>
</item>
<item>
<ptxt>No. 1 brake (B1)</ptxt>
</item>
<item>
<ptxt>No. 2 brake (B2)</ptxt>
</item>
<item>
<ptxt>No. 4 brake (B4)</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>