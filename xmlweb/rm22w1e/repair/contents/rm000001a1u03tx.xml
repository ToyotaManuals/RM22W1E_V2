<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001Q" variety="S001Q">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001Q_7C3Q5_T00J8" variety="T00J8">
<name>VANE PUMP (for 1UR-FE)</name>
<para id="RM000001A1U03TX" category="A" type-id="8000E" name-id="PA0RW-02" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM000001A1U03TX_02" type-id="11" category="10" proc-id="RM22W0E___0000B5Q00000">
<content3 releasenbr="1">
<atten3>
<ptxt>When installing parts, coat the parts indicated by arrows with power steering fluid (See page <xref label="Seep01" href="RM000005296001X"/>).</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM000001A1U03TX_01" type-id="01" category="01">
<s-1 id="RM000001A1U03TX_01_0001" proc-id="RM22W0E___0000B5G00000">
<ptxt>INSTALL VANE PUMP HOUSING OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the lip of a new oil seal with power steering fluid.</ptxt>
</s2>
<s2>
<ptxt>Using SST and a press, press in the oil seal.</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00280</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C228151E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure that the oil seal is installed facing in the correct direction as shown in the illustration.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1U03TX_01_0002" proc-id="RM22W0E___0000B5H00000">
<ptxt>INSTALL VANE PUMP SHAFT WITH VANE PUMP PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the inside surface of the bushing in the vane pump front housing with power steering fluid.</ptxt>
</s2>
<s2>
<ptxt>Gradually insert the vane pump shaft with vane pump pulley.</ptxt>
<figure>
<graphic graphicname="C114703E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not damage the lip of the oil seal in the front housing.</ptxt>
</item>
<item>
<ptxt>Wrap protective tape around the spline of the vane pump shaft with vane pump pulley in order to prevent damage to the oil seal.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1U03TX_01_0003" proc-id="RM22W0E___0000B5I00000">
<ptxt>INSTALL VANE PUMP FRONT SIDE PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat a new O-ring with power steering fluid and install it into the front housing. </ptxt>
<figure>
<graphic graphicname="C132035" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Coat a new O-ring with power steering fluid and install it onto the front side plate.</ptxt>
<figure>
<graphic graphicname="C107035" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Align the notch of the front side plate with the notch of the front housing, and install the front side plate.</ptxt>
<figure>
<graphic graphicname="C132040E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Align</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure that the front side plate is installed facing in the correct direction.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1U03TX_01_0004" proc-id="RM22W0E___0000B5J00000">
<ptxt>INSTALL VANE PUMP CAM RING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the vane pump cam ring with power steering fluid.</ptxt>
</s2>
<s2>
<ptxt>Align the notch of the cam ring with the notch of the front side plate, and install the cam ring with the inscribed mark facing upward.</ptxt>
<figure>
<graphic graphicname="C139139E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Align</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Inscribed Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure that the cam ring is installed facing in the correct direction.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1U03TX_01_0005" proc-id="RM22W0E___0000B5K00000">
<ptxt>INSTALL VANE PUMP ROTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the vane pump rotor with power steering fluid.</ptxt>
</s2>
<s2>
<ptxt>Install the vane pump rotor.</ptxt>
<figure>
<graphic graphicname="C107038" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>The vane pump rotor can be installed in both directions.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Coat the 10 vane pump plates with power steering fluid.</ptxt>
</s2>
<s2>
<ptxt>Install the vane pump plates with the round end facing outward.</ptxt>
<figure>
<graphic graphicname="C141370E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Round End</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Outward</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure that the vane pump plates are installed facing in the correct direction.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1U03TX_01_0006" proc-id="RM22W0E___0000B5L00000">
<ptxt>INSTALL VANE PUMP SHAFT SNAP RING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver and snap ring expander, install a new shaft snap ring onto the vane pump shaft.</ptxt>
<figure>
<graphic graphicname="D030336E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not expand the shaft snap ring any further than needed.</ptxt>
</item>
<item>
<ptxt>Make sure that the shaft snap ring is completely fit into the groove.</ptxt>
</item>
<item>
<ptxt>Do not damage the vane pump rotor and shaft.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1U03TX_01_0007" proc-id="RM22W0E___0000B5M00000">
<ptxt>INSTALL VANE PUMP REAR HOUSING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat a new O-ring with power steering fluid and install it onto the rear housing.</ptxt>
<figure>
<graphic graphicname="C107040" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Align the straight pin of the rear housing with the notches of the cam ring, front side plate and front housing.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Align</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure that the O-ring is not protruding anywhere when installing the vane pump rear housing.</ptxt>
</atten3>
<figure>
<graphic graphicname="C136780E05" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the rear housing with the 4 bolts.</ptxt>
<figure>
<graphic graphicname="C107024" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>22</t-value1>
<t-value2>224</t-value2>
<t-value4>16</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1U03TX_01_0012" proc-id="RM22W0E___0000B5100000">
<ptxt>INSPECT TOTAL PRELOAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the pump rotates smoothly without abnormal noise.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install a service bolt.</ptxt>
<spec>
<title>Recommended service bolt</title>
<subtitle>Thread diameter</subtitle>
<specitem>
<ptxt>10 mm (0.394 in.)</ptxt>
</specitem>
<subtitle>Thread pitch</subtitle>
<specitem>
<ptxt>1.25 mm (0.0492 in.)</ptxt>
</specitem>
<subtitle>Bolt length</subtitle>
<specitem>
<ptxt>50 mm (1.97 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Using a torque wrench, measure the pump rotating torque.</ptxt>
<figure>
<graphic graphicname="C053369E13" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard rotating torque</title>
<specitem>
<ptxt>0.3 N*m (3 kgf*cm, 2 in.*lbf) or less</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Service Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the rotating torque is not as specified, check the vane pump housing oil seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001A1U03TX_01_0009" proc-id="RM22W0E___0000B5N00000">
<ptxt>INSTALL FLOW CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the flow control valve, pressure port union and spring with power steering fluid.</ptxt>
<figure>
<graphic graphicname="C228110" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the compression spring and flow control valve to the front housing.</ptxt>
<atten3>
<ptxt>Do not mistake the direction of the control valve.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Coat a new O-ring with power steering fluid and install it onto the pressure port union.</ptxt>
</s2>
<s2>
<ptxt>Install the pressure port union to the front housing.</ptxt>
<torque>
<torqueitem>
<t-value1>69</t-value1>
<t-value2>704</t-value2>
<t-value4>51</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1U03TX_01_0016" proc-id="RM22W0E___0000B5P00000">
<ptxt>FIX VANE PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, fix the vane pump in a vise.</ptxt>
<sst>
<sstitem>
<s-number>09630-00014</s-number>
<s-subnumber>09631-00132</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C158820E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001A1U03TX_01_0014" proc-id="RM22W0E___0000B5O00000">
<ptxt>INSTALL POWER STEERING SUCTION PORT UNION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat a new O-ring with power steering fluid and install it to the suction port union.</ptxt>
</s2>
<s2>
<ptxt>Install the suction port union to the vane pump with the bolt.</ptxt>
<figure>
<graphic graphicname="C158821" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>