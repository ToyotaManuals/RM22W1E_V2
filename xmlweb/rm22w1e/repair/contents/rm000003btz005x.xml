<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SD_T00LG" variety="T00LG">
<name>CAN COMMUNICATION SYSTEM (for RHD)</name>
<para id="RM000003BTZ005X" category="L" type-id="3001A" name-id="NW04T-04" from="201301" to="201308">
<name>PRECAUTION</name>
<subpara id="RM000003BTZ005X_z0" proc-id="RM22W0E___0000DNQ00000">
<content5 releasenbr="1">
<step1>
<ptxt>IGNITION SWITCH EXPRESSION</ptxt>
<atten4>
<ptxt>The type of ignition switch used on this model differs according to the specifications of the vehicle. The expressions listed in the table below are used in this section.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Expression</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition Switch (position)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine Switch (condition)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition Switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off (Lock)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition Switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On (IG)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition Switch ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On (ACC)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine Start</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>START</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On (Start)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</step1>
<step1>
<ptxt>STEERING SYSTEM HANDLING PRECAUTIONS</ptxt>
<step2>
<ptxt>Care must be taken when replacing parts. Incorrect replacement could affect the performance of the steering system and result in hazards when driving.</ptxt>
</step2>
</step1>
<step1>
<ptxt>SRS AIRBAG SYSTEM HANDLING PRECAUTIONS</ptxt>
<step2>
<ptxt>This vehicle is equipped with an SRS (Supplemental Restraint System), which includes components such as the driver airbag and front passenger airbag. Failure to carry out service operations in the correct sequence could cause unexpected SRS deployment during servicing and may lead to a serious accident. Before servicing (including installation/removal, inspection and replacement of parts), be sure to read the precautionary notice for the Supplemental Restraint System (See page <xref label="Seep01" href="RM000000KT10JGX"/>).</ptxt>
</step2>
</step1>
<step1>
<ptxt>BUS LINE REPAIR</ptxt>
<step2>
<ptxt>After repairing the bus line with solder, wrap the repaired part with vinyl tape (See page <xref label="Seep02" href="RM000000UZ30DCX"/>).</ptxt>
<figure>
<graphic graphicname="E120441" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The CANL bus line and CANH bus line must be installed together.</ptxt>
</item>
<item>
<ptxt>When installing the CAN bus lines, make sure to twist them.</ptxt>
</item>
<item>
<ptxt>CAN bus lines are likely to be influenced by noise if the bus lines are not twisted together.</ptxt>
</item>
<item>
<ptxt>Leave approximately 80 mm (3.15 in.) loose in the twisted wires around the connectors.</ptxt>
</item>
</list1>
</atten3>
</step2>
<step2>
<ptxt>Do not use by-pass wiring between the connectors.</ptxt>
<figure>
<graphic graphicname="E134624E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>The feature of the twisted wire harness will be lost if bypass wiring is used.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>By-pass Wire</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>CONNECTOR HANDLING</ptxt>
<step2>
<ptxt>When inserting probes into a connector, insert them from the rear of the connector.</ptxt>
<figure>
<graphic graphicname="E120443E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tester Probe</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Use a repair wire to check the connector if it is impossible to check resistance from the rear of the connector.</ptxt>
<figure>
<graphic graphicname="E120444E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Repair Wire</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>