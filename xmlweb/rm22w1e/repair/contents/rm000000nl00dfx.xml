<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R4_T00K7" variety="T00K7">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM000000NL00DFX" category="D" type-id="303F9" name-id="AV002D-160" from="201301">
<name>IDENTIFICATION OF NOISE SOURCE</name>
<subpara id="RM000000NL00DFX_z0" proc-id="RM22W0E___0000C6Y00000">
<content5 releasenbr="1">
<step1>
<ptxt>RADIO DESCRIPTION</ptxt>
<step2>
<ptxt>Radio frequency band</ptxt>
<step3>
<ptxt>Radio broadcasts use the radio frequency bands shown in the table below.</ptxt>
<figure>
<graphic graphicname="E108734E07" width="7.106578999in" height="2.775699831in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>Service area</ptxt>
<figure>
<graphic graphicname="E108735E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<step3>
<ptxt>The service areas of AM and FM broadcasts are vastly different. Sometimes an AM broadcast can be received very clearly but an FM stereo cannot. FM stereo has the smallest service area, and is prone to pick up static and other types of interference such as noise.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Radio reception problems</ptxt>
<atten4>
<ptxt>In addition to static, other problems such as "phasing", "multipath" and "fade out" exist. These problems are not caused by electrical noise, but by the radio signal propagation method itself.</ptxt>
</atten4>
<step3>
<ptxt>Phasing</ptxt>
<ptxt>AM broadcasts are susceptible to electrical interference and another kind of interference called phasing. Occurring only at night, phasing is the interference created when a vehicle receives 2 radio wave signals from the same transmitter. One signal is reflected off the ionosphere and the other signal is received directly from the transmitter.</ptxt>
<figure>
<graphic graphicname="I100011E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>Multipath</ptxt>
<ptxt>Multipath is a type of interference created when a vehicle receives 2 radio wave signals from the same transmitter. One signal is reflected off buildings or mountains and the other signal is received directly from the transmitter.</ptxt>
<figure>
<graphic graphicname="I100012E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>Fade out</ptxt>
<ptxt>Fade out is caused by objects (buildings, mountains and other such large obstacles) that deflect away part of a signal, resulting in a weaker signal when the object is between the transmitter and vehicle. High frequency radio waves, such as FM broadcasts, are easily deflected by obstructions. Low frequency radio waves, such as AM broadcasts, are less likely to deflect. </ptxt>
<figure>
<graphic graphicname="I100013E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>Noise problem</ptxt>
<ptxt>Technicians must have a clear understanding about each customer's noise complaint. Use the following table to diagnose noise problems.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.71in"/>
<colspec colname="COL2" colwidth="3.01in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>Radio Frequency</ptxt>
</entry>
<entry align="center">
<ptxt>Noise Occurrence Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Presumable Cause</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>AM</ptxt>
</entry>
<entry valign="middle">
<ptxt>Noise occurs in a specified area</ptxt>
</entry>
<entry valign="middle">
<ptxt>Foreign noise</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AM</ptxt>
</entry>
<entry valign="middle">
<ptxt>Noise occurs when listening to an intermittent broadcast</ptxt>
</entry>
<entry valign="middle">
<ptxt>An identical program transmitted from multiple towers can cause noise where the signals overlap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AM</ptxt>
</entry>
<entry valign="middle">
<ptxt>Noise occurs only at night</ptxt>
</entry>
<entry valign="middle">
<ptxt>Signal phasing</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FM</ptxt>
</entry>
<entry valign="middle">
<ptxt>Noise occurs while driving in a specified area</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multipath resulting from a change in FM frequency</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>