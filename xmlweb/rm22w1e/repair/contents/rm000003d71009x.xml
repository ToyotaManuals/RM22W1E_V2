<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3ML_T00FO" variety="T00FO">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV)</name>
<para id="RM000003D71009X" category="C" type-id="302I1" name-id="AT31C-05" from="201308">
<dtccode>P0500</dtccode>
<dtcname>Vehicle Speed Sensor "A"</dtcname>
<subpara id="RM000003D71009X_01" type-id="60" category="03" proc-id="RM22W0E___000084400001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The speed sensor detects the wheel speed and sends the appropriate signals to the skid control ECU. The skid control ECU converts these wheel speed signals into a 4-pulse signal and outputs it to the ECM via the combination meter. The ECM determines the vehicle speed based on the frequency of these pulse signals.</ptxt>
<figure>
<graphic graphicname="A115899E06" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0500</ptxt>
</entry>
<entry valign="middle">
<ptxt>While the vehicle is being driven, no vehicle speed sensor signal is transmitted to the ECM (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in speed signal circuit</ptxt>
</item>
<item>
<ptxt>Wheel speed sensor</ptxt>
</item>
<item>
<ptxt>Combination meter</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Skid control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003D71009X_02" type-id="64" category="03" proc-id="RM22W0E___000084500001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM assumes that the vehicle is being driven when the indicated vehicle speed is more than 9 km/h (5.6 mph). If there is no speed signal from the combination meter despite these conditions being met, the ECM interprets this as a malfunction in the speed signal circuit. The ECM then illuminates the MIL and stores the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000003D71009X_03" type-id="32" category="03" proc-id="RM22W0E___000084600001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C177226E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003D71009X_04" type-id="51" category="05" proc-id="RM22W0E___000084700001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000003D71009X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003D71009X_05_0001" proc-id="RM22W0E___000084800001">
<testtitle>CHECK OPERATION OF SPEEDOMETER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Drive the vehicle and check whether the operation of the speedometer in the combination meter is normal.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The vehicle speed sensor is operating normally if the speedometer reading is normal.</ptxt>
</item>
<item>
<ptxt>If the speedometer does not operate, check it by following the procedure described in Speedometer Malfunction (See page <xref label="Seep01" href="RM000002ZM601EX"/>).</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003D71009X_05_0002" fin="false">OK</down>
<right ref="RM000003D71009X_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D71009X_05_0002" proc-id="RM22W0E___000084900001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (VEHICLE SPD)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / ECT / Data List.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle.</ptxt>
</test1>
<test1>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
<table pgwide="1">
<title>ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle speed/</ptxt>
<ptxt>Min.: 0 km/h (0 mph)</ptxt>
<ptxt>Max.: 255 km/h (158 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual vehicle speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>The speed is indicated on the speedometer.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Vehicle speeds displayed on intelligent tester and speedometer display are equal.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003D71009X_05_0003" fin="true">OK</down>
<right ref="RM000003D71009X_05_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D71009X_05_0003">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D71009X_05_0004" proc-id="RM22W0E___000084A00001">
<testtitle>CHECK COMBINATION METER ASSEMBLY (+S VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C177448E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-4 (+S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003D71009X_05_0005" fin="false">OK</down>
<right ref="RM000003D71009X_05_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D71009X_05_0005" proc-id="RM22W0E___000084B00001">
<testtitle>CHECK COMBINATION METER ASSEMBLY (SPD SIGNAL WAVEFORM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A179960E02" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Move the shift lever to N.</ptxt>
</test1>
<test1>
<ptxt>Jack up the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-4 (+S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Wheel is turned slowly</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Voltage generated intermittently</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>The output voltage should fluctuate up and down, similarly to the diagram, when the wheel is turned slowly.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/ Multi-information Display)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/o Multi-information Display)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003D71009X_05_0006" fin="false">A</down>
<right ref="RM000003D71009X_05_0009" fin="true">B</right>
<right ref="RM000003D71009X_05_0011" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003D71009X_05_0006" proc-id="RM22W0E___000084C00001">
<testtitle>CHECK HARNESS AND CONNECTOR (COMBINATION METER - ECM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C179560E02" width="2.775699831in" height="5.787629434in"/>
</figure>
<test1>
<ptxt>Disconnect the combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-4 (+S) - A38-8 (SPD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E7-4 (+S) or A38-8 (SPD) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-4 (+S) - A52-8 (SPD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E7-4 (+S) or A52-8 (SPD) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003D71009X_05_0007" fin="true">OK</down>
<right ref="RM000003D71009X_05_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003D71009X_05_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329203AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D71009X_05_0008">
<testtitle>GO TO METER / GAUGE SYSTEM PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000003BBU00FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D71009X_05_0009">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM0000038ID00LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003D71009X_05_0010">
<testtitle>REPAIR HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003D71009X_05_0011">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM0000039M200BX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>