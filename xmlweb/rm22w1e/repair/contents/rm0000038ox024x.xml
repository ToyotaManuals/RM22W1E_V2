<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001M" variety="S001M">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001M_7C3PL_T00IO" variety="T00IO">
<name>BRAKE FLUID</name>
<para id="RM0000038OX024X" category="A" type-id="30019" name-id="BR3VJ-14" from="201308">
<name>REPLACEMENT</name>
<subpara id="RM0000038OX024X_01" type-id="11" category="10" proc-id="RM22W0E___0000ASS00001">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform brake fluid replacement with the shift lever in P*1 or neutral*2 and the parking brake applied.</ptxt>
<ptxt>*1: for Automatic Transmission</ptxt>
<ptxt>*2: for Manual Transmission</ptxt>
</item>
<item>
<ptxt>Perform brake fluid replacement while adding fluid to maintain the fluid level between the MIN and MAX lines of the reservoir.</ptxt>
</item>
<item>
<ptxt>As brake fluid may overflow when fluid is released from the brake actuator, do not leave the brake fluid can in the reservoir filler opening when adding brake fluid.</ptxt>
</item>
<item>
<ptxt>If the brake pedal is depressed with the reservoir cap removed, brake fluid may overflow.</ptxt>
</item>
<item>
<ptxt>Do not allow brake fluid to come into contact with any painted surface. If contact occurs, wash off the fluid immediately.</ptxt>
</item>
<item>
<ptxt>When the brake fluid is replaced, DTCs may be stored. Therefore, after fluid replacement, always clear the DTCs and check that a normal system code is output.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM0000038OX024X_02" type-id="01" category="01">
<s-1 id="RM0000038OX024X_02_0004" proc-id="RM22W0E___0000AST00001">
<ptxt>REPLACE BRAKE FLUID</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</s2>
<s2>
<ptxt>Remove the master cylinder reservoir filler cap assembly.</ptxt>
</s2>
<s2>
<ptxt>Add brake fluid until the fluid level is between the MIN and MAX lines of the reservoir.</ptxt>
</s2>
<s2>
<ptxt>While depressing the brake pedal, loosen the bleeder plug of the front disc brake cylinder RH, and then repeatedly depress the brake pedal.</ptxt>
</s2>
<s2>
<ptxt>Repeatedly depress the brake pedal until the air is completely bled, and then tighten the bleeder plug while depressing the brake pedal.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed the air from the bleeder plug of the front disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>With the brake pedal depressed, loosen the bleeder plug of the rear disc brake cylinder RH, continue to hold the brake pedal and allow brake fluid to be drained from the bleeder plug while the pump motor operates.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Air is bled as the pump motor operates while the brake pedal is being depressed</ptxt>
</item>
<item>
<ptxt>Be sure to release the brake pedal to stop the motor after approximately 100 seconds of continuous operation.</ptxt>
</item>
<item>
<ptxt>As brake fluid is continuously drained while the pump operates, it is not necessary to repeatedly depress the brake pedal.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Tighten the bleeder plug , and then release the brake pedal.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed the air from the bleeder plug of the rear disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch off.</ptxt>
</s2>
<s2>
<ptxt>Inspect for brake fluid leaks.</ptxt>
</s2>
<s2>
<ptxt>Check and adjust the brake fluid level (See page <xref label="Seep01" href="RM0000012XQ03DX"/>).</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>