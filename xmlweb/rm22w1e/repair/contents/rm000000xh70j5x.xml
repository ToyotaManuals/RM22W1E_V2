<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000XH70J5X" category="C" type-id="305UD" name-id="ES11HX-003" from="201308">
<dtccode>P0013</dtccode>
<dtcname>Camshaft Position "B" Actuator Circuit / Open (Bank 1)</dtcname>
<dtccode>P0023</dtccode>
<dtcname>Camshaft Position "B" Actuator Circuit / Open (Bank 2)</dtcname>
<subpara id="RM000000XH70J5X_01" type-id="60" category="03" proc-id="RM22W0E___00002DM00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>This DTC is designed to detect opens or shorts in the Camshaft Oil Control Valve (OCV) circuit. If the OCV duty-cycle is excessively high or low while the engine is running, the ECM will illuminate the MIL and store the DTC.</ptxt>
</item>
<item>
<ptxt>The VVT (variable valve timing) system adjusts the intake valve timing to improve the driveability. The engine oil pressure turns the camshaft actuator to adjust the valve timing. The OCV is a solenoid valve and switches the engine oil line. The valve moves when the ECM applies 12 volts to the solenoid. The ECM changes the energizing time to the solenoid (duty-cycle) in accordance with the camshaft position, crankshaft position, throttle position, etc.</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="A130637E09" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0013</ptxt>
</entry>
<entry valign="middle">
<ptxt>An open or short in the Camshaft Oil Control Valve (OCV) for exhaust side (for Bank 1) circuit (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in Camshaft Oil Control Valve (OCV) (for exhaust side of Bank 1) circuit</ptxt>
</item>
<item>
<ptxt>OCV (for exhaust side of Bank 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0023</ptxt>
</entry>
<entry valign="middle">
<ptxt>An open or short in the OCV for exhaust side (for Bank 2) circuit (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in OCV (for exhaust side of Bank 2) circuit</ptxt>
</item>
<item>
<ptxt>OCV (for exhaust side of Bank 2)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XH70J5X_02" type-id="64" category="03" proc-id="RM22W0E___00002DN00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is designed to detect opens or shorts in the camshaft oil control valve (OCV) circuit. If the OCV duty-cycle is excessively high or low while the engine is running, the ECM will illuminate the MIL and store the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000XH70J5X_07" type-id="32" category="03" proc-id="RM22W0E___00002DO00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A162759E13" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XH70J5X_08" type-id="51" category="05" proc-id="RM22W0E___00002DP00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTC P0013 is output, check the bank 1 VVT system for exhaust side circuit.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>If DTC P0023 is output, check the bank 2 VVT system for exhaust side circuit.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XH70J5X_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XH70J5X_09_0001" proc-id="RM22W0E___00002DQ00001">
<testtitle>CHECK DTC (DTC P0013 OR P0023)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear DTC after recording the freeze frame data and DTC.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Allow the engine to idle and check DTC.</ptxt>
</test1>
<test1>
<ptxt>Check that P0013 or P0023 is not output.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>P0013 or P0023 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XH70J5X_09_0002" fin="true">OK</down>
<right ref="RM000000XH70J5X_09_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XH70J5X_09_0003" proc-id="RM22W0E___00002DR00001">
<testtitle>INSPECT CAMSHAFT OIL CONTROL VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C125 or C126 OCV connector.</ptxt>
<figure>
<graphic graphicname="A160397E02" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Remove the camshaft oil control valve (OCV) (See page <xref label="Seep01" href="RM00000321401RX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry align="center">
<ptxt>6.9 to 7.9 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XH70J5X_09_0005" fin="false">OK</down>
<right ref="RM000000XH70J5X_09_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XH70J5X_09_0005" proc-id="RM22W0E___00002DS00001">
<testtitle>CHECK HARNESS AND CONNECTOR (OCV - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C125 or C126 OCV connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C45 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C125-1 - C45-56 (OE1+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C125-2 - C45-55 (OE1-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C126-1 - C45-50 (OE2+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C126-2 - C45-49 (OE2-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C125-1 or C45-56 (OE1+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C125-2 or C45-55 (OE1-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C126-1 or C45-50 (OE2+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C126-2 or C45-49 (OE2-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XH70J5X_09_0006" fin="true">OK</down>
<right ref="RM000000XH70J5X_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XH70J5X_09_0002">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ13NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XH70J5X_09_0004">
<testtitle>REPLACE CAMSHAFT OIL CONTROL VALVE ASSEMBLY<xref label="Seep01" href="RM00000321401RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XH70J5X_09_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XH70J5X_09_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292039X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>