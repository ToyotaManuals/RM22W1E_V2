<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12027_S001J" variety="S001J">
<name>REAR SUSPENSION</name>
<ttl id="12027_S001J_7C3P0_T00I3" variety="T00I3">
<name>REAR UPPER ARM</name>
<para id="RM000003BZE00IX" category="A" type-id="80001" name-id="RP1AT-02" from="201301">
<name>REMOVAL</name>
<subpara id="RM000003BZE00IX_01" type-id="11" category="10" proc-id="RM22W0E___0000A5V00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the RH side and LH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003BZE00IX_02" type-id="01" category="01">
<s-1 id="RM000003BZE00IX_02_0008" proc-id="RM22W0E___00009WH00000">
<ptxt>REMOVE STABILIZER CONTROL VALVE PROTECTOR (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp, and disconnect the connector from the protector.</ptxt>
<figure>
<graphic graphicname="C172095" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and protector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BZE00IX_02_0007" proc-id="RM22W0E___00009WN00000">
<ptxt>OPEN STABILIZER CONTROL WITH ACCUMULATOR HOUSING SHUTTER VALVE (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, loosen the lower and upper chamber shutter valves of the stabilizer control with accumulator housing 2.0 to 3.5 turns.</ptxt>
<figure>
<graphic graphicname="C175131E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When loosening a shutter valve, make sure that the end protrudes 2 to 3.5 mm (0.0787 to 0.137 in.) from the surface of the block, and do not turn the shutter valve any further.</ptxt>
</item>
<item>
<ptxt>Do not remove the shutter valves.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000003BZE00IX_02_0001">
<ptxt>REMOVE REAR WHEEL</ptxt>
</s-1>
<s-1 id="RM000003BZE00IX_02_0009">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL (w/ Active Height Control)</ptxt>
</s-1>
<s-1 id="RM000003BZE00IX_02_0010" proc-id="RM22W0E___00009Y200000">
<ptxt>REMOVE REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH (w/ Active Height Control)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C176365" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the connector and 2 clamps.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C172823" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Remove the 2 bolts, nut and sensor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BZE00IX_02_0002" proc-id="RM22W0E___0000A5W00000">
<ptxt>DISCONNECT SPEED SENSOR WIRE HARNESS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the speed sensor wire harness from the upper control arm.</ptxt>
<figure>
<graphic graphicname="C174362" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZE00IX_02_0003" proc-id="RM22W0E___0000A5X00000">
<ptxt>REMOVE REAR STABILIZER CONTROL TUBE INSULATOR (w/ KDSS)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and rear stabilizer control tube insulator.</ptxt>
<figure>
<graphic graphicname="C174360" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZE00IX_02_0011" proc-id="RM22W0E___0000A5Z00000">
<ptxt>REMOVE UPPER ARM BUSH HEAT INSULATOR (w/o KDSS)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and upper arm bush heat insulator.</ptxt>
<figure>
<graphic graphicname="C180039" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZE00IX_02_0006" proc-id="RM22W0E___0000A4K00000">
<ptxt>SUPPORT REAR AXLE HOUSING ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Support the rear axle housing with a jack using a wooden block to avoid damage.</ptxt>
<figure>
<graphic graphicname="C174050" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003BZE00IX_02_0012" proc-id="RM22W0E___0000A6000000">
<ptxt>REMOVE REAR UPPER CONTROL ARM ASSEMBLY LH (w/ Active Height Control)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts, 2 washers, 2 bolts and upper control arm.</ptxt>
<figure>
<graphic graphicname="C174363" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZE00IX_02_0005" proc-id="RM22W0E___0000A5Y00000">
<ptxt>REMOVE REAR UPPER CONTROL ARM ASSEMBLY LH (w/o Active Height Control)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts, 2 washers, 2 bolts and upper control arm.</ptxt>
<figure>
<graphic graphicname="C178733" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>