<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM0000018WX07PX" category="C" type-id="302IK" name-id="ESRAY-02" from="201301" to="201308">
<dtccode>P2120</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" Circuit</dtcname>
<dtccode>P2122</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" Circuit Low Input</dtcname>
<dtccode>P2123</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" Circuit High Input</dtcname>
<dtccode>P2125</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "E" Circuit</dtcname>
<dtccode>P2127</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "E" Circuit Low Input</dtcname>
<dtccode>P2128</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "E" Circuit High Input</dtcname>
<dtccode>P2138</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" / "E" Voltage Correlation</dtcname>
<subpara id="RM0000018WX07PX_01" type-id="60" category="03" proc-id="RM22W0E___00002NI00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>This is the repair procedure for the accelerator pedal position sensor.</ptxt>
</item>
<item>
<ptxt>This electrical throttle system does not use a throttle cable.</ptxt>
</item>
<item>
<ptxt>This accelerator pedal position sensor is a non-contact type.</ptxt>
</item>
</list1>
</atten4>
<ptxt>The accelerator pedal position sensor is mounted on the accelerator pedal and detects the opening angle of the accelerator pedal. Since this sensor is electronically controlled with Hall-effect elements, accurate control and reliability can be obtained. It has 2 sensors to detect the accelerator position and a malfunction of the accelerator pedal position sensor.</ptxt>
<ptxt>In the accelerator pedal position sensor, the voltage applied to pedal terminals VPA and VPA2 of the ECM changes between 0 V and 5 V in proportion to the opening angle of the accelerator pedal. The VPA is a signal to indicate the actual accelerator pedal opening angle which is used for the engine control, and the VPA2 is a signal to indicate information about the opening angle which is used for detecting malfunctions. The ECM judges the current opening angle of the accelerator pedal using signals from terminals VPA and VPA2, and the ECM controls the throttle motor based on these signals.</ptxt>
<figure>
<graphic graphicname="A129363E05" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<title>P2120</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON</ptxt>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal fully closed for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal open partway for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal fully open for 3 seconds</ptxt>
</item>
</list1>
</entry>
<entry>
<ptxt>Condition (a) continues for 0.5 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) VPA is less than 0.4 V and VPA2 is more than 0.97 deg, or VPA is more than 4.8 V.</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor</ptxt>
</item>
<item>
<ptxt>Accelerator pedal</ptxt>
</item>
<item>
<ptxt>Accelerator pedal rod (arm) deformed</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2122</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON</ptxt>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal fully closed for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal open partway for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal fully open for 3 seconds</ptxt>
</item>
</list1>
</entry>
<entry>
<ptxt>Condition (a) and (b) continue for 0.5 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) VPA is less than 0.4 V.</ptxt>
<ptxt>(b) VPA2 is more than 0.97 deg.</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor</ptxt>
</item>
<item>
<ptxt>Open in VCPA circuit</ptxt>
</item>
<item>
<ptxt>VPA circuit open or ground short</ptxt>
</item>
<item>
<ptxt>Accelerator pedal</ptxt>
</item>
<item>
<ptxt>Accelerator pedal rod (arm) deformed</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2123</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON</ptxt>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal fully closed for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal open partway for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal fully open for 3 seconds</ptxt>
</item>
</list1>
</entry>
<entry>
<ptxt>Condition (a) continues for 2.0 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) VPA is more than 4.8 V.</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor</ptxt>
</item>
<item>
<ptxt>Open in EPA circuit</ptxt>
</item>
<item>
<ptxt>Accelerator pedal</ptxt>
</item>
<item>
<ptxt>Accelerator pedal rod (arm) deformed</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2125</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON</ptxt>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal fully closed for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal open partway for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal fully open for 3 seconds</ptxt>
</item>
</list1>
</entry>
<entry>
<ptxt>Condition (a) continues for 0.5 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) VPA2 is less than 1.2 V and VPA is more than 2.7 deg, or VPA2 is more than 4.8 V and VPA is more than 0.4 V but less than 3.45 V.</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor</ptxt>
</item>
<item>
<ptxt>Accelerator pedal</ptxt>
</item>
<item>
<ptxt>Accelerator pedal rod (arm) deformed</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2127</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON</ptxt>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal fully closed for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal open partway for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal fully open for 3 seconds</ptxt>
</item>
</list1>
</entry>
<entry>
<ptxt>Condition (a) and (b) continue for 0.5 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) VPA2 is less than 1.2 V.</ptxt>
<ptxt>(b) VPA2 is more than 2.7 deg.</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor</ptxt>
</item>
<item>
<ptxt>Open in VCP2 circuit</ptxt>
</item>
<item>
<ptxt>VPA2 circuit open or ground short</ptxt>
</item>
<item>
<ptxt>Accelerator pedal</ptxt>
</item>
<item>
<ptxt>Accelerator pedal rod (arm) deformed</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2128</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON</ptxt>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal fully closed for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal open partway for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal fully open for 3 seconds</ptxt>
</item>
</list1>
</entry>
<entry>
<ptxt>Conditions (a) and (b) continue for 2.0 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) VPA2 is more than 4.8 V.</ptxt>
<ptxt>(b) VPA is more than 0.4 V but less than 3.45 V.</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal position sensor</ptxt>
</item>
<item>
<ptxt>Open in EPA2 circuit</ptxt>
</item>
<item>
<ptxt>Accelerator pedal</ptxt>
</item>
<item>
<ptxt>Accelerator pedal rod (arm) deformed</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2138</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON</ptxt>
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal fully closed for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal open partway for 3 seconds</ptxt>
</item>
<item>
<ptxt>Accelerator pedal fully open for 3 seconds</ptxt>
</item>
</list1>
</entry>
<entry>
<ptxt>Condition (a) or (b) continues for 2.0 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) The difference between VPA and VPA2 is less than 0.02 V.</ptxt>
<ptxt>(b) VPA is less than 0.4 V and VPA2 is less than 1.2 V.</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>VPA and VPA2 circuits are short-circuited</ptxt>
</item>
<item>
<ptxt>Accelerator pedal position sensor</ptxt>
</item>
<item>
<ptxt>Accelerator pedal</ptxt>
</item>
<item>
<ptxt>Accelerator pedal rod (arm) deformed</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>DTC No.</ptxt>
</entry>
<entry>
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P2120</ptxt>
</entry>
<entry morerows="6" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accel Sens. No.1 Volt %</ptxt>
</item>
<item>
<ptxt>Accel Sens. No.2 Volt %</ptxt>
</item>
<item>
<ptxt>Accel Sensor Out No.1</ptxt>
</item>
<item>
<ptxt>Accel Sensor Out No.2</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2122</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2123</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2125</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2127</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2128</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2138</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When DTC P2120, P2122, P2123, P2125, P2127, P2128 and/or P2138 is stored, check the output voltage of the accelerator pedal position sensor by entering the following menus: Powertrain / Engine / Data List / Accel Sensor Out No.1 and Accel Sensor Out No.2.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry morerows="2" valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
<entry namest="COL2" nameend="COL5" valign="middle">
<ptxt>Accelerator Pedal Position Expressed as Voltage Output</ptxt>
</entry>
</row>
<row>
<entry namest="COL2" nameend="COL3" valign="middle">
<ptxt>Accelerator Pedal Released</ptxt>
</entry>
<entry namest="COL4" nameend="COL5" valign="middle">
<ptxt>Accelerator Pedal Depressed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Accel Sensor Out No.1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sensor Out No.2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sensor Out No.1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sensor Out No.2</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>VC circuit open</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VPA circuit open or ground short</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.2 to 2.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.4 to 5.0 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VPA2 circuit open or ground short</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.5 to 1.1 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.9 to 3.6 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>EPA circuit open</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM0000018WX07PX_02" type-id="32" category="03" proc-id="RM22W0E___00002NJ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A149601E07" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000018WX07PX_03" type-id="51" category="05" proc-id="RM22W0E___00002NK00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07YX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06NX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000018WX07PX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000018WX07PX_04_0001" proc-id="RM22W0E___00002NL00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (ACCELERATOR PEDAL POSITION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
<figure>
<graphic graphicname="C129061E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / Accel Sens. No.1 Volt % and Accel Sens. No.2 Volt %.</ptxt>
</test1>
<test1>
<ptxt>Read the values.</ptxt>
<spec>
<title>Standard</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Accelerator Pedal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sens. No.1 Volt %</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sens. No.2 Volt %</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Released</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 to 22%</ptxt>
</entry>
<entry valign="middle">
<ptxt>24 to 40%</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>52 to 90%</ptxt>
</entry>
<entry valign="middle">
<ptxt>68 to 99%</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000018WX07PX_04_0002" fin="false">A</down>
<right ref="RM0000018WX07PX_04_0010" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM0000018WX07PX_04_0002" proc-id="RM22W0E___00002NM00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ACCELERATOR PEDAL POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the accelerator pedal position sensor connector.</ptxt>
<figure>
<graphic graphicname="A115665E35" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A31-1 (VCP2) - A38-56 (VCP2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-2 (EPA2) - A38-58 (EPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-3 (VPA2) - A38-54 (VPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-4 (VCPA) - A38-55 (VCPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-5 (EPA) - A38-57 (EPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-6 (VPA) - A38-53 (VPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A31-1 (VCP2) - A52-56 (VCP2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-2 (EPA2) - A52-58 (EPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-3 (VPA2) - A52-54 (VPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-4 (VCPA) - A52-55 (VCPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-5 (EPA) - A52-57 (EPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-6 (VPA) - A52-53 (VPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A31-1 (VCP2) or A38-56 (VCP2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-2 (EPA2) or A38-58 (EPA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-3 (VPA2) or A38-54 (VPA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-4 (VCPA) or A38-55 (VCPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-5 (EPA) or A38-57 (EPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-6 (VPA) or A38-53 (VPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A31-1 (VCP2) or A52-56 (VCP2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-2 (EPA2) or A52-58 (EPA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-3 (VPA2) or A52-54 (VPA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-4 (VCPA) or A52-55 (VCPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-5 (EPA) or A52-57 (EPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-6 (VPA) or A52-53 (VPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000018WX07PX_04_0003" fin="false">OK</down>
<right ref="RM0000018WX07PX_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018WX07PX_04_0003" proc-id="RM22W0E___00002NN00000">
<testtitle>INSPECT ECM TERMINAL VOLTAGE (VCPA AND VCP2 TERMINALS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the accelerator pedal position sensor connector.</ptxt>
<figure>
<graphic graphicname="A115666E65" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A31-4 (VCPA) - A31-5 (EPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A31-1 (VCP2) - A31-2 (EPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000018WX07PX_04_0004" fin="false">OK</down>
<right ref="RM0000018WX07PX_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018WX07PX_04_0004" proc-id="RM22W0E___00002NO00000">
<testtitle>REPLACE ACCELERATOR PEDAL ROD ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the accelerator pedal rod assembly (See page <xref label="Seep01" href="RM00000284A00GX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000018WX07PX_04_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018WX07PX_04_0005" proc-id="RM22W0E___00002NP00000">
<testtitle>CHECK IF OUTPUT DTC RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Fully close the accelerator pedal for 3 seconds, then hold it partway open for 3 seconds, then fully open it for 3 seconds.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="4.79in"/>
<colspec colname="COL2" colwidth="2.29in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P2120, P2122, P2123, P2125, P2127, P2128 and/or P2138 are output again</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000018WX07PX_04_0006" fin="false">A</down>
<right ref="RM0000018WX07PX_04_0008" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000018WX07PX_04_0006" proc-id="RM22W0E___00002NQ00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM0000018WX07PX_04_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM0000018WX07PX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<res>
<down ref="RM0000018WX07PX_04_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018WX07PX_04_0010" proc-id="RM22W0E___00002NR00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off and leave the vehicle for 15 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Fully close the accelerator pedal for 3 seconds, then hold it partway open for 3 seconds, then fully open it for 3 seconds.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the tester to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Powertrain / Engine / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P2120, P2122, P2123, P2125, P2127, P2128 and/or P2138.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or UNKNOWN, idle the engine.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000018WX07PX_04_0008" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018WX07PX_04_0008">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>