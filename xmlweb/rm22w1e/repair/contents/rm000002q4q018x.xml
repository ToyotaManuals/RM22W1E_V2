<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002Q4Q018X" category="C" type-id="803TK" name-id="ACBL9-02" from="201301" to="201308">
<dtccode>B1467/67</dtccode>
<dtcname>Rear Air Foot Duct Sensor Circuit on Driver Side</dtcname>
<subpara id="RM000002Q4Q018X_01" type-id="60" category="03" proc-id="RM22W0E___0000H7U00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The air duct temperature sensor LH*1 or RH*2 (for driver side) detects the duct temperature and sends the appropriate signals to the air conditioning amplifier assembly.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.29in"/>
<colspec colname="COL3" colwidth="2.43in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1467/67</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>An open or short in the air duct temperature sensor LH*1 or RH*2 circuit.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Air duct temperature sensor LH*1</ptxt>
</item>
<item>
<ptxt>Air duct temperature sensor RH*2</ptxt>
</item>
<item>
<ptxt>Harness or connector (air duct temperature sensor LH)*1</ptxt>
</item>
<item>
<ptxt>Harness or connector (air duct temperature sensor RH)*2</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002Q4Q018X_02" type-id="32" category="03" proc-id="RM22W0E___0000H7V00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E156584E28" width="7.106578999in" height="2.775699831in"/>
</figure>
<figure>
<graphic graphicname="E156584E29" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002Q4Q018X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002Q4Q018X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002Q4Q018X_04_0001" proc-id="RM22W0E___0000H7W00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (AIR DUCT TEMPERATURE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the air duct temperature sensor LH*1 or RH*2 (for driver side) is functioning properly. </ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Foot Duct Sensor (Rear D)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Min.: -12.7°C (9.14°F)</ptxt>
<ptxt>Max.: 76.55°C (169.79°F)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Actual air duct temperature sensor (for driver side) displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open in the circuit: -12.7°C (9.14°F)</ptxt>
<ptxt>Short in the circuit: 76.55°C (169.79°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002Q4Q018X_04_0005" fin="true">OK</down>
<right ref="RM000002Q4Q018X_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Q4Q018X_04_0003" proc-id="RM22W0E___0000H7X00000">
<testtitle>CHECK AIR DUCT TEMPERATURE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LHD</ptxt>
<figure>
<graphic graphicname="E160938E01" width="2.775699831in" height="6.791605969in"/>
</figure>
<test2>
<ptxt>Remove the air duct temperature sensor LH (See page <xref label="Seep01" href="RM000003B9O00LX"/>).</ptxt>
</test2>
</test1>
<test1>
<ptxt>for RHD</ptxt>
<test2>
<ptxt>Remove the air duct temperature sensor RH (See page <xref label="Seep02" href="RM000003B9O00LX"/>).</ptxt>
</test2>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry>
<ptxt>Tester Connection</ptxt>
</entry>
<entry>
<ptxt>Condition</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="10" valign="middle">
<ptxt>1 (+) - 2 (-)</ptxt>
</entry>
<entry>
<ptxt>at 10°C (50°F)</ptxt>
</entry>
<entry>
<ptxt>9.4 to 10.5 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>at 15°C (59°F)</ptxt>
</entry>
<entry>
<ptxt>7.5 to 8.3 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>at 20°C (68°F)</ptxt>
</entry>
<entry>
<ptxt>6.0 to 6.5 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>at 25°C (77°F)</ptxt>
</entry>
<entry>
<ptxt>4.5 to 5.2 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>at 30°C (86°F)</ptxt>
</entry>
<entry>
<ptxt>3.8 to 4.2 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>at 35°C (95°F)</ptxt>
</entry>
<entry>
<ptxt>3.1 to 3.4 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>at 40°C (104°F)</ptxt>
</entry>
<entry>
<ptxt>2.5 to 2.8 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>at 45°C (113°F)</ptxt>
</entry>
<entry>
<ptxt>2.0 to 2.3 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>at 50°C (122°F)</ptxt>
</entry>
<entry>
<ptxt>1.6 to 2.0 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>at 55°C (131°F)</ptxt>
</entry>
<entry>
<ptxt>1.3 to 1.6 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>at 60°C (140°F)</ptxt>
</entry>
<entry>
<ptxt>1.1 to 1.4 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Touching the sensor even slightly may change the resistance value. Hold the connector of the sensor.</ptxt>
</item>
<item>
<ptxt>When measuring the resistance, the sensor temperature must be the same as the ambient temperature.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>As the temperature increases, the resistance decreases (see the graph).</ptxt>
<ptxt>If the resistance value is not as specified, replace the sensor.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002Q4Q018X_04_0004" fin="false">A</down>
<right ref="RM000002Q4Q018X_04_0007" fin="true">B</right>
<right ref="RM000002Q4Q018X_04_0010" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002Q4Q018X_04_0004" proc-id="RM22W0E___0000H7Y00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - AIR DUCT TEMPERATURE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LHD</ptxt>
<figure>
<graphic graphicname="E156820E08" width="2.775699831in" height="5.787629434in"/>
</figure>
<test2>
<ptxt>Disconnect the K21 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E35 amplifier connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E35-25 (TR3) - K21-1 (+)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-34 (SGND) - K21-2 (-)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-25 (TR3) - Body ground </ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-34 (SGND) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD</ptxt>
<figure>
<graphic graphicname="E156820E09" width="2.775699831in" height="5.787629434in"/>
</figure>
<test2>
<ptxt>Disconnect the L12 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E35 amplifier connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E35-26 (TR4) - L12-1 (+)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-12 (SG-9) - L12-2 (-)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-26 (TR4) - Body ground </ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-12 (SG-9) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002Q4Q018X_04_0005" fin="true">OK</down>
<right ref="RM000002Q4Q018X_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Q4Q018X_04_0005">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Q4Q018X_04_0007">
<testtitle>REPLACE AIR DUCT TEMPERATURE SENSOR LH<xref label="Seep01" href="RM000003B9O00LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Q4Q018X_04_0010">
<testtitle>REPLACE AIR DUCT TEMPERATURE SENSOR RH<xref label="Seep01" href="RM000003B9O00LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Q4Q018X_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>