<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12037_S001R" variety="S001R">
<name>VARIABLE GEAR RATIO STEERING</name>
<ttl id="12037_S001R_7C3Q8_T00JB" variety="T00JB">
<name>VARIABLE GEAR RATIO STEERING SYSTEM</name>
<para id="RM000000YQL01XX" category="J" type-id="302PU" name-id="VG08U-01" from="201301" to="201308">
<dtccode/>
<dtcname>VGRS System Stops or does not Operate</dtcname>
<subpara id="RM000000YQL01XX_01" type-id="51" category="05" proc-id="RM22W0E___0000B7K00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>This problem may be caused by a malfunction in the vehicle stability control system or steering system.</ptxt>
<ptxt>Make sure that no DTC is output for the skid control ECU or steering control ECU before proceeding with the following procedures.</ptxt>
<ptxt>If a DTC is output for either of these ECUs, first troubleshoot it and then proceed to the troubleshooting below.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000YQL01XX_02" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YQL01XX_02_0001" proc-id="RM22W0E___0000B7L00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (RECORD OF OVERHEAT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the record of overheat is functioning properly.</ptxt>
<table pgwide="1">
<title>VGRS</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.46in"/>
<colspec colname="COL2" colwidth="2.25in"/>
<colspec colname="COL3" colwidth="1.78in"/>
<colspec colname="COLSPEC0" colwidth="1.59in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Motor Overheat Record</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Record of continuous overheat preventive control/Rec or Unrec</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rec or Unrec</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.74in"/>
<colspec colname="COL2" colwidth="1.39in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Rec" (Record of motor overheating)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"Unrec" (No record of motor overheating)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If "Rec" is displayed, reset the setting to "Unrec". Enter the VGRS menu on the tester, select record clearance, and follow the prompts to set to "Unrec".</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000YQL01XX_02_0002" fin="false">A</down>
<right ref="RM000000YQL01XX_02_0003" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0002" proc-id="RM22W0E___0000B7M00000">
<testtitle>CHECK HEAT PROTECTION FUNCTION FOR STEERING ACTUATOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the engine switch on (IG) and wait for 30 minutes until the steering actuator cools down.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and slowly turn the steering wheel from lock to lock (approximately 90°/sec.) 3 times.</ptxt>
<atten4>
<ptxt>If turning the steering wheel quickly while driving at low speeds, the steering angle may differ between the left and right, or the steering wheel may become off-center.</ptxt>
<ptxt>However, this is not a malfunction.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Check the number of turns from lock to lock.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.64in"/>
<colspec colname="COL2" colwidth="1.49in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3.2 turns</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2.7 turns</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YQL01XX_02_0003" fin="false">A</down>
<right ref="RM000000YQL01XX_02_0007" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0003" proc-id="RM22W0E___0000B7N00000">
<testtitle>CHECK STEERING EFFORT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the steering wheel.</ptxt>
</test1>
<test1>
<ptxt>Measure the steering effort.</ptxt>
<spec>
<title>Standard steering effort</title>
<specitem>
<ptxt>Below 6.9 N*m (70 kgf*cm, 61 in.*lbf)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>After completing the inspection or repair, attempt to simulate the problem and then perform "Read Value Using Intelligent Tester" above again to reconfirm that the problem does not occur.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<title>Result</title>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1VD-FTV with DPF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (except 1VD-FTV with DPF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YQL01XX_02_0004" fin="false">A</down>
<right ref="RM000000YQL01XX_02_0012" fin="true">B</right>
<right ref="RM000000YQL01XX_02_0008" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0004" proc-id="RM22W0E___0000B7O00000">
<testtitle>CHECK STEERING ACTUATOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for abnormal noise or vibration in the steering actuator.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Neither abnormal noise nor vibration occurs.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YQL01XX_02_0005" fin="false">OK</down>
<right ref="RM000000YQL01XX_02_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0005" proc-id="RM22W0E___0000B7P00000">
<testtitle>CHECK STEERING CONTROL ECU (POWER SOURCE CIRCUIT)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C149134E14" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.64in"/>
<colspec colname="COL2" colwidth="1.32in"/>
<colspec colname="COL3" colwidth="1.17in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E77-3 (IG) - </ptxt>
<ptxt>Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E77-8 (PIG) - </ptxt>
<ptxt>Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.63in"/>
<colspec colname="COL2" colwidth="1.29in"/>
<colspec colname="COL3" colwidth="1.21in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E77-7 (PGND) - </ptxt>
<ptxt>Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<title>Result</title>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YQL01XX_02_0010" fin="true">A</down>
<right ref="RM000000YQL01XX_02_0006" fin="true">B</right>
<right ref="RM000000YQL01XX_02_0011" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0006">
<testtitle>REPLACE STEERING CONTROL ECU<xref label="Seep01" href="RM000003DZD00EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0007">
<testtitle>VGRS SYSTEM IS NORMAL</testtitle>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0012">
<testtitle>GO TO POWER STEERING SYSTEM (PROBLEM SYMPTOMS TABLE)<xref label="Seep01" href="RM00000203M011X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0008">
<testtitle>GO TO POWER STEERING SYSTEM (PROBLEM SYMPTOMS TABLE)<xref label="Seep01" href="RM000002GX001NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0009">
<testtitle>GO TO STEERING WHEEL VIBRATES OR ABNORMAL NOISE IS HEARD FROM STEERING WHEEL<xref label="Seep01" href="RM000000YQO01TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0010">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YQL01XX_02_0011">
<testtitle>REPLACE STEERING CONTROL ECU<xref label="Seep01" href="RM000003EFD004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>