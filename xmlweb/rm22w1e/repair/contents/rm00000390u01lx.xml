<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UH_T00NK" variety="T00NK">
<name>FRONT SEAT ASSEMBLY (for Manual Seat)</name>
<para id="RM00000390U01LX" category="A" type-id="8000E" name-id="SEB0I-01" from="201301" to="201308">
<name>REASSEMBLY</name>
<subpara id="RM00000390U01LX_02" type-id="11" category="10" proc-id="RM22W0E___0000FRD00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000390U01LX_01" type-id="01" category="01">
<s-1 id="RM00000390U01LX_01_0002" proc-id="RM22W0E___0000FQV00000">
<ptxt>INSTALL FRONT OUTER SEAT TRACK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the spring to install the front outer seat track assembly LH to the seat track adjusting handle as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B293540" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0003" proc-id="RM22W0E___0000FQW00000">
<ptxt>INSTALL FRONT INNER SEAT TRACK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the spring to install the front inner seat track assembly LH to the seat track adjusting handle as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B293538" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for 4 Way Seat Type:</ptxt>
<s3>
<ptxt>Using a T40 "TORX" socket wrench, install the reinforcement with the 4 "TORX" bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B180773" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Temporarily install the seat frame with adjuster with 4 "TORX" bolts.</ptxt>
<figure>
<graphic graphicname="B180772" width="2.775699831in" height="3.779676365in"/>
</figure>
</s3>
<s3>
<ptxt>Temporarily install the inner seat track bracket with 2 "TORX" bolts.</ptxt>
<figure>
<graphic graphicname="B185504E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s3>
<s3>
<ptxt>Using a T40 "TORX" socket wrench , tighten the 6 "TORX" bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
<subtitle>Bolt A</subtitle>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
<s2>
<ptxt>for 8 Way Seat Type:</ptxt>
<s3>
<ptxt>Using a T40 "TORX" socket wrench , install the reinforcement with the 2 "TORX" bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B180777" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Temporarily install the seat frame with adjuster with the 4 "TORX" bolts.</ptxt>
<figure>
<graphic graphicname="B180776" width="2.775699831in" height="3.779676365in"/>
</figure>
</s3>
<s3>
<ptxt>Using a T40 "TORX" socket wrench, tighten the 4 "TORX" bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Install the spring and then using a T40 "TORX" socket wrench, install the "TORX" bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B185565" width="2.775699831in" height="3.779676365in"/>
</figure>
</s3>
<s3>
<ptxt>Using a T40 "TORX" socket wrench, install the inner seat track bracket with the 2 "TORX" bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Attach the spring.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0029" proc-id="RM22W0E___0000FRA00000">
<ptxt>INSTALL FRONT SEAT WIRE LH (w/ Wire Harness)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the front seat wire LH.</ptxt>
</s2>
<s2>
<ptxt>Attach the wire harness clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0034" proc-id="RM22W0E___0000FIN00000">
<ptxt>INSTALL FRONT SEAT AIRBAG ASSEMBLY LH (w/ Front Seat Side Airbag)
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Make sure that the seat frame assembly is not deformed. If it is, replace it with a new one.</ptxt>
</atten2>
<s2>
<ptxt>Install the front seat airbag assembly with 2 nuts.</ptxt>
<figure>
<graphic graphicname="B102198" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 2 clamps to connect the wire harness.</ptxt>
</s2>
<s2>
<ptxt>Attach the 5 clamps to connect the wire harness.</ptxt>
<figure>
<graphic graphicname="B102194" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01LX_01_0032" proc-id="RM22W0E___0000FLG00000">
<ptxt>INSTALL SEAT POSITION SENSOR (w/ Front Seat Side Airbag)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the ignition switch is off.</ptxt>
</s2>
<s2>
<ptxt>Check that the cable is disconnected from the battery negative (-) terminal.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Using a 1.0 mm (0.0393 in.) feeler gauge, install the seat position sensor.</ptxt>
<figure>
<graphic graphicname="C107067E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>If the seat position sensor has been dropped, or there are any cracks, dents or other defects in the case or connector, replace the seat position sensor with a new one.</ptxt>
</item>
<item>
<ptxt>When installing the seat position sensor, be careful that the SRS wiring does not interfere with other parts and that it is not pinched between other parts.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Be sure that the clearance between the seat position sensor and the seat rail is within 0.6 to 1.4 mm (0.0236 to 0.0551 in.).</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using a T30 "TORX" socket wrench, install the seat position sensor with the screw.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Make sure that the clearance between the seat position sensor and the seat rail is within 0.6 to 1.4 mm (0.0236 to 0.0551 in.).</ptxt>
</s2>
<s2>
<ptxt>Check that there is no looseness in the installation parts of the seat position sensor.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Install the seat slide position sensor protector to the seat position sensor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01LX_01_0005" proc-id="RM22W0E___0000FQY00000">
<ptxt>INSTALL FRONT LOWER SEAT CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180778" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the front lower seat cushion shield LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0004" proc-id="RM22W0E___0000FQX00000">
<ptxt>INSTALL FRONT LOWER SEAT CUSHION SHIELD RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the front lower seat cushion shield RH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0006" proc-id="RM22W0E___0000FQZ00000">
<ptxt>INSTALL UPPER ACTIVE HEADREST UNIT (w/ Active Headrest)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180727" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the cable clamp to install the upper active headrest unit to the lower active headrest unit.</ptxt>
</s2>
<s2>
<ptxt>Install the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0007" proc-id="RM22W0E___0000FR000000">
<ptxt>INSTALL LOWER ACTIVE HEADREST UNIT (w/ Active Headrest)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180726" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install the lower active headrest unit together with the upper active headrest unit with the 6 nuts.</ptxt>
<torque>
<subtitle>Upper active headrest unit</subtitle>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
<subtitle>Lower active headrest unit</subtitle>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0008" proc-id="RM22W0E___0000FR100000">
<ptxt>INSTALL LUMBAR SUPPORT ADJUSTER ASSEMBLY LH (w/Lumbar Support)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws to install the bush to the seat frame.</ptxt>
<figure>
<graphic graphicname="B180725" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the lumbar support adjuster assembly LH with the 2 screws.</ptxt>
<figure>
<graphic graphicname="B180724" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the lumbar support adjuster connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 hooks to install the seatback spring.</ptxt>
<figure>
<graphic graphicname="B293541E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Active Headrest:</ptxt>
<ptxt>Connect the active headrest cable with the cable clamp.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 springs.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0010" proc-id="RM22W0E___0000FR300000">
<ptxt>INSTALL OUTSIDE RECLINING ADJUSTER COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180769" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the outside reclining adjuster cover LH with the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0009" proc-id="RM22W0E___0000FR200000">
<ptxt>INSTALL OUTSIDE RECLINING ADJUSTER COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the outside reclining adjuster cover RH with the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0012" proc-id="RM22W0E___0000FR500000">
<ptxt>INSTALL INSIDE RECLINING ADJUSTER COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180770" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the inside reclining adjuster cover LH.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
<s2>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0011" proc-id="RM22W0E___0000FR400000">
<ptxt>INSTALL INSIDE RECLINING ADJUSTER COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the inside reclining adjuster cover RH.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
<s2>
<ptxt>w/ Lumbar Support:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0014" proc-id="RM22W0E___0000FR600000">
<ptxt>INSTALL FRONT SEPARATE TYPE SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the fastening tape to install the front separate type seatback cover to the front separate type seatback pad.</ptxt>
<figure>
<graphic graphicname="B180730E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Using hog ring pliers, install new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<ptxt>Connect the seatback cover bracket to the front separate type seatback pad.</ptxt>
<figure>
<graphic graphicname="B180731" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0015" proc-id="RM22W0E___0000FIP00000">
<ptxt>INSTALL SEATBACK COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the seatback cover with pad in place.</ptxt>
</s2>
<s2>
<ptxt>w/ Seatback Board:</ptxt>
<s3>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<ptxt>Install the seatback cover bracket to the seat frame with the nut.</ptxt>
<figure>
<graphic graphicname="B180716" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
<atten2>
<ptxt>After the seatback cover with pad is installed, make sure the seatback cover bracket is not twisted.</ptxt>
</atten2>
</s3>
<s3>
<figure>
<graphic graphicname="B191295E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Attach the 9 hooks.</ptxt>
</s3>
<s3>
<ptxt>Using hog ring pliers, install new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
<s2>
<ptxt>w/o Seatback Board:</ptxt>
<s3>
<ptxt>Attach the 4 claws to install the 2 headrest supports.</ptxt>
<figure>
<graphic graphicname="B180768" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<ptxt>Install the seatback cover bracket to the seat frame with the nut.</ptxt>
<figure>
<graphic graphicname="B180716" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
<atten2>
<ptxt>After the seatback cover with pad is installed, make sure the seatback cover bracket is not twisted.</ptxt>
</atten2>
</s3>
<s3>
<ptxt>Close the 2 fasteners.</ptxt>
<figure>
<graphic graphicname="B185566" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Using hog ring pliers, install new hog rings.</ptxt>
<figure>
<graphic graphicname="B180766" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0016" proc-id="RM22W0E___0000FIQ00000">
<ptxt>INSTALL FRONT SEAT HEADREST SUPPORT (w/ Active Headrest)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180712" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the 2 front seat headrest supports.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0017" proc-id="RM22W0E___0000FIR00000">
<ptxt>INSTALL FRONT SEATBACK BOARD SUB-ASSEMBLY LH (w/ Seatback Board)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182646" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Move the front seatback board sub-assembly in the direction of the arrow to attach the 2 hooks and install it.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0019" proc-id="RM22W0E___0000FR700000">
<ptxt>INSTALL FRONT SEPARATE TYPE SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180733E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using hog ring pliers, install the front separate type seat cushion cover to the front separate type seat cushion pad with new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0020" proc-id="RM22W0E___0000FR800000">
<ptxt>INSTALL SEAT CUSHION COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the hooks to install the seat cushion cover with pad.</ptxt>
<figure>
<graphic graphicname="B180764" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Bend the 2 seat frame claws downward.</ptxt>
</s2>
<s2>
<ptxt>w/ Occupant Detection Sensor:</ptxt>
<ptxt>Attach the 2 clamps to install connector holder.</ptxt>
<figure>
<graphic graphicname="B180765" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Lumbar Support:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="B180763" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 2 clips.</ptxt>
<figure>
<graphic graphicname="B180762" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0022" proc-id="RM22W0E___0000FIS00000">
<ptxt>INSTALL FRONT INNER SEAT CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180761" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw and clip to install the front inner seat cushion shield LH.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0033" proc-id="RM22W0E___0000G9F00000">
<ptxt>INSTALL FRONT SEAT INNER BELT ASSEMBLY LH (for Manual Seat)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B189679E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install the front seat inner belt assembly with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not allow the anchor part of the front seat inner belt assembly to overlap the protruding part of the front seat adjuster.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the connectors and attach the clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01LX_01_0035" proc-id="RM22W0E___0000FRC00000">
<ptxt>INSTALL FRONT SEAT INNER BELT ASSEMBLY LH (for Bench Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B189679E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install the front seat inner belt assembly LH with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not allow the anchor part of the front seat inner belt assembly to overlap the protruding part of the front seat.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the connectors and attach the clamps.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Protruding Part</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01LX_01_0024" proc-id="RM22W0E___0000FR900000">
<ptxt>INSTALL LUMBAR SWITCH (w/ Lumbar Support)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180760" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the lumbar switch with the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0026" proc-id="RM22W0E___0000FIT00000">
<ptxt>INSTALL FRONT SEAT CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Lumbar Support:</ptxt>
<ptxt>Connect the lumbar switch connector.</ptxt>
</s2>
<s2>
<ptxt>for 4 Way Seat Type:</ptxt>
<s3>
<ptxt>Attach the 3 claws in the order shown in the illustration to install the front seat cushion shield LH.</ptxt>
<figure>
<graphic graphicname="B180780E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Install the 2 screws.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 8 Way Seat Type:</ptxt>
<s3>
<ptxt>Attach the 3 claws in the order shown in the illustration to install the front seat cushion shield LH.</ptxt>
<figure>
<graphic graphicname="B180786E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Install the 3 screws.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Seatback Board:</ptxt>
<ptxt>Connect the 2 rubber bands to the 2 hooks.</ptxt>
<figure>
<graphic graphicname="B180757" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Seatback Board:</ptxt>
<ptxt>Connect the rubber band to the 2 hooks.</ptxt>
<figure>
<graphic graphicname="B180756" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0027" proc-id="RM22W0E___0000FIU00000">
<ptxt>INSTALL RECLINING ADJUSTER RELEASE HANDLE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185571" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the reclining adjuster release handle LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0028" proc-id="RM22W0E___0000FIV00000">
<ptxt>INSTALL VERTICAL SEAT ADJUSTER KNOB (for 8 Way Seat Type)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180781" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the snap ring to the vertical seat adjuster knob.</ptxt>
</s2>
<s2>
<ptxt>Install the vertical seat adjuster knob.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0030" proc-id="RM22W0E___0000FIW00000">
<ptxt>INSTALL VERTICAL ADJUSTING HANDLE LH (for 8 Way Seat Type)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180753" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the vertical adjusting handle with the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01LX_01_0031" proc-id="RM22W0E___0000FIX00000">
<ptxt>INSTALL VERTICAL ADJUSTER COVER LH (for 8 Way Seat Type)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180752" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the vertical adjuster cover LH.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>