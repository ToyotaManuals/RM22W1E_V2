<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZC_T00SF" variety="T00SF">
<name>LIGHTING SYSTEM</name>
<para id="RM0000011TZ085X" category="J" type-id="3017J" name-id="LE63Z-01" from="201301" to="201308">
<dtccode/>
<dtcname>Headlight Leveling ECU Power Source Circuit</dtcname>
<subpara id="RM0000011TZ085X_01" type-id="60" category="03" proc-id="RM22W0E___0000J6300000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit detects the state of the ignition switch, and sends it to the headlight leveling ECU*1 or headlight swivel ECU*2.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Static Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM0000011TZ085X_02" type-id="32" category="03" proc-id="RM22W0E___0000J6400000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E227418E05" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="E227418E07" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000011TZ085X_03" type-id="51" category="05" proc-id="RM22W0E___0000J6500000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the headlight leveling ECU*1 or headlight swivel ECU*2, initialization of the ECU is necessary (except w/ Dynamic Headlight Auto Leveling) (See page <xref label="Seep01" href="RM000002CE4030X"/>).</ptxt>
</item>
</list1>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Static Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM0000011TZ085X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000011TZ085X_05_0028" proc-id="RM22W0E___0000J6800000">
<testtitle>CHECK VEHICLE TYPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the vehicle type.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>w/ Static Headlight Auto Leveling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/ Dynamic Headlight Auto Leveling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011TZ085X_05_0023" fin="false">A</down>
<right ref="RM0000011TZ085X_05_0029" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM0000011TZ085X_05_0023" proc-id="RM22W0E___0000J6700000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (IGNITION POWER SUPPLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000002WL5020X"/>).</ptxt>
<table pgwide="1">
<title>HL Auto Leveling</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>+B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition power supply voltage value / 0 to 19.75 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition column.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011TZ085X_05_0021" fin="true">OK</down>
<right ref="RM0000011TZ085X_05_0019" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011TZ085X_05_0019" proc-id="RM22W0E___0000J6600000">
<testtitle>CHECK HARNESS AND CONNECTOR (HEADLIGHT LEVELING ECU ASSEMBLY - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the headlight leveling ECU connector.</ptxt>
<figure>
<graphic graphicname="E136327E50" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E109-1 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E109-9 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Headlight Leveling ECU Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM0000011TZ085X_05_0025" fin="true">OK</down>
<right ref="RM0000011TZ085X_05_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011TZ085X_05_0029" proc-id="RM22W0E___0000J6900000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (IGNITION POWER SUPPLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000002WL5020X"/>).</ptxt>
<table pgwide="1">
<title>AFS</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>+B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition power supply voltage value / 0 to 19.75 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition column.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011TZ085X_05_0021" fin="true">OK</down>
<right ref="RM0000011TZ085X_05_0030" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011TZ085X_05_0030" proc-id="RM22W0E___0000J6A00000">
<testtitle>CHECK HARNESS AND CONNECTOR (HEADLIGHT SWIVEL ECU ASSEMBLY - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the headlight leveling ECU connector.</ptxt>
<figure>
<graphic graphicname="E131580E40" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E101-15 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E101-22 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Headlight Swivel ECU Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM0000011TZ085X_05_0031" fin="true">A</down>
<right ref="RM0000011TZ085X_05_0032" fin="true">B</right>
<right ref="RM0000011TZ085X_05_0022" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000011TZ085X_05_0022">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000011TZ085X_05_0021">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002WKO02DX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011TZ085X_05_0025">
<testtitle>REPLACE HEADLIGHT LEVELING ECU ASSEMBLY<xref label="Seep01" href="RM000003AV9009X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011TZ085X_05_0031">
<testtitle>REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY<xref label="Seep01" href="RM000003A5S005X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011TZ085X_05_0032">
<testtitle>REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY<xref label="Seep01" href="RM000003AV9009X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>