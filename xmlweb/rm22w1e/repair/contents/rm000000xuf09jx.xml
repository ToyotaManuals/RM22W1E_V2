<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T9_T00MC" variety="T00MC">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000000XUF09JX" category="U" type-id="303FJ" name-id="DL002L-114" from="201301">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM000000XUF09JX_z0" proc-id="RM22W0E___0000EMR00000">
<content5 releasenbr="1">
<step1>
<ptxt>READ DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / (desired system) / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Entry &amp; Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Power Save Cnt 10 Min.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power save counter - 10 minutes / Min: 0, Max: 225</ptxt>
</entry>
<entry valign="middle">
<ptxt>Within range from 0 to 225</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Power Save Cnt 9 Days</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power save counter - 9 days / Min: 0, Max: 225</ptxt>
</entry>
<entry valign="middle">
<ptxt>Within range from 0 to 225</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Power Save Cnt 14 Days</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power save counter - 14 days / Min: 0, Max: 225</ptxt>
</entry>
<entry valign="middle">
<ptxt>Within range from 0 to 225</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Power Save Cnt 30 Days</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power save counter - 30 days / Min: 0, Max: 225</ptxt>
</entry>
<entry valign="middle">
<ptxt>Within range from 0 to 225</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Unmatched Vehicle-ID</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No communication condition between vehicle and electrical key transmitter (vehicle-ID) / Yes or No</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yes: Key ID code not matched to vehicle</ptxt>
<ptxt>No: Key ID code matched to vehicle</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No Response</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No communication condition between vehicle and electrical key transmitter (response) / Yes or No</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yes: Key ID code not matched to vehicle</ptxt>
<ptxt>No: Key ID code matched to vehicle</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Unmatch Code or Form</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No communication condition between vehicle and electrical key transmitter (response code and format) / Yes or No</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yes: Key ID code not matched to vehicle</ptxt>
<ptxt>No: Key ID code matched to vehicle</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Key Low Battery</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key low battery / Yes or No</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yes: Key battery voltage low</ptxt>
<ptxt>No: Key battery voltage normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ID Code Difference(Resp)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No communication condition between vehicle and electrical key transmitter (ID code) / Yes or No</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yes: Key ID code not matched to vehicle</ptxt>
<ptxt>No: Key ID code matched to vehicle</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C Code Difference</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No communication condition between vehicle and electrical key transmitter (challenge code) / Yes or No</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yes: Key ID code not matched to vehicle</ptxt>
<ptxt>No: Key ID code matched to vehicle</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Unlock Mode</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door unlock mode / All, Each, D_Door, Side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mode status displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Auto Entry Cancel SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Entry door lock function cancel / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mode status displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Trunk Open Mode</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Luggage compartment door open mode with vehicle locked / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Customization status displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Available Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition available area / Front or All</ptxt>
</entry>
<entry valign="middle">
<ptxt>Customization status displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>D-Door Touch Sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side door touch sensor / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side door sensor touched</ptxt>
<ptxt>OFF: Driver side door sensor not touched</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>D-Door Trigger Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side door outside handle lock switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side door outside handle lock switch pushed</ptxt>
<ptxt>OFF: Driver side door outside handle lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P-Door Touch Sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side door touch sensor / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front passenger side door sensor touched</ptxt>
<ptxt>OFF: Front passenger side door sensor not touched</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P-Door Trigger Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side door outside handle lock switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front passenger side door outside handle lock switch pushed</ptxt>
<ptxt>OFF: Front passenger side door outside handle lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Dr-Door Touch Sensor*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side rear door touch sensor / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side rear door sensor touched</ptxt>
<ptxt>OFF: Driver side rear door sensor not touched</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Dr-Door Trigger Switch*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side rear door outside handle lock switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side rear door outside handle lock switch pushed</ptxt>
<ptxt>OFF: Driver side rear door outside handle lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Pr-Door Touch Sensor*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Passenger side rear door touch sensor / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Passenger side rear door sensor touched</ptxt>
<ptxt>OFF: Passenger side rear door sensor not touched</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Pr-Door Trigger Switch*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Passenger side rear door outside handle lock switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Passenger side rear door outside handle lock switch pushed</ptxt>
<ptxt>OFF: Passenger side rear door outside handle lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tr/B-Door Lock SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door lock switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Back door lock switch pushed</ptxt>
<ptxt>OFF: Back door lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: For vehicles with entry function for rear doors.</ptxt>
</atten4>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>IG SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG) / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Engine switch on (IG)</ptxt>
<ptxt>OFF: Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ACC SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (ACC) / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Engine switch on (ACC)</ptxt>
<ptxt>OFF: Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>D Door Courtesy SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side door opened</ptxt>
<ptxt>OFF: Driver side door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P Door Courtesy SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front passenger side door opened</ptxt>
<ptxt>OFF: Front passenger side door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL Door Courtesy SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear LH side door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Rear side door LH opened</ptxt>
<ptxt>OFF: Rear side door LH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RR Door Courtesy SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear RH side door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Rear side door RH opened</ptxt>
<ptxt>OFF: Rear side door RH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>D Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side door unlocked</ptxt>
<ptxt>OFF: Driver side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front passenger side door unlocked</ptxt>
<ptxt>OFF: Front passenger side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DR Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side rear door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side rear door unlocked</ptxt>
<ptxt>OFF: Driver side rear door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PR Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Passenger side rear door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Passenger side rear door unlocked</ptxt>
<ptxt>OFF: Passenger side rear door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Handle SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door opener switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Back door opener switch pushed</ptxt>
<ptxt>OFF: Back door opener switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Courtesy SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Back door opened</ptxt>
<ptxt>OFF: Back door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Back Door (w/ Power Back Door System)</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Door Lock Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door lock status signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Back door unlocked</ptxt>
<ptxt>OFF: Back door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>PERFORM ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / Entry &amp; Start / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, perform the Active Test.</ptxt>
<table pgwide="1">
<title>Entry &amp; Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Overhead Tuner Power Supply ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door control receiver</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>D-Seat Outside Transmitter</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door electrical key oscillator (for Driver Side)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P-Seat Outside Transmitter</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door electrical key oscillator (for Front Passenger Side)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DR-Seat Outside Transmitter*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door electrical key oscillator (for Driver Side Rear)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PR-Seat Outside Transmitter*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door electrical key oscillator (for Passenger Side Rear)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fr-Seat Inside Transmitter</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Indoor electrical key antenna (for Front Floor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rr-Seat Inside Transmitter</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Indoor electrical key antenna (for Rear 1 Floor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Inside Trunk/Backdoor Transmitter</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Indoor electrical key antenna (for Rear 2 Floor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Outside Trunk/Backdoor Transmitter</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door electrical key antenna (for Outer)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>D-Seat Select</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side select</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P-Seat Select</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side select</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Dr-Seat Select*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side rear select</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Pr-Seat Select*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Passenger side rear select</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless Buzzer Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wireless buzzer</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: For vehicles with entry function for rear doors.</ptxt>
</atten4>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>