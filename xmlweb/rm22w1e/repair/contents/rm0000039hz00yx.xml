<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12062_S002E" variety="S002E">
<name>POWER OUTLETS (INT)</name>
<ttl id="12062_S002E_7C6P2_T00U5" variety="T00U5">
<name>VOLTAGE INVERTER</name>
<para id="RM0000039HZ00YX" category="A" type-id="80001" name-id="PI0VB-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000039HZ00YX_01" type-id="01" category="01">
<s-1 id="RM0000039HZ00YX_01_0014" proc-id="RM22W0E___0000K1O00001">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY (except Face to Face Seat Type)</ptxt>
<content1 releasenbr="1">
<ptxt>Remove the rear No. 2 seat assembly (See page <xref label="Seep01" href="RM00000391S00ZX"/>).</ptxt>
</content1>
</s-1>
<s-1 id="RM0000039HZ00YX_01_0015" proc-id="RM22W0E___0000K1P00001">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY (for Face to Face Seat Type)</ptxt>
<content1 releasenbr="1">
<ptxt>Remove the rear No. 2 seat assembly (See page <xref label="Seep01" href="RM00000311C003X"/>).</ptxt>
</content1>
</s-1>
<s-1 id="RM0000039HZ00YX_01_0006" proc-id="RM22W0E___000068I00001">
<ptxt>REMOVE REAR STEP COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181673" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to remove the step cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 2 claws and remove the step cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039HZ00YX_01_0007" proc-id="RM22W0E___000068J00001">
<ptxt>REMOVE REAR DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181674" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws and 4 clips, and remove the scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039HZ00YX_01_0008" proc-id="RM22W0E___000068L00001">
<ptxt>REMOVE REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 6 clips and remove the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039HZ00YX_01_0012" proc-id="RM22W0E___000068M00001">
<ptxt>REMOVE REAR SEAT COVER CAP (except Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190187E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to remove the rear seat cover cap on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 3 claws and remove the rear seat cover cap.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039HZ00YX_01_0011" proc-id="RM22W0E___0000FL100001">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182644" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When removing the front quarter trim panel, operate the reclining adjuster release handle and move the No. 1 rear seat to the position shown in the illustration.</ptxt>
</atten4>
<s2>
<figure>
<graphic graphicname="B181687" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 3 claws and remove the cover.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B181689" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and rear No. 1 seat belt anchor.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B181691" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>w/ Rear No. 2 Seat, except Face to Face Seat Type:</ptxt>
<ptxt>Remove the bolt and rear No. 2 seat belt anchor.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear No. 2 Seat, except Face to Face Seat Type:</ptxt>
<s3>
<ptxt>Remove the clip and bolt.</ptxt>
</s3>
<s3>
<ptxt>Detach the 18 clips and 2 claws.</ptxt>
</s3>
<s3>
<ptxt>w/o Rear Air Conditioning System:</ptxt>
<ptxt>Disconnect the rear seat lock control lever cable and then remove the quarter trim panel.</ptxt>
</s3>
<s3>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<ptxt>Disconnect the thermistor connector and rear seat lock control lever cable, and then remove the quarter trim panel.</ptxt>
<figure>
<graphic graphicname="B186889" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Rear No. 2 Seat or w/ Rear No. 2 Seat, for Face to Face Seat Type:</ptxt>
<s3>
<ptxt>Remove the clip.</ptxt>
</s3>
<s3>
<ptxt>Detach the 18 clips and 2 claws, and remove the quarter trim panel.</ptxt>
<figure>
<graphic graphicname="B190221" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000039HZ00YX_01_0005" proc-id="RM22W0E___0000JWS00001">
<ptxt>REMOVE VOLTAGE INVERTER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B343188" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and voltage inverter.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>