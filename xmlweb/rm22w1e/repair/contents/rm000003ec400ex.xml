<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3Y1_T00R4" variety="T00R4">
<name>POWER BACK DOOR SYSTEM</name>
<para id="RM000003EC400EX" category="C" type-id="303GM" name-id="DH04S-07" from="201308">
<dtccode>B2222</dtccode>
<dtcname>PBD Pulse Sensor Malfunction</dtcname>
<subpara id="RM000003EC400EX_01" type-id="60" category="03" proc-id="RM22W0E___0000IMU00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>A pulse sensor is built into the power back door unit assembly (power back door ECU) to detect foreign objects and the back door position. The pulse sensor monitors the operating speed of the back door while the power back door is in operation to detect foreign objects. The pulse sensor also monitors where the back door is to detect the back door position. If a pulse signal that is out of the normal range is output, the power back door unit assembly (power back door ECU) will set DTC B2222.</ptxt>
</item>
<item>
<ptxt>If DTC B2222 is set, the power back door system will be turned off. Thus, the back door will be switched to manual operation mode (not electrically controlled) and can be moved freely.</ptxt>
</item>
<item>
<ptxt>In order to restore the power back door system to normal operation mode, first solve the problem indicated by DTC B2222 and then manually close the back door fully (reset operation).</ptxt>
</item>
</list1>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B2222</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Power back door unit does not operate</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Power back door unit assembly (power back door ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003EC400EX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003EC400EX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003EC400EX_03_0001" proc-id="RM22W0E___0000IMV00001">
<testtitle>CHECK DTC OUTPUT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000003EBY008X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000003EBY008X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2222 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003EC400EX_03_0002" fin="true">OK</down>
<right ref="RM000003EC400EX_03_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003EC400EX_03_0002">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003EC400EX_03_0003">
<testtitle>REPLACE POWER BACK DOOR UNIT ASSEMBLY (POWER BACK DOOR ECU)<xref label="Seep01" href="RM0000039HB01DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>