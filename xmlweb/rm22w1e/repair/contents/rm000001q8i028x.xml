<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001M" variety="S001M">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001M_7C3PM_T00IP" variety="T00IP">
<name>BRAKE PEDAL (for LHD)</name>
<para id="RM000001Q8I028X" category="A" type-id="80001" name-id="BR69Y-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM000001Q8I028X_02" type-id="01" category="01">
<s-1 id="RM000001Q8I028X_02_0048" proc-id="RM22W0E___0000ATF00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001Q8I028X_02_0047" proc-id="RM22W0E___0000ATE00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001Q8I028X_02_0045" proc-id="RM22W0E___0000ATD00000">
<ptxt>REMOVE FRONT DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181682" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 7 claws and 4 clips, and remove the scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8I028X_02_0042" proc-id="RM22W0E___0000A9R00000">
<ptxt>REMOVE COWL SIDE TRIM BOARD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181911" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the cap nut.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clips and remove the cowl side trim board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8I028X_02_0037" proc-id="RM22W0E___000014A00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180655" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the No. 1 instrument panel under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8I028X_02_0028" proc-id="RM22W0E___0000A9S00000">
<ptxt>REMOVE LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180295E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the hole cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 16 claws.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 9 claws.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Detach the 2 claws and remove the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Detach the 2 claws and disconnect the 2 control cables.</ptxt>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the lower No. 1 instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8I028X_02_0044" proc-id="RM22W0E___0000ATC00000">
<ptxt>REMOVE DRIVER SIDE KNEE AIRBAG ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and driver side knee airbag.</ptxt>
<figure>
<graphic graphicname="B189602E01" width="2.775699831in" height="6.791605969in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8I028X_02_0022" proc-id="RM22W0E___0000AT900000">
<ptxt>REMOVE PUSH ROD PIN</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip and push rod pin from the brake pedal lever.</ptxt>
<figure>
<graphic graphicname="C177105E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q8I028X_02_0038" proc-id="RM22W0E___0000ATB00000">
<ptxt>REMOVE STOP LIGHT SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the stop light switch connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the stop light switch (See page <xref label="Seep01" href="RM0000038XP00HX_01_0001"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q8I028X_02_0024" proc-id="RM22W0E___0000ATA00000">
<ptxt>REMOVE BRAKE PEDAL SUPPORT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the hexagon bolt.</ptxt>
<figure>
<graphic graphicname="C229381E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>Hold the bolt in place and loosen the nut.</ptxt>
</atten4>
<atten3>
<ptxt>Only loosen the nut. Do not remove the bolt or nut.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the brake pedal support reinforcement set bolt.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 nuts and brake pedal support assembly.</ptxt>
<figure>
<graphic graphicname="C177106E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>