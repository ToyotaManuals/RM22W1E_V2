<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZC_T00SF" variety="T00SF">
<name>LIGHTING SYSTEM</name>
<para id="RM000002ZS0027X" category="J" type-id="3011B" name-id="LE63Y-02" from="201308">
<dtccode/>
<dtcname>Door Courtesy Switch Circuit</dtcname>
<subpara id="RM000002ZS0027X_01" type-id="60" category="03" proc-id="RM22W0E___0000J3300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The main body ECU receives a door open/closed signal from each door courtesy light switch.</ptxt>
</content5>
</subpara>
<subpara id="RM000002ZS0027X_02" type-id="32" category="03" proc-id="RM22W0E___0000J3400001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E246968E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002ZS0027X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002ZS0027X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002ZS0027X_05_0001" proc-id="RM22W0E___0000J3500001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR COURTESY LIGHT SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000002WL502AX"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>D Door Courtesy SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door courtesy light switch LH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front door courtesy light switch LH on</ptxt>
<ptxt>OFF: Front door courtesy light switch LH off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P Door Courtesy SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door courtesy light switch RH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front door courtesy light switch RH on</ptxt>
<ptxt>OFF: Front door courtesy light switch RH off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL Door Courtesy SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear door courtesy light switch LH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Rear door courtesy light switch LH on</ptxt>
<ptxt>OFF: Rear door courtesy light switch LH off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RR Door Courtesy SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear door courtesy light switch RH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Rear door courtesy light switch RH on</ptxt>
<ptxt>OFF: Rear door courtesy light switch RH off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Courtesy SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Back door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Back door courtesy light switch on</ptxt>
<ptxt>OFF: Back door courtesy light switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Door courtesy light switch on/off</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0016" fin="true">OK</down>
<right ref="RM000002ZS0027X_05_0017" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0017" proc-id="RM22W0E___0000J3E00001">
<testtitle>CHECK DOOR COURTESY LIGHT SWITCH</testtitle>
<content6 releasenbr="1">
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Front door courtesy light switch LH does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front door courtesy light switch RH does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear door courtesy light switch LH does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear door courtesy light switch RH does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Back door courtesy light switch does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0003" fin="false">A</down>
<right ref="RM000002ZS0027X_05_0007" fin="false">B</right>
<right ref="RM000002ZS0027X_05_0010" fin="false">C</right>
<right ref="RM000002ZS0027X_05_0013" fin="false">D</right>
<right ref="RM000002ZS0027X_05_0021" fin="false">E</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0003" proc-id="RM22W0E___0000J3600001">
<testtitle>INSPECT FRONT DOOR COURTESY LIGHT SWITCH LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the front door courtesy light switch LH (See page <xref label="Seep02" href="RM0000038W500FX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the front door courtesy light switch LH (See page <xref label="Seep01" href="RM0000038W300FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0005" fin="false">OK</down>
<right ref="RM000002ZS0027X_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0005" proc-id="RM22W0E___0000J3700001">
<testtitle>CHECK HARNESS AND CONNECTOR (FRONT DOOR COURTESY LIGHT SWITCH LH - MAIN BODY ECU [COWL SIDE JUNCTION BLOCK LH])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the K2 front door courtesy light switch LH connector.</ptxt>
</test1>
<test1>
<ptxt>for LHD:</ptxt>
<test2>
<ptxt>Disconnect the E1 main body ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K2-1 - E1-24 (DCTY)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K2-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD:</ptxt>
<test2>
<ptxt>Disconnect the E2 main body ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K2-1 - E2-21 (PCTY)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K2-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0002" fin="true">OK</down>
<right ref="RM000002ZS0027X_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0007" proc-id="RM22W0E___0000J3800001">
<testtitle>INSPECT FRONT DOOR COURTESY LIGHT SWITCH RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the front door courtesy light switch RH (See page <xref label="Seep02" href="RM0000038W500FX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the front door courtesy light switch RH (See page <xref label="Seep01" href="RM0000038W300FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0009" fin="false">OK</down>
<right ref="RM000002ZS0027X_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0009" proc-id="RM22W0E___0000J3900001">
<testtitle>CHECK HARNESS AND CONNECTOR (FRONT DOOR COURTESY LIGHT SWITCH RH - MAIN BODY ECU [COWL SIDE JUNCTION BLOCK LH])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the L1 front door courtesy light switch RH switch connector.</ptxt>
</test1>
<test1>
<ptxt>for LHD:</ptxt>
<test2>
<ptxt>Disconnect the E2 main body ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>L1-1 - E2-21 (PCTY)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L1-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD:</ptxt>
<test2>
<ptxt>Disconnect the E1 main body ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry>
<ptxt>Tester Connection</ptxt>
</entry>
<entry>
<ptxt>Condition</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>L1-1 - E1-24 (DCTY)</ptxt>
</entry>
<entry>
<ptxt>Always</ptxt>
</entry>
<entry>
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>L1-1 - Body ground</ptxt>
</entry>
<entry>
<ptxt>Always</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0002" fin="true">OK</down>
<right ref="RM000002ZS0027X_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0010" proc-id="RM22W0E___0000J3A00001">
<testtitle>INSPECT REAR DOOR COURTESY LIGHT SWITCH LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear door courtesy light switch LH (See page <xref label="Seep02" href="RM0000038W100FX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the rear door courtesy light switch LH (See page <xref label="Seep01" href="RM0000038VZ00FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0012" fin="false">OK</down>
<right ref="RM000002ZS0027X_05_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0012" proc-id="RM22W0E___0000J3B00001">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR DOOR COURTESY LIGHT SWITCH LH - MAIN BODY ECU [COWL SIDE JUNCTION BLOCK LH])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the 2C main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the K3 rear door courtesy light switch LH connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K3-1 - 2C-2 (LCTY)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K3-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0002" fin="true">OK</down>
<right ref="RM000002ZS0027X_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0013" proc-id="RM22W0E___0000J3C00001">
<testtitle>INSPECT REAR DOOR COURTESY LIGHT SWITCH RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear door courtesy light switch RH (See page <xref label="Seep02" href="RM0000038W100FX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the rear door courtesy light switch RH (See page <xref label="Seep01" href="RM0000038VZ00FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0015" fin="false">OK</down>
<right ref="RM000002ZS0027X_05_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0015" proc-id="RM22W0E___0000J3D00001">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR DOOR COURTESY LIGHT SWITCH RH - MAIN BODY ECU [COWL SIDE JUNCTION BLOCK LH])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the L2 rear door courtesy light switch RH connector.</ptxt>
</test1>
<test1>
<ptxt>except Double Swing Out Type:</ptxt>
<test2>
<ptxt>Disconnect the E2 main body ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>L2-1 - E2-7 (RCTY)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L2-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for Double Swing Out Type:</ptxt>
<test2>
<ptxt>Disconnect the E1 main body ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>L2-1 - E1-17 (RCTY)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L2-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0002" fin="true">OK</down>
<right ref="RM000002ZS0027X_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0021" proc-id="RM22W0E___0000J3H00001">
<testtitle>INSPECT VEHICLE TYPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the vehicle type.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>for Double Swing Out Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>except Double Swing Out Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0022" fin="false">A</down>
<right ref="RM000002ZS0027X_05_0018" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0022" proc-id="RM22W0E___0000J3I00001">
<testtitle>INSPECT BACK DOOR WITH MOTOR LOCK ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the back door with motor lock assembly (See page <xref label="Seep02" href="RM000004XHH001X"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the back door with motor lock assembly (See page <xref label="Seep01" href="RM000004XHF001X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0023" fin="false">OK</down>
<right ref="RM000002ZS0027X_05_0024" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0023" proc-id="RM22W0E___0000J3J00001">
<testtitle>CHECK HARNESS AND CONNECTOR (BACK DOOR WITH MOTOR LOCK ASSEMBLY - MAIN BODY ECU [COWL SIDE JUNCTION BLOCK LH] AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect E2 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect W3 back door with motor lock connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>W3-2 (+) - E2-25 (BCTY)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W3-1 (E) - Body ground</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>W3-2 (+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0002" fin="true">OK</down>
<right ref="RM000002ZS0027X_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0018" proc-id="RM22W0E___0000J3F00001">
<testtitle>INSPECT BACK DOOR LOCK ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the back door lock (See page <xref label="Seep02" href="RM0000039H800ZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the back door lock (See page <xref label="Seep01" href="RM000001VYW00WX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0020" fin="false">OK</down>
<right ref="RM000002ZS0027X_05_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0020" proc-id="RM22W0E___0000J3G00001">
<testtitle>CHECK HARNESS AND CONNECTOR (BACK DOOR LOCK ASSEMBLY - MAIN BODY ECU [COWL SIDE JUNCTION BLOCK LH] AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E2 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>w/o Power Back Door:</ptxt>
<test2>
<ptxt>Disconnect the S1 back door lock connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>S1-3 (+) - E2-25 (BCTY)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S1-1 (ACT-) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S1-3 (+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>w/ Power Back Door:</ptxt>
<test2>
<ptxt>Disconnect the S22 back door lock connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>S22-3 (CTY) - E2-25 (BCTY)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S22-4 (E) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S22-3 (CTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002ZS0027X_05_0002" fin="true">OK</down>
<right ref="RM000002ZS0027X_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0016">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002WKO02QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0004">
<testtitle>REPLACE FRONT DOOR COURTESY LIGHT SWITCH LH<xref label="Seep01" href="RM0000038W500FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0002">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0008">
<testtitle>REPLACE FRONT DOOR COURTESY LIGHT SWITCH RH<xref label="Seep01" href="RM0000038W500FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0011">
<testtitle>REPLACE REAR DOOR COURTESY LIGHT SWITCH LH<xref label="Seep01" href="RM0000038W100FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0014">
<testtitle>REPLACE REAR DOOR COURTESY LIGHT SWITCH RH<xref label="Seep01" href="RM0000038W100FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0024">
<testtitle>REPLACE BACK DOOR WITH MOTOR LOCK ASSEMBLY<xref label="Seep01" href="RM000004XHH001X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZS0027X_05_0019">
<testtitle>REPLACE BACK DOOR LOCK ASSEMBLY<xref label="Seep01" href="RM0000039H800ZX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>