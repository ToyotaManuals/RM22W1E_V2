<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM00000188C08OX" category="C" type-id="303H6" name-id="ES17HG-001" from="201308">
<dtccode>P1271</dtccode>
<dtcname>Fuel Regulator Circuit Malfunction (EDU Drive)</dtcname>
<dtccode>P1272</dtccode>
<dtcname>Fuel Pressure Regulator Malfunction</dtcname>
<subpara id="RM00000188C08OX_01" type-id="60" category="03" proc-id="RM22W0E___00003LG00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM controls the internal fuel pressure of the common rail assembly by opening and closing the pressure discharge valve. When sudden deceleration occurs, the internal fuel pressure will temporarily become higher than usual and combustion noise may result. Therefore the ECM will open the valve temporarily to discharge the excess pressure inside the common rail assembly. Also, the pressure discharge valve opens when the engine switch is turned off to allow prompt discharge of the common rail internal pressure and so that the engine stops smoothly.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>For more information on the pressure discharge valve and common rail system, refer to System Description (See page <xref label="Seep01" href="RM000000XSN037X"/>).</ptxt>
</item>
<item>
<ptxt>For more information on the injector driver, refer to System Description (See page <xref label="Seep02" href="RM000000XSN037X"/>).</ptxt>
</item>
<item>
<ptxt>If P1271 and/or P1272 is present, refer to the DTC chart for the fuel system (See page <xref label="Seep03" href="RM000000XSN037X"/>).</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="A208468E03" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="A205138E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>P1271</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) to OFF</ptxt>
<ptxt>and</ptxt>
<ptxt>Drive the vehicle at 50 km/h (31 mph) in third gear, and then decelerate by releasing the accelerator pedal</ptxt>
</entry>
<entry valign="middle">
<ptxt>When either condition below is met a certain number of times (0.5 seconds) (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>Open or short in pressure discharge valve circuit.</ptxt>
</item>
<item>
<ptxt>There is no valve opening confirmation (IJf) signal from the injector driver to the ECM for a specific number of times consecutively despite the ECM sending valve opening command (PRD) signals after the engine is started.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in pressure discharge valve circuit</ptxt>
</item>
<item>
<ptxt>Pressure discharge valve (common rail assembly (for bank 2))</ptxt>
</item>
<item>
<ptxt>No. 1 injector driver</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P1272</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) to OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressure discharge valve does not open.</ptxt>
<ptxt>Actual pressure decreasing rate deviates from the simulated pressure decreasing rate after the ignition switch is turned off (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in pressure discharge valve circuit (P1271 is stored simultaneously)</ptxt>
</item>
<item>
<ptxt>Pressure discharge valve (common rail assembly (for bank 2))</ptxt>
</item>
<item>
<ptxt>No. 1 injector driver</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P1271</ptxt>
<ptxt>P1272</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Common Rail Pressure</ptxt>
</item>
<item>
<ptxt>Target Common Rail Pressure</ptxt>
</item>
<item>
<ptxt>Pressure Discharge Valve</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>After confirming DTC P1271 and/or P1272, check the fuel pressure inside the common rail by entering the following menus: Engine and ECT / Data List / Common Rail Pressure.</ptxt>
</item>
<item>
<ptxt>P1271:</ptxt>
<ptxt>After clearing the DTC, drive the vehicle at 50 km/h (31 mph) in third gear, and then decelerate by releasing the accelerator pedal. Check that P1271 is not output.</ptxt>
</item>
<item>
<ptxt>P1272:</ptxt>
<ptxt>After clearing the DTC, start and stop the engine twice (wait 30 seconds or more after turning the ignition switch off before starting the engine the second time), and then confirm that P1272 is not stored.</ptxt>
</item>
</list1>
<table pgwide="1">
<title>Reference</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Engine Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail Pressure (kPa)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idling</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Common Rail Pressure is within 5000 kPa of Target Common Rail Pressure</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2500 rpm (No engine load)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Results of Real-vehicle Check</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Common Rail Pressure (Idling)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Common Rail Pressure (1 Second after Engine Stopped)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Pressure discharge valve normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>35000 kPa</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>500 kPa</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Open in pressure discharge valve circuit</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>35000 kPa</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>30000 kPa</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188C08OX_02" type-id="64" category="03" proc-id="RM22W0E___00003LH00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<topic>
<title>P1271 (Open or short in pressure discharge valve circuit):</title>
<ptxt>This DTC will be stored if there is no valve opening confirmation (IJf) signal sent from the injector driver to the ECM despite the ECM commanding the injector driver to open the pressure discharge valve. This DTC refers to an open or short circuit malfunction of the pressure discharge valve circuit, not a malfunction in which a valve is stuck open or closed.</ptxt>
<ptxt>The injector driver monitors the current supplied to the pressure discharge valve to verify that the current flows into the valve. If the current exceeds the specified level, the injector driver interprets this as the IJf signal being low. If this DTC is stored, the ECM enters fail-safe mode and limits engine power. The ECM continues operating in fail-safe mode until the engine switch is turned off.</ptxt>
<figure>
<graphic graphicname="A208469E05" width="7.106578999in" height="3.779676365in"/>
</figure>
</topic>
<topic>
<title>P1272 (Pressure discharge valve stuck closed):</title>
<ptxt>The pressure discharge valve will open and discharge the internal fuel pressure from the common rail to the fuel tank when the engine switch is turned off. In this event, the ECM compares the actual drop rate of the internal fuel pressure and the target drop rate. If the ECM judges that the actual drop rate is smaller than the target, the ECM then determines that the valve is stuck closed and stores this DTC. This DTC will be stored if the internal fuel pressure does not drop to the target after the engine switch has been turned off.</ptxt>
<ptxt>If this DTC is stored, the ECM enters fail-safe mode and limits engine power. The ECM continues operating in fail-safe mode until the engine switch is turned off.</ptxt>
</topic>
</content5>
</subpara>
<subpara id="RM00000188C08OX_03" type-id="65" category="03" proc-id="RM22W0E___00003LI00001">
<name>MONITOR STRATEGY</name>
<content5 releasenbr="1">
<table pgwide="1">
<title>P1271</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<tbody>
<row>
<entry>
<ptxt>Required sensors</ptxt>
</entry>
<entry>
<ptxt>Injector driver</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Frequency of operation</ptxt>
</entry>
<entry>
<ptxt>Continuous</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Duration</ptxt>
</entry>
<entry>
<ptxt>3 seconds</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>MIL operation</ptxt>
</entry>
<entry>
<ptxt>1 driving cycle</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P1272</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<tbody>
<row>
<entry>
<ptxt>Required sensors</ptxt>
</entry>
<entry>
<ptxt>Fuel pressure sensor</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Frequency of operation</ptxt>
</entry>
<entry>
<ptxt>Once per driving cycle</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Duration</ptxt>
</entry>
<entry>
<ptxt>1 second</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>MIL operation</ptxt>
</entry>
<entry>
<ptxt>2 driving cycles</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000188C08OX_04" type-id="66" category="03" proc-id="RM22W0E___00003LJ00001">
<name>TYPICAL ENABLING CONDITIONS</name>
<content5 releasenbr="1">
<table pgwide="1">
<title>P1271</title>
<tgroup cols="1">
<colspec colname="COL1" colwidth="7.08in"/>
<thead>
<row>
<entry align="center">
<ptxt>Specification</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>The vehicle is driven at 50 km/h (31 mph) in 3rd gear, and then decelerated by complete release of the accelerator pedal 3 times.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P1272</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry namest="COL2" nameend="COL3" valign="middle" align="center">
<ptxt>Specification</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Minimum</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Maximum</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Common Rail Pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>30000 kPa (306 kgf/cm<sup>2</sup>, 4350 psi)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Fuel temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0°C (32°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Battery voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL3">
<ptxt>The monitor will not run if the fuel pressure sensor, pressure discharge valve circuit or fuel temperature sensor is malfunctioning.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000188C08OX_05" type-id="67" category="03" proc-id="RM22W0E___00003LK00001">
<name>TYPICAL MALFUNCTION THRESHOLDS</name>
<content5 releasenbr="1">
<table pgwide="1">
<title>P1271</title>
<tgroup cols="1">
<colspec colname="COL1" colwidth="7.08in"/>
<thead>
<row>
<entry align="center">
<ptxt>Specification</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>There are no confirmation signals from the injector driver for a specific number of times consecutively, despite the ECM sending command signals regularly during deceleration.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P1272</title>
<tgroup cols="1">
<colspec colname="COL1" colwidth="7.08in"/>
<thead>
<row>
<entry align="center">
<ptxt>Specification</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>The internal pressure stays beyond the specified level after the engine switch is turned off.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000188C08OX_06" type-id="32" category="03" proc-id="RM22W0E___00003LL00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A301619E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="unordered">
<item>
<ptxt>Refer to DTC P0201 (See page <xref label="Seep01" href="RM00000187V07MX_06"/>).</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM00000188C08OX_07" type-id="51" category="05" proc-id="RM22W0E___00003LM00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08XX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK08XX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>After completing repairs, confirm that P1271 and/or P1272 does not recur.</ptxt>
</item>
<item>
<ptxt>If P062D (No. 1 injector driver) and P1271 are present simultaneously, there is an open in the INJF wire harness between the injector driver and ECM, or there is an open in the wire harness for both an injector assembly and the pressure discharge valve.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188C08OX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000188C08OX_08_0001" proc-id="RM22W0E___00003LN00001">
<testtitle>CHECK IF DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK189X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at 50 km/h (31 mph) in 3rd gear, and then decelerate by completely releasing the accelerator pedal. Perform this 3 times.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the pending DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P1272 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P1271 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P1271 and P1272 are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000188C08OX_08_0015" fin="false">A</down>
<right ref="RM00000188C08OX_08_0002" fin="false">B</right>
<right ref="RM00000188C08OX_08_0016" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0015" proc-id="RM22W0E___00003M000001">
<testtitle>PERFORM ACTIVE TEST USING GTS (PRESSURE DISCHARGE VALVE CHECK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Utility / Pressure Discharge Valve Check.</ptxt>
</test1>
<test1>
<ptxt>Select the check type for "Valve Check for Graph".</ptxt>
</test1>
<test1>
<ptxt>Press "Start" and perform "Pressure Discharge Valve Check".</ptxt>
<atten4>
<ptxt>For more information about "Pressure Discharge Valve Check / Valve Check for Graph", refer to System Check (See page <xref label="Seep01" href="RM0000012X107TX"/>).</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Check the engine speed and fuel pressure when the pressure discharge valve is turned from off to on.</ptxt>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>Pressure Discharge Valve</ptxt>
</entry>
<entry>
<ptxt>OFF</ptxt>
</entry>
<entry>
<ptxt>ON</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Engine Speed</ptxt>
</entry>
<entry>
<ptxt>Idling: 550 to 650 rpm</ptxt>
</entry>
<entry>
<ptxt>Changes to 0 rpm</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Fuel Pressure</ptxt>
</entry>
<entry>
<ptxt>Idling: 27000 to 49000 kPa</ptxt>
</entry>
<entry>
<ptxt>Changes to 0 kPa</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>When the system is normal, the following changes will be displayed on the GTS.</ptxt>
<figure>
<graphic graphicname="A267387E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</atten4>
</test1>
</content6>
<res>
<right ref="RM00000188C08OX_08_0012" fin="false">OK</right>
<right ref="RM00000188C08OX_08_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0002" proc-id="RM22W0E___00003LO00001">
<testtitle>INSPECT COMMON RAIL ASSEMBLY (FOR BANK 2) (PRESSURE DISCHARGE VALVE RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the common rail assembly (for bank 2) (See page <xref label="Seep01" href="RM0000031EN004X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188C08OX_08_0003" fin="false">OK</down>
<right ref="RM00000188C08OX_08_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0003" proc-id="RM22W0E___00003LP00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PRESSURE DISCHARGE VALVE - NO. 1 INJECTOR DRIVER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 injector driver connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the pressure discharge valve connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>z68-2 (COM3) - C153-2 (RLF+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z68-1 (RLV) - C153-1 (RLF-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z68-2 (COM3) or C153-2 (RLF+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z68-1 (RLV) or C153-1 (RLF-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the No.1 injector driver connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the pressure discharge valve connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188C08OX_08_0004" fin="false">OK</down>
<right ref="RM00000188C08OX_08_0011" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0004" proc-id="RM22W0E___00003LQ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 INJECTOR DRIVER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 injector driver connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C152-4 (PRD) - C45-54 (PRD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C152-1 (INJF) - C45-61 (INJ1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C152-4 (PRD) or C45-54 (PRD) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C152-1 (INJF) or C45-61 (INJ1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C152-4 (PRD) - C46-54 (PRD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C152-1 (INJF) - C46-61 (INJ1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C152-4 (PRD) or C46-54 (PRD) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C152-1 (INJF) or C46-61 (INJ1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the No. 1 injector driver connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188C08OX_08_0005" fin="false">OK</down>
<right ref="RM00000188C08OX_08_0011" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0005" proc-id="RM22W0E___00003LR00001">
<testtitle>INSPECT NO. 1 INJECTOR DRIVER (POWER SOURCE CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 injector driver connector.</ptxt>
<figure>
<graphic graphicname="A204910E24" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C150-3 (+B) - C150-1 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 1 injector Driver)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the No. 1 injector driver connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188C08OX_08_0007" fin="false">OK</down>
<right ref="RM00000188C08OX_08_0014" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0007" proc-id="RM22W0E___00003LS00001">
<testtitle>REPLACE NO. 1 INJECTOR DRIVER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the No. 1 injector driver (See page <xref label="Seep01" href="RM000003AJ9005X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188C08OX_08_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0009" proc-id="RM22W0E___00003LU00001">
<testtitle>CHECK IF DTC OUTPUT RECURS (DTC P1271 OR P1272)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK189X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at 50 km/h (31 mph) in 3rd gear, and then decelerate by completely releasing the accelerator pedal. Perform this 3 times.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If no pending DTCs are output, proceed to the next step to check "All Readiness".</ptxt>
</item>
<item>
<ptxt>Perform the following procedure using the GTS to determine whether or not the DTC judgment has been carried out.</ptxt>
</item>
</list1>
</atten4>
<test2>
<ptxt>Enter the following menus: Engine and ECT / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P1271 and P1272.</ptxt>
</test2>
<test2>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3" align="center">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COLSPEC1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Pending DTC</ptxt>
</entry>
<entry valign="middle">
<ptxt>P1271 or P1272 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>All Readiness</ptxt>
</entry>
<entry valign="middle">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If STATUS is INCOMPLETE or N/A, perform the following procedure 3 times: drive the vehicle at 50 km/h (31 mph) in 3rd gear, and then decelerate by completely releasing the accelerator pedal.</ptxt>
</atten4>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000188C08OX_08_0006" fin="true">A</down>
<right ref="RM00000188C08OX_08_0013" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0011" proc-id="RM22W0E___00003LW00001">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188C08OX_08_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0014" proc-id="RM22W0E___00003LZ00001">
<testtitle>GO TO INJECTOR CIRCUIT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Go to injector circuit (See page <xref label="Seep01" href="RM000002V5Q06YX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188C08OX_08_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0013" proc-id="RM22W0E___00003LY00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329203AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188C08OX_08_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0008" proc-id="RM22W0E___00003LT00001">
<testtitle>REPLACE COMMON RAIL ASSEMBLY (FOR BANK 2) (PRESSURE DISCHARGE VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the common rail assembly (for bank 2) (See page <xref label="Seep01" href="RM0000031FK00ZX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188C08OX_08_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0010" proc-id="RM22W0E___00003LV00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM0000031ED006X_01_0003"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform PM forced regeneration (See page <xref label="Seep02" href="RM00000141502YX_01_0014"/>).</ptxt>
<atten4>
<ptxt>When fuel lines are disconnected, air may enter the fuel lines, leading to engine starting trouble. Therefore, perform forced regeneration and bleed the air from the fuel lines.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000188C08OX_08_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0012" proc-id="RM22W0E___00003LX00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK189X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at 50 km/h (31 mph) in 3rd gear, and then decelerate by completely releasing the accelerator pedal. Perform this 3 times.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the pending DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the GTS to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Engine and ECT / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P1271 and P1272.</ptxt>
</test2>
<test2>
<ptxt>Check the DTC judgment result.</ptxt>
<atten4>
<ptxt>If STATUS is INCOMPLETE or N/A, perform the following procedure 3 times: drive the vehicle at 50 km/h (31 mph) in 3rd gear, and then decelerate by completely releasing the accelerator pedal.</ptxt>
</atten4>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000188C08OX_08_0006" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188C08OX_08_0006">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM00000188C08OX_08_0016">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW065X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>