<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000001D6V0PRX" category="J" type-id="305BG" name-id="ES11JA-002" from="201301">
<dtccode/>
<dtcname>VC Output Circuit</dtcname>
<subpara id="RM000001D6V0PRX_01" type-id="60" category="03" proc-id="RM22W0E___00000SI00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM constantly generates a 5 V power source voltage from the battery voltage supplied to the +B (BATT) terminal to operate the microprocessor. The ECM also provides this power source voltage to the sensors through the VC output circuit.</ptxt>
<figure>
<graphic graphicname="A116143E65" width="7.106578999in" height="3.779676365in"/>
</figure>
<ptxt>When the VC circuit is short-circuited, the microprocessor in the ECM and the sensors that are supplied with power through the VC circuit are inactivated because the power is not supplied from the VC circuit. Under this condition, the system does not start up and the MIL does not illuminate even if the system malfunctions.</ptxt>
<atten4>
<ptxt>Under normal conditions, the MIL is illuminated for several seconds when the ignition switch is first turned to ON. The MIL goes off when the engine is started.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001D6V0PRX_02" type-id="32" category="03" proc-id="RM22W0E___00000SJ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A299800E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="A266671E01" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001D6V0PRX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001D6V0PRX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001D6V0PRX_05_0001" proc-id="RM22W0E___00000SK00000">
<testtitle>CHECK MIL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the Malfunction Indicator Lamp (MIL) lights up when turning the ignition switch to ON.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0002" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0009" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0002" proc-id="RM22W0E___00000SL00000">
<testtitle>CHECK COMMUNICATION BETWEEN GTS AND ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Check the communication between the GTS and ECM.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Communication is possible</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Communication is not possible</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0004" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0010" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0004" proc-id="RM22W0E___00000SM00000">
<testtitle>CHECK MIL (THROTTLE BODY WITH MOTOR ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle body with motor assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0005" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0011" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0005" proc-id="RM22W0E___00000SN00000">
<testtitle>CHECK MIL (ACCELERATOR PEDAL SENSOR ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the accelerator pedal sensor assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0017" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0012" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0017" proc-id="RM22W0E___00000SP00000">
<testtitle>CHECK MIL (NO. 1 EMISSION CONTROL VALVE SET)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 1 emission control valve set connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0020" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0013" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0020" proc-id="RM22W0E___00000SR00000">
<testtitle>CHECK MIL (NO. 2 EMISSION CONTROL VALVE SET)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 2 emission control valve set connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0019" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0018" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0019" proc-id="RM22W0E___00000SQ00000">
<testtitle>CHECK MIL (VVT SENSOR [FOR INTAKE SIDE OF BANK 1])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the VVT sensor (for intake side of bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0021" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0023" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0021" proc-id="RM22W0E___00000SS00000">
<testtitle>CHECK MIL (VVT SENSOR [FOR INTAKE SIDE OF BANK 2])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the VVT sensor (for intake side of bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0028" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0025" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0028" proc-id="RM22W0E___00000ST00000">
<testtitle>CHECK MIL (VVT SENSOR [FOR EXHAUST SIDE OF BANK 1])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the VVT sensor (for exhaust side of bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0029" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0031" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0029" proc-id="RM22W0E___00000SU00000">
<testtitle>CHECK MIL (VVT SENSOR [FOR EXHAUST SIDE OF BANK 2])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the VVT sensor (for exhaust side of bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0030" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0032" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0030" proc-id="RM22W0E___00000SV00000">
<testtitle>CHECK MIL (POWER STEERING OIL PRESSURE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the power steering oil pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0008" fin="false">B</down>
<right ref="RM000001D6V0PRX_05_0033" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0008" proc-id="RM22W0E___00000SO00000">
<testtitle>CHECK HARNESS AND CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle body with motor assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the accelerator pedal sensor assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the No. 1 emission control valve set connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the No. 2 emission control valve set connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the VVT sensor (for intake side of bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the VVT sensor (for intake side of bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the VVT sensor (for exhaust side of bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the VVT sensor (for exhaust side of bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the power steering oil pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A38-57 (VCPA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A38-59 (VCP2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-80 (VCTA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-67 (VCV1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-66 (VCV2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A52-57 (VCPA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A52-59 (VCP2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-80 (VCTA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-67 (VCV1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-66 (VCV2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001D6V0PRX_05_0016" fin="true">OK</down>
<right ref="RM000001D6V0PRX_05_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0009">
<testtitle>SYSTEM OK</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0010">
<testtitle>GO TO MIL CIRCUIT<xref label="Seep01" href="RM000000WZ110TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0011">
<testtitle>REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY<xref label="Seep01" href="RM000000Q0P04BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0012">
<testtitle>REPLACE ACCELERATOR PEDAL SENSOR ASSEMBLY<xref label="Seep01" href="RM0000028B2014X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0013">
<testtitle>REPLACE NO. 1 EMISSION CONTROL VALVE SET<xref label="Seep01" href="RM0000030WH01ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0018">
<testtitle>REPLACE NO. 2 EMISSION CONTROL VALVE SET<xref label="Seep01" href="RM0000040KD00TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0023">
<testtitle>REPLACE VVT SENSOR (FOR INTAKE SIDE OF BANK 1)<xref label="Seep01" href="RM0000028AM014X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0025">
<testtitle>REPLACE VVT SENSOR (FOR INTAKE SIDE OF BANK 2)<xref label="Seep01" href="RM0000028AM014X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0031">
<testtitle>REPLACE VVT SENSOR (FOR EXHAUST SIDE OF BANK 1)<xref label="Seep01" href="RM0000028AM014X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0032">
<testtitle>REPLACE VVT SENSOR (FOR EXHAUST SIDE OF BANK 2)<xref label="Seep01" href="RM0000028AM014X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0033">
<testtitle>REPLACE POWER STEERING OIL PRESSURE SENSOR<xref label="Seep01" href="RM000003BJ401HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0015">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001D6V0PRX_05_0016">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202PX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>