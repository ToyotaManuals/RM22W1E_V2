<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3U4_T00N7" variety="T00N7">
<name>FRONT SEAT SIDE AIRBAG ASSEMBLY (for Manual Seat)</name>
<para id="RM000002OEP01TX" category="N" type-id="3000N" name-id="RSD92-04" from="201308">
<name>DISPOSAL</name>
<subpara id="RM000002OEP01TX_02" type-id="11" category="10" proc-id="RM22W0E___0000FIL00001">
<content3 releasenbr="1">
<atten4>
<ptxt>When scrapping a vehicle equipped with the SRS or disposing of the steering pad, be sure to deploy the airbag first in accordance with the procedure described below. If any abnormality occurs with the airbag deployment, contact the SERVICE DEPT. of the DISTRIBUTOR..</ptxt>
</atten4>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Never dispose of a front seat airbag that has an unactivated airbag.</ptxt>
</item>
<item>
<ptxt>The airbag produces an exploding sound when it is deployed, so perform the operation outdoors and where it will not create a nuisance to nearby residents.</ptxt>
</item>
<item>
<ptxt>When deploying the airbag, always use the specified SST (SRS Airbag Deployment Tool). Perform the operation in a place away from electrical noise.</ptxt>
</item>
<item>
<ptxt>When deploying the airbag, perform the operation at least 10 m (32.8 ft.) away from the airbag.</ptxt>
</item>
<item>
<ptxt>The front seat airbag becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a front seat airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water to a front seat airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
<item>
<ptxt>An airbag or pretensioner may be activated by static electricity. To prevent this, be sure to touch a metal surface with bare hands to discharge static electricity before performing this procedure.</ptxt>
</item>
</list1>
</atten2>
</content3>
</subpara>
<subpara id="RM000002OEP01TX_01" type-id="01" category="01">
<s-1 id="RM000002OEP01TX_01_0001" proc-id="RM22W0E___0000FIJ00001">
<ptxt>DISPOSE OF FRONT SEAT AIRBAG ASSEMBLY LH (WHEN INSTALLED IN VEHICLE)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Prepare a battery as the power source to deploy the airbag.</ptxt>
</atten4>
<s2>
<ptxt>Check the function of SST (See page <xref label="Seep01" href="RM0000038MY01LX_01_0001"/>).</ptxt>
<figure>
<graphic graphicname="C110371E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Read the precaution (See page <xref label="Seep02" href="RM000000KT10L4X"/>).</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep05" href="RM000003C32006X"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the front seat (See page <xref label="Seep03" href="RM00000390S00ZX"/>).</ptxt>
<atten4>
<ptxt>Keep the front seat in the cabin.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install SST.</ptxt>
<figure>
<graphic graphicname="B181634E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Disconnect the connector (yellow colored) from the front seat airbag.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Connect SST connector to the front seat airbag connector.</ptxt>
<sst>
<sstitem>
<s-number>09082-00700</s-number>
</sstitem>
<sstitem>
<s-number>09082-00820</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>To avoid damaging SST connector and wire harness, do not lock the secondary lock of the twin lock.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Install the front seat (See page <xref label="Seep04" href="RM00000390Q013X"/>).</ptxt>
</s3>
<s3>
<ptxt>Move SST at least 10 m (32.8 ft.) away from the vehicle front side window.</ptxt>
<figure>
<graphic graphicname="C110476E12" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Maintaining enough clearance for SST wire harness in the front side window, close all doors and windows of the vehicle.</ptxt>
<atten3>
<ptxt>Take care not to damage SST wire harness.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Connect the red clip of SST to the battery positive (+) terminal and the black clip of SST to the battery negative (-) terminal. </ptxt>
</s3>
</s2>
<s2>
<ptxt>Deploy the airbag.</ptxt>
<s3>
<ptxt>Check that no one is inside the vehicle or within a 10 m (32.8 ft.) radius of the vehicle.</ptxt>
</s3>
<s3>
<ptxt>Press SST activation switch to deploy the airbag.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>When deploying the airbag, make sure that no one is near the vehicle.</ptxt>
</item>
<item>
<ptxt>The front seat airbag becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a front seat airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water to a front seat airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
</list1>
</atten2>
<atten4>
<ptxt>The airbag is deployed as the LED of SST activation switch comes on.</ptxt>
</atten4>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002OEP01TX_01_0002" proc-id="RM22W0E___0000FIK00001">
<ptxt>DISPOSE OF FRONT SEAT AIRBAG ASSEMBLY LH (WHEN NOT INSTALLED IN VEHICLE)</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Be sure to follow the procedure detailed below when deploying the airbag.</ptxt>
</atten3>
<atten4>
<ptxt>Prepare a battery as the power source to deploy the airbag.</ptxt>
</atten4>
<s2>
<ptxt>Check the function of SST (See page <xref label="Seep01" href="RM0000038MY01LX_01_0001"/>).</ptxt>
<figure>
<graphic graphicname="C110371E09" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the front seat airbag (See page <xref label="Seep02" href="RM00000390T020X"/>).</ptxt>
</s2>
<s2>
<ptxt>Using a service-purpose wire harness for the vehicle, tie down the front seat airbag to a tire.</ptxt>
<figure>
<graphic graphicname="B104882E11" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Cross-sectional area of stripped wire harness section</title>
<specitem>
<ptxt>1.25 mm<sup>2</sup> (0.00192 in.<sup>2</sup>) or more</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>To calculate the cross-sectional area of the stripped wire harness section:</ptxt>
<ptxt>Cross-sectional area = 3.14 x (Diameter)<sup>2</sup> divided by 4</ptxt>
</atten4>
<atten2>
<ptxt>If the wire harness is too thin or an alternative object is used to tie down the front seat airbag, it may be snapped by the shock when the airbag is deployed. Always use a wire harness for vehicle use with a cross-sectional area of at least 1.25 mm<sup>2</sup> (0.00192 in.<sup>2</sup>).</ptxt>
</atten2>
<s3>
<ptxt>Install the 2 nuts to the front seat airbag.</ptxt>
<figure>
<graphic graphicname="C107070" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Wind the wire harness around the stud bolts of the front seat airbag as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C107071" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Position the front seat airbag inside a tire.</ptxt>
<figure>
<graphic graphicname="C107072E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Tire size</title>
<specitem>
<ptxt>Must exceed the following dimensions</ptxt>
</specitem>
<subtitle>Width</subtitle>
<specitem>
<ptxt>185 mm (7.28 in.)</ptxt>
</specitem>
<subtitle>Inner diameter</subtitle>
<specitem>
<ptxt>360 mm (14.2 in.)</ptxt>
</specitem>
</spec>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Make sure that the wire harnesses are tight. If there is slack in the wire harnesses, the front seat airbag may become loose due to the shock when the airbag is deployed.</ptxt>
</item>
<item>
<ptxt>Always tie down the front seat airbag with the airbag deployment direction facing inside the tire.</ptxt>
</item>
</list1>
</atten2>
<atten3>
<ptxt>As the tire may be damaged by the airbag deployment, use a tire that you are planning to throw away.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Install SST.</ptxt>
<figure>
<graphic graphicname="C107073E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Connect SST connector to the front seat airbag connector.</ptxt>
<sst>
<sstitem>
<s-number>09082-00820</s-number>
</sstitem>
</sst>
</s3>
</s2>
<s2>
<ptxt>Place tires.</ptxt>
<figure>
<graphic graphicname="C107074E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Place at least 2 tires under the tire which the front seat airbag is tied to.</ptxt>
</s3>
<s3>
<ptxt>Place at least 2 tires over the tire which the front seat airbag is tied to. The top tire should have a disc wheel installed.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not place SST connector under the tire because it could be damaged.</ptxt>
</item>
<item>
<ptxt>As the disc wheel may be damaged by the airbag deployment, use a disc wheel that you are planning to throw away.</ptxt>
</item>
<item>
<ptxt>As the tires may be damaged by the airbag deployment, use tires that you are planning to throw away.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Tie the tires together with 2 wire harnesses.</ptxt>
<figure>
<graphic graphicname="B105087" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Make sure that the wire harnesses are tight. Looseness in the wire harnesses results in the tires coming free due to the shock when the airbag is deployed.</ptxt>
</atten2>
</s3>
</s2>
<s2>
<ptxt>Install SST.</ptxt>
<figure>
<graphic graphicname="C107075E18" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Connect SST connector.</ptxt>
<sst>
<sstitem>
<s-number>09082-00700</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>To avoid damaging SST connector and wire harness, do not lock the secondary lock of the twin lock. Also, secure some slack for SST wire harness inside the tire.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Deploy the airbag.</ptxt>
<s3>
<ptxt>Connect the red clip of SST to the battery positive (+) terminal and the black clip of SST to the battery negative (-) terminal.</ptxt>
</s3>
<s3>
<ptxt>Check that no one is within a 10 m (32.8 ft.) radius of the tire which the front seat airbag is tied to.</ptxt>
</s3>
<s3>
<ptxt>Press SST activation switch to deploy the airbag.</ptxt>
<atten2>
<ptxt>When deploying the airbag, make sure that no one is near the tire.</ptxt>
</atten2>
<atten4>
<ptxt>The airbag is deployed as the LED of SST activation switch comes on.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Dispose of the front seat airbag.</ptxt>
<figure>
<graphic graphicname="H000544E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<list1 type="unordered">
<item>
<ptxt>The front seat airbag becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a front seat airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water to a front seat airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
</list1>
</atten2>
<s3>
<ptxt>Remove the front seat airbag from the tire.</ptxt>
</s3>
<s3>
<ptxt>Place the front seat airbag in a plastic bag, tie it tightly and dispose of it in the same way as other general parts.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>