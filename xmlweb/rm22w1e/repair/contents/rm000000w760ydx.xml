<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0019" variety="S0019">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0019_7C3ME_T00FH" variety="T00FH">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1GR-FE)</name>
<para id="RM000000W760YDX" category="D" type-id="303F3" name-id="AT0036-255" from="201308">
<name>DIAGNOSIS SYSTEM</name>
<subpara id="RM000000W760YDX_z0" proc-id="RM22W0E___00007SG00001">
<content5 releasenbr="1">
<step1>
<ptxt>DESCRIPTION</ptxt>
<step2>
<ptxt>When troubleshooting On-Board Diagnostic (OBD II) vehicles, the vehicle must be connected to the OBD II scan tool (complying with SAE J1987). Various data output from the vehicle ECM can then be read.</ptxt>
</step2>
<step2>
<ptxt>OBD II regulations require that the vehicle on-board computer illuminate the Malfunction Indicator Lamp (MIL) on the instrument panel when the computer detects a malfunction in:</ptxt>
<figure>
<graphic graphicname="A093827E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>The emission control system/components</ptxt>
</step3>
<step3>
<ptxt>The powertrain control components (which affect vehicle emissions)</ptxt>
</step3>
<step3>
<ptxt>The computer</ptxt>
<ptxt>In addition, the applicable Diagnostic Trouble Codes (DTCs) prescribed by SAE J2012 are stored in the ECM memory.</ptxt>
<ptxt>When the malfunction does not reoccur, the MIL stays illuminated until the engine switch is turned off, and the MIL turns off when the engine is started. However, the DTCs remain stored in the ECM memory.</ptxt>
</step3>
</step2>
<step2>
<ptxt>To check DTCs, connect the intelligent tester to the Data Link Connector 3 (DLC3) of the vehicle. The tester displays output DTCs, the freeze frame data and a variety of the engine data.</ptxt>
<ptxt>The DTCs and freeze frame data can be cleared with the tester (See page <xref label="Seep01" href="RM000000W770Y3X"/>).</ptxt>
</step2>
</step1>
<step1>
<ptxt>NORMAL MODE AND CHECK MODE</ptxt>
<step2>
<ptxt>The diagnosis system operates in "normal mode" during normal vehicle use. In normal mode, "2 trip detection logic" is used to ensure accurate detection of malfunctions. "Check mode" is also available to technicians as an option. In check mode, "1 trip detection logic" is used for simulating malfunction symptoms and increasing the system ability to detect malfunctions, including intermittent malfunctions.</ptxt>
</step2>
</step1>
<step1>
<ptxt>2 TRIP DETECTION LOGIC</ptxt>
<step2>
<ptxt>When a malfunction is first detected, the malfunction is temporarily stored in the ECM memory (1st trip). If the same malfunction is detected during the next drive cycle, the MIL is illuminated (2nd trip).</ptxt>
</step2>
</step1>
<step1>
<ptxt>FREEZE FRAME DATA</ptxt>
<step2>
<ptxt>Freeze frame data records the engine conditions (fuel system, calculated load, engine coolant temperature, fuel trim, engine speed, vehicle speed, etc.) when a malfunction is detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</step2>
<step2>
<ptxt>The ECM records freeze frame data in 5 different instances: 1) 3 times before the DTC is stored, 2) once when the DTC is stored, and 3) once after the DTC is stored. These data can be used to simulate the vehicle condition around the time when the malfunction occurred. The data may help find the cause of the malfunction, or judge if the DTC is being caused by a temporary malfunction or not.</ptxt>
<figure>
<graphic graphicname="C140837E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>CHECK DATA LINK CONNECTOR 3 (DLC3) (See page <xref label="Seep02" href="RM000000UZ20DEX"/>)</ptxt>
</step1>
<step1>
<ptxt>CHECK MIL</ptxt>
<step2>
<ptxt>Check that the MIL illuminates when turning the engine switch on (IG).</ptxt>
<ptxt>If the MIL does not illuminate, there is a problem in the MIL circuit (See page <xref label="Seep03" href="RM000000WZ114FX"/>).</ptxt>
</step2>
<step2>
<ptxt>When the engine is started, the MIL should turn off.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>