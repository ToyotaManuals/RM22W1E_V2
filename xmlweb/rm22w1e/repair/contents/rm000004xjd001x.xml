<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3V5_T00O8" variety="T00O8">
<name>REAR NO. 1 SEAT ASSEMBLY (for Bench Seat Type)</name>
<para id="RM000004XJD001X" category="A" type-id="80002" name-id="SE8BO-01" from="201301" to="201308">
<name>DISASSEMBLY</name>
<subpara id="RM000004XJD001X_01" type-id="11" category="10" proc-id="RM22W0E___0000GCR00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
</content3>
</subpara>
<subpara id="RM000004XJD001X_02" type-id="01" category="01">
<s-1 id="RM000004XJD001X_02_0001" proc-id="RM22W0E___0000GCS00000">
<ptxt>REMOVE REAR SEAT HEADREST ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the rear seat headrest assembly on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Remove the rear seat headrest assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0002" proc-id="RM22W0E___0000GCT00000">
<ptxt>REMOVE RECLINING REMOTE CONTROL LEVER KNOB</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the reclining remote control lever knob on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Remove the screw and reclining remote control lever knob.</ptxt>
<figure>
<graphic graphicname="B292808" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0003" proc-id="RM22W0E___0000GCU00000">
<ptxt>REMOVE RECLINING REMOTE CONTROL BEZEL</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the reclining remote control bezel on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Remove the screw.</ptxt>
<figure>
<graphic graphicname="B292810" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the hook and remove the reclining remote control bezel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0004" proc-id="RM22W0E___0000GCV00000">
<ptxt>REMOVE SEAT ADJUSTER COVER</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the seat adjuster cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the seat adjuster cover from the No. 1 reclining adjuster release handle LH.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B292812E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0005" proc-id="RM22W0E___0000GCW00000">
<ptxt>REMOVE NO. 1 RECLINING ADJUSTER RELEASE HANDLE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw and No. 1 reclining adjuster release handle LH.</ptxt>
<figure>
<graphic graphicname="B292814" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0038" proc-id="RM22W0E___0000GDJ00000">
<ptxt>REMOVE NO. 1 RECLINING ADJUSTER RELEASE HANDLE RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0006" proc-id="RM22W0E___0000GCX00000">
<ptxt>REMOVE SEATBACK COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292816E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 hooks and open the 2 fasteners.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastener</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a clip remover, remove the 2 clips.</ptxt>
<figure>
<graphic graphicname="B292818" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the rear seatback cover.</ptxt>
<figure>
<graphic graphicname="B292819" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the 2 rear seat headrest supports.</ptxt>
<figure>
<graphic graphicname="B292821" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to remove the 2 rear seat headrest supports on the other side.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the seatback cover with pad from the rear seatback frame sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0007" proc-id="RM22W0E___0000GCY00000">
<ptxt>REMOVE BENCH TYPE REAR SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the hog rings and bench type rear seatback cover from the bench type rear seatback pad.</ptxt>
<figure>
<graphic graphicname="B292822" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0008" proc-id="RM22W0E___0000GCZ00000">
<ptxt>REMOVE REAR SEAT NO. 1 RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292824" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the guide and remove the rear seat No. 1 reclining cover LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0009" proc-id="RM22W0E___0000GD000000">
<ptxt>REMOVE REAR SEAT NO. 1 RECLINING COVER RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0010" proc-id="RM22W0E___0000GD100000">
<ptxt>REMOVE NO. 2 SEAT ADJUSTER CONTROL CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292825E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cable End</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>Detach the 4 cable clamps from both sides of the rear seat reclining adjuster assembly as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 4 cable ends from both sides of the rear seat reclining adjuster assembly as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Detach the 8 cable clamps and remove the No. 2 seat adjuster control cable assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0011" proc-id="RM22W0E___0000GD200000">
<ptxt>REMOVE REAR SEATBACK FRAME SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292827" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and rear seatback frame sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0012" proc-id="RM22W0E___0000GD300000">
<ptxt>REMOVE REAR SEAT CUSHION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292828" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw and rear seat cushion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0013" proc-id="RM22W0E___0000GD400000">
<ptxt>REMOVE FOLD SEAT STOPPER BAND ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292829" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw and rear seat cushion together with the fold seat stopper band assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0014" proc-id="RM22W0E___0000GD500000">
<ptxt>REMOVE REAR SEAT TURN LOCK COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292830" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws and rear seat turn lock cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0015" proc-id="RM22W0E___0000GD600000">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292831E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Open the 2 fasteners and detach the fastening tape.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastener</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 2 hooks.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B292833E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the hog rings and seat cushion cover with pad from the rear seat cushion frame sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0016" proc-id="RM22W0E___0000GD700000">
<ptxt>REMOVE BENCH TYPE REAR SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B293925" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the hog rings.</ptxt>
</s2>
<s2>
<ptxt>Remove the hog rings and bench type rear seat cushion cover from the rear No. 1 seat cushion pad.</ptxt>
<figure>
<graphic graphicname="B293927" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0017" proc-id="RM22W0E___0000GD800000">
<ptxt>REMOVE REAR SEATBACK LOCK COVER</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the rear seatback lock cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Remove the 2 screws and rear seatback lock cover from the rear seat lock assembly LH.</ptxt>
<figure>
<graphic graphicname="B292835" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0018" proc-id="RM22W0E___0000GD900000">
<ptxt>REMOVE REAR SEAT RECLINING ADJUSTER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and rear seat reclining adjuster assembly LH from the rear seat cushion frame sub-assembly.</ptxt>
<figure>
<graphic graphicname="B292836" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0019" proc-id="RM22W0E___0000GDA00000">
<ptxt>REMOVE REAR SEAT RECLINING ADJUSTER ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0020" proc-id="RM22W0E___0000GDB00000">
<ptxt>REMOVE REAR SEAT HINGE COVER</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the rear seat hinge cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Remove the rear seat hinge cover from the rear seat reclining adjuster assembly LH.</ptxt>
<figure>
<graphic graphicname="B292837" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0021" proc-id="RM22W0E___0000GDC00000">
<ptxt>REMOVE REAR SEAT LOCK CONTROL CABLE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292839E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tension Spring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cable End A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cable End B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>Remove the 2 tension springs from both sides of the rear seat cushion frame sub-assembly as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 cable clamps.</ptxt>
</s2>
<s2>
<ptxt>Twist the 2 cable ends A and disconnect them from both sides of the rear seat cushion frame sub-assembly as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 cable clamps.</ptxt>
</s2>
<s2>
<ptxt>Twist the 2 cable ends B and disconnect them from both sides of the rear seat lock assembly as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Pull out the 2 cable ends B from the 2 holes of the rear seat cushion frame sub-assembly as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 cable clamps and remove the rear seat lock control cable from the rear seat cushion frame sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0040" proc-id="RM22W0E___0000GDL00000">
<ptxt>REMOVE REAR SEAT INNER BELT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292831E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Open the 2 fasteners and detach the fastening tape.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastener</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the bolt and rear seat inner belt assembly.</ptxt>
<figure>
<graphic graphicname="B293895" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XJD001X_02_0041" proc-id="RM22W0E___0000GDM00000">
<ptxt>REMOVE REAR NO. 1 SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat inner belt assembly LH.</ptxt>
<figure>
<graphic graphicname="B293896" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XJD001X_02_0024" proc-id="RM22W0E___0000GDD00000">
<ptxt>REMOVE REAR SEAT LOCK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292847" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the rear seat No. 1 cushion spring.</ptxt>
</s2>
<s2>
<ptxt>Remove the rear seat lock assembly LH from the rear seat cushion frame sub-assembly.</ptxt>
<figure>
<graphic graphicname="B292848" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0039" proc-id="RM22W0E___0000GDK00000">
<ptxt>REMOVE FOLD SEAT LOCK COLLAR</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the fold seat lock collar on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Remove the fold seat lock collar from the rear seat lock assembly LH.</ptxt>
<figure>
<graphic graphicname="B293924" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0042" proc-id="RM22W0E___0000GDN00000">
<ptxt>REMOVE REAR SEAT OUTER BELT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and rear seat outer belt assembly.</ptxt>
<figure>
<graphic graphicname="B293894" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XJD001X_02_0043" proc-id="RM22W0E___0000GDO00000">
<ptxt>REMOVE REAR NO. 1 SEAT INNER BELT ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat inner belt assembly RH.</ptxt>
<figure>
<graphic graphicname="B293897" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XJD001X_02_0027" proc-id="RM22W0E___0000GDE00000">
<ptxt>REMOVE REAR SEAT LOCK ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0028" proc-id="RM22W0E___0000GDF00000">
<ptxt>REMOVE REAR SEAT HINGE SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292841" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 E-rings.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 rear seat hinge pins and rear seat hinge sub-assembly LH from the rear seat cushion frame sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0031" proc-id="RM22W0E___0000GDI00000">
<ptxt>REMOVE REAR SEAT HINGE SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0029" proc-id="RM22W0E___0000GDG00000">
<ptxt>REMOVE REAR SEAT CUSHION SUPPORT SPRING</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the rear seat cushion support spring on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Remove the 2 rear seat hinge set plates and rear seat cushion support spring from the rear seat cushion frame sub-assembly.</ptxt>
<figure>
<graphic graphicname="B292842" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XJD001X_02_0030" proc-id="RM22W0E___0000GDH00000">
<ptxt>REMOVE REAR SEAT CUSHION SPACER</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the rear seat cushion spacer on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Remove the rear seat cushion spacer from the rear seat cushion support spring.</ptxt>
<figure>
<graphic graphicname="B292843" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>