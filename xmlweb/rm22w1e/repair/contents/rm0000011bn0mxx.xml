<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM0000011BN0MXX" category="D" type-id="3001B" name-id="NS0087-512" from="201308">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM0000011BN0MXX_z0" proc-id="RM22W0E___0000CHP00001">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the following procedure to troubleshoot the navigation system.</ptxt>
</item>
<item>
<ptxt>*: Use the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CUSTOMER PROBLEM ANALYSIS</testtitle>
<list1 type="unordered">
<item>
<ptxt>When troubleshooting, confirm that the problem symptoms have been accurately identified. Preconceptions should be discarded in order to make an accurate judgment. To clearly understand what the problem symptoms are, it is extremely important to ask the customer about the problem and the conditions at the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Gather as much information as possible for reference.</ptxt>
<ptxt>Past problems that seem unrelated may also help in some cases.</ptxt>
</item>
<item>
<ptxt>The following 5 items are important points in problem analysis:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<tbody>
<row>
<entry>
<ptxt>What</ptxt>
</entry>
<entry>
<ptxt>Vehicle model, system name</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>When</ptxt>
</entry>
<entry>
<ptxt>Date, time, occurrence frequency</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Where</ptxt>
</entry>
<entry>
<ptxt>Road conditions</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Under what conditions?</ptxt>
</entry>
<entry>
<ptxt>Driving conditions, weather conditions</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>How did it happen?</ptxt>
</entry>
<entry>
<ptxt>Problem symptoms</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<test1>
<ptxt>Measure the battery voltage with the engine switch off.</ptxt>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding to the next step.</ptxt>
</item>
</list1>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK CABIN</testtitle>
<test1>
<ptxt>Check that condensation is not likely to occur in the cabin, and that the temperature is not high or extremely low in the cabin.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Condensation is not likely being produced and the temperature is not high or extremely low.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A humid cabin and a rapid change in temperature may lead to condensation. Condensation in the cabin may cause a short circuit.</ptxt>
</item>
<item>
<ptxt>The navigation system may not operate normally when the temperature is -20°C (-4°F) or less, or 65°C (149°F) or higher.</ptxt>
</item>
</list1>
</atten4>
</test1>
<results>
<result>NG</result>
<action-ci-right>SET CABIN TO APPROPRIATE TEMPERATURE</action-ci-right>
<result-ci-down>OK</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK NAVIGATION SYSTEM NORMAL CONDITION</testtitle>
<test1>
<ptxt>Refer to Check System Normal Condition (See page <xref label="Seep01" href="RM000003SKF0FLX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Symptom is not normal operation.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Symptom is normal operation.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>SYMPTOM IS NORMAL OPERATION</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK CAN COMMUNICATION SYSTEM*</testtitle>
<test1>
<ptxt>Check for DTC output.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: <xref label="Seep02" href="RM000001RSW03ZX"/>
</ptxt>
</item>
<item>
<ptxt>for RHD: <xref label="Seep22" href="RM000001RSW040X"/>
</ptxt>
</item>
</list1>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>CAN DTCs are not output.</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>CAN DTCs are output (for LHD).</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>CAN DTCs are output (for RHD).</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO CAN COMMUNICATION SYSTEM (See page <xref label="Seep03" href="RM000001RSO09SX"/>)</action-ci-right>
<result>C</result>
<action-ci-right>GO TO CAN COMMUNICATION SYSTEM (See page <xref label="Seep23" href="RM000001RSO09TX"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK FOR DTC*</testtitle>
<test1>
<ptxt>Refer to DTC Check/Clear (See page <xref label="Seep04" href="RM0000011BU0PQX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTCs are output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs are not output.</ptxt>
<ptxt>(The multi-display displays content.)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs are not output.</ptxt>
<ptxt>(The multi-display does not display anything.)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If more information is needed, perform a check using the "System Check Mode" screen after checking for DTCs using the intelligent tester.</ptxt>
</atten4>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to step 11</action-ci-right>
<result>C</result>
<action-ci-right>Go to step 12</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CLEAR DTC*</testtitle>
<test1>
<ptxt>Refer to DTC Check/Clear (See page <xref label="Seep05" href="RM0000011BU0PQX"/>).</ptxt>
<atten4>
<ptxt>The present DTCs may not indicate actual malfunctions depending on the vehicle operating conditions.</ptxt>
</atten4>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>RECHECK FOR DTC*</testtitle>
<test1>
<ptxt>Refer to DTC Check/Clear (See page <xref label="Seep06" href="RM0000011BU0PQX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTCs are output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs are not output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Even if the malfunction symptom is not confirmed, check for DTCs. This is because the system stores history DTCs.</ptxt>
</item>
<item>
<ptxt>If more information is needed, perform a recheck using the "System Check Mode" screen after rechecking for DTCs using the intelligent tester.</ptxt>
</item>
<item>
<ptxt>Check the DTCs and inspect the area that the code indicates.</ptxt>
</item>
</list1>
</atten4>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to step 11</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>DIAGNOSTIC TROUBLE CODE CHART</testtitle>
<test1>
<ptxt>Find the output code in Diagnostic Trouble Code Chart (See page <xref label="Seep07" href="RM0000011BO0JSX"/>).</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The navigation system outputs DTCs for the following system.</ptxt>
</item>
<item>
<ptxt>When DTCs other than those in Diagnostic Trouble Code Chart for the navigation system are output, refer to Diagnostic Trouble Code Chart for the relevant system.</ptxt>
</item>
<item>
<ptxt>Refer to Audio and Visual System (w/ Navigation System) (See page <xref label="Seep08" href="RM0000017YP0BPX"/>)</ptxt>
</item>
<item>
<ptxt>Refer to Rear Seat Entertainment System (See page <xref label="Seep24" href="RM000003WTP02WX"/>)</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTCs for the navigation system are output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs for the audio and visual system (w/ Navigation System) are output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs for the rear seat entertainment system..</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>A</result>
<action-ci-right>Go to step 14</action-ci-right>
<result>B</result>
<action-ci-right>GO TO AUDIO AND VISUAL SYSTEM (w/ Navigation System) (See page <xref label="Seep09" href="RM0000017YP0BPX"/>)</action-ci-right>
<result>C</result>
<action-ci-right>GO TO REAR SEAT ENTERTAINMENT SYSTEM (See page <xref label="Seep25" href="RM000003WTP02WX"/>)</action-ci-right>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK HARD DISC</testtitle>
<test1>
<ptxt>Enter the "Failure Diagnosis" screen. Refer to Check Hard Disc in Operation Check (See page <xref label="Seep10" href="RM000003SKF0FLX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>"OK" is displayed.</ptxt>
</specitem>
</spec>
</test1>
<results>
<result>NG</result>
<action-ci-right>REPLACE HARD DISC (See page <xref label="Seep11" href="RM000002V0L01HX"/>)</action-ci-right>
<result-ci-down>OK</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to Problem Symptoms Table (See page <xref label="Seep12" href="RM0000011BR0NBX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Fault is not listed in Problem Symptoms Table.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fault is listed in Problem Symptoms Table.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the symptom does not recur and no DTCs are output, attempt to reproduce the symptoms (See page <xref label="Seep13" href="RM0000011BR0NBX"/>).</ptxt>
</atten4>
</test1>
<results>
<result>B</result>
<action-ci-right>ADJUST, REPAIR OR REPLACE IN ACCORDANCE WITH PROBLEM SYMPTOMS TABLE</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PERFORM TROUBLESHOOTING BASED ON MALFUNCTION SYMPTOM</testtitle>
<test1>
<ptxt>Refer to Terminals of ECU (See page <xref label="Seep14" href="RM0000011BQ0MGX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK CIRCUIT</testtitle>
<test1>
<ptxt>Adjust, repair or replace as necessary.</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>RECHECK FOR DIAGNOSTIC TROUBLE CODE*</testtitle>
<atten4>
<ptxt>After clearing DTCs, recheck for DTCs (See page <xref label="Seep15" href="RM0000011BU0PQX"/>).</ptxt>
</atten4>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PERFORM CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>