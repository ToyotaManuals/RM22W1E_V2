<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DD_T006G" variety="T006G">
<name>POWER STEERING OIL PRESSURE SWITCH</name>
<para id="RM000002H5903XX" category="A" type-id="30014" name-id="ES10O4-002" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000002H5903XX_01" type-id="01" category="01">
<s-1 id="RM000002H5903XX_01_0002" proc-id="RM22W0E___000016D00000">
<ptxt>INSTALL POWER STEERING OIL PRESSURE SWITCH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new O-ring to the power steering oil pressure switch.</ptxt>
</s2>
<s2>
<ptxt>Apply a light coat of engine oil to the O-ring.</ptxt>
</s2>
<s2>
<ptxt>Using a 24 mm deep socket wrench, install the power steering oil pressure switch.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>210</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the power steering oil pressure switch connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5903XX_01_0046" proc-id="RM22W0E___000016H00000">
<ptxt>INSTALL FRONT HEIGHT CONTROL SENSOR SUB-ASSEMBLY RH (w/ Active Height Control)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front height control sensor sub-assembly RH with the bolt and nut.</ptxt>
<torque>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
<subtitle>for nut</subtitle>
<torqueitem>
<t-value1>5.6</t-value1>
<t-value2>57</t-value2>
<t-value3>50</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H5903XX_01_0010">
<ptxt>ADD POWER STEERING FLUID</ptxt>
</s-1>
<s-1 id="RM000002H5903XX_01_0028" proc-id="RM22W0E___000010H00000">
<ptxt>BLEED POWER STEERING SYSTEM
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the fluid level.</ptxt>
</s2>
<s2>
<ptxt>Jack up the front of the vehicle and support it with stands.</ptxt>
</s2>
<s2>
<ptxt>Turn the steering wheel.</ptxt>
<s3>
<ptxt>With the engine stopped, turn the wheel slowly from lock to lock several times.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Lower the vehicle.</ptxt>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>Idle the engine for a few minutes.</ptxt>
</s2>
<s2>
<ptxt>Turn the steering wheel.</ptxt>
<s3>
<ptxt>With the engine idling, turn the wheel left or right to the full lock position and keep it there for 2 to 3 seconds, then turn the wheel to the opposite full lock position and keep it there for 2 to 3 seconds. *1</ptxt>
</s3>
<s3>
<ptxt>Repeat *1 several times.</ptxt>
<atten3>
<ptxt>For vehicles with VGRS, if the steering wheel is turned from lock to lock repeatedly, the system may stop operating and the amount of rotation before the steering wheel locks may increase due to operation of the overheating prevention function. When the system temperature drops, the system operation automatically returns to normal.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Check for foaming or emulsification.</ptxt>
<figure>
<graphic graphicname="C124274E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Correct</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Incorrect</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the system has to be bled twice because of foaming or emulsification, check for fluid leaks in the system.</ptxt>
</s2>
<s2>
<ptxt>Check the fluid level.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002H5903XX_01_0020">
<ptxt>INSPECT FOR POWER STEERING FLUID LEAK</ptxt>
</s-1>
<s-1 id="RM000002H5903XX_01_0045" proc-id="RM22W0E___000016G00000">
<ptxt>CHECK POWER STEERING FLUID LEVEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Keep the vehicle horizontal.</ptxt>
<figure>
<graphic graphicname="C158723E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>COLD Range</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>HOT Range</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>With the engine stopped, check the fluid level in the reservoir.</ptxt>
<ptxt>If necessary, add fluid.</ptxt>
<spec>
<title>Fluid</title>
<specitem>
<ptxt>ATF DEXRON II or III, or equivalent</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>If the fluid is hot, check that the fluid level is within the HOT range. If the fluid is cold, check that the fluid level is within the COLD range.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Start the engine and idle it.</ptxt>
</s2>
<s2>
<ptxt>Turn the steering wheel from lock to lock several times to raise the fluid temperature.</ptxt>
<spec>
<title>Fluid temperature</title>
<specitem>
<ptxt>80°C (176°F)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Check for foaming or emulsification.</ptxt>
<figure>
<graphic graphicname="C124274E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Correct</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Incorrect</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If foaming or emulsification is identified, bleed the power steering system.</ptxt>
</s2>
<s2>
<ptxt>With the engine idling, measure the fluid level in the reservoir.</ptxt>
<figure>
<graphic graphicname="C124276E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine idling</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine stopped</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.0 mm (0.197 in.) or less</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Wait a few minutes and remeasure the fluid level in the reservoir.</ptxt>
<spec>
<title>Maximum fluid level rise</title>
<specitem>
<ptxt>5.0 mm (0.197 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the fluid level rise is more than the maximum, bleed the power steering system.</ptxt>
</s2>
<s2>
<ptxt>Check the fluid level.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002H5903XX_01_0021" proc-id="RM22W0E___000016E00000">
<ptxt>INSTALL FRONT FENDER APRON TRIM PACKING A
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front fender apron trim packing A with the 3 clips.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002H5903XX_01_0043" proc-id="RM22W0E___000016F00000">
<ptxt>INSTALL FRONT WHEEL
</ptxt>
<content1 releasenbr="1">
<torque>
<subtitle>for Aluminum Wheel</subtitle>
<torqueitem>
<t-value1>131</t-value1>
<t-value2>1336</t-value2>
<t-value4>97</t-value4>
</torqueitem>
<subtitle>for Steel Wheel</subtitle>
<torqueitem>
<t-value1>209</t-value1>
<t-value2>2131</t-value2>
<t-value4>154</t-value4>
</torqueitem>
</torque>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>