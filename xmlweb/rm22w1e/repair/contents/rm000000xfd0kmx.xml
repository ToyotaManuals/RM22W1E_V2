<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TW_T00MZ" variety="T00MZ">
<name>AIRBAG SYSTEM</name>
<para id="RM000000XFD0KMX" category="D" type-id="303F3" name-id="RS2T3-54" from="201308">
<name>DIAGNOSIS SYSTEM</name>
<subpara id="RM000000XFD0KMX_z0" proc-id="RM22W0E___0000F6Z00001">
<content5 releasenbr="1">
<step1>
<ptxt>SYMPTOM SIMULATION</ptxt>
<atten4>
<ptxt>The most difficult case in troubleshooting is when no problem symptoms occur. In such a case, a thorough problem analysis must be carried out. A simulation of the same or similar conditions and environment in which the problem occurred in the customer's vehicle should be carried out. No matter how much skill or experience a technician has, troubleshooting without confirming the problem symptoms will lead to important repairs being overlooked and mistakes or delays.</ptxt>
</atten4>
<figure>
<graphic graphicname="D025083E19" width="2.775699831in" height="3.779676365in"/>
</figure>
<step2>
<ptxt>Vibration method: When vibration seems to be the major cause.</ptxt>
<atten4>
<ptxt>Perform the simulation method only during the primary check period (for approximately 6 seconds after the ignition switch is turnedON).</ptxt>
</atten4>
<step3>
<ptxt>Slightly vibrate the part of the sensor considered to be the cause of the problem with your fingers and check whether the malfunction occurs.</ptxt>
<atten3>
<ptxt>Shaking the relays too strongly may result in open relays.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>Slightly shake the connector vertically and horizontally.</ptxt>
</step3>
<step3>
<ptxt>Slightly shake the wire harness vertically and horizontally.</ptxt>
<atten4>
<ptxt>The connector joint and fulcrum of the vibration are the major areas to be checked thoroughly.</ptxt>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>FUNCTION OF SRS WARNING LIGHT</ptxt>
<step2>
<ptxt>Primary check.</ptxt>
<step3>
<ptxt>Turn the ignition switch off. Wait for at least 2 seconds, then turn the ignition switch to ON. The SRS warning light comes on for approximately 6 seconds and the diagnosis of the airbag system (including the seat belt pretensioners) is performed.</ptxt>
<atten4>
<ptxt>If trouble is detected during the primary check, the SRS warning light remains on even after the primary check period (of approximately 6 seconds) has elapsed.</ptxt>
</atten4>
</step3>
</step2>
<step2>
<ptxt>Constant check.</ptxt>
<step3>
<ptxt>After the primary check, the center airbag sensor constantly monitors the airbag system for trouble.</ptxt>
<atten4>
<ptxt>If trouble is detected during the constant check, the center airbag sensor functions as follows:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The SRS warning light comes on.</ptxt>
</item>
<item>
<ptxt>The SRS warning light goes off, and then comes on. This blinking pattern indicates a source voltage drop. The SRS warning light goes off 10 seconds after the source voltage returns to normal.</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
<step2>
<ptxt>Review.</ptxt>
<step3>
<ptxt>When the airbag system is normal:</ptxt>
<ptxt>The SRS warning light comes on only during the primary check period (for approximately 6 seconds after the ignition switch is turned to ON).</ptxt>
</step3>
<step3>
<ptxt>When the airbag system has trouble:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The SRS warning light remains on even after the primary check period has elapsed.</ptxt>
</item>
<item>
<ptxt>The SRS warning light goes off after the primary check, but comes on again during the constant check.</ptxt>
</item>
<item>
<ptxt>The SRS warning light does not come on when turning the ignition switch from off to ON.</ptxt>
</item>
</list1>
<atten4>
<ptxt>The center airbag sensor functions to keep the SRS warning light on if the airbag has been deployed.</ptxt>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<figure>
<graphic graphicname="C093955E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Turn the ignition switch to ON, and check that the SRS warning light comes on for approximately 6 seconds (primary check).</ptxt>
</step2>
<step2>
<ptxt>Check that the SRS warning light goes off approximately 6 seconds after the ignition switch is turned to ON (constant check).</ptxt>
<atten4>
<ptxt>When any of the following symptoms occur, refer to the "Problem Symptoms Table" (See page <xref label="Seep01" href="RM0000039WX00UX"/>).</ptxt>
<list1 type="unordered">
<item>
<ptxt>The SRS warning light comes on occasionally, after the primary check period has elapsed.</ptxt>
</item>
<item>
<ptxt>The SRS warning light comes on, but a DTC is not output.</ptxt>
</item>
<item>
<ptxt>The ignition switch is turned from off to ON, but the SRS warning light does not come on.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
<step1>
<ptxt>FUNCTION OF PASSENGER AIRBAG ON/OFF INDICATOR</ptxt>
<step2>
<ptxt>Initial check</ptxt>
<step3>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step3>
<step3>
<ptxt>The passenger airbag ON/OFF indicator comes on for approximately 4 seconds, then goes off for approximately 2 seconds.</ptxt>
</step3>
<step3>
<ptxt>Approximately 6 seconds after the ignition switch is turned to ON, the passenger airbag ON/OFF indicator will be ON or OFF depending on the conditions listed below.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>"ON" indicator</ptxt>
</entry>
<entry valign="middle">
<ptxt>"OFF" indicator</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vacant</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Adult is seated</ptxt>
</entry>
<entry valign="middle">
<ptxt>On</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Child is seated</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>On</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Child restraint system is set</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>On</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front passenger occupant classification failure</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>On</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The timing chart below shows the operation of the passenger airbag ON/OFF indicator. Use the chart to check the indicator light circuit.</ptxt>
</item>
<item>
<ptxt>When the occupant classification system has trouble, both the SRS warning light and the passenger airbag ON/OFF indicator ("OFF") come on. In this case, check the DTCs for "Airbag System" first. Then troubleshoot the occupant classification system if DTC B1650/32 is output, and troubleshoot the passenger airbag ON/OFF indicator if DTC B1660/43 is output.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="C123768E85" width="7.106578999in" height="3.779676365in"/>
</figure>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK PASSENGER AIRBAG ON/OFF INDICATOR</ptxt>
<figure>
<graphic graphicname="C175161" width="2.775699831in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Check that the passenger airbag ON/OFF indicator ("ON" and "OFF") comes on for approximately 4 seconds, then goes off for approximately 2 seconds.</ptxt>
<atten4>
<ptxt>Refer to the table in the previous step regarding the passenger airbag ON/OFF indicator when the ignition switch is turned to ON and approximately 6 seconds have passed.</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>ACTIVATION PREVENTION MECHANISM</ptxt>
<step2>
<ptxt>FUNCTION OF ACTIVATION PREVENTION MECHANISM</ptxt>
<step3>
<ptxt>An activation prevention mechanism is built into the connector (on the center airbag sensor side) of the airbag system squib circuit to prevent accidental airbag activation.</ptxt>
</step3>
<step3>
<ptxt>This mechanism closes the circuit when the connector is disconnected by bringing the short spring into contact with the terminals and shutting off external electricity to prevent accidental airbag activation.</ptxt>
</step3>
</step2>
<step2>
<ptxt>RELEASING OF ACTIVATION PREVENTION MECHANISM</ptxt>
<step3>
<ptxt>To release the activation prevention mechanism, insert a piece of paper with the same thickness as the male terminal (approximately 0.5 mm (0.020 in.)) between the terminals and the short spring to break the connection.</ptxt>
</step3>
<step3>
<ptxt>Refer to the following illustrations concerning connectors utilizing the activation prevention mechanism and its release method.</ptxt>
<atten2>
<ptxt>Never release the activation prevention mechanism on the squib connector even when inspecting with the squib disconnected.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not release the activation prevention mechanism unless specially directed by the troubleshooting procedure.</ptxt>
</item>
<item>
<ptxt>To prevent the terminal and the short spring from being damaged, always use a piece of paper with the same thickness as the male terminal.</ptxt>
</item>
</list1>
</atten3>
<figure>
<graphic graphicname="C171175E21" width="7.106578999in" height="9.803535572in"/>
</figure>
<figure>
<graphic graphicname="C146620E07" width="7.106578999in" height="9.803535572in"/>
</figure>
<figure>
<graphic graphicname="C146621E03" width="7.106578999in" height="9.803535572in"/>
</figure>
<figure>
<graphic graphicname="C174122E04" width="7.106578999in" height="9.803535572in"/>
</figure>
<figure>
<graphic graphicname="C172819E02" width="7.106578999in" height="7.795582503in"/>
</figure>
<figure>
<graphic graphicname="C172820E02" width="7.106578999in" height="7.795582503in"/>
</figure>
<figure>
<graphic graphicname="C142505E05" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="C172826E02" width="7.106578999in" height="8.799559038in"/>
</figure>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>