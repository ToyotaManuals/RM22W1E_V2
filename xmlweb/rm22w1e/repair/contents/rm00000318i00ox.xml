<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001F" variety="S001F">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001F_7C3O4_T00H7" variety="T00H7">
<name>REAR DIFFERENTIAL CARRIER ASSEMBLY (for Standard)</name>
<para id="RM00000318I00OX" category="A" type-id="8000E" name-id="AD00Q-02" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM00000318I00OX_01" type-id="01" category="01">
<s-1 id="RM00000318I00OX_01_0001" proc-id="RM22W0E___00009IQ00000">
<ptxt>INSPECT DIFFERENTIAL SIDE GEAR BACKLASH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 thrust washers to the side gears.</ptxt>
<figure>
<graphic graphicname="C124142" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the side gears to the differential case.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 pinion gears and 4 thrust washers to the spider.</ptxt>
</s2>
<s2>
<ptxt>Install the spider assembly to the differential case.</ptxt>
<atten4>
<ptxt>Install the spider to the differential case tightly.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using a dial indicator, measure the side gear backlash while holding the side gear and spider.</ptxt>
<figure>
<graphic graphicname="C161846" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard backlash</title>
<specitem>
<ptxt>0.02 to 0.15 mm (0.000787 to 0.00591 in.)</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Measure at all 4 pinion gear locations.</ptxt>
</item>
<item>
<ptxt>Measure the backlash at the differential case LH and RH.</ptxt>
</item>
<item>
<ptxt>Apply hypoid gear oil to each sliding surface and rotating part.</ptxt>
</item>
</list1>
<ptxt>If the backlash is not within the specification, replace the side gear thrust washers with washers of a different thickness.</ptxt>
</atten4>
<spec>
<title>Standard Washer Thickness</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>1.53 to 1.57 mm (0.0602 to 0.0618 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.83 to 1.87 mm (0.0720 to 0.0736 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.58 to 1.62 mm (0.0622 to 0.0638 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.88 to 1.92 mm (0.0740 to 0.0756 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.63 to 1.67 mm (0.0641 to 0.0657 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.93 to 1.97 mm (0.0760 to 0.0776 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.68 to 1.72 mm (0.0661 to 0.0677 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.98 to 2.02 mm (0.0780 to 0.0795 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.73 to 1.77 mm (0.0681 to 0.0697 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>2.03 to 2.07 mm (0.0799 to 0.0815 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.78 to 1.82 mm (0.0701 to 0.0717 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>2.08 to 2.12 mm (0.0820 to 0.0835 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0002" proc-id="RM22W0E___00009IR00000">
<ptxt>ASSEMBLE DIFFERENTIAL CASE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the spider assembly to the differential case.</ptxt>
<atten4>
<ptxt>Install the spider to the differential case tightly.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Align the matchmarks and assemble the differential case LH and RH.</ptxt>
<figure>
<graphic graphicname="C161843E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 8 bolts and tighten them uniformly, a little at a time.</ptxt>
<torque>
<torqueitem>
<t-value1>47</t-value1>
<t-value2>479</t-value2>
<t-value4>35</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0003" proc-id="RM22W0E___00009IS00000">
<ptxt>INSTALL REAR DIFFERENTIAL CASE BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a press, press the RH side bearing (inner race) into the differential case.</ptxt>
<figure>
<graphic graphicname="C161847E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09710-30050</s-number>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using SST and a press, press the LH side bearing (inner race) into the differential case.</ptxt>
<figure>
<graphic graphicname="C161848E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09710-30050</s-number>
</sstitem>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00480</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0004" proc-id="RM22W0E___00009IT00000">
<ptxt>INSTALL DIFFERENTIAL RING GEAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the threads of the bolts and differential case with non-residue solvent.</ptxt>
</s2>
<s2>
<ptxt>Clean the contact surfaces of the differential case and ring gear.</ptxt>
</s2>
<s2>
<ptxt>Heat the ring gear to about 100°C (212°F) in boiling water.</ptxt>
<figure>
<graphic graphicname="C161849E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Carefully take the ring gear out of the boiling water.</ptxt>
</s2>
<s2>
<ptxt>After the moisture on the ring gear has completely evaporated, quickly align the matchmarks on the ring gear and differential case and set the ring gear onto the differential case. Then temporarily install the 12 bolts so that the bolt holes in the ring gear and differential case are aligned.</ptxt>
</s2>
<s2>
<ptxt>After the ring gear cools down sufficiently, remove the 12 bolts. Then apply thread lock adhesive to the 12 bolts and install them.</ptxt>
<figure>
<graphic graphicname="C161840E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Thread lock</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1360K, Three Bond 1360K or equivalent</ptxt>
</specitem>
</spec>
<torque>
<torqueitem>
<t-value1>137</t-value1>
<t-value2>1397</t-value2>
<t-value4>101</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0005" proc-id="RM22W0E___00009IU00000">
<ptxt>CHECK DIFFERENTIAL RING GEAR RUNOUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the bearing outer races on their respective bearings. </ptxt>
<atten4>
<ptxt>Do not interchange the left and right outer races.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the assembled plate washers to the side bearing.</ptxt>
</s2>
<s2>
<ptxt>Install the differential case to the differential carrier.</ptxt>
<atten4>
<ptxt>If it is difficult to install the differential case to the carrier, replace the plate washer with a thinner one. However, select a plate washer that allows no clearance between it and the carrier.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Align the matchmarks on the bearing caps and differential carrier.</ptxt>
</s2>
<s2>
<ptxt>Install and uniformly tighten the 4 bolts a little at a time.</ptxt>
</s2>
<s2>
<ptxt>Using a dial indicator, check the ring runout.</ptxt>
<figure>
<graphic graphicname="C161850" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum runout</title>
<specitem>
<ptxt>0.05 mm (0.00197)</ptxt>
</specitem>
</spec>
<ptxt>If the runout is more than the maximum, replace the ring gear.</ptxt>
</s2>
<s2>
<ptxt>Remove the differential case.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0006" proc-id="RM22W0E___00009IV00000">
<ptxt>INSTALL REAR DRIVE PINION FRONT TAPERED ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a press, press in the bearing (outer race).</ptxt>
<figure>
<graphic graphicname="C161851E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-60020</s-number>
<s-subnumber>09951-00710</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0007" proc-id="RM22W0E___00009IW00000">
<ptxt>INSTALL REAR DRIVE PINION REAR TAPERED ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a press, press in the bearing (outer race).</ptxt>
<figure>
<graphic graphicname="C162726E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-60020</s-number>
<s-subnumber>09951-00890</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0008" proc-id="RM22W0E___00009IX00000">
<ptxt>INSTALL REAR DRIVE PINION REAR TAPERED ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the washer to the drive pinion.</ptxt>
<atten4>
<ptxt>After installing the washer, check the tooth contact pattern. If necessary, replace the washer with one of a different thickness.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using SST and a press, press the rear bearing (inner race) into the drive pinion.</ptxt>
<sst>
<sstitem>
<s-number>09506-35010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161852E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0009" proc-id="RM22W0E___00009IY00000">
<ptxt>INSPECT DIFFERENTIAL DRIVE PINION PRELOAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the drive pinion, front bearing (inner race) and oil slinger.</ptxt>
<figure>
<graphic graphicname="C028218" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Install the spacer and oil seal after adjusting the gear contact pattern.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using SST, install the companion flange.</ptxt>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03040</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161853E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST to hold the companion flange, install the nut.</ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
<s-subnumber>09330-00030</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161854E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>441</t-value1>
<t-value2>4497</t-value2>
<t-value4>325</t-value4>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Apply hypoid gear oil to the nut.</ptxt>
</item>
<item>
<ptxt>As there is no spacer, tighten a little at a time, being careful not to overtighten it.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Using a torque wrench, measure the preload.</ptxt>
<figure>
<graphic graphicname="C124126" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry align="center">
<ptxt>1.0 to 1.7 N*m (11 to 17 kgf*cm, 10 to 14 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry align="center">
<ptxt>0.9 to 1.4 N*m (9 to 13 kgf*cm, 8 to 12 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>Measure the total preload after turning the bearing clockwise and counterclockwise several times to make the bearing smooth.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0010" proc-id="RM22W0E___00009IZ00000">
<ptxt>INSTALL DIFFERENTIAL CASE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the bearing (outer races) on their respective bearings.</ptxt>
<atten4>
<ptxt>Do not interchange the left and right outer races.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the differential case to the carrier.</ptxt>
<atten4>
<ptxt>Make sure that there is backlash between the ring gear and drive pinion.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0011" proc-id="RM22W0E___00009J000000">
<ptxt>INSTALL REAR DIFFERENTIAL BEARING ADJUSTING NUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 adjusting nuts to the carrier, making sure the nuts are threaded properly.</ptxt>
<figure>
<graphic graphicname="C161855" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0012" proc-id="RM22W0E___00009J100000">
<ptxt>INSPECT AND ADJUST DIFFERENTIAL RING GEAR BACKLASH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the matchmarks on the bearing caps and carrier. Screw in the 4 bearing cap bolts 2 or 3 turns and press down the bearing caps by hand.</ptxt>
<figure>
<graphic graphicname="C161856E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>If the bearing caps do not fit tightly on the carrier, the adjusting nuts are not engaged properly.</ptxt>
</atten4>
<ptxt>Reinstall the adjusting nuts if necessary.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 4 bolts.</ptxt>
<figure>
<graphic graphicname="C161857" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>90</t-value1>
<t-value2>918</t-value2>
<t-value4>66</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Loosen the 4 bolts to the point where the adjusting nuts can be turned by SST.</ptxt>
<sst>
<sstitem>
<s-number>09504-00011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161858E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, tighten the adjusting nut on the ring gear side until the ring gear has a backlash of about 0.2 mm (0.00787 in.).</ptxt>
<sst>
<sstitem>
<s-number>09504-00011</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>While turning the ring gear, use SST to tighten the adjusting nut on the drive pinion side. After the bearings are settled, loosen the adjusting nut on the drive pinion side.</ptxt>
<figure>
<graphic graphicname="C161859E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Place a dial indicator on the top of the adjusting nut on the ring gear side.</ptxt>
<figure>
<graphic graphicname="C161860E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Adjust the side bearing to zero preload by tightening the other adjusting nut until the pointer on the indicator begins to move.</ptxt>
</s2>
<s2>
<ptxt>Using SST, tighten the adjusting nut 1 to 1.5 notches from the zero preload position.</ptxt>
<sst>
<sstitem>
<s-number>09504-00011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161859E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a dial indicator, adjust the ring gear backlash until it is within the specification.</ptxt>
<figure>
<graphic graphicname="C161861E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard backlash</title>
<specitem>
<ptxt>0.10 to 0.20 mm (0.00394 to 0.00787 in.)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The backlash is adjusted by turning the left and right adjusting nuts by an equal amount. For example, loosen the nut on the left side 1 notch and tighten the nut on the right side 1 notch.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Tighten the 4 bearing cap bolts.</ptxt>
<figure>
<graphic graphicname="C161857" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>90</t-value1>
<t-value2>918</t-value2>
<t-value4>66</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>After rotating the ring gear 5 turns or more, recheck the ring gear backlash.</ptxt>
<spec>
<title>Standard backlash</title>
<specitem>
<ptxt>0.10 to 0.20 mm (0.00394 to 0.00787 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0013" proc-id="RM22W0E___00009J200000">
<ptxt>INSPECT TOTAL PRELOAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a torque wrench, measure the preload with the teeth of the drive pinion and ring gear in contact.</ptxt>
<figure>
<graphic graphicname="C124126" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Drive Pinion Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry align="center">
<ptxt>Standard drive pinion preload plus 0.4 to 0.6 N*m (4 to 6 kgf*cm, 4 to 5 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry align="center">
<ptxt>Standard drive pinion preload plus 0.4 to 0.6 N*m (4 to 6 kgf*cm, 4 to 5 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0014" proc-id="RM22W0E___00009J300000">
<ptxt>INSPECT TOOTH CONTACT BETWEEN RING GEAR AND DRIVE PINION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat 3 or 4 teeth at 3 different positions on the ring gear with Prussian blue.</ptxt>
<figure>
<graphic graphicname="C161862" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the companion flange in both directions and inspect the ring gear for proper tooth contact.</ptxt>
<figure>
<graphic graphicname="C116070E03" width="7.106578999in" height="5.787629434in"/>
</figure>
<ptxt>If the teeth are not contacting properly, use the following table and select a proper washer for correction.</ptxt>
<spec>
<title>Standard Washer Thickness</title>
<table pgwide="1">
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>1.04 to 1.06 mm (0.0409 to 0.0417 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.315 to 1.335 mm (0.0518 to 0.0526 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.065 to 1.085 mm (0.0420 to 0.0427 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.34 to 1.36 mm (0.0528 to 0.0535 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.09 to 1.11 mm (0.0419 to 0.0437 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.365 to 1.385 mm (0.0537 to 0.0545 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.115 to 1.135 mm (0.0439 to 0.0447 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.39 to 1.41 mm (0.0547 to 0.0555 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.14 to 1.16 mm (0.0449 to 0.0457 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.415 to 1.435 mm (0.0558 to 0.0565 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.165 to 1.185 mm (0.0459 to 0.0467 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.44 to 1.46 mm (0.0567 to 0.0575 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.19 to 1.21 mm (0.0469 to 0.0476 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.465 to 1.485 mm (0.0577 to 0.0585 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.215 to 1.235 mm (0.0478 to 0.0486 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.49 to 1.51 mm (0.0587 to 0.0595 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.24 to 1.26 mm (0.0488 to 0.0496 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.515 to 1.535 mm (0.0597 to 0.0605 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.265 to 1.285 mm (0.0498 to 0.0506 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.54 to 1.56 mm (0.0607 to 0.0615 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.29 to 1.31 mm (0.0508 to 0.0516 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0030" proc-id="RM22W0E___00009JE00000">
<ptxt>REMOVE DRIVE PINION COMPANION FLANGE REAR NUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST to hold the flange, remove the nut.</ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
<s-subnumber>09330-00030</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161836E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0036" proc-id="RM22W0E___00009ID00000">
<ptxt>REMOVE REAR DRIVE PINION REAR COMPANION FLANGE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the companion flange.</ptxt>
<figure>
<graphic graphicname="C161837E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03040</s-subnumber>
</sstitem>
</sst>
</s2>
</content1></s-1>
<s-1 id="RM00000318I00OX_01_0032">
<ptxt>REMOVE REAR DIFFERENTIAL DRIVE PINION OIL SLINGER</ptxt>
</s-1>
<s-1 id="RM00000318I00OX_01_0037" proc-id="RM22W0E___00009IF00000">
<ptxt>REMOVE REAR DRIVE PINION FRONT TAPERED ROLLER BEARING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the front bearing (inner race) from the drive pinion.</ptxt>
<sst>
<sstitem>
<s-number>09556-22010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C124133E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If the front bearing is damaged or worn, replace the bearing.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000318I00OX_01_0019" proc-id="RM22W0E___00009J400000">
<ptxt>INSTALL REAR DIFFERENTIAL DRIVE PINION BEARING SPACER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new spacer to the drive pinion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0020" proc-id="RM22W0E___00009J500000">
<ptxt>INSTALL REAR DRIVE PINION FRONT TAPERED ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, tap in the bearing (outer race).</ptxt>
<sst>
<sstitem>
<s-number>09316-60011</s-number>
<s-subnumber>09316-00011</s-subnumber>
<s-subnumber>09316-00021</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161827E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the bearing (inner race).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0034">
<ptxt>INSTALL REAR DIFFERENTIAL DRIVE PINION OIL SLINGER</ptxt>
</s-1>
<s-1 id="RM00000318I00OX_01_0022" proc-id="RM22W0E___00009J600000">
<ptxt>INSTALL REAR DIFFERENTIAL CARRIER OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, tap in a new oil seal.</ptxt>
<figure>
<graphic graphicname="C161864E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09214-76011</s-number>
</sstitem>
</sst>
<spec>
<title>Standard oil seal depth</title>
<specitem>
<ptxt>0.05 to 0.95 mm (0.00197 to 0.0374 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Coat the lip of the oil seal with MP grease.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0023" proc-id="RM22W0E___00009J700000">
<ptxt>INSTALL REAR DRIVE PINION COMPANION FLANGE REAR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, install the companion flange.</ptxt>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03040</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161853E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0024" proc-id="RM22W0E___00009J800000">
<ptxt>INSPECT DRIVE PINION PRELOAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the threads of a new nut with hypoid gear oil.</ptxt>
</s2>
<s2>
<ptxt>Using SST to hold the companion flange, install the nut by tightening it until the standard preload is reached.</ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
<s-subnumber>09330-00030</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="ZK09978E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>441</t-value1>
<t-value2>4497</t-value2>
<t-value4>325</t-value4>
<ptxt>or less</ptxt>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a torque wrench, measure the preload.</ptxt>
<figure>
<graphic graphicname="C124126" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry align="center">
<ptxt>1.0 to 1.7 N*m (11 to 17 kgf*cm, 10 to 14 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry align="center">
<ptxt>0.9 to 1.4 N*m (9 to 13 kgf*cm, 8 to 12 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0025" proc-id="RM22W0E___00009J900000">
<ptxt>INSPECT TOTAL PRELOAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a torque wrench, measure the preload with the teeth of the drive pinion and ring gear in contact.</ptxt>
<figure>
<graphic graphicname="C124126" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Drive Pinion Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry align="center">
<ptxt>Standard drive pinion preload plus 0.4 to 0.6 N*m (4 to 6 kgf*cm, 4 to 5 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry align="center">
<ptxt>Standard drive pinion preload plus 0.4 to 0.6 N*m (4 to 6 kgf*cm, 4 to 5 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0026" proc-id="RM22W0E___00009JA00000">
<ptxt>INSPECT DIFFERENTIAL RING GEAR BACKLASH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the ring gear backlash.</ptxt>
<figure>
<graphic graphicname="C161835" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard backlash</title>
<specitem>
<ptxt>0.10 to 0.20 mm (0.00394 to 0.00787 in.)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Measure at 3 or more positions around the circumference of the ring gear.</ptxt>
</atten4>
<ptxt>If the backlash is not as specified, adjust the side bearing preload or repair as necessary.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0027" proc-id="RM22W0E___00009JB00000">
<ptxt>INSPECT RUNOUT OF REAR DRIVE PINION COMPANION FLANGE REAR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the runout of the companion flange vertically and laterally.</ptxt>
<figure>
<graphic graphicname="F001237E12" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum Runout</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Runout</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Vertical runout</ptxt>
</entry>
<entry align="center">
<ptxt>0.10 mm (0.00394 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Lateral runout</ptxt>
</entry>
<entry align="center">
<ptxt>0.10 mm (0.00394 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the runout is more than the maximum, replace the companion flange.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0028" proc-id="RM22W0E___00009JC00000">
<ptxt>STAKE DRIVE PINION COMPANION FLANGE REAR NUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a chisel and hammer, stake the nut.</ptxt>
<figure>
<graphic graphicname="C161318" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318I00OX_01_0029" proc-id="RM22W0E___00009JD00000">
<ptxt>INSTALL REAR DIFFERENTIAL BEARING ADJUSTING NUT LOCK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 2 new nut locks to the bearing caps.</ptxt>
<figure>
<graphic graphicname="C161866" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 2 bolts, and bend the nut locks.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>