<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3X7_T00QA" variety="T00QA">
<name>POWER WINDOW REGULATOR MOTOR (for Rear Door)</name>
<para id="RM000002STY027X" category="A" type-id="30014" name-id="WS89B-01" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000002STY027X_03" type-id="11" category="10" proc-id="RM22W0E___0000I1M00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0DSX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000002STY027X_01" type-id="01" category="01">
<s-1 id="RM000002STY027X_01_0042" proc-id="RM22W0E___0000I1H00001">
<ptxt>INSTALL REAR POWER WINDOW REGULATOR MOTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply MP grease to the sliding and rotating areas of the regulator motor.</ptxt>
</s2>
<s2>
<ptxt>Using a T25 "TORX" driver, install the motor with the 3 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>5.4</t-value1>
<t-value2>55</t-value2>
<t-value3>48</t-value3>
</torqueitem>
</torque>
<atten4>
<ptxt>A new rear window regulator motor uses self-tapping screws to thread new installation holes when the self-tapping screws are inserted.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002STY027X_01_0043" proc-id="RM22W0E___0000I1I00001">
<ptxt>INSTALL REAR DOOR WINDOW REGULATOR SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply MP grease to the sliding parts of the rear door window regulator sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180924E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temporary Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Loosely install the temporary bolt onto the rear door window regulator sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Insert the rear door window regulator sub-assembly LH into the door panel. Use the temporary bolt to hang the rear door window regulator sub-assembly LH on the door panel.</ptxt>
<atten3>
<ptxt>Be careful not to drop the rear door window regulator sub-assembly LH as it may become damaged.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Temporarily install the rear door window regulator sub-assembly LH with the 3 bolts.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 4 bolts in the order shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0016" proc-id="RM22W0E___0000I1D00001">
<ptxt>INSTALL REAR DOOR GLASS SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Slide the rear door glass sub-assembly LH as shown in the illustration to install it.</ptxt>
<figure>
<graphic graphicname="B180922" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0017" proc-id="RM22W0E___0000I1E00001">
<ptxt>INSTALL REAR DOOR QUARTER WINDOW GLASS
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear door quarter window glass LH together with the rear door quarter window weatherstrip LH in the direction indicated by the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="B183926" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0051" proc-id="RM22W0E___0000I1J00001">
<ptxt>INSTALL REAR DOOR REAR LOWER WINDOW FRAME SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear door rear lower window frame sub-assembly LH with the 2 bolts and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0018" proc-id="RM22W0E___0000I1F00001">
<ptxt>INSTALL REAR DOOR GLASS RUN
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear door glass run LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0020" proc-id="RM22W0E___0000E5900001">
<ptxt>INSTALL REAR DOOR SERVICE HOLE COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply butyl tape to the door.</ptxt>
</s2>
<s2>
<ptxt>Pass the rear door lock remote control cable assembly LH and rear door inside locking cable assembly LH through a new rear door service hole cover LH.</ptxt>
<figure>
<graphic graphicname="B180918" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When installing the rear door service hole cover LH, pull the links and connectors through the rear door service hole cover LH.</ptxt>
</item>
<item>
<ptxt>There should be no wrinkles or folds after attaching the rear door service hole cover LH.</ptxt>
</item>
<item>
<ptxt>After attaching the rear door service hole cover LH, check the sealing quality.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Connect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 6 clamps.</ptxt>
</s2>
<s2>
<ptxt>Install the bolt as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B180916E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>8.4</t-value1>
<t-value2>86</t-value2>
<t-value3>74</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0025" proc-id="RM22W0E___0000BPI00000">
<ptxt>INSTALL REAR SPEAKER SET (w/ Rear Speaker)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the speaker by attaching the claw of the speaker to the door panel.</ptxt>
<figure>
<graphic graphicname="B181520" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the speaker with the 3 screws.</ptxt>
<atten3>
<ptxt>Do not touch the cone part of the speaker.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0053" proc-id="RM22W0E___0000I1L00001">
<ptxt>INSTALL REAR  INNER DOOR GLASS WEATHERSTRIP
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear inner door glass weatherstrip LH to the door panel.</ptxt>
<figure>
<graphic graphicname="B180917E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0026" proc-id="RM22W0E___0000BPJ00001">
<ptxt>INSTALL REAR DOOR TRIM BOARD SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the rear door lock remote control cable assembly LH and rear door inside locking cable assembly LH to the rear door inside handle sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B183925" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 4 claws and 9 clips to install the rear door trim board sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180912" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0027" proc-id="RM22W0E___0000BPN00001">
<ptxt>INSTALL ASSIST GRIP COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 9 claws to install the assist grip cover LH to the rear door trim board sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0028" proc-id="RM22W0E___0000BPK00000">
<ptxt>INSTALL REAR DOOR ARMREST BASE PANEL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector. </ptxt>
</s2>
<s2>
<ptxt>Attach the 7 claws to install the armrest base panel.</ptxt>
<figure>
<graphic graphicname="B183923" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0052" proc-id="RM22W0E___0000BPL00001">
<ptxt>INSTALL REAR DOOR INSIDE HANDLE BEZEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt> Attach the 4 claws to install the rear door inside handle bezel LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002STY027X_01_0039" proc-id="RM22W0E___0000I1G00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>