<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C3ZV_T00SY" variety="T00SY">
<name>FRONT BUMPER (for Standard)</name>
<para id="RM0000038JZ013X" category="A" type-id="8000E" name-id="ET8EB-01" from="201301" to="201308">
<name>REASSEMBLY</name>
<subpara id="RM0000038JZ013X_01" type-id="11" category="10" proc-id="RM22W0E___0000JIR00000">
<content3 releasenbr="1">
<atten4>
<ptxt>When installing the front bumper protector, heat the front bumper surface using a heat light.</ptxt>
</atten4>
<spec>
<title>Standard Heating Temperature</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Front Bumper</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not heat the front bumper excessively.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM0000038JZ013X_02" type-id="01" category="01">
<s-1 id="RM0000038JZ013X_02_0001" proc-id="RM22W0E___0000JIS00000">
<ptxt>INSTALL FRONT NO. 2 BUMPER PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the front bumper surface.</ptxt>
<s3>
<ptxt>Using a heat light, heat the front bumper cover surface.</ptxt>
</s3>
<s3>
<ptxt>Remove the double-sided tape from the front bumper cover.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any tape adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install a new front No. 2 bumper protector.</ptxt>
<figure>
<graphic graphicname="B302162" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the peeling paper from the face of 2 new front No. 2 bumper protectors.</ptxt>
<atten4>
<ptxt>After removing the peeling paper, keep the exposed adhesive free from foreign matter.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Install the 2 front No. 2 bumper protectors as shown in the illustration.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0014" proc-id="RM22W0E___0000JIX00000">
<ptxt>INSTALL FRONT BUMPER GUARD (w/ Garnish)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 6 clips to install the  front bumper guard.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 nuts.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0015" proc-id="RM22W0E___0000JIY00000">
<ptxt>INSTALL FRONT BUMPER EXTENSION MOUNTING BRACKET (w/ Bracket)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws.</ptxt>
</s2>
<s2>
<ptxt>Install  the front bumper extension mounting bracket with the 2 bolts.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0007" proc-id="RM22W0E___0000JIV00000">
<ptxt>INSTALL LOWER NO. 1 RADIATOR GRILLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 16 claws to install the lower No. 1 radiator grille.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 retainers.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0023" proc-id="RM22W0E___0000JJ100000">
<ptxt>INSTALL FRONT BUMPER HOLE COVER LH (w/ Fog Light)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 5 claws to the front bumper hole cover LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws and 2 retainers.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0024" proc-id="RM22W0E___0000JJ200000">
<ptxt>INSTALL FRONT BUMPER HOLE COVER RH (w/ Fog Light)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0016" proc-id="RM22W0E___0000JIZ00000">
<ptxt>INSTALL FRONT BUMPER HOLE COVER LH (w/o Fog Light)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 5 claws to the front bumper hole cover LH.</ptxt>
</s2>
<s2>
<ptxt>Install the screw and 2 retainers.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0017" proc-id="RM22W0E___0000JJ000000">
<ptxt>INSTALL FRONT BUMPER HOLE COVER RH (w/o Fog Light)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0020" proc-id="RM22W0E___0000IZS00000">
<ptxt>INSTALL FOG LIGHT ASSEMBLY LH (w/ Fog Light)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 guides and install the fog light assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038JZ013X_02_0010" proc-id="RM22W0E___0000JIW00000">
<ptxt>INSTALL FOG LIGHT ASSEMBLY RH (w/ Fog Light)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0026" proc-id="RM22W0E___0000IZV00000">
<ptxt>INSTALL FRONT BUMPER INNER BRACKET (w/ Fog Light)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws and install the front bumper inner bracket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0021" proc-id="RM22W0E___0000CTF00000">
<ptxt>INSTALL NO. 2 ULTRASONIC SENSOR RETAINER (w/ TOYOTA Parking Assist-sensor System)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to install the No. 2 ultrasonic sensor retainer on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Temporarily install the No. 2 ultrasonic sensor retainer to the front bumper.</ptxt>
<figure>
<graphic graphicname="E244160E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Keyhole</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not damage the bumper with the protrusion when installing the retainer.</ptxt>
</item>
<item>
<ptxt>Securely install the No. 2 ultrasonic sensor retainer so that there are no gaps between the retainer and surface of the front bumper.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Attach the 2 claws to install the No. 2 ultra sonic sensor retainer.</ptxt>
<figure>
<graphic graphicname="E244159" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038JZ013X_02_0022" proc-id="RM22W0E___0000CTG00000">
<ptxt>INSTALL NO. 1 ULTRASONIC SENSOR (w/ TOYOTA Parking Assist-sensor System)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to install the No. 1 ultrasonic sensor on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the ultrasonic sensor.</ptxt>
<atten3>
<ptxt>Push the No. 2 ultrasonic sensor retainer from the outside of the bumper when there is a gap between the retainer and the bumper surface. In this case, do not push on the No. 1 ultrasonic sensor.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000038JZ013X_02_0028" proc-id="RM22W0E___0000CTI00000">
<ptxt>INSTALL ULTRASONIC SENSOR CLIP (w/ TOYOTA Parking Assist-sensor System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws to install the ultrasonic sensor clip.</ptxt>
<atten3>
<ptxt>Hold the No. 1 ultrasonic sensor in place so that the No. 2 ultrasonic sensor retainer does not come out when installing the ultrasonic sensor clip.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000038JZ013X_02_0013" proc-id="RM22W0E___0000CTJ00000">
<ptxt>INSTALL NO. 4 ENGINE ROOM WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o TOYOTA Parking Assist-sensor System, w/ Fog Light:</ptxt>
<ptxt>Install the No. 4 engine room wire.</ptxt>
<s3>
<ptxt>Attach the 11 clamps to install the No. 4 engine room wire.</ptxt>
</s3>
<s3>
<ptxt>Connect the 2 connectors.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/ Fog Light:</ptxt>
<ptxt>Install the No. 4 engine room wire.</ptxt>
<s3>
<ptxt>Attach the 12 clamps to install the No. 4 engine room wire.</ptxt>
</s3>
<s3>
<ptxt>Connect the 4 connectors.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/o Fog Light:</ptxt>
<ptxt>Install the No. 4 engine room wire.</ptxt>
<s3>
<ptxt>Attach the 12 clamps to install the No. 4 engine room wire.</ptxt>
</s3>
<s3>
<ptxt>Connect the 2 connectors.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0018" proc-id="RM22W0E___0000IUB00000">
<ptxt>INSTALL HEADLIGHT WASHER ACTUATOR SUB-ASSEMBLY LH (w/ Headlight Cleaner System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws.</ptxt>
</s2>
<s2>
<ptxt>Install the headlight washer actuator sub-assembly LH with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>1.1</t-value1>
<t-value2>11</t-value2>
<t-value3>10</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000038JZ013X_02_0004" proc-id="RM22W0E___0000JIT00000">
<ptxt>INSTALL HEADLIGHT WASHER ACTUATOR SUB-ASSEMBLY RH (w/ Headlight Cleaner System)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0019" proc-id="RM22W0E___0000IUC00000">
<ptxt>INSTALL TYPE 1 HEADLIGHT WASHER NOZZLE SUB-ASSEMBLY LH (w/ Headlight Cleaner System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the type 1 headlight washer nozzle sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038JZ013X_02_0006" proc-id="RM22W0E___0000JIU00000">
<ptxt>INSTALL TYPE 1 HEADLIGHT WASHER NOZZLE SUB-ASSEMBLY RH (w/ Headlight Cleaner System)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JZ013X_02_0025" proc-id="RM22W0E___0000IUG00000">
<ptxt>INSTALL HEADLIGHT CLEANER HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 clamps and install the headlight cleaner hose.</ptxt>
</s2>
<s2>
<ptxt>Connect the headlight cleaner hose.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>