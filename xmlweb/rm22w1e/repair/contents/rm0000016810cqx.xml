<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3XC_T00QF" variety="T00QF">
<name>POWER WINDOW CONTROL SYSTEM (for Models with Jam Protection Function on Driver Door Window Only)</name>
<para id="RM0000016810CQX" category="D" type-id="303FF" name-id="WS004N-100" from="201301">
<name>OPERATION CHECK</name>
<subpara id="RM0000016810CQX_z0" proc-id="RM22W0E___0000I3H00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK WINDOW LOCK SWITCH</ptxt>
<atten4>
<ptxt>Before performing the window lock switch operation check, make sure that the window lock switch is off (the switch is not pushed in).</ptxt>
</atten4>
<figure>
<graphic graphicname="B156493" width="2.775699831in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Check that the front passenger side power window and the rear power windows cannot be operated when the window lock switch of the multiplex network master switch is pressed.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Operation of front passenger side power window and rear power windows are disabled.</ptxt>
</specitem>
</spec>
</step2>
<step2>
<ptxt>Check that the front passenger side power window and the rear power windows can be operated when the window lock switch is pressed again.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Front passenger side power window and rear power windows can be operated.</ptxt>
</specitem>
</spec>
</step2>
</step1>
<step1>
<ptxt>CHECK MANUAL UP/DOWN FUNCTION</ptxt>
<step2>
<ptxt>Check that the driver side power window operates as follows:</ptxt>
<table pgwide="1">
<title>OK</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Multiplex Network Master Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power Window</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Driver side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulled halfway up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Up (Closes)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pushed halfway down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Down (Opens)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Check that the front passenger side and rear power windows operate as follows:</ptxt>
<table pgwide="1">
<title>OK</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power Window Regulator Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power Window</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="5" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Window lock switch off</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Front passenger side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulled halfway up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Up (Closes)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pushed halfway down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Down (Opens)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear LH side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulled halfway up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Up (Closes)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pushed halfway down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Down (Opens)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear RH side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulled halfway up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Up (Closes)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pushed halfway down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Down (Opens)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>CHECK AUTO UP/DOWN FUNCTION (for driver side)</ptxt>
<step2>
<ptxt>Check that the driver side power window operates as follows:</ptxt>
<table pgwide="1">
<title>OK</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Multiplex Network Master Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power Window</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Driver side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulled up (One touch operation)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Auto up (Closes)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pushed down (One touch operation)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Auto down (Opens)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>CHECK REMOTE MANUAL UP/DOWN FUNCTION</ptxt>
<step2>
<ptxt>Check that the front passenger side and rear power windows operate as follows:</ptxt>
<table pgwide="1">
<title>OK</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Multiplex Network Master Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power Window</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="5" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Window lock switch off</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Front passenger side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulled halfway up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Up (Closes)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pushed halfway down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Down (Opens)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear LH side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulled halfway up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Up (Closes)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pushed halfway down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Down (Opens)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear RH side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulled halfway up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Up (Closes)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pushed halfway down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Down (Opens)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>CHECK POWER WINDOW OPERATION FUNCTION AFTER IGNITION SWITCH OFF</ptxt>
<step2>
<ptxt>Check that all the power windows can be operated with the multiplex network master switch after the ignition switch is turned off.</ptxt>
</step2>
<step2>
<ptxt>Check that the key-off operation function does not operate after a front side door is opened.</ptxt>
</step2>
<step2>
<ptxt>Check that all the power windows cannot be operated after more than approximately 45 seconds have elapsed since the ignition switch was turned off.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CHECK JAM PROTECTION FUNCTION (for driver side)</ptxt>
<step2>
<ptxt>Check the basic function.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>The jam protection function is operative during both the auto up and manual up operation.</ptxt>
</item>
<item>
<ptxt>Do not put your fingers between the door frame and door glass to check functions. Also, prevent any part of your body from being caught during inspection.</ptxt>
</item>
</list1>
</atten2>
<atten4>
<ptxt>The jam protection function is operative during both the auto up and manual up operation while the engine is running or the ignition switch is ON, and also 45 seconds after the ignition switch is off.</ptxt>
</atten4>
<step3>
<ptxt>Fully open the window.</ptxt>
</step3>
<step3>
<ptxt>Hold a thick book wrapped with cloth in the position shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B154743E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</step3>
<step3>
<ptxt>Operate the auto up or manual up function to check that the power window goes down when the window contacts the book, and stops when the opening reaches approximately 200 mm (7.87 in.).</ptxt>
<atten4>
<ptxt>The power window moves down approximately 125 mm (4.93 in.). However, when the opening does not reach 200 mm (7.87 in.), the power window continues to go down until the opening reaches 200 mm (7.87 in.) or until approximately 5 seconds. have elapsed.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>While the power window is moving down, check that the door glass cannot be moved up even when the power window regulator switch is used.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK POWER WINDOW REGULATOR SWITCH LED ILLUMINATION</ptxt>
<step2>
<ptxt>Check the LED illumination.</ptxt>
<step3>
<ptxt>Check that the LED located on the power window regulator switch illuminates when the ignition switch is ON and the window lock switch is off.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK PTC OPERATION</ptxt>
<atten4>
<ptxt>PTC operation is a function that prevents overloading of the power window regulator by stopping the power window motor. PTC operation activates when the power window regulator switch is operated for a predetermined amount of time.</ptxt>
</atten4>
<step2>
<ptxt>Disconnect the power window regulator motor.</ptxt>
</step2>
<step2>
<ptxt>Connect the ammeter's positive (+) lead to terminal 2 of the wire harness side connector and the negative (-) lead to battery's negative terminal.</ptxt>
</step2>
<step2>
<ptxt>Connect the battery's positive (+) lead to terminal 1 of the wire harness side connector, and raise that window to the fully closed position.</ptxt>
</step2>
<step2>
<ptxt>Continue to apply voltage, and check that the current changes to less than 1A within 4 to 90 seconds.</ptxt>
</step2>
<step2>
<ptxt>Disconnect the leads from the terminals.</ptxt>
</step2>
<step2>
<ptxt>Approximately 60 seconds later, connect the battery's positive (+) lead to terminal 2 and the negative (-) lead to terminal 1, and check that the window begins to lower.</ptxt>
<atten3>
<ptxt>If the result is not as specified, replace the power window regulator motor.</ptxt>
</atten3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>