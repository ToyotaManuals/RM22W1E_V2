<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002LIR03UX" category="D" type-id="303F2" name-id="AC3ST-02" from="201301">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000002LIR03UX_z0" proc-id="RM22W0E___0000H5400000">
<content5 releasenbr="1">
<step1>
<ptxt>GENERAL</ptxt>
<step2>
<ptxt>The air conditioning system has the following features:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The 4-ZONE air conditioner system controls the temperature of the front and rear seats independently. In addition, it uses the amount of sunlight and temperature of each seat to adjust the temperature and airflow volume. When the 4-ZONE switch is on, the temperature, the airflow mode and airflow volume of each of the 4 seats (driver, front passenger, rear left, rear right) are automatically adjusted.</ptxt>
</item>
<item>
<ptxt>The 2-ZONE air conditioner system controls the temperature of the left and right seats independently, and sets the temperature for the driver seat and front passenger seat. In addition, it uses the amount of sunlight and temperature of the left and right seats to adjust the temperature and airflow volume.</ptxt>
<ptxt>When the 2-ZONE switch is on, the temperature, airflow mode and airflow volume of the driver seat and front passenger seat are automatically adjusted.</ptxt>
</item>
<item>
<ptxt>In accordance with the temperature set using the temperature control switch, the air conditioning amplifier assembly determines the outlet temperature based on the input signals from various sensors. In addition, corrections are made in accordance with the signals from the water temperature sensor to control the outlet air temperature.</ptxt>
</item>
<item>
<ptxt>Controls the blower motor in accordance with the airflow volume determined by the air conditioning amplifier assembly based on the input signals from various sensors.</ptxt>
</item>
<item>
<ptxt>Automatically changes the outlets in accordance with the outlet mode ratio that is determined by the air conditioning amplifier assembly based on the input signals from various sensors.</ptxt>
</item>
<item>
<ptxt>Based on the signals from the ambient temperature sensor, this system calculates the outside temperature and indicates it in the multi-information display in the combination meter.</ptxt>
</item>
<item>
<ptxt>For vehicles with the rear air conditioner, there are rear side and roof side air ducts, and independent temperature and airflow mode adjustments are automatically performed.</ptxt>
</item>
<item>
<ptxt>The Positive Temperature Coefficient (PTC) heater system contains a PTC heater that heats the air that has passed through the heater core to ensure proper heater performance.</ptxt>
</item>
<item>
<ptxt>A compact, lightweight, and highly efficient straight flow (full-path flow) aluminum heater core is used.</ptxt>
</item>
<item>
<ptxt>The micro dust and pollen filter control is used to remove pollen in the air around the upper areas of the driver and front passenger seats.</ptxt>
</item>
<item>
<ptxt>The air conditioning amplifier assembly is equipped with a self diagnosis function. If there is a malfunction in the system, it stores the DTCs (Diagnostic Trouble Codes) in its memory.</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>MODE POSITION AND DAMPER OPERATION</ptxt>
<figure>
<graphic graphicname="E157039E03" width="7.106578999in" height="7.795582503in"/>
</figure>
</step1>
<table pgwide="1">
<title>Function of Main Damper</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.30in"/>
<colspec colname="COL3" colwidth="1.51in"/>
<colspec colname="COL5" colwidth="0.85in"/>
<colspec colname="COL6" colwidth="3.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operation Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damper Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Air Inlet Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fresh</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Brings in fresh air.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Recirculation</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Recirculates internal air.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air Mix Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Max Cool to Max Hot</ptxt>
</entry>
<entry valign="middle">
<ptxt>K, L, M</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Varies the mixture ratio of cold air and hot air in order to regulate the temperature continuously from hot to cool.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Cool Air Bypass Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Max Cool to Max Hot</ptxt>
</entry>
<entry valign="middle">
<ptxt>H, I, J</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Cool air blows out from the front center register and side registers in order to adjust the temperature around the heads of the occupants during cooling or warming.</ptxt>
</entry>
</row>
<row>
<entry morerows="4" valign="middle">
<ptxt>Mode Control Damper</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E106660" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Def</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Defrosts the windshield through the center defroster.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106659" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot/Def</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Defrosts the windshield through the center defroster and side defroster while air is also blown out from the front and rear footwell register ducts.</ptxt>
<ptxt>In addition, air blows out slightly from the front and rear center registers and side register.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106658" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot</ptxt>
</entry>
<entry valign="middle">
<ptxt>E</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the front and rear footwell register duct, front and rear center register and side register.</ptxt>
<ptxt>In addition, air blows out slightly from the center defroster and side defroster.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106657" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Bi - Level</ptxt>
</entry>

<entry valign="middle">
<ptxt>F</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the center registers, side register, and rear center register to defrost the windshield.</ptxt>
<ptxt>Air also blows out from the front and rear footwell register ducts.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Face</ptxt>
</entry>
<entry valign="middle">
<ptxt>G</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the center registers and side registers to defrost the windshield.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="E157040E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<title>Function of Main Damper</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.22in"/>
<colspec colname="COL3" colwidth="1.67in"/>
<colspec colname="COL5" colwidth="0.92in"/>
<colspec colname="COL6" colwidth="3.27in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operation Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damper Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle">
<ptxt>Mode Control Damper</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Face</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the rear roof side register.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106657" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Bi - Level</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the rear roof side register and rear footwell register.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106658" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the rear footwell register.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air Mix Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Max Cool to Max Hot</ptxt>
</entry>
<entry valign="middle">
<ptxt>D, E</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Varies the mixture ratio of cold air and hot air in order to regulate the temperature continuously from hot to cool.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<step1>
<ptxt>AIR OUTLETS AND AIRFLOW VOLUME</ptxt>
<figure>
<graphic graphicname="E155416E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="10" align="center">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="0.71in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="0.71in"/>
<colspec colname="COL5" colwidth="0.71in"/>
<colspec colname="COL6" colwidth="0.71in"/>
<colspec colname="COL7" colwidth="0.71in"/>
<colspec colname="COL8" colwidth="0.71in"/>
<colspec colname="COL9" colwidth="0.69in"/>
<colspec colname="COLSPEC0" colwidth="0.710000000000001in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" morerows="2" valign="middle">
<ptxt>Air Outlet Mode</ptxt>
</entry>
<entry namest="COL3" nameend="COL4" valign="middle">
<ptxt>Selectable Mode</ptxt>
</entry>
<entry namest="COL5" nameend="COL6" valign="middle">
<ptxt>Register</ptxt>
</entry>
<entry namest="COL7" nameend="COL9" valign="middle">
<ptxt>Register Duct</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Defroster</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Automatic</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Manual</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Foot</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Face</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Foot</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
<entry valign="middle">
<ptxt>E</ptxt>
</entry>
<entry valign="middle">
<ptxt>F</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>FACE-U*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FACE-L*2, *6</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<graphic graphicname="E106657" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>B/L-U*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B/L*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle">
<graphic graphicname="E106658" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>FOOT F*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155418" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FOOT R*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155418" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FOOT D*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155418" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106659" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>F/D</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155418" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106660" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>DEF</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<ptxt>○</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The size of the circle indicates the proportion of airflow volume.</ptxt>
</item>
<item>
<ptxt>*1: Greater airflow volume at the upper area.</ptxt>
</item>
<item>
<ptxt>*2: Greater airflow volume at the lower area.</ptxt>
</item>
<item>
<ptxt>*3: Greater airflow volume at the front.</ptxt>
</item>
<item>
<ptxt>*4: Greater airflow volume at the rear foot.</ptxt>
</item>
<item>
<ptxt>*5: Greater airflow volume at the defroster.</ptxt>
</item>
<item>
<ptxt>*6: In contrast to FACE-U, a small amount of air comes out of the front footwell register duct.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="E155417E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="7" align="center">
<colspec colname="COL1" colwidth="1.01in"/>
<colspec colname="COL2" colwidth="1.01in"/>
<colspec colname="COL3" colwidth="1.01in"/>
<colspec colname="COL4" colwidth="1.01in"/>
<colspec colname="COLSPEC0" colwidth="1.01in"/>
<colspec colname="COL5" colwidth="1.01in"/>
<colspec colname="COL6" colwidth="1.02in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" morerows="2" valign="middle">
<ptxt>Air Outlet Mode</ptxt>
</entry>
<entry namest="COL3" nameend="COL6" valign="middle">
<ptxt>Register Duct</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Roof</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Foot 1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Side 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Foot 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>C Pillar</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>A</ptxt>
</entry>
<entry>
<ptxt>B</ptxt>
</entry>
<entry>
<ptxt>C</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
<entry valign="middle">
<ptxt>E</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>FACE</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106657" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>BI-LEVEL</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155418" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155418" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106658" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>FOOT</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<graphic graphicname="E155420" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The size of the circle indicates the proportion of airflow volume.</ptxt>
</atten4>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>