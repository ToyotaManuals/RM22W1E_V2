<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12013_S0016" variety="S0016">
<name>3UR-FE STARTING</name>
<ttl id="12013_S0016_7C3LK_T00EN" variety="T00EN">
<name>STARTER</name>
<para id="RM000002MDL034X" category="G" type-id="3001K" name-id="ST5P4-01" from="201301">
<name>INSPECTION</name>
<subpara id="RM000002MDL034X_01" type-id="01" category="01">
<s-1 id="RM000002MDL034X_01_0001" proc-id="RM22W0E___000074T00000">
<ptxt>INSPECT STARTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>As a large electric current passes through the cable during this inspection, a thick cable must be used. Otherwise, the cable may become hot and cause injury.</ptxt>
</atten2>
<atten3>
<ptxt>These tests must be performed within 3 to 5 seconds to avoid burning out the coil.</ptxt>
</atten3>
<s2>
<ptxt>Perform a pull-in test.</ptxt>
<s3>
<ptxt>Remove the nut, and then disconnect the field coil lead wire from terminal C.</ptxt>
</s3>
<s3>
<ptxt>Connect the battery to the magnet starter switch as shown in the illustration. Then check that the clutch pinion gear moves outward.</ptxt>
<figure>
<graphic graphicname="A258308E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal 50</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the clutch pinion gear does not move outward, replace the magnet starter switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Perform a holding test.</ptxt>
<s3>
<ptxt>When the battery is connected as above with the clutch pinion gear out, disconnect the negative (-) terminal lead from terminal C. Check that the pinion gear remains out.</ptxt>
<figure>
<graphic graphicname="A258309E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the clutch pinion gear moves inward, replace the magnet starter switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the clutch pinion gear return.</ptxt>
<s3>
<ptxt>Disconnect the negative (-) lead from the starter body. Check that the clutch pinion gear returns inward.</ptxt>
<figure>
<graphic graphicname="A258310" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>If the clutch pinion gear does not return inward, replace the magnet starter switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Perform an operation test without load.</ptxt>
<s3>
<ptxt>Connect the field coil lead wire to terminal C with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Mount the starter in a vise between aluminum plates.</ptxt>
</s3>
<s3>
<ptxt>Connect the battery and an ammeter to the starter as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A258311E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal 30</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal 50</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Check that the starter rotates smoothly and steadily while the pinion gear is moving outward. Then measure the current.</ptxt>
<spec>
<title>Standard current</title>
<specitem>
<ptxt>150 A or less at 11.5 V</ptxt>
</specitem>
</spec>
<ptxt>If the result is not as specified, replace the starter assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002MDL034X_01_0007" proc-id="RM22W0E___000074X00000">
<ptxt>INSPECT MAGNET STARTER SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check if the pull-in coil has an open circuit.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="A162877E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal 50 - Terminal C</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal 50</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the magnet starter switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check if the holding coil has an open circuit.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="A162878E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal 50 - Switch body</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 2 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal 50</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the magnet starter switch assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002MDL034X_01_0002" proc-id="RM22W0E___000074U00000">
<ptxt>INSPECT STARTER ARMATURE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the surface of the commutator for dirt and burns.</ptxt>
<ptxt>If the surface is dirty or burnt, correct it with sandpaper (No. 400) or a lathe.</ptxt>
<ptxt>If necessary, replace the starter armature assembly.</ptxt>
</s2>
<s2>
<ptxt>Inspect the commutator for an open circuit.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="A215965E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Segment - Segment</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Segment</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the starter armature assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the commutator for a short circuit.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="A215966E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Segment - Armature coil core</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Armature Coil Core</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Commutator</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the starter armature assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the commutator circle runout.</ptxt>
<s3>
<ptxt>Place the armature shaft on V-blocks.</ptxt>
<figure>
<graphic graphicname="A215967" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Using a dial indicator, measure the circle runout.</ptxt>
<spec>
<title>Maximum runout</title>
<specitem>
<ptxt>0.05 mm (0.00197 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the circle runout is more than the maximum, replace the starter armature assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Using a vernier caliper, measure the commutator diameter.</ptxt>
<figure>
<graphic graphicname="A215958" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>29.0 mm (1.14 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum diameter</title>
<specitem>
<ptxt>28.0 mm (1.10 in.)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Be sure to measure the commutator diameter at the ridges.</ptxt>
</atten4>
<ptxt>If the diameter is less than the minimum, replace the starter armature assembly.</ptxt>
</s2>
<s2>
<ptxt>Check that the undercut portion is clean and free of foreign matter. Smooth out the edge.</ptxt>
<figure>
<graphic graphicname="A062965E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard undercut depth</title>
<specitem>
<ptxt>0.4 mm (0.0157 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum undercut depth</title>
<specitem>
<ptxt>0.2 mm (0.00787 in.)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Be sure to measure the commutator undercut depth at a cutout.</ptxt>
</atten4>
<ptxt>If the undercut depth is less than the minimum, increase the depth using a hacksaw blade.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002MDL034X_01_0005" proc-id="RM22W0E___000074V00000">
<ptxt>INSPECT STARTER BRUSH HOLDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the brush length.</ptxt>
<figure>
<graphic graphicname="A215975" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard length</title>
<specitem>
<ptxt>14.4 mm (0.567 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum length</title>
<specitem>
<ptxt>9.0 mm (0.354 in.)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Be sure to measure the brush lengths at the protrusions.</ptxt>
</atten4>
<ptxt>If the length is less than the minimum, replace the starter brush holder assembly.</ptxt>
</s2>
<s2>
<ptxt>Inspect the load of the brush spring.</ptxt>
<s3>
<ptxt>Take a pull scale reading immediately after the brush spring separates from the brush.</ptxt>
<figure>
<graphic graphicname="ST00019E13" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard spring load</title>
<specitem>
<ptxt>22 to 27 N (2 to 3 kgf, 5.0 to 6.1 lbf)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum spring load</title>
<specitem>
<ptxt>14 N (1 kgf, 3.1 lbf)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brush</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brush Spring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the load is less than the minimum, replace the starter brush holder assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002MDL034X_01_0006" proc-id="RM22W0E___000074W00000">
<ptxt>INSPECT STARTER CENTER BEARING CLUTCH SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the gear teeth on the planetary gears, and the internal gear and pinion gear of the starter center bearing clutch for wear or damage.</ptxt>
<ptxt>If any of the gears is damaged, replace the gear or starter center bearing clutch sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Hold the starter clutch, rotate the pinion gear clockwise and check that it turns freely. Try to rotate the pinion gear counterclockwise and check that it locks.</ptxt>
<figure>
<graphic graphicname="A146951" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Free</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Lock</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the starter center bearing clutch sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>