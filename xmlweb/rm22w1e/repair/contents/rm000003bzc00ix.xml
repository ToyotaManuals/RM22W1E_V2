<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12027_S001J" variety="S001J">
<name>REAR SUSPENSION</name>
<ttl id="12027_S001J_7C3P0_T00I3" variety="T00I3">
<name>REAR UPPER ARM</name>
<para id="RM000003BZC00IX" category="A" type-id="30014" name-id="RP021-03" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000003BZC00IX_01" type-id="11" category="10" proc-id="RM22W0E___0000A5J00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the RH side and LH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0DSX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003BZC00IX_02" type-id="01" category="01">
<s-1 id="RM000003BZC00IX_02_0010" proc-id="RM22W0E___0000A5O00000">
<ptxt>TEMPORARILY INSTALL REAR UPPER CONTROL ARM ASSEMBLY LH (w/ Active Height Control)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the rear upper control arm and 2 washers with the 2 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="C174363" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZC00IX_02_0001" proc-id="RM22W0E___0000A5K00000">
<ptxt>TEMPORARILY INSTALL REAR UPPER CONTROL ARM ASSEMBLY LH (w/o Active Height Control)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the rear upper control arm and 2 washers with the 2 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="C178733" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZC00IX_02_0016" proc-id="RM22W0E___0000A4800000">
<ptxt>STABILIZE SUSPENSION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear wheels.</ptxt>
<torque>
<subtitle>for aluminum wheel</subtitle>
<torqueitem>
<t-value1>131</t-value1>
<t-value2>1336</t-value2>
<t-value4>97</t-value4>
</torqueitem>
<subtitle>for steel wheel</subtitle>
<torqueitem>
<t-value1>209</t-value1>
<t-value2>2131</t-value2>
<t-value4>154</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Lower the vehicle.</ptxt>
</s2>
<s2>
<ptxt>Press down on the vehicle several times to stabilize the suspension.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BZC00IX_02_0003" proc-id="RM22W0E___0000A5L00000">
<ptxt>TIGHTEN REAR UPPER CONTROL ARM ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Tighten the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>150</t-value1>
<t-value2>1530</t-value2>
<t-value4>111</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Perform this procedure with all 4 wheels on the ground.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZC00IX_02_0004" proc-id="RM22W0E___0000A5M00000">
<ptxt>INSTALL REAR STABILIZER CONTROL TUBE INSULATOR (w/ KDSS)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear stabilizer control tube insulator with the 2 bolts.</ptxt>
<figure>
<graphic graphicname="C174360" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZC00IX_02_0011" proc-id="RM22W0E___0000A5P00000">
<ptxt>INSTALL UPPER ARM BUSH HEAT INSULATOR (w/o KDSS)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the upper arm bush heat insulator with the bolt.</ptxt>
<figure>
<graphic graphicname="C180039" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZC00IX_02_0005" proc-id="RM22W0E___0000A5N00000">
<ptxt>CONNECT SPEED SENSOR WIRE HARNESS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the speed sensor wire harness with the bolt.</ptxt>
<figure>
<graphic graphicname="C174362" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZC00IX_02_0012" proc-id="RM22W0E___00009XS00000">
<ptxt>INSTALL REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH (w/ Active Height Control)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C172823" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Install the sensor with the 2 bolts and nut.</ptxt>
<torque>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
<subtitle>for nut</subtitle>
<torqueitem>
<t-value1>5.6</t-value1>
<t-value2>57</t-value2>
<t-value3>50</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector and 2 clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BZC00IX_02_0013" proc-id="RM22W0E___0000A5Q00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL (w/ Active Height Control)</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003BZC00IX_02_0014" proc-id="RM22W0E___0000A5R00000">
<ptxt>PERFORM VEHICLE OFF SET CALIBRATION (w/ Active Height Control)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Perform the vehicle off set calibration (See page <xref label="Seep01" href="RM000003BJ600DX_01_0004"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BZC00IX_02_0015" proc-id="RM22W0E___00009XN00000">
<ptxt>ADJUST REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH (w/ Active Height Control)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make adjustments from the link that deviates the most from the specified vehicle height value.</ptxt>
</item>
<item>
<ptxt>When the front and rear are at the same level, make adjustments from the front first.</ptxt>
</item>
<item>
<ptxt>If adjustment cannot be completed through the vehicle height offset calibration, adjust the sensor link using the following procedure.</ptxt>
</item>
</list1>
</atten3>
<s2>
<figure>
<graphic graphicname="C176369" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Loosen the nut and adjust the link installation position by moving the height control sensor link up or down in the long hole of the bracket.</ptxt>
<atten4>
<ptxt>When the link is moved 1 mm (0.0394 in.), the vehicle height changes by approximately 2 mm (0.0787 in.).</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Tighten the nut of the height control sensor link.</ptxt>
<torque>
<torqueitem>
<t-value1>5.6</t-value1>
<t-value2>57</t-value2>
<t-value3>50</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003BZC00IX_02_0007" proc-id="RM22W0E___00009JQ00000">
<ptxt>MEASURE VEHICLE HEIGHT (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the inspection on a level surface.</ptxt>
</item>
<item>
<ptxt>Ensure that the wheels are on the ground and facing straight ahead.</ptxt>
</item>
<item>
<ptxt>Perform the inspection with the vehicle load completely on the suspension.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Perform this step with the fuel tank full.</ptxt>
</item>
<item>
<ptxt>If there are any parts installed to the vehicle which place any unbalanced load on the left or right side of the vehicle, remove them.</ptxt>
</item>
</list1>
</atten4>
<s2>
<ptxt>Set the tire pressure to the specified value(s) (See page <xref label="Seep01" href="RM000003DGS00LX"/>).</ptxt>
</s2>
<s2>
<ptxt>Bounce the vehicle to stabilize the suspension.</ptxt>
</s2>
<s2>
<ptxt>Measure the distance from the ground to the top of the bumper and calculate the difference in the vehicle height between left and right. Perform this procedure for both the front and rear wheels.</ptxt>
<figure>
<graphic graphicname="C171398" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Height difference of left and right sides</title>
<specitem>
<ptxt>15 mm (0.591 in.) or less</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>If not as specified, perform the vehicle tilt calibration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003BZC00IX_02_0008" proc-id="RM22W0E___00009W800000">
<ptxt>CLOSE STABILIZER CONTROL WITH ACCUMULATOR HOUSING SHUTTER VALVE (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the inspection on a level surface.</ptxt>
</item>
<item>
<ptxt>Ensure that the wheels are on the ground and facing straight ahead.</ptxt>
</item>
<item>
<ptxt>Perform the inspection with the vehicle load completely on the suspension.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Perform this step with the fuel tank full.</ptxt>
</item>
<item>
<ptxt>If there are any parts installed to the vehicle which place any unbalanced load on the left or right side of the vehicle, remove them.</ptxt>
</item>
</list1>
</atten4>
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, tighten the lower and upper chamber shutter valves of the stabilizer control with accumulator housing.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003BZC00IX_02_0009" proc-id="RM22W0E___00009VZ00000">
<ptxt>INSTALL STABILIZER CONTROL VALVE PROTECTOR (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172095" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the valve protector with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the clamp, and connect the connector to the valve protector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BZC00IX_02_0017" proc-id="RM22W0E___0000A5S00000">
<ptxt>ADJUST HEADLIGHT ASSEMBLY (w/ Active Height Control)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Standard:</ptxt>
<ptxt>Adjust the headlight (See page <xref label="Seep01" href="RM0000011MI096X"/>).</ptxt>
</s2>
<s2>
<ptxt>for HID:</ptxt>
<ptxt>Adjust the headlight (See page <xref label="Seep02" href="RM0000011MI095X"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>