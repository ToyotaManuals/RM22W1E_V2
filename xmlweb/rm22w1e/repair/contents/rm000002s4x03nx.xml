<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S0010" variety="S0010">
<name>1UR-FE LUBRICATION</name>
<ttl id="12012_S0010_7C3KO_T00DR" variety="T00DR">
<name>OIL PUMP</name>
<para id="RM000002S4X03NX" category="A" type-id="80001" name-id="LU3VR-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM000002S4X03NX_01" type-id="01" category="01">
<s-1 id="RM000002S4X03NX_01_0058" proc-id="RM22W0E___00006YZ00000">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000028RU03WX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0200" proc-id="RM22W0E___00006ZA00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0161" proc-id="RM22W0E___00006Z200000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0198" proc-id="RM22W0E___000013G00000">
<ptxt>REMOVE V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Raise the front of the V-bank cover to detach the 3 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<figure>
<graphic graphicname="A274415E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0181" proc-id="RM22W0E___000015000000">
<ptxt>REMOVE AIR CLEANER AND HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 PCV hose and No. 1 air hose.</ptxt>
<figure>
<graphic graphicname="A271401" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the mass air flow meter connector and detach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and loosen the hose clamp, and then remove the air cleaner and hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0182" proc-id="RM22W0E___000016K00000">
<ptxt>REMOVE FRONT FENDER APRON TRIM PACKING A
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 clips and front fender apron trim packing A.</ptxt>
<figure>
<graphic graphicname="A177003" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0101" proc-id="RM22W0E___00004J500000">
<ptxt>DRAIN ENGINE OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the oil filler cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and No. 2 engine under cover seal.</ptxt>
<figure>
<graphic graphicname="A174194" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the oil pan drain plug and gasket, and drain the engine oil into a container.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket and the oil pan drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the No. 2 engine under cover seal with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0021" proc-id="RM22W0E___00006YX00000">
<ptxt>REMOVE RADIATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000003AB400EX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0137" proc-id="RM22W0E___00006Z000000">
<ptxt>REMOVE INTAKE MANIFOLD</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000031GQ01SX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0183" proc-id="RM22W0E___00004K300000">
<ptxt>DISCONNECT COOLER COMPRESSOR ASSEMBLY</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Remove the 3 bolts, nut and stud bolt, and then disconnect the cooler compressor.</ptxt>
<figure>
<graphic graphicname="A178491" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>It is not necessary to completely remove the compressor. With the hoses connected to the compressor, hang the compressor on the vehicle body with a rope.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0146" proc-id="RM22W0E___00006Z100000">
<ptxt>REMOVE AIR SWITCHING VALVE ASSEMBLY (w/ Secondary Air Injection System)</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>for Bank 1 (See page <xref label="Seep01" href="RM00000436500AX"/>)</ptxt>
</item>
<item>
<ptxt>for Bank 2 (See page <xref label="Seep02" href="RM00000436900AX"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0194" proc-id="RM22W0E___00006Z600000">
<ptxt>REMOVE AIR TUBE (w/ Secondary Air Injection System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="A272570" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the No. 3 air injection system hose.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and air tube.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0023" proc-id="RM22W0E___00006YY00000">
<ptxt>DISCONNECT ENGINE WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Engine Room LH Side:</ptxt>
<s3>
<ptxt>Disconnect the injector connector.</ptxt>
<figure>
<graphic graphicname="A272568" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the 2 camshaft timing oil control valve connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 4 ignition coil connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 2 VVT sensor connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the camshaft position sensor connector.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the engine coolant temperature sensor connector.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the noise filter connector.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 8 clamps.</ptxt>
</s3>
<s3>
<ptxt>Remove the bolt and ground wire.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for Engine Room RH Side:</ptxt>
<s3>
<ptxt>Disconnect the 2 camshaft timing oil control valve connectors.</ptxt>
<figure>
<graphic graphicname="A272569" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the 4 ignition coil connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 2 VVT sensor connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the injector connector.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the noise filter connector.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 6 clamps.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the clamp and engine wire.</ptxt>
<figure>
<graphic graphicname="A272571" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0104" proc-id="RM22W0E___00004N100000">
<ptxt>REMOVE OIL FILTER ELEMENT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect a hose with an inside diameter of 15 mm (0.591 in.) to the pipe.</ptxt>
<figure>
<graphic graphicname="A094910E10" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hose</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the oil filter drain plug.</ptxt>
<figure>
<graphic graphicname="A207196" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the pipe to the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A272715E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hose</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>If the O-ring is removed with the drain plug, install the O-ring together with the pipe.</ptxt>
</atten3>
<atten4>
<ptxt>Use a container to catch the draining oil.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Check that oil is drained from the oil filter. Then disconnect the pipe and remove the O-ring as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A207198" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, remove the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A207199E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09228-06501</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Filter Bracket Clip</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not remove the oil filter bracket clip.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the oil filter element and O-ring from the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A209947E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Filter Element</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be sure to remove the cap O-ring by hand, without using any tools, to prevent damage to the cap O-ring groove.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0130" proc-id="RM22W0E___00004NB00000">
<ptxt>REMOVE OIL PRESSURE SENDER GAUGE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the oil pressure sender gauge connector.</ptxt>
<figure>
<graphic graphicname="A163657" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the oil pressure sender gauge.</ptxt>
<figure>
<graphic graphicname="A162467" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0184" proc-id="RM22W0E___00006Z400000">
<ptxt>REMOVE NO. 1 OIL COOLER BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and bracket.</ptxt>
<figure>
<graphic graphicname="A272590" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the ground wire from the cylinder block.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0185" proc-id="RM22W0E___00004N200000">
<ptxt>REMOVE OIL FILTER BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts, 2 nuts and filter bracket.</ptxt>
<figure>
<graphic graphicname="A162469" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 O-rings.</ptxt>
<figure>
<graphic graphicname="A162470" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0126" proc-id="RM22W0E___00004JW00000">
<ptxt>REMOVE ENGINE OIL LEVEL DIPSTICK GUIDE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the engine oil level dipstick.</ptxt>
<figure>
<graphic graphicname="A218612" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the engine wire clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and engine oil level dipstick guide.</ptxt>
</s2>
<s2>
<ptxt>Remove the O-ring from the engine oil level dipstick guide.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0201">
<ptxt>DISCONNECT POWER STEERING OIL PRESSURE SWITCH CONNECTOR</ptxt>
</s-1>
<s-1 id="RM000002S4X03NX_01_0186" proc-id="RM22W0E___00006Z500000">
<ptxt>DISCONNECT VANE PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and disconnect the vane pump.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0199" proc-id="RM22W0E___00004KO00000">
<ptxt>DISCONNECT OIL COOLER TUBE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and disconnect the oil cooler tube.</ptxt>
<figure>
<graphic graphicname="A272652" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0187" proc-id="RM22W0E___00004JT00000">
<ptxt>REMOVE GENERATOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the generator connector.</ptxt>
<figure>
<graphic graphicname="A177892" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Open the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and disconnect the generator wire.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the wire harness bracket from the generator.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts, nut and generator.</ptxt>
<figure>
<graphic graphicname="A177893" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the stud bolt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0202" proc-id="RM22W0E___000015Q00000">
<ptxt>REMOVE NO. 4 ENGINE COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A214270" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0203" proc-id="RM22W0E___000015R00000">
<ptxt>REMOVE NO. 3 ENGINE COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A214271" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0167" proc-id="RM22W0E___00004MJ00000">
<ptxt>REMOVE FUEL TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and fuel tube (See page <xref label="Seep01" href="RM0000028RU03WX"/>).</ptxt>
<figure>
<graphic graphicname="A272572" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0168" proc-id="RM22W0E___00004MI00000">
<ptxt>REMOVE FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel pipe clamp.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 clamps and remove the fuel hose (See page <xref label="Seep01" href="RM0000028RU03WX"/>).</ptxt>
<figure>
<graphic graphicname="A272573" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0196" proc-id="RM22W0E___00006Z800000">
<ptxt>REMOVE NO. 4 AIR TUBE (w/ Secondary Air Injection System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and bracket.</ptxt>
<figure>
<graphic graphicname="A272588E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the bolt, 2 nuts, 2 stud bolts, No. 4 air tube and gasket.</ptxt>
<figure>
<graphic graphicname="A272653" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0197" proc-id="RM22W0E___00006Z900000">
<ptxt>REMOVE NO. 3 AIR TUBE (w/ Secondary Air Injection System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, 2 nuts, 2 stud bolts, No. 3 air tube and gasket.</ptxt>
<figure>
<graphic graphicname="A272654" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0166" proc-id="RM22W0E___00006Z300000">
<ptxt>REMOVE NO. 1 WATER BY-PASS PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the heater water hose.</ptxt>
<figure>
<graphic graphicname="A272576" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the No. 1 water by-pass pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0190" proc-id="RM22W0E___00006V100000">
<ptxt>DISCONNECT WATER BY-PASS HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A218799" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the water by-pass hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0157" proc-id="RM22W0E___00004M600000">
<ptxt>REMOVE NO. 1 WATER BY-PASS HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A215865" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the No. 1 water by-pass hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0191" proc-id="RM22W0E___00004MK00000">
<ptxt>REMOVE NO. 2 WATER BY-PASS PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A210013" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 4 hoses and remove the No. 2 water by-pass pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0159" proc-id="RM22W0E___00006V200000">
<ptxt>DISCONNECT NO. 5 WATER BY-PASS HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A215869" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the No. 5 water by-pass hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0195" proc-id="RM22W0E___00006Z700000">
<ptxt>REMOVE WATER BY-PASS PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 water by-pass hose from the front water by-pass joint.</ptxt>
<figure>
<graphic graphicname="A272575" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the heater hose.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and water by-pass pipe sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0188" proc-id="RM22W0E___00004NC00000">
<ptxt>REMOVE WATER INLET HOUSING
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A215871" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 bolts and water inlet housing.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the engine water pump.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0103" proc-id="RM22W0E___00004ND00000">
<ptxt>REMOVE FRONT WATER BY-PASS JOINT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 nuts, water by-pass joint and 2 gaskets.</ptxt>
<figure>
<graphic graphicname="A229715" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0153" proc-id="RM22W0E___00004M700000">
<ptxt>REMOVE WATER PUMP PULLEY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A230493E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using SST, hold the water pump pulley.</ptxt>
<sst>
<sstitem>
<s-number>09960-10010</s-number>
<s-subnumber>09962-01000</s-subnumber>
<s-subnumber>09963-01000</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Remove the 4 bolts and water pump pulley.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0088" proc-id="RM22W0E___00004M800000">
<ptxt>REMOVE NO. 1 IDLER PULLEY SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and No. 1 idler pulley.</ptxt>
<figure>
<graphic graphicname="A210019" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0124" proc-id="RM22W0E___00004M900000">
<ptxt>REMOVE FAN BRACKET ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and fan bracket.</ptxt>
<figure>
<graphic graphicname="A210020" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0089" proc-id="RM22W0E___00004NE00000">
<ptxt>REMOVE V-RIBBED BELT TENSIONER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, 6 mm hexagon bolt and belt tensioner.</ptxt>
<figure>
<graphic graphicname="A162527" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0172" proc-id="RM22W0E___000014Y00000">
<ptxt>REMOVE IGNITION COIL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Bank 1:</ptxt>
<s3>
<ptxt>Detach the 4 clamps and disconnect the engine wire.</ptxt>
<figure>
<graphic graphicname="A271367" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the 4 ignition coil connectors.</ptxt>
</s3>
<s3>
<ptxt>Remove the 4 bolts and 4 ignition coils.</ptxt>
<figure>
<graphic graphicname="A271372" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>for Bank 2:</ptxt>
<s3>
<ptxt>Remove the 2 bolts and disconnect the water by-pass pipe.</ptxt>
<figure>
<graphic graphicname="A271376" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Detach the 3 clamps and disconnect the engine wire.</ptxt>
<figure>
<graphic graphicname="A271366" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the 4 ignition coil connectors.</ptxt>
</s3>
<s3>
<ptxt>Remove the 4 bolts and 4 ignition coils.</ptxt>
<figure>
<graphic graphicname="A271373" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03NX_01_0055" proc-id="RM22W0E___00004NF00000">
<ptxt>REMOVE CYLINDER HEAD COVER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 14 bolts, seal washer, cylinder head cover sub-assembly LH and cylinder head cover gasket LH.</ptxt>
<figure>
<graphic graphicname="A210022" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure the removed parts are returned to the same places they were removed from.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the 5 gaskets from the camshaft bearing caps (No. 2, No. 3).</ptxt>
<figure>
<graphic graphicname="A229745" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100143" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Gasket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0056" proc-id="RM22W0E___00004NG00000">
<ptxt>REMOVE CYLINDER HEAD COVER SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 14 bolts, seal washer, cylinder head cover sub-assembly RH and cylinder head cover gasket RH.</ptxt>
<figure>
<graphic graphicname="A210024" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure the removed parts are returned to the same places they were removed from.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the 5 gaskets from the camshaft bearing caps (No. 1, No. 3).</ptxt>
<figure>
<graphic graphicname="A229746" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100143" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Gasket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0189" proc-id="RM22W0E___00004GM00000">
<ptxt>REMOVE CRANKSHAFT PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the crankshaft pulley set bolt.</ptxt>
<sst>
<sstitem>
<s-number>09213-70011</s-number>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A164228E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Temporarily install the pulley set bolt to the crankshaft until 2 or 3 threads are engaged.</ptxt>
</s2>
<s2>
<ptxt>Using the pulley set bolt and SST, remove the crankshaft pulley.</ptxt>
<sst>
<sstitem>
<s-number>09950-50013</s-number>
<s-subnumber>09951-05010</s-subnumber>
<s-subnumber>09952-05010</s-subnumber>
<s-subnumber>09953-05010</s-subnumber>
<s-subnumber>09954-05011</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="A164229E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0057" proc-id="RM22W0E___00004N000000">
<ptxt>REMOVE TIMING CHAIN COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 26 bolts and nut shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A210026" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the timing chain cover by prying between it and the cylinder head and cylinder block with a screwdriver as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A229747" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100144" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to damage the cylinder head, camshaft housing and cylinder block contact surfaces of the chain cover.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the gasket from the cylinder block.</ptxt>
<figure>
<graphic graphicname="A210028" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the oil pan.</ptxt>
<figure>
<graphic graphicname="A164415" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0093" proc-id="RM22W0E___00004N300000">
<ptxt>REMOVE WATER INLET PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the water inlet pipe.</ptxt>
<figure>
<graphic graphicname="A210029" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 O-rings from the water inlet pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03NX_01_0162" proc-id="RM22W0E___00004GL00000">
<ptxt>REMOVE FRONT CRANKSHAFT OIL SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, pry out the front crankshaft oil seal.</ptxt>
<figure>
<graphic graphicname="A218840E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not damage the surface of the front crankshaft oil seal press fit hole or crankshaft.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>