<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002K" variety="S002K">
<name>WIPER / WASHER</name>
<ttl id="12068_S002K_7C3YC_T00RF" variety="T00RF">
<name>WIPER SWITCH</name>
<para id="RM000001Y2V01BX" category="G" type-id="3001K" name-id="WW4QR-01" from="201301">
<name>INSPECTION</name>
<subpara id="RM000001Y2V01BX_01" type-id="01" category="01">
<s-1 id="RM000001Y2V01BX_01_0001" proc-id="RM22W0E___0000IT000000">
<ptxt>INSPECT WINDSHIELD WIPER SWITCH ASSEMBLY (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the windshield wiper switch.</ptxt>
<figure>
<graphic graphicname="B184771E06" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>Front Wiper Switch (w/o Rain Sensor)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E16-1 (+S) - E16-3 (+1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>INT</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>E16-2 (+B) - E16-3 (+1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>MIST</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>LO</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E16-2 (+B) - E16-4 (+2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>HI</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>Front Wiper Switch (w/ Rain Sensor)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E16-1 (+S) - E16-3 (+1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>AUTO</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>E16-2 (+B) - E16-3 (+1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>MIST</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>LO</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E16-2 (+B) - E16-4 (+2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>HI</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the front washer switch.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E15-2 (EW) - E15-3 (WF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>On</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the intermittent operation.</ptxt>
<s3>
<ptxt>Connect the voltmeter positive (+) lead to terminal E16-3 (+1) and the negative (-) lead to terminal E15-2 (EW).</ptxt>
</s3>
<s3>
<ptxt>Connect the battery positive (+) lead to terminal E16-2 (+B) and the negative (-) lead to terminal E15-2 (EW) and E16-1 (+S).</ptxt>
</s3>
<s3>
<ptxt>Turn the wiper switch to the INT position.</ptxt>
</s3>
<s3>
<ptxt>Connect the battery positive (+) lead to terminal E16-1 (+S) for 5 seconds.</ptxt>
</s3>
<s3>
<ptxt>Connect the battery negative (-) lead to terminal E16-1 (+S). Operate the intermittent wiper relay and check the voltage between terminals E16-3 (+1) and E15-2 (EW).</ptxt>
<spec>
<title>Standard Voltage</title>
<specitem>
<ptxt>Refer to the illustration below.</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="E157470E06" width="7.106578999in" height="2.775699831in"/>
</figure>
<figure>
<graphic graphicname="B278090E02" width="7.106578999in" height="1.771723296in"/>
</figure>
<ptxt>If the result is not as specified, replace the switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the front washer operation.</ptxt>
<s3>
<ptxt>Turn the wiper switch OFF.</ptxt>
</s3>
<s3>
<ptxt>Connect the battery positive (+) lead to terminal E16-2 (+B) and the negative (-) lead to terminals E16-1 (+S) and E15-2 (EW).</ptxt>
</s3>
<s3>
<ptxt>Connect the voltmeter positive (+) lead to E16-3 (+1) and the negative (-) lead to terminal E15-2 (EW). </ptxt>
</s3>
<s3>
<ptxt>Turn the washer switch ON and OFF, and check the voltage between terminals E16-3 (+1) and E15-2 (EW).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Refer to the illustration below.</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="E157493E10" width="7.106578999in" height="1.771723296in"/>
</figure>
<figure>
<graphic graphicname="E157493E08" width="7.106578999in" height="1.771723296in"/>
</figure>
<ptxt>If the result is not as specified, replace the switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the rear wiper switch.</ptxt>
<figure>
<graphic graphicname="B184771E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>Rear Wiper Switch (w/ Rear Wiper Intermittent Function)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-6 (C1R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>LO</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-7 (+1R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>HI</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-6 (C1R)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-7 (+1R)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>Rear Wiper Switch (w/o Rear Wiper Intermittent Function)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-7 (+1R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-7 (+1R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the rear washer switch.</ptxt>
<s3>
<ptxt>Measure the resistance of the switch.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E15-2 (EW) - E16-5 (WR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>On</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Y2V01BX_01_0003" proc-id="RM22W0E___0000IT100000">
<ptxt>INSPECT WINDSHIELD WIPER SWITCH ASSEMBLY (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the windshield wiper switch.</ptxt>
<figure>
<graphic graphicname="B189269E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>Front Wiper Switch (w/o Rain Sensor)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E16-4 (+S) - E16-2 (+1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>INT</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>E16-3 (+B) - E16-2 (+1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>MIST</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>LO</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E16-3 (+B) - E16-1 (+2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>HI</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>Front Wiper Switch (w/ Rain Sensor)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E16-4 (+S) - E16-2 (+1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>AUTO</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>E16-3 (+B) - E16-2 (+1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>MIST</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>LO</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E16-3 (+B) - E16-1 (+2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>HI</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the front washer switch.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E15-2 (EW) - E15-1 (WF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>On</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the intermittent operation.</ptxt>
<s3>
<ptxt>Connect the voltmeter positive (+) lead to terminal E16-2 (+1) and the negative (-) lead to terminal E15-2 (EW).</ptxt>
</s3>
<s3>
<ptxt>Connect the battery positive (+) lead to terminal E16-3 (+B) and the negative (-) lead to terminal E15-2 (EW) and E16-4 (+S).</ptxt>
</s3>
<s3>
<ptxt>Turn the wiper switch to the INT position.</ptxt>
</s3>
<s3>
<ptxt>Connect the battery positive (+) lead to terminal E16-4 (+S) for 5 seconds.</ptxt>
</s3>
<s3>
<ptxt>Connect the battery negative (-) lead to terminal E16-4 (+S). Operate the intermittent wiper relay and check the voltage between terminals E16-2 (+1) and E15-2 (EW).</ptxt>
<spec>
<title>Standard Voltage</title>
<specitem>
<ptxt>Refer to the illustration below.</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="E157470E07" width="7.106578999in" height="2.775699831in"/>
</figure>
<figure>
<graphic graphicname="B278090E04" width="7.106578999in" height="1.771723296in"/>
</figure>
<ptxt>If the result is not as specified, replace the switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the front washer operation.</ptxt>
<s3>
<ptxt>Turn the wiper switch OFF.</ptxt>
</s3>
<s3>
<ptxt>Connect the battery positive (+) lead to terminal E16-3 (+B) and the negative (-) lead to terminals E16-4 (+S) and E15-2 (EW).</ptxt>
</s3>
<s3>
<ptxt>Connect the voltmeter positive (+) lead to E16-2 (+1) and the negative (-) lead to terminal E15-2 (EW). </ptxt>
</s3>
<s3>
<ptxt>Turn the washer switch ON and OFF, and check the voltage between terminals E16-2 (+1) and E15-2 (EW).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Refer to the illustration below.</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="E157493E09" width="7.106578999in" height="1.771723296in"/>
</figure>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the rear wiper switch.</ptxt>
<figure>
<graphic graphicname="B189269E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>Rear Wiper Switch (w/ Rear Wiper Intermittent Function)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-5 (C1R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>LO</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-4 (+1R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>HI</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-5 (C1R)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-4 (+1R)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>Rear Wiper Switch (w/o Rear Wiper Intermittent Function)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-4 (+1R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E15-2 (EW) - E15-4 (+1R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the rear washer switch.</ptxt>
<s3>
<ptxt>Measure the resistance of the switch.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>E15-2 (EW) - E16-10 (WR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>On</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the windshield wiper switch assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>