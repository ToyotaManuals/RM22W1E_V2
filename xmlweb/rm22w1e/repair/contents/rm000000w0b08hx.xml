<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000000W0B08HX" category="J" type-id="300LQ" name-id="ES169H-001" from="201301" to="201308">
<dtccode/>
<dtcname>Pre-heating Control Circuit</dtcname>
<subpara id="RM000000W0B08HX_01" type-id="60" category="03" proc-id="RM22W0E___000037C00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The glow plug is mounted inside the engine combustion chamber. To ensure efficient engine starting with a cold engine, the ECM calculates a time interval of the current that needs to flow through the glow plug depending on the starting engine coolant temperature when the engine switch is turned on (IG). The ECM then turns on the glow relay and permits current to flow through the glow plug based on the ECM calculated time. The glow plug is then heated, and enhances fuel combustion with a cold engine.</ptxt>
<ptxt>A DTC will be stored if the glow plug or the circuit is open.</ptxt>
<figure>
<graphic graphicname="A154593E07" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>These troubleshooting procedures are for: 1) difficult engine starting in cold weather, and 2) difficulty driving/vehicle malfunctions in cold weather immediately after engine is started.</ptxt>
</item>
<item>
<ptxt>After the engine is started, the ECM performs "after-glow" for a certain period of time. In proportion to the actual engine coolant temperature, the time period varies. The after-glow reduces diesel engine knocking, white smoke emissions and engine noises when the engine is cold.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="G038988E16" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W0B08HX_02" type-id="32" category="03" proc-id="RM22W0E___000037D00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A182513E04" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W0B08HX_03" type-id="51" category="05" proc-id="RM22W0E___000037E00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000W0B08HX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W0B08HX_04_0007" proc-id="RM22W0E___000037L00000">
<testtitle>CHECK ECM TERMINAL VOLTAGE (GREL TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the GLOW 1 relay from the engine room relay block.</ptxt>
<figure>
<graphic graphicname="A182516E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Remove the GLOW 2 relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.34in"/>
<colspec colname="COLSPEC0" colwidth="1.34in"/>
<colspec colname="COL2" colwidth="1.45in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>GLOW 1 relay (2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>GLOW 2 relay (2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W0B08HX_04_0001" fin="false">OK</down>
<right ref="RM000000W0B08HX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0001" proc-id="RM22W0E___000037F00000">
<testtitle>INSPECT GLOW RELAY (GLOW 1 or 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the GLOW 1 relay (See page <xref label="Seep01" href="RM000003BIA02HX_01_0004"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the GLOW 2 relay (See page <xref label="Seep02" href="RM000003BIA02HX_01_0005"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0B08HX_04_0002" fin="false">OK</down>
<right ref="RM000000W0B08HX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0002" proc-id="RM22W0E___000037G00000">
<testtitle>INSPECT FUSE (GLOW 1 and GLOW 2)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A182514E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Remove the GLOW 1 and GLOW 2 fuses from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>GLOW 1 H-fuse</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>GLOW 2 H-fuse</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W0B08HX_04_0003" fin="false">OK</down>
<right ref="RM000000W0B08HX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0003" proc-id="RM22W0E___000037H00000">
<testtitle>INSPECT GLOW PLUG ASSEMBLY (RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the glow plug assembly (See page <xref label="Seep01" href="RM0000013XN01OX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0B08HX_04_0004" fin="false">OK</down>
<right ref="RM000000W0B08HX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0004" proc-id="RM22W0E___000037I00000">
<testtitle>CHECK GLOW PLUG ASSEMBLY (INSTALLATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the glow plug installation (See page <xref label="Seep01" href="RM0000013ZT023X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Glow plugs are installed securely.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W0B08HX_04_0006" fin="false">OK</down>
<right ref="RM000000W0B08HX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0006" proc-id="RM22W0E___000037K00000">
<testtitle>CHECK HARNESS AND CONNECTOR (GLOW PLUG RELAY - GLOW PLUG - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the GLOW 1 relay from the engine room relay block.</ptxt>
<figure>
<graphic graphicname="A182515E01" width="2.775699831in" height="5.787629434in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the C77 glow plug connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>No. 1</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 1 relay (3) - C77-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 1 relay (5) - Positive (+) battery terminal cable</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 1 relay (1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Remove the GLOW 2 relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C78 glow plug connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>No. 2</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 2 relay (3) - C78-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 2 relay (5) - Positive (+) battery terminal cable</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 2 relay (1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W0B08HX_04_0009" fin="true">OK</down>
<right ref="RM000000W0B08HX_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0005" proc-id="RM22W0E___000037J00000">
<testtitle>CHECK HARNESS AND CONNECTOR (GLOW PLUG RELAY - ECM AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the GLOW 1 relay from the engine room relay block.</ptxt>
<figure>
<graphic graphicname="A275259E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Remove the GLOW 2 relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 1 relay (2) - A38-12 (GREL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 2 relay (2) - A38-23 (GRL2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 1 relay (2) or A38-12 (GREL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 2 relay (2) or A38-23 (GRL2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 1 relay (2) - A52-12 (GREL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 2 relay (2) - A52-23 (GRL2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 1 relay (2) or A52-12 (GREL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>GLOW 2 relay (2) or A52-23 (GRL2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W0B08HX_04_0015" fin="true">OK</down>
<right ref="RM000000W0B08HX_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0010">
<testtitle>REPLACE GLOW RELAY (GLOW 1 or 2)</testtitle>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0011">
<testtitle>CHECK FOR SHORT IN ALL HARNESSES AND COMPONENTS CONNECTED TO FUSE, AND REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0012">
<testtitle>REPLACE GLOW PLUG ASSEMBLY<xref label="Seep01" href="RM0000013XP01YX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0013">
<testtitle>TIGHTEN GLOW PLUG<xref label="Seep01" href="RM0000013ZT023X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0009">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ10BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W0B08HX_04_0015">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202SX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>