<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001F" variety="S001F">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001F_7C3O8_T00HB" variety="T00HB">
<name>REAR AXLE HUB BOLT</name>
<para id="RM00000306N00VX" category="A" type-id="30019" name-id="AD03I-03" from="201301">
<name>REPLACEMENT</name>
<subpara id="RM00000306N00VX_01" type-id="11" category="10" proc-id="RM22W0E___00009L300000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the LH side and RH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000306N00VX_02" type-id="01" category="01">
<s-1 id="RM00000306N00VX_02_0025" proc-id="RM22W0E___00009WH00000">
<ptxt>REMOVE STABILIZER CONTROL VALVE PROTECTOR (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp, and disconnect the connector from the protector.</ptxt>
<figure>
<graphic graphicname="C172095" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and protector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0022" proc-id="RM22W0E___00009WN00000">
<ptxt>OPEN STABILIZER CONTROL WITH ACCUMULATOR HOUSING SHUTTER VALVE (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, loosen the lower and upper chamber shutter valves of the stabilizer control with accumulator housing 2.0 to 3.5 turns.</ptxt>
<figure>
<graphic graphicname="C175131E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When loosening a shutter valve, make sure that the end protrudes 2 to 3.5 mm (0.0787 to 0.137 in.) from the surface of the block, and do not turn the shutter valve any further.</ptxt>
</item>
<item>
<ptxt>Do not remove the shutter valves.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0001">
<ptxt>REMOVE REAR WHEEL LH</ptxt>
</s-1>
<s-1 id="RM00000306N00VX_02_0002" proc-id="RM22W0E___00009L400000">
<ptxt>DISCONNECT REAR DISC BRAKE CYLINDER ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and disconnect the rear disc brake cylinder.</ptxt>
<figure>
<graphic graphicname="C158344" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not twist or bend the flexible hose.</ptxt>
</item>
<item>
<ptxt>Do not disconnect the flexible hose from the disc brake cylinder.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0003" proc-id="RM22W0E___00009L500000">
<ptxt>REMOVE REAR DISC LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put matchmarks on the rear disc and axle hub if planning to reuse the disc.</ptxt>
<figure>
<graphic graphicname="C172168E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the shoe adjuster as shown in the illustration until the disc turns freely, and then remove the disc.</ptxt>
<figure>
<graphic graphicname="C155453" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0004" proc-id="RM22W0E___00009L600000">
<ptxt>REMOVE PARKING BRAKE SHOE RETURN TENSION SPRING LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the return spring.</ptxt>
<sst>
<sstitem>
<s-number>09703-30011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172171E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0005" proc-id="RM22W0E___00009L700000">
<ptxt>REMOVE NO. 1 PARKING BRAKE SHOE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the shoe hold down spring cup, compression spring and shoe hold down spring pin.</ptxt>
<sst>
<sstitem>
<s-number>09718-00011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172174E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the tension spring from the No. 1 parking brake shoe.</ptxt>
<figure>
<graphic graphicname="C172180" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 1 parking brake shoe and shoe adjuster screw set.</ptxt>
<figure>
<graphic graphicname="C172179" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0006" proc-id="RM22W0E___00009L800000">
<ptxt>REMOVE NO. 2 PARKING BRAKE SHOE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the shoe hold down spring cup, compression spring and shoe hold down spring pin.</ptxt>
<sst>
<sstitem>
<s-number>09718-00011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172175E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 2 parking brake shoe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0007" proc-id="RM22W0E___00009L900000">
<ptxt>REMOVE PARKING BRAKE SHOE LEVER SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 3 parking brake cable from the parking brake shoe lever.</ptxt>
</s2>
<s2>
<ptxt>Remove the parking brake shoe lever.</ptxt>
<figure>
<graphic graphicname="C172177" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0008" proc-id="RM22W0E___00009LA00000">
<ptxt>REMOVE REAR AXLE HUB BOLT LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a screwdriver or equivalent to hold the axle hub, remove the hub bolt.</ptxt>
<figure>
<graphic graphicname="C172330E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09650-17011</s-number>
</sstitem>
</sst>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not deform the oil deflector.</ptxt>
</item>
<item>
<ptxt>Make sure to align the notch of SST with the flange of the oil deflector.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000306N00VX_02_0009" proc-id="RM22W0E___00009LB00000">
<ptxt>INSTALL REAR AXLE HUB BOLT LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install a washer and hub nut to a new hub bolt as shown in the illustration.</ptxt>
<atten3>
<ptxt>Install a hub nut to prevent damage to the hub bolt.</ptxt>
</atten3>
<figure>
<graphic graphicname="C172331E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a screwdriver or equivalent to hold the hub, turn the hub nut until the bottom surface of the hub bolt head touches the axle hub.</ptxt>
</s2>
<s2>
<ptxt>Remove the washer and hub nut.</ptxt>
<atten3>
<ptxt>Do not damage the threads of the hub bolt.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000306N00VX_02_0010" proc-id="RM22W0E___00009LC00000">
<ptxt>INSTALL PARKING BRAKE SHOE LEVER SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply high temperature grease to the parking brake anchor block.</ptxt>
<figure>
<graphic graphicname="C177128E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Install the parking brake shoe lever to the No. 3 parking brake cable.</ptxt>
<atten3>
<ptxt>Be carefully to distinguish between the parking brake shoe lever RH and LH.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0011" proc-id="RM22W0E___00009LD00000">
<ptxt>INSTALL NO. 2 PARKING BRAKE SHOE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply high temperature grease to the areas of the backing plate that contact the shoe.</ptxt>
</s2>
<s2>
<ptxt>Using SST, install the No. 2 parking brake shoe with the shoe hold down spring pin, compression spring and shoe hold down spring cup.</ptxt>
<sst>
<sstitem>
<s-number>09718-00011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172175E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0012" proc-id="RM22W0E___00009LE00000">
<ptxt>INSTALL NO. 1 PARKING BRAKE SHOE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply high temperature grease to the areas of the backing plate that contact the shoe.</ptxt>
</s2>
<s2>
<ptxt>Apply high temperature grease to the thread and all joining areas of the parking brake shoe adjuster screw set.</ptxt>
<figure>
<graphic graphicname="C172178E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Set the No. 1 parking brake shoe and shoe adjuster screw set in place.</ptxt>
<figure>
<graphic graphicname="C172179" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the tension spring.</ptxt>
<figure>
<graphic graphicname="C172180" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, install the shoe hold down spring pin, compression spring and shoe hold down spring cup.</ptxt>
<sst>
<sstitem>
<s-number>09718-00011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172174E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0013" proc-id="RM22W0E___00009LF00000">
<ptxt>INSTALL PARKING BRAKE SHOE RETURN TENSION SPRING LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, install the shoe return spring.</ptxt>
<sst>
<sstitem>
<s-number>09703-30011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172171E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0014" proc-id="RM22W0E___00009LG00000">
<ptxt>CHECK PARKING BRAKE INSTALLATION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that each part is installed properly.</ptxt>
<figure>
<graphic graphicname="C172182E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0015" proc-id="RM22W0E___00009LH00000">
<ptxt>INSTALL REAR DISC LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the matchmarks, and then install the rear disc.</ptxt>
<figure>
<graphic graphicname="C172168E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When replacing the rear disc with a new one, select the installation position where the rear disc has the minimum runout.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0016" proc-id="RM22W0E___00009LI00000">
<ptxt>ADJUST PARKING BRAKE SHOE CLEARANCE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Completely release the parking brake lever.</ptxt>
</s2>
<s2>
<ptxt>Loosen the adjusting nut to completely release the parking brake cable.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install the hub nuts.</ptxt>
<figure>
<graphic graphicname="C170906" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the hole plug.</ptxt>
</s2>
<s2>
<ptxt>Insert an adjustment tool into the adjustment hole of the disc. Rotate the adjustment wheel in the "X" direction until the shoes are locked. Then rotate the adjustment wheel in the "Y" direction 8 notches.</ptxt>
<figure>
<graphic graphicname="C158874E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Check that the disc can be rotated lightly. If not, rotate the adjustment wheel in the "Y" direction and check again.</ptxt>
</s2>
<s2>
<ptxt>Install the hole plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the hub nuts.</ptxt>
</s2>
<s2>
<ptxt>Turn the adjusting nut until the parking brake lever travel becomes correct.</ptxt>
<figure>
<graphic graphicname="C170906" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Parking Brake Lever Travel when Pulled with a Force of 200 N (20 kgf, 45 lbf)</title>
<specitem>
<ptxt>5 to 7 clicks</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Operate the parking brake lever 3 to 4 times, and check the parking brake lever travel.</ptxt>
<spec>
<title>Standard Parking Brake Lever Travel when Pulled with a Force of 200 N (20 kgf, 45 lbf)</title>
<specitem>
<ptxt>5 to 7 clicks</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Check whether the parking brake drags or not.</ptxt>
</s2>
<s2>
<ptxt>When operating the parking brake lever, check that the brake warning light comes on.</ptxt>
<spec>
<title>Standard Condition</title>
<specitem>
<ptxt>The brake warning light always illuminates at the first click.</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0017" proc-id="RM22W0E___00009LJ00000">
<ptxt>CONNECT REAR DISC BRAKE CYLINDER ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the rear disc brake cylinder and install 2 new bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>95</t-value1>
<t-value2>969</t-value2>
<t-value4>70</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="C158344" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not twist the flexible hose.</ptxt>
</item>
<item>
<ptxt>Make sure that the bolts are free from damage and foreign matter.</ptxt>
</item>
<item>
<ptxt>Do not overtighten the bolts.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0018" proc-id="RM22W0E___00009LK00000">
<ptxt>INSTALL REAR WHEEL LH</ptxt>
<content1 releasenbr="1">
<torque>
<subtitle>for Aluminum Wheel</subtitle>
<torqueitem>
<t-value1>131</t-value1>
<t-value2>1336</t-value2>
<t-value4>97</t-value4>
</torqueitem>
<subtitle>for Steel Wheel</subtitle>
<torqueitem>
<t-value1>209</t-value1>
<t-value2>2131</t-value2>
<t-value4>154</t-value4>
</torqueitem>
</torque>
</content1>
</s-1>
<s-1 id="RM00000306N00VX_02_0020" proc-id="RM22W0E___00009LL00000">
<ptxt>CHECK PARKING BRAKE LEVER TRAVEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fully pull the parking brake lever to engage the parking brake.</ptxt>
</s2>
<s2>
<ptxt>Release the lever to disengage the parking brake.</ptxt>
</s2>
<s2>
<ptxt>Slowly pull the parking brake lever all the way, and count the number of clicks.</ptxt>
<spec>
<title>Standard Parking Brake Lever Travel when Pulled with a Force of 200 N (20 kgf, 45 lbf)</title>
<specitem>
<ptxt>5 to 7 clicks</ptxt>
</specitem>
</spec>
<ptxt>If the parking brake lever travel is not as specified, adjust the parking brake shoe clearance and parking brake lever travel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0021" proc-id="RM22W0E___00009LI00000">
<ptxt>ADJUST PARKING BRAKE LEVER TRAVEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Completely release the parking brake lever.</ptxt>
</s2>
<s2>
<ptxt>Loosen the adjusting nut to completely release the parking brake cable.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install the hub nuts.</ptxt>
<figure>
<graphic graphicname="C170906" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the hole plug.</ptxt>
</s2>
<s2>
<ptxt>Insert an adjustment tool into the adjustment hole of the disc. Rotate the adjustment wheel in the "X" direction until the shoes are locked. Then rotate the adjustment wheel in the "Y" direction 8 notches.</ptxt>
<figure>
<graphic graphicname="C158874E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Check that the disc can be rotated lightly. If not, rotate the adjustment wheel in the "Y" direction and check again.</ptxt>
</s2>
<s2>
<ptxt>Install the hole plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the hub nuts.</ptxt>
</s2>
<s2>
<ptxt>Turn the adjusting nut until the parking brake lever travel becomes correct.</ptxt>
<figure>
<graphic graphicname="C170906" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Parking Brake Lever Travel when Pulled with a Force of 200 N (20 kgf, 45 lbf)</title>
<specitem>
<ptxt>5 to 7 clicks</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Operate the parking brake lever 3 to 4 times, and check the parking brake lever travel.</ptxt>
<spec>
<title>Standard Parking Brake Lever Travel when Pulled with a Force of 200 N (20 kgf, 45 lbf)</title>
<specitem>
<ptxt>5 to 7 clicks</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Check whether the parking brake drags or not.</ptxt>
</s2>
<s2>
<ptxt>When operating the parking brake lever, check that the brake warning light comes on.</ptxt>
<spec>
<title>Standard Condition</title>
<specitem>
<ptxt>The brake warning light always illuminates at the first click.</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0023" proc-id="RM22W0E___00009JQ00000">
<ptxt>MEASURE VEHICLE HEIGHT (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the inspection on a level surface.</ptxt>
</item>
<item>
<ptxt>Ensure that the wheels are on the ground and facing straight ahead.</ptxt>
</item>
<item>
<ptxt>Perform the inspection with the vehicle load completely on the suspension.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Perform this step with the fuel tank full.</ptxt>
</item>
<item>
<ptxt>If there are any parts installed to the vehicle which place any unbalanced load on the left or right side of the vehicle, remove them.</ptxt>
</item>
</list1>
</atten4>
<s2>
<ptxt>Set the tire pressure to the specified value(s) (See page <xref label="Seep01" href="RM000003DGS00LX"/>).</ptxt>
</s2>
<s2>
<ptxt>Bounce the vehicle to stabilize the suspension.</ptxt>
</s2>
<s2>
<ptxt>Measure the distance from the ground to the top of the bumper and calculate the difference in the vehicle height between left and right. Perform this procedure for both the front and rear wheels.</ptxt>
<figure>
<graphic graphicname="C171398" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Height difference of left and right sides</title>
<specitem>
<ptxt>15 mm (0.591 in.) or less</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>If not as specified, perform the vehicle tilt calibration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0024" proc-id="RM22W0E___00009W800000">
<ptxt>CLOSE STABILIZER CONTROL WITH ACCUMULATOR HOUSING SHUTTER VALVE (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the inspection on a level surface.</ptxt>
</item>
<item>
<ptxt>Ensure that the wheels are on the ground and facing straight ahead.</ptxt>
</item>
<item>
<ptxt>Perform the inspection with the vehicle load completely on the suspension.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Perform this step with the fuel tank full.</ptxt>
</item>
<item>
<ptxt>If there are any parts installed to the vehicle which place any unbalanced load on the left or right side of the vehicle, remove them.</ptxt>
</item>
</list1>
</atten4>
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, tighten the lower and upper chamber shutter valves of the stabilizer control with accumulator housing.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000306N00VX_02_0026" proc-id="RM22W0E___00009VZ00000">
<ptxt>INSTALL STABILIZER CONTROL VALVE PROTECTOR (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172095" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the valve protector with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the clamp, and connect the connector to the valve protector.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>