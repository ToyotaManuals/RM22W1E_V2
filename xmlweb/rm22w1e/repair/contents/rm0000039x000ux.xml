<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000I" variety="S000I">
<name>1VD-FTV FUEL</name>
<ttl id="12008_S000I_7C3H6_T00A9" variety="T00A9">
<name>FUEL SUB TANK</name>
<para id="RM0000039X000UX" category="A" type-id="80001" name-id="FU93H-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000039X000UX_01" type-id="01" category="01">
<s-1 id="RM0000039X000UX_01_0023" proc-id="RM22W0E___000066O00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="2">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0002" proc-id="RM22W0E___000066600001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
<s2>
<ptxt>Disconnect the cables from the negative (-) main battery and sub-battery terminals.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0019">
<ptxt>REMOVE SPARE TIRE</ptxt>
</s-1>
<s-1 id="RM0000039X000UX_01_0020" proc-id="RM22W0E___000066L00000">
<ptxt>REMOVE TAILPIPE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, clamp and gasket.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the tailpipe from the 2 exhaust pipe supports.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039X000UX_01_0021" proc-id="RM22W0E___000066M00001">
<ptxt>REMOVE SPARE WHEEL CARRIER CROSSMEMBER AND SPARE WHEEL CARRIER BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt labeled A, and then loosen the 4 bolts labeled B.</ptxt>
<figure>
<graphic graphicname="A176384E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Slightly raise the spare wheel carrier crossmember, and then remove the 6 bolts labeled C, spare wheel carrier crossmember and 2 brackets.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0022" proc-id="RM22W0E___000066N00001">
<ptxt>REMOVE NO. 1 SPARE WHEEL STOPPER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and spare wheel stopper.</ptxt>
<figure>
<graphic graphicname="A176385" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0018">
<ptxt>REMOVE FUEL TANK CAP ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM0000039X000UX_01_0003" proc-id="RM22W0E___000066700001">
<ptxt>DISCONNECT FUEL TANK EVAPORATION VENT TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 fuel tank evaporation vent tubes.</ptxt>
<figure>
<graphic graphicname="A175074" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0004" proc-id="RM22W0E___000066800001">
<ptxt>DISCONNECT FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 3 fuel hoses.</ptxt>
<figure>
<graphic graphicname="A175075" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0005" proc-id="RM22W0E___000066900001">
<ptxt>DISCONNECT FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the breather hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A175076" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0006" proc-id="RM22W0E___000066A00001">
<ptxt>DISCONNECT NO. 3 FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A175077" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0007" proc-id="RM22W0E___000066B00001">
<ptxt>DISCONNECT FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A175078" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0009" proc-id="RM22W0E___000066C00001">
<ptxt>REMOVE FUEL SUB TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the floor No. 3 wire connector.</ptxt>
</s2>
<s2>
<ptxt>Using a clip remover, detach the 3 wire harness clamps.</ptxt>
</s2>
<s2>
<ptxt>Place a transmission jack under the fuel sub tank.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the 2 fuel tank bands.</ptxt>
<figure>
<graphic graphicname="A175079" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Lower the transmission jack to remove the fuel sub tank from the vehicle.</ptxt>
<atten3>
<ptxt>Do not allow the fuel sub tank to contact the vehicle, especially the differential.</ptxt>
</atten3>
<atten4>
<ptxt>Lower the transmission jack slowly.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0010" proc-id="RM22W0E___000066D00001">
<ptxt>REMOVE FLOOR NO. 3 WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the sender gauge connector.</ptxt>
<figure>
<graphic graphicname="A175080" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a clip remover, detach the wire harness clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0011" proc-id="RM22W0E___000066E00001">
<ptxt>REMOVE FUEL SENDER GAUGE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 screws and sender gauge.</ptxt>
<figure>
<graphic graphicname="A175087" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the gasket from the sender gauge.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0016" proc-id="RM22W0E___000066J00001">
<ptxt>REMOVE FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 fuel hoses.</ptxt>
<figure>
<graphic graphicname="A175081" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0017" proc-id="RM22W0E___000066K00001">
<ptxt>REMOVE FUEL TANK EVAPORATION VENT TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 fuel tank evaporation vent tubes.</ptxt>
<figure>
<graphic graphicname="A175082" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0012" proc-id="RM22W0E___000066F00001">
<ptxt>REMOVE FUEL AND EVAPORATION VENT TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 6 bolts and fuel and evaporation vent tube.</ptxt>
<figure>
<graphic graphicname="A175086" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the gasket from the fuel and evaporation vent tube.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0013" proc-id="RM22W0E___000066G00001">
<ptxt>REMOVE FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the breather hose from the fuel sub tank.</ptxt>
<figure>
<graphic graphicname="A175083" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0014" proc-id="RM22W0E___000066H00001">
<ptxt>REMOVE NO. 3 FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel hose from the fuel sub tank.</ptxt>
<figure>
<graphic graphicname="A175084" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039X000UX_01_0015" proc-id="RM22W0E___000066I00001">
<ptxt>REMOVE FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the hose from the fuel sub tank.</ptxt>
<figure>
<graphic graphicname="A175085" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>