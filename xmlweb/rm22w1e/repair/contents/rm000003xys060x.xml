<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R4_T00K7" variety="T00K7">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM000003XYS060X" category="C" type-id="804GS" name-id="AV6XZ-07" from="201308">
<dtccode>CB-13</dtccode>
<dtcname>USB Over Current Detection</dtcname>
<subpara id="RM000003XYS060X_01" type-id="60" category="03" proc-id="RM22W0E___0000CCW00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>CB-13</ptxt>
</entry>
<entry valign="middle">
<ptxt>USB device or "iPod" overcurrent malfunction</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>USB device or "iPod"</ptxt>
</item>
<item>
<ptxt>No. 1 stereo jack adapter assembly</ptxt>
</item>
<item>
<ptxt>Wire harness or connector</ptxt>
</item>
<item>
<ptxt>Multi-media interface ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003XYS060X_02" type-id="32" category="03" proc-id="RM22W0E___0000CCX00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E249768E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003XYS060X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003XYS060X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003XYS060X_04_0001" proc-id="RM22W0E___0000CCY00001">
<testtitle>REPLACE USB DEVICE OR "iPod"</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the USB device or "iPod" from the No. 1 stereo jack adapter assembly.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
<atten4>
<ptxt>When one of these DTCs has been stored, it is necessary to turn off the engine switch to make it possible for the vehicle to recognize a new device when it is connected.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Turn the engine switch on (ACC).</ptxt>
</test1>
<test1>
<ptxt>Connect a known good USB device or "iPod" to the No. 1 stereo jack adapter assembly.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003XYS060X_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003XYS060X_04_0002" proc-id="RM22W0E___0000CCZ00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000014CZ0E0X"/>). </ptxt>
</test1>
</content6>
<res>
<down ref="RM000003XYS060X_04_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003XYS060X_04_0003" proc-id="RM22W0E___0000CD000001">
<testtitle>RECHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTC occurs again.</ptxt>
<atten4>
<ptxt>If DTCs are detected frequently, replace the multi-media interface ECU.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction disappears. </ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XYS060X_04_0004" fin="true">OK</down>
<right ref="RM000003XYS060X_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XYS060X_04_0004">
<testtitle>USB DEVICE OR "iPod" WAS DEFECTIVE</testtitle>
</testgrp>
<testgrp id="RM000003XYS060X_04_0005" proc-id="RM22W0E___0000CD100001">
<testtitle>INSPECT MULTI-MEDIA INTERFACE ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the m1 No. 1 stereo jack adapter assembly connector.</ptxt>
<figure>
<graphic graphicname="E194160E05" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>m1-1 (UPO) - m1-4 (UJSG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 1 Stereo Jack Adapter Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003XYS060X_04_0006" fin="true">OK</down>
<right ref="RM000003XYS060X_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XYS060X_04_0006">
<testtitle>REPLACE NO. 1 STEREO JACK ADAPTER ASSEMBLY<xref label="Seep01" href="RM000003AWF03LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XYS060X_04_0007" proc-id="RM22W0E___0000CD200001">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA INTERFACE ECU - NO. 1 STEREO JACK ADAPTER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the L55 multi-media interface ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>L55-1 (UPO) - m1-1 (UPO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>L55-4 (UESG) - m1-4 (UJSG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>m1-1 (UPO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>m1-4 (UJSG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XYS060X_04_0008" fin="true">OK</down>
<right ref="RM000003XYS060X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XYS060X_04_0008">
<testtitle>REPLACE MULTI-MEDIA INTERFACE ECU<xref label="Seep01" href="RM000003AWI017X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XYS060X_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>