<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SE_T00LH" variety="T00LH">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000002S8H02ZX" category="C" type-id="800LD" name-id="NW3DQ-02" from="201308">
<dtccode>B2789</dtccode>
<dtcname>No Response from ID BOX</dtcname>
<subpara id="RM000002S8H02ZX_01" type-id="60" category="03" proc-id="RM22W0E___0000E1Y00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when LIN communication between the certification ECU (smart key ECU assembly) and ID code box (immobiliser code ECU) stops for 10 seconds or more.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.57in"/>
<colspec colname="COL2" colwidth="3.15in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2789</ptxt>
</entry>
<entry valign="middle">
<ptxt>No communication between the certification ECU (smart key ECU assembly) and ID code box (immobiliser code ECU) for 10 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ID code box (immobiliser code ECU)</ptxt>
</item>
<item>
<ptxt>Certification ECU (smart key ECU assembly)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002S8H02ZX_02" type-id="32" category="03" proc-id="RM22W0E___0000E1Z00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B173719E20" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002S8H02ZX_03" type-id="51" category="05" proc-id="RM22W0E___0000E2000001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot:</ptxt>
<ptxt>Connect the intelligent tester to the vehicle, and turn a courtesy switch on and off at 1.5 second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</atten3>
<atten4>
<ptxt>DTC B2785 is stored when the communication between the ID code box (immobiliser code ECU) and certification ECU (smart key ECU assembly) stops.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002S8H02ZX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002S8H02ZX_04_0001" proc-id="RM22W0E___0000E2100001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8H02ZX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8H02ZX_04_0002" proc-id="RM22W0E___0000E2200001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.56in"/>
<colspec colname="COL2" colwidth="1.57in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2789 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2789 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8H02ZX_04_0003" fin="false">A</down>
<right ref="RM000002S8H02ZX_04_0007" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8H02ZX_04_0003" proc-id="RM22W0E___0000E2300001">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU - ID CODE BOX)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E156407E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the E29 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E28 box connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-10 (LIN) - E28-3 (LIN1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-10 (LIN) or E28-3 (LIN1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8H02ZX_04_0004" fin="false">OK</down>
<right ref="RM000002S8H02ZX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8H02ZX_04_0004" proc-id="RM22W0E___0000E2400001">
<testtitle>CHECK HARNESS AND CONNECTOR (ID CODE BOX - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E28 box connector.</ptxt>
<figure>
<graphic graphicname="B157071E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E28-8 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E28-1 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8H02ZX_04_0005" fin="false">OK</down>
<right ref="RM000002S8H02ZX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8H02ZX_04_0005" proc-id="RM22W0E___0000E2500001">
<testtitle>REPLACE ID CODE BOX (IMMOBILISER CODE ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the ID code box (immobiliser code ECU) with a new or normally functioning one.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8H02ZX_04_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8H02ZX_04_0006" proc-id="RM22W0E___0000E2600001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.56in"/>
<colspec colname="COL2" colwidth="1.57in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2789 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2789 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8H02ZX_04_0011" fin="true">A</down>
<right ref="RM000002S8H02ZX_04_0010" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8H02ZX_04_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000UZ30DCX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8H02ZX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8H02ZX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8H02ZX_04_0010">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM000002S8H02ZX_04_0011">
<testtitle>END (ID CODE BOX IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>