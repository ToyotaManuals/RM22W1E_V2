<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001X" variety="S001X">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001X_7C3RJ_T00KM" variety="T00KM">
<name>TOYOTA PARKING ASSIST-SENSOR SYSTEM</name>
<para id="RM000002KST04ZX" category="C" type-id="802GR" name-id="PM467-03" from="201308">
<dtccode>C1AE6</dtccode>
<dtcname>Rear Left Sensor Malfunction</dtcname>
<subpara id="RM000002KST04ZX_01" type-id="60" category="03" proc-id="RM22W0E___0000CQP00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The No. 1 ultrasonic sensor (RL sensor) is installed on the rear bumper. The ECU detects obstacles based on signals received from the No. 1 ultrasonic sensor (RL sensor). If the No. 1 ultrasonic sensor (RL sensor) has an open circuit or other malfunction, it will not function normally.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1AE6</ptxt>
</entry>
<entry valign="middle">
<ptxt>A malfunction of the No. 1 ultrasonic sensor (RL sensor).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>No. 1 ultrasonic sensor (RL sensor)</ptxt>
</item>
<item>
<ptxt>Parking assist ECU*1</ptxt>
</item>
<item>
<ptxt>Clearance warning ECU assembly*2</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Side Monitor System</ptxt>
</item>
<item>
<ptxt>*2: w/o Side Monitor System</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002KST04ZX_03" type-id="51" category="05" proc-id="RM22W0E___0000CQQ00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>If the DTC is output after repairs, turn the engine switch on (IG) and move the shift lever to R. Then clear the DTC.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002KST04ZX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002KST04ZX_04_0001" proc-id="RM22W0E___0000CQR00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000003AKD003X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000003AKD003X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC C1AE6 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002KST04ZX_04_0007" fin="true">OK</down>
<right ref="RM000002KST04ZX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002KST04ZX_04_0003" proc-id="RM22W0E___0000CQS00001">
<testtitle>REPLACE NO. 1 ULTRASONIC SENSOR (RL SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the No. 1 ultrasonic sensor (RL sensor) with a normally functioning one (See page <xref label="Seep01" href="RM0000039LP006X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002KST04ZX_04_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002KST04ZX_04_0004" proc-id="RM22W0E___0000CQT00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000003AKD003X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000003AKD003X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC C1AE6 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1AE6 is output (w/ Side Monitor System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1AE6 is output (w/o Side Monitor System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002KST04ZX_04_0006" fin="true">A</down>
<right ref="RM000002KST04ZX_04_0010" fin="true">B</right>
<right ref="RM000002KST04ZX_04_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002KST04ZX_04_0006">
<testtitle>END (NO. 1 ULTRASONIC SENSOR [RL SENSOR] IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002KST04ZX_04_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002KST04ZX_04_0005">
<testtitle>REPLACE CLEARANCE WARNING ECU ASSEMBLY<xref label="Seep01" href="RM0000039L7003X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002KST04ZX_04_0010">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM0000039LH004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>