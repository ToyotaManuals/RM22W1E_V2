<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3U4_T00N7" variety="T00N7">
<name>FRONT SEAT SIDE AIRBAG ASSEMBLY (for Manual Seat)</name>
<para id="RM00000390T01IX" category="A" type-id="80001" name-id="RSGI7-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM00000390T01IX_02" type-id="11" category="10" proc-id="RM22W0E___0000FJD00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for LHD and RHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000390T01IX_01" type-id="01" category="01">
<s-1 id="RM00000390T01IX_01_0036" proc-id="RM22W0E___0000FJ200000">
<ptxt>REMOVE FRONT SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front seat assembly LH (See page <xref label="Seep01" href="RM00000390S00RX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390T01IX_01_0038" proc-id="RM22W0E___0000FJ400000">
<ptxt>REMOVE VERTICAL ADJUSTER COVER LH (for 8 Way Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180752" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the vertical adjuster cover LH.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01IX_01_0039" proc-id="RM22W0E___0000FJ500000">
<ptxt>REMOVE VERTICAL ADJUSTING HANDLE LH (for 8 Way Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180753" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws and vertical adjusting handle LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01IX_01_0040" proc-id="RM22W0E___0000FJ600000">
<ptxt>REMOVE VERTICAL SEAT ADJUSTER KNOB (for 8 Way Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180754" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the snap ring and vertical seat adjuster knob.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01IX_01_0041" proc-id="RM22W0E___0000FJ700000">
<ptxt>REMOVE RECLINING ADJUSTER RELEASE HANDLE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180755E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Lift the reclining adjuster release handle LH.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the claw and remove the reclining adjuster release handle LH.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01IX_01_0042" proc-id="RM22W0E___0000FJ800000">
<ptxt>REMOVE FRONT SEAT CUSHION SHIELD LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Seatback Board:</ptxt>
<ptxt>Disconnect the 2 rubber bands from the 2 hooks.</ptxt>
<figure>
<graphic graphicname="B180757" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Seatback Board:</ptxt>
<ptxt>Disconnect the rubber band from the 2 hooks.</ptxt>
<figure>
<graphic graphicname="B180756" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for 4 Way Seat Type:</ptxt>
<figure>
<graphic graphicname="B180758E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the 2 screws.</ptxt>
</s3>
<s3>
<ptxt>Detach the 3 claws in the order shown in the illustration, and then remove the front seat cushion shield LH.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 8 Way Seat Type:</ptxt>
<figure>
<graphic graphicname="B180759E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the 3 screws.</ptxt>
</s3>
<s3>
<ptxt>Detach the 3 claws in the order shown in the illustration, and then remove the front seat cushion shield LH.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Lumbar Support:</ptxt>
<ptxt>Disconnect the lumbar switch connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01IX_01_0035" proc-id="RM22W0E___0000G9M00000">
<ptxt>REMOVE FRONT SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connectors and detach the clamps.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B189677" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the nut and front seat inner belt assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01IX_01_0043" proc-id="RM22W0E___0000FJ900000">
<ptxt>REMOVE FRONT INNER SEAT CUSHION SHIELD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180761" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and clip, and then remove the front inner seat cushion shield LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01IX_01_0044" proc-id="RM22W0E___0000FJA00000">
<ptxt>REMOVE FRONT SEATBACK BOARD SUB-ASSEMBLY LH (w/ Seatback Board)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180711" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Move the front seatback board sub-assembly LH in the direction of the arrow to detach the 2 hooks and remove it.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01IX_01_0045" proc-id="RM22W0E___0000FJB00000">
<ptxt>REMOVE FRONT SEAT HEADREST SUPPORT (w/ Active Headrest)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180712" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the 2 front seat headrest supports.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01IX_01_0046" proc-id="RM22W0E___0000FJC00000">
<ptxt>REMOVE SEATBACK COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Seatback Board:</ptxt>
<s3>
<ptxt>Remove the 3 hog rings and detach the 9 hooks.</ptxt>
<figure>
<graphic graphicname="B191295E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<ptxt>Remove the nut and seatback cover bracket from the seat frame.</ptxt>
<figure>
<graphic graphicname="B180716" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Seatback Board:</ptxt>
<s3>
<ptxt>Remove the 3 hog rings.</ptxt>
<figure>
<graphic graphicname="B180766" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Open the 2 fasteners, and then open the front separate type seatback cover.</ptxt>
<figure>
<graphic graphicname="B180767" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<ptxt>Remove the nut and seatback cover bracket from the seat frame.</ptxt>
<figure>
<graphic graphicname="B180716" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Detach the 4 claws and remove the 2 front seat headrest supports.</ptxt>
<figure>
<graphic graphicname="B180768" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Remove the seatback cover with pad from the seatback frame.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390T01IX_01_0037" proc-id="RM22W0E___0000FJ300000">
<ptxt>REMOVE FRONT SEAT AIRBAG ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 5 clamps and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="B102194" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 clamps and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="B102198" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts and front seat airbag assembly LH.</ptxt>
<atten2>
<ptxt>Make sure that the seat frame assembly is not deformed. If it is, replace it with a new one.</ptxt>
</atten2>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>