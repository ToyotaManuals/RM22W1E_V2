<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001X" variety="S001X">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001X_7C3RS_T00KV" variety="T00KV">
<name>PARKING ASSIST MONITOR SYSTEM (w/o Side Monitor System)</name>
<para id="RM000003WW903LX" category="J" type-id="804E8" name-id="PM5OO-01" from="201301" to="201308">
<dtccode/>
<dtcname>Image from Camera for Parking Assist Monitor is Abnormal</dtcname>
<subpara id="RM000003WW903LX_01" type-id="60" category="03" proc-id="RM22W0E___0000CW600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The video signal from the rear television camera assembly is transmitted to the multi-media module receiver assembly.</ptxt>
</content5>
</subpara>
<subpara id="RM000003WW903LX_02" type-id="32" category="03" proc-id="RM22W0E___0000CW700000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E240589E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003WW903LX_03" type-id="51" category="05" proc-id="RM22W0E___0000CW800000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When "System initializing" is displayed on the multi-media module receiver assembly after disconnecting the cable from the negative (-) battery terminal, correct the steering angle neutral point (See page <xref label="Seep01" href="RM0000035DE03BX"/>).</ptxt>
</item>
<item>
<ptxt>Depending on the parts that are replaced or operations that are performed during vehicle inspection or maintenance, calibration of other systems as well as the parking assist monitor system (w/o Side Monitor System) may be needed (See page <xref label="Seep02" href="RM0000035DD03RX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Images may be unclear even in normal conditions if:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Noise may occur in the image during power back door operation (w/ Power Back Door System).</ptxt>
</item>
<item>
<ptxt>Electrical devices are used in the cabin (noise may occur in the image).</ptxt>
</item>
<item>
<ptxt>Accessories that generate radio waves have been installed (noise may occur in the image).</ptxt>
</item>
<item>
<ptxt>The outer mirror switch assembly is operated (noise may occur in the image).</ptxt>
</item>
<item>
<ptxt>The camera screen is frosted over (the image immediately after turning the ignition switch to ON may be blurred or darker than normal).</ptxt>
</item>
<item>
<ptxt>The camera lens is dirty with snow, mud, etc.</ptxt>
</item>
<item>
<ptxt>A strong beam of light, such as a sunbeam or headlight, hits the camera.</ptxt>
</item>
<item>
<ptxt>It is too dark around the camera (at night etc.).</ptxt>
</item>
<item>
<ptxt>The ambient temperature around the camera is either too high or too low.</ptxt>
</item>
<item>
<ptxt>The vehicle is tilted at a steep angle.</ptxt>
</item>
<item>
<ptxt>The rear television camera assembly lens is scratched.</ptxt>
</item>
<item>
<ptxt>The rear television camera assembly lens has drops of water on it or the humidity is high.</ptxt>
</item>
<item>
<ptxt>When the camera is used under fluorescent lights, sodium lights, or mercury lights etc., the lights and the illuminated area may appear to flicker.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000003WW903LX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003WW903LX_04_0008" proc-id="RM22W0E___0000CVT00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA MODULE RECEIVER - REAR TELEVISION CAMERA AND BODY GROUND)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F77 multi-media module receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the S5 rear television camera assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F77-18 (V+) - S5-2 (CV+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-19 (V-) - S5-1 (CV-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-39 (CA+) - S5-4 (CB+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-40 (CGND) - S5-3 (CGND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-12 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-18 (V+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-19 (V-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-39 (CA+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-40 (CGND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000003WW903LX_04_0009" fin="false">OK</down>
<right ref="RM000003WW903LX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WW903LX_04_0009" proc-id="RM22W0E___0000CVU00000">
<testtitle>CHECK MULTI-MEDIA MODULE RECEIVER ASSEMBLY (CA+, CGND)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the multi-media module receiver assembly connector.</ptxt>
<figure>
<graphic graphicname="E245957E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F77-40 (CGND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F77-39 (CA+) - F77-40 (CGND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG), shift lever in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.5 to 7.05 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Multi-media Module Receiver Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000003WW903LX_04_0011" fin="false">OK</down>
<right ref="RM000003WW903LX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WW903LX_04_0011" proc-id="RM22W0E___0000CVV00000">
<testtitle>CHECK REAR TELEVISION CAMERA ASSEMBLY (CV+, CGND)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the S5 rear television camera assembly connector.</ptxt>
</test1>
<figure>
<graphic graphicname="E239396E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" align="center" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Multi-media Module Receiver Assembly)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Waveform A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Synchronization Signal</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Video Waveform</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<test1>
<ptxt>Check the waveform of the rear television camera assembly using an oscilloscope. </ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A waterproof connector is used for the rear television camera assembly. Therefore, inspect the waveform at the multi-media module receiver assembly with the connector connected.</ptxt>
</item>
<item>
<ptxt>The video waveform changes according to the image sent by the rear television camera assembly.</ptxt>
</item>
</list1>
</atten4>
<table>
<title>Measurement Condition</title>
<tgroup cols="2" align="center">
<colspec colname="COLSPEC1" colwidth="1.65in"/>
<colspec colname="COLSPEC0" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Tester No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>F77-18 (V+) - F77-40 (CGND)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.2 V/DIV., 50 μS/DIV</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Waveform A: Engine starts, shift lever in R (camera lens is not covered, displaying an image)</ptxt>
<ptxt>Waveform B: Engine starts, shift lever in R (camera lens is covered, blacking out the screen)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is as shown in the illustration. </ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000003WW903LX_04_0018" fin="false">OK</down>
<right ref="RM000003WW903LX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WW903LX_04_0018" proc-id="RM22W0E___0000CW900000">
<testtitle>REPLACE HARNESS AND CONNECTOR (GVIF CABLE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the harness and connector (GVIF cable) with a normally functioning one.  </ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WW903LX_04_0019" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WW903LX_04_0019" proc-id="RM22W0E___0000CWA00000">
<testtitle>CHECK PARKING ASSIST MONITOR SYSTEM (w/o Side Monitor System)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the same malfunction recurs when the parking assist monitor screen is displayed.  </ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction does not reoccur (returns to normal).</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WW903LX_04_0012" fin="true">OK</down>
<right ref="RM000003WW903LX_04_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WW903LX_04_0012">
<testtitle>END (GVIF CABLE IS DEFECTIVE)<xref label="Seep01" href="RM0000035D703YX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WW903LX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003WW903LX_04_0005">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WW903LX_04_0007">
<testtitle>REPLACE REAR TELEVISION CAMERA ASSEMBLY<xref label="Seep01" href="RM0000038O600AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WW903LX_04_0020">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01SX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>