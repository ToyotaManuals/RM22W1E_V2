<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T9_T00MC" variety="T00MC">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000000XUQ0H7X" category="J" type-id="800Q8" name-id="TD63A-03" from="201308">
<dtccode/>
<dtcname>Rear Door LH Entry Lock Function does not Operate</dtcname>
<subpara id="RM000000XUQ0H7X_01" type-id="60" category="03" proc-id="RM22W0E___0000EQ500001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the rear door LH entry lock function does not operate but the entry unlock function operates, the communication line between the vehicle and electrical key transmitter is normal. The part at fault may be an entry lock switch circuit (certification ECU (smart key ECU assembly) → rear door outside handle LH (entry lock switch) → rear door electrical key oscillator LH).</ptxt>
<atten4>
<ptxt>For vehicles with entry function for rear doors.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XUQ0H7X_04" type-id="32" category="03" proc-id="RM22W0E___0000EQ600001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B186276E07" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XUQ0H7X_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000XUQ0H7X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XUQ0H7X_05_0012" proc-id="RM22W0E___0000EQ700001">
<testtitle>CHECK POWER DOOR LOCK OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When the master switch assembly door control switch is operated, check that the locked doors unlock.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Door locks operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0H7X_05_0027" fin="false">OK</down>
<right ref="RM000000XUQ0H7X_05_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0027" proc-id="RM22W0E___0000EQE00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR LOCK POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Main Body / Data List.</ptxt>
</test1>
<test1>
<ptxt>Read the Data List according to the display on the intelligent tester.</ptxt>
<test2>
<ptxt>for LHD</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DR Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side rear door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side rear door unlocked</ptxt>
<ptxt>OFF: Driver side rear door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between ON and OFF as shown in the chart above.</ptxt>
</specitem>
</spec>
</test2>
<test2>
<ptxt>for RHD</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PR Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Passenger side rear door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Passenger side rear door unlocked</ptxt>
<ptxt>OFF: Passenger side rear door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between ON and OFF as shown in the chart above.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0H7X_05_0013" fin="false">OK</down>
<right ref="RM000000XUQ0H7X_05_0028" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0013" proc-id="RM22W0E___0000EQ800001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (LOCK SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List.</ptxt>
<test2>
<ptxt>for LHD</ptxt>
<table pgwide="1">
<title>Entry &amp; Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Dr-Door Trigger Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side rear door outside handle lock switch / ON or OFF </ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Entry lock switch pushed</ptxt>
<ptxt>OFF: Entry lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On tester screen, item changes between ON and OFF according to above chart.</ptxt>
</specitem>
</spec>
</test2>
<test2>
<ptxt>for RHD</ptxt>
<table pgwide="1">
<title>Entry &amp; Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Pr-Door Trigger Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Passenger side rear door outside handle lock switch / ON or OFF </ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Entry lock switch pushed</ptxt>
<ptxt>OFF: Entry lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On tester screen, item changes between ON and OFF according to above chart.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0H7X_05_0025" fin="true">OK</down>
<right ref="RM000000XUQ0H7X_05_0015" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0015" proc-id="RM22W0E___0000EQA00001">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR DOOR OUTSIDE HANDLE - DOOR ELECTRICAL KEY OSCILLATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the z28 oscillator connector.</ptxt>
<figure>
<graphic graphicname="B164339E49" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the z27 handle connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z28-9 (TRG-) - z27-1 (TRG-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z28-9 (TRG-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0H7X_05_0016" fin="false">OK</down>
<right ref="RM000000XUQ0H7X_05_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0016" proc-id="RM22W0E___0000EQB00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU [SMART KEY ECU ASSEMBLY] - REAR DOOR OUTSIDE HANDLE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E30 ECU connector.</ptxt>
<figure>
<graphic graphicname="B187978E03" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the z27 handle connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E30-25 (TSW3) - z27-3 (TRG+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-25 (TSW3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E30-26 (TSW4) - z27-3 (TRG+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-26 (TSW4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0H7X_05_0014" fin="false">OK</down>
<right ref="RM000000XUQ0H7X_05_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0014" proc-id="RM22W0E___0000EQ900001">
<testtitle>INSPECT REAR DOOR OUTSIDE HANDLE LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="B184271E09" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>z27-1 (TRG-) - z27-3 (TRG+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lock switch pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0H7X_05_0026" fin="false">OK</down>
<right ref="RM000000XUQ0H7X_05_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0026" proc-id="RM22W0E___0000EQD00001">
<testtitle>REPLACE DOOR ELECTRICAL KEY OSCILLATOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the door electrical key oscillator (See page <xref label="Seep01" href="RM000002M8106XX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0H7X_05_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0017" proc-id="RM22W0E___0000EQC00001">
<testtitle>CHECK DOOR ELECTRICAL KEY OSCILLATOR (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the entry function operates normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0H7X_05_0023" fin="true">OK</down>
<right ref="RM000000XUQ0H7X_05_0025" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0019">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM<xref label="Seep01" href="RM000002T6K05PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0020">
<testtitle>REPLACE REAR DOOR OUTSIDE HANDLE<xref label="Seep01" href="RM0000039H4025X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0021">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0022">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0023">
<testtitle>END (DOOR ELECTRICAL OSCILLATOR IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0025">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM000000XUQ0H7X_05_0028">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM (Proceed to Only Rear Door LH LOCK/UNLOCK Functions do not Operate)<xref label="Seep01" href="RM000000TNU074X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>