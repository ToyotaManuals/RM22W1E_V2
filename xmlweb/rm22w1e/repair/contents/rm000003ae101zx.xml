<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3QW_T00JZ" variety="T00JZ">
<name>AMPLIFIER ANTENNA</name>
<para id="RM000003AE101ZX" category="A" type-id="80001" name-id="AVCDX-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM000003AE101ZX_01" type-id="01" category="01">
<s-1 id="RM000003AE101ZX_01_0012" proc-id="RM22W0E___0000BRD00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="2">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003AE101ZX_01_0010" proc-id="RM22W0E___0000BRB00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003AE101ZX_01_0006" proc-id="RM22W0E___0000BR700001">
<ptxt>REMOVE ROOF HEADLINING ASSEMBLY (w/ Diversity)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>Remove the roof headlining (See page <xref label="Seep01" href="RM0000038MO00VX"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Remove the roof headlining (See page <xref label="Seep02" href="RM0000038MO00UX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AE101ZX_01_0007" proc-id="RM22W0E___0000BR800001">
<ptxt>REMOVE REAR NO. 2 ROOF AIR DUCT (w/ Diversity)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>Remove the 2 clips and duct.</ptxt>
<figure>
<graphic graphicname="B181742" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Remove the 2 clips and duct.</ptxt>
<figure>
<graphic graphicname="B184106" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AE101ZX_01_0008" proc-id="RM22W0E___0000BR900001">
<ptxt>REMOVE REAR NO. 5 ROOF AIR DUCT (w/ Diversity)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>Remove the 2 clips and duct.</ptxt>
<figure>
<graphic graphicname="B181739" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Remove the 2 clips and duct.</ptxt>
<figure>
<graphic graphicname="B184105" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AE101ZX_01_0003" proc-id="RM22W0E___0000BR400001">
<ptxt>REMOVE REAR NO. 4 ROOF AIR DUCT (w/ Diversity)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 clips and duct.</ptxt>
<figure>
<graphic graphicname="B181740" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AE101ZX_01_0004" proc-id="RM22W0E___0000BR500001">
<ptxt>REMOVE REAR NO. 3 ROOF AIR DUCT (w/ Diversity)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip and duct.</ptxt>
<figure>
<graphic graphicname="B181741" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AE101ZX_01_0005" proc-id="RM22W0E___0000BR600001">
<ptxt>REMOVE REAR NO. 1 ROOF AIR DUCT (w/ Diversity)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 clips and duct.</ptxt>
<figure>
<graphic graphicname="B181743" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AE101ZX_01_0009" proc-id="RM22W0E___0000BRA00001">
<ptxt>REMOVE AMPLIFIER ANTENNA ASSEMBLY (w/ Diversity)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 3 connectors and remove the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B181738" width="7.106578999in" height="5.787629434in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 9 clamps and remove the antenna.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AE101ZX_01_0011" proc-id="RM22W0E___0000BRC00001">
<ptxt>REMOVE AMPLIFIER ANTENNA ASSEMBLY (w/o Diversity)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 connectors and detach the clamp.</ptxt>
<figure>
<graphic graphicname="B191388" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and amplifier antenna.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>