<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000VSO0IIX" category="C" type-id="302BE" name-id="ESSGX-06" from="201301" to="201308">
<dtccode>P0443</dtccode>
<dtcname>Evaporative Emission Control System Purge Control Valve Circuit</dtcname>
<subpara id="RM000000VSO0IIX_01" type-id="60" category="03" proc-id="RM22W0E___00001DD00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>To reduce hydrocarbon emissions, evaporated fuel from the fuel tank is routed through a charcoal canister to the intake manifold for combustion in the cylinders.</ptxt>
<ptxt>The ECM changes the duty signal sent to the purge VSV (Vacuum Switching Valve for Purge Control) so that the intake amount of hydrocarbon emissions is appropriate for the driving conditions (engine load, engine speed, vehicle speed, etc.) after the engine is warmed up.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0443</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal voltage of the ECM output circuit does not correspond with the drive signals sent from the ECM to the purge VSV (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in purge VSV circuit</ptxt>
</item>
<item>
<ptxt>Purge VSV</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000VSO0IIX_06" type-id="73" category="03" proc-id="RM22W0E___00001DL00000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A179391E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Start the engine and warm it up until the engine coolant temperature is 75°C (167°F) or higher [A].</ptxt>
<atten4>
<ptxt>The A/C switch and all accessory switches should be off.</ptxt>
</atten4>
</item>
<item>
<ptxt>Idle the engine for 15 minutes or more [B].</ptxt>
<atten4>
<ptxt>Check the EVAP (Purge) VSV item in the Data List. When the value of this item is between 5 and 95%, the judgment will be performed.</ptxt>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes [C].</ptxt>
</item>
<item>
<ptxt>Read the pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a pending DTC is output, the system is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If a pending DTC is not output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P0443.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows INCOMPLETE or N/A, perform steps [D] through [E].</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Drive the vehicle at a constant speed between 40 and 60 km/h (25 and 38 mph) for 10 minutes [D].</ptxt>
<atten2>
<ptxt>When performing the confirmation driving pattern, obey all speed limits and traffic laws.</ptxt>
</atten2>
</item>
<item>
<ptxt>Check the DTC judgment result [E].</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000VSO0IIX_02" type-id="32" category="03" proc-id="RM22W0E___00001DE00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A278620E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000VSO0IIX_03" type-id="51" category="05" proc-id="RM22W0E___00001DF00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000VSO0IIX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000VSO0IIX_04_0001" proc-id="RM22W0E___00001DG00000">
<testtitle>PERFORM ACTIVE TEST USING GTS (ACTIVATE THE VSV FOR EVAP CONTROL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
<figure>
<graphic graphicname="A214696E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the vacuum hose (charcoal canister side) from the purge VSV.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Activate the VSV for EVAP Control.</ptxt>
</test1>
<test1>
<ptxt>When the purge VSV is operated using the GTS, check whether the port of the purge VSV applies suction to your finger.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.30in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>VSV ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Purge VSV port applies suction to finger</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>VSV OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Purge VSV port applies no suction to finger</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the vacuum hose to the purge VSV.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000VSO0IIX_04_0006" fin="true">OK</down>
<right ref="RM000000VSO0IIX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VSO0IIX_04_0002" proc-id="RM22W0E___00001DH00000">
<testtitle>INSPECT PURGE VSV</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the purge VSV (See page <xref label="Seep01" href="RM00000398D005X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000VSO0IIX_04_0003" fin="false">OK</down>
<right ref="RM000000VSO0IIX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VSO0IIX_04_0003" proc-id="RM22W0E___00001DI00000">
<testtitle>INSPECT PURGE VSV (POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the purge VSV connector.</ptxt>
<figure>
<graphic graphicname="A161605E40" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C55-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Purge VSV)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the purge VSV connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000VSO0IIX_04_0005" fin="false">OK</down>
<right ref="RM000000VSO0IIX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VSO0IIX_04_0005" proc-id="RM22W0E___00001DJ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (PURGE VSV - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the purge VSV connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C55-2 - C46-63 (PRG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C55-2 or C46-63 (PRG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C55-2 - C45-63 (PRG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C55-2 or C45-63 (PRG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the purge VSV connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000VSO0IIX_04_0016" fin="false">OK</down>
<right ref="RM000000VSO0IIX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VSO0IIX_04_0016" proc-id="RM22W0E___00001DK00000">
<testtitle>PERFORM CONFIRMATION DRIVING PATTERN</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up until the engine coolant temperature is 75°C (167°F) or higher.</ptxt>
</test1>
<test1>
<ptxt>Idle the engine for 15 minutes or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</test1>
<test1>
<ptxt>Input the DTC: P0443.</ptxt>
</test1>
<test1>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL3" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NORMAL (DTC is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABNORMAL (DTC P0443 is output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the judgment result shows INCOMPLETE or N/A, drive the vehicle at a constant speed between 40 and 60 km/h (25 and 38 mph) for 10 minutes and check the DTC judgment result again.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000VSO0IIX_04_0010" fin="true">A</down>
<right ref="RM000000VSO0IIX_04_0011" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000VSO0IIX_04_0006">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ108X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000VSO0IIX_04_0007">
<testtitle>REPLACE PURGE VSV<xref label="Seep01" href="RM00000398F005X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000VSO0IIX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR OR REPLACE FUSE (PURGE VSV - NO. 1 INTEGRATION RELAY)</testtitle>
</testgrp>
<testgrp id="RM000000VSO0IIX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000VSO0IIX_04_0011">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000VSO0IIX_04_0010">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ108X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>