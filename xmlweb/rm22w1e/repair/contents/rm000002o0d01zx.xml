<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12054_S0027" variety="S0027">
<name>METER / GAUGE / DISPLAY</name>
<ttl id="12054_S0027_7C3TU_T00MX" variety="T00MX">
<name>METER / GAUGE SYSTEM</name>
<para id="RM000002O0D01ZX" category="C" type-id="802SZ" name-id="ME46T-02" from="201301">
<dtccode>U0129</dtccode>
<dtcname>Lost Communication with Brake System Control Module</dtcname>
<subpara id="RM000002O0D01ZX_01" type-id="60" category="03" proc-id="RM22W0E___0000F5J00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The combination meter communicates with the skid control ECU via the CAN line.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U0129</ptxt>
</entry>
<entry valign="middle">
<ptxt>When 15 seconds have elapsed after the engine is started and IG voltage is 10.5 V or more while no communication with the skid control ECU continues for 3 seconds or more (no communication with the ECM or skid control ECU continues for 60 seconds or more).</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002O0D01ZX_02" type-id="32" category="03" proc-id="RM22W0E___0000F5K00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E140490E12" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002O0D01ZX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002O0D01ZX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002O0D01ZX_04_0001" proc-id="RM22W0E___0000F5L00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs and note any codes that are output  (See page <xref label="Seep01" href="RM00000302X011X"/>).</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM00000302X011X"/>).</ptxt>
</test1>
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep03" href="RM00000302X011X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>CAN communication system DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system (for LHD) DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system (for RHD) DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If CAN communication system DTC U0129 and another DTC are output at the same time, perform troubleshooting for CAN communication system DTC U0129 first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002O0D01ZX_04_0005" fin="true">A</down>
<right ref="RM000002O0D01ZX_04_0003" fin="true">B</right>
<right ref="RM000002O0D01ZX_04_0006" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002O0D01ZX_04_0003">
<testtitle>Go to CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002O0D01ZX_04_0005">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002O0D01ZX_04_0006">
<testtitle>Go to CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>