<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12060_S002C" variety="S002C">
<name>SEAT BELT</name>
<ttl id="12060_S002C_7C3V8_T00OB" variety="T00OB">
<name>SEAT BELT WARNING SYSTEM</name>
<para id="RM000000Y5I0FVX" category="D" type-id="3001B" name-id="SB0019-345" from="201308">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM000000Y5I0FVX_z0" proc-id="RM22W0E___0000GF900001">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the following procedures to troubleshoot the seat belt warning system.</ptxt>
</item>
<item>
<ptxt>*: Use the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding to the next step.</ptxt>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK FOR DTC*</testtitle>
<test1>
<ptxt>Check for DTCs and make a note of any codes that are output.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs.</ptxt>
</test1>
<test1>
<ptxt>Based on the DTCs output above, try to cause the output of the same CAN communication system DTC or airbag system DTC by simulating the symptoms indicated by the DTC. Then recheck for DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>CAN communication system DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>CAN communication system DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Airbag system DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to CAN COMMUNICATION SYSTEM (See page <xref label="Seep05" href="RM000001RSO09SX"/>)</action-ci-right>
<result>C</result>
<action-ci-right>Go to CAN COMMUNICATION SYSTEM (See page <xref label="Seep07" href="RM000001RSO09TX"/>)</action-ci-right>
<result>D</result>
<action-ci-right>Go to AIRBAG SYSTEM (See page <xref label="Seep06" href="RM000000XF60KBX"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to the problem symptoms table (See page <xref label="Seep01" href="RM000003D2I02HX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>OVERALL ANALYSIS AND TROUBLESHOOTING*</testtitle>
<test1>
<ptxt>Operation check (See page <xref label="Seep04" href="RM000002N5302PX"/>)</ptxt>
</test1>
<test1>
<ptxt>Data List / Active Test (See page <xref label="Seep02" href="RM0000012G70FDX"/>)</ptxt>
</test1>
<test1>
<ptxt>Terminals of ECU (See page <xref label="Seep03" href="RM0000014DJ09GX"/>)</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>REPAIR OR REPLACE</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>