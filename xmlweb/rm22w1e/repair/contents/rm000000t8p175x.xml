<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000T8P175X" category="C" type-id="302I5" name-id="ESUAO-04" from="201301" to="201308">
<dtccode>P0560</dtccode>
<dtcname>System Voltage</dtcname>
<subpara id="RM000000T8P175X_01" type-id="60" category="03" proc-id="RM22W0E___00001FX00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The battery supplies electricity to the ECM even when the engine switch is off. This power allows the ECM to store data such as DTC history, freeze frame data and fuel trim values. If the battery voltage falls below a minimum level, the memory is cleared and the ECM determines that there is a malfunction in the power supply circuit. When the engine is next started, the ECM illuminates the MIL and stores the DTC.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0560</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open in the ECM backup power source circuit (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in backup power source circuit</ptxt>
</item>
<item>
<ptxt>Battery</ptxt>
</item>
<item>
<ptxt>Battery terminals</ptxt>
</item>
<item>
<ptxt>EFI fuse</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTC P0560 is stored, the ECM does not store other DTCs and the data stored in the ECM is partly cleared.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000T8P175X_08" type-id="73" category="03" proc-id="RM22W0E___00001G400000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<list1 type="ordered">
<item>
<ptxt>Turn the engine switch on (IG) and wait for 5 seconds or more.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000T8P175X_05" type-id="32" category="03" proc-id="RM22W0E___00001FY00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A164743E13" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000T8P175X_06" type-id="51" category="05" proc-id="RM22W0E___00001FZ00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000T8P175X_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000T8P175X_07_0005" proc-id="RM22W0E___00001G000000">
<testtitle>CHECK HARNESS AND CONNECTOR (ECM - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the positive (+) battery terminal.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A52-1 (BATT) - Battery positive (+) terminal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A52-1 (BATT) or Battery positive (+) terminal - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A38-1 (BATT) - Battery positive (+) terminal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A38-1 (BATT) or Battery positive (+) terminal - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8P175X_07_0006" fin="false">OK</down>
<right ref="RM000000T8P175X_07_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8P175X_07_0006" proc-id="RM22W0E___00001G100000">
<testtitle>INSPECT BATTERY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the battery is not depleted (See page <xref label="Seep01" href="RM000001AR90AOX_01_0001"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Battery is not depleted.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8P175X_07_0010" fin="false">OK</down>
<right ref="RM000000T8P175X_07_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8P175X_07_0010" proc-id="RM22W0E___00001G200000">
<testtitle>CHECK BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the battery terminals are not loose or corroded.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Battery terminals are not loose or corroded.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8P175X_07_0012" fin="false">OK</down>
<right ref="RM000000T8P175X_07_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8P175X_07_0012" proc-id="RM22W0E___00001G300000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Wait 5 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0560 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000T8P175X_07_0004" fin="true">A</down>
<right ref="RM000000T8P175X_07_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000T8P175X_07_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000T8P175X_07_0008">
<testtitle>REPLACE BATTERY</testtitle>
</testgrp>
<testgrp id="RM000000T8P175X_07_0011">
<testtitle>REPAIR BATTERY TERMINAL</testtitle>
</testgrp>
<testgrp id="RM000000T8P175X_07_0009">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ108X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000T8P175X_07_0004">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>