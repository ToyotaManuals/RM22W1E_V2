<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001S" variety="S001S">
<name>STEERING COLUMN</name>
<ttl id="12038_S001S_7C3QE_T00JH" variety="T00JH">
<name>STEERING SYSTEM</name>
<para id="RM0000039RR018X" category="T" type-id="3001H" name-id="SR11Y-21" from="201301" to="201308">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM0000039RR018X_z0" proc-id="RM22W0E___0000BFP00000">
<content5 releasenbr="7">
<atten4>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Steering System</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="15" colsep="1" valign="middle" align="left">
<ptxt>Hard steering</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Tires (Improperly inflated)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000003DGS00LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering fluid level (Low) for 1VD-FTV without DPF</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001I4G01ZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Power steering fluid level (Low) for 1VD-FTV with DPF</ptxt>
</entry>
<entry colsep="1" align="center">
<ptxt>
<xref href="RM000001I4G020X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Drive belt (Loose) for 1UR-FE</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000002WF601SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Drive belt (Loose) for 1GR-FE</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001ZYL029X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Drive belt (Loose) for 3UR-FE</ptxt>
</entry>
<entry colsep="1" align="center">
<ptxt>
<xref href="RM000002WF601TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Front wheel alignment (Incorrect)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001V43020X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Steering system joints (Worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Suspension arm ball joints (Worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000308J00VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Steering column (Binding)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering vane pump for 1GR-FE</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001A1Q03RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering vane pump for 1UR-FE</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001A1Q03TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering vane pump for 1VD-FTV without DPF</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000018NX00EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Power steering vane pump for 1VD-FTV with DPF</ptxt>
</entry>
<entry colsep="1" align="center">
<ptxt>
<xref href="RM0000018NX00FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Power steering vane pump for 3UR-FE</ptxt>
</entry>
<entry colsep="1" align="center">
<ptxt>
<xref href="RM000001A1Q03SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering gear</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000039TB01AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="3" colsep="1" valign="middle" align="left">
<ptxt>Poor return</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Tires (Improperly inflated)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000003DGS00LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Front wheel alignment (Incorrect)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001V43020X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Steering column (Binding)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering gear</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000039TB01AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="4" colsep="1" valign="middle" align="left">
<ptxt>Excessive play</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Steering system joints (Worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Suspension arm ball joints (Worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM00000308J00VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Intermediate shaft, Sliding yoke (Worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Front wheel bearing (Worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001IWW04VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering gear</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000039TB01AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="8" colsep="1" valign="middle" align="left">
<ptxt>Abnormal noise</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering fluid level (Low) for 1VD-FTV without DPF</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001I4G01ZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Power steering fluid level (Low) for 1VD-FTV with DPF</ptxt>
</entry>
<entry colsep="1" align="center">
<ptxt>
<xref href="RM000001I4G020X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Steering system joints (Worn)</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering vane pump for 1GR-FE</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001A1Q03RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering vane pump for 1UR-FE</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM000001A1Q03TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering vane pump for 1VD-FTV without DPF</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000018NX00EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Power steering vane pump for 1VD-FTV with DPF</ptxt>
</entry>
<entry colsep="1" align="center">
<ptxt>
<xref href="RM0000018NX00FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Power steering vane pump for 3UR-FE</ptxt>
</entry>
<entry colsep="1" align="center">
<ptxt>
<xref href="RM000001A1Q03SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Power steering gear</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>
<xref href="RM0000039TB01AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>