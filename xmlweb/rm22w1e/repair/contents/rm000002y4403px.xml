<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12067_S002J" variety="S002J">
<name>MIRROR (EXT)</name>
<ttl id="12067_S002J_7C3Y6_T00R9" variety="T00R9">
<name>POWER MIRROR CONTROL SYSTEM (w/ Retract Mirror)</name>
<para id="RM000002Y4403PX" category="J" type-id="302OR" name-id="MX3D5-01" from="201301" to="201308">
<dtccode/>
<dtcname>Driver Side Power Mirror cannot be Adjusted with Power Mirror Switch</dtcname>
<subpara id="RM000002Y4403PX_01" type-id="61" category="03" proc-id="RM22W0E___0000IOT00000">
<name>SYSTEM DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit detects the conditions of the outer mirror switch.</ptxt>
<ptxt>The outer mirror switch sends information about the operating condition of the mirror switch (switch input signals) through the CAN communication line. Then the switch input signals are sent to the outer mirror control ECU. Mirror adjustment is controlled by the outer mirror control ECU.</ptxt>
</content5>
</subpara>
<subpara id="RM000002Y4403PX_02" type-id="32" category="03" proc-id="RM22W0E___0000IOU00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B182636E32" width="7.106578999in" height="5.787629434in"/>
</figure>
<figure>
<graphic graphicname="B182636E33" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002Y4403PX_04" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002Y4403PX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002Y4403PX_03_0001" proc-id="RM22W0E___0000IOV00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally. </ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>CAN DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002Y4403PX_03_0002" fin="false">A</down>
<right ref="RM000002Y4403PX_03_0005" fin="true">B</right>
<right ref="RM000002Y4403PX_03_0017" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0002" proc-id="RM22W0E___0000IOW00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (OUTER MIRROR SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the Data List for proper functioning of the mirror master switch and mirror control switch (See page <xref label="Seep01" href="RM000002Y41012X"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Mirror Selection SW (L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mirror master switch signal for LH mirror / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Switch is in L position</ptxt>
<ptxt>OFF: Switch is off or in R position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mirror Selection SW (R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mirror master switch signal for RH mirror / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Switch is in R position</ptxt>
<ptxt>OFF: Switch is off or in L position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mirror Position SW (R)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mirror control switch signal (right) / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Right switch is on</ptxt>
<ptxt>OFF: Any switch except right is on or all switches are off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mirror Position SW (L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mirror control switch signal (left) / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Left switch is on</ptxt>
<ptxt>OFF: Any switch except left is on or all switches are off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mirror Position SW (Up)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mirror control switch signal (up) / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Up switch is on</ptxt>
<ptxt>OFF: Any switch except up is on or all switches are off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mirror Position SW (Dwn)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mirror control switch signal (down) / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Down switch is on</ptxt>
<ptxt>OFF: Any switch except down is on or all switches are off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On tester screen, each item changes between ON and OFF according to above chart. </ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002Y4403PX_03_0014" fin="false">OK</down>
<right ref="RM000002Y4403PX_03_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0014" proc-id="RM22W0E___0000IP000000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (POWER MIRROR CONTROL FUNCTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Select the Active Test, use the intelligent tester to generate a control command, and then check the power mirror control function (See page <xref label="Seep01" href="RM000002Y41012X"/>).</ptxt>
<table pgwide="1">
<title>Mirror</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>LH Mirror Up/Down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Mirror vertical operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>UP / OFF / DOWN</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>LH Mirror Right/Left</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Mirror horizontal operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>RIGHT / OFF / LEFT</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RH Mirror Up/Down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Mirror vertical operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>UP / OFF / DOWN</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RH Mirror Right/Left</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Mirror horizontal operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>RIGHT / OFF / LEFT</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Outer rear view mirror (for Driver Side) does not operate normally</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Outer rear view mirror (for Driver Side) operates normally</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002Y4403PX_03_0015" fin="false">A</down>
<right ref="RM000002Y4403PX_03_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0015" proc-id="RM22W0E___0000IP100000">
<testtitle>INSPECT OUTER REAR VIEW MIRROR ASSEMBLY (for Driver Side)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LHD:</ptxt>
<figure>
<graphic graphicname="B308845E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Remove the mirror LH (See page <xref label="Seep01" href="RM000000W1P03UX"/>).</ptxt>
</test2>
<test2>
<ptxt>Apply battery voltage and check the operation of the power mirror. </ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 7 (MV)</ptxt>
<ptxt>Battery negative (-) → Terminal 8 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Turns upward (A)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 7 (MV)</ptxt>
<ptxt>Battery positive (+) → Terminal 8 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Turns downward (B)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 9 (MH)</ptxt>
<ptxt>Battery negative (-) → Terminal 8 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Turns left (C)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 9 (MH)</ptxt>
<ptxt>Battery positive (+) → Terminal 8 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Turns right (D)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD:</ptxt>
<figure>
<graphic graphicname="B308844E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Remove the mirror RH (See page <xref label="Seep02" href="RM000000W1P03UX"/>).</ptxt>
</test2>
<test2>
<ptxt>Apply battery voltage and check the operation of the power mirror. </ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 7 (MV)</ptxt>
<ptxt>Battery negative (-) → Terminal 8 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Turns upward (A)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 7 (MV)</ptxt>
<ptxt>Battery positive (+) → Terminal 8 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Turns downward (B)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 9 (MH)</ptxt>
<ptxt>Battery negative (-) → Terminal 8 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Turns left (C)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 9 (MH)</ptxt>
<ptxt>Battery positive (+) → Terminal 8 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Turns right (D)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002Y4403PX_03_0008" fin="false">OK</down>
<right ref="RM000002Y4403PX_03_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0008" proc-id="RM22W0E___0000IOX00000">
<testtitle>CHECK HARNESS AND CONNECTOR (OUTER MIRROR CONTROL ECU - OUTER REAR VIEW MIRROR ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LHD:</ptxt>
<figure>
<graphic graphicname="B315166E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Disconnect the I18 ECU connector. </ptxt>
</test2>
<test2>
<ptxt>Disconnect the I35 mirror connector. </ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I18-18 (M+L) - I35-8 (M+)</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-19 (MHL) - I35-9 (MH)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-20 (MVL) - I35-7 (MV)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I35-8 (M+) - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I35-9 (MH) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I35-7 (MV) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD:</ptxt>
<figure>
<graphic graphicname="B315166E10" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Disconnect the I24 ECU connector. </ptxt>
</test2>
<test2>
<ptxt>Disconnect the I32 mirror connector. </ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.34in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I24-38 (M+R) - I32-8 (M+)</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-39 (MHR) - I32-9 (MH)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-40 (MVR) - I32-7 (MV)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I32-8 (M+) - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I32-9 (MH) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I32-7 (MV) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002Y4403PX_03_0004" fin="true">OK</down>
<right ref="RM000002Y4403PX_03_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0012" proc-id="RM22W0E___0000IOZ00000">
<testtitle>INSPECT OUTER MIRROR SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the switch (See page <xref label="Seep01" href="RM000000W1P03UX"/>).</ptxt>
<figure>
<graphic graphicname="B181045E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="4" valign="middle" align="center">
<ptxt>7 (M+) - 9 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Up switch pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>95 to 105 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Down switch pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>446 to 493 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Left switch pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>760 to 840 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Right switch pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>237 to 262 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No switches pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>8 (MSW) - 9 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Mirror master switch L</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>95 to 105 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mirror master switch R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mirror master switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002Y4403PX_03_0009" fin="false">OK</down>
<right ref="RM000002Y4403PX_03_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0009" proc-id="RM22W0E___0000IOY00000">
<testtitle>CHECK HARNESS AND CONNECTOR (OUTER MIRROR SWITCH - MAIN BODY ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E1 ECU connector. </ptxt>
<figure>
<graphic graphicname="B182634E01" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the G1 switch connector. </ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E1-2 (MIRE) - G1-9 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E1-11 (MIRB) - G1-7 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E1-12 (MIRS) - G1-8 (MSW)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G1-7 (M+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G1-9 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G1-8 (MSW) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002Y4403PX_03_0013" fin="true">OK</down>
<right ref="RM000002Y4403PX_03_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0013">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0007">
<testtitle>REPLACE OUTER REAR VIEW MIRROR ASSEMBLY (for Driver Side)<xref label="Seep01" href="RM000000W1P03UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0004">
<testtitle>REPLACE OUTER MIRROR CONTROL ECU<xref label="Seep01" href="RM0000039C400JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0010">
<testtitle>REPLACE OUTER MIRROR SWITCH ASSEMBLY<xref label="Seep01" href="RM000000W1L05LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002Y4403PX_03_0017">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>