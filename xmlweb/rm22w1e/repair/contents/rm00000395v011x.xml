<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3V0_T00O3" variety="T00O3">
<name>REAR SEAT CUSHION HEATER (for 60/40 Split Seat Type 40 Side)</name>
<para id="RM00000395V011X" category="A" type-id="80001" name-id="SE6SU-04" from="201308">
<name>REMOVAL</name>
<subpara id="RM00000395V011X_02" type-id="11" category="10" proc-id="RM22W0E___0000G8R00001">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000395V011X_01" type-id="01" category="01">
<s-1 id="RM00000395V011X_01_0001" proc-id="RM22W0E___0000G8P00001">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep01" href="RM00000391400RX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000395V011X_01_0018" proc-id="RM22W0E___0000FW400001">
<ptxt>REMOVE SEAT ADJUSTER BOLT COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181163" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0019" proc-id="RM22W0E___0000FW500001">
<ptxt>REMOVE RECLINING ADJUSTER RELEASE HANDLE RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181164" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw and release handle.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0020" proc-id="RM22W0E___0000FW600001">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181165E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws in the order shown in the illustration, and then remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0021" proc-id="RM22W0E___0000FW700001">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181166" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0022" proc-id="RM22W0E___0000FW800001">
<ptxt>REMOVE UPPER SEAT TRACK RAIL COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181168" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0023" proc-id="RM22W0E___0000FW900001">
<ptxt>REMOVE UPPER SEAT TRACK RAIL COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181169" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0024" proc-id="RM22W0E___0000FWB00001">
<ptxt>REMOVE REAR NO. 2 SEAT LEG SIDE COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 screws and detach the 3 claws.</ptxt>
<figure>
<graphic graphicname="B181170" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt that is securing the fold seat stopper band and remove the seat leg side cover together with the fold seat stopper band.</ptxt>
<figure>
<graphic graphicname="B181171" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0025" proc-id="RM22W0E___0000FWF00001">
<ptxt>REMOVE REAR NO. 2 SEAT PROTECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Detach the claw and remove the wire harness connector.</ptxt>
<figure>
<graphic graphicname="B182658" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Detach the claw and remove the wire harness connector.</ptxt>
</s2>
<s2>
<ptxt>Using a clip remover, remove the clip.</ptxt>
<figure>
<graphic graphicname="B184060" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and remove the seat protector from the seat hinge.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws, open the protector and remove the wire harness.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0026" proc-id="RM22W0E___0000FWG00001">
<ptxt>REMOVE REAR NO. 1 SEAT HINGE SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181174" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 nuts and hinge.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0027" proc-id="RM22W0E___0000FWH00001">
<ptxt>REMOVE REAR NO. 1 SEAT HINGE SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181175" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 nuts and hinge.</ptxt>
</s2>
<s2>
<ptxt>Detach the cable clamp and disconnect the cable.</ptxt>
<figure>
<graphic graphicname="B181176" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0028" proc-id="RM22W0E___0000FWI00001">
<ptxt>REMOVE REAR SEAT COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 12 claws.</ptxt>
<figure>
<graphic graphicname="B181177" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B181179" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0029" proc-id="RM22W0E___0000FWJ00001">
<ptxt>REMOVE REAR UNDER SIDE COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181178" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 5 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the clip and 2 claws, and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0030" proc-id="RM22W0E___0000FWL00001">
<ptxt>REMOVE REAR SEAT UNDER TRAY COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181181" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0031" proc-id="RM22W0E___0000FWN00001">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<s3>
<ptxt>Detach the side airbag wire harness clamps.</ptxt>
</s3>
<s3>
<ptxt>Remove the side airbag wire harness through the hole in the seat cushion.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<s3>
<ptxt>Disconnect the 2 seat heater connectors.</ptxt>
<figure>
<graphic graphicname="B184061" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the seatback heater wire harness through the hole in the seat cushion.</ptxt>
<figure>
<graphic graphicname="B184058" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Remove the seat cushion cover with pad.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0032" proc-id="RM22W0E___0000FWO00001">
<ptxt>REMOVE REAR SEPARATE TYPE SEAT CUSHION COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 pieces of fastening tape.</ptxt>
<figure>
<graphic graphicname="B181184E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the hog rings and cover.</ptxt>
<figure>
<graphic graphicname="B181186" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395V011X_01_0017" proc-id="RM22W0E___0000G8Q00001">
<ptxt>REMOVE REAR SEAT CUSHION HEATER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181187" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Cut off the tack pins which fasten the seat heater to the seat cushion cover, and then remove the seat cushion heater from the seat cushion cover.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>