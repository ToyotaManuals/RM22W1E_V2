<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UI_T00NL" variety="T00NL">
<name>FRONT POWER SEAT CONTROL SYSTEM (w/ Seat Position Memory System)</name>
<para id="RM000002RB102YX" category="J" type-id="300RM" name-id="SE6TL-02" from="201301">
<dtccode/>
<dtcname>Front Power Seat does not Operate with Front Power Seat Switch</dtcname>
<subpara id="RM000002RB102YX_01" type-id="60" category="03" proc-id="RM22W0E___0000FRN00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When a signal is input into the position control ECU, the ECU manages the signals received from the front power seat switch LH, and operates each motor. When 2 or more signals are input, the motors only operate when the signals are from the slide switch and reclining switch.</ptxt>
</content5>
</subpara>
<subpara id="RM000002RB102YX_02" type-id="32" category="03" proc-id="RM22W0E___0000FRO00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B183729E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002RB102YX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002RB102YX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002RB102YX_04_0001" proc-id="RM22W0E___0000FRP00000">
<testtitle>INSPECT FUSE (FR P/SEAT LH, FR P/SEAT RH, ECU-B2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the FR P/SEAT LH*1, FR P/SEAT RH*2 and ECU-B2 fuses from the main body ECU.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR P/SEAT LH fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ECU-B2 fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR P/SEAT RH fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ECU-B2 fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RB102YX_04_0002" fin="false">OK</down>
<right ref="RM000002RB102YX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RB102YX_04_0002" proc-id="RM22W0E___0000FRQ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (POSITION CONTROL ECU AND SWITCH - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B128942E04" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Disconnect the c14*1, c15*1 or c26*2, c27*2 switch connectors.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>c14-5 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c15-8 (SYSB) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>c26-5 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c27-8 (SYSB) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>c14-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>c26-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RB102YX_04_0003" fin="true">OK</down>
<right ref="RM000002RB102YX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RB102YX_04_0003">
<testtitle>REPLACE POSITION CONTROL ECU AND SWITCH<xref label="Seep01" href="RM00000390T01LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002RB102YX_04_0004">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000002RB102YX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>