<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000001FQP0I7X" category="J" type-id="801AH" name-id="BCE7E-03" from="201308">
<dtccode/>
<dtcname>VSC OFF Indicator Light does not Come ON</dtcname>
<subpara id="RM000001FQP0I7X_01" type-id="60" category="03" proc-id="RM22W0E___0000ADS00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to VSC OFF Indicator Light Remains ON (See page <xref label="Seep01" href="RM000001FQO0IBX"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000001FQP0I7X_02" type-id="32" category="03" proc-id="RM22W0E___0000ADT00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to VSC OFF Indicator Light Remains ON (See page <xref label="Seep01" href="RM000001FQO0IBX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000001FQP0I7X_03" type-id="51" category="05" proc-id="RM22W0E___0000ADU00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001FQP0I7X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001FQP0I7X_05_0020" proc-id="RM22W0E___0000ABS00001">
<testtitle>CHECK CAN COMMUNICATION LINE
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Select "CAN Bus Check" from the System Selection Menu screen, and follow the prompts on the screen to inspect the CAN Bus.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>"CAN Bus Check" indicates no malfunctions in CAN communication.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001FQP0I7X_05_0021" fin="false">A</down>
<right ref="RM000001FQP0I7X_05_0011" fin="true">B</right>
<right ref="RM000001FQP0I7X_05_0018" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0021" proc-id="RM22W0E___0000ABT00001">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (for LHD: See page <xref label="Seep01" href="RM000001RSW03ZX"/>, for RHD: See page <xref label="Seep02" href="RM000001RSW040X"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>CAN system DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN system DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN system DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001FQP0I7X_05_0022" fin="false">A</down>
<right ref="RM000001FQP0I7X_05_0011" fin="true">B</right>
<right ref="RM000001FQP0I7X_05_0018" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0022" proc-id="RM22W0E___0000ADP00001">
<testtitle>READ VALUE USING GTS (TRC/VSC OFF MODE)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="2.26in"/>
<colspec colname="COL3" colwidth="1.60in"/>
<colspec colname="COL4" colwidth="1.45in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>TRC/VSC Off Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>TRC/VSC off mode/ Normal, TRC OFF, Unknown or VSC OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal: Normal mode</ptxt>
<ptxt>TRC OFF: TRC OFF mode</ptxt>
<ptxt>VSC OFF: VSC OFF mode</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that the mode display changes according to VSC OFF switch operation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Display changes according to switch operation.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000001FQP0I7X_05_0007" fin="true">OK</down>
<right ref="RM000001FQP0I7X_05_0023" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0023" proc-id="RM22W0E___0000ADQ00001">
<testtitle>INSPECT VSC OFF SWITCH
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the VSC OFF switch (See page <xref label="Seep01" href="RM000001WZ2044X"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the VSC OFF switch (See page <xref label="Seep02" href="RM000001WZ003GX"/>).</ptxt>
</test1>
</content6><res>
<down ref="RM000001FQP0I7X_05_0024" fin="false">OK</down>
<right ref="RM000001FQP0I7X_05_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0024" proc-id="RM22W0E___0000ADR00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU - VSC OFF SWITCH)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G13 VSC OFF switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.61in"/>
<colspec colname="COLSPEC1" colwidth="2.13in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A24-9 (CSW) - G13-6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A24-9 (CSW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G13-3 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001FQP0I7X_05_0016" fin="true">A</down>
<right ref="RM000001FQP0I7X_05_0005" fin="true">B</right>
<right ref="RM000001FQP0I7X_05_0019" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0011">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0007">
<testtitle>GO TO METER / GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L048X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0005">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0015">
<testtitle>REPLACE VSC OFF SWITCH<xref label="Seep01" href="RM000001WZ2044X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0018">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001FQP0I7X_05_0019">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>