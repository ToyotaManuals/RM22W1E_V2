<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000187K053X" category="C" type-id="302H4" name-id="ESRAB-03" from="201301" to="201308">
<dtccode>P0101</dtccode>
<dtcname>Mass or Volume Air Flow Circuit Range / Performance Problem</dtcname>
<subpara id="RM00000187K053X_01" type-id="60" category="03" proc-id="RM22W0E___00002L300000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0100 (See page <xref label="Seep01" href="RM00000187J06WX"/>).</ptxt>
<table pgwide="1">
<title>P0101</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1">
<ptxt>After warming up engine, idle engine for 20 seconds, then maintain engine at 3000 rpm for 20 seconds</ptxt>
</entry>
<entry>
<ptxt>After the engine is warmed up, condition (a) continues for more than 10 seconds when idling (2 trip detection logic):</ptxt>
<ptxt>(a) Mass air flow meter output is more than 4.4 V.</ptxt>
</entry>
<entry morerows="1">
<ptxt>Mass air flow meter</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Conditions (a) and (b) continue for more than 10 seconds with the engine speed at 1250 rpm or more (2 trip detection logic):</ptxt>
<ptxt>(a) VTA is more than 0.1 V.</ptxt>
<ptxt>(b) Mass air flow meter output is less than 0.7 V.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>DTC No.</ptxt>
</entry>
<entry>
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P0101</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>MAF</ptxt>
</item>
<item>
<ptxt>Actual EGR Valve Pos.</ptxt>
</item>
<item>
<ptxt>Actual EGR Valve Pos. #2</ptxt>
</item>
<item>
<ptxt>Actual Throttle position</ptxt>
</item>
<item>
<ptxt>Actual Throttle position #2</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTC P0101 is stored, either of the following symptoms may appear:</ptxt>
<list1 type="ordered">
<item>
<ptxt>As the ECU mistakenly determines that there is less air than the actual intake air amount, EGR is decreased to match the target EGR.</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Combustion noise worsens</ptxt>
</item>
</list2>
<item>
<ptxt>As the ECU mistakenly determines that there is more air than the actual intake air amount, EGR is increased to match the target EGR):</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Misfire</ptxt>
</item>
<item>
<ptxt>White smoke</ptxt>
</item>
<item>
<ptxt>Black smoke</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187K053X_02" type-id="51" category="05" proc-id="RM22W0E___00002L400000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187K053X_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187K053X_03_0001" proc-id="RM22W0E___00002L500000">
<testtitle>CHECK OTHER DTC OUTPUT (IN ADDITION TO DTC P0101)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0101 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0101 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If codes other than P0101 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000187K053X_03_0006" fin="false">B</down>
<right ref="RM00000187K053X_03_0002" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM00000187K053X_03_0006" proc-id="RM22W0E___00002L800000">
<testtitle>CHECK AIR INDUCTION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the air induction system for vacuum leakage (See page <xref label="Seep01" href="RM0000031FH005X_01_0005"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No leakage from air induction system.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187K053X_03_0003" fin="false">OK</down>
<right ref="RM00000187K053X_03_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187K053X_03_0003" proc-id="RM22W0E___00002L600000">
<testtitle>REPLACE MASS AIR FLOW METER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the mass air flow meter (See page <xref label="Seep01" href="RM000002PPQ01EX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187K053X_03_0004" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187K053X_03_0007">
<testtitle>REPAIR OR REPLACE AIR INDUCTION SYSTEM</testtitle>
<res>
<down ref="RM00000187K053X_03_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187K053X_03_0004" proc-id="RM22W0E___00002L700000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>After warming up the engine, idle the engine for 20 seconds, then maintain the engine at 3000 rpm for 20 seconds.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the tester to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Powertrain / Engine / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P0101.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or UNKNOWN, maintain the engine at 3000 rpm for 30 seconds and then idle the engine for 5 minutes.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000187K053X_03_0005" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187K053X_03_0002">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW05BX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000187K053X_03_0005">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>