<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000B" variety="S000B">
<name>1UR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000B_7C3F5_T0088" variety="T0088">
<name>REAR CRANKSHAFT OIL SEAL</name>
<para id="RM000002BRI02OX" category="A" type-id="30014" name-id="EMAQH-05" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000002BRI02OX_01" type-id="01" category="01">
<s-1 id="RM000002BRI02OX_01_0001" proc-id="RM22W0E___00004GP00001">
<ptxt>INSTALL REAR CRANKSHAFT OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply MP grease to the lip of a new rear crankshaft oil seal.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not allow foreign matter to contact the lip of the oil seal.</ptxt>
</item>
<item>
<ptxt>Do not allow MP grease to contact the dust seal.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Using SST and a hammer, tap in the rear crankshaft oil seal until its surface is flush with the rear oil seal retainer edge.</ptxt>
<sst>
<sstitem>
<s-number>09223-15030</s-number>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="A149490E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Keep the lip free from foreign matter.</ptxt>
</item>
<item>
<ptxt>Do not tap the rear crankshaft oil seal at an angle.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BRI02OX_01_0005" proc-id="RM22W0E___00004GR00001">
<ptxt>INSTALL DRIVE PLATE AND RING GEAR SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, hold the crankshaft.</ptxt>
<sst>
<sstitem>
<s-number>09213-70011</s-number>
<s-subnumber>09213-70020</s-subnumber>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Install the crankshaft angle sensor rotor.</ptxt>
<figure>
<graphic graphicname="A228418" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Engine Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Align the pin hole of the crankshaft angle sensor rotor with the pin of the crankshaft.</ptxt>
</item>
<item>
<ptxt>As the crankshaft angle sensor rotor is not reversible, be sure to install it so that it is facing in the direction shown in the illustration.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Install the drive plate and ring gear and the rear drive plate spacer to the crankshaft.</ptxt>
<figure>
<graphic graphicname="A228419E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Drive Plate and Ring Gear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Drive Plate Spacer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Transmission Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>As the rear drive plate spacer and the drive plate and ring gear are not reversible, be sure to install them so that they are facing in the directions shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the drive plate and ring gear and bolts.</ptxt>
<atten4>
<ptxt>The bolts are tightened in 2 progressive steps.</ptxt>
</atten4>
<s3>
<ptxt>Clean the bolts and bolt holes.</ptxt>
</s3>
<s3>
<ptxt>Apply adhesive to 2 or 3 threads at the end of each of the 10 bolts.</ptxt>
<figure>
<graphic graphicname="A192239E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1324, Three Bond 1324 or equivalent</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adhesive</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Step 1:</ptxt>
<ptxt>Uniformly install and tighten the 10 new bolts in several steps in the sequence shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>301</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A228359E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Mark the top of each drive plate installation bolt with paint.</ptxt>
</s3>
<s3>
<ptxt>Step 2:</ptxt>
<ptxt>Tighten the drive plate installation bolts 90°.</ptxt>
</s3>
<s3>
<ptxt>Check that the painted marks are now at a 90° angle to the top.</ptxt>
<atten3>
<ptxt>Do not start the engine for at least an hour after installing the drive plate.</ptxt>
</atten3>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000002BRI02OX_01_0003" proc-id="RM22W0E___00004GQ00001">
<ptxt>INSTALL AUTOMATIC TRANSMISSION ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000018ZB04VX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>