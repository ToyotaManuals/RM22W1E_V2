<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000000XIE0R5X" category="C" type-id="803OO" name-id="BCGHK-01" from="201308">
<dtccode>C1425</dtccode>
<dtcname>Open in Stop Light Switch Circuit</dtcname>
<subpara id="RM000000XIE0R5X_01" type-id="60" category="03" proc-id="RM22W0E___0000AIA00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU (master cylinder solenoid) has an open detection circuit, which stores this DTC when detecting an open in the stop light switch input line or the ground line of the stop light switch circuit with the stop light switch off (brake pedal not depressed).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.00in"/>
<colspec colname="COL2" colwidth="3.14in"/>
<colspec colname="COL3" colwidth="2.94in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1425</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the IG1 terminal voltage is between 9.5 and 17.4 V and the skid control ECU STPO terminal output is off, an open in the stop light switch circuit continues for 3 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Stop light switch circuit</ptxt>
</item>
<item>
<ptxt>Stop light control relay assembly</ptxt>
</item>
<item>
<ptxt>Stop light control relay circuit</ptxt>
</item>
<item>
<ptxt>Skid control ECU (Master cylinder solenoid)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XIE0R5X_02" type-id="32" category="03" proc-id="RM22W0E___0000AIB00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C246896E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XIE0R5X_03" type-id="51" category="05" proc-id="RM22W0E___0000AIC00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XIE0R5X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XIE0R5X_04_0054" proc-id="RM22W0E___0000AH900001">
<testtitle>READ VALUE USING GTS (STOP LIGHT SW)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.64in"/>
<colspec colname="COL2" colwidth="1.93in"/>
<colspec colname="COL3" colwidth="2.42in"/>
<colspec colname="COLSPEC1" colwidth="1.09in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Brake pedal depressed</ptxt>
<ptxt>OFF: Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that the stop light switch condition observed on the GTS changes according to brake pedal operation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The GTS displays ON and OFF according to brake pedal operation.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000XIE0R5X_04_0007" fin="false">OK</down>
<right ref="RM000000XIE0R5X_04_0055" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0007" proc-id="RM22W0E___0000AID00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal several times to test the stop light circuit.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIE0R5X_04_0025" fin="true">A</down>
<right ref="RM000000XIE0R5X_04_0014" fin="true">B</right>
<right ref="RM000000XIE0R5X_04_0058" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0055" proc-id="RM22W0E___0000AIG00001">
<testtitle>CHECK HARNESS AND CONNECTOR (STP TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214161E71" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.44in"/>
<colspec colname="COL2" colwidth="1.50in"/>
<colspec colname="COL3" colwidth="1.19in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>A24-7 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU [Master Cylinder Solenoid])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIE0R5X_04_0056" fin="false">OK</down>
<right ref="RM000000XIE0R5X_04_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0056" proc-id="RM22W0E___0000AH400001">
<testtitle>CHECK HARNESS AND CONNECTOR (+B/GND TERMINAL)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the stop light control relay assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C206543E13" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.71in"/>
<colspec colname="COL2" colwidth="1.05in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A39-6 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.71in"/>
<colspec colname="COL2" colwidth="1.05in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A39-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Stop Light Control Relay Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000000XIE0R5X_04_0057" fin="false">OK</down>
<right ref="RM000000XIE0R5X_04_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0057" proc-id="RM22W0E___0000AH500001">
<testtitle>CHECK HARNESS AND CONNECTOR (STP TERMINAL)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the stop light control relay assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C206543E14" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.52in"/>
<colspec colname="COL3" colwidth="1.23in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>A39-2 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Stop Light Control Relay Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000000XIE0R5X_04_0051" fin="false">OK</down>
<right ref="RM000000XIE0R5X_04_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0051" proc-id="RM22W0E___0000AIF00001">
<testtitle>CHECK HARNESS AND CONNECTOR (OUT TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the stop light control relay assembly connector. </ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E205929E05" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.44in"/>
<colspec colname="COL2" colwidth="1.50in"/>
<colspec colname="COL3" colwidth="1.19in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>A39-8 (OUT) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Stop Light Control Relay Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIE0R5X_04_0036" fin="false">OK</down>
<right ref="RM000000XIE0R5X_04_0049" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0036" proc-id="RM22W0E___0000AIE00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal several times to test the stop light circuit.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIE0R5X_04_0025" fin="true">A</down>
<right ref="RM000000XIE0R5X_04_0014" fin="true">B</right>
<right ref="RM000000XIE0R5X_04_0058" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0014">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0025">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0018">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0049">
<testtitle>REPLACE STOP LIGHT CONTROL RELAY ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000000XIE0R5X_04_0058">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>