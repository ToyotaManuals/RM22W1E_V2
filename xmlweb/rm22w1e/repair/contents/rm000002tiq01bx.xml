<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12054_S0027" variety="S0027">
<name>METER / GAUGE / DISPLAY</name>
<ttl id="12054_S0027_7C3TU_T00MX" variety="T00MX">
<name>METER / GAUGE SYSTEM</name>
<para id="RM000002TIQ01BX" category="J" type-id="801NB" name-id="ME46P-02" from="201301">
<dtccode/>
<dtcname>Meter Illumination is Always Dark</dtcname>
<subpara id="RM000002TIQ01BX_01" type-id="60" category="03" proc-id="RM22W0E___0000F5700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>w/ Multi-information Display:</ptxt>
<ptxt>In this circuit, the combination meter assembly receives auto dimmer signals from the main body ECU using the CAN communication lines. When the combination meter assembly receives an auto dimmer signal, it dims the meter illumination. The main body ECU determines whether it is daytime, twilight or nighttime based on the waveform transmitted from the automatic light control sensor. If the main body ECU determines that it is daytime, the ECU does not send auto dimmer signals. Therefore, the meter illumination (warning and indicator lights) will not dim even if the driver accidentally turns the light control switch to the TAIL or HEAD position in the daytime.</ptxt>
<atten4>
<ptxt>When the meter illumination does not dim at night, the light control rheostat position is in the TAIL cancel position, or there may be a malfunction in the automatic light control sensor, main body ECU, CAN communication system, wire harness or connector, or combination meter.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002TIQ01BX_02" type-id="32" category="03" proc-id="RM22W0E___0000F5800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E156904E02" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002TIQ01BX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002TIQ01BX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002TIQ01BX_05_0001" proc-id="RM22W0E___0000F5900000">
<testtitle>CHECK CAN COMMUNICATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM00000302X011X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system (for LHD) DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system (for RHD) DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Lighting system DTC (B1244) is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002TIQ01BX_05_0002" fin="false">A</down>
<right ref="RM000002TIQ01BX_05_0005" fin="true">B</right>
<right ref="RM000002TIQ01BX_05_0010" fin="true">C</right>
<right ref="RM000002TIQ01BX_05_0006" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000002TIQ01BX_05_0002" proc-id="RM22W0E___0000F5A00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (DIMMER SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Operate the intelligent tester according to the display and select Active Test.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Dimmer Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illumination dimming operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Meter illumination is dimmed when the dimmer switch is ON.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Refer to the sensitivity setting in the customization table of the automatic light control system (See page <xref label="Seep01" href="RM000002X0L038X"/>).</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002TIQ01BX_05_0007" fin="true">OK</down>
<right ref="RM000002TIQ01BX_05_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002TIQ01BX_05_0003" proc-id="RM22W0E___0000F5B00000">
<testtitle>REPLACE COMBINATION METER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the combination meter with a new one (See page <xref label="Seep01" href="RM0000038ID00IX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002TIQ01BX_05_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002TIQ01BX_05_0009" proc-id="RM22W0E___0000F5C00000">
<testtitle>CHECK COMBINATION METER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the operation of the combination meter returns to normal (See page <xref label="Seep01" href="RM0000038ID00IX"/>). </ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Operation of combination meter returns to normal.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002TIQ01BX_05_0004" fin="true">OK</down>
<right ref="RM000002TIQ01BX_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002TIQ01BX_05_0004">
<testtitle>END (COMBINATION METER IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002TIQ01BX_05_0005">
<testtitle>Go to CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002TIQ01BX_05_0006">
<testtitle>Go to LIGHTING SYSTEM<xref label="Seep01" href="RM000002WKK02SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002TIQ01BX_05_0007">
<testtitle>CHECK LIGHTING SETTING<xref label="Seep01" href="RM000002WKK02SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002TIQ01BX_05_0008">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000002TIQ01BX_05_0010">
<testtitle>Go to CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>