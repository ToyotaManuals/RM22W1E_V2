<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZC_T00SF" variety="T00SF">
<name>LIGHTING SYSTEM</name>
<para id="RM000001PPW00EX" category="J" type-id="801K1" name-id="LE4SH-01" from="201301">
<dtccode/>
<dtcname>License Plate Light Circuit</dtcname>
<subpara id="RM000001PPW00EX_01" type-id="60" category="03" proc-id="RM22W0E___0000J8J00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the light switch TAIL signal is received from the cowl side junction block ECU, the license plate light illuminates.</ptxt>
</content5>
</subpara>
<subpara id="RM000001PPW00EX_02" type-id="32" category="03" proc-id="RM22W0E___0000J8K00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E234119E02" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001PPW00EX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001PPW00EX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001PPW00EX_05_0007" proc-id="RM22W0E___0000J8M00000">
<testtitle>INSPECT FUSE (TAIL FUSE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the TAIL fuse from the main body ECU.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>TAIL fuse</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001PPW00EX_05_0013" fin="false">OK</down>
<right ref="RM000001PPW00EX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001PPW00EX_05_0013" proc-id="RM22W0E___0000J8N00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the 2A ECU connector.</ptxt>
</test1>
<figure>
<graphic graphicname="E157348E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2A-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001PPW00EX_05_0015" fin="false">OK</down>
<right ref="RM000001PPW00EX_05_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001PPW00EX_05_0015" proc-id="RM22W0E___0000J8O00000">
<testtitle>CHECK VEHICLE TYPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the vehicle type.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>for Double Swing Out Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>except Double Swing Out Type, w/ Tire Carrier</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>except Double Swing Out Type, w/o Tire Carrier</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001PPW00EX_05_0003" fin="false">A</down>
<right ref="RM000001PPW00EX_05_0016" fin="false">B</right>
<right ref="RM000001PPW00EX_05_0017" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000001PPW00EX_05_0003" proc-id="RM22W0E___0000J8L00000">
<testtitle>CHECK HARNESS AND CONNECTOR (LICENSE PLATE LIGHT - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E234120E03" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the 2C ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the W2 light connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2C-22 - W2-1 (B)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-2 (E) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-1 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001PPW00EX_05_0010" fin="true">OK</down>
<right ref="RM000001PPW00EX_05_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001PPW00EX_05_0016" proc-id="RM22W0E___0000J8P00000">
<testtitle>CHECK HARNESS AND CONNECTOR (LICENSE PLATE LIGHT - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E234120E04" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the 2C ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the h2 light connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2C-22 - h2-1</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>h2-2 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>h2-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001PPW00EX_05_0010" fin="true">OK</down>
<right ref="RM000001PPW00EX_05_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001PPW00EX_05_0017" proc-id="RM22W0E___0000J8Q00000">
<testtitle>CHECK HARNESS AND CONNECTOR (LICENSE PLATE LIGHT - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E157464E04" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the 2C ECU connector.</ptxt>
</test1>
<test1>
<ptxt>for LH:</ptxt>
<test2>
<ptxt>Disconnect the S3 light connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2C-22 - S3-1</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S3-2 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S3-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RH:</ptxt>
<test2>
<ptxt>Disconnect the S2 light connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2C-22 - S2-1</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S2-2 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S2-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000001PPW00EX_05_0010" fin="true">OK</down>
<right ref="RM000001PPW00EX_05_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001PPW00EX_05_0009">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000001PPW00EX_05_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001PPW00EX_05_0010">
<testtitle>REPLACE MAIN BODY ECU</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>