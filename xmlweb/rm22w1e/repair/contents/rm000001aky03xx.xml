<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001Q" variety="S001Q">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001Q_7C3Q6_T00J9" variety="T00J9">
<name>POWER STEERING SYSTEM (for 1VD-FTV with DPF)</name>
<para id="RM000001AKY03XX" category="J" type-id="804U7" name-id="PA1GC-01" from="201301" to="201308">
<dtccode/>
<dtcname>Power Steering Warning Light Circuit</dtcname>
<subpara id="RM000001AKY03XX_01" type-id="60" category="03" proc-id="RM22W0E___0000B6K00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The power steering ECU is connected to the combination meter via CAN communication. If the power steering ECU detects a malfunction, the power steering warning light comes on. At this time, the vehicle enters fail-safe mode.</ptxt>
</content5>
</subpara>
<subpara id="RM000001AKY03XX_02" type-id="32" category="03" proc-id="RM22W0E___0000B6L00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C216948E07" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001AKY03XX_03" type-id="51" category="05" proc-id="RM22W0E___0000B6M00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001AKY03XX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001AKY03XX_05_0014" proc-id="RM22W0E___0000B6O00000">
<testtitle>INSPECT CAN COMMUNICATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs of the CAN communication system.</ptxt>
<ptxt>for LHD</ptxt>
<ptxt>Refer to the following procedures (See page <xref label="Seep02" href="RM000001RSW03JX"/>).</ptxt>
<ptxt>for RHD</ptxt>
<ptxt>Refer to the following procedures (See page <xref label="Seep03" href="RM000001RSW03KX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.84in"/>
<colspec colname="COL1" colwidth="2.84in"/>
<colspec colname="COL2" colwidth="1.4in"/>
<thead>
<row>
<entry namest="COLSPEC0" nameend="COL1" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COLSPEC0" nameend="COL1" valign="middle">
<ptxt>DTC is not output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>DTC is output.</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001AKY03XX_05_0003" fin="false">A</down>
<right ref="RM000001AKY03XX_05_0006" fin="true">B</right>
<right ref="RM000001AKY03XX_05_0015" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001AKY03XX_05_0003" proc-id="RM22W0E___0000B6100000">
<testtitle>CHECK HARNESS AND CONNECTOR (POWER STEERING ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the power steering ECU connector.</ptxt>
<figure>
<graphic graphicname="C214365E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E102-5 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E102-3 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="left" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Power Steering ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001AKY03XX_05_0005" fin="false">OK</down>
<right ref="RM000001AKY03XX_05_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001AKY03XX_05_0005" proc-id="RM22W0E___0000B6N00000">
<testtitle>PERFORM ACTIVE TEST USING GTS (METER/GAUGE SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Perform the Active Test of the combination meter using GTS (See page <xref label="Seep01" href="RM000002Z4S01AX"/>).</ptxt>
<table pgwide="1">
<title>Combination Meter</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.28in"/>
<colspec colname="COL4" colwidth="2.26in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>PPS Indicator</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power steering warning light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perform the test with the vehicle stopped and the engine idling.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that the power steering warning light operates in accordance with the Active Test.</ptxt>
<atten4>
<ptxt>Reconnect the connectors and restore the vehicle to its previous condition before checking the combination meter.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The power steering warning light turns on and off in accordance with GTS operation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001AKY03XX_05_0004" fin="true">OK</down>
<right ref="RM000001AKY03XX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001AKY03XX_05_0006">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001AKY03XX_05_0015">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001AKY03XX_05_0012">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001AKY03XX_05_0009">
<testtitle>INSPECT METER/GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L03NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001AKY03XX_05_0004">
<testtitle>REPLACE POWER STEERING ECU ASSEMBLY<xref label="Seep01" href="RM000003FC000WX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>