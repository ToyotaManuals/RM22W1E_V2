<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12067_S002J" variety="S002J">
<name>MIRROR (EXT)</name>
<ttl id="12067_S002J_7C3Y6_T00R9" variety="T00R9">
<name>POWER MIRROR CONTROL SYSTEM (w/ Retract Mirror)</name>
<para id="RM000002Y4604EX" category="J" type-id="3057C" name-id="MX3VK-01" from="201308">
<dtccode/>
<dtcname>Power Retractable Mirrors do not Operate with Power Retract Mirror Switch</dtcname>
<subpara id="RM000002Y4604EX_01" type-id="61" category="03" proc-id="RM22W0E___0000IPF00001">
<name>SYSTEM DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit detects the conditions of the mirror retract switch.</ptxt>
<ptxt>The outer mirror switch sends information about the operating condition of the mirror retract switch (switch input signals) through the CAN communication line. Then the switch input signals are sent to the outer mirror control ECU.</ptxt>
</content5>
</subpara>
<subpara id="RM000002Y4604EX_02" type-id="32" category="03" proc-id="RM22W0E___0000IPG00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B188307E05" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002Y4604EX_04" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002Y4604EX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002Y4604EX_03_0001" proc-id="RM22W0E___0000IPH00001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>CAN DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002Y4604EX_03_0002" fin="false">A</down>
<right ref="RM000002Y4604EX_03_0008" fin="true">B</right>
<right ref="RM000002Y4604EX_03_0023" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0002" proc-id="RM22W0E___0000IPI00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (MIRROR RETRACT SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the Data List for proper functioning of the mirror retract switch (See page <xref label="Seep01" href="RM000002Y41012X"/>). </ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Outer Mirror Fold SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mirror retract switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Switch is on (retracting)</ptxt>
<ptxt>OFF: Switch is off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On tester screen, each item changes between ON and OFF according to above chart. </ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002Y4604EX_03_0019" fin="false">OK</down>
<right ref="RM000002Y4604EX_03_0014" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0019" proc-id="RM22W0E___0000IPP00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (MIRROR RETRACT FUNCTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Select the Active Test, use the intelligent tester to generate a control command, and then check the power mirror control function (See page <xref label="Seep01" href="RM000002Y41012X"/>).</ptxt>
<table pgwide="1">
<title>Mirror</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Mirror Fold</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Mirror retract operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>FOLD / OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Mirror Return</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Mirror retract operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>RETURN / OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Outer rear view mirror operates normally</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Outer rear view mirror LH does not operate normally</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Outer rear view mirror RH does not operate normally</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002Y4604EX_03_0021" fin="true">A</down>
<right ref="RM000002Y4604EX_03_0003" fin="false">B</right>
<right ref="RM000002Y4604EX_03_0011" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0014" proc-id="RM22W0E___0000IPO00001">
<testtitle>INSPECT RETRACTABLE OUTER MIRROR SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the switch (See page <xref label="Seep01" href="RM000000W1L06DX"/>).</ptxt>
<figure>
<graphic graphicname="B315167E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>5 (E) - 6 (MR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed (Retracts)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002Y4604EX_03_0006" fin="false">OK</down>
<right ref="RM000002Y4604EX_03_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0006" proc-id="RM22W0E___0000IPL00001">
<testtitle>CHECK HARNESS AND CONNECTOR (OUTER MIRROR SWITCH - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E2 ECU connector. </ptxt>
<figure>
<graphic graphicname="B315168E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the G16 switch connector. </ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E2-6 (RET) - G16-6 (MR)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G16-5 (E) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G16-6 (MR) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002Y4604EX_03_0007" fin="true">OK</down>
<right ref="RM000002Y4604EX_03_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0003" proc-id="RM22W0E___0000IPJ00001">
<testtitle>INSPECT OUTER MIRROR RETRACTOR LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the mirror LH (See page <xref label="Seep01" href="RM000000W1P04HX"/>).</ptxt>
<figure>
<graphic graphicname="B315169E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>For each mirror position: Disconnect the battery, set the mirror position by hand, connect the battery, and check the retractable mirror movement. </ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Disconnect and reconnect the battery once between each mirror position check.</ptxt>
</item>
<item>
<ptxt>The mirror position cannot be changed manually when the battery is connected. To change the mirror position manually, the battery must be disconnected first.</ptxt>
</item>
</list1>
</atten3>
<spec>
<title>OK</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Mirror Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 19 (MR)</ptxt>
<ptxt>Battery negative (-) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward position (A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (A) to retracted position (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 19 (MR)</ptxt>
<ptxt>Battery positive (+) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward position (A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Does not move</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 19 (MR)</ptxt>
<ptxt>Battery negative (-) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position between forward position (A) and driving position (C)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (B) to retracted position (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 19 (MR)</ptxt>
<ptxt>Battery positive (+) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position between forward position (A) and driving position (C)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (B) to forward position (A)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 19 (MR)</ptxt>
<ptxt>Battery negative (-) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driving position (C)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (C) to retracted position (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 19 (MR)</ptxt>
<ptxt>Battery positive (+) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driving position (C)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Does not move</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 19 (MR)</ptxt>
<ptxt>Battery negative (-) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position between driving position (C) and retracted position (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (D) to retracted position (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 19 (MR)</ptxt>
<ptxt>Battery positive (+) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position between driving position (C) and retracted position (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (D) to driving position (C)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 19 (MR)</ptxt>
<ptxt>Battery negative (-) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retracted position (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Does not move</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 19 (MR)</ptxt>
<ptxt>Battery positive (+) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retracted position (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (E) to driving position (C)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002Y4604EX_03_0004" fin="false">OK</down>
<right ref="RM000002Y4604EX_03_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0004" proc-id="RM22W0E___0000IPK00001">
<testtitle>CHECK HARNESS AND CONNECTOR (OUTER REAR VIEW MIRROR LH - OUTER MIRROR CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LHD:</ptxt>
<figure>
<graphic graphicname="B315170E05" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Disconnect the I17 ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the I35 mirror connector. </ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I17-1 (RL) - I35-19 (MR)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I17-2 (FL) - I35-20 (MF)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I35-19 (MR) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I35-20 (MF) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD:</ptxt>
<test2>
<ptxt>Disconnect the I23 ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the I35 mirror connector. </ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I23-1 (RL) - I35-19 (MR)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I23-2 (FL) - I35-20 (MF)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I35-19 (MR) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I35-20 (MF) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002Y4604EX_03_0021" fin="true">OK</down>
<right ref="RM000002Y4604EX_03_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0011" proc-id="RM22W0E___0000IPM00001">
<testtitle>INSPECT OUTER MIRROR RETRACTOR RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the mirror RH (See page <xref label="Seep01" href="RM000000W1P04HX"/>).</ptxt>
<figure>
<graphic graphicname="B315171E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>For each mirror position: Disconnect the battery, set the mirror position by hand, connect the battery, and check the retractable mirror movement. </ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Disconnect and reconnect the battery once between each mirror position check.</ptxt>
</item>
<item>
<ptxt>The mirror position cannot be changed manually when the battery is connected. To change the mirror position manually, the battery must be disconnected first.</ptxt>
</item>
</list1>
</atten3>
<spec>
<title>OK</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Mirror Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 19 (MR)</ptxt>
<ptxt>Battery negative (-) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward position (A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (A) to retracted position (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 19 (MR)</ptxt>
<ptxt>Battery positive (+) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward position (A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Does not move</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 19 (MR)</ptxt>
<ptxt>Battery negative (-) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position between forward position (A) and driving position (C)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (B) to retracted position (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 19 (MR)</ptxt>
<ptxt>Battery positive (+) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position between forward position (A) and driving position (C)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (B) to forward position (A)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 19 (MR)</ptxt>
<ptxt>Battery negative (-) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driving position (C)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (C) to retracted position (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 19 (MR)</ptxt>
<ptxt>Battery positive (+) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driving position (C)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Does not move</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 19 (MR)</ptxt>
<ptxt>Battery negative (-) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position between driving position (C) and retracted position (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (D) to retracted position (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 19 (MR)</ptxt>
<ptxt>Battery positive (+) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Position between driving position (C) and retracted position (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (D) to driving position (C)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 19 (MR)</ptxt>
<ptxt>Battery negative (-) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retracted position (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Does not move</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery negative (-) → Terminal 19 (MR)</ptxt>
<ptxt>Battery positive (+) → Terminal 20 (MF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retracted position (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moves from (E) to driving position (C)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002Y4604EX_03_0012" fin="false">OK</down>
<right ref="RM000002Y4604EX_03_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0012" proc-id="RM22W0E___0000IPN00001">
<testtitle>CHECK HARNESS AND CONNECTOR (OUTER REAR VIEW MIRROR RH - OUTER MIRROR CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LHD:</ptxt>
<figure>
<graphic graphicname="B315170E06" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Disconnect the I17 ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the I32 mirror connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I17-5 (RR) - I32-19 (MR)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I17-6 (FR) - I32-20 (MF)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I32-19 (MR) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I32-20 (MF) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD:</ptxt>
<test2>
<ptxt>Disconnect the I23 ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the I32 mirror connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I23-5 (RR) - I32-19 (MR)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I23-6 (FR) - I32-20 (MF)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I32-19 (MR) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I32-20 (MF) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002Y4604EX_03_0021" fin="true">OK</down>
<right ref="RM000002Y4604EX_03_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0008">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0007">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0015">
<testtitle>REPLACE RETRACTABLE OUTER MIRROR SWITCH<xref label="Seep01" href="RM000000W1L06DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0010">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0009">
<testtitle>REPLACE OUTER MIRROR RETRACTOR LH<xref label="Seep01" href="RM000000W1P04HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0020">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0021">
<testtitle>REPLACE OUTER MIRROR CONTROL ECU<xref label="Seep01" href="RM0000039C400NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0013">
<testtitle>REPLACE OUTER MIRROR RETRACTOR RH<xref label="Seep01" href="RM000000W1P04HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0022">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002Y4604EX_03_0023">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>