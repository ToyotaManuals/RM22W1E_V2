<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002K" variety="S002K">
<name>WIPER / WASHER</name>
<ttl id="12068_S002K_7C3Y9_T00RC" variety="T00RC">
<name>WIPER AND WASHER SYSTEM (w/ Rain Sensor)</name>
<para id="RM000002OXI02FX" category="J" type-id="3017B" name-id="WW5AP-03" from="201308">
<dtccode/>
<dtcname>Headlight Cleaner Motor and Relay Circuit</dtcname>
<subpara id="RM000002OXI02FX_01" type-id="60" category="03" proc-id="RM22W0E___0000IR900001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit provides power to the headlight cleaner control relay.</ptxt>
<ptxt>The headlight cleaner control relay sends the signal from the switch, etc. to the headlight cleaner motor to operate the headlight cleaner system.</ptxt>
</content5>
</subpara>
<subpara id="RM000002OXI02FX_02" type-id="32" category="03" proc-id="RM22W0E___0000IRA00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E158383E10" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002OXI02FX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002OXI02FX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002OXI02FX_04_0008" proc-id="RM22W0E___0000IRE00001">
<testtitle>INSPECT FUSE (ECU-IG No.1, H-LP CLR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the ECU-IG No.1 fuse from the main body ECU.</ptxt>
</test1>
<test1>
<ptxt>Remove the H-LP CLR fuse from the engine junction block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ECU-IG No.1 fuse</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H-LP CLR fuse</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002OXI02FX_04_0010" fin="false">OK</down>
<right ref="RM000002OXI02FX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0010" proc-id="RM22W0E___0000IRF00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (HEADLIGHT CLEANER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, perform the Active Test (See page <xref label="Seep01" href="RM000002M5Y01JX"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Head Light Cleaner</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight cleaner operation </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON / OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>for Headlight Cleaner System with Wiper Washer Function</ptxt>
<ptxt>The headlight cleaner system only operates when the windshield wiper switch assembly (front washer switch) is turned on the first time. After that, the headlight cleaner system cannot be operated with the windshield wiper switch assembly (front washer switch) until the engine switch is turned off and then turned to on (IG).</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Headlight cleaner operates.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002OXI02FX_04_0004" fin="true">OK</down>
<right ref="RM000002OXI02FX_04_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0001" proc-id="RM22W0E___0000IRB00001">
<testtitle>CHECK HARNESS AND CONNECTOR (HEADLIGHT CLEANER CONTROL RELAY - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the headlight cleaner control relay connector.</ptxt>
<figure>
<graphic graphicname="E139345E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>4 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002OXI02FX_04_0002" fin="false">OK</down>
<right ref="RM000002OXI02FX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0002" proc-id="RM22W0E___0000IRC00001">
<testtitle>INSPECT HEADLIGHT CLEANER MOTOR AND PUMP ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the headlight cleaner motor and pump assembly (See page <xref label="Seep01" href="RM000002M6D03EX"/>).</ptxt>
<figure>
<graphic graphicname="B172829E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Apply battery voltage to the headlight cleaner motor and check the operation of the headlight cleaner motor.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 1</ptxt>
<ptxt>Battery negative (-) → Terminal 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Headlight cleaner motor operation is normal</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002OXI02FX_04_0003" fin="false">OK</down>
<right ref="RM000002OXI02FX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0003" proc-id="RM22W0E___0000IRD00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CLEANER MOTOR - CONTROL RELAY AND BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the headlight cleaner motor and pump assembly connector.</ptxt>
<figure>
<graphic graphicname="E125074E09" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the headlight cleaner control relay connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2 - 6 (PB)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002OXI02FX_04_0011" fin="false">OK</down>
<right ref="RM000002OXI02FX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0004">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002M5V01HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0009">
<testtitle>REPLACE FUSE (ECU-IG No. 1, H-LP CLR)</testtitle>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0006">
<testtitle>REPLACE HEADLIGHT CLEANER MOTOR AND PUMP ASSEMBLY<xref label="Seep01" href="RM000002M6D03EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0011" proc-id="RM22W0E___0000IRG00001">
<testtitle>REPLACE HEADLIGHT CLEANER CONTROL RELAY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the headlight cleaner control relay with a new or normally functioning one (See page <xref label="Seep01" href="RM000002M6D03FX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check that the headlight cleaner system operates normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Headlight cleaner system operates normally.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>for Headlight Cleaner System with Wiper Washer Function</ptxt>
<ptxt>The headlight cleaner system only operates when the windshield wiper switch assembly (front washer switch) is turned on the first time. After that, the headlight cleaner system cannot be operated with the windshield wiper switch assembly (front washer switch) until the engine switch is turned off and then turned to on (IG).</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002OXI02FX_04_0012" fin="true">OK</down>
<right ref="RM000002OXI02FX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002OXI02FX_04_0012">
<testtitle>END (HEADLIGHT CLEANER CONTROL RELAY IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>