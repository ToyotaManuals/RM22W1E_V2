<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S002F" variety="S002F">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S002F_7C3WU_T00PX" variety="T00PX">
<name>INSTRUMENT PANEL SAFETY PAD</name>
<para id="RM0000039OR00NX" category="A" type-id="80002" name-id="IT3KL-02" from="201308">
<name>DISASSEMBLY</name>
<subpara id="RM0000039OR00NX_02" type-id="11" category="10" proc-id="RM22W0E___0000HHC00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039OR00NX_01" type-id="01" category="01">
<s-1 id="RM0000039OR00NX_01_0001" proc-id="RM22W0E___0000HH500001">
<ptxt>REMOVE CENTER HEATER TO REGISTER DUCT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws and center heater to register duct.</ptxt>
<figure>
<graphic graphicname="B180538" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OR00NX_01_0002" proc-id="RM22W0E___0000HH600001">
<ptxt>REMOVE REAR NO. 1 AIR DUCT (w/ Rear Air Duct)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and rear No. 1 air duct.</ptxt>
<figure>
<graphic graphicname="B180539" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OR00NX_01_0010" proc-id="RM22W0E___0000FIE00001">
<ptxt>REMOVE FRONT PASSENGER AIRBAG ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw and remove the wire.</ptxt>
<figure>
<graphic graphicname="B183434" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the 2 connectors and remove the instrument panel wire from the front passenger airbag.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
<figure>
<graphic graphicname="B189610E01" width="2.775699831in" height="4.7836529in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 5 hooks (A).</ptxt>
<figure>
<graphic graphicname="B189611E01" width="2.775699831in" height="4.7836529in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 5 hooks (B) and remove the front passenger airbag from the instrument panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039OR00NX_01_0004" proc-id="RM22W0E___0000HH700001">
<ptxt>REMOVE DEFROSTER NOZZLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 11 claws and remove the defroster nozzle.</ptxt>
<figure>
<graphic graphicname="B180544" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OR00NX_01_0005" proc-id="RM22W0E___0000HH800001">
<ptxt>REMOVE NO. 1 SIDE DEFROSTER NOZZLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the No. 1 side defroster nozzle.</ptxt>
<figure>
<graphic graphicname="B180541" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OR00NX_01_0006" proc-id="RM22W0E___0000HH900001">
<ptxt>REMOVE NO. 2 SIDE DEFROSTER NOZZLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 claws and remove the No. 2 side defroster nozzle.</ptxt>
<figure>
<graphic graphicname="B180543" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OR00NX_01_0011" proc-id="RM22W0E___0000CHC00001">
<ptxt>REMOVE NAVIGATION ANTENNA ASSEMBLY (w/ Multi-display)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<s3>
<ptxt>Detach the 2 clamps.</ptxt>
<figure>
<graphic graphicname="E247530" width="2.775699831in" height="3.779676365in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the 2 screws and navigation antenna assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<s3>
<ptxt>Detach the 2 clamps.</ptxt>
<figure>
<graphic graphicname="E247531" width="2.775699831in" height="3.779676365in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the 2 screws and navigation antenna assembly.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000039OR00NX_01_0012" proc-id="RM22W0E___0000BS800001">
<ptxt>REMOVE ANTENNA CORD SUB-ASSEMBLY (w/ Multi-display)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Detach the 8 clamps and remove the antenna cord.</ptxt>
<figure>
<graphic graphicname="B181729" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<ptxt>Detach the 6 clamps and remove the antenna cord.</ptxt>
<figure>
<graphic graphicname="B181731" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039OR00NX_01_0013" proc-id="RM22W0E___0000BSA00001">
<ptxt>REMOVE ANTENNA CORD SUB-ASSEMBLY (w/o Multi-display)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Detach the 7 clamps and remove the antenna cord.</ptxt>
<figure>
<graphic graphicname="B181730" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<ptxt>Detach the 5 clamps and remove the antenna cord.</ptxt>
<figure>
<graphic graphicname="B181732" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039OR00NX_01_0008" proc-id="RM22W0E___0000HHA00001">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL WIRE (for LHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamps and remove the No. 2 instrument panel wire from the instrument panel.</ptxt>
<figure>
<graphic graphicname="B180536" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039OR00NX_01_0014" proc-id="RM22W0E___0000HHB00001">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL WIRE (for RHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamps and remove the No. 2 instrument panel wire from the instrument panel.</ptxt>
<figure>
<graphic graphicname="B190100" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>