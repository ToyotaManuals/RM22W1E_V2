<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7C3AF_T003I" variety="T003I">
<name>H150F MANUAL TRANSMISSION / TRANSAXLE</name>
<para id="RM00000196600HX" category="F" type-id="30027" name-id="SS0F5-16" from="201301">
<name>SERVICE DATA</name>
<subpara id="RM00000196600HX_z0" proc-id="RM22W0E___000008C00000">
<content5 releasenbr="1">
<table pgwide="1">
<title>TRANSMISSION CASE OIL SEAL</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<tbody>
<row>
<entry>
<ptxt>transmission case oil seal</ptxt>
</entry>
<entry>
<ptxt>15.4 to 16.2 mm (0.607 to 0.637 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>MANUAL TRANSMISSION UNIT</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 3 transmission clutch hub shaft snap ring clearance</ptxt>
</entry>
<entry>
<ptxt>0.1 mm (0.0039 in.) or less</ptxt>
</entry>
</row>
<row>
<entry morerows="5">
<ptxt>No. 3 transmission clutch hub shaft snap ring thickness</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
<entry>
<ptxt>2.40 to 2.45 mm (0.0945 to 0.0965 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>B</ptxt>
</entry>
<entry>
<ptxt>2.45 to 2.50 mm (0.0965 to 0.0984 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C</ptxt>
</entry>
<entry>
<ptxt>2.50 to 2.55 mm (0.0984 to 0.100 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>D</ptxt>
</entry>
<entry>
<ptxt>2.55 to 2.60 mm (0.100 to 0.102 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>E</ptxt>
</entry>
<entry>
<ptxt>2.60 to 2.65 mm (0.102 to 0.104 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>F</ptxt>
</entry>
<entry>
<ptxt>2.65 to 2.70 mm (0.104 to 0.106 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Reverse gear thrust clearance</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.15 to 0.52 mm (.0.00591 to 0.0204 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Intermediate plate straight pin</ptxt>
</entry>
<entry>
<ptxt>7.0 to 9.0 mm (.0.276 to 0.354 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Transfer adapter straight pin</ptxt>
</entry>
<entry>
<ptxt>Upper side</ptxt>
</entry>
<entry>
<ptxt>4.0 to 6.0 mm (.0.158 to 0.236 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Front side</ptxt>
</entry>
<entry>
<ptxt>9.0 to 11.0 mm (.0.355 to 0.433 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Transfer adapter ring pin</ptxt>
</entry>
<entry>
<ptxt>4.0 to 6.0 mm (.0.157 to 0.236 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>INPUT SHAFT</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<tbody>
<row>
<entry morerows="1">
<ptxt>No. 2 synchronizer ring clearance</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.08 to 0.16 mm (0.0315 to 0.0629 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>0.08 mm (0.0315 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Front bearing shaft snap ring clearance</ptxt>
</entry>
<entry>
<ptxt>0.10 mm (0.00393 in.) or less</ptxt>
</entry>
</row>
<row>
<entry morerows="5">
<ptxt>Front bearing shaft snap ring thickness</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
<entry>
<ptxt>2.50 to 2.55 mm (0.0984 to 0.100 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>B</ptxt>
</entry>
<entry>
<ptxt>2.55 to 2.60 mm (0.100 to 0.102 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C</ptxt>
</entry>
<entry>
<ptxt>2.60 to 2.65 mm (0.102 to 0.104 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>D</ptxt>
</entry>
<entry>
<ptxt>2.65 to 2.70 mm (0.104 to 0.106 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>E</ptxt>
</entry>
<entry>
<ptxt>2.70 to 2.75 mm (0.106 to 0.108 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>F</ptxt>
</entry>
<entry>
<ptxt>2.75 to 2.80 mm (0.108 to 0.110 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>OUTPUT SHAFT</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>3rd gear thrust clearance</ptxt>
</entry>
<entry>
<ptxt>0.10 to 0.45 mm (0.00394 to 0.0177 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>5th gear thrust clearance</ptxt>
</entry>
<entry>
<ptxt>0.10 to 0.35 mm (0.00394 to 0.0137 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>2nd gear thrust clearance</ptxt>
</entry>
<entry>
<ptxt>0.10 to 0.35 mm (0.00394 to 0.0137 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>1st gear thrust clearance</ptxt>
</entry>
<entry>
<ptxt>0.10 to 0.45 mm (0.00394 to 0.0177 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>3rd gear radial clearance</ptxt>
</entry>
<entry>
<ptxt>0.020 to 0.073 mm (0.000788 to 0.00287 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>5th gear radial clearance</ptxt>
</entry>
<entry>
<ptxt>0.015 too 0.068 mm (0.000591 to 0.00267 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>2nd gear radial clearance</ptxt>
</entry>
<entry>
<ptxt>0.015 too 0.068 mm (0.000591 to 0.00267 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>1st gear radial clearance</ptxt>
</entry>
<entry>
<ptxt>0.020 to 0.073 mm (0.000788 to 0.00287 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Output shaft runout</ptxt>
</entry>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.03 mm (0.00118 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="7">
<ptxt>Output shaft outer diameter</ptxt>
</entry>
<entry>
<ptxt>Standard: Part A</ptxt>
</entry>
<entry>
<ptxt>37.979 to 37.995 mm (1.4952 to 1.4959 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Standard: Part B</ptxt>
</entry>
<entry>
<ptxt>45.984 to 46.000 mm (1.8104 to 1.8110 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Standard: Part C</ptxt>
</entry>
<entry>
<ptxt>57.984 to 58.000 mm (2.2828 to 2.2835 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Standard: Part D</ptxt>
</entry>
<entry>
<ptxt>49.979 to 49.995 mm (1.9677 to 1.9683 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum: Part A</ptxt>
</entry>
<entry>
<ptxt>37.979 mm (1.4952 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum: Part B</ptxt>
</entry>
<entry>
<ptxt>45.984 mm (1.8104 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum: Part C</ptxt>
</entry>
<entry>
<ptxt>57.984 mm (2.2828 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum: Part D</ptxt>
</entry>
<entry>
<ptxt>49.979 mm (1.9677 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>3rd gear inner diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>44.015 to 44.040 mm (1.733 to 1.734 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>44.040 mm (1.734 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>5th gear inner diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>53.015 to 53.040 mm (2.087 to 2.088 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>53.040 mm (2.088 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>2nd gear inner diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>65.015 to 65.040 mm (2.560 to 2.561in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>65.040 mm (2.561in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>1st gear inner diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>57.015 to 57.040 mm (2.2447 to 2.2457 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>57.040 mm (2.2457 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>No. 3 synchronizer ring (For 3rd gear) clearance</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>1.15 to 2.05 mm (0.0453 to 0.0807 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>1.15 mm (0.0453 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>No. 3 synchronizer ring (For 5th gear) clearance</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.80 to 1.60 mm (0.0315 to 0.0629 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>0.80 mm (0.0315 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>No. 2 synchronizer ring (For 2nd gear) clearance</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>1.23 to 2.13 mm (0.0485 to 0.0838 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>1.23 mm (0.0485 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>No. 1 synchronizer ring (For 1st gear) clearance</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>1.25 to 2.15 mm (0.0493 to 0.0846 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>1.25 mm (0.0493 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 3 transmission clutch hub shaft snap ring clearance</ptxt>
</entry>
<entry>
<ptxt>0.10 mm (0.00393 in.) or less</ptxt>
</entry>
</row>
<row>
<entry morerows="5">
<ptxt>No. 3 transmission clutch hub shaft snap ring thickness</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
<entry>
<ptxt>2.40 to 2.45 mm (0.0945 to 0.0965 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>B</ptxt>
</entry>
<entry>
<ptxt>2.45 to 2.50 mm (0.0965 to 0.0984 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C</ptxt>
</entry>
<entry>
<ptxt>2.50 to 2.55 mm (0.0984 to 0.100 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>D</ptxt>
</entry>
<entry>
<ptxt>2.55 to 2.60 mm (0.100 to 0.102 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>E</ptxt>
</entry>
<entry>
<ptxt>2.60 to 2.65 mm (0.102 to 0.104 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>F</ptxt>
</entry>
<entry>
<ptxt>2.65 to 2.70 mm (0.104 to 0.106 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 2 clutch hub setting shaft snap ring clearance</ptxt>
</entry>
<entry>
<ptxt>0.10 mm (0.00393 in.) or less</ptxt>
</entry>
</row>
<row>
<entry morerows="5">
<ptxt>No. 2 clutch hub setting shaft snap ring thickness</ptxt>
</entry>
<entry>
<ptxt>4</ptxt>
</entry>
<entry>
<ptxt>1.90 to 1.95 mm (0.0748 to 0.0768 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>5</ptxt>
</entry>
<entry>
<ptxt>1.95 to 2.00 mm (0.0768 to 0.0787 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>6</ptxt>
</entry>
<entry>
<ptxt>2.00 to 2.05 mm (0.0787 to 0.0807 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>7</ptxt>
</entry>
<entry>
<ptxt>2.05 to 2.10 mm (0.0807 to 0.0827 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>8</ptxt>
</entry>
<entry>
<ptxt>2.10 to 2.15 mm (0.0827 to 0.0847 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>9</ptxt>
</entry>
<entry>
<ptxt>2.15 to 2.20 mm (0.0847 to 0.0866 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 1 clutch hub shaft snap ring clearance</ptxt>
</entry>
<entry>
<ptxt>0.10 mm (0.00393 in.) or less</ptxt>
</entry>
</row>
<row>
<entry morerows="5">
<ptxt>No. 1 clutch hub shaft snap ring thickness</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
<entry>
<ptxt>2.90 to 2.95 mm (0.114 to 0.116 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>B</ptxt>
</entry>
<entry>
<ptxt>2.95 to 3.00 mm (0.116 to 0.118 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C</ptxt>
</entry>
<entry>
<ptxt>3.00 to 3.05 mm (0.118 to 0.120 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>D</ptxt>
</entry>
<entry>
<ptxt>3.05 to 3.10 mm (0.120 to 0.122 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>E</ptxt>
</entry>
<entry>
<ptxt>3.10 to 3.15 mm (0.122 to 0.124 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>F</ptxt>
</entry>
<entry>
<ptxt>3.15 to 3.20 mm (0.124 to 0.126 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Reverse gear snap ring clearance</ptxt>
</entry>
<entry>
<ptxt>0.10 mm (0.00393 in.) or less</ptxt>
</entry>
</row>
<row>
<entry morerows="7">
<ptxt>Reverse gear snap ring thickness</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
<entry>
<ptxt>2.40 to 2.45 mm (0.0945 to 0.0965 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>B</ptxt>
</entry>
<entry>
<ptxt>2.45 to 2.50 mm (0.0965 to 0.0984 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C</ptxt>
</entry>
<entry>
<ptxt>2.50 to 2.55 mm (0.0984 to 0.100 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>D</ptxt>
</entry>
<entry>
<ptxt>2.55 to 2.60 mm (0.100 to 0.102 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>E</ptxt>
</entry>
<entry>
<ptxt>2.60 to 2.65 mm (0.102 to 0.104 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>F</ptxt>
</entry>
<entry>
<ptxt>2.65 to 2.70 mm (0.104 to 0.106 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>G</ptxt>
</entry>
<entry>
<ptxt>2.70 to 2.75 mm (0.106 to 0.108 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>H</ptxt>
</entry>
<entry>
<ptxt>2.75 to 2.80 mm (0.108 to 0.110 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>COUNTER GEAR AND REVERSE IDLER GEAR</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<tbody>
<row>
<entry morerows="1">
<ptxt>Counter gear journal outer diameter</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>35.957 to 35.970 mm (1.41562 to 1.41613 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Minimum</ptxt>
</entry>
<entry>
<ptxt>35.957 mm (1.41562 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Counter gear runout</ptxt>
</entry>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.03 mm (0.00118 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Reverse idler gear radial clearance</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.015 to 0.059 mm (0.000591 to 0.0232 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.059 mm (0.0232 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Front No. 1 counter gear bearing snap ring clearance</ptxt>
</entry>
<entry>
<ptxt>0.1 mm (0.00393 in.) or less</ptxt>
</entry>
</row>
<row>
<entry morerows="5">
<ptxt>Front No. 1 counter gear bearing snap ring thickness</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
<entry>
<ptxt>2.45 to 2.50 mm (0.0965 to 0.0984 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>B</ptxt>
</entry>
<entry>
<ptxt>2.50 to 2.55 mm (0.0984 to 0.100 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C</ptxt>
</entry>
<entry>
<ptxt>2.55 to 2.60 mm (0.100 to 0.102 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>D</ptxt>
</entry>
<entry>
<ptxt>2.60 to 2.65 mm (0.102 to 0.104 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>E</ptxt>
</entry>
<entry>
<ptxt>2.65 to 2.70 mm (0.104 to 0.106 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>F</ptxt>
</entry>
<entry>
<ptxt>2.70 to 2.75 mm (0.106 to 0.108 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>OIL PUMP</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<tbody>
<row>
<entry morerows="1">
<ptxt>Tip clearance</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.10 to 0.22 mm (0.00394 to 0.00866 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.22 mm (0.00866 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Body clearance</ptxt>
</entry>
<entry>
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.075 to 0.170 mm (0.00296 to 0.00669 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Maximum</ptxt>
</entry>
<entry>
<ptxt>0.170 mm (0.00669 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Manual transmission oil</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>Capacity</ptxt>
</entry>
<entry>
<ptxt>Classification</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>3.0 liters (3.2 US qts, 2.6 Imp. qts)</ptxt>
</entry>
<entry>
<ptxt>API GL-4</ptxt>
<ptxt>SAE 75W-90</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>