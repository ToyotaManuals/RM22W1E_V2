<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T7_T00MA" variety="T00MA">
<name>ENTRY AND START SYSTEM (for Start Function)</name>
<para id="RM000000YA209TX" category="J" type-id="800SS" name-id="TD4GY-02" from="201301">
<dtccode/>
<dtcname>Engine Switch Indicator Circuit</dtcname>
<subpara id="RM000000YA209TX_01" type-id="60" category="03" proc-id="RM22W0E___0000EJI00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Engine switch condition (power source mode) or system malfunctions can be checked by observing the status of the engine switch indicator light.</ptxt>
<table pgwide="1">
<title>Engine Switch Indicator Light Condition</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry morerows="1" valign="middle">
<ptxt>Power Source Mode/Condition</ptxt>
</entry>
<entry namest="COL2" nameend="COL4" valign="middle">
<ptxt>Indicator Light Condition</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Brake pedal released (for A/T)</ptxt>
<ptxt>Clutch pedal released (for M/T)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Brake pedal depressed, shift lever in P or N (for A/T)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Clutch pedal depressed (for M/T)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates (Green)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates (Green)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>On (ACC, IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates (Amber)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates (Green)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates (Green)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine running</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Steering lock not unlocked</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flashes (Green) for 30 sec.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flashes (Green) for 30 sec.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flashes (Green) for 30 sec.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>System malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flashes (Amber) for 15 sec.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flashes (Amber) for 15 sec.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flashes (Amber) for 15 sec.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Clutch start switch malfunction (for M/T)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flashes (Green) for 15 sec.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flashes (Green) for 15 sec.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flashes (Green) for 15 sec.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000YA209TX_02" type-id="32" category="03" proc-id="RM22W0E___0000EJJ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C165204E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000YA209TX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000YA209TX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YA209TX_04_0001" proc-id="RM22W0E___0000EJK00000">
<testtitle>INSPECT ENGINE SWITCH</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C164809E05" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E18 engine switch connector.</ptxt>
</test1>
<test1>
<ptxt>Apply battery voltage between the terminals of the engine switch, and check the illumination condition of the engine switch indicator light.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>If the positive (+) lead and the negative (-) lead are incorrectly connected, the engine switch indicator light will not illuminate.</ptxt>
</item>
<item>
<ptxt>If the voltage is too low, the indicator light will not illuminate.</ptxt>
</item>
</list1>
</atten3>
<spec>
<title>OK</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery terminal (+) → Terminal 11 (SWIL) - Battery terminal (-) → Terminal 5 (GND)</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Battery voltage is applied to the terminals</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Illuminates</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery terminal (+) → Terminal 12 (INDS) - Battery terminal (-) → Terminal 5 (GND)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery terminal (+) → Terminal 13 (INDW) - Battery terminal (-) → Terminal 5 (GND)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.76in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Does not illuminate (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Does not illuminate (for 1UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Does not illuminate (for 3UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Does not illuminate (for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YA209TX_04_0002" fin="false">A</down>
<right ref="RM000000YA209TX_04_0004" fin="true">B</right>
<right ref="RM000000YA209TX_04_0006" fin="true">C</right>
<right ref="RM000000YA209TX_04_0008" fin="true">D</right>
<right ref="RM000000YA209TX_04_0007" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000000YA209TX_04_0002" proc-id="RM22W0E___0000EJL00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - ENGINE SWITCH AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C162990E05" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E1 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E18 engine switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E1-25 (SWIL) - E18-11 (SWIL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-15 (INDS) - E18-12 (INDS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-14 (INDW) - E18-13 (INDW)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E18-5 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-25 (SWIL) or E18-11 (SWIL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-15 (INDS) or E18-12 (INDS) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-14 (INDW) or E18-13 (INDW) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YA209TX_04_0003" fin="true">OK</down>
<right ref="RM000000YA209TX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YA209TX_04_0003">
<testtitle>REPLACE MAIN BODY ECU</testtitle>
</testgrp>
<testgrp id="RM000000YA209TX_04_0004">
<testtitle>REPLACE ENGINE SWITCH<xref label="Seep01" href="RM000002MDV01ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YA209TX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YA209TX_04_0006">
<testtitle>REPLACE ENGINE SWITCH<xref label="Seep01" href="RM000002MDV020X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YA209TX_04_0008">
<testtitle>REPLACE ENGINE SWITCH<xref label="Seep01" href="RM000002MDV021X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YA209TX_04_0007">
<testtitle>REPLACE ENGINE SWITCH<xref label="Seep01" href="RM000002MDV022X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>