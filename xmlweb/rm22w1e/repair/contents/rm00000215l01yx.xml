<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T8_T00MB" variety="T00MB">
<name>THEFT DETERRENT SYSTEM (w/ Entry and Start System)</name>
<para id="RM00000215L01YX" category="J" type-id="3026K" name-id="TD120-01" from="201301">
<dtccode/>
<dtcname>Glass Breakage Sensor Circuit</dtcname>
<subpara id="RM00000215L01YX_01" type-id="60" category="03" proc-id="RM22W0E___0000EKT00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the glass breakage sensor detects that the back door glass, quarter window glass LH or quarter window glass RH is tapped or broken, the sensor will set off the alarm for 30 seconds.</ptxt>
</content5>
</subpara>
<subpara id="RM00000215L01YX_02" type-id="32" category="03" proc-id="RM22W0E___0000EKU00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B190851E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000215L01YX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM00000215L01YX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000215L01YX_05_0001" proc-id="RM22W0E___0000EKV00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (GLASS BREAK SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Entry&amp;Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Glass break sensor detection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Glass breakage sensor detection / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Glass breakage sensor operates</ptxt>
<ptxt>OFF: Glass breakage sensor does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>ON (glass breakage sensor operates) appears on screen.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000215L01YX_05_0007" fin="true">OK</down>
<right ref="RM00000215L01YX_05_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01YX_05_0002" proc-id="RM22W0E___0000EKW00000">
<testtitle>INSPECT CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E30 ECU connector.</ptxt>
<figure>
<graphic graphicname="B188312E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E30-23 (GBS) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000215L01YX_05_0003" fin="false">OK</down>
<right ref="RM00000215L01YX_05_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01YX_05_0003" proc-id="RM22W0E___0000EKX00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU [SMART KEY ECU ASSEMBLY] - GLASS BREAKAGE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E30 ECU connector.</ptxt>
<figure>
<graphic graphicname="B188315E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E30-23 (GBS) - L34-2 (SSR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L34-1 (SSR-) - K26-2 (SSR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>K26-1 (SSR-) - S10-1 (SSR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S18-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000215L01YX_05_0012" fin="false">OK</down>
<right ref="RM00000215L01YX_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01YX_05_0012" proc-id="RM22W0E___0000EKZ00000">
<testtitle>INSPECT QUARTER WINDOW GLASS RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the L34 sensor connector.</ptxt>
<figure>
<graphic graphicname="B189175E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (SSR-) - 2 (SSR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000215L01YX_05_0005" fin="false">OK</down>
<right ref="RM00000215L01YX_05_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01YX_05_0005" proc-id="RM22W0E___0000EKY00000">
<testtitle>INSPECT QUARTER WINDOW GLASS LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the K26 sensor connector.</ptxt>
<figure>
<graphic graphicname="B189175E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (SSR-) - 2 (SSR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000215L01YX_05_0013" fin="false">OK</down>
<right ref="RM00000215L01YX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01YX_05_0013" proc-id="RM22W0E___0000EL000000">
<testtitle>INSPECT BACK DOOR GLASS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the S10 sensor connector.</ptxt>
<figure>
<graphic graphicname="B189433E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the S18 defogger connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>S10-1 (SSR+) - S18-1 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000215L01YX_05_0007" fin="true">OK</down>
<right ref="RM00000215L01YX_05_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01YX_05_0007">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM00000215L01YX_05_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000215L01YX_05_0010">
<testtitle>REPLACE REAR DOOR QUARTER WINDOW GLASS RH</testtitle>
</testgrp>
<testgrp id="RM00000215L01YX_05_0009">
<testtitle>REPLACE REAR DOOR QUARTER WINDOW GLASS LH</testtitle>
</testgrp>
<testgrp id="RM00000215L01YX_05_0011">
<testtitle>REPLACE BACK DOOR GLASS</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>