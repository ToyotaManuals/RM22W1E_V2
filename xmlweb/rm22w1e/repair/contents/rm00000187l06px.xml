<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000187L06PX" category="C" type-id="302H7" name-id="ESRAE-04" from="201301" to="201308">
<dtccode>P0110</dtccode>
<dtcname>Intake Air Temperature Circuit</dtcname>
<dtccode>P0112</dtccode>
<dtcname>Intake Air Temperature Circuit Low Input</dtcname>
<dtccode>P0113</dtccode>
<dtcname>Intake Air Temperature Circuit High Input</dtcname>
<subpara id="RM00000187L06PX_01" type-id="60" category="03" proc-id="RM22W0E___00002L900000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The intake air temperature sensor is built into the Mass Air Flow (MAF) meter and senses the intake air temperature.</ptxt>
<ptxt>A thermistor built into the sensor changes its resistance value according to the intake air temperature.</ptxt>
<ptxt>The lower the intake air temperature, the greater the thermistor resistance value. The higher the intake air temperature, the lower the thermistor resistance value (See Fig. 1).</ptxt>
<ptxt>The intake air temperature sensor is connected to the ECM.</ptxt>
<ptxt>The 5 V power source voltage in the ECM is applied to the intake air temperature sensor from terminal THA via resistor R.</ptxt>
<ptxt>Resistor R and the intake air temperature sensor are connected in series. When the resistance value of the intake air temperature sensor changes in accordance with changes in the intake air temperature, the potential at terminal THA also changes. Based on this signal, the ECM increases the fuel injection volume to improve driveability with a cold engine.</ptxt>
<figure>
<graphic graphicname="G036559E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>P0110</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 0.5 seconds</ptxt>
</entry>
<entry>
<ptxt>Open or short in the intake air temperature (IAT) sensor for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Open or short in intake air temperature (IAT) sensor circuit</ptxt>
</item>
<item>
<ptxt>IAT sensor (built into mass air flow meter)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0112</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 0.5 seconds</ptxt>
</entry>
<entry>
<ptxt>Short in the IAT sensor for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Short in IAT sensor circuit</ptxt>
</item>
<item>
<ptxt>IAT sensor (built into mass air flow meter)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0113</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ignition switch ON for 0.5 seconds</ptxt>
</entry>
<entry>
<ptxt>Open in the IAT sensor for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Open in IAT sensor circuit</ptxt>
</item>
<item>
<ptxt>IAT sensor (built into mass air flow meter)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>DTC No.</ptxt>
</entry>
<entry>
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P0110</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>Intake Air</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>P0112</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>P0113</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTC P0110, P0112 and/or P0113 is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Misfire</ptxt>
</item>
<item>
<ptxt>Combustion noise</ptxt>
</item>
<item>
<ptxt>Black smoke</ptxt>
</item>
<item>
<ptxt>White smoke</ptxt>
</item>
<item>
<ptxt>Lack of power</ptxt>
</item>
</list2>
<item>
<ptxt>When DTC P0110, P0112 or P0113 is stored, check the intake air temperature by entering the following menus: Powertrain / Engine / Data List / Intake Air.</ptxt>
<table pgwide="1">
<title>Reference</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Temperature Displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Malfunction</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>140°C (284°F) or higher</ptxt>
</entry>
<entry valign="middle">
<ptxt>Short circuit</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187L06PX_02" type-id="32" category="03" proc-id="RM22W0E___00002LA00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A165735E16" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000187L06PX_03" type-id="51" category="05" proc-id="RM22W0E___00002LB00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07YX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06NX"/>).</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTCs relating to different systems are stored, and they share terminal E2 as their ground, check this ground circuit first.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187L06PX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187L06PX_04_0001" proc-id="RM22W0E___00002LC00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (INTAKE AIR TEMPERATURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / Intake Air.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Same as actual intake air temperature.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>140°C (284°F) or higher</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (same as actual intake air temperature)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If there is an open circuit, the tester indicates -40°C (-40°F).</ptxt>
</item>
<item>
<ptxt>If there is a short circuit, the tester indicates 140°C (284°F) or more.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000187L06PX_04_0002" fin="false">A</down>
<right ref="RM00000187L06PX_04_0004" fin="false">B</right>
<right ref="RM00000187L06PX_04_0012" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0002" proc-id="RM22W0E___00002LD00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR OPEN IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
<figure>
<graphic graphicname="A166418E03" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Connect terminals 4 (THA) and 5 (E2) of the mass air flow meter wire harness side connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / Intake Air.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>140°C (284°F) or higher</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<right ref="RM00000187L06PX_04_0008" fin="false">OK</right>
<right ref="RM00000187L06PX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0003" proc-id="RM22W0E___00002LE00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MASS AIR FLOW METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
<figure>
<graphic graphicname="A182382E02" width="2.775699831in" height="5.787629434in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C50-4 (THA) - C45-71 (THA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-5 (E2) - C45-75 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C50-4 (THA) - C46-71 (THA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-5 (E2) - C46-75 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<right ref="RM00000187L06PX_04_0011" fin="false">OK</right>
<right ref="RM00000187L06PX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0004" proc-id="RM22W0E___00002LF00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR SHORT IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
<figure>
<graphic graphicname="A166688E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / Intake Air.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>-40°C (-40°F)</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<right ref="RM00000187L06PX_04_0010" fin="false">OK</right>
<right ref="RM00000187L06PX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0005" proc-id="RM22W0E___00002LG00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MASS AIR FLOW METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
<figure>
<graphic graphicname="A182382E03" width="2.775699831in" height="5.787629434in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C50-4 (THA) or C45-71 (THA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C50-4 (THA) or C46-71 (THA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187L06PX_04_0006" fin="false">OK</down>
<right ref="RM00000187L06PX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0006" proc-id="RM22W0E___00002LH00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187L06PX_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0008" proc-id="RM22W0E___00002LI00000">
<testtitle>CONFIRM GOOD CONNECTION TO SENSOR. IF OK, REPLACE MASS AIR FLOW METER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the mass air flow meter (See page <xref label="Seep01" href="RM000002PPQ01EX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187L06PX_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<res>
<right ref="RM00000187L06PX_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0011" proc-id="RM22W0E___00002LK00000">
<testtitle>CONFIRM GOOD CONNECTION TO ECM. IF OK, REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187L06PX_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0010" proc-id="RM22W0E___00002LJ00000">
<testtitle>REPLACE MASS AIR FLOW METER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the mass air flow meter (See page <xref label="Seep01" href="RM000002PPQ01EX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187L06PX_04_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0012" proc-id="RM22W0E___00002LL00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187L06PX_04_0013" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187L06PX_04_0013">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>