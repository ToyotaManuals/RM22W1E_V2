<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000F" variety="S000F">
<name>1GR-FE FUEL</name>
<ttl id="12008_S000F_7C3G9_T009C" variety="T009C">
<name>FUEL SYSTEM</name>
<para id="RM000000YL705QX" category="G" type-id="8000T" name-id="FUAGV-02" from="201308">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM000000YL705QX_01" type-id="01" category="01">
<s-1 id="RM000000YL705QX_01_0001" proc-id="RM22W0E___00005QS00001">
<ptxt>CHECK FOR FUEL PUMP OPERATION AND FUEL LEAK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Turn the GTS on.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Fuel Pump / Speed.</ptxt>
</s2>
<s2>
<ptxt>Check fuel pump operation.</ptxt>
<s3>
<ptxt>Check for pressure in the fuel inlet tube from the fuel line. Check that the sound of fuel flowing in the fuel tank can be heard.</ptxt>
<ptxt>If there is no sound, check the integration relay, fuel pump, ECM and wiring connector.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check for fuel leak.</ptxt>
<s3>
<ptxt>Check that there are no fuel leaks after performing maintenance anywhere on the system.</ptxt>
<ptxt>If there are fuel leaks, repair or replace parts as necessary.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000YL705QX_01_0002" proc-id="RM22W0E___00005QT00001">
<ptxt>CHECK FUEL PRESSURE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the battery voltage is 11 to 14 V.</ptxt>
</s2>
<s2>
<ptxt>Discharge the fuel system pressure (See page <xref label="Seep01" href="RM0000028RU04PX"/>).</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep06" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003C32006X"/>).</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the fuel pipe clamp from the fuel tube connector.</ptxt>
<figure>
<graphic graphicname="A274146" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the No. 1 fuel hose (See page <xref label="Seep03" href="RM0000028RU04PX"/>).</ptxt>
</s2>
<s2>
<ptxt>Install SST (pressure gauge) as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A274077E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<sst>
<sstitem>
<s-number>09268-45101</s-number>
<s-subnumber>09268-41250</s-subnumber>
<s-subnumber>09268-41260</s-subnumber>
<s-subnumber>09268-41280</s-subnumber>
<s-subnumber>09268-41500</s-subnumber>
<s-subnumber>09268-41700</s-subnumber>
<s-subnumber>95336-08070</s-subnumber>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>SST (T Joint)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>SST (Hose Band)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>SST (Hose Joint)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>SST (Gauge Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Fuel Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry>
<ptxt>SST (Fuel Tube Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Wipe off any gasoline.</ptxt>
</s2>
<s2>
<ptxt>Reconnect the cable to the negative (-) battery terminal.</ptxt>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep04" href="RM000003C32006X"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Operate the fuel pump.</ptxt>
<s3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the ignition switch to ON.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Turn the GTS on.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Activate the Fuel Pump Speed Control / ON.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Measure the fuel pressure.</ptxt>
<spec>
<title>Standard fuel pressure</title>
<specitem>
<ptxt>321 to 327 kPa (3.27 to 3.33 kgf/cm<sup>2</sup>, 46.5 to 47.4 psi)</ptxt>
</specitem>
</spec>
<ptxt>If the pressure is higher than the specification, replace the fuel pressure regulator.</ptxt>
<ptxt>If the pressure is below the specification, check the fuel hoses and connections, fuel pump, fuel filter and fuel pressure regulator.</ptxt>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>Measure the fuel pressure.</ptxt>
<spec>
<title>Standard fuel pressure</title>
<specitem>
<ptxt>321 to 327 kPa (3.27 to 3.33 kgf/cm<sup>2</sup>, 46.5 to 47.4 psi)</ptxt>
</specitem>
</spec>
<ptxt>If the pressure is not as specified, check the fuel pump, pressure regulator and/or injectors.</ptxt>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Check that the fuel pressure remains as specified for 5 minutes after the engine has stopped.</ptxt>
<spec>
<title>Standard fuel pressure</title>
<specitem>
<ptxt>147 kPa (1.5 kgf/cm<sup>2</sup>, 21 psi) or more</ptxt>
</specitem>
</spec>
<ptxt>If the pressure is not as specified, check the fuel pump, pressure regulator and/or injectors.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal (See page <xref label="Seep05" href="RM000003C32006X"/>) and carefully remove SST and the fuel tube connector to prevent gasoline from spraying.</ptxt>
</s2>
<s2>
<ptxt>Reconnect the No. 1 fuel hose (fuel tube connector).</ptxt>
</s2>
<s2>
<ptxt>Check for fuel leaks.</ptxt>
<s3>
<ptxt>Check that there are no fuel leaks after performing maintenance anywhere on the system.</ptxt>
<ptxt>If there are fuel leaks, repair or replace parts as necessary.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>