<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002LJ5048X" category="C" type-id="300B5" name-id="ACBKT-02" from="201301">
<dtccode>B1413/13</dtccode>
<dtcname>Evaporator Temperature Sensor Circuit</dtcname>
<subpara id="RM000002LJ5048X_01" type-id="60" category="03" proc-id="RM22W0E___0000H1Q00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The No. 1 cooler thermistor is installed on the evaporator in the air conditioner unit to detect the cooled air temperature that has passed through the evaporator and control the air conditioning. It sends appropriate signals to the air conditioning amplifier assembly. The resistance of the No. 1 cooler thermistor changes in accordance with the cooled air temperature that has passed through the evaporator. As the temperature decreases, the resistance increases. As the temperature increases, the resistance decreases.</ptxt>
<ptxt>The air conditioning amplifier assembly applies voltage (5 V) to the No. 1 cooler thermistor and reads voltage changes as the resistance of the No. 1 cooler thermistor changes. This sensor is used for frost prevention.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1413/13</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>An open or short in the No. 1 cooler thermistor circuit.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>No. 1 cooler thermistor</ptxt>
</item>
<item>
<ptxt>Air conditioning harness assembly (No. 1 cooler thermistor)</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002LJ5048X_02" type-id="32" category="03" proc-id="RM22W0E___0000H1R00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E140288E08" width="7.106578999in" height="2.775699831in"/>
</figure>
<figure>
<graphic graphicname="E140288E09" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002LJ5048X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002LJ5048X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002LJ5048X_04_0001" proc-id="RM22W0E___0000H1S00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (NO. 1 COOLER THERMISTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the No. 1 cooler thermistor is functioning properly. </ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Evaporator Fin Thermistor</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No. 1 cooler thermistor /</ptxt>
<ptxt>Min.: -29.7°C (-21.46°F)</ptxt>
<ptxt>Max.: 59.55°C (139.19°F)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Actual evaporator temperature displayed</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Open in the circuit: -29.7°C (-21.46°F).</ptxt>
<ptxt>Short in the circuit: 59.55°C (139.19°F).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LJ5048X_04_0012" fin="true">OK</down>
<right ref="RM000002LJ5048X_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LJ5048X_04_0002" proc-id="RM22W0E___0000H1T00000">
<testtitle>INSPECT NO. 1 COOLER THERMISTOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E160596E02" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Remove the No. 1 cooler thermistor.</ptxt>
<list1 type="unordered">
<item>
<ptxt>for LHD (See page <xref label="Seep01" href="RM000003AXS02YX"/>)</ptxt>
</item>
<item>
<ptxt>for RHD (See page <xref label="Seep02" href="RM000003AXS02XX"/>)</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="8" valign="middle">
<ptxt>1 (+) - 2 (-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>-10°C (14°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.30 to 9.10 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>-5°C (23°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.65 to 6.95 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>0°C (32°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.40 to 5.35 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>5°C (41°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.40 to 4.15 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>10°C (50°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.70 to 3.25 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>15°C (59°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.14 to 2.58 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.71 to 2.05 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>25°C (77°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.38 to 1.64 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>30°C (86°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.11 to 1.32 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Even slightly touching the sensor may change the resistance value. Be sure to hold the connector of the sensor.</ptxt>
</item>
<item>
<ptxt>When measuring, the sensor temperature must be the same as the ambient temperature.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>As the temperature increases, the resistance decreases (see the graph).</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002LJ5048X_04_0009" fin="true">A</down>
<right ref="RM000002LJ5048X_04_0010" fin="true">B</right>
<right ref="RM000002LJ5048X_04_0007" fin="true">C</right>
<right ref="RM000002LJ5048X_04_0013" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000002LJ5048X_04_0012">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LJ5048X_04_0010">
<testtitle>REPLACE AIR CONDITIONING HARNESS ASSEMBLY<xref label="Seep01" href="RM000003AXS02VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LJ5048X_04_0007">
<testtitle>REPLACE NO. 1 COOLER THERMISTOR<xref label="Seep01" href="RM000003AXS02YX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LJ5048X_04_0013">
<testtitle>REPLACE NO. 1 COOLER THERMISTOR<xref label="Seep01" href="RM000003AXS02XX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LJ5048X_04_0009">
<testtitle>REPLACE AIR CONDITIONING HARNESS ASSEMBLY<xref label="Seep01" href="RM000003AXS02WX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>