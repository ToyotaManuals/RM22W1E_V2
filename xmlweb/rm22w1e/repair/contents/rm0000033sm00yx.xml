<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12060_S002C" variety="S002C">
<name>SEAT BELT</name>
<ttl id="12060_S002C_7C3VI_T00OL" variety="T00OL">
<name>REAR CENTER SEAT OUTER BELT ASSEMBLY (for Rear No. 1 Seat)</name>
<para id="RM0000033SM00YX" category="A" type-id="30014" name-id="SB1RM-04" from="201308">
<name>INSTALLATION</name>
<subpara id="RM0000033SM00YX_02" type-id="11" category="10" proc-id="RM22W0E___0000GJF00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0DSX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000033SM00YX_01" type-id="01" category="01">
<s-1 id="RM0000033SM00YX_01_0001" proc-id="RM22W0E___0000GJC00001">
<ptxt>INSTALL NO. 1 SEAT 3 POINT TYPE BELT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185876E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Install the belt with the nut labeled A.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the belt sensor with the 3 nuts labeled B.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000033SM00YX_01_0002" proc-id="RM22W0E___0000GJD00001">
<ptxt>INSTALL REAR SEAT SHOULDER BELT HOLE COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B188669" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seat belt as shown in the illustration.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B185854" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 3 claws to install the shoulder belt hole cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000033SM00YX_01_0030" proc-id="RM22W0E___0000G4F00001">
<ptxt>INSTALL REAR LOWER SEATBACK COVER SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184963" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0031" proc-id="RM22W0E___0000G4G00001">
<ptxt>INSTALL REAR SEATBACK HINGE COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181237" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0032" proc-id="RM22W0E___0000G4O00001">
<ptxt>INSTALL SEATBACK COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seatback cover with pad.</ptxt>
</s2>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Pull out the seat belt.</ptxt>
<figure>
<graphic graphicname="B184030" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Pull out the seat belt.</ptxt>
<figure>
<graphic graphicname="B189169" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Pass the side airbag wire harness through the hole in the seatback.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Pass the seatback heater wire harness through the hole in the seatback.</ptxt>
<s3>
<figure>
<graphic graphicname="B184057" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Connect the seatback heater wire harness and install a new cable tie.</ptxt>
<figure>
<graphic graphicname="B182598E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<figure>
<graphic graphicname="B184020" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the seatback cover bracket to the seat frame with the cap nut.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>After the seatback trim cover is assembled, make sure the side airbag strap is not twisted.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Using hog ring pliers, install new hog rings to the seatback frame.</ptxt>
<figure>
<graphic graphicname="B262669E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Using hog ring pliers, install new hog rings to the seatback frame.</ptxt>
<figure>
<graphic graphicname="B262670E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0033" proc-id="RM22W0E___0000G4P00001">
<ptxt>INSTALL REAR SEAT BELT HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181239" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0034" proc-id="RM22W0E___0000G4Q00001">
<ptxt>INSTALL SHOULDER BELT ANCHOR COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184040" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0035" proc-id="RM22W0E___0000G4R00001">
<ptxt>INSTALL REAR NO. 1 SEAT HEADREST SUPPORT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<ptxt>Attach the 4 claws to install the 2 supports.</ptxt>
<figure>
<graphic graphicname="B181190" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<ptxt>Attach the 8 claws to install the 4 supports.</ptxt>
<figure>
<graphic graphicname="B181243" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0036">
<ptxt>INSTALL REAR SEAT HEADREST ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM0000033SM00YX_01_0037" proc-id="RM22W0E___0000G4S00001">
<ptxt>INSTALL REAR SEATBACK BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Center Armrest:</ptxt>
<figure>
<graphic graphicname="B184026" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Push the board down to attach the 2 claws on the bottom.</ptxt>
</s3>
<s3>
<ptxt>Attach the 2 claws on the top of the board to install it.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/o Center Armrest:</ptxt>
<figure>
<graphic graphicname="B189166" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Push the board down to attach the 3 claws on the bottom.</ptxt>
</s3>
<s3>
<ptxt>Attach the 3 claws on the top of the board to install it.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0038" proc-id="RM22W0E___0000G4T00001">
<ptxt>INSTALL REAR SEATBACK COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182667" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0039" proc-id="RM22W0E___0000G4W00001">
<ptxt>INSTALL SEAT CUSHION COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seat cushion cover with pad.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Pass the side airbag wire harness through the hole in the seat cushion.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<s3>
<ptxt>Pass the seatback heater wire harness through the hole in the seat cushion.</ptxt>
<figure>
<graphic graphicname="B184056" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Connect the 2 seat heater connectors.</ptxt>
<figure>
<graphic graphicname="B182596" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the side airbag wire harness clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0040" proc-id="RM22W0E___0000G6Q00001">
<ptxt>INSTALL REAR NO. 1 SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184532E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the belt with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the vehicle body.</ptxt>
</atten3>
<atten4>
<ptxt>The No. 1 seat 3 point type belt anchor is fixed with the same bolt as the rear No. 1 seat inner belt LH. Therefore, connect it when installing the rear No. 1 seat inner belt LH.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0041" proc-id="RM22W0E___0000G4Y00001">
<ptxt>INSTALL REAR SEAT UNDER TRAY COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181181" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0042" proc-id="RM22W0E___0000G4Z00001">
<ptxt>INSTALL REAR SEAT CUSHION COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181231" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0043" proc-id="RM22W0E___0000G5100001">
<ptxt>INSTALL REAR UNDER SIDE COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181230" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws and 2 clips to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 5 screws.</ptxt>
</s2>
<s2>
<ptxt>Attach the 10 claws.</ptxt>
<figure>
<graphic graphicname="B181229" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0044" proc-id="RM22W0E___0000G5200001">
<ptxt>INSTALL REAR SEATBACK STAY COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181226" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0045" proc-id="RM22W0E___0000G5300001">
<ptxt>INSTALL SEAT BELT ANCHOR COVER CAP
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the anchor cover cap with the screw.</ptxt>
<figure>
<graphic graphicname="B181228" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the claw to close the cap.</ptxt>
<figure>
<graphic graphicname="B184029" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0046" proc-id="RM22W0E___0000G5400001">
<ptxt>INSTALL REAR NO. 1 SEAT HINGE SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184017" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the hinge with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0047" proc-id="RM22W0E___0000G5500001">
<ptxt>INSTALL REAR NO. 1 SEAT HINGE SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the cable clamp to connect the cable.</ptxt>
<figure>
<graphic graphicname="B181176" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the hinge with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B184045" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0048" proc-id="RM22W0E___0000G5600001">
<ptxt>INSTALL REAR NO. 2 SEAT PROTECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the wire harness, and attach 2 claws to close the protector.</ptxt>
</s2>
<s2>
<ptxt>Attach the claw to install the protector to the seat hinge.</ptxt>
<figure>
<graphic graphicname="B182595" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Attach the claw to install the wire harness connector.</ptxt>
<figure>
<graphic graphicname="B184016" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Attach the claw to install the wire harness connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0049" proc-id="RM22W0E___0000G5A00001">
<ptxt>INSTALL REAR NO. 2 SEAT LEG SIDE COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Pass the fold seat stopper band through the seat leg side cover and attach it to the seat cushion frame with the bolt.</ptxt>
<figure>
<graphic graphicname="B184044" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 4 claws to install the seat leg side cover.</ptxt>
<figure>
<graphic graphicname="B181224" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 4 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0050" proc-id="RM22W0E___0000G5C00001">
<ptxt>INSTALL UPPER SEAT TRACK RAIL COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184041" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0051" proc-id="RM22W0E___0000FYC00001">
<ptxt>INSTALL UPPER SEAT TRACK RAIL COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182665" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0052" proc-id="RM22W0E___0000G5E00001">
<ptxt>INSTALL REAR NO. 1 SEAT RECLINING COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184015" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0053" proc-id="RM22W0E___0000G5F00001">
<ptxt>INSTALL REAR NO. 1 SEAT RECLINING COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184027E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws in the order shown in the illustration to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0054" proc-id="RM22W0E___0000G5G00001">
<ptxt>INSTALL RECLINING ADJUSTER RELEASE HANDLE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184013" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the release handle with the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0055" proc-id="RM22W0E___0000G5H00001">
<ptxt>INSTALL SEAT ADJUSTER BOLT COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184012" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000033SM00YX_01_0056" proc-id="RM22W0E___0000GJE00001">
<ptxt>INSTALL REAR NO. 1 SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear No. 1 seat assembly (See page <xref label="Seep01" href="RM00000390X00SX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>