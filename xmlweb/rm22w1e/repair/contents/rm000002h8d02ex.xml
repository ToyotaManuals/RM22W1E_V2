<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000G" variety="S000G">
<name>1UR-FE FUEL</name>
<ttl id="12008_S000G_7C3GN_T009Q" variety="T009Q">
<name>FUEL PRESSURE REGULATOR</name>
<para id="RM000002H8D02EX" category="A" type-id="80001" name-id="FU8TK-03" from="201308">
<name>REMOVAL</name>
<subpara id="RM000002H8D02EX_01" type-id="01" category="01">
<s-1 id="RM000002H8D02EX_01_0002" proc-id="RM22W0E___00004J400001">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE
</ptxt>
<content1 releasenbr="1">
<atten2>
<list1 type="unordered">
<item>
<ptxt>Do not disconnect any part of the fuel system until you have discharged the fuel system pressure.</ptxt>
</item>
<item>
<ptxt>After discharging the fuel pressure, place a cloth or equivalent over fittings as you separate them to reduce the risk of fuel spray on yourself or in the engine compartment.</ptxt>
</item>
</list1>
</atten2>
<s2>
<ptxt>Disconnect the fuel pump ECU connector (See page <xref label="Seep01" href="RM000002YPO025X_01_0001"/>).</ptxt>
</s2>
<s2>
<ptxt>Start the engine. After the engine stops, turn the engine switch off.</ptxt>
<atten4>
<ptxt>DTC P0171/0174 (system too lean) may be stored.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Crank the engine again, and then check that the engine does not start.</ptxt>
</s2>
<s2>
<ptxt>Loosen the fuel tank cap, and then discharge the pressure in the fuel tank completely.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep03" href="RM000003C32006X"/>).</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Connect the fuel pump ECU connector (See page <xref label="Seep04" href="RM000002YPM025X_01_0001"/>).</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002H8D02EX_01_0007" proc-id="RM22W0E___00005WJ00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002H8D02EX_01_0003" proc-id="RM22W0E___00005WI00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002H8D02EX_01_0004" proc-id="RM22W0E___000013G00000">
<ptxt>REMOVE V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Raise the front of the V-bank cover to detach the 3 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<figure>
<graphic graphicname="A274415E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002H8D02EX_01_0006" proc-id="RM22W0E___000013H00000">
<ptxt>REMOVE AIR CLEANER CAP AND HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 PCV hose and No. 1 air hose.</ptxt>
<figure>
<graphic graphicname="A272589" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the mass air flow meter connector and detach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Loosen the hose clamp and remove the air cleaner cap and hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002H8D02EX_01_0001" proc-id="RM22W0E___00005WH00001">
<ptxt>REMOVE FUEL PRESSURE REGULATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 fuel tube from the pressure regulator (See page <xref label="Seep01" href="RM0000028RU04NX"/>).</ptxt>
<figure>
<graphic graphicname="A218699" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Put a cloth or equivalent under the pressure regulator.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Remove the No. 1 air hose from the pressure regulator.</ptxt>
<figure>
<graphic graphicname="A218700" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts, and then pull out the pressure regulator.</ptxt>
<figure>
<graphic graphicname="A218701" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the pressure regulator.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>