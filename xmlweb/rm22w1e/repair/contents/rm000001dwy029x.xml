<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000001DWY029X" category="U" type-id="303FJ" name-id="BC19E-45" from="201301">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM000001DWY029X_z0" proc-id="RM22W0E___0000ANL00000">
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the GTS to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the GTS on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the GTS, read the Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4" align="left">
<colspec colname="COLSPEC1" colwidth="1.49in"/>
<colspec colname="COL2" colwidth="1.87in"/>
<colspec colname="COL3" colwidth="1.87in"/>
<colspec colname="COL4" colwidth="1.85in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ABS Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS warning light/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Warning light on</ptxt>
<ptxt>OFF: Warning light off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake warning light/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Warning light on</ptxt>
<ptxt>OFF: Warning light off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Buzzer</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Buzzer on</ptxt>
<ptxt>OFF: Buzzer off</ptxt>
</entry>
<entry valign="middle">
<ptxt>The combination meter has a built-in buzzer.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stop Light SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Brake pedal depressed</ptxt>
<ptxt>OFF: Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Parking Brake SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Parking brake switch/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Parking brake applied</ptxt>
<ptxt>OFF: Parking brake released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Reservoir Warning SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake fluid level warning switch/ ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Main Idle SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main idle switch/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Accelerator pedal released</ptxt>
<ptxt>OFF: Accelerator pedal depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Master Cylinder Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master cylinder pressure sensor reading/ Min.: 0.00 V, Max.: 5.00 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>When brake pedal released: 0.3 to 0.9 V </ptxt>
</entry>
<entry valign="middle">
<ptxt>The value increases when the brake pedal is depressed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Zero Point of M/C</ptxt>
</entry>
<entry valign="middle">
<ptxt>Memorized zero value/ Min.: -12.50 MPa, Max.: 12.40 MPa</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Accumulator Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accumulator pressure sensor reading/ Min.: 0.00 V, Max.: 5.00 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.58 to 5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>If the value is constant regardless of the pump operation, an accumulator pressure sensor malfunction is suspected.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Deceleration Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Deceleration sensor reading/ Min.: -18.525 m/s<sup>2</sup>, Max.: 18.387 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes continuously during acceleration/deceleration.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Zero Point of Decele</ptxt>
</entry>
<entry valign="middle">
<ptxt>Memorized zero value/ Min.: -25.10 m/s<sup>2</sup>, Max.: 24.90 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Forward and Rearward G</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward and rearward G/ Min.: -25.10 m/s<sup>2</sup>, Max.: 24.90 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes in proportion with acceleration during acceleration/deceleration.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear Diff Lock Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear differential lock detection switch/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Rear differential lock detection switch on</ptxt>
<ptxt>OFF: Rear differential lock detection switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L4 Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transfer L4 detection switch/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: L4</ptxt>
<ptxt>OFF: except L4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FR Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FL Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RR Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor RH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RL Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor LH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Maximum wheel speed sensor reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual vehicle speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FR Wheel Acceleration</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wheel acceleration RH/ Min.: -200.84 m/s<sup>2</sup>, Max.: 199.27 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes continuously during acceleration/deceleration.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FL Wheel Acceleration</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wheel acceleration LH/ Min.: -200.84 m/s<sup>2</sup>, Max.: 199.27 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes continuously during acceleration/deceleration.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RR Wheel Acceleration</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear wheel acceleration RH/ Min.: -200.84 m/s<sup>2</sup>, Max.: 199.27 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes continuously during acceleration/deceleration.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RL Wheel Acceleration</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear wheel acceleration LH/ Min.: -200.84 m/s<sup>2</sup>, Max.: 199.27 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes continuously during acceleration/deceleration.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FR Wheel Direction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wheel direction RH/ Forward or Back</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward: Forward</ptxt>
<ptxt>Back: Back</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FL Wheel Direction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wheel direction LH/ Forward or Back</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward: Forward</ptxt>
<ptxt>Back: Back</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RR Wheel Direction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear wheel direction RH/ Forward or Back</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward: Forward</ptxt>
<ptxt>Back: Back</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RL Wheel Direction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear wheel direction LH/ Forward or Back</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward: Forward</ptxt>
<ptxt>Back: Back</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FR Wheel ABS Ctrl Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wheel RH ABS control status/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: During control</ptxt>
<ptxt>OFF: Not during control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FL Wheel ABS Ctrl Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wheel LH ABS control status/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: During control</ptxt>
<ptxt>OFF: Not during control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RR Wheel ABS Ctrl Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear wheel RH ABS control status/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: During control</ptxt>
<ptxt>OFF: Not during control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RL Wheel ABS Ctrl Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear wheel LH ABS control status/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: During control</ptxt>
<ptxt>OFF: Not during control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RR Wheel EBD Ctrl Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear wheel RH EBD control status/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: During control</ptxt>
<ptxt>OFF: Not during control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RL Wheel EBD Ctrl Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear wheel LH EBD control status/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: During control</ptxt>
<ptxt>OFF: Not during control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>BA Ctrl Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>BA control status/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: During control</ptxt>
<ptxt>OFF: Not during control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine Revolutions</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine speed/ Min.: 0 rpm, Max.: 65535 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual engine speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Real Engine Torque</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual engine torque/ Min.: -1024.0 Nm, Max.: 1023.0 Nm</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Accelerator Opening Angle %</ptxt>
</entry>
<entry valign="middle">
<ptxt>Percentage of accelerator pedal opening angle/ Min.: 0.0%, Max.: 128.0%</ptxt>
</entry>
<entry valign="middle">
<ptxt>When accelerator pedal released: 0%</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes in proportion with the movement of the pedal during accelerator pedal operation.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Solenoid Relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid relay/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Solenoid relay on</ptxt>
<ptxt>OFF: Solenoid relay off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ECB* Motor Relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>Motor relay/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Motor relay on</ptxt>
<ptxt>OFF: Motor relay off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>TR(A)C/VSC Solenoid (SRM2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>TRC/VSC solenoid (SRM2)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>TR(A)C/VSC Solenoid (SRM1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>TRC/VSC solenoid (SRM1)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>TR(A)C/VSC Solenoid (SRC2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>TRC/VSC solenoid (SRC2)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>TR(A)C/VSC Solenoid (SRC1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>TRC/VSC solenoid (SRC1)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SFRH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SFRH)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SFRR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SFRR)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SFLH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SFLH)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SFLR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SFLR)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SRRH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SRRH)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SRRR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SRRR)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SRLH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SRLH)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SRLR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SRLR)/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Operating</ptxt>
<ptxt>OFF: Not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FR Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FL Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RR Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor RH open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RL Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor LH open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>EFI Communication Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM communication open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Deceleration Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Deceleration sensor open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Master Cylinder Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master cylinder pressure sensor open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>IG1 Voltage Value</ptxt>
</entry>
<entry valign="middle">
<ptxt>IG1 voltage value/ Min.: 0.00 V, Max.: 20.00 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON: 11 to 14 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>IG2 Voltage Value</ptxt>
</entry>
<entry valign="middle">
<ptxt>IG2 voltage value/ Min.: 0.00 V, Max.: 20.00 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON: 11 to 14 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Number of DTC</ptxt>
</entry>
<entry valign="middle">
<ptxt>Number of DTCs/ Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>IG Voltage Value Decreased</ptxt>
</entry>
<entry valign="middle">
<ptxt>IG voltage value decreased detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>IG Voltage Value Increased</ptxt>
</entry>
<entry valign="middle">
<ptxt>IG voltage value increased detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FR Speed Sensor Voltage Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH voltage open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>FL Speed Sensor Voltage Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH voltage open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RR Speed Sensor Voltage Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor RH voltage open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RL Speed Sensor Voltage Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor LH voltage open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>M/C Pressure Sensor Noise</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master cylinder pressure sensor noise detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>EFI Communication Invalid</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM communication invalid detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: Electronically Controlled Brake</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the GTS to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the GTS on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the GTS, perform the Active Test.</ptxt>
</step2>
</step1>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" align="center" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ABS Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS warning light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Warning light ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Observe the combination meter.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake warning light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Warning light ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Observe the combination meter.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Buzzer</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>The buzzer can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Solenoid Relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>Relay ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Motor Relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>Motor relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>Relay ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid of the motor can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SRLR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SRLR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid  (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SRLH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SRLH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid  (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SRRR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SRRR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid  (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SRRH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SRRH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid  (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SFLR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SFLR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SFLH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SFLH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid  (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SFRR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SFRR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid  (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid (SFRH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS solenoid (SFRH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid  (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VSC/TRC Solenoid (SRMR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSC/TRC solenoid (SRM2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VSC/TRC Solenoid (SRMF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSC/TRC solenoid (SRM1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VSC/TRC Solenoid (SRCR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSC/TRC solenoid (SRC2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VSC/TRC Solenoid (SRCF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSC/TRC solenoid (SRC1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Solenoid ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the solenoid (clicking sound) can be heard.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABS Solenoid</ptxt>
</entry>
<entry valign="middle">
<ptxt>Selection of multiple holding solenoids (SFRH, SFLH, SRRH and SRLH) and pressure reduction solenoids (SFRR, SFLR, SRRR and SRLR) possible</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Start/Stop</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>VSC/TRC Solenoid</ptxt>
</entry>
<entry valign="middle">
<ptxt>Selection of multiple VSC/TRC solenoids [SRMR (SRM2), SRMF (SRM1), SRCR (SRC2) and SRCF (SRC1)] possible</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Start/Stop</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>