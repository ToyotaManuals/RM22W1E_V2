<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R4_T00K7" variety="T00K7">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM0000020F2082X" category="C" type-id="800HF" name-id="AV6XX-03" from="201301" to="201308">
<dtccode>63-46</dtccode>
<dtcname>Scratched / Reversed Disc</dtcname>
<subpara id="RM0000020F2082X_01" type-id="60" category="03" proc-id="RM22W0E___0000C8H00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>63-46</ptxt>
</entry>
<entry valign="middle">
<ptxt>Scratches or dirt is found on CD surface or CD is set upside down.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>CD</ptxt>
</item>
<item>
<ptxt>Radio receiver assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000020F2082X_02" type-id="51" category="05" proc-id="RM22W0E___0000C8I00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>After the inspection is completed, clear the DTCs.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000020F2082X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000020F2082X_04_0001" proc-id="RM22W0E___0000C8J00000">
<testtitle>CHECK THAT CD IS INSERTED PROPERLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether or not the CD is inserted upside down.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>CD is properly inserted.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000020F2082X_04_0003" fin="false">OK</down>
<right ref="RM0000020F2082X_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000020F2082X_04_0003" proc-id="RM22W0E___0000C8L00000">
<testtitle>CHECK DISC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the disc is not deformed or cracked.</ptxt>
<figure>
<graphic graphicname="I100152" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>No deformation or cracks on the disc</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000020F2082X_04_0002" fin="false">OK</down>
<right ref="RM0000020F2082X_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000020F2082X_04_0002" proc-id="RM22W0E___0000C8K00000">
<testtitle>DISC CLEANING</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disc cleaning</ptxt>
<figure>
<graphic graphicname="I100151" width="2.775699831in" height="1.771723296in"/>
</figure>
<test2>
<ptxt>If dirt is on the disc surface, wipe it clean with a soft cloth from the inside to the outside in a radial direction.</ptxt>
<atten3>
<ptxt>Do not use a conventional record cleaner or anti-static preservative.</ptxt>
</atten3>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000020F2082X_04_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000020F2082X_04_0009" proc-id="RM22W0E___0000C8N00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000014CZ0DBX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000020F2082X_04_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000020F2082X_04_0010" proc-id="RM22W0E___0000C8O00000">
<testtitle>RECHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same trouble occurs again.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction disappears.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000020F2082X_04_0005" fin="true">OK</down>
<right ref="RM0000020F2082X_04_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000020F2082X_04_0004" proc-id="RM22W0E___0000C8M00000">
<testtitle>REPLACE CD AND RECHECK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the disc and recheck.</ptxt>
<test2>
<ptxt>Replace the disc with a known good one.</ptxt>
</test2>
<test2>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000014CZ0DBX"/>).</ptxt>
</test2>
<test2>
<ptxt>Recheck for DTCs and check if the same trouble occurs again.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction disappears.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000020F2082X_04_0005" fin="true">OK</down>
<right ref="RM0000020F2082X_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000020F2082X_04_0005">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM0000020F2082X_04_0007">
<testtitle>CHANGE DISC</testtitle>
</testgrp>
<testgrp id="RM0000020F2082X_04_0008">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AI400BX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>