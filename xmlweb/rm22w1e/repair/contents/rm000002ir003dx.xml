<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3ML_T00FO" variety="T00FO">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV)</name>
<para id="RM000002IR003DX" category="C" type-id="305O2" name-id="AT9MZ-01" from="201301" to="201308">
<dtccode>P2772</dtccode>
<dtcname>Four Wheel Drive (4WD) Low Switch Circuit Range / Performance</dtcname>
<subpara id="RM000002IR003DX_01" type-id="60" category="03" proc-id="RM22W0E___000086C00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM detects the signal from the transfer L4 position switch.</ptxt>
<ptxt>This DTC indicates that the transfer L4 position switch remains on.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2772</ptxt>
</entry>
<entry valign="middle">
<ptxt>The transfer L4 position switch remains on while the vehicle is running under the following conditions for 1.8 sec. or more (1 trip detection logic).</ptxt>
<ptxt>(a) The output shaft speed is between 1000 and 3000 rpm.</ptxt>
<ptxt>(b) The transfer position switch is in H4.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in transfer L4 position switch circuit</ptxt>
</item>
<item>
<ptxt>Four wheel drive control ECU</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002IR003DX_09" type-id="64" category="03" proc-id="RM22W0E___000086G00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM monitors the transfer-case L4 position switch to determine when the transfer-case L4 gears are engaged. If the transfer-case L4 gears remain engaged under the following conditions, the ECM will conclude that there is a malfunction of the L4 position switch, illuminate the MIL and store the DTC:</ptxt>
<list1 type="unordered">
<item>
<ptxt>L4 switch indicated that the L4 transfer-case gears are engaged.</ptxt>
</item>
<item>
<ptxt>The transfer-case shifter is in the "H4" position.</ptxt>
</item>
<item>
<ptxt>The transfer-case output shaft rpm is between 1000 and 3000 rpm.</ptxt>
</item>
<item>
<ptxt>The specified time period has elapsed.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002IR003DX_06" type-id="32" category="03" proc-id="RM22W0E___000086D00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C179559E02" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002IR003DX_07" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002IR003DX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002IR003DX_08_0001" proc-id="RM22W0E___000086E00000">
<testtitle>CHECK HARNESS AND CONNECTOR (FOUR WHEEL DRIVE CONTROL ECU - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the four wheel drive control ECU connector.</ptxt>
<figure>
<graphic graphicname="C162221E12" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A27-21 (L4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002IR003DX_08_0004" fin="true">OK</down>
<right ref="RM000002IR003DX_08_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002IR003DX_08_0002" proc-id="RM22W0E___000086F00000">
<testtitle>CHECK HARNESS AND CONNECTOR (FOUR WHEEL DRIVE CONTROL ECU - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="C177782E13" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the A27 four wheel drive control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A38-21 (L4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A52-21 (L4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002IR003DX_08_0003" fin="true">OK</down>
<right ref="RM000002IR003DX_08_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002IR003DX_08_0003">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002IR003DX_08_0004">
<testtitle>GO TO TRANSFER SYSTEM INSPECTION<xref label="Seep01" href="RM0000038UF00MX_01_0006"/>
</testtitle>
</testgrp>
<testgrp id="RM000002IR003DX_08_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>