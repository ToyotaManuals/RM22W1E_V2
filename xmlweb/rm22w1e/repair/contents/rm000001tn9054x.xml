<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SD_T00LG" variety="T00LG">
<name>CAN COMMUNICATION SYSTEM (for RHD)</name>
<para id="RM000001TN9054X" category="J" type-id="800CN" name-id="NW48U-02" from="201308">
<dtccode/>
<dtcname>Center Airbag Sensor Communication Stop Mode</dtcname>
<subpara id="RM000001TN9054X_03" type-id="60" category="03" proc-id="RM22W0E___0000DR300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Center Airbag Sensor Communication Stop Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either Condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>"Airbag" is not displayed on the "Bus Check" screen.</ptxt>
</item>
<item>
<ptxt>"Center Airbag Sensor Communication Stop Mode" in "DTC Combination Table" applies.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Power source or inside of center airbag sensor assembly</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly CAN branch wire or connector</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>For vehicles with an airbag system.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000001TN9054X_04" type-id="32" category="03" proc-id="RM22W0E___0000DR400001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C176784E04" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001TN9054X_01" type-id="51" category="05" proc-id="RM22W0E___0000DR200001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Operating the ignition switch, any switches or any doors triggers related ECU and sensor communication with the CAN, which causes resistance variation.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001TN9054X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001TN9054X_05_0010" proc-id="RM22W0E___0000DR800001">
<testtitle>PRECAUTION</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content6>
<res>
<down ref="RM000001TN9054X_05_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TN9054X_05_0007" proc-id="RM22W0E___0000DR600001">
<testtitle>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal before measuring the resistances of the CAN main wire and the CAN branch wire.</ptxt>
<atten2>
<ptxt>For vehicles with an SRS system:</ptxt>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000001TN9054X_05_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TN9054X_05_0008" proc-id="RM22W0E___0000DR700001">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (CENTER AIRBAG SENSOR ASSEMBLY CAN BRANCH WIRE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E47 center airbag sensor assembly connector.</ptxt>
<figure>
<graphic graphicname="C222818E15" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E47-13 (CANH) - E47-22 (CANL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>54 to 69 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Center Airbag Sensor Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001TN9054X_05_0001" fin="false">OK</down>
<right ref="RM000001TN9054X_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TN9054X_05_0001" proc-id="RM22W0E___0000DR500001">
<testtitle>CHECK HARNESS AND CONNECTOR (CENTER AIRBAG SENSOR ASSEMBLY - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
<figure>
<graphic graphicname="C222818E16" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E47-25 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E47-26 (E2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E47-21 (IG2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Center Airbag Sensor Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001TN9054X_05_0005" fin="true">OK</down>
<right ref="RM000001TN9054X_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TN9054X_05_0005">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000002Y2N00ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001TN9054X_05_0009">
<testtitle>REPAIR OR REPLACE CENTER AIRBAG SENSOR ASSEMBLY CAN BRANCH WIRE OR CONNECTOR (CANH, CANL)</testtitle>
</testgrp>
<testgrp id="RM000001TN9054X_05_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>