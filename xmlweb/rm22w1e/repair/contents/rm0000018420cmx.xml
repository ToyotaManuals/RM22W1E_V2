<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R4_T00K7" variety="T00K7">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM0000018420CMX" category="J" type-id="80137" name-id="AV973-03" from="201301" to="201308">
<dtccode/>
<dtcname>Stereo Component Amplifier Communication Error</dtcname>
<subpara id="RM0000018420CMX_01" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000018420CMX_02" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000018420CMX_02_0010" proc-id="RM22W0E___0000CAQ00000">
<testtitle>IDENTIFY COMPONENT SHOWN BY SUB-CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Enter diagnostic mode.</ptxt>
<figure>
<graphic graphicname="E110917E19" width="7.106578999in" height="6.791605969in"/>
</figure>
</test1>
<test1>
<ptxt>Press preset switch "2" to change the mode to "Detailed Information Mode".</ptxt>
</test1>
<test1>
<ptxt>Identify the component shown by the sub-code.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>"P190" set by the radio receiver assembly is shown in the preceding illustration as an example.</ptxt>
</item>
<item>
<ptxt>For details of the DTC display, refer to DTC Check/Clear (See page <xref label="Seep01" href="RM0000014CZ0DBX"/>).</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000018420CMX_02_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018420CMX_02_0001" proc-id="RM22W0E___0000CAM00000">
<testtitle>CHECK POWER SOURCE CIRCUIT OF COMPONENT SHOWN BY SUB-CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the power source circuit of the component shown by the sub-code.</ptxt>
<ptxt>If the power source circuit is operating normally, proceed to the next step.</ptxt>
<table pgwide="1">
<title>Component Table</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Radio receiver assembly (190)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Radio receiver power source circuit (See page <xref label="Seep01" href="RM0000012CI0J4X"/>)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Multi-media interface ECU (388)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Multi-media interface ECU power source circuit (See page <xref label="Seep04" href="RM000003XYW04NX"/>)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000018420CMX_02_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018420CMX_02_0002" proc-id="RM22W0E___0000CAN00000">
<testtitle>INSPECT RADIO RECEIVER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F1 and F55 radio receiver assembly connectors.</ptxt>
<figure>
<graphic graphicname="E194165E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F1-5 (ATX+) - F1-15 (ATX-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F55-9 (TXM+) - F55-10 (TXM-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Component without harness connected</ptxt>
<ptxt>(Radio Receiver Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Component without harness connected</ptxt>
<ptxt>(Radio Receiver Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000018420CMX_02_0003" fin="false">OK</down>
<right ref="RM0000018420CMX_02_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018420CMX_02_0003" proc-id="RM22W0E___0000CAO00000">
<testtitle>CHECK HARNESS AND CONNECTOR (STEREO COMPONENT AMPLIFIER - COMPONENT SHOWN BY SUB-CODE)</testtitle>
<content6 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Start the check from the circuit that is near the component shown by the sub-code first.</ptxt>
</item>
<item>
<ptxt>For details of the connectors, refer to Terminals of ECU (See page <xref label="Seep01" href="RM0000012A70DSX"/>).</ptxt>
</item>
</list1>
</atten4>
<test1>
<ptxt>Referring to the following AVC-LAN wiring diagram, check the AVC-LAN circuit between the stereo component amplifier assembly and the component shown by the sub-code.</ptxt>
<test2>
<ptxt>Disconnect all connectors between the stereo component amplifier assembly and component shown by the sub-code.</ptxt>
</test2>
<test2>
<ptxt>Check for an open or short in the AVC-LAN circuit between the stereo component amplifier assembly and component shown by the sub-code.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There is no open or short circuit.</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="E194159E07" width="7.106578999in" height="5.787629434in"/>
</figure>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000018420CMX_02_0004" fin="false">OK</down>
<right ref="RM0000018420CMX_02_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018420CMX_02_0004" proc-id="RM22W0E___0000CAP00000">
<testtitle>REPLACE COMPONENT SHOWN BY SUB-CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the component shown by the sub-code with a known good one and check if the same problem occurs again.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction disappears.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000018420CMX_02_0005" fin="true">OK</down>
<right ref="RM0000018420CMX_02_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018420CMX_02_0005">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM0000018420CMX_02_0006">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AI400BX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000018420CMX_02_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000018420CMX_02_0008">
<testtitle>REPLACE STEREO COMPONENT AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM000003AI901FX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>