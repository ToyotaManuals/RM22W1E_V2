<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12062_S002E" variety="S002E">
<name>POWER OUTLETS (INT)</name>
<ttl id="12062_S002E_7C3WT_T00PW" variety="T00PW">
<name>REAR POWER OUTLET SOCKET</name>
<para id="RM0000044UU004X" category="A" type-id="30014" name-id="PI07V-04" from="201308">
<name>INSTALLATION</name>
<subpara id="RM0000044UU004X_01" type-id="01" category="01">
<s-1 id="RM0000044UU004X_01_0001" proc-id="RM22W0E___0000HF500001">
<ptxt>INSTALL POWER OUTLET SOCKET COVER NO.2</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Engage the 2 claws and install the power outlet socket cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044UU004X_01_0002" proc-id="RM22W0E___0000HF600001">
<ptxt>INSTALL POWER OUTLET SOCKET ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Engage the claw and install the power outlet socket assembly.</ptxt>
</s2>
<s2>
<ptxt>Engage the 4 claws and install the power outlet socket bezel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044UU004X_01_0016" proc-id="RM22W0E___0000FKP00001">
<ptxt>INSTALL FRONT QUARTER TRIM PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182644" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When installing the front quarter trim panel, operate the reclining adjuster release handle and move the No. 1 rear seat to the position shown in the illustration.</ptxt>
</atten4>
<s2>
<ptxt>w/ Rear No. 2 Seat, except Face to Face Seat Type:</ptxt>
<s3>
<ptxt>w/o Rear Air Conditioning System:</ptxt>
<ptxt>Connect the rear seat lock control lever cable.</ptxt>
</s3>
<s3>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<ptxt>Connect the thermistor connector and rear seat lock control lever cable.</ptxt>
</s3>
<s3>
<ptxt>Attach the 18 clips and 2 claws to install the quarter trim panel.</ptxt>
</s3>
<s3>
<ptxt>Install the clip and bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B186889" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Rear No. 2 Seat or w/ Rear No. 2 Seat, for Face to Face Seat Type:</ptxt>
<s3>
<ptxt>Attach the 18 clips and 2 claws to install the quarter trim panel.</ptxt>
</s3>
<s3>
<ptxt>Install the clip.</ptxt>
<figure>
<graphic graphicname="B190221" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Tonneau Cover:</ptxt>
<s3>
<ptxt>Attach the 18 clips and 2 claws to install the quarter trim panel.</ptxt>
</s3>
<s3>
<ptxt>Install the screw and clip.</ptxt>
<figure>
<graphic graphicname="B186437" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B181691E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>w/ Rear No. 2 Seat, except Face to Face Seat Type:</ptxt>
<ptxt>Install the rear No. 2 seat belt anchor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the vehicle body.</ptxt>
</atten3>
</s2>
<s2>
<figure>
<graphic graphicname="B181689" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the rear No. 1 seat belt anchor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="B181687" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 3 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0017" proc-id="RM22W0E___0000CFP00001">
<ptxt>INSTALL REAR SEAT COVER CAP (except Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190214" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the rear seat cover cap on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 3 claws to install the rear seat cover cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0018" proc-id="RM22W0E___0000CFW00001">
<ptxt>INSTALL NO. 1 TONNEAU COVER HOLDER CAP (w/ Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B186439" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the tonneau cover holder cap on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the tonneau cover holder cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0019" proc-id="RM22W0E___0000CFS00001">
<ptxt>INSTALL REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 6 clips to install the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0020" proc-id="RM22W0E___0000FKQ00001">
<ptxt>INSTALL REAR DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181674" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws and 4 clips to install the scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0021" proc-id="RM22W0E___0000CFR00001">
<ptxt>INSTALL REAR STEP COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181673" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the step cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the step cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0022" proc-id="RM22W0E___0000FYQ00001">
<ptxt>INSTALL REAR NO. 2 SEAT ASSEMBLY (except Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181811E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Temporarily install the seat assembly with the 4 bolts.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Tighten the bolts in the order shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>37</t-value1>
<t-value2>377</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0023" proc-id="RM22W0E___0000FYR00001">
<ptxt>INSTALL REAR NO. 2 SEAT HINGE COVER LH (except Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and 2 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0024" proc-id="RM22W0E___0000FYS00001">
<ptxt>INSTALL REAR SEAT CUSHION HINGE COVER LH (except Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Return the seat to the upright position.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0014">
<ptxt>INSTALL REAR NO. 2 SEAT HEADREST ASSEMBLY LH (w/ Rear Center Seat Headrest)</ptxt>
</s-1>
<s-1 id="RM0000044UU004X_01_0025" proc-id="RM22W0E___0000G8S00000">
<ptxt>INSTALL REAR NO. 2 SEAT ASSEMBLY (for Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182647E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Temporarily install the rear No. 2 seat with the 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Tighten the bolts in the order shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>37</t-value1>
<t-value2>377</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0026" proc-id="RM22W0E___0000G8T00000">
<ptxt>INSTALL REAR SIDE SEAT LEG PROTECTOR LH (for Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182655" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the protector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044UU004X_01_0027" proc-id="RM22W0E___0000CFX00001">
<ptxt>INSTALL TONNEAU COVER ASSEMBLY (w/ Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the tonneau cover assembly.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>