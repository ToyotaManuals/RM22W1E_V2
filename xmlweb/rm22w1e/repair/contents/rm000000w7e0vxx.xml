<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MT_T00FW" variety="T00FW">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1UR-FE)</name>
<para id="RM000000W7E0VXX" category="U" type-id="303FJ" name-id="AT01QX-230" from="201308">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM000000W7E0VXX_z0" proc-id="RM22W0E___00008LV00001">
<content5 releasenbr="1">
<step1>
<ptxt>READ DATA LIST</ptxt>
<atten4>
<ptxt>Using the GTS to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the GTS on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the GTS, read the Data List.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.78in"/>
<colspec colname="COL3" colwidth="1.78in"/>
<colspec colname="COL4" colwidth="1.74in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>N Range Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever N status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Shift lever in N</ptxt>
</item>
<item>
<ptxt>OFF: Shift lever not in N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Neutral Position SW Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In P or N: ON</ptxt>
</item>
<item>
<ptxt>Not in P or N: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the GTS differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep06" href="RM000000W8412GX"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Transfer L4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transfer status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Transfer in L4</ptxt>
</item>
<item>
<ptxt>OFF: Transfer not in L4</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Depressed: ON</ptxt>
</item>
<item>
<ptxt>Released: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SPD (NT)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Input shaft speed/</ptxt>
<ptxt>Min.: 0 rpm</ptxt>
<ptxt>Max.: 12750 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>ON (after warming up engine): Input turbine speed (NT) equal to engine speed</ptxt>
</item>
<item>
<ptxt>OFF (idling with shift lever in N): Input turbine speed (NT) nearly equal to engine speed</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Data is displayed in increments of 50 rpm.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SPD (SP2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output shaft speed/</ptxt>
<ptxt>Min.: 0 km/h (0 mph)</ptxt>
<ptxt>Max.: 255 km/h (158 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle stopped: 0 km/h (0 mph) (output shaft speed is equal to vehicle speed)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (P Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In P: ON</ptxt>
</item>
<item>
<ptxt>Not in P: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the GTS differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep08" href="RM000000W8412GX"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Pattern Switch (PWR/M)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern switch (PWR) status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Pattern switch (PWR) is pushed</ptxt>
</item>
<item>
<ptxt>OFF: Pattern switch (PWR) is not pushed</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (R Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In R: ON</ptxt>
</item>
<item>
<ptxt>Not in R: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the GTS differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep07" href="RM000000W8412GX"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (N Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In N: ON</ptxt>
</item>
<item>
<ptxt>Not in N: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the GTS differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep09" href="RM000000W8412GX"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (N, P Range) Supported</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of PNP switch (N or P) support/</ptxt>
<ptxt>Supp or Unsupp</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (N, P Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Shift lever in P or N</ptxt>
</item>
<item>
<ptxt>OFF: Shift lever not in P or N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Sports Shift Up SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sport shift up switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Shift lever held in "+" (up-shift)</ptxt>
</item>
<item>
<ptxt>OFF: Shift lever not in "+" (up-shift)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Sports Shift Down SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sport shift down switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Shift lever held in "-" (down-shift)</ptxt>
</item>
<item>
<ptxt>OFF: Shift lever not in "-" (down-shift)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Sports Mode Selection SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sport mode select switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>ON: In S, "+" and "-"</ptxt>
</item>
<item>
<ptxt>OFF: Not in S, "+" and "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (D Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In D, S, "+" and "-": ON</ptxt>
</item>
<item>
<ptxt>Not in D, S, "+" and "-": OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the GTS differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep10" href="RM000000W8412GX"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A/T Oil Temperature 1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 ATF temperature sensor value/</ptxt>
<ptxt>Min.: -40°C (-40°F)</ptxt>
<ptxt>Max.: 215°C (419°F)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>After stall test: Approximately 80°C (176°F)</ptxt>
</item>
<item>
<ptxt>Equal to ambient temperature while engine is cold</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>If the value is -40°C (-40°F) or 215°C (419°F), the No. 1 ATF temperature sensor circuit is open or shorted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A/T Oil Temperature 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 ATF temperature sensor value/</ptxt>
<ptxt>Min.: -40°C (-40°F)</ptxt>
<ptxt>Max.: 215°C (419°F)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>After stall test: Approximately 80°C (176°F)</ptxt>
</item>
<item>
<ptxt>Equal to ambient temperature while engine is cold</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>If the value is -40°C (-40°F) or 215°C (419°F), the No. 2 ATF temperature sensor circuit is open or shorted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lock Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Operating: ON</ptxt>
</item>
<item>
<ptxt>Not operating: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM gear shift command/</ptxt>
<ptxt>1st, 2nd, 3rd, 4th, 5th or 6th</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>In D: 1st, 2nd, 3rd, 4th, 5th or 6th</ptxt>
</item>
<item>
<ptxt>In S: 1st, 2nd, 3rd, 4th, 5th or 6th</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SLT Solenoid Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift solenoid SLT status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal is depressed: OFF</ptxt>
</item>
<item>
<ptxt>Accelerator pedal is released: ON</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SLU Solenoid Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift solenoid SLU status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift solenoid SLU is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Operating: ON</ptxt>
</item>
<item>
<ptxt>Not operating: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Snow or 2nd Start Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>2nd Start mode status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: 2nd Start mode on</ptxt>
</item>
<item>
<ptxt>OFF: 2nd Start mode off</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AT Fluid</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flex lock-up judder judgment/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Judder judgment occurs</ptxt>
</item>
<item>
<ptxt>OFF: Judder judgment does not occur</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>PERFORM ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the GTS to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the GTS on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the GTS, perform the Active Test.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SLU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid SLU</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SLT)*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid SLT and raise line pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>OFF: Line pressure up (when Active Test "Activate the Solenoid (SLT)" is performed, ECM commands SLT solenoid to turn OFF)</ptxt>
</item>
<item>
<ptxt>ON: No action (normal operation)</ptxt>
</item>
</list1>
</atten4>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Vehicle stopped</ptxt>
</item>
<item>
<ptxt>Engine idling</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (S1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (S2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid S2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (S3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid S3</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (S4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid S4</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Lock Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control shift solenoid SLU to set automatic transmission to lock-up</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Possible to check shift solenoid valve SLU operation.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Throttle valve opening angle: Less than 35%</ptxt>
</item>
<item>
<ptxt>Vehicle speed: 80 km/h (50 mph) or more</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Control the Shift Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valves and set each shift position by yourself</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Press "→" button: Shift up</ptxt>
</item>
<item>
<ptxt>Press "←" button: Shift down</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Possible to check operation of the shift solenoid valves.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<ptxt>50 km/h (30 mph) or less</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid SR</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SL1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid SL1</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SL2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid SL2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: Activate the Solenoid (SLT) in the Active Test is performed to check the line pressure changes by connecting SST to the automatic transmission, which is used in the Hydraulic Test (See page <xref label="Seep12" href="RM000000W7B0PQX"/>) as well. Please note that the pressure values in the Active Test and Hydraulic Test are different.</ptxt>
</atten4>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>