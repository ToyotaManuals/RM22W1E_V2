<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3V7_T00OA" variety="T00OA">
<name>CLIMATE CONTROL SEAT SYSTEM</name>
<para id="RM000001E1Q01PX" category="U" type-id="3001G" name-id="SE2M3-06" from="201301">
<name>TERMINALS OF ECU</name>
<subpara id="RM000001E1Q01PX_z0" proc-id="RM22W0E___0000GF400000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK SEAT CLIMATE CONTROL ECU LH</ptxt>
<figure>
<graphic graphicname="B308687E04" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the c35 seat climate control ECU LH connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>c35-12 (B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c35-13 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c35-15 (V5) - c35-6 (VS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - G</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front seat heater switch (LH side) signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front seat heater switch (LH side) (blower on HI)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 100 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c35-15 (V5) - c35-17 (VG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front seat heater switch (LH side) signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 to 6 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c35-3 (GND) -Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Reconnect the c35 seat climate control ECU LH connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>c35-15 (V5) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front seat heater switch (LH side) power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.7 to 5.2 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>c35-26 (IND) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Seat heater switch indicator light (LH side) illumination signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG), front LH blower fan operating (seat heater switch indicator light [LH side] illuminated)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG), front LH blower fan not operating (seat heater switch indicator light [LH side] not illuminated)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>c35-24 (BCS) - c35-1 (BFC-)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L - B</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Front climate control blower LH drive signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG), front seat heater switch (LH side) off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG), front seat heater switch (LH side) on (blower on HI)</ptxt>
</entry>
<entry valign="middle">
<ptxt>12.6 to 13.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>CHECK SEAT CLIMATE CONTROL ECU RH</ptxt>
<figure>
<graphic graphicname="B308687E03" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the c32 seat climate control ECU RH connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>c32-12 (B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c32-13 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c32-15 (V5) - c32-6 (VS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - W</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front seat heater switch (RH side) signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front seat heater switch (RH side) (blower on HI)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 100 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c32-15 (V5) - c32-17 (VG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front seat heater switch (RH side) signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 to 6 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c32-3 (GND) -Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Reconnect the c32 seat climate control ECU RH connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>c32-15 (V5) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front seat heater switch (RH side) power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.7 to 5.2 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>c32-26 (IND) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Seat heater switch indicator light (RH side) illumination signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG), front RH blower fan operating (seat heater switch indicator light [RH side] illuminated)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG), front RH blower fan not operating (seat heater switch indicator light [RH side] not illuminated)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>c32-24 (BCS) - c32-1 (BFC-)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L - B</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Front climate control blower RH drive signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG), front seat heater switch (RH side) off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG), front seat heater switch (RH side) on (blower on HI)</ptxt>
</entry>
<entry valign="middle">
<ptxt>12.6 to 13.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>