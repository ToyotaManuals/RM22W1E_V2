<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000188708YX" category="C" type-id="3038Y" name-id="ESRAO-04" from="201308">
<dtccode>P0488</dtccode>
<dtcname>Exhaust Gas Recirculation Throttle Position Control Range / Performance</dtcname>
<dtccode>P213B</dtccode>
<dtcname>Exhaust Gas Recirculation Throttle Position Control (Range / Performance)</dtcname>
<subpara id="RM00000188708YX_01" type-id="60" category="03" proc-id="RM22W0E___00002SF00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM opens and closes the throttle valve using a rotary solenoid type actuator. Due to the opening and closing of the valve, the exhaust gas recirculation volume can be properly controlled. Also, engine vibration and noise will be reduced by closing the valve when the engine is stopped.</ptxt>
<table pgwide="1">
<title>P0488</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Over a period of 10 seconds or more, slowly raise engine speed to 3000 rpm and lower it</ptxt>
</entry>
<entry>
<ptxt>The throttle motor activation duty is less than 10% or 90% or more for 30 seconds (1 trip detection logic).</ptxt>
<atten4>
<ptxt>The throttle valve commanded duty can be read using the tester as the Data List item Throttle Motor Duty.</ptxt>
</atten4>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Throttle valve (for Bank 1) stuck</ptxt>
</item>
<item>
<ptxt>Throttle valve (for Bank 1) does not move smoothly</ptxt>
</item>
<item>
<ptxt>Open or short in throttle control motor (for Bank 1) circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P213B</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Over a period of 10 seconds or more, slowly raise engine speed to 3000 rpm and lower it</ptxt>
</entry>
<entry>
<ptxt>The throttle motor activation duty is less than 10% or 90% or more for 30 seconds (1 trip detection logic).</ptxt>
<atten4>
<ptxt>The throttle valve commanded duty can be read using the tester as the Data List item Throttle Motor Duty.</ptxt>
</atten4>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Throttle valve (for Bank 2) stuck</ptxt>
</item>
<item>
<ptxt>Throttle valve (for Bank 2) does not move smoothly</ptxt>
</item>
<item>
<ptxt>Open or short in throttle control motor (for Bank 2) circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>DTC No.</ptxt>
</entry>
<entry>
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0488</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Throttle Motor Duty #1</ptxt>
</item>
<item>
<ptxt>Throttle Motor Duty #2</ptxt>
</item>
<item>
<ptxt>Actual Throttle Position</ptxt>
</item>
<item>
<ptxt>Actual Throttle Position #2</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P213B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Reference</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Throttle Valve Position</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Moment when accelerator pedal is depressed further or released at 3000 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle valve opening angle varies smoothly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000188708YX_02" type-id="64" category="03" proc-id="RM22W0E___00002SG00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM opens and closes the throttle valve by adjusting the current flowing to the rotary solenoid with a duty ratio. If the throttle valve does not move smoothly or is stuck, the duty ratio used during valve movement control increases or decreases greatly. The ECM will determine that the throttle valve is malfunctioning and illuminate the MIL.</ptxt>
<figure>
<graphic graphicname="A128641E06" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000188708YX_03" type-id="32" category="03" proc-id="RM22W0E___00002SH00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A165769E07" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000188708YX_04" type-id="51" category="05" proc-id="RM22W0E___00002SI00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08YX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06NX"/>).</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Although the DTC titles say Exhaust Gas Recirculation Throttle Position Control Range / Performance, these DTCs relate to the throttle valve</ptxt>
</item>
<item>
<ptxt>After warming up the engine, DTC P0488 and P213B can be stored when 1 second or more passes after quickly accelerating the engine from idling.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188708YX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000188708YX_05_0012" proc-id="RM22W0E___00002SN00001">
<testtitle>CHECK HARNESS AND CONNECTOR (THROTTLE CONTROL MOTOR POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle control motor connector.</ptxt>
<figure>
<graphic graphicname="A165767E06" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<title>for Bank 1</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-2 (VCR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C86-2 (VCR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000188708YX_05_0015" fin="false">OK</down>
<right ref="RM00000188708YX_05_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188708YX_05_0015" proc-id="RM22W0E___00002SP00001">
<testtitle>CHECK HARNESS AND CONNECTOR (GROUND CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle control motor connector.</ptxt>
<figure>
<graphic graphicname="A165767E08" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for Bank 1</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-3 (E2R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C86-3 (E2R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000188708YX_05_0004" fin="false">OK</down>
<right ref="RM00000188708YX_05_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188708YX_05_0004" proc-id="RM22W0E___00002SJ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (THROTTLE CONTROL MOTOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle control motor connector.</ptxt>
<figure>
<graphic graphicname="A165778E04" width="2.775699831in" height="5.787629434in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for Bank 1 (LHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-1 (VTAR) - C45-42 (LUSL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2 (LHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C86-1 (VTAR) - C45-84 (LUS2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 1 (RHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-1 (VTAR) - C46-42 (LUSL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2 (RHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C86-1 (VTAR) - C46-84 (LUS2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for Bank 1 (LHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-1 (VTAR) or C45-42 (LUSL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2 (LHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C86-1 (VTAR) or C45-84 (LUS2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 1 (RHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-1 (VTAR) or C46-42 (LUSL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for Bank 2 (RHD)</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C86-1 (VTAR) or C46-84 (LUS2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000188708YX_05_0005" fin="false">OK</down>
<right ref="RM00000188708YX_05_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188708YX_05_0005" proc-id="RM22W0E___00002SK00001">
<testtitle>REPLACE DIESEL THROTTLE BODY ASSEMBLY (for Bank 1 or Bank 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the diesel throttle body assembly (for Bank 1 or Bank 2) (See page <xref label="Seep01" href="RM00000316V006X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188708YX_05_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188708YX_05_0010" proc-id="RM22W0E___00002SM00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK18AX"/>).</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Over a period of 10 seconds or more, slowly raise the engine speed to 3000 rpm and lower it.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0488 and/or P213B is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000188708YX_05_0008" fin="false">A</down>
<right ref="RM00000188708YX_05_0014" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000188708YX_05_0008" proc-id="RM22W0E___00002SL00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329203AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188708YX_05_0014" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188708YX_05_0013">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (THROTTLE CONTROL MOTOR - BATTERY)</testtitle>
<res>
<right ref="RM00000188708YX_05_0014" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188708YX_05_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<res>
<down ref="RM00000188708YX_05_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188708YX_05_0014" proc-id="RM22W0E___00002SO00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK18AX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Over a period of 10 seconds or more, slowly raise the engine speed to 3000 rpm and lower it.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the tester to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Powertrain / Engine / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P0488 and/or P213B.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or UNKNOWN, over a period of 10 seconds or more, slowly raise the engine speed to 3000 rpm again and then idle the engine for 5 minutes.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000188708YX_05_0011" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188708YX_05_0011">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>