<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000I" variety="S000I">
<name>1VD-FTV FUEL</name>
<ttl id="12008_S000I_7C3H9_T00AC" variety="T00AC">
<name>FUEL SUPPLY PUMP</name>
<para id="RM0000031EI007X" category="A" type-id="30014" name-id="FU93S-02" from="201308">
<name>INSTALLATION</name>
<subpara id="RM0000031EI007X_02" type-id="11" category="10" proc-id="RM22W0E___000069L00001">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injectors (including shuffling the injectors between the cylinders), common rail, cylinder head or intake manifold, it is necessary to replace the injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>w/ DPF:</ptxt>
<ptxt>When fuel lines are disconnected, air may enter the fuel lines, leading to engine starting trouble. Therefore, perform forced regeneration and bleed the air from the fuel lines (See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>).</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM0000031EI007X_01" type-id="01" category="01">
<s-1 id="RM0000031EI007X_01_0001" proc-id="RM22W0E___000069E00001">
<ptxt>INSTALL FUEL SUPPLY PUMP</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Do not push the supply pump drive gear.</ptxt>
</atten3>
<s2>
<ptxt>Install a new O-ring to the supply pump.</ptxt>
<figure>
<graphic graphicname="A164258E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>New O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Apply a light coat of engine oil to the O-ring.</ptxt>
</s2>
<s2>
<ptxt>Using an 8 mm x 1.25 pitch tap, remove the adhesive from the timing gear case bolt hole.</ptxt>
<figure>
<graphic graphicname="A164817" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Apply adhesive to 2 or more threads as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A165070E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1344, Three Bond 1344 or equivalent</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adhesive</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Align the set key on the drive shaft with the groove of the supply pump drive gear.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Groove</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Set Key</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="A164256E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not hold the supply pump by the parts indicated by the arrows in the illustration, as this may result in deformation, damage and fuel leakage.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Temporarily install the fuel supply pump with the 2 nuts.</ptxt>
</s2>
<s2>
<ptxt>Using a 6 mm hexagon wrench, install the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a wrench, hold the crankshaft.</ptxt>
</s2>
<s2>
<ptxt>Install the set nut.</ptxt>
<torque>
<torqueitem>
<t-value1>68</t-value1>
<t-value2>693</t-value2>
<t-value4>50</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Turn the crankshaft 2 times, and check that there are no problems.</ptxt>
</s2>
<s2>
<ptxt>Using a dial indicator, check the thrust clearance of the supply pump drive shaft while moving the pump drive shaft gear back and forth.</ptxt>
<figure>
<graphic graphicname="A164255" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard thrust clearance</title>
<specitem>
<ptxt>0.05 to 0.30 mm (0.00197 to 0.0118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the clearance is not within the specified range, disassemble and reassemble the supply pump and pump drive shaft gear. Then repeat the step above.</ptxt>
</s2>
<s2>
<ptxt>Connect the fuel temperature sensor connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031EI007X_01_0002" proc-id="RM22W0E___000069F00001">
<ptxt>INSTALL NO. 1 FUEL PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 2 new gaskets and the No. 1 fuel pipe with the union bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>17</t-value1>
<t-value2>175</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031EI007X_01_0103" proc-id="RM22W0E___00005IV00001">
<ptxt>INSTALL TIMING CHAIN COVER PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the cover plate with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>9.1</t-value1>
<t-value2>93</t-value2>
<t-value3>81</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031EI007X_01_0106" proc-id="RM22W0E___000059B00000">
<ptxt>INSTALL TIMING GEAR COVER INSULATOR (w/ Intercooler)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Viscous Heater:</ptxt>
<ptxt>Install the timing gear cover insulator with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>w/ Viscous Heater:</ptxt>
<ptxt>Install the timing gear cover insulator.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031EI007X_01_0111" proc-id="RM22W0E___000054U00001">
<ptxt>INSTALL FUEL TUBE SUB-ASSEMBLY (w/ DPF)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 clamps to install the fuel tube.</ptxt>
</s2>
<s2>
<ptxt>Connect the 3 fuel tube connectors (See page <xref label="Seep01" href="RM0000028RU03ZX"/>).</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031EI007X_01_0109">
<ptxt>INSTALL NO. 1 WATER BY-PASS PIPE</ptxt>
</s-1>
<s-1 id="RM0000031EI007X_01_0054" proc-id="RM22W0E___000069H00001">
<ptxt>INSTALL FUEL COOLER ASSEMBLY (w/o DPF)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000031FI00SX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000031EI007X_01_0005" proc-id="RM22W0E___000069G00001">
<ptxt>INSTALL THERMOSTAT</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000144C03QX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000031EI007X_01_0110" proc-id="RM22W0E___000069K00001">
<ptxt>INSTALL EGR VALVE ASSEMBLY WITH EGR COOLER (w/ EGR System)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000031JK00BX_01_0008"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000031EI007X_01_0107" proc-id="RM22W0E___000069J00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
<s2>
<ptxt>Connect the cables to the negative (-) main battery and sub-battery terminals.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031EI007X_01_0105" proc-id="RM22W0E___000069I00001">
<ptxt>PERFORM FUEL SUPPLY PUMP INITIALIZATION</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000000TIN06NX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>