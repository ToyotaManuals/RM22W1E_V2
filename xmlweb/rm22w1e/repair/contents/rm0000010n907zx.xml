<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0019" variety="S0019">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0019_7C3M5_T00F8" variety="T00F8">
<name>PARK / NEUTRAL POSITION SWITCH</name>
<para id="RM0000010N907ZX" category="A" type-id="30014" name-id="AT8T6-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000010N907ZX_01" type-id="01" category="01">
<s-1 id="RM0000010N907ZX_01_0001" proc-id="RM22W0E___00007EF00000">
<ptxt>INSTALL PARK/NEUTRAL POSITION SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Make sure that the manual valve lever shaft has not been rotated prior to installing the park/neutral position switch as the detent spring may become detached from the manual valve lever shaft.</ptxt>
</atten4>
<s2>
<ptxt>Install the switch to the manual valve shaft.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install the bolt.</ptxt>
<figure>
<graphic graphicname="C155872" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install a new lock washer and the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>6.9</t-value1>
<t-value2>70</t-value2>
<t-value3>61</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="C155871E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Temporarily install the control shaft lever RH.</ptxt>
<figure>
<graphic graphicname="C170829" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the control shaft lever RH counterclockwise until it stops, and then turn it clockwise 2 notches to set it to the N position.</ptxt>
<figure>
<graphic graphicname="C172409E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the control shaft lever RH.</ptxt>
</s2>
<s2>
<ptxt>Align the groove with the neutral basic line.</ptxt>
<figure>
<graphic graphicname="D025795E10" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Hold the switch in position and tighten the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a screwdriver, bend the tabs of the lock washer.</ptxt>
<figure>
<graphic graphicname="C155870" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the transmission control rod bush and collar to the transmission control shaft lever RH.</ptxt>
</s2>
<s2>
<ptxt>Install the transmission control shaft lever RH with the spring washer and nut.</ptxt>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>163</t-value2>
<t-value4>12</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="C170829" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the switch connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000010N907ZX_01_0012" proc-id="RM22W0E___00007EH00000">
<ptxt>CONNECT FLOOR SHIFT GEAR SHIFTING ROD SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C170828" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the shifting rod to the transmission control shaft lever RH with the pin.</ptxt>
</s2>
<s2>
<ptxt>Install a new clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000010N907ZX_01_0016" proc-id="RM22W0E___00007EI00000">
<ptxt>INSPECT SHIFT LEVER POSITION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>When moving the shift lever from P to R with the engine switch on (IG) and the brake pedal depressed, make sure that it moves smoothly and correctly into position.</ptxt>
</s2>
<s2>
<ptxt>Check that the shift lever does not stop when moving the shift lever from R to P, and check that the shift lever does not stick when moving the shift lever from D to S.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and make sure that the vehicle moves forward after moving the shift lever from N to D and moves rearward after moving the shift lever to R.</ptxt>
<ptxt>If the operation cannot be performed as specified, inspect the park/neutral position switch assembly and check the shift lever assembly installation condition.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000010N907ZX_01_0011" proc-id="RM22W0E___00007EG00000">
<ptxt>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000010ND06JX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>