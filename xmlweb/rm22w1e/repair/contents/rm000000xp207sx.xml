<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3ML_T00FO" variety="T00FO">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV)</name>
<para id="RM000000XP207SX" category="J" type-id="800PO" name-id="AT31D-06" from="201308">
<dtccode/>
<dtcname>Pattern Select Switch Power Mode Circuit</dtcname>
<subpara id="RM000000XP207SX_01" type-id="60" category="03" proc-id="RM22W0E___000086O00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM memory contains the programs for the normal and power shift patterns and lock-up pattern.</ptxt>
<ptxt>By following the programs corresponding to the signals from the pattern select switch, the park/neutral position switch and other various sensors, the ECM switches the solenoid valves on and off, and controls the transmission gear change and the lock-up clutch operation.</ptxt>
</content5>
</subpara>
<subpara id="RM000000XP207SX_02" type-id="32" category="03" proc-id="RM22W0E___000086P00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C179561E11" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XP207SX_03" type-id="51" category="05" proc-id="RM22W0E___000086Q00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<ptxt>When the pattern select (PWR) switch is pushed, switch contact is made and power mode is selected. To cancel power mode, push the pattern select (PWR) switch once again.</ptxt>
</content5>
</subpara>
<subpara id="RM000000XP207SX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XP207SX_04_0001" proc-id="RM22W0E___000086R00001">
<testtitle>INSPECT PATTERN SELECT (PWR) SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the suspension control switch (See page <xref label="Seep01" href="RM000003C4A018X"/>).</ptxt>
<figure>
<graphic graphicname="C238423E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>w/ CRAWL</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>3 (P) - 16 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (PWR) switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 (P) - 16 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (PWR) switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>w/o CRAWL</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>16 (P) - 20 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (PWR) switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>16 (P) - 20 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (PWR) switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ CRAWL</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o CRAWL</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Suspension Control Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000XP207SX_04_0002" fin="false">OK</down>
<right ref="RM000000XP207SX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XP207SX_04_0002" proc-id="RM22W0E___000086S00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PATTERN SELECT (PWR) SWITCH - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the suspension control switch connector.</ptxt>
<figure>
<graphic graphicname="C227138E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>w/ CRAWL</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>V4-16 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>w/o CRAWL</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>V4-20 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ CRAWL</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o CRAWL</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Suspension Control Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000XP207SX_04_0003" fin="false">OK</down>
<right ref="RM000000XP207SX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XP207SX_04_0003" proc-id="RM22W0E___000086T00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PATTERN SELECT (PWR) SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C197708E72" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A38-47 (PWR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (PWR) switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-47 (PWR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (PWR) switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A52-47 (PWR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (PWR) switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A52-47 (PWR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select (PWR) switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000XP207SX_04_0004" fin="true">OK</down>
<right ref="RM000000XP207SX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XP207SX_04_0004">
<testtitle>PROCEED TO NEXT CIRCUIT INSPECTION SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000W730ZLX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XP207SX_04_0005">
<testtitle>REPLACE PATTERN SELECT (PWR) SWITCH (SUSPENSION CONTROL SWITCH)<xref label="Seep01" href="RM000003C4A018X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XP207SX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XP207SX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>