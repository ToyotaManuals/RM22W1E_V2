<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12054_S0027" variety="S0027">
<name>METER / GAUGE / DISPLAY</name>
<ttl id="12054_S0027_7C3TS_T00MV" variety="T00MV">
<name>LIGHT CONTROL RHEOSTAT</name>
<para id="RM0000039O1004X" category="A" type-id="80001" name-id="ME48J-03" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000039O1004X_01" type-id="01" category="01">
<s-1 id="RM0000039O1004X_01_0001" proc-id="RM22W0E___0000F2T00001">
<ptxt>DISABLE AUTO TILT AWAY FUNCTION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disable the Autoaway/Return function by changing the customize parameter (See page <xref label="Seep01" href="RM000000UZ00A8X"/>). </ptxt>
<atten3>
<ptxt>Record the current customize parameter setting (whether the Autoaway/Return function is enabled or disabled) in order to restore the current setting after finishing the operation.</ptxt>
</atten3>
<atten4>
<ptxt>Performing the above operation causes the Autoaway/Return function to be disabled when the engine switch is turned off.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Turn the engine switch on (IG). Operate the tilt and telescopic switch to fully extend and lower the steering column.</ptxt>
</s2>
<s2>
<ptxt>Turn the engine switch off.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039O1004X_01_0009" proc-id="RM22W0E___0000BB400001">
<ptxt>REMOVE NO. 1 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154744E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 3 claws and remove the No. 1 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039O1004X_01_0010" proc-id="RM22W0E___000072V00000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E155426E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the panel cushion.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039O1004X_01_0014" proc-id="RM22W0E___000072W00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip.</ptxt>
<figure>
<graphic graphicname="B181910" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 8 claws.</ptxt>
</s2>
<s2>
<ptxt>Remove the panel pad and disconnect the connectors and 2 clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039O1004X_01_0012" proc-id="RM22W0E___0000BB500001">
<ptxt>REMOVE NO. 2 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws and remove the No. 2 instrument cluster finish panel garnish.</ptxt>
<figure>
<graphic graphicname="B291251E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Entry and Start System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000039O1004X_01_0015" proc-id="RM22W0E___0000BB600001">
<ptxt>REMOVE INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180017" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 9 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the instrument cluster finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039O1004X_01_0008" proc-id="RM22W0E___0000F2U00001">
<ptxt>REMOVE LIGHT CONTROL RHEOSTAT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the rheostat.</ptxt>
<figure>
<graphic graphicname="E154735" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>