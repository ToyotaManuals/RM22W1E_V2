<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000PDV0QXX" category="C" type-id="302GR" name-id="ESDQE-08" from="201308">
<dtccode>P0016</dtccode>
<dtcname>Crankshaft Position - Camshaft Position Correlation (Bank 1 Sensor A)</dtcname>
<dtccode>P0017</dtccode>
<dtcname>Crankshaft Position - Camshaft Position Correlation  (Bank 1 Sensor B)</dtcname>
<dtccode>P0018</dtccode>
<dtcname>Crankshaft Position - Camshaft Position Correlation (Bank 2 Sensor A)</dtcname>
<dtccode>P0019</dtccode>
<dtcname>Crankshaft Position - Camshaft Position Correlation  (Bank 2 Sensor B)</dtcname>
<subpara id="RM000000PDV0QXX_01" type-id="60" category="03" proc-id="RM22W0E___000025J00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>In the VVT (Variable Valve Timing) system, the appropriate intake and exhaust valve open and close timing is controlled by the ECM. The ECM performs intake and exhaust valve control by performing the following: 1) controlling the camshaft and camshaft timing oil control valve, and operating the camshaft timing gear; and 2) changing the relative positions of the camshaft and crankshaft.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0016</ptxt>
</entry>
<entry valign="middle">
<ptxt>Deviation in the crankshaft position sensor signal and VVT sensor (for Intake side of Bank 1) signal (2 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Mechanical system (Timing chain has jumped tooth or chain stretched)</ptxt>
</item>
<item>
<ptxt>Camshaft oil control valve (OCV) (for intake side of bank 1)</ptxt>
</item>
<item>
<ptxt>Camshaft timing gear assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0017</ptxt>
</entry>
<entry valign="middle">
<ptxt>Deviation in the crankshaft position sensor signal and VVT sensor (for Exhaust side of Bank 1) signal (2 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Mechanical system (Timing chain has jumped tooth or chain stretched)</ptxt>
</item>
<item>
<ptxt>OCV (for exhaust side of bank 1)</ptxt>
</item>
<item>
<ptxt>Camshaft timing exhaust gear assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0018</ptxt>
</entry>
<entry valign="middle">
<ptxt>Deviation in the crankshaft position sensor signal and VVT sensor (for Intake side of Bank 2) signal (2 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Mechanical system (Timing chain has jumped tooth or chain stretched)</ptxt>
</item>
<item>
<ptxt>OCV (for intake side of bank 2)</ptxt>
</item>
<item>
<ptxt>Camshaft timing gear assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0019</ptxt>
</entry>
<entry valign="middle">
<ptxt>Deviation in the crankshaft position sensor signal and VVT sensor (for Exhaust side of Bank 2) signal (2 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Mechanical system (Timing chain has jumped tooth or chain stretched)</ptxt>
</item>
<item>
<ptxt>OCV (for exhaust side of bank 2)</ptxt>
</item>
<item>
<ptxt>Camshaft timing exhaust gear assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000PDV0QXX_02" type-id="64" category="03" proc-id="RM22W0E___000025K00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>To monitor the correlation of the intake camshaft position and crankshaft position, the ECM checks the VVT learning value while the engine is idling. The VVT learning value is calibrated based on the camshaft position and crankshaft position. The intake valve timing is set to the most retarded angle while the engine is idling. If the VVT learning value is out of the specified range in consecutive driving cycles, the ECM illuminates the MIL and stores DTC P0016 (Bank 1) or P0018 (Bank 2).</ptxt>
<ptxt>To monitor the correlation of the exhaust camshaft position and crankshaft position, the ECM checks the VVT learning value while the engine is idling. The VVT learning value is calibrated based on the camshaft position and crankshaft position. The exhaust valve timing is set to the most advanced angle while the engine is idling. If the VVT learning value is out of the specified range in consecutive driving cycles, the ECM illuminates the MIL and stores DTC P0017 (Bank 1) or P0019 (Bank 2).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PDV0QXX_06" type-id="32" category="03" proc-id="RM22W0E___000025L00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0335 (See page <xref label="Seep01" href="RM000000TCW0XTX_07"/>).</ptxt>
<ptxt>Refer to DTC P0340 (See page <xref label="Seep02" href="RM000000WBZ0OTX_07"/>).</ptxt>
<ptxt>Refer to DTC P0365 (See page <xref label="Seep03" href="RM000000YTB0JEX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PDV0QXX_07" type-id="51" category="05" proc-id="RM22W0E___000025M00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PDV0QXX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PDV0QXX_09_0007" proc-id="RM22W0E___000025N00001">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0016, P0017, P0018 OR P0019)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0016, P0017, P0018 or P0019 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0016, P0017, P0018 or P0019 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PDV0QXX_09_0008" fin="false">A</down>
<right ref="RM000000PDV0QXX_09_0014" fin="true">B</right>

</res>
</testgrp>
<testgrp id="RM000000PDV0QXX_09_0008" proc-id="RM22W0E___000025O00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (OPERATE OCV)</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>If the VVT system can be operated through the Active Test, it can be assumed that the VVT system is operating normally.</ptxt>
</atten4>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the A/C on.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on</ptxt>
</test1>
<test1>
<ptxt>Warm up the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the VVT Linear (Bank 1) or Control the VVT Linear (Bank 2) or Control the VVT Exhaust Linear (Bank 1) or Control the Exhaust VVT Linear (Bank 2) / All Data / VVT Change Angle #1, VVT Change Angle #2, VVT Ex Chg Angle #1, VVT Ex Chg Angle #2.</ptxt>
</test1>
<test1>
<ptxt>Perform the Active Test. Check that the displacement angle varies.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Displacement angle varies.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDV0QXX_09_0009" fin="false">OK</down>
<right ref="RM000000PDV0QXX_09_0017" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0QXX_09_0009" proc-id="RM22W0E___000025P00001">
<testtitle>ADJUST VALVE TIMING</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Adjust the valve timing (See page <xref label="Seep01" href="RM0000030XP01QX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000PDV0QXX_09_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0QXX_09_0017" proc-id="RM22W0E___000025F00001">
<testtitle>INSPECT CAMSHAFT OIL CONTROL VALVE (FOR INTAKE SIDE OR EXHAUST SIDE)
</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A160397E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Remove the OCV.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6.9 to 7.9 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<figure>
<graphic graphicname="A097066E17" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Check the valve operation.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>No battery voltage applied to terminals 1 and 2 → Battery voltage applied to terminals 1 and 2</ptxt>
</entry>
<entry align="center">
<ptxt>Valve moves quickly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000000PDV0QXX_09_0011" fin="false">OK</down>
<right ref="RM000000PDV0QXX_09_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0QXX_09_0011" proc-id="RM22W0E___000025Q00001">
<testtitle>REPLACE CAMSHAFT TIMING GEAR OR CAMSHAFT TIMING EXHAUST GEAR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the camshaft timing gear (See page <xref label="Seep01" href="RM0000030XO01QX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the camshaft timing exhaust gear (See page <xref label="Seep02" href="RM0000030XO01QX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDV0QXX_09_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PDV0QXX_09_0012" proc-id="RM22W0E___000025R00001">
<testtitle>CONFIRM WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000000PDK17ZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the tester (See page <xref label="Seep02" href="RM000000PDL0SDX"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle in an urban area for approximately 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs using the tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTC is output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDV0QXX_09_0013" fin="true">OK</down>
<right ref="RM000000PDV0QXX_09_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0QXX_09_0013">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000PDV0QXX_09_0014">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000032SF053X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDV0QXX_09_0015">
<testtitle>REPLACE CAMSHAFT OIL CONTROL VALVE<xref label="Seep01" href="RM00000321401RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDV0QXX_09_0016">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000032SF053X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>