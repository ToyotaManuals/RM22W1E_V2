<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S002F" variety="S002F">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S002F_7C3WZ_T00Q2" variety="T00Q2">
<name>FRONT CONSOLE BOX (w/o Console Box Lid)</name>
<para id="RM000004XLA001X" category="A" type-id="30014" name-id="IT31K-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000004XLA001X_01" type-id="11" category="10" proc-id="RM22W0E___0000HTI00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0DSX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000004XLA001X_02" type-id="01" category="01">
<s-1 id="RM000004XLA001X_02_0001" proc-id="RM22W0E___0000HTJ00000">
<ptxt>INSTALL LOWER CONSOLE BOX</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B291242" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the lower console box.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws and 4 bolts.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0002" proc-id="RM22W0E___0000HTK00000">
<ptxt>INSTALL UPPER CONSOLE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B291235" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 8 claws to install the upper console panel sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0003" proc-id="RM22W0E___0000HTL00000">
<ptxt>INSTALL SHIFT LEVER KNOB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B291233" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the shift lever knob and twist it in the direction indicated by the arrow.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0004" proc-id="RM22W0E___0000HTM00000">
<ptxt>INSTALL REAR UPPER CONSOLE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 12 claws to install the rear upper console panel sub-assembly.</ptxt>
<figure>
<graphic graphicname="B291241" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0005" proc-id="RM22W0E___0000HTN00000">
<ptxt>INSTALL UPPER NO. 2 CONSOLE PANEL GARNISH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 claws to install the upper No. 2 console panel garnish.</ptxt>
<figure>
<graphic graphicname="B291237E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Separate Seat Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Bench Seat Type</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0006" proc-id="RM22W0E___0000HTO00000">
<ptxt>INSTALL UPPER NO. 1 CONSOLE PANEL GARNISH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B291239" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 8 claws to install the upper No. 1 console panel garnish.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0007" proc-id="RM22W0E___0000BBF00000">
<ptxt>INSTALL LOWER CENTER INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292996" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 7 claws to install the lower center instrument cluster finish panel sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0008" proc-id="RM22W0E___00008U800000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292995" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Connect the connectors and attach the 2 clamps.</ptxt>
</s2>
<s2>
<ptxt>Attach the 8 claws to install the lower instrument panel pad sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Install the clip and screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0009" proc-id="RM22W0E___00008U900000">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292997" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 7 claws to install the No. 2 instrument panel finish panel cushion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0010" proc-id="RM22W0E___0000BBG00000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180005" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 7 claws to install the lower instrument panel pad sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Install the clip and screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0011" proc-id="RM22W0E___0000BBH00000">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL FINISH PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182501" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 7 claws to install the No. 1 instrument panel finish panel cushion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XLA001X_02_0012" proc-id="RM22W0E___0000HTP00000">
<ptxt>INSTALL FRONT SEAT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000390Q00XX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>