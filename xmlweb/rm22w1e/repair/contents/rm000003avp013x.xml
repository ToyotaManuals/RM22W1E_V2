<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002K" variety="S002K">
<name>WIPER / WASHER</name>
<ttl id="12068_S002K_7C3YK_T00RN" variety="T00RN">
<name>HEADLIGHT CLEANER SWITCH</name>
<para id="RM000003AVP013X" category="A" type-id="80001" name-id="WW4SY-03" from="201308">
<name>REMOVAL</name>
<subpara id="RM000003AVP013X_02" type-id="11" category="10" proc-id="RM22W0E___0000IVC00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for LHD and RHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003AVP013X_01" type-id="01" category="01">
<s-1 id="RM000003AVP013X_01_0010" proc-id="RM22W0E___0000IVA00001">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Console Box Lid:</ptxt>
<s3>
<ptxt>Remove the No. 2 instrument panel finish panel cushion (See page <xref label="Seep01" href="RM000004XLC001X_02_0007"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Cool Box:</ptxt>
<s3>
<ptxt>Remove the No. 2 instrument panel finish panel cushion (See page <xref label="Seep02" href="RM000003B6S00JX_01_0002"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/o Cool Box:</ptxt>
<s3>
<ptxt>Remove the No. 2 instrument panel finish panel cushion (See page <xref label="Seep03" href="RM0000039QF00GX_01_0001"/>).</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AVP013X_01_0011" proc-id="RM22W0E___0000IVB00001">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Console Box Lid:</ptxt>
<s3>
<ptxt>Remove the lower instrument panel pad sub-assembly LH (See page <xref label="Seep01" href="RM000004XLC001X_02_0008"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Cool Box:</ptxt>
<s3>
<ptxt>Remove the lower instrument panel pad sub-assembly LH (See page <xref label="Seep02" href="RM000003B6S00JX_01_0003"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/o Cool Box:</ptxt>
<s3>
<ptxt>Remove the lower instrument panel pad sub-assembly LH (See page <xref label="Seep03" href="RM0000039QF00GX_01_0002"/>).</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AVP013X_01_0013" proc-id="RM22W0E___0000BB400001">
<ptxt>REMOVE NO. 1 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154744E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 3 claws and remove the No. 1 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003AVP013X_01_0014" proc-id="RM22W0E___0000BB500001">
<ptxt>REMOVE NO. 2 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws and remove the No. 2 instrument cluster finish panel garnish.</ptxt>
<figure>
<graphic graphicname="B291251E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Entry and Start System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003AVP013X_01_0015" proc-id="RM22W0E___000014A00001">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY (w/ Floor Under Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180655" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the No. 1 instrument panel under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003AVP013X_01_0016" proc-id="RM22W0E___0000A9S00001">
<ptxt>REMOVE LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180295E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the hole cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 16 claws.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 9 claws.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Detach the 2 claws and remove the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Detach the 2 claws and disconnect the 2 control cables.</ptxt>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the lower No. 1 instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003AVP013X_01_0017" proc-id="RM22W0E___0000A9P00001">
<ptxt>REMOVE NO. 1 SWITCH HOLE BASE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180298" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the No. 1 switch hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003AVP013X_01_0009" proc-id="RM22W0E___0000IV900001">
<ptxt>REMOVE HEADLIGHT CLEANER SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the switch.</ptxt>
<figure>
<graphic graphicname="E156027" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>