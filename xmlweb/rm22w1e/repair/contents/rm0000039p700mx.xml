<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12057_S002A" variety="S002A">
<name>PRE-CRASH SAFETY</name>
<ttl id="12057_S002A_7C3UE_T00NH" variety="T00NH">
<name>SEAT BELT CONTROL ECU (for LHD)</name>
<para id="RM0000039P700MX" category="A" type-id="30014" name-id="PC14C-02" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM0000039P700MX_01" type-id="01" category="01">
<s-1 id="RM0000039P700MX_01_0001" proc-id="RM22W0E___0000FMT00000">
<ptxt>INSTALL SEAT BELT CONTROL ECU</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E155217" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seat belt control ECU to the certification ECU with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the seat belt control ECU together with the certification ECU with the 2 nuts.</ptxt>
<figure>
<graphic graphicname="E155216" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 2 connectors.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039P700MX_01_0015" proc-id="RM22W0E___0000A9I00000">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Attach the 2 claws to install the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to connect the 2 control cables.</ptxt>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 16 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 9 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B182569" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to close the hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039P700MX_01_0016" proc-id="RM22W0E___0000A9H00000">
<ptxt>INSTALL NO. 1 SWITCH HOLE BASE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180298" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 claws to install the No. 1 switch hole base.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039P700MX_01_0017" proc-id="RM22W0E___0000AT300000">
<ptxt>INSTALL DRIVER SIDE KNEE AIRBAG ASSEMBLY (w/ Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B189604E01" width="2.775699831in" height="6.791605969in"/>
</figure>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the driver side knee airbag with the 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000039P700MX_01_0018" proc-id="RM22W0E___0000D7B00000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL SUB-ASSEMBLY (w/o Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180302" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the lower instrument panel.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039P700MX_01_0019" proc-id="RM22W0E___000014600000">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY (w/ Floor Under Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182553" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 3 claws to install the No. 1 instrument panel under cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039P700MX_01_0020" proc-id="RM22W0E___0000BAG00000">
<ptxt>INSTALL NO. 2 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the No. 2 instrument cluster finish panel garnish.</ptxt>
<figure>
<graphic graphicname="B291252E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Entry and Start System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000039P700MX_01_0021" proc-id="RM22W0E___0000BAH00000">
<ptxt>INSTALL NO. 1 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E155133" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws to install the No. 1 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039P700MX_01_0022" proc-id="RM22W0E___00008U800000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292995" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Connect the connectors and attach the 2 clamps.</ptxt>
</s2>
<s2>
<ptxt>Attach the 8 claws to install the lower instrument panel pad sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Install the clip and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039P700MX_01_0023" proc-id="RM22W0E___00008U900000">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292997" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 7 claws to install the No. 2 instrument panel finish panel cushion.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039P700MX_01_0012" proc-id="RM22W0E___0000FMU00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL (w/ Driver Side Knee Airbag)</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039P700MX_01_0014" proc-id="RM22W0E___0000FMV00000">
<ptxt>CHECK SRS WARNING LIGHT (w/ Driver Side Knee Airbag)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0IZX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>