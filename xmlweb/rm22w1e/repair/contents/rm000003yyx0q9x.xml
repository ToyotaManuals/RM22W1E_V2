<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R3_T00K6" variety="T00K6">
<name>AUDIO AND VISUAL SYSTEM (w/ Navigation System)</name>
<para id="RM000003YYX0Q9X" category="J" type-id="804JL" name-id="AVCHS-02" from="201308">
<dtccode/>
<dtcname>USB Audio System Recognition/Play Error</dtcname>
<subpara id="RM000003YYX0Q9X_04" type-id="32" category="03" proc-id="RM22W0E___0000C3L00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C253073E06" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003YYX0Q9X_02" type-id="51" category="05" proc-id="RM22W0E___0000C3E00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>When a large amount of data is in a USB device, it may take a while before play begins.</ptxt>
</item>
<item>
<ptxt>When using a USB device, files that are copy protected or encrypted due to copyright reasons cannot be played.</ptxt>
</item>
<item>
<ptxt>When files are not played in the sorted order, perform the following procedure before inspection.</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Add numbers in front of the file names. </ptxt>
</item>
<item>
<ptxt>Put the files in a folder and copy the folder data to the USB device.</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000003YYX0Q9X_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003YYX0Q9X_03_0007" proc-id="RM22W0E___0000C3F00001">
<testtitle>CHECK USB DEVICE OR "iPod"</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the USB device or "iPod" from the No. 1 stereo jack adapter assembly.</ptxt>
</test1>
<test1>
<ptxt>Check if playable files are present on the USB device or "iPod".</ptxt>
<atten4>
<ptxt>Refer to System Description for playable files (See page <xref label="Seep01" href="RM000000NKZ0EUX"/>).</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Check if the USB device is a compatible format or "iPod" is a compatible version.</ptxt>
<atten4>
<ptxt>Refer to System Description for compatible formats and versions (See page <xref label="Seep02" href="RM000000NKZ0EUX"/>).</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>No playable files exist, or incompatible device format or version.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Playable files exist, and compatible device format or version.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003YYX0Q9X_03_0002" fin="true">A</down>
<right ref="RM000003YYX0Q9X_03_0012" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0002">
<testtitle>USB DEVICE FORMAT WAS INCOMPATIBLE, "iPod" VERSION WAS INCOMPATIBLE, OR NO PLAYABLE FILES PRESENT</testtitle>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0012" proc-id="RM22W0E___0000C3J00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA MODULE RECEIVER ASSEMBLY - NO. 1 STEREO JACK ADAPTER ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F86 multi-media module receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the z62 No. 1 stereo jack adapter assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F86-1 (USV1) - z62-4 (UJSG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F86-2 (US1-) - z62-3 (UDI+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F86-3 (US1+) - z62-2 (UDI-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F86-4 (UGD1) - z62-1 (UPO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F86-5 (USG1) - z62-5 (UJSS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F86-1 (USV1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F86-2 (US1-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F86-3 (US1+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F86-4 (UGD1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F86-5 (USG1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003YYX0Q9X_03_0013" fin="false">OK</down>
<right ref="RM000003YYX0Q9X_03_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0013" proc-id="RM22W0E___0000C3K00001">
<testtitle>CHECK MULTI-MEDIA MODULE RECEIVER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the multi-media module receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below. </ptxt>
<figure>
<graphic graphicname="E210105E65" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z62-1 (UPO) - z62-4 (UJSG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (ACC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 1 Stereo Jack Adapter Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003YYX0Q9X_03_0010" fin="false">OK</down>
<right ref="RM000003YYX0Q9X_03_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0010" proc-id="RM22W0E___0000C3I00001">
<testtitle>FORMAT USB DEVICE OR RESTORE "iPod" AND RECHECK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Delete all files in the USB device or "iPod" and format/restore it.</ptxt>
</test1>
<test1>
<ptxt>Save the data again and check if it can be played on the in-vehicle device.</ptxt>
<atten3>
<ptxt>Formatting a USB device or restoring an "iPod" erases all music on the device. </ptxt>
<ptxt>Ensure that backup music data is available before performing this operation.</ptxt>
</atten3>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction disappears.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003YYX0Q9X_03_0011" fin="true">OK</down>
<right ref="RM000003YYX0Q9X_03_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0008" proc-id="RM22W0E___0000C3G00001">
<testtitle>REPLACE USB DEVICE OR "iPod"</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the engine switch off.</ptxt>
<atten4>
<ptxt>When this malfunction occurs, it is necessary to turn off the engine switch to make it possible for the vehicle to recognize a new device when it is connected.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Turn the engine switch on (ACC).</ptxt>
</test1>
<test1>
<ptxt>Connect a known good USB device or "iPod" to the No. 1 stereo jack adapter assembly.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the malfunction occurred when a USB device was in use, use another USB device for the inspection. If the malfunction occurred when an "iPod" was in use, use another "iPod" for the inspection. </ptxt>
</item>
<item>
<ptxt>Refer to System Description for compatible formats and versions (See page <xref label="Seep01" href="RM000000NKZ0EUX"/>).</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003YYX0Q9X_03_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0011">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0009" proc-id="RM22W0E___0000C3H00001">
<testtitle>CHECK USB DEVICE OR "iPod"</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the USB device or "iPod" is recognized by the multi-media module receiver assembly, or if information such as track, artist and album names are displayed on the screen.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>USB device or "iPod" is recognized or track information such as track, artist and album names are displayed.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003YYX0Q9X_03_0005" fin="true">OK</down>
<right ref="RM000003YYX0Q9X_03_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0005">
<testtitle>USB DEVICE OR "iPod" WAS INCOMPATIBLE OR DEFECTIVE</testtitle>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0006">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000011BR0O2X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003YYX0Q9X_03_0015">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY024X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>