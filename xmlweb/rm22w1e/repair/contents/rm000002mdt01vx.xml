<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12013_S0016" variety="S0016">
<name>3UR-FE STARTING</name>
<ttl id="12013_S0016_7C3LL_T00EO" variety="T00EO">
<name>ENGINE SWITCH</name>
<para id="RM000002MDT01VX" category="G" type-id="3001K" name-id="ST17J-18" from="201301">
<name>INSPECTION</name>
<subpara id="RM000002MDT01VX_01" type-id="01" category="01">
<s-1 id="RM000002MDT01VX_01_0001" proc-id="RM22W0E___000075E00000">
<ptxt>INSPECT ENGINE SWITCH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A129763E09" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry>
<ptxt>Tester Connection</ptxt>
</entry>
<entry>
<ptxt>Switch Condition</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>7 (SS1) - 5 (GND)</ptxt>
</entry>
<entry>
<ptxt>Not pushed</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>2 (SS2) - 5 (GND)</ptxt>
</entry>
<entry>
<ptxt>Not pushed</ptxt>
</entry>
<entry>
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>7 (SS1) - 5 (GND)</ptxt>
</entry>
<entry>
<ptxt>Pushed</ptxt>
</entry>
<entry>
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>2 (SS2) - 5 (GND)</ptxt>
</entry>
<entry>
<ptxt>Pushed</ptxt>
</entry>
<entry>
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Engine Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the engine switch.</ptxt>
</s2>
<s2>
<ptxt>Apply battery voltage between the terminals of the switch, and check the illumination condition of the switch.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the positive (+) lead and the negative (-) lead are incorrectly connected, the engine switch indicator will not illuminate.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>If the voltage is too low, the indicator will not illuminate.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 11 (SWIL)</ptxt>
<ptxt>Battery negative (-) → Terminal 5 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates (illumination of lettering)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 12 (INDS)</ptxt>
<ptxt>Battery negative (-) → Terminal 5 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates (green)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 13 (INDW)</ptxt>
<ptxt>Battery negative (-) → Terminal 5 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates (amber)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="A109252E74" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Engine Switch)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the engine switch.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>