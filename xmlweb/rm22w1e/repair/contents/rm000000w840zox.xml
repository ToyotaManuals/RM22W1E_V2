<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MT_T00FW" variety="T00FW">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1UR-FE)</name>
<para id="RM000000W840ZOX" category="C" type-id="302G7" name-id="AT9A5-03" from="201301" to="201308">
<dtccode>P0705</dtccode>
<dtcname>Transmission Range Sensor Circuit Malfunction (PRNDL Input)</dtcname>
<subpara id="RM000000W840ZOX_01" type-id="60" category="03" proc-id="RM22W0E___00008J600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The Park/Neutral Position (PNP) switch detects the shift lever position and sends signals to the ECM.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0705</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<ptxt>(A) Any 2 or more of the following signals are ON simultaneously (2 trip detection logic).</ptxt>
<list1 type="unordered">
<item>
<ptxt>P input signal</ptxt>
</item>
<item>
<ptxt>N input signal</ptxt>
</item>
<item>
<ptxt>R input signal</ptxt>
</item>
<item>
<ptxt>D input signal</ptxt>
</item>
</list1>
<ptxt>(B) Any 2 or more of the following signals are ON simultaneously (2 trip detection logic).</ptxt>
<list1 type="unordered">
<item>
<ptxt>NSW input signal</ptxt>
</item>
<item>
<ptxt>R input signal</ptxt>
</item>
<item>
<ptxt>D input signal</ptxt>
</item>
</list1>
<ptxt>(C) Any of the following signals are ON for 2.0 sec. or more when the shift lever is in S (2 trip detection logic).</ptxt>
<list1 type="unordered">
<item>
<ptxt>NSW input signal</ptxt>
</item>
<item>
<ptxt>P input signal</ptxt>
</item>
<item>
<ptxt>N input signal</ptxt>
</item>
<item>
<ptxt>R input signal</ptxt>
</item>
</list1>
<ptxt>(D) All the signals are OFF simultaneously for the P, R, N and D positions (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Park/neutral position switch</ptxt>
</item>
<item>
<ptxt>Transmission control switch (Lower shift lever assembly)</ptxt>
</item>
<item>
<ptxt>Open or short in park/neutral position switch circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W840ZOX_02" type-id="64" category="03" proc-id="RM22W0E___00008J700000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates a problem with the park/neutral position switch and the wire harness in the park/neutral position switch circuit.</ptxt>
<ptxt>The park/neutral position switch detects the shift lever position and sends a signal to the ECM.</ptxt>
<ptxt>For security, the park/neutral position switch detects the shift lever position so that the engine can be started only when the shift lever is in P or N.</ptxt>
<ptxt>The park/neutral position switch sends a signal to the ECM according to the shift lever position (P, R, N, D or S).</ptxt>
<ptxt>The ECM determines that there is a problem with the switch or related parts if it receives more than 1 position signal simultaneously. The ECM will illuminate the MIL and store the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W840ZOX_07" type-id="32" category="03" proc-id="RM22W0E___00008J800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C257148E07" width="7.106578999in" height="7.795582503in"/>
</figure>
<figure>
<graphic graphicname="C257148E08" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W840ZOX_08" type-id="51" category="05" proc-id="RM22W0E___00008J900000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the GTS to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the GTS on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the GTS, read the Data List.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Neutral Position SW Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In P or N: ON</ptxt>
</item>
<item>
<ptxt>Not in P or N: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the GTS differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (R Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In R: ON</ptxt>
</item>
<item>
<ptxt>Not in R: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the GTS differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (P Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In P: ON</ptxt>
</item>
<item>
<ptxt>Not in P: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the GTS differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (N Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In N: ON</ptxt>
</item>
<item>
<ptxt>Not in N: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the GTS differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (D Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In D: ON</ptxt>
</item>
<item>
<ptxt>Not in D: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the GTS differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Sports Shift Up SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sport shift up switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Shift lever held in "+" (up-shift)</ptxt>
</item>
<item>
<ptxt>OFF: Shift lever not in "+" (up-shift)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Sports Shift Down SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sport shift down switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Shift lever held in "-" (down-shift)</ptxt>
</item>
<item>
<ptxt>OFF: Shift lever not in "-" (down-shift)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000000W840ZOX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W840ZOX_09_0006" proc-id="RM22W0E___00008JC00000">
<testtitle>CHECK HARNESS AND CONNECTOR (BK/UP LP FUSE - PARK/NEUTRAL POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
<figure>
<graphic graphicname="C197711E49" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C24-2 (RB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C24-2 (RB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Park/Neutral Position Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W840ZOX_09_0018" fin="false">OK</down>
<right ref="RM000000W840ZOX_09_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840ZOX_09_0018" proc-id="RM22W0E___00008JG00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NSW TERMINAL VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
<figure>
<graphic graphicname="C197711E75" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>C24-4 (B) - Body ground</ptxt>
</entry>
<entry>
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C24-4 (B) - Body ground</ptxt>
</entry>
<entry>
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Park/Neutral Position Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W840ZOX_09_0001" fin="false">OK</down>
<right ref="RM000000W840ZOX_09_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840ZOX_09_0001" proc-id="RM22W0E___00008JA00000">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
<figure>
<graphic graphicname="C197712E28" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>2 (RB) - 6 (PL)</ptxt>
</item>
<item>
<ptxt>4 (B) - 5 (L)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Shift lever in P</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 1 (RL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>2 (RB) - 9 (NL)</ptxt>
</item>
<item>
<ptxt>4 (B) - 5 (L)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Shift lever in N</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 7 (DL)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in D</ptxt>
</item>
<item>
<ptxt>Shift lever in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>2 (RB) - 6 (PL)</ptxt>
</item>
<item>
<ptxt>4 (B) - 5 (L)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in P</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 1 (RL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>2 (RB) - 9 (NL)</ptxt>
</item>
<item>
<ptxt>4 (B) - 5 (L)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in N</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 7 (DL)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever not in D</ptxt>
</item>
<item>
<ptxt>Shift lever not in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Park/Neutral Position Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W840ZOX_09_0007" fin="false">OK</down>
<right ref="RM000000W840ZOX_09_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840ZOX_09_0007" proc-id="RM22W0E___00008JD00000">
<testtitle>CHECK HARNESS AND CONNECTOR (LH ECU-IG FUSE - TRANSMISSION CONTROL SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the transmission control switch connector.</ptxt>
<figure>
<graphic graphicname="C203343E35" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E52-3 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E52-3 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transmission Control Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W840ZOX_09_0004" fin="false">OK</down>
<right ref="RM000000W840ZOX_09_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840ZOX_09_0004" proc-id="RM22W0E___00008JB00000">
<testtitle>INSPECT TRANSMISSION CONTROL SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the transmission control switch connector.</ptxt>
<figure>
<graphic graphicname="C203344E19" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>3 (IG) - 7 (S)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in S, "+" or "-"</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 (IG) - 7 (S)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in S, "+" or "-"</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Transmission Control Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W840ZOX_09_0008" fin="false">OK</down>
<right ref="RM000000W840ZOX_09_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840ZOX_09_0008" proc-id="RM22W0E___00008JE00000">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH, TRANSMISSION CONTROL SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C257145E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C45-2 (P) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in P</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-26 (R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in R</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-25 (N) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-27 (D) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in D</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-25 (S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-2 (P) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in P</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-26 (R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in R</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-25 (N) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-27 (D) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in D</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A38-25 (S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C46-2 (P) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in P</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-26 (R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in R</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-25 (N) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-27 (D) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in D</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A52-25 (S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-2 (P) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in P</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-26 (R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in R</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-25 (N) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-27 (D) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in D</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A52-25 (S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Shift lever not in S, "+" or "-"</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>*: The voltage will drop slightly due to the turning on of the back up light.</ptxt>
</atten4>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000W840ZOX_09_0010" fin="true">OK</down>
<right ref="RM000000W840ZOX_09_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840ZOX_09_0009" proc-id="RM22W0E___00008JF00000">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C24 park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C45 ECM connector (for LHD).</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C46 ECM connector (for RHD).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C24-4 (B) - C45-120 (NSW)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C24-4 (B) or C45-120 (NSW) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C24-4 (B) - C46-120 (NSW)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C24-4 (B) or C46-120 (NSW) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W840ZOX_09_0010" fin="true">OK</down>
<right ref="RM000000W840ZOX_09_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840ZOX_09_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W840ZOX_09_0015">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W840ZOX_09_0011">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC076X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W840ZOX_09_0017">
<testtitle>REPLACE TRANSMISSION CONTROL SWITCH (LOWER SHIFT LEVER ASSEMBLY)<xref label="Seep01" href="RM000002YBF03JX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>