<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LW_T00EZ" variety="T00EZ">
<name>CRUISE CONTROL SYSTEM</name>
<para id="RM000000ST00AWX" category="D" type-id="303FA" name-id="CC0009-143" from="201301">
<name>ROAD TEST</name>
<subpara id="RM000000ST00AWX_z0" proc-id="RM22W0E___000079800000">
<content5 releasenbr="1">
<step1>
<ptxt>PROBLEM SYMPTOM CONFIRMATION</ptxt>
<step2>
<ptxt>Inspect the SET function.</ptxt>
<figure>
<graphic graphicname="E107085" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Turn the main switch on.</ptxt>
</step3>
<step3>
<ptxt>Drive at the required speed (40 km/h (25 mph) or more).</ptxt>
</step3>
<step3>
<ptxt>Push the cruise control main switch to -SET.</ptxt>
</step3>
<step3>
<ptxt>Check that the "SET" indicator light illuminates in the combination meter assembly.</ptxt>
</step3>
<step3>
<ptxt>After releasing the switch, check that the vehicle cruises at the set speed.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Inspect the + (ACCELERATION) function.</ptxt>
<figure>
<graphic graphicname="E107086" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Turn the main switch on.</ptxt>
</step3>
<step3>
<ptxt>Drive at the required speed (40 km/h (25 mph) or more).</ptxt>
</step3>
<step3>
<ptxt>Push the cruise control main switch to -SET.</ptxt>
</step3>
<step3>
<ptxt>Check that the vehicle speed increases while the cruise control main switch is pushed to +RES, and that the vehicle cruises at the newly set speed when the switch is released.</ptxt>
</step3>
<step3>
<ptxt>Momentarily push the cruise control main switch to +RES and then immediately release it. Check that the vehicle speed increases by approximately 1.6 km/h (1.0 mph) (tap-up control).</ptxt>
</step3>
</step2>
<step2>
<ptxt>Inspect the - (COAST) function.</ptxt>
<figure>
<graphic graphicname="E107085" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Turn the main switch on.</ptxt>
</step3>
<step3>
<ptxt>Drive at the required speed (40 km/h (25 mph) or more).</ptxt>
</step3>
<step3>
<ptxt>Push the cruise control main switch to -SET.</ptxt>
</step3>
<step3>
<ptxt>Check that the vehicle speed decreases while the cruise control main switch is pushed to -SET, and that the vehicle cruises at the newly set speed when the switch is released.</ptxt>
</step3>
<step3>
<ptxt>Momentarily push the cruise control main switch to -SET, and then immediately release it. Check that vehicle speed decreases by approximately 1.6 km/h (1.0 mph) (tap-down control).</ptxt>
</step3>
</step2>
<step2>
<ptxt>Inspect the CANCEL function.</ptxt>
<figure>
<graphic graphicname="E107087" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Turn the main switch on.</ptxt>
</step3>
<step3>
<ptxt>Drive at the required speed (40 km/h (25 mph) or more).</ptxt>
</step3>
<step3>
<ptxt>Push the cruise control main switch to -SET.</ptxt>
</step3>
<step3>
<ptxt>When performing one of the following, check that the cruise control system is canceled and that the normal driving mode is reset.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Depressing the brake pedal.</ptxt>
</item>
<item>
<ptxt>Depressing the clutch pedal (for M/T).</ptxt>
</item>
<item>
<ptxt>Moving the shift lever from D to N (for A/T).</ptxt>
</item>
<item>
<ptxt>With the shift lever in S, selecting the S1, S2 or S3 range (for A/T).</ptxt>
</item>
<item>
<ptxt>Turning the main switch off.</ptxt>
</item>
<item>
<ptxt>Pulling the cruise control main switch to CANCEL.</ptxt>
</item>
</list1>
</step3>
<step3>
<ptxt>Set the cruise control, pull the cruise control main switch to CANCEL, and check that the "SET" indicator light illuminates in the combination meter assembly and normal driving is restored.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Inspect the RES (RESUME) function.</ptxt>
<figure>
<graphic graphicname="E107086" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Turn the main switch on.</ptxt>
</step3>
<step3>
<ptxt>Drive at the required speed (40 km/h (25 mph) or more).</ptxt>
</step3>
<step3>
<ptxt>Push the cruise control main switch to -SET.</ptxt>
</step3>
<step3>
<ptxt>Cancel the cruise control system according to any of the above operations (other than turning the main switch off).</ptxt>
</step3>
<step3>
<ptxt>After pushing the cruise control main switch to +RES at a driving speed of more than 40 km/h (25 mph), check that the vehicle drives at the set speed.</ptxt>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>