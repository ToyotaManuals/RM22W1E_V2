<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000001D6V0SCX" category="J" type-id="305BG" name-id="ES169I-002" from="201308">
<dtccode/>
<dtcname>VC Output Circuit</dtcname>
<subpara id="RM000001D6V0SCX_01" type-id="60" category="03" proc-id="RM22W0E___00003O200001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM constantly generates 5 V of power from the battery voltage supplied to the +B (BATT) terminal to operate the microprocessor. The ECM also provides this power to the sensors through the VC output circuit.</ptxt>
<figure>
<graphic graphicname="A116143E68" width="7.106578999in" height="3.779676365in"/>
</figure>
<ptxt>When the VC circuit is short-circuited, the microprocessor in the ECM and sensors that are supplied with power through the VC circuit are inactivated because the power is not supplied from the VC circuit. Under this condition, the system does not start up and the MIL does not illuminate even if the system malfunctions.</ptxt>
<atten4>
<ptxt>Under normal conditions, the MIL is illuminated for several seconds when the engine switch is first turned on (IG). The MIL goes off when the engine is started.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001D6V0SCX_02" type-id="32" category="03" proc-id="RM22W0E___00003O300001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A165964E07" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="A275261E01" width="7.106578999in" height="9.803535572in"/>
</figure>
<figure>
<graphic graphicname="A275262E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="A165964E08" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="A275261E02" width="7.106578999in" height="9.803535572in"/>
</figure>
<figure>
<graphic graphicname="A275262E03" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001D6V0SCX_03" type-id="51" category="05" proc-id="RM22W0E___00003O400001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08XX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001D6V0SCX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001D6V0SCX_05_0001" proc-id="RM22W0E___00003O500001">
<testtitle>CHECK MIL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the Malfunction Indicator Lamp (MIL) lights up when turning the engine switch on (IG).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0002" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0009" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0002" proc-id="RM22W0E___00003O600001">
<testtitle>CHECK COMMUNICATION BETWEEN GTS AND ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and GTS on.</ptxt>
</test1>
<test1>
<ptxt>Check the communication between the GTS and ECM.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Communication is possible</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Communication is not possible</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0004" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0010" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0004" proc-id="RM22W0E___00003O700001">
<testtitle>CHECK MIL (THROTTLE POSITION SENSOR (for Bank 1))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C83 throttle position sensor (for Bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the C83 throttle position sensor (for Bank 1) connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0005" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0011" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0005" proc-id="RM22W0E___00003O800001">
<testtitle>CHECK MIL (THROTTLE POSITION SENSOR (for Bank 2))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C84 throttle position sensor (for Bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the C84 throttle position sensor (for Bank 2) connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0017" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0012" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0017" proc-id="RM22W0E___00003OB00001">
<testtitle>CHECK MIL (NO. 1 EGR VALVE POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C87 No. 1 EGR valve connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the C87 No. 1 EGR valve connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0020" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0013" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0020" proc-id="RM22W0E___00003OD00001">
<testtitle>CHECK MIL (NO. 2 EGR VALVE POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C88 No. 2 EGR valve connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the C88 No. 2 EGR valve connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0006" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0018" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0006" proc-id="RM22W0E___00003O900001">
<testtitle>CHECK MIL (ACCELERATOR PEDAL POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A31 accelerator pedal position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the A31 accelerator pedal position sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0019" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0027" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0019" proc-id="RM22W0E___00003OC00001">
<testtitle>CHECK MIL (MANIFOLD ABSOLUTE PRESSURE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C80 manifold absolute pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the C80 manifold absolute pressure sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0021" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0023" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0021" proc-id="RM22W0E___00003OE00001">
<testtitle>CHECK MIL (FUEL PRESSURE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C82 fuel pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the C82 fuel pressure sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0028" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0025" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0028" proc-id="RM22W0E___00003OF00001">
<testtitle>CHECK MIL (DIFFERENTIAL PRESSURE SENSOR ASSEMBLY (for Bank 1))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the N16 differential pressure sensor assembly (for Bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the N16 differential pressure sensor assembly (for Bank 1) connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0029" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0032" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0029" proc-id="RM22W0E___00003OG00001">
<testtitle>CHECK MIL (DIFFERENTIAL PRESSURE SENSOR ASSEMBLY (for Bank 2))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C157 differential pressure sensor assembly (for Bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the C157 differential pressure sensor assembly (for Bank 2) connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0030" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0033" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0030" proc-id="RM22W0E___00003OH00001">
<testtitle>CHECK MIL (NOZZLE VANE POSITION SENSOR (for Bank 1))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C89 nozzle vane position sensor (for Bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the C89 nozzle vane position sensor (for Bank 1) connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0031" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0034" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0031" proc-id="RM22W0E___00003OI00001">
<testtitle>CHECK MIL (NOZZLE VANE POSITION SENSOR (for Bank 2))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the z66 nozzle vane position sensor (for Bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Check the MIL.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>MIL illuminates</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIL does not illuminate</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the z66 nozzle vane position sensor (for Bank 2) connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0008" fin="false">B</down>
<right ref="RM000001D6V0SCX_05_0035" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0008" proc-id="RM22W0E___00003OA00001">
<testtitle>CHECK HARNESS AND CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle position sensor (for Bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the throttle position sensor (for Bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the No. 1 EGR valve connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the No. 2 EGR valve connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the accelerator pedal position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the manifold absolute pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the fuel pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the differential pressure sensor assembly (for Bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the differential pressure sensor assembly (for Bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the nozzle vane position sensor (for Bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the nozzle vane position sensor (for Bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connectors.</ptxt>
<figure>
<graphic graphicname="A182331E15" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C45-91 (VC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-110 (VCVL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-118 (VCM) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-120 (VNVC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-66 (VCPM) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A38-55 (VCPA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A38-56 (VCP2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C46-91 (VC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-110 (VCVL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-118 (VCM) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-120 (VNVC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-66 (VCPM) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A52-55 (VCPA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A52-56 (VCP2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the throttle position sensor (for Bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the throttle position sensor (for Bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the No. 1 EGR valve connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the No. 2 EGR valve connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the accelerator pedal position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the manifold absolute pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the fuel pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the differential pressure sensor assembly (for Bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the differential pressure sensor assembly (for Bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the nozzle vane position sensor (for Bank 1) connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the nozzle vane position sensor (for Bank 2) connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connectors.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001D6V0SCX_05_0016" fin="true">OK</down>
<right ref="RM000001D6V0SCX_05_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0009">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0010">
<testtitle>GO TO MIL CIRCUIT<xref label="Seep01" href="RM000000WZ114HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0011">
<testtitle>REPLACE DIESEL THROTTLE BODY ASSEMBLY (for Bank 1)<xref label="Seep01" href="RM00000316V006X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0012">
<testtitle>REPLACE DIESEL THROTTLE BODY ASSEMBLY (for Bank 2)<xref label="Seep01" href="RM00000316V006X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0013">
<testtitle>REPLACE NO. 1 EGR VALVE ASSEMBLY<xref label="Seep01" href="RM0000031JM00BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0018">
<testtitle>REPLACE NO. 2 EGR VALVE ASSEMBLY<xref label="Seep01" href="RM0000031JM00BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0027">
<testtitle>REPLACE ACCELERATOR PEDAL ROD ASSEMBLY<xref label="Seep01" href="RM00000284A00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0023">
<testtitle>REPLACE MANIFOLD ABSOLUTE PRESSURE SENSOR<xref label="Seep01" href="RM0000031DW003X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0025">
<testtitle>REPLACE COMMON RAIL (for Bank 1)<xref label="Seep01" href="RM0000031FK00ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0032">
<testtitle>REPLACE DIFFERENTIAL PRESSURE SENSOR (for Bank 1)<xref label="Seep01" href="RM000003TI200QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0033">
<testtitle>REPLACE DIFFERENTIAL PRESSURE SENSOR (for Bank 2)<xref label="Seep01" href="RM000003TI200QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0034">
<testtitle>REPLACE TURBOCHARGER SUB-ASSEMBLY (for Bank 1)<xref label="Seep01" href="RM0000032A8023X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0035">
<testtitle>REPLACE TURBOCHARGER SUB-ASSEMBLY (for Bank 2)<xref label="Seep01" href="RM0000032A8023X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0015">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001D6V0SCX_05_0016">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329203AX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>