<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000TEO0ETX" category="C" type-id="3033F" name-id="ES11I5-003" from="201308">
<dtccode>P0137</dtccode>
<dtcname>Oxygen Sensor Circuit Low Voltage (Bank 1 Sensor 2)</dtcname>
<dtccode>P0157</dtccode>
<dtcname>Oxygen Sensor Circuit Low Voltage (Bank 2 Sensor 2)</dtcname>
<subpara id="RM000000TEO0ETX_01" type-id="60" category="03" proc-id="RM22W0E___00002A300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>In order to obtain a high purification rate of the carbon monoxide (CO), hydrocarbon (HC) and nitrogen oxide (NOx) components in the exhaust gas, a TWC is used. For the most efficient use of the TWC, the air-fuel ratio must be precisely controlled so that it is always close to the stoichiometric air-fuel ratio. For the purpose of helping the ECM to deliver accurate air-fuel ratio control, a Heated Oxygen (HO2) sensor is used.</ptxt>
<ptxt>The HO2 sensor is located behind the TWC, and detects the oxygen concentration in the exhaust gas. Since the sensor is integrated with the heater that heats the sensing portion, it is possible to detect the oxygen concentration even when the intake air volume is low (the exhaust gas temperature is low).</ptxt>
<ptxt>When the air-fuel ratio becomes lean, the oxygen concentration in the exhaust gas is rich. The HO2 sensor informs the ECM that the post-TWC air-fuel ratio is lean (low voltage, i.e. below 0.45 V).</ptxt>
<ptxt>Conversely, when the air-fuel ratio is richer than the stoichiometric air-fuel ratio, the oxygen concentration in the exhaust gas becomes lean. The HO2 sensor informs the ECM that the post-TWC air-fuel ratio is rich (high voltage, i.e. higher than 0.45 V). The HO2 sensor has the property of changing its output voltage drastically when the air-fuel ratio is close to the stoichiometric level.</ptxt>
<ptxt>The ECM uses the supplementary information from the HO2 sensor to determine whether the air-fuel ratio after the TWC is rich or lean, and adjusts the fuel injection time accordingly. Thus, if the HO2 sensor is working improperly due to internal malfunctions, the ECM is unable to compensate for deviations in the primary air-fuel ratio control.</ptxt>
<figure>
<graphic graphicname="A115539E07" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0137</ptxt>
<ptxt>P0157</ptxt>
</entry>
<entry>
<list1 type="nonmark">
<item>
<ptxt>Low voltage (open):</ptxt>
</item>
<list2 type="nonmark">
<item>
<ptxt>During active air-fuel ratio control, the following conditions (a) and (b) are met for a certain period of time (2 trip detection logic):</ptxt>
</item>
</list2>
<list2 type="ordered">
<item>
<ptxt>The HO2 sensor voltage output is below 0.21 V.</ptxt>
</item>
<item>
<ptxt>The target air-fuel ratio is rich.</ptxt>
</item>
</list2>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in HO2 sensor (for Bank 1, 2) circuit</ptxt>
</item>
<item>
<ptxt>HO2 sensor (for Bank 1, 2)</ptxt>
</item>
<item>
<ptxt>HO2 sensor heater (for Bank 1, 2)</ptxt>
</item>
<item>
<ptxt>Integration relay</ptxt>
</item>
<item>
<ptxt>Gas leakage from exhaust system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000TEO0ETX_02" type-id="64" category="03" proc-id="RM22W0E___00002A400001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<topic>
<title>Active Air-Fuel Ratio Control</title>
<ptxt>The ECM usually performs air-fuel ratio feedback control so that the Air Fuel ratio (A/F) sensor output indicates a near stoichiometric air-fuel ratio. This vehicle includes active air-fuel ratio control in addition to regular air-fuel ratio control. The ECM performs active air-fuel ratio control to detect any deterioration in the Three-Way Catalytic Converter (TWC) and Heated Oxygen (HO2) sensor malfunctions (refer to the diagram below).</ptxt>
<ptxt>Active air-fuel ratio control is performed for approximately 15 to 20 seconds while driving with a warm engine. During active air-fuel ratio control, the air-fuel ratio is forcibly regulated to become lean or rich by the ECM. If the ECM detects a malfunction, a DTC is stored.</ptxt>
</topic>
<topic/>
<topic>
<title>Open or Short in Heated Oxygen (HO2) Sensor Circuit (DTC P0137 and P0157)</title>
<ptxt>During active air-fuel ratio control, the ECM calculates the Oxygen Storage Capacity (OSC)* of the Three-Way Catalytic Converter (TWC) by forcibly regulating the air-fuel ratio to become rich or lean. If the HO2 sensor has an open or short, or the voltage output of the sensor decreases significantly, the OSC indicates an extraordinarily high value. Even if the ECM attempts to continue regulating the air-fuel ratio to become rich or lean, the HO2 sensor output does not change.</ptxt>
<ptxt>While performing active air-fuel ratio control, when the target air-fuel ratio is rich and the HO2 sensor voltage output is 0.21 V or less (lean), the ECM interprets this as an abnormally low sensor output voltage and stores DTC P0137 or P0157.</ptxt>
<ptxt>*: The TWC has the capability to store oxygen. The OSC and the emission purification capacity of the TWC are mutually related. The ECM determines whether the catalyst has deteriorated based on the calculated OSC value.</ptxt>
<figure>
<graphic graphicname="A244503E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</topic>
</content5>
</subpara>
<subpara id="RM000000TEO0ETX_08" type-id="32" category="03" proc-id="RM22W0E___00002A500001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A187620E03" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TEO0ETX_09" type-id="73" category="03" proc-id="RM22W0E___00002A600001">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>This confirmation driving pattern is used in the "Perform Confirmation Driving Pattern" procedure of the following diagnostic troubleshooting procedure.</ptxt>
</item>
<item>
<ptxt>Performing this confirmation driving pattern will activate the Heated Oxygen (HO2) sensor monitor (the catalyst monitor is performed simultaneously). This is very useful for verifying the completion of a repair.</ptxt>
</item>
</list1>
</atten4>
<atten3>
<ptxt>This test will not be completed if the vehicle is driven under absolutely constant speed conditions such as with cruise control activated.</ptxt>
</atten3>
<figure>
<graphic graphicname="A172863E04" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG).</ptxt>
</item>
<item>
<ptxt>Turn the tester on.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC / Clear.</ptxt>
</item>
<item>
<ptxt>Clear DTCs.</ptxt>
</item>
<item>
<ptxt>Switch the ECM from normal mode to check mode</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / O2S (A/FS) Monitor.</ptxt>
</item>
<item>
<ptxt>Check that the item O2S (A/FS) Monitor displays Incmpl (Incomplete).</ptxt>
</item>
<item>
<ptxt>Start the engine and warm it up. (Proceed to "A")</ptxt>
</item>
<item>
<ptxt>Drive the vehicle at between 90 km/h and 110 km/h (56 mph and 68 mph) for at least 10 minutes. (Proceed to "B")</ptxt>
</item>
<item>
<ptxt>Note the state of the Data List items. Those items will change to Compl (Complete) as the O2 Sensor monitor operates.</ptxt>
</item>
<item>
<ptxt>On the tester, enter the following menus: Powertrain / Engine and ECT / DTC / Pending and check if any DTCs (any pending DTCs) are output.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000TEO0ETX_10" type-id="51" category="05" proc-id="RM22W0E___00002A700001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Malfunctioning areas can be identified by performing the Control the Injection Volume for A/F sensor function provided in the Active Test. The Control the Injection Volume for A/F sensor function can help to determine whether the Air Fuel ratio (A/F) sensor, Heated Oxygen (HO2) sensor and other potential trouble areas are malfunctioning.</ptxt>
<ptxt>The following instructions describe how to conduct the Control the Injection Volume for A/F sensor operation using the intelligent tester.</ptxt>
<list1 type="ordered">
<item>
<ptxt>Connect the tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>Start the engine and turn the tester on.</ptxt>
</item>
<item>
<ptxt>Warm up the engine at an engine speed of 2500 rpm for approximately 90 seconds.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F sensor.</ptxt>
</item>
<item>
<ptxt>Perform the Active Test operation with the engine in an idling condition (press the RIGHT or LEFT button to change the fuel injection volume).</ptxt>
</item>
<item>
<ptxt>Monitor the output voltages of the A/F and HO2 sensors (AFS B1 S1 and O2S B1 S2 or AFS B2 S1 and O2S B2 S2) displayed on the tester.</ptxt>
</item>
</list1>
</atten4>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The Control the Injection Volume for A/F sensor operation lowers the fuel injection volume by 12.5% or increases the injection volume by 25%.</ptxt>
</item>
<item>
<ptxt>Each sensor reacts in accordance with increases and decreases in the fuel injection volume.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display Item (Sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Injection Volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Voltage</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>AFS B1 S1 or AFS B2 S1</ptxt>
<ptxt>(A/F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+25%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 3.1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AFS B1 S1 or AFS B2 S1</ptxt>
<ptxt>(A/F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 3.4 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1 S2 or O2S B2 S2</ptxt>
<ptxt>(HO2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+25%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 0.55 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1 S2 or O2S B2 S2</ptxt>
<ptxt>(HO2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 0.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The Air Fuel ratio (A/F) sensor has an output delay of a few seconds and the Heated Oxygen (HO2) sensor has a maximum output delay of approximately 20 seconds.</ptxt>
</atten3>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.51in"/>
<colspec colname="COL2" colwidth="2.63in"/>
<colspec colname="COL3" colwidth="2.58in"/>
<colspec colname="COL4" colwidth="1.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Case</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A/F Sensor (Sensor 1) Output Voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>HO2 Sensor (Sensor 2) Output Voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Suspected Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A151324E12" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150788E04" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150788E04" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>A/F sensor</ptxt>
</item>
<item>
<ptxt>A/F sensor heater</ptxt>
</item>
<item>
<ptxt>A/F sensor circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A151324E12" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>HO2 sensor</ptxt>
</item>
<item>
<ptxt>HO2 sensor heater</ptxt>
</item>
<item>
<ptxt>HO2 sensor circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Fuel injector</ptxt>
</item>
<item>
<ptxt>Fuel pressure</ptxt>
</item>
<item>
<ptxt>Gas leakage from exhaust system (Air-fuel ratio extremely rich or lean)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>Following the Control the Injection Volume for A/F sensor procedure enables technicians to check and graph the voltage outputs of both the A/F and HO2 sensors.</ptxt>
</item>
<item>
<ptxt>To display the graph, enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume / All Data / AFS Voltage B1S1 and O2S B1S2 or AFS Voltage B2S1 and O2S B2S2.</ptxt>
</item>
</list1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Sensor 1 refers to the sensor closest to the engine assembly.</ptxt>
</item>
<item>
<ptxt>Sensor 2 refers to the sensor farthest away from the engine assembly.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TEO0ETX_13" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TEO0ETX_13_0053" proc-id="RM22W0E___00002A800001">
<testtitle>READ OUTPUT DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0137 or P0157 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0137 or P0157 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TEO0ETX_13_0060" fin="false">A</down>
<right ref="RM000000TEO0ETX_13_0113" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0060" proc-id="RM22W0E___00002A900001">
<testtitle>CHECK FOR EXHAUST GAS LEAKAGE</testtitle>
<content6 releasenbr="1">
<spec>
<title>OK</title>
<specitem>
<ptxt>No gas leakage.</ptxt>
</specitem>
</spec>
</content6>
<res>
<down ref="RM000000TEO0ETX_13_0100" fin="false">OK</down>
<right ref="RM000000TEO0ETX_13_0072" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0100" proc-id="RM22W0E___000026600001">
<testtitle>INSPECT HEATED OXYGEN SENSOR (HEATER RESISTANCE)
</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A180104E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Disconnect the C20 or C56 heated oxygen (HO2) sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>1 (HT1B) - 2 (+B)</ptxt>
</entry>
<entry align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry align="center">
<ptxt>11 to 16 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1 (HT1B) - 4 (E1)</ptxt>
</entry>
<entry align="center">
<ptxt>Always</ptxt>
</entry>
<entry align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1 (HT2B) - 2 (+B)</ptxt>
</entry>
<entry align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry align="center">
<ptxt>11 to 16 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1 (HT2B) - 4 (E1)</ptxt>
</entry>
<entry align="center">
<ptxt>Always</ptxt>
</entry>
<entry align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000000TEO0ETX_13_0101" fin="false">OK</down>
<right ref="RM000000TEO0ETX_13_0102" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0101" proc-id="RM22W0E___000026B00001">
<testtitle>INSPECT INTEGRATION RELAY (EFI RELAY)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the integration relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Inspect the EFI MAIN fuse.</ptxt>
<test2>
<ptxt>Remove the EFI MAIN fuse from the integration relay.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.37in"/>
<colspec colname="COL3" colwidth="2.34in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>EFI MAIN fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
<test2>
<ptxt>Reinstall the  EFI MAIN fuse.</ptxt>
</test2>
</test1>
<figure>
<graphic graphicname="A148742E04" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Inspect the EFI relay.</ptxt>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC4" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>1C-1 - 1B-4</ptxt>
</entry>
<entry>
<ptxt>No battery voltage applied to terminals 1B-2 and 1B-3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Battery voltage applied to terminals 1B-2 and 1B-3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6><res>
<down ref="RM000000TEO0ETX_13_0063" fin="false">OK</down>
<right ref="RM000000TEO0ETX_13_0073" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0063" proc-id="RM22W0E___00002AA00001">
<testtitle>CHECK HARNESS AND CONNECTOR (HEATED OXYGEN SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A187312E02" width="2.775699831in" height="7.795582503in"/>
</figure>
<test1>
<ptxt>Disconnect the C20 or C56 HO2 sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C20-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C45 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C20-1 (HT1B) - C45-45 (HT1B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C20-3 (OX1B) - C45-114 (OX1B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C20-4 (E1) - C45-115 (EX1B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-1 (HT2B) - C45-44 (HT2B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-3 (OX2B) - C45-112 (OX2B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-4 (E1) - C45-113 (EX2B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C20-1 (HT1B) or C45-45 (HT1B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C20-3 (OX1B) or C45-114 (OX1B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-1 (HT2B) or C45-44 (HT2B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-3 (OX2B) or C45-112 (OX2B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<figure>
<graphic graphicname="A164717E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content6>
<res>
<down ref="RM000000TEO0ETX_13_0064" fin="false">OK</down>
<right ref="RM000000TEO0ETX_13_0074" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0064" proc-id="RM22W0E___00002AB00001">
<testtitle>REPLACE HEATED OXYGEN SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the heated oxygen sensor (See page <xref label="Seep01" href="RM0000031YL01PX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TEO0ETX_13_0065" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0065" proc-id="RM22W0E___00002AC00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</test1>
<test1>
<ptxt>Input DTCs: P0137 or P0157.</ptxt>
</test1>
<test1>
<ptxt>Check that DTC MONITOR is NORMAL. If DTC MONITOR is INCOMPLETE, perform the drive pattern adding the vehicle speed.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NORMAL</ptxt>
<ptxt>(No DTC output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABNORMAL</ptxt>
<ptxt>(P0137 or P0157 detected)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TEO0ETX_13_0078" fin="true">A</down>
<right ref="RM000000TEO0ETX_13_0075" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0072">
<testtitle>REPAIR OR REPLACE EXHAUST GAS LEAKAGE POINT</testtitle>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0102">
<testtitle>REPLACE HEATED OXYGEN SENSOR<xref label="Seep01" href="RM0000031YL01PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0073">
<testtitle>REPLACE INTEGRATION RELAY</testtitle>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0074">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0075">
<testtitle>REPLACE AIR FUEL RATIO SENSOR<xref label="Seep01" href="RM0000031YH01RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0078">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000TEO0ETX_13_0113">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000032SF053X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>