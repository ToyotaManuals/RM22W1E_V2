<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000XCT0WYX" category="C" type-id="302I3" name-id="ES16D9-001" from="201301" to="201308">
<dtccode>P0504</dtccode>
<dtcname>Brake Switch "A" / "B" Correlation</dtcname>
<subpara id="RM000000XCT0WYX_01" type-id="60" category="03" proc-id="RM22W0E___000026O00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The stop light switch is a duplex system that transmits two signals: STP and ST1-. These two signals are used by the ECM to monitor whether or not the brake system is working properly. If the signals, which indicate the brake pedal is being depressed and released, are detected simultaneously, the ECM interprets this as a malfunction in the stop light switch and stores the DTC.</ptxt>
<atten4>
<ptxt>The normal signal conditions are as shown in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Signal (ECM Terminal)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake Pedal Released</ptxt>
</entry>
<entry valign="middle">
<ptxt>In Transition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake Pedal Depressed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>STP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ST1-</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>[OFF] denotes ground potential.</ptxt>
</item>
<item>
<ptxt>[ON] denotes battery potential (+B).</ptxt>
</item>
<item>
<ptxt>On the GTS, both the Data List items Stop Light Switch and ST1 are ON when the brake pedal is depressed because the ST1 indication characteristic is opposite to the Stop Light Switch indication.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0504</ptxt>
</entry>
<entry valign="middle">
<ptxt>Conditions (a), (b) and (c) continue for 0.5 seconds or more (1 trip detection logic):</ptxt>
<ptxt>(a) The engine switch is on (IG).</ptxt>
<ptxt>(b) The brake pedal is released.</ptxt>
<ptxt>(c) The STP signal is OFF when the ST1- signal is OFF.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in stop light switch signal circuit</ptxt>
</item>
<item>
<ptxt>STOP fuse</ptxt>
</item>
<item>
<ptxt>IGN fuse</ptxt>
</item>
<item>
<ptxt>Stop light switch</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XCT0WYX_02" type-id="32" category="03" proc-id="RM22W0E___000026P00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A179962E06" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XCT0WYX_03" type-id="51" category="05" proc-id="RM22W0E___000026Q00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>STP signal conditions can be checked using the GTS.</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Connect the tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG).</ptxt>
</item>
<item>
<ptxt>Turn the tester on.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Stop Light Switch and ST1.</ptxt>
</item>
<item>
<ptxt>Check the Data List indication when the brake pedal is depressed and released.</ptxt>
</item>
</list2>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Brake Pedal Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Stop Light Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ST1</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XCT0WYX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XCT0WYX_04_0001" proc-id="RM22W0E___000026R00000">
<testtitle>CHECK STOP LIGHT SWITCH ASSEMBLY (B+ VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B158579E19" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the A30 stop light switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A30-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A30-4 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XCT0WYX_04_0002" fin="false">OK</down>
<right ref="RM000000XCT0WYX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XCT0WYX_04_0002" proc-id="RM22W0E___000026S00000">
<testtitle>INSPECT STOP LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C171632E13" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Remove the stop light switch.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.39in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin not pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pin pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>3 - 4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin not pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pin pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XCT0WYX_04_0003" fin="false">OK</down>
<right ref="RM000000XCT0WYX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XCT0WYX_04_0003" proc-id="RM22W0E___000026T00000">
<testtitle>CHECK ECM (STP AND ST1 - VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A112585E88" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the A38 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="left">
<ptxt>A38-35 (ST1-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 3 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="left">
<ptxt>A38-36 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 3 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XCT0WYX_04_0007" fin="true">OK</down>
<right ref="RM000000XCT0WYX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XCT0WYX_04_0005">
<testtitle>REPLACE STOP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM0000038XP00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XCT0WYX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XCT0WYX_04_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XCT0WYX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>