<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000F" variety="S000F">
<name>1GR-FE FUEL</name>
<ttl id="12008_S000F_7C3GA_T009D" variety="T009D">
<name>FUEL INJECTOR</name>
<para id="RM000000Q4609MX" category="A" type-id="30014" name-id="FU953-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000000Q4609MX_02" type-id="01" category="01">
<s-1 id="RM000000Q4609MX_02_0048" proc-id="RM22W0E___00004AK00000">
<ptxt>INSTALL FUEL INJECTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new insulator to each fuel injector.</ptxt>
<figure>
<graphic graphicname="A223045E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>New Insulator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>New O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Apply a light coat of spindle oil or gasoline to new O-rings and install one to each fuel injector.</ptxt>
</s2>
<s2>
<ptxt>Install the 6 injectors.</ptxt>
<s3>
<ptxt>While turning each fuel injector left and right, install it to the fuel delivery pipe.</ptxt>
<figure>
<graphic graphicname="A223046E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Outward</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100137" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Position the fuel injectors with the connectors facing outward.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000Q4609MX_02_0049" proc-id="RM22W0E___00004AL00000">
<ptxt>INSTALL FUEL DELIVERY PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the fuel delivery pipe together with the 6 fuel injectors on the intake manifold.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install the 4 bolts, which are used to hold the fuel delivery pipe in place, to the intake manifold.</ptxt>
</s2>
<s2>
<ptxt>Check that the fuel injectors rotate smoothly.</ptxt>
<figure>
<graphic graphicname="A223047E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the fuel injectors do not rotate smoothly, replace the O-ring of any injector that does not rotate smoothly.</ptxt>
</s2>
<s2>
<ptxt>Position the fuel injectors with the connectors facing outward.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 6 fuel injector connectors.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000Q4609MX_02_0050" proc-id="RM22W0E___00005QU00000">
<ptxt>CONNECT NO. 2 FUEL PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 2 fuel pipe sub-assembly (See page <xref label="Seep01" href="RM0000028RU03VX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000Q4609MX_02_0051" proc-id="RM22W0E___00005QV00000">
<ptxt>CONNECT NO. 1 FUEL PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 1 fuel pipe sub-assembly (See page <xref label="Seep01" href="RM0000028RU03VX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000Q4609MX_02_0074" proc-id="RM22W0E___00005QW00000">
<ptxt>INSTALL INTAKE AIR SURGE TANK</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000456B00PX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000000Q4609MX_02_0091" proc-id="RM22W0E___000047800000">
<ptxt>INSPECT FOR FUEL LEAK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Make sure that there are no fuel leaks after performing maintenance on the fuel system.</ptxt>
<s3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Fuel Pump / Speed.</ptxt>
</s3>
<s3>
<ptxt>Check that there are no leaks from the fuel system.</ptxt>
<ptxt>If there are fuel leaks, repair or replace parts as necessary.</ptxt>
</s3>
<s3>
<ptxt>Turn the ignition switch off.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the GTS from the DLC3.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>