<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SU_T00LX" variety="T00LX">
<name>TRANSMITTER BATTERY (w/o Entry and Start System)</name>
<para id="RM000003ZV3016X" category="A" type-id="30019" name-id="DL4RX-05" from="201301">
<name>REPLACEMENT</name>
<subpara id="RM000003ZV3016X_02" type-id="11" category="10" proc-id="RM22W0E___0000EC200000">
<content3 releasenbr="1">
<atten3>
<ptxt>Take extra care when handling these precision electronic components.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM000003ZV3016X_01" type-id="01" category="01">
<s-1 id="RM000003ZV3016X_01_0001" proc-id="RM22W0E___0000EBW00000">
<ptxt>REMOVE TRANSMITTER HOUSING COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Twist a screwdriver in the direction of the arrow in the illustration and remove the transmitter housing cover.</ptxt>
<figure>
<graphic graphicname="B225782E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not forcibly pry open the cover.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000003ZV3016X_01_0002" proc-id="RM22W0E___0000EBX00000">
<ptxt>REMOVE DOOR CONTROL TRANSMITTER MODULE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a precision screwdriver, remove the door control transmitter module.</ptxt>
<figure>
<graphic graphicname="B137124E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When replacing the transmitter battery, before starting work, remove static electricity that has built up in the body by touching, for example, the vehicle to prevent the door control transmitter module set from being damaged.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000003ZV3016X_01_0003" proc-id="RM22W0E___0000EBY00000">
<ptxt>REMOVE TRANSMITTER BATTERY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Twist a coin in the direction of the arrow in the illustration and remove the transmitter battery cover.</ptxt>
<figure>
<graphic graphicname="B102246" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When replacing the transmitter battery, before starting work, remove static electricity that has built up in the body by touching, for example, the vehicle to prevent the door control transmitter module set from being damaged.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the battery (lithium battery: CR2016).</ptxt>
<figure>
<graphic graphicname="B102247" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the transmitter battery, before starting work, remove static electricity that has built up in the body by touching, for example, the vehicle to prevent the door control transmitter module set from being damaged.</ptxt>
</item>
<item>
<ptxt>Do not push the terminals with your finger.</ptxt>
</item>
<item>
<ptxt>Do not forcibly pry up the battery. The terminals may become damaged.</ptxt>
</item>
<item>
<ptxt>Do not touch the battery with wet hands. Water may cause rust.</ptxt>
</item>
<item>
<ptxt>Do not touch or move any components inside the transmitter. the transmitter may cease to work.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003ZV3016X_01_0004" proc-id="RM22W0E___0000EBZ00000">
<ptxt>INSTALL TRANSMITTER BATTERY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new battery (lithium battery: CR2016) with the positive (+) side facing upward as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B102768" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the transmitter battery, before starting work, remove static electricity that has built up in the body by touching, for example, the vehicle to prevent the door control transmitter module set from being damaged.</ptxt>
</item>
<item>
<ptxt>Be careful not to bend the transmitter battery electrode during insertion.</ptxt>
</item>
<item>
<ptxt>Keep the transmitter battery cover interior free of dust and oil.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the transmitter battery cover.</ptxt>
<figure>
<graphic graphicname="B137137" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When replacing the transmitter battery, before starting work, remove static electricity that has built up in the body by touching, for example, the vehicle to prevent the door control transmitter module set from being damaged.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003ZV3016X_01_0005" proc-id="RM22W0E___0000EC000000">
<ptxt>INSTALL DOOR CONTROL TRANSMITTER MODULE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the door control transmitter module.</ptxt>
<figure>
<graphic graphicname="B137125" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When replacing the transmitter battery, before starting work, remove static electricity that has built up in the body by touching, for example, the vehicle to prevent the door control transmitter module set from being damaged.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003ZV3016X_01_0006" proc-id="RM22W0E___0000EC100000">
<ptxt>INSTALL TRANSMITTER HOUSING COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the transmitter housing cover to the transmitter housing case.</ptxt>
<figure>
<graphic graphicname="B225783" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Check that the transmitter LED illuminates 3 times when a switch is pressed 3 times.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>