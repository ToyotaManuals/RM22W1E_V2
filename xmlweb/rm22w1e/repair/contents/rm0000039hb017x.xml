<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3XV_T00QY" variety="T00QY">
<name>POWER BACK DOOR DRIVE UNIT</name>
<para id="RM0000039HB017X" category="A" type-id="80001" name-id="DH3E1-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM0000039HB017X_01" type-id="01" category="01">
<s-1 id="RM0000039HB017X_01_0024" proc-id="RM22W0E___0000IKA00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039HB017X_01_0023" proc-id="RM22W0E___0000IK900000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039HB017X_01_0025" proc-id="RM22W0E___0000CGA00000">
<ptxt>REMOVE TONNEAU COVER ASSEMBLY (w/ Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the tonneau cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039HB017X_01_0015" proc-id="RM22W0E___0000IK700000">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY RH (w/ Rear No. 2 Seat)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000391S00VX"/>)</ptxt>
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000039HB017X_01_0008" proc-id="RM22W0E___0000CG300000">
<ptxt>REMOVE REAR STEP COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181673" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to remove the step cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 2 claws and remove the step cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039HB017X_01_0009" proc-id="RM22W0E___0000CG400000">
<ptxt>REMOVE REAR DOOR SCUFF PLATE RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM0000039HB017X_01_0010" proc-id="RM22W0E___0000CG200000">
<ptxt>REMOVE REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 6 clips and remove the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039HB017X_01_0011" proc-id="RM22W0E___0000CG500000">
<ptxt>REMOVE REAR SEAT COVER CAP RH (w/ Rear No. 2 Seat)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190187E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to remove the rear seat cover cap on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 3 claws and remove the rear seat cover cap.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039HB017X_01_0012" proc-id="RM22W0E___0000CG600000">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182645" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When removing the front quarter trim panel, operate the reclining adjuster release handle and move the No. 1 rear seat to the position shown in the illustration.</ptxt>
</atten4>
<s2>
<figure>
<graphic graphicname="B181688" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 3 claws and remove the cover.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B181690" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and rear No. 1 seat belt anchor.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B181692" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>w/ Rear No. 2 Seat, except Face to Face Seat Type:</ptxt>
<s3>
<ptxt>Detach the 3 claws and remove the cover.  </ptxt>
</s3>
<s3>
<figure>
<graphic graphicname="B181693" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and rear No. 2 seat belt anchor.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Rear No. 2 Seat, except Face to Face Seat Type:</ptxt>
<s3>
<ptxt>Remove the clip and bolt.</ptxt>
</s3>
<s3>
<ptxt>Detach the 16 clips and 2 claws.</ptxt>
</s3>
<s3>
<ptxt>w/o Rear Air Conditioning System:</ptxt>
<ptxt>Disconnect the rear seat lock control lever cable and then remove the quarter trim panel.</ptxt>
</s3>
<s3>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<ptxt>Disconnect the thermistor connector and rear seat lock control lever cable, and then remove the quarter trim panel.</ptxt>
<figure>
<graphic graphicname="B186890" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Rear No. 2 Seat or w/ Rear No. 2 Seat, for Face to Face Seat Type:</ptxt>
<s3>
<ptxt>Remove the clip.</ptxt>
</s3>
<s3>
<ptxt>Detach the 16 clips and 2 claws, and remove the quarter trim panel.</ptxt>
<figure>
<graphic graphicname="B190220" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Tonneau Cover:</ptxt>
<s3>
<ptxt>Remove the screw and clip.</ptxt>
</s3>
<s3>
<ptxt>Detach the 16 clips and 2 claws, and remove the quarter trim panel.</ptxt>
<figure>
<graphic graphicname="B186438" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000039HB017X_01_0016" proc-id="RM22W0E___0000CGP00000">
<ptxt>REMOVE CENTER BACK DOOR GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 5 clips and 4 claws, and remove the center back door garnish.</ptxt>
<figure>
<graphic graphicname="B313300" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039HB017X_01_0017" proc-id="RM22W0E___0000CGS00000">
<ptxt>REMOVE BACK DOOR SIDE GARNISH RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Power Back Door:</ptxt>
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>w/ Power Back Door:</ptxt>
<figure>
<graphic graphicname="B313302" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Detach the clip and 4 claws, and remove the back door side garnish RH.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000039HB017X_01_0018" proc-id="RM22W0E___0000HMX00000">
<ptxt>REMOVE BACK DOOR SERVICE HOLE COVER RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Move the back door to a half-open position so that the hole in the center of the back door service hole cover RH is aligned lengthwise with the power back door rod.</ptxt>
<figure>
<graphic graphicname="B309323E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Back Door Rod</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hole of Back Door Service Hole Cover RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Back Door is Half-open</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 2 clips and separate the back door service hole cover RH, passing the power back door rod through the hole of the back door service hole cover RH.</ptxt>
<figure>
<graphic graphicname="B266396" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>If the back door is in a fully-open position, the power back door rod will interfere with the hole of the back door service hole cover RH, so do not perform this operation with the back door in a fully open position.</ptxt>
</atten3>
<atten4>
<ptxt>If any of the clips have remained on the back door, remove the clips from the back door and install them to the back door service hole cover RH.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the ball joint bolt, power back door rod and back door stay plate.</ptxt>
<figure>
<graphic graphicname="B193888" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the back door service hole cover RH from the power back door rod.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039HB017X_01_0013" proc-id="RM22W0E___0000HLA00000">
<ptxt>REMOVE REAR UPPER PILLAR GARNISH RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B194465" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws and open the seat belt shoulder anchor cover.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B195963" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and seat belt shoulder anchor.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B184328" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>w/o Power Back Door:</ptxt>
<s3>
<ptxt>Detach the 5 clips and remove the rear upper pillar garnish.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B194836E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>w/ Power Back Door:</ptxt>
<s3>
<ptxt>Detach the 5 clips.</ptxt>
</s3>
<s3>
<ptxt>Pass the power back door rod through the rear upper pillar garnish, and remove the rear upper pillar garnish.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000039HB017X_01_0019" proc-id="RM22W0E___0000IK800000">
<ptxt>REMOVE POWER BACK DOOR UNIT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector</ptxt>
<figure>
<graphic graphicname="B192437" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the power back door unit assembly.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>