<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZC_T00SF" variety="T00SF">
<name>LIGHTING SYSTEM</name>
<para id="RM0000011SX0BPX" category="C" type-id="303CD" name-id="LE63Q-01" from="201301" to="201308">
<dtccode>B2416</dtccode>
<dtcname>Height Control Sensor Malfunction</dtcname>
<dtccode>B241A</dtccode>
<dtcname>Rear Height Control Sensor</dtcname>
<subpara id="RM0000011SX0BPX_01" type-id="60" category="03" proc-id="RM22W0E___0000J6Z00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The DTC is stored when the headlight swivel ECU*1 or headlight leveling ECU*2 detects malfunctions in the rear height control sensor LH power source or rear height control sensor LH (w/o Active Height Control Suspension).</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Static Headlight Auto Leveling</ptxt>
</item>
</list1>
<ptxt>The headlight swivel ECU receives signals indicating the height of the vehicle from the suspension control ECU (w/ Active Height Control Suspension).</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>B2416</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>w/o Active Height Control Suspension:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Malfunction in the rear height control sensor LH.</ptxt>
</item>
<item>
<ptxt>Open or short in the rear height control sensor LH power source circuit.</ptxt>
</item>
</list1>
</entry>
<entry morerows="2" valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Rear height control sensor sub-assembly LH (w/o Active Height Control Suspension)</ptxt>
</item>
<item>
<ptxt>Suspension control ECU (w/ Active Height Control Suspension)</ptxt>
</item>
<item>
<ptxt>Headlight swivel ECU assembly (w/ Dynamic Headlight Auto Leveling)</ptxt>
</item>
<item>
<ptxt>Headlight leveling ECU assembly (w/ Static Headlight Auto Leveling)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>w/ Active Height Control Suspension:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Malfunction in the suspension control ECU.</ptxt>
</item>
<item>
<ptxt>Open or short in the suspension control ECU circuit.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B241A</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Malfunction in the rear height control sensor LH.</ptxt>
</item>
<item>
<ptxt>Open or short in the rear height control sensor LH circuit.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000011SX0BPX_02" type-id="32" category="03" proc-id="RM22W0E___0000J7000000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E225964E05" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="E225964E06" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="E247668E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000011SX0BPX_03" type-id="51" category="05" proc-id="RM22W0E___0000J7100000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the headlight leveling ECU*1 or headlight swivel ECU*2 and rear height control sensor LH, initialization of the ECU is necessary (except w/ Dynamic Headlight Auto Leveling) (See page <xref label="Seep01" href="RM000002CE4030X"/>).</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Static Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM0000011SX0BPX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000011SX0BPX_04_0019" proc-id="RM22W0E___0000J7300000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002WP401YX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000002WP401YX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2416 or B241A output does not occur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0021" fin="true">OK</down>
<right ref="RM0000011SX0BPX_04_0028" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0028" proc-id="RM22W0E___0000J7600000">
<testtitle>CHECK VEHICLE TYPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the vehicle type.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>w/ Static Headlight Auto Leveling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/ Dynamic Headlight Auto Leveling, w/o Active Height Control Suspension</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/ Dynamic Headlight Auto Leveling, w/ Active Height Control Suspension</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0029" fin="false">A</down>
<right ref="RM0000011SX0BPX_04_0020" fin="false">B</right>
<right ref="RM0000011SX0BPX_04_0038" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0029" proc-id="RM22W0E___0000J7700000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (REAR HEIGHT CONTROL SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000002WL5020X"/>).</ptxt>
<table pgwide="1">
<title>HL Auto Leveling</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Height Sens Pw Supply Val</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear height control sensor power supply value / 0 to 5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.7 to 5.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rr Height Sens Signal Val</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear height control sensor signal value / 0 to 5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>Approximately 2.5 V (When vehicle level)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The value changes according to the vehicle height.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Normal conditions listed above are displayed.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0032" fin="true">OK</down>
<right ref="RM0000011SX0BPX_04_0030" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0030" proc-id="RM22W0E___0000J7800000">
<testtitle>CHECK HARNESS AND CONNECTOR (HEADLIGHT LEVELING ECU ASSEMBLY - REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E109 headlight leveling ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the N7 rear height control sensor LH connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E109-12 (SBR) - N7-1 (SHB)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E109-19 (SHRL) - N7-2 (SHRL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E109-21 (SGR) - N7-3 (SHG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E109-12 (SBR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E109-19 (SHRL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E109-21 (SGR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0031" fin="false">OK</down>
<right ref="RM0000011SX0BPX_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0031" proc-id="RM22W0E___0000J7900000">
<testtitle>INSPECT REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear height control sensor LH (See page <xref label="Seep02" href="RM000003BJ900EX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the rear height control sensor LH (See page <xref label="Seep01" href="RM000003BJ700DX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0032" fin="true">OK</down>
<right ref="RM0000011SX0BPX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0020" proc-id="RM22W0E___0000J7400000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (REAR HEIGHT CONTROL SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000002WL5020X"/>).</ptxt>
<table pgwide="1">
<title>AFS</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Height Sens Pw Supply Val</ptxt>
</entry>
<entry valign="middle">
<ptxt>Height control sensor power supply value / 0 to 6.25 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.7 to 5.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rr Height Sens Signal Val</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear height control sensor signal value / 0 to 5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>Approximately 2.5 V (When vehicle level)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The value changes according to the vehicle height.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Normal conditions listed above are displayed.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0005" fin="true">A</down>
<right ref="RM0000011SX0BPX_04_0039" fin="true">B</right>
<right ref="RM0000011SX0BPX_04_0001" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0001" proc-id="RM22W0E___0000J7200000">
<testtitle>CHECK HARNESS AND CONNECTOR (HEADLIGHT SWIVEL ECU ASSEMBLY - REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E101 headlight swivel ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the N7 rear height control sensor LH connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E101-18 (SBR) - N7-1 (SHB)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E101-19 (SHRL) - N7-2 (SHRL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E109-21 (SGR) - N7-3 (SHG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E101-18 (SBR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E101-19 (SHRL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E109-21 (SGR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0022" fin="false">OK</down>
<right ref="RM0000011SX0BPX_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0022" proc-id="RM22W0E___0000J7500000">
<testtitle>INSPECT REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear height control sensor LH (See page <xref label="Seep02" href="RM000003BJ900EX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the rear height control sensor LH (See page <xref label="Seep01" href="RM000003BJ700DX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0005" fin="true">A</down>
<right ref="RM0000011SX0BPX_04_0039" fin="true">B</right>
<right ref="RM0000011SX0BPX_04_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0038" proc-id="RM22W0E___0000J7D00000">
<testtitle>CHECK FOR DTC (ACTIVE HEIGHT CONTROL SUSPENSION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000001CU200KX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC "C1712 Front Height Control Sensor LH Circuit Malfunction" and "C1714 Rear Height Control Sensor LH Circuit Malfunction" output does not occur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0033" fin="false">OK</down>
<right ref="RM0000011SX0BPX_04_0036" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0033" proc-id="RM22W0E___0000J7A00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (REAR HEIGHT CONTROL SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000002WL5020X"/>).</ptxt>
<table pgwide="1">
<title>AFS</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Height Sens Pw Supply Val</ptxt>
</entry>
<entry valign="middle">
<ptxt>Height control sensor power supply value / 0 to 6.25 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.7 to 5.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fr Height Sens Signal Val</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front height control sensor signal value / 0 to 5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>Approximately 2.5 V (When vehicle level)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The value changes according to the vehicle height.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rr Height Sens Signal Val</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear height control sensor signal value / 0 to 5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>Approximately 2.5 V (When vehicle level)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The value changes according to the vehicle height.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Normal conditions listed above are displayed.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0005" fin="true">A</down>
<right ref="RM0000011SX0BPX_04_0039" fin="true">B</right>
<right ref="RM0000011SX0BPX_04_0034" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0034" proc-id="RM22W0E___0000J7B00000">
<testtitle>CHECK HARNESS AND CONNECTOR (HEADLIGHT SWIVEL ECU ASSEMBLY - SUSPENSION CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E101 headlight swivel ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the K35 and K36 suspension control ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E101-18 (SBR) - K35-23 (SHB5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E101-19 (SHRL) - K36-28 (SHR2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E101-17 (SHFL) - K35-22 (SHF2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E101-21 (SGR) - K35-11 (SHG5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E101-18 (SBR)  - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E101-19 (SHRL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E101-17 (SHFL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E101-21 (SGR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0035" fin="false">OK</down>
<right ref="RM0000011SX0BPX_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0035" proc-id="RM22W0E___0000J7C00000">
<testtitle>CHECK SUSPENSION CONTROL ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E162713E04" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K35-23 (SHB5) - K35-11 (SHG5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K35-22 (SHF2) - K35-11 (SHG5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Approximately 2.5 V (When vehicle level)</ptxt>
<ptxt>(The value changes according to the vehicle height)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-28 (SHR2) - K35-11 (SHG5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Approximately 2.5 V (When vehicle level)</ptxt>
<ptxt>(The value changes according to the vehicle height)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Suspension Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011SX0BPX_04_0005" fin="true">A</down>
<right ref="RM0000011SX0BPX_04_0039" fin="true">B</right>
<right ref="RM0000011SX0BPX_04_0037" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0007">
<testtitle>REPLACE REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH<xref label="Seep01" href="RM000003BJ900EX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0021">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0005">
<testtitle>REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY<xref label="Seep01" href="RM000003A5S005X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0032">
<testtitle>REPLACE HEADLIGHT LEVELING ECU ASSEMBLY<xref label="Seep01" href="RM000003AV9009X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0036">
<testtitle>GO TO ACTIVE HEIGHT CONTROL SUSPENSION<xref label="Seep01" href="RM000001CTF00LX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0037">
<testtitle>REPLACE SUSPENSION CONTROL ECU<xref label="Seep01" href="RM000003A0D00DX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SX0BPX_04_0039">
<testtitle>REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY<xref label="Seep01" href="RM000003AV9009X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>