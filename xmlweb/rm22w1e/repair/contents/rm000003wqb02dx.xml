<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R3_T00K6" variety="T00K6">
<name>AUDIO AND VISUAL SYSTEM (w/ Navigation System)</name>
<para id="RM000003WQB02DX" category="C" type-id="803XB" name-id="AVCHT-01" from="201301" to="201308">
<dtccode>B15D6</dtccode>
<dtcname>Display Disconnected</dtcname>
<subpara id="RM000003WQB02DX_01" type-id="60" category="03" proc-id="RM22W0E___0000C2400000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The multi-display assembly and multi-media module receiver assembly are connected by an AVC-LAN communication line.</ptxt>
<ptxt>When an AVC-LAN communication error occurs between the display module board and multi-display assembly, these DTCs will be stored.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B15D6*</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A device that is listed in the AVC-LAN connected device record of the master unit is missing.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Multi-display assembly</ptxt>
</item>
<item>
<ptxt>Multi-media module receiver assembly</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*: Even if no fault is present, this DTC may be stored depending on the battery condition or engine start voltage.</ptxt>
</item>
<item>
<ptxt>The multi-media module receiver assembly is the master unit.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000003WQB02DX_02" type-id="32" category="03" proc-id="RM22W0E___0000C2500000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C260261E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003WQB02DX_03" type-id="51" category="05" proc-id="RM22W0E___0000C2600000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003WQB02DX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003WQB02DX_04_0019" proc-id="RM22W0E___0000C2E00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0N6X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WQB02DX_04_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0003" proc-id="RM22W0E___0000C2900000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTC is output again (See page <xref label="Seep01" href="RM0000011BU0N6X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B15D6 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WQB02DX_04_0020" fin="true">OK</down>
<right ref="RM000003WQB02DX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0005" proc-id="RM22W0E___0000C2A00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-DISPLAY ASSEMBLY - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the multi-display assembly connector.</ptxt>
<figure>
<graphic graphicname="E206146E08" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F79-13 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F79-12 (+B2) - F79-13 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F79-23 (IG) - F79-13 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>F79-24 (ACC) - F79-13 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (ACC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Multi-display assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000003WQB02DX_04_0001" fin="false">OK</down>
<right ref="RM000003WQB02DX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0001" proc-id="RM22W0E___0000C2700000">
<testtitle>INSPECT MULTI-MEDIA MODULE RECEIVER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the multi-media module receiver assembly (See page <xref label="Seep01" href="RM000003AHY01SX"/>).</ptxt>
<figure>
<graphic graphicname="E238990E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>45 (TX3+) - 46 (TX3-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WQB02DX_04_0006" fin="false">OK</down>
<right ref="RM000003WQB02DX_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0006" proc-id="RM22W0E___0000C2B00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA MODULE RECEIVER ASSEMBLY - MULTI-DISPLAY ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F77 multi-media module receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the F79 multi-display assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>F77-45 (TX3+) - F79-7 (TX+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>F77-46 (TX3-) - F79-19 (TX-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>F77-45 (TX3+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>F77-46 (TX3-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WQB02DX_04_0002" fin="false">OK</down>
<right ref="RM000003WQB02DX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0002" proc-id="RM22W0E___0000C2800000">
<testtitle>REPLACE MULTI-DISPLAY ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the multi-display assembly with a new or normally functioning one (See page <xref label="Seep01" href="RM000003B6H01RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WQB02DX_04_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0012" proc-id="RM22W0E___0000C2C00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0N6X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WQB02DX_04_0015" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0015" proc-id="RM22W0E___0000C2D00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTC is output again (See page <xref label="Seep01" href="RM0000011BU0N6X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B15D6 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WQB02DX_04_0022" fin="true">OK</down>
<right ref="RM000003WQB02DX_04_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0020">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0016">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0010">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0021">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WQB02DX_04_0022">
<testtitle>END (MULTI-DISPLAY ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>