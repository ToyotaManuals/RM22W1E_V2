<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001S" variety="S001S">
<name>STEERING COLUMN</name>
<ttl id="12038_S001S_7C3QD_T00JG" variety="T00JG">
<name>POWER TILT AND POWER TELESCOPIC STEERING COLUMN SYSTEM</name>
<para id="RM000000ZDY05YX" category="C" type-id="3052X" name-id="SR0XF-34" from="201301" to="201308">
<dtccode>B2624</dtccode>
<dtcname>Vehicles Speed Malfunction</dtcname>
<subpara id="RM000000ZDY05YX_01" type-id="60" category="03" proc-id="RM22W0E___0000BFK00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The multiplex tilt and telescopic ECU forms a network with the ECUs of other systems through the communication bus. Each ECU informs the other ECUs that it is connected to the network by outputting a specified signal (periodic signal) to the communication bus on a regular schedule. The multiplex tilt and telescopic ECU receives vehicle speed signals from the combination meter via the communication bus to prevent improper operation of the auto away/return function during driving.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.17in"/>
<colspec colname="COL2" colwidth="2.96in"/>
<colspec colname="COL3" colwidth="2.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2624</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>The auto away function is disabled with the last vehicle speed signal* at 10 km/h (6 mph) or more.</ptxt>
</item>
<item>
<ptxt>Vehicle speed signal is lost for 5 seconds or more and the auto away/return function is disabled.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>CAN communication system </ptxt>
</item>
<item>
<ptxt>Meter/gauge system</ptxt>
</item>
<item>
<ptxt>Multiplex tilt and telescopic ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: If the engine switch is turned off immediately after the vehicle comes to a sudden stop, the last vehicle speed signal may not be 0 km/h (0 mph).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000ZDY05YX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000ZDY05YX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000ZDY05YX_04_0001" proc-id="RM22W0E___0000BFL00000">
<testtitle>CHECK OTHER DTC OUTPUT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if DTC B2621 is output (See page <xref label="Seep01" href="RM000000XZH055X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>B2621 is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>B2621 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000ZDY05YX_04_0002" fin="false">A</down>
<right ref="RM000000ZDY05YX_04_0003" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000ZDY05YX_04_0002" proc-id="RM22W0E___0000BFM00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (SPEED SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the speed signal is functioning properly.</ptxt>
<table pgwide="1">
<title>Tilt &amp; Telescopic</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.46in"/>
<colspec colname="COL2" colwidth="2.30in"/>
<colspec colname="COL3" colwidth="1.75in"/>
<colspec colname="COL4" colwidth="1.57in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Freeze Speed Info</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle speed data/min.: 0 km/h (0 mph), max.: 255 km/h (158 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Actual vehicle speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>Tester indication is normal</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Tester indication is abnormal</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>A vehicle speed of 3 km/h (2 mph) or more may be displayed on the intelligent tester screen immediately after the vehicle comes to a sudden stop. This is caused by a delay in vehicle signal input via the communication bus and is not a malfunction.</ptxt>
</atten4>
</content6>
<res>
<down ref="RM000000ZDY05YX_04_0005" fin="true">A</down>
<right ref="RM000000ZDY05YX_04_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000ZDY05YX_04_0005">
<testtitle>REPLACE MULTIPLEX TILT AND TELESCOPIC ECU<xref label="Seep01" href="RM0000039SD012X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000ZDY05YX_04_0003">
<testtitle>REPAIR CIRCUIT INDICATED BY OUTPUT CODE<xref label="Seep01" href="RM000000XZC059X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000ZDY05YX_04_0004">
<testtitle>GO TO METER/GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L03NX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>