<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12057_S002A" variety="S002A">
<name>PRE-CRASH SAFETY</name>
<ttl id="12057_S002A_7C3UG_T00NJ" variety="T00NJ">
<name>PRE-CRASH SAFETY SYSTEM</name>
<para id="RM000002JR000VX" category="C" type-id="802CE" name-id="PC0TN-03" from="201301">
<dtccode>B2074</dtccode>
<dtcname>Driver Side Seat Belt Buckle Switch Circuit Malfunction</dtcname>
<subpara id="RM000002JR000VX_01" type-id="60" category="03" proc-id="RM22W0E___0000FP800000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when there is a malfunction in the front seat inner belt buckle switch circuit for the driver side. The seat belt control ECU receives buckle switch condition information from the center airbag sensor via the CAN communication line and controls the pre-crash safety system.</ptxt>
<ptxt>When DTC B2074 is stored, DTC B1655 is also output.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B2074</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Malfunction in the front seat inner belt (buckle switch) circuit.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Front seat inner belt LH assembly</ptxt>
</item>
<item>
<ptxt>Center airbag sensor</ptxt>
</item>
<item>
<ptxt>Seat belt control ECU</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002JR000VX_02" type-id="32" category="03" proc-id="RM22W0E___0000FP900000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E149011E03" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002JR000VX_03" type-id="51" category="05" proc-id="RM22W0E___0000FPA00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>If DTC B1655 is not cleared, DTC B2074 continues to be output.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002JR000VX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002JR000VX_04_0020" proc-id="RM22W0E___0000FPB00000">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally (See page <xref label="Seep01" href="RM000001RSO08CX"/> for LHD, <xref label="Seep02" href="RM000001RSO08DX"/> for RHD).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(DTC U0151 is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>DTC U0151: Lost Communication with Airbag ECU </ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002JR000VX_04_0021" fin="false">A</down>
<right ref="RM000002JR000VX_04_0024" fin="true">B</right>
<right ref="RM000002JR000VX_04_0026" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002JR000VX_04_0021" proc-id="RM22W0E___0000FPC00000">
<testtitle>CHECK DTC (AIRBAG SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Body / SRS Airbag / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000VVL03NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000VVL03NX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1655 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>DTC B1655: Driver Side Seat Belt Buckle Switch Circuit Malfunction</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002JR000VX_04_0022" fin="false">OK</down>
<right ref="RM000002JR000VX_04_0025" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002JR000VX_04_0022" proc-id="RM22W0E___0000FPD00000">
<testtitle>CHECK DTC (PRE-CRASH SAFETY SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Body / Pre-Crash / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000VVL03NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000VVL03NX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
<ptxt>(DTC B2074 is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
<ptxt>(for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002JR000VX_04_0023" fin="true">A</down>
<right ref="RM000002JR000VX_04_0011" fin="true">B</right>
<right ref="RM000002JR000VX_04_0027" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002JR000VX_04_0023">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002JR000VX_04_0024">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (DIAGNOSIS SYSTEM)<xref label="Seep01" href="RM000001RSW03JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002JR000VX_04_0026">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (DIAGNOSIS SYSTEM)<xref label="Seep01" href="RM000001RSW03KX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002JR000VX_04_0025">
<testtitle>GO TO AIRBAG SYSTEM (DTC B1655)<xref label="Seep01" href="RM000002P5G03TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002JR000VX_04_0011">
<testtitle>REPLACE SEAT BELT CONTROL ECU<xref label="Seep01" href="RM0000039P900MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002JR000VX_04_0027">
<testtitle>REPLACE SEAT BELT CONTROL ECU<xref label="Seep01" href="RM000003A6M004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>