<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000T8M0PIX" category="C" type-id="300YC" name-id="ESUAM-04" from="201301" to="201308">
<dtccode>P0505</dtccode>
<dtcname>Idle Control System Malfunction</dtcname>
<subpara id="RM000000T8M0PIX_01" type-id="60" category="03" proc-id="RM22W0E___00001B800000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The idling speed is controlled by the ETCS (Electronic Throttle Control System). The ETCS is comprised of: 1) the one valve type throttle body; 2) the throttle actuator, which operates the throttle valve; 3) the throttle position sensor, which detects the opening angle of the throttle valve; 4) the accelerator pedal position sensor, which detects the accelerator pedal position; and 5) the ECM, which controls the ETCS. Based on the target idling speed, the ECM controls the throttle actuator to provide the proper throttle valve opening angle.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0505</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idling speed continues to vary greatly from the target idling speed (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ETCS</ptxt>
</item>
<item>
<ptxt>Air induction system</ptxt>
</item>
<item>
<ptxt>PCV hose connections</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000T8M0PIX_02" type-id="64" category="03" proc-id="RM22W0E___00001B900000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM monitors the idling speed and idling air flow volume to conduct Idle Speed Control (ISC). The ECM determines that the ISC system is malfunctioning if the following condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>After driving at a vehicle speed of 10 km/h (6.25 mph) or more, the difference between the target and actual engine idling speed exceeds the threshold 5 times or more during a driving cycle, and then the system determines that the IAC flow rate learned value is stuck at the upper or lower limit, or that the IAC flow rate learned value has been changed by an amount that exceeds the threshold.</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="A121611E06" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000T8M0PIX_08" type-id="73" category="03" proc-id="RM22W0E___00001BG00000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A232053E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on [A].</ptxt>
</item>
<item>
<ptxt>Start the engine and warm it up until the engine coolant temperature is 75°C (167°F) or higher with all the accessories switched off [B].</ptxt>
</item>
<item>
<ptxt>Idle the engine for 1 minute or more [C].</ptxt>
<atten4>
<ptxt>In order to keep the idling stable, turn off the A/C and all other electric loads and do not perform any shift operations.</ptxt>
</atten4>
</item>
<item>
<ptxt>Accelerate the vehicle to 10 km/h (6.25 mph) or more, and then idle the engine for 20 seconds or more [D].</ptxt>
<atten2>
<ptxt>When performing the confirmation driving pattern, obey all speed limits and traffic laws.</ptxt>
</atten2>
</item>
<item>
<ptxt>Repeat step [D] 5 times.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes [E].</ptxt>
</item>
<item>
<ptxt>Read the pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a pending DTC is output, the system is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If a pending DTC is not output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P0505.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows INCOMPLETE or N/A, perform steps [D] and [E] again.</ptxt>
</item>
</list1>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000T8M0PIX_06" type-id="51" category="05" proc-id="RM22W0E___00001BA00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>The following conditions may also cause DTC P0505 to be stored:</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>The floor carpet overlapping slightly onto the accelerator pedal, causing the accelerator pedal to be slightly depressed and therefore the throttle valve position to be slightly open.</ptxt>
</item>
<item>
<ptxt>The accelerator pedal being not fully released.</ptxt>
</item>
</list2>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Refer to "Data List / Active Test" [Engine Speed, ISC Feedback Value and ISC Learning Value] (See page <xref label="Seep01" href="RM000000SXS097X"/>).</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000T8M0PIX_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000T8M0PIX_07_0001" proc-id="RM22W0E___00001BB00000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0505)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0505 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0505 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0505 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000T8M0PIX_07_0002" fin="false">A</down>
<right ref="RM000000T8M0PIX_07_0005" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000T8M0PIX_07_0002" proc-id="RM22W0E___00001BC00000">
<testtitle>CHECK PCV HOSE CONNECTIONS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the PCV hose connections (See page <xref label="Seep01" href="RM00000398V007X_01_0003"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>PCV hose is connected correctly and is not damaged.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8M0PIX_07_0003" fin="false">OK</down>
<right ref="RM000000T8M0PIX_07_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8M0PIX_07_0003" proc-id="RM22W0E___00001BD00000">
<testtitle>CHECK AIR INDUCTION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the air induction system for vacuum leaks (See page <xref label="Seep01" href="RM00000311201OX_01_0001"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No leaks in air induction system.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8M0PIX_07_0004" fin="false">OK</down>
<right ref="RM000000T8M0PIX_07_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8M0PIX_07_0004" proc-id="RM22W0E___00001BE00000">
<testtitle>INSPECT THROTTLE BODY ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for contamination between the throttle valve and housing, and check that the throttle valve moves smoothly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Throttle valve is not contaminated with foreign objects and moves smoothly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8M0PIX_07_0013" fin="false">OK</down>
<right ref="RM000000T8M0PIX_07_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8M0PIX_07_0013" proc-id="RM22W0E___00001BF00000">
<testtitle>CONFIRM WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle in accordance with the driving pattern described in Confirmation Driving Pattern.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0505 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000T8M0PIX_07_0012" fin="true">A</down>
<right ref="RM000000T8M0PIX_07_0015" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000T8M0PIX_07_0005">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000032SF04AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000T8M0PIX_07_0006">
<testtitle>REPAIR OR REPLACE PCV HOSE<xref label="Seep01" href="RM00000436K004X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000T8M0PIX_07_0007">
<testtitle>REPAIR OR REPLACE AIR INDUCTION SYSTEM<xref label="Seep01" href="RM0000030XN01RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000T8M0PIX_07_0008">
<testtitle>REPLACE THROTTLE BODY ASSEMBLY<xref label="Seep01" href="RM000002PQN034X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000T8M0PIX_07_0015">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000T8M0PIX_07_0012">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>