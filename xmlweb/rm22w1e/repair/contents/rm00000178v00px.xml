<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12019_S001C" variety="S001C">
<name>H150F MANUAL TRANSMISSION / TRANSAXLE</name>
<ttl id="12019_S001C_7C3NE_T00GH" variety="T00GH">
<name>MANUAL TRANSMISSION UNIT</name>
<para id="RM00000178V00PX" category="A" type-id="80002" name-id="MT1N4-01" from="201301">
<name>DISASSEMBLY</name>
<subpara id="RM00000178V00PX_01" type-id="01" category="01">
<s-1 id="RM00000178V00PX_01_0038" proc-id="RM22W0E___000091V00000">
<ptxt>REMOVE TRANSMISSION BREATHER SUB-ASSEMBLY (for 1GR-FE)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C178013" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Loosen the hose clamp, and remove the breather hose.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0039" proc-id="RM22W0E___000091W00000">
<ptxt>REMOVE TRANSMISSION BREATHER SUB-ASSEMBLY (for 1VD-FTV)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C178014" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Loosen the hose clamp, and remove the breather hose.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0040" proc-id="RM22W0E___000091X00000">
<ptxt>REMOVE CLUTCH HOUSING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174421E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Remove the 10 bolts and clutch housing.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0041" proc-id="RM22W0E___000091Y00000">
<ptxt>REMOVE FILLER PLUG</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174902" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the filler plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the filler plug.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0092" proc-id="RM22W0E___000093700000">
<ptxt>REMOVE DRAIN PLUG</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172955" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the drain plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the drain plug.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0042" proc-id="RM22W0E___000091Z00000">
<ptxt>REMOVE MANUAL TRANSMISSION CASE PLUG</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174726" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T55 "TORX" socket wrench, remove the transmission case plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the transmission case plug.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0020" proc-id="RM22W0E___000091U00000">
<ptxt>REMOVE BACK-UP LIGHT SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174428E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Using SST, remove the back-up light switch and gasket.</ptxt>
<sst>
<sstitem>
<s-number>09817-16011</s-number>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0043" proc-id="RM22W0E___000092000000">
<ptxt>REMOVE SHIFT POSITION SWITCH (for 1VD-FTV)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174429E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Using SST, remove the shift position switch and gasket.</ptxt>
<sst>
<sstitem>
<s-number>09817-16011</s-number>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0091" proc-id="RM22W0E___000093600000">
<ptxt>REMOVE CONTROL SHAFT COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174728" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 6 bolts and control shaft cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0088" proc-id="RM22W0E___000093300000">
<ptxt>REMOVE REVERSE RESTRICT PIN ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174430" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 restrict pins.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0001" proc-id="RM22W0E___000091S00000">
<ptxt>REMOVE FLOOR SHIFT CONTROL SHIFT LEVER RETAINER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174717" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer, remove the floor shift control shift lever retainer.</ptxt>
<figure>
<graphic graphicname="C174718" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0089" proc-id="RM22W0E___000093400000">
<ptxt>REMOVE NO. 1 REVERSE RESTRICT PIN</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171347" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the slotted pin.</ptxt>
</s2>
<s2>
<ptxt>Remove the No. 1 reverse restrict pin and spring.</ptxt>
<figure>
<graphic graphicname="C171384" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0044" proc-id="RM22W0E___000092100000">
<ptxt>REMOVE TRANSMISSION OIL PUMP COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174391" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 5 bolts and oil pump cover.</ptxt>
</s2>
<s2>
<ptxt>Using a magnet hand, remove the 2 pins.</ptxt>
<figure>
<graphic graphicname="C171196" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the oil pump cover O-ring from the oil pump cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0095" proc-id="RM22W0E___000093A00000">
<ptxt>REMOVE TRANSMISSION OIL PUMP COVER OIL SEAL</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C178035E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, pry out the oil pump cover oil seal.</ptxt>
<atten3>
<ptxt>Be careful not to damage the oil seal and oil pump cover contact surfaces.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0045" proc-id="RM22W0E___000092200000">
<ptxt>REMOVE FRONT BEARING RETAINER (MTM)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171381" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 8 bolts and front bearing retainer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0094" proc-id="RM22W0E___000093900000">
<ptxt>REMOVE TRANSMISSION FRONT BEARING RETAINER OIL SEAL</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171295E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, pry out the oil seal.</ptxt>
<atten3>
<ptxt>Be careful not to damage the oil seal and front bearing retainer contact surfaces.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0002" proc-id="RM22W0E___000091T00000">
<ptxt>REMOVE SHIFT AND SELECT LEVER SHAFT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174901" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a 14 mm hexagon wrench, remove the interlock hole plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts, shift and select lever shaft, shift and select lever and shift lever housing.</ptxt>
<figure>
<graphic graphicname="C174721E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0046" proc-id="RM22W0E___000092300000">
<ptxt>REMOVE TRANSFER ADAPTER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174720" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 11 bolts and transfer adapter.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0047" proc-id="RM22W0E___000092400000">
<ptxt>REMOVE TRANSFER ADAPTER STRAIGHT PIN AND RING PIN</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C178015E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Remove the 4 straight pins from the transfer adapter.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 ring pins from the transfer adapter.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0050" proc-id="RM22W0E___000092500000">
<ptxt>REMOVE TRANSFER ADAPTER PLUG</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174722" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T40 "TORX" socket wrench, remove the transfer adapter plug.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0051" proc-id="RM22W0E___000092600000">
<ptxt>REMOVE MANUAL TRANSMISSION OIL STRAINER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171350E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and oil strainer.</ptxt>
</s2>
<s2>
<ptxt>Remove the O-ring from the oil strainer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0090" proc-id="RM22W0E___000093500000">
<ptxt>REMOVE TRANSMISSION MAGNET</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171351" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the transmission magnet.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0052" proc-id="RM22W0E___000092700000">
<ptxt>REMOVE NO. 2 REVERSE IDLER THRUST WASHER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171200" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the thrust washer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0053" proc-id="RM22W0E___000092800000">
<ptxt>REMOVE REVERSE IDLER GEAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the reverse idler gear.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0054" proc-id="RM22W0E___000092900000">
<ptxt>REMOVE REVERSE IDLER GEAR BUSH OR BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bearing.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0055" proc-id="RM22W0E___000092A00000">
<ptxt>REMOVE REVERSE IDLER THRUST WASHER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the thrust washer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0056" proc-id="RM22W0E___000092B00000">
<ptxt>REMOVE REVERSE IDLER GEAR SHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the reverse idler gear shaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0057" proc-id="RM22W0E___000092C00000">
<ptxt>REMOVE REVERSE IDLER GEAR SHAFT WOODRUFF KEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the woodruff key from the reverse idler gear shaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0058" proc-id="RM22W0E___000092D00000">
<ptxt>REMOVE MANUAL TRANSMISSION CASE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171195" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a snap ring expander, remove the 2 snap rings from the input shaft and counter gear.</ptxt>
</s2>
<s2>
<ptxt>Using a brass bar and hammer, carefully tap the transmission case.</ptxt>
<figure>
<graphic graphicname="C171197" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the transmission case from the intermediate plate.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0060" proc-id="RM22W0E___000092E00000">
<ptxt>REMOVE NO. 1 OIL RECEIVER PIPE (MTM)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171354E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and oil receiver pipe from the transmission case.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0061" proc-id="RM22W0E___000092F00000">
<ptxt>REMOVE TRANSFER ADAPTER STRAIGHT PIN</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C178017E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<s2>
<ptxt>Remove the 4 straight pins from the transmission case.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0093" proc-id="RM22W0E___000093800000">
<ptxt>REMOVE INTERMEDIATE PLATE STRAIGHT PIN</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C178016E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<s2>
<ptxt>Remove the 4 straight pins from the intermediate plate.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0063" proc-id="RM22W0E___000092G00000">
<ptxt>REMOVE MANUAL TRANSMISSION CASE RECEIVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and oil receiver from the intermediate plate.</ptxt>
<figure>
<graphic graphicname="C171355" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0064" proc-id="RM22W0E___000092H00000">
<ptxt>REMOVE SHIFT DETENT BALL PLUG</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171201" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T40 "TORX" socket wrench, remove the 4 ball plugs.</ptxt>
</s2>
<s2>
<ptxt>Using a magnet hand, remove the 4 springs and 4 balls.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0065" proc-id="RM22W0E___000092I00000">
<ptxt>REMOVE SHIFT FORK SHAFT SNAP RING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171202" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using 2 screwdrivers and a hammer, tap out the 4 snap rings.</ptxt>
<atten4>
<ptxt>Use a piece of cloth to prevent the snap ring from flying off.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0066" proc-id="RM22W0E___000092J00000">
<ptxt>REMOVE NO. 4 GEAR SHIFT FORK SHAFT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171203" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the No. 4 gear shift fork set bolt.</ptxt>
</s2>
<s2>
<ptxt>Remove the No. 4 gear shift fork shaft and 2 balls.</ptxt>
<figure>
<graphic graphicname="C171204" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0067" proc-id="RM22W0E___000092K00000">
<ptxt>REMOVE REVERSE SHIFT FORK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the reverse shift fork.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0068" proc-id="RM22W0E___000092L00000">
<ptxt>REMOVE NO. 3 GEAR SHIFT FORK SHAFT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171205" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a magnet hand, remove the ball.</ptxt>
</s2>
<s2>
<ptxt>Remove the No. 3 gear shift fork set bolt.</ptxt>
<figure>
<graphic graphicname="C171216" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 3 shift gear fork shaft and reverse shift head.</ptxt>
</s2>
<s2>
<ptxt>Using a magnet hand, remove the interlock pin from the No. 3 gear shift fork shaft.</ptxt>
<figure>
<graphic graphicname="C171357" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0071" proc-id="RM22W0E___000092O00000">
<ptxt>REMOVE NO. 3 GEAR SHIFT FORK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 3 gear shift fork.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0069" proc-id="RM22W0E___000092M00000">
<ptxt>REMOVE NO. 1 GEAR SHIFT FORK SHAFT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171217" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a magnet hand, remove the interlock pin.</ptxt>
</s2>
<s2>
<ptxt>Remove the No. 1 gear shift fork set bolt.</ptxt>
<figure>
<graphic graphicname="C171218" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 1 gear shift fork shaft.</ptxt>
</s2>
<s2>
<ptxt>Using a magnet hand, remove the interlock pin from the No. 1 gear shift fork shaft.</ptxt>
<figure>
<graphic graphicname="C171358" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0072" proc-id="RM22W0E___000092P00000">
<ptxt>REMOVE NO. 1 GEAR SHIFT FORK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 1 gear shift fork.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0070" proc-id="RM22W0E___000092N00000">
<ptxt>REMOVE NO. 2 GEAR SHIFT FORK SHAFT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171219" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a magnet hand, remove the interlock pin.</ptxt>
</s2>
<s2>
<ptxt>Remove the No. 2 gear shift fork set bolt.</ptxt>
<figure>
<graphic graphicname="C171220" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 2 gear shift fork shaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0073" proc-id="RM22W0E___000092Q00000">
<ptxt>REMOVE NO. 2 GEAR SHIFT FORK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 2 gear shift fork.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0074" proc-id="RM22W0E___000092R00000">
<ptxt>REMOVE NO. 3 TRANSMISSION CLUTCH HUB SHAFT SNAP RING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171222" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using 2 screwdrivers and a hammer, tap out the snap ring.</ptxt>
<atten4>
<ptxt>Use a piece of cloth to prevent the snap ring from flying off.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0075" proc-id="RM22W0E___000092S00000">
<ptxt>REMOVE NO. 4 TRANSMISSION CLUTCH HUB</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C271140E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using SST, remove the No. 4 transmission clutch hub.</ptxt>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03080</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0076" proc-id="RM22W0E___000092T00000">
<ptxt>REMOVE REVERSE GEAR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171225" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the reverse synchronizer ring set and reverse gear.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0077" proc-id="RM22W0E___000092U00000">
<ptxt>REMOVE REVERSE SYNCHRONIZER RING SET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the reverse synchronizer ring set from the reverse gear.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0078" proc-id="RM22W0E___000092V00000">
<ptxt>REMOVE REVERSE GEAR BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the reverse gear bearing.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0079" proc-id="RM22W0E___000092W00000">
<ptxt>REMOVE NO. 1 TRANSMISSION HUB SLEEVE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C271141E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the No. 1 transmission hub sleeve, 3 shifting keys, and 3 springs from the No. 4 transmission clutch hub.</ptxt>
<atten4>
<ptxt>Use a piece of cloth to prevent the spring from flying off.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0080" proc-id="RM22W0E___000092X00000">
<ptxt>REMOVE OUTPUT SHAFT REAR BEARING(MTM) RETAINER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171360" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and rear bearing retainer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0082" proc-id="RM22W0E___000092Y00000">
<ptxt>REMOVE COUNTER GEAR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174896" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a snap ring expander, remove the snap ring.</ptxt>
</s2>
<s2>
<ptxt>Using SST, remove the outer race.</ptxt>
<figure>
<graphic graphicname="C171228E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-40011</s-number>
<s-subnumber>09951-04020</s-subnumber>
<s-subnumber>09952-04010</s-subnumber>
<s-subnumber>09953-04030</s-subnumber>
<s-subnumber>09954-04020</s-subnumber>
<s-subnumber>09955-04011</s-subnumber>
<s-subnumber>09957-04010</s-subnumber>
<s-subnumber>09958-04011</s-subnumber>
</sstitem>
</sst>
<atten3>
<ptxt>Do not drop the counter gear. Hold the front side of the counter gear when removing the bearing outer race.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the center bearing and counter gear.</ptxt>
<figure>
<graphic graphicname="C171230" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0084" proc-id="RM22W0E___000092Z00000">
<ptxt>REMOVE INPUT SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171362" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the input shaft from the output shaft.</ptxt>
<atten3>
<ptxt>Do not drop the input shaft bearing.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0085" proc-id="RM22W0E___000093000000">
<ptxt>REMOVE NO. 2 SYNCHRONIZER RING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 2 synchronizer ring from the input shaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0086" proc-id="RM22W0E___000093100000">
<ptxt>REMOVE OUTPUT SHAFT BEARING SHAFT SNAP RING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171364" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a snap ring expander, remove the snap ring.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178V00PX_01_0087" proc-id="RM22W0E___000093200000">
<ptxt>REMOVE OUTPUT SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C171231" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the output shaft from the intermediate plate by pulling on the output shaft and tapping on the intermediate plate with a plastic-faced hammer.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>