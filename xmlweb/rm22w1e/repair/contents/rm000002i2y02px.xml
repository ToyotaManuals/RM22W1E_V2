<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CT_T005W" variety="T005W">
<name>IGNITION COIL AND SPARK PLUG</name>
<para id="RM000002I2Y02PX" category="A" type-id="80001" name-id="ES11GI-001" from="201301">
<name>REMOVAL</name>
<subpara id="RM000002I2Y02PX_01" type-id="01" category="01">
<s-1 id="RM000002I2Y02PX_01_0016" proc-id="RM22W0E___00000Z800000">
<ptxt>REMOVE V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Raise the front of the V-bank cover to detach the 2 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<figure>
<graphic graphicname="A267633E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002I2Y02PX_01_0017" proc-id="RM22W0E___000011F00000">
<ptxt>REMOVE AIR CLEANER CAP AND HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the air cleaner cap and hose.</ptxt>
<s3>
<ptxt>Disconnect the mass air flow meter connector, vacuum hose and No. 2 PCV hose and detach the clamp.</ptxt>
<figure>
<graphic graphicname="A267624" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Loosen the clamp.</ptxt>
</s3>
<s3>
<ptxt>Unfasten the 4 hook clamps, and then remove the bolt and the air cleaner cap and hose.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002I2Y02PX_01_0037" proc-id="RM22W0E___000011L00000">
<ptxt>REMOVE AIR CLEANER CASE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the air cleaner filter element.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A270980" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 bolts and air cleaner case.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002I2Y02PX_01_0033" proc-id="RM22W0E___000011J00000">
<ptxt>REMOVE AIR TUBE (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223569" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts, 2 nuts and air tube.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 gaskets.</ptxt>
<atten3>
<ptxt>Be careful not to damage the installation surface of the gaskets.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002I2Y02PX_01_0024" proc-id="RM22W0E___000011H00000">
<ptxt>REMOVE NO. 1 EMISSION CONTROL VALVE SET (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A267738" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the No. 1 emission control valve set connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 1 air hose from the No. 1 emission control valve set.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223570" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 nuts and No. 1 emission control valve set.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002I2Y02PX_01_0035" proc-id="RM22W0E___000011K00000">
<ptxt>REMOVE NO. 2 AIR TUBE (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223563" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts, 2 nuts and No. 2 air tube.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 gaskets.</ptxt>
<atten3>
<ptxt>Be careful not to damage the installation surface of the gaskets.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002I2Y02PX_01_0025" proc-id="RM22W0E___000011I00000">
<ptxt>REMOVE NO. 2 EMISSION CONTROL VALVE SET (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223562" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the No. 2 emission control valve set connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 3 air hose.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223564" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 nuts and No. 2 emission control valve set.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002I2Y02PX_01_0004" proc-id="RM22W0E___000011E00000">
<ptxt>REMOVE IGNITION COIL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 6 ignition coil connectors.</ptxt>
<figure>
<graphic graphicname="A272605E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>Bank 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>Bank 2</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 6 bolts and 6 ignition coils.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002I2Y02PX_01_0019" proc-id="RM22W0E___000011G00000">
<ptxt>REMOVE SPARK PLUG</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 6 spark plugs.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>