<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZC_T00SF" variety="T00SF">
<name>LIGHTING SYSTEM</name>
<para id="RM000002ZNJ01OX" category="J" type-id="3016S" name-id="LE60J-02" from="201301" to="201308">
<dtccode/>
<dtcname>Hazard Warning Switch Circuit</dtcname>
<subpara id="RM000002ZNJ01OX_01" type-id="60" category="03" proc-id="RM22W0E___0000J4300000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The turn signal flasher receives a hazard warning signal switch information signal, and illuminates the turn signal lights.</ptxt>
</content5>
</subpara>
<subpara id="RM000002ZNJ01OX_02" type-id="32" category="03" proc-id="RM22W0E___0000J4400000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E249796E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002ZNJ01OX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002ZNJ01OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002ZNJ01OX_04_0009" proc-id="RM22W0E___0000J4700000">
<testtitle>CHECK VEHICLE TYPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the vehicle type.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/o Multi-display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ Multi-display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002ZNJ01OX_04_0001" fin="false">A</down>
<right ref="RM000002ZNJ01OX_04_0010" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002ZNJ01OX_04_0001" proc-id="RM22W0E___0000J4500000">
<testtitle>INSPECT HAZARD WARNING SIGNAL SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the hazard warning signal switch (See page <xref label="Seep02" href="RM0000038XL00DX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the hazard warning signal switch (See page <xref label="Seep01" href="RM0000038XJ00AX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002ZNJ01OX_04_0003" fin="false">OK</down>
<right ref="RM000002ZNJ01OX_04_0002" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZNJ01OX_04_0003" proc-id="RM22W0E___0000J4600000">
<testtitle>CHECK HARNESS AND CONNECTOR (HAZARD WARNING SIGNAL SWITCH - TURN SIGNAL FLASHER ASSEMBLY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F11 hazard warning signal switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E19 turn signal flasher connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F11-4 (F) - E19-8 (HAZ)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F11-1 (E) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F11-4 (F) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002ZNJ01OX_04_0004" fin="true">OK</down>
<right ref="RM000002ZNJ01OX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZNJ01OX_04_0010" proc-id="RM22W0E___0000J4800000">
<testtitle>INSPECT MULTI-DISPLAY (HAZARD WARNING SIGNAL SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the multi-display (See page <xref label="Seep01" href="RM000003B6H01RX"/>).</ptxt>
<figure>
<graphic graphicname="E246620E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>21 (HAZ) - Body ground (Multi-display body ground)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hazard warning switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Hazard warning switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002ZNJ01OX_04_0011" fin="false">OK</down>
<right ref="RM000002ZNJ01OX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZNJ01OX_04_0011" proc-id="RM22W0E___0000J4900000">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-DISPLAY [HAZARD WARNING SIGNAL SWITCH] - TURN SIGNAL FLASHER ASSEMBLY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F79 multi-display connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E19 turn signal flasher connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F79-21 (HAZ) - E19-8 (HAZ)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F79-13 (GND1) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F79-21 (HAZ) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002ZNJ01OX_04_0004" fin="true">OK</down>
<right ref="RM000002ZNJ01OX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZNJ01OX_04_0002">
<testtitle>REPLACE HAZARD WARNING SIGNAL SWITCH<xref label="Seep01" href="RM0000038XL00DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZNJ01OX_04_0012">
<testtitle>REPLACE MULTI-DISPLAY (HAZARD WARNING SIGNAL SWITCH)<xref label="Seep01" href="RM000003B6H01RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZNJ01OX_04_0004">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002WKO02DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZNJ01OX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>