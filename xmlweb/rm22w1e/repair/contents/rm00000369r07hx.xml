<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM00000369R07HX" category="C" type-id="803KY" name-id="BCGGI-01" from="201308">
<dtccode>C1403</dtccode>
<dtcname>Rear Speed Sensor RH Malfunction</dtcname>
<dtccode>C1273</dtccode>
<dtcname>Low Output Signal of Rear Speed Sensor RH (Test Mode DTC)</dtcname>
<dtccode>C1274</dtccode>
<dtcname>Low Output Signal of Rear Speed Sensor LH (Test Mode DTC)</dtcname>
<dtccode>C1404</dtccode>
<dtcname>Rear Speed Sensor LH Malfunction</dtcname>
<subpara id="RM00000369R07HX_01" type-id="60" category="03" proc-id="RM22W0E___0000AOI00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1401 and C1402 (See page <xref label="Seep01" href="RM000000XI90WDX_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.93in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1403</ptxt>
<ptxt>C1404</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>At a vehicle speed of 10 km/h (6 mph) or more, the output voltage from one of the speed sensors is less than that from the other sensors for 15 seconds or more.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of less than 10 km/h (6 mph), the output from one of the speed sensors is 0 km/h (0 mph) for 1 second or more.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed less than 10 km/h (6 mph), outputs from both rear speed sensors are 0 km/h (0 mph) for 15 seconds or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Rear speed sensor RH/LH</ptxt>
</item>
<item>
<ptxt>Speed sensor rotor RH/LH</ptxt>
</item>
<item>
<ptxt>Sensor installation</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1273</ptxt>
<ptxt>C1274</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stored only during test mode.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Rear speed sensor RH/LH</ptxt>
</item>
<item>
<ptxt>Speed sensor rotor RH/LH</ptxt>
</item>
<item>
<ptxt>Sensor installation</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTCs C1403 and C1273 are for the rear speed sensor RH.</ptxt>
</item>
<item>
<ptxt>DTCs C1404 and C1274 are for the rear speed sensor LH.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000369R07HX_03" type-id="51" category="05" proc-id="RM22W0E___0000AOJ00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Check the speed sensor signal after cleaning or replacement (See page <xref label="Seep01" href="RM0000054KD001X"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000369R07HX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000369R07HX_06_0001" proc-id="RM22W0E___0000AOK00001">
<testtitle>CHECK REAR SPEED SENSOR INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Check the speed sensor installation (See page <xref label="Seep01" href="RM000001B2E01XX"/>).</ptxt>
<figure>
<graphic graphicname="C144929E49" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>There is no clearance between the sensor and the rear axle.</ptxt>
<ptxt>The installation nut is tightened properly.</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Speed Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>No clearance</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000369R07HX_06_0016" fin="false">OK</down>
<right ref="RM00000369R07HX_06_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369R07HX_06_0016" proc-id="RM22W0E___0000AOM00001">
<testtitle>INSPECT REAR SPEED SENSOR TIP</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear speed sensor (See page <xref label="Seep01" href="RM000001B2G01TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the speed sensor tip.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No scratches, oil, or foreign matter on the sensor tip.</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>If no damage to the speed sensor tip is found during this inspection, do not replace the speed sensor.</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM00000369R07HX_06_0017" fin="false">OK</down>
<right ref="RM00000369R07HX_06_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369R07HX_06_0017" proc-id="RM22W0E___0000AON00001">
<testtitle>INSPECT REAR SPEED SENSOR ROTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear axle shaft together with the parking brake plate (See page <xref label="Seep01" href="RM00000270F014X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the speed sensor rotor.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No scratches, oil, or foreign matter on the rotors.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The rear speed sensor rotor is incorporated into the rear axle hub and bearing assembly.</ptxt>
<ptxt>If the rear speed sensor rotor needs to be replaced, replace it together with the rear axle hub and bearing assembly.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000369R07HX_06_0005" fin="false">OK</down>
<right ref="RM00000369R07HX_06_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369R07HX_06_0005" proc-id="RM22W0E___0000AOL00001">
<testtitle>READ VALUE USING GTS (RR/RL WHEEL SPEED)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.16in"/>
<colspec colname="COL2" colwidth="2.33in"/>
<colspec colname="COL3" colwidth="1.63in"/>
<colspec colname="COLSPEC1" colwidth="1.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>RR Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor RH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor LH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that there is no difference between the speed value output from the speed sensor displayed on the GTS and the speed value displayed on the speedometer when driving the vehicle.</ptxt>
<atten4>
<ptxt>Factors that affect the indicated vehicle speed include tire size, tire inflation and tire wear. The speed indicated on the speedometer has an allowable margin of error. This can be tested using a speedometer tester (calibrated chassis dynamometer). For details about testing and the margin of error, see the reference chart (See page <xref label="Seep01" href="RM000002Z4Q02RX"/>).</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The speed value output from the speed sensor displayed on the GTS is the same as the actual vehicle speed measured using a speedometer tester (calibrated chassis dynamometer).</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000369R07HX_06_0023" fin="true">OK</down>
<right ref="RM00000369R07HX_06_0018" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000369R07HX_06_0018" proc-id="RM22W0E___0000AOO00001">
<testtitle>REPLACE REAR SPEED SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the rear speed sensor (See page <xref label="Seep01" href="RM000001B2G01TX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000369R07HX_06_0019" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000369R07HX_06_0019" proc-id="RM22W0E___0000AOP00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00VX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 40 km/h (25 mph) or more or at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00VX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000369R07HX_06_0011" fin="true">A</down>
<right ref="RM00000369R07HX_06_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000369R07HX_06_0020">
<testtitle>INSTALL REAR SPEED SENSOR CORRECTLY</testtitle>
</testgrp>
<testgrp id="RM00000369R07HX_06_0021">
<testtitle>CLEAN OR REPLACE REAR SPEED SENSOR</testtitle>
</testgrp>
<testgrp id="RM00000369R07HX_06_0022">
<testtitle>CLEAN OR REPLACE REAR SPEED SENSOR ROTOR</testtitle>
</testgrp>
<testgrp id="RM00000369R07HX_06_0023">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000369R07HX_06_0012">
<testtitle>REPLACE REAR AXLE HUB AND BEARING ASSEMBLY<xref label="Seep01" href="RM00000271100WX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000369R07HX_06_0011">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>