<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DI_T006L" variety="T006L">
<name>THROTTLE BODY</name>
<para id="RM000002PQL03SX" category="A" type-id="30014" name-id="ES11DD-002" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000002PQL03SX_01" type-id="01" category="01">
<s-1 id="RM000002PQL03SX_01_0001" proc-id="RM22W0E___00001WR00001">
<ptxt>INSTALL THROTTLE BODY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 water by-pass hoses to the throttle body.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket to the intake air surge tank.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Groove</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Surge Tank Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle Body Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="A165587E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>Align the protrusion of the gasket with the groove of the intake air surge tank.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the throttle body with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the throttle position sensor and control motor connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 water by-pass hoses.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002PQL03SX_01_0011" proc-id="RM22W0E___00001WS00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add engine coolant.</ptxt>
<spec>
<title>Standard Capacity</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>with rear heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>16.2 liters (17.1 US qts, 14.3 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>without rear heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>13.4 liters (14.2 US qts, 11.8 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Slowly pour coolant into the radiator reservoir until it reaches the F line.</ptxt>
</s2>
<s2>
<ptxt>Install the reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand, and then check the coolant level. If the coolant level is low, add coolant.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and warm it up until the thermostat opens.</ptxt>
<atten4>
<ptxt>The thermostat opening timing can be confirmed by pressing the radiator inlet hose by hand, and checking when the engine coolant starts to flow inside the hose.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Maintain the engine speed at 2000 to 2500 rpm.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the radiator reservoir still has some coolant in it.</ptxt>
</item>
<item>
<ptxt>Pay attention to the needle of the water temperature meter. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
<item>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the F line.</ptxt>
</item>
<item>
<ptxt>Run the engine at 2000 rpm until the coolant level has stabilized.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand to bleed air.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the fan.</ptxt>
</item>
</list1>
</atten2>
</s2>
<s2>
<ptxt>Stop the engine, and wait until the engine coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Check that the coolant level is between the F and L lines.</ptxt>
<ptxt>If the coolant level is below the L line, repeat all of the procedures above.</ptxt>
<ptxt>If the coolant level is above the F line, drain coolant so that the coolant level is between the F and L lines.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03SX_01_0018" proc-id="RM22W0E___00001WU00001">
<ptxt>INSTALL AIR CLEANER HOSE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the air cleaner hose so that the protrusion of the air cleaner cap aligns with the groove of the hose as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A243381E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Groove</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the 2 clamps.</ptxt>
<torque>
<torqueitem>
<t-value1>2.5</t-value1>
<t-value2>25</t-value2>
<t-value3>22</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the vacuum hose.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 2 ventilation hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03SX_01_0012" proc-id="RM22W0E___00001WT00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A178451" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and water pump for leaks. If no external leaks are found, check the heater core, cylinder block and head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03SX_01_0024" proc-id="RM22W0E___00001WX00000">
<ptxt>CHECK THROTTLE BODY ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the throttle control motor operating sounds.</ptxt>
<s3>
<ptxt>Turn the engine switch on (IG).</ptxt>
</s3>
<s3>
<ptxt>When depressing the accelerator pedal, check the operating sound of the running motor. Make sure that no friction noises are emitted from the motor. If friction noise exists, replace the throttle body.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the throttle position sensor.</ptxt>
<s3>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the engine switch on (IG).</ptxt>
</s3>
<s3>
<ptxt>Turn the intelligent tester main switch on.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Throttle Sensor Volt %.</ptxt>
</s3>
<s3>
<ptxt>Depress the accelerator pedal. When the throttle valve is fully opened, check that the value of "Throttle Sensor Volt %" is within the specification.</ptxt>
<spec>
<title>Standard throttle valve opening percentage</title>
<specitem>
<ptxt>60% or more</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>When checking the standard throttle valve opening percentage, the shift lever should be in N.</ptxt>
</atten4>
<ptxt>If the percentage is less than 60%, replace the throttle body.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03SX_01_0020" proc-id="RM22W0E___00001WW00001">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover with the 10 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03SX_01_0021" proc-id="RM22W0E___000011S00002">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03SX_01_0022" proc-id="RM22W0E___000011Q00002">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly RH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002PQL03SX_01_0025" proc-id="RM22W0E___00001WY00001">
<ptxt>PERFORM INITIALIZATION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Perform the following procedure after replacing the ECM, throttle body assembly or any throttle body components. The following procedure should also be performed if the throttle body is cleaned.</ptxt>
</atten3>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal. Wait at least 60 seconds and reconnect the cable.</ptxt>
</s2>
<s2>
<ptxt>Turn the engine switch on (IG) without operating the accelerator pedal.</ptxt>
<atten3>
<ptxt>If the accelerator pedal is operated, perform the above steps again.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the intelligent tester to the DLC3 and clear the DTCs (See page <xref label="Seep01" href="RM000000PDK17ZX"/>).</ptxt>
</s2>
<s2>
<ptxt>Start the engine and check that the MIL is not illuminated and that the idle speed is within the specified range when the A/C is switched off after the engine is warmed up.</ptxt>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine Idle Speed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A/C switched off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>650 to 750 rpm</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to perform this step with all accessories off.</ptxt>
</item>
<item>
<ptxt>Make sure that the shift lever is in N or P.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Throttle Sensor Volt %. Fully depress the accelerator pedal and check that the value is 60% or more.</ptxt>
</s2>
<s2>
<ptxt>Perform a road test and confirm that there are no abnormalities.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002PQL03SX_01_0019" proc-id="RM22W0E___00001WV00001">
<ptxt>INSTALL V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A177597E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 3 V-bank cover grommets with the 3 pins, and press down on the V-bank cover to attach the pins.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>