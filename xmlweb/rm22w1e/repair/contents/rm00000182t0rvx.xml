<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM00000182T0RVX" category="C" type-id="803M9" name-id="NSASW-01" from="201308">
<dtccode>B15C0</dtccode>
<dtcname>Short in GPS Antenna</dtcname>
<dtccode>B15C1</dtccode>
<dtcname>Open in GPS Antenna</dtcname>
<subpara id="RM00000182T0RVX_01" type-id="60" category="03" proc-id="RM22W0E___0000JXD00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>These DTCs are stored when a malfunction occurs in the navigation antenna assembly (w/o Roof Antenna) or telephone antenna assembly (w/ Roof Antenna).</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B15C0</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Navigation antenna error</ptxt>
</entry>
<entry morerows="1" valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>w/o Roof Antenna</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Navigation antenna assembly</ptxt>
</item>
<item>
<ptxt>Multi-media module receiver assembly</ptxt>
</item>
</list2>
<item>
<ptxt>w/ Roof Antenna, w/o DAB Function</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Telephone antenna assembly</ptxt>
</item>
<item>
<ptxt>No. 3 antenna cord sub-assembly</ptxt>
</item>
<item>
<ptxt>No. 2 antenna cord sub-assembly</ptxt>
</item>
<item>
<ptxt>Antenna cord sub-assembly</ptxt>
</item>
<item>
<ptxt>Multi-media module receiver assembly</ptxt>
</item>
</list2>
<item>
<ptxt>w/ Roof Antenna, w/ DAB Function</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Telephone antenna assembly</ptxt>
</item>
<item>
<ptxt>Digital audio broadcasting antenna assembly</ptxt>
</item>
<item>
<ptxt>No. 2 antenna cord sub-assembly</ptxt>
</item>
<item>
<ptxt>Antenna cord sub-assembly</ptxt>
</item>
<item>
<ptxt>Multi-media module receiver assembly</ptxt>
</item>
</list2>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B15C1</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Error of the power source to the navigation antenna</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000182T0RVX_04" type-id="32" category="03" proc-id="RM22W0E___0000JXR00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E281002E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000182T0RVX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM00000182T0RVX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000182T0RVX_03_0011" proc-id="RM22W0E___0000JXJ00001">
<testtitle>CHECK VEHICLE TYPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Select the vehicle setting.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/o Roof Antenna</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ Roof Antenna, w/o DAB Function</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ Roof Antenna, w/ DAB Function</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0001" fin="false">A</down>
<right ref="RM00000182T0RVX_03_0013" fin="false">B</right>
<right ref="RM00000182T0RVX_03_0031" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0001" proc-id="RM22W0E___0000JXF00001">
<testtitle>CHECK CONNECTION OF NAVIGATION ANTENNA ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the navigation antenna assembly is securely connected to the multi-media module receiver assembly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Navigation antenna assembly is securely connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0002" fin="false">OK</down>
<right ref="RM00000182T0RVX_03_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0002" proc-id="RM22W0E___0000JXG00001">
<testtitle>REPLACE NAVIGATION ANTENNA ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the navigation antenna assembly with a normally functioning one (See page <xref label="Seep01" href="RM000000VF6030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0009" proc-id="RM22W0E___0000JXH00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0PQX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0010" proc-id="RM22W0E___0000JXI00001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTCs are output again (See page <xref label="Seep01" href="RM0000011BU0PQX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0003" fin="true">OK</down>
<right ref="RM00000182T0RVX_03_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0013" proc-id="RM22W0E___0000JXL00001">
<testtitle>CHECK NO. 3 ANTENNA CORD SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 3 antenna cord sub-assembly from telephone antenna assembly connector.</ptxt>
<figure>
<graphic graphicname="E272601E03" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the No. 3 antenna cord sub-assembly from No. 2 antenna cord sub-assembly.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A-2 - B-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-2a - B-1a</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-2a - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Telephone Antenna Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 2 Antenna Cord Sub-assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0014" fin="false">OK</down>
<right ref="RM00000182T0RVX_03_0023" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0014" proc-id="RM22W0E___0000JXM00001">
<testtitle>CHECK NO. 2 ANTENNA CORD SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 2 antenna cord sub-assembly from No. 3 antenna cord sub-assembly.</ptxt>
<figure>
<graphic graphicname="E272598E03" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the No. 2 antenna cord sub-assembly from antenna cord sub-assembly.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C-1 - D-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C-1a - D-1a</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C-1a - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 3 Antenna Cord Sub-assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Antenna Cord Sub-assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0017" fin="false">OK</down>
<right ref="RM00000182T0RVX_03_0024" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0017" proc-id="RM22W0E___0000JXN00001">
<testtitle>CHECK ANTENNA CORD SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the antenna cord sub-assembly from No. 2 antenna cord sub-assembly.</ptxt>
<figure>
<graphic graphicname="E272599E03" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the antenna cord sub-assembly from multi-media module receiver assembly.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E-1 - GA-1 (GPS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E-1a - GA-1a</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E-1a - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 2 Antenna Cord Sub-assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Multi-media Module Receiver Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0018" fin="false">OK</down>
<right ref="RM00000182T0RVX_03_0025" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0018" proc-id="RM22W0E___0000JXO00001">
<testtitle>REPLACE TELEPHONE ANTENNA ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the telephone antenna assembly with a normally functioning one (See page <xref label="Seep01" href="RM000002O6S02GX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0020" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0020" proc-id="RM22W0E___0000JXP00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0PQX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0021" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0021" proc-id="RM22W0E___0000JXQ00001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTCs are output again (See page <xref label="Seep01" href="RM0000011BU0PQX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0028" fin="true">OK</down>
<right ref="RM00000182T0RVX_03_0030" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0004">
<testtitle>SECURELY CONNECT NAVIGATION ANTENNA ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0005">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003DFN005X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0003">
<testtitle>END (NAVIGATION ANTENNA ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0022">
<testtitle>REPLACE DIGITAL AUDIO BROADCASTING ANTENNA ASSEMBLY<xref label="Seep01" href="RM000003AE101VX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0023">
<testtitle>REPLACE NO. 3 ANTENNA CORD SUB-ASSEMBLY<xref label="Seep01" href="RM000003AWL02EX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0024">
<testtitle>REPLACE NO. 2 ANTENNA CORD SUB-ASSEMBLY<xref label="Seep01" href="RM000003AWL02EX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0025">
<testtitle>REPLACE ANTENNA CORD SUB-ASSEMBLY<xref label="Seep01" href="RM000003AWL02EX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0030">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY024X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0028">
<testtitle>END (TELEPHONE ANTENNA ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0031" proc-id="RM22W0E___0000K1S00000">
<testtitle>CHECK DIGITAL AUDIO BROADCASTING ANTENNA ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the digital audio broadcasting antenna assembly from telephone antenna assembly connector.</ptxt>
<figure>
<graphic graphicname="E282970E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the digital audio broadcasting antenna assembly from No. 2 antenna cord sub-assembly.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A-2 - B-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-2a - B-1a</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-2a - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Telephone Antenna Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 2 Antenna Cord Sub-assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0032" fin="false">OK</down>
<right ref="RM00000182T0RVX_03_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0032" proc-id="RM22W0E___0000K1T00000">
<testtitle>CHECK NO. 2 ANTENNA CORD SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the No. 2 antenna cord sub-assembly from digital audio broadcasting antenna assembly.</ptxt>
<figure>
<graphic graphicname="E282971E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the No. 2 antenna cord sub-assembly from antenna cord sub-assembly.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C-1 - D-4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C-1a - D-4a</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C-1a - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Digital Audio Broadcasting Antenna Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Antenna Cord Sub-assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0033" fin="false">OK</down>
<right ref="RM00000182T0RVX_03_0024" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0033" proc-id="RM22W0E___0000K1U00000">
<testtitle>CHECK ANTENNA CORD SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the antenna cord sub-assembly from No. 2 antenna cord sub-assembly.</ptxt>
<figure>
<graphic graphicname="E282972E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the antenna cord sub-assembly from multi-media module receiver assembly.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E-4 - GA-1 (GPS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E-4a - GA-1a</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E-4 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E-4a - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 2 Antenna Cord Sub-assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Multi-media Module Receiver Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0034" fin="false">OK</down>
<right ref="RM00000182T0RVX_03_0025" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0034" proc-id="RM22W0E___0000K1V00000">
<testtitle>REPLACE TELEPHONE ANTENNA ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the telephone antenna assembly with a normally functioning one (See page <xref label="Seep01" href="RM000002O6S02GX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0035" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0035" proc-id="RM22W0E___0000K1W00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0PQX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0036" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000182T0RVX_03_0036" proc-id="RM22W0E___0000K1X00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTCs are output again (See page <xref label="Seep01" href="RM0000011BU0PQX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000182T0RVX_03_0001" fin="true">OK</down>
<right ref="RM00000182T0RVX_03_0030" fin="true">NG</right>
</res>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>