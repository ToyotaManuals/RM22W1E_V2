<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3W2_T00P5" variety="T00P5">
<name>COMPRESSOR (for 1GR-FE)</name>
<para id="RM0000016TR08HX" category="A" type-id="8000E" name-id="AC5WJ-03" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM0000016TR08HX_01" type-id="01" category="01">
<s-1 id="RM0000016TR08HX_01_0005" proc-id="RM22W0E___0000GTI00000">
<ptxt>INSTALL MAGNET CLUTCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E168158" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the magnet clutch stator with the parts shown in the illustration matched.</ptxt>
</s2>
<s2>
<ptxt>Using a snap ring expander, install a new snap ring with the chamfered side facing up.</ptxt>
<figure>
<graphic graphicname="I032461" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Using a snap ring expander, install the magnet clutch rotor and a new snap ring with the chamfered side facing up.</ptxt>
<figure>
<graphic graphicname="I031582" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not damage the seal cover of the bearing when installing the snap ring.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the compressor washer(s) and magnet clutch hub.</ptxt>
<atten3>
<ptxt>Do not change the combination of the compressor washer(s) used before disassembly.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using SST, hold the magnet clutch hub and install the bolt.</ptxt>
<sst>
<sstitem>
<s-number>09985-00270</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="E168159E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Make sure that there is no foreign matter or oil on the compressor shaft, bolt, and clutch hub.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016TR08HX_01_0008" proc-id="RM22W0E___0000GTJ00000">
<ptxt>INSTALL COOLER BRACKET</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E168155" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the cooler bracket with the screw.</ptxt>
</s2>
<s2>
<ptxt>Attach the clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016TR08HX_01_0009" proc-id="RM22W0E___0000GTK00000">
<ptxt>INSPECT MAGNET CLUTCH CLEARANCE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E168160E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Clamp the cooler compressor in a vise.</ptxt>
</s2>
<s2>
<ptxt>Set the dial indicator to the magnet clutch hub.</ptxt>
</s2>
<s2>
<ptxt>Connect the battery positive lead to terminal 3 of the magnet clutch connector and the negative lead to the ground wire. Turn the magnet clutch on and off and measure the clearance.</ptxt>
<spec>
<title>Standard clearance</title>
<specitem>
<ptxt>0.35 to 0.60 mm (0.014 to 0.024 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the measured value is not within the standard clearance, remove the magnet clutch hub and adjust the clearance using compressor washers to obtain the standard clearance.</ptxt>
<spec>
<title>Compressor washer thickness</title>
<specitem>
<ptxt>0.1 mm (0.004 in.)</ptxt>
<ptxt>0.3 mm (0.012 in.)</ptxt>
<ptxt>0.5 mm (0.020 in.)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Adjustment should be performed with 3 or fewer magnet clutch washers.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the cooler compressor from the vise.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>