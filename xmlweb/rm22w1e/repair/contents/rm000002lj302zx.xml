<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002LJ302ZX" category="C" type-id="3007N" name-id="ACBKO-02" from="201301" to="201308">
<dtccode>B1411/11</dtccode>
<dtcname>Room Temperature Sensor Circuit</dtcname>
<subpara id="RM000002LJ302ZX_01" type-id="60" category="03" proc-id="RM22W0E___0000H1G00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The front room temperature sensor for the front seat is installed in the instrument panel to detect the room temperature and control the heater and air conditioner "auto" mode. The resistance of the room temperature sensor changes in accordance with the front room temperature. As the temperature decreases, the resistance increases. As the temperature increases, the resistance decreases.</ptxt>
<ptxt>The air conditioning amplifier assembly applies voltage (5 V) to the front room temperature sensor and reads voltage changes as the resistance of the front room temperature sensor changes. This sensor also sends the appropriate signals to the air conditioning amplifier assembly.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1411/11</ptxt>
</entry>
<entry valign="middle">
<ptxt>An open or short in the front room temperature sensor circuit.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Front room temperature sensor</ptxt>
</item>
<item>
<ptxt>Harness or connector between room temperature sensor and air conditioning amplifier assembly</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002LJ302ZX_02" type-id="32" category="03" proc-id="RM22W0E___0000H1H00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E156584E44" width="7.106578999in" height="2.775699831in"/>
</figure>
<figure>
<graphic graphicname="E156584E45" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002LJ302ZX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002LJ302ZX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002LJ302ZX_04_0001" proc-id="RM22W0E___0000H1I00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (FRONT ROOM TEMPERATURE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the front room temperature sensor is functioning properly. </ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Room Temperature Sensor</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front room temperature sensor /</ptxt>
<ptxt>Min.: -6.5°C (20.3°F)</ptxt>
<ptxt>Max.: 57.25°C (135.05°F)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Actual room temperature for front seat displayed</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Open in the circuit: -6.5°C (20.3°F).</ptxt>
<ptxt>Short in the circuit: 57.25°C (135.05°F).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LJ302ZX_04_0011" fin="true">OK</down>
<right ref="RM000002LJ302ZX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LJ302ZX_04_0002" proc-id="RM22W0E___0000H1J00000">
<testtitle>INSPECT ROOM TEMPERATURE SENSOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E160595E02" width="2.775699831in" height="5.787629434in"/>
</figure>
<test1>
<ptxt>Remove the front room temperature sensor (See page <xref label="Seep01" href="RM0000039P900NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="10" valign="middle">
<ptxt>1 (+) - 2 (-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10°C (50°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.00 to 3.73 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>15°C (59°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.45 to 2.88 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.95 to 2.30 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>25°C (77°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.60 to 1.80 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>30°C (86°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.28 to 1.47 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>35°C (95°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.00 to 1.22 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>40°C (104°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.80 to 1.00 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>45°C (113°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.65 to 0.85 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>50°C (122°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.50 to 0.70 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>55°C (131°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.44 to 0.60 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>60°C (140°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.36 to 0.50 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Even slightly touching the sensor may change the resistance value. Be sure to hold the connector of the sensor.</ptxt>
</item>
<item>
<ptxt>When measuring, the sensor temperature must be the same as the ambient temperature.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>As the temperature increases, the resistance decreases (see the graph).</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002LJ302ZX_04_0003" fin="false">OK</down>
<right ref="RM000002LJ302ZX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LJ302ZX_04_0003" proc-id="RM22W0E___0000H1K00000">
<testtitle>CHECK HARNESS AND CONNECTOR (FRONT ROOM TEMPERATURE SENSOR - AIR CONDITIONING AMPLIFIER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>w/ Rear Heater</ptxt>
<figure>
<graphic graphicname="E156619E03" width="2.775699831in" height="5.787629434in"/>
</figure>
<test2>
<ptxt>Disconnect the E32 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E35 amplifier connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E32-1 (+) - E35-9 (TR)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E32-2 (-) - E35-32 (SG-1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-9 (TR) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E35-32 (SG-1) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>w/o Rear Heater</ptxt>
<figure>
<graphic graphicname="E160288E03" width="2.775699831in" height="4.7836529in"/>
</figure>
<test2>
<ptxt>Disconnect the E32 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E81 amplifier connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E32-1 (+) - E81-29 (TR)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E32-2 (-) - E81-34 (SG-1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-29 (TR) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-34 (SG-1) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002LJ302ZX_04_0011" fin="true">OK</down>
<right ref="RM000002LJ302ZX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LJ302ZX_04_0006">
<testtitle>REPLACE FRONT ROOM TEMPERATURE SENSOR<xref label="Seep01" href="RM0000039P900NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LJ302ZX_04_0010">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002LJ302ZX_04_0011">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>