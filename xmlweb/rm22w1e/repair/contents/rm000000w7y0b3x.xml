<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MR_T00FU" variety="T00FU">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 3UR-FE)</name>
<para id="RM000000W7Y0B3X" category="C" type-id="302FF" name-id="AT711-03" from="201308">
<dtccode>P0717</dtccode>
<dtcname>Turbine Speed Sensor Circuit No Signal</dtcname>
<subpara id="RM000000W7Y0B3X_01" type-id="60" category="03" proc-id="RM22W0E___00008BL00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This sensor detects the rotation speed of the turbine which indicates the input speed of the transmission. By comparing the input turbine speed signal NT with the counter gear speed sensor signal SP2, the ECM detects the shift timing of the gears and appropriately controls the engine torque and hydraulic pressure according to various conditions, and as a result, the gears shift smoothly.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.19in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0717</ptxt>
</entry>
<entry valign="middle">
<ptxt>All conditions are met for 5 seconds or more (1 trip detection logic):</ptxt>
<ptxt>(a) Gear shift is not being performed.</ptxt>
<ptxt>(b) Transmission is in 4th, 5th or 6th gear.</ptxt>
<ptxt>(c) Transmission input shaft speed is 300 rpm or less.</ptxt>
<ptxt>(d) Transmission output shaft speed is 1000 rpm or more.</ptxt>
<ptxt>(e) Park/neutral position switch NSW and R input signals are OFF.</ptxt>
<ptxt>(f) Shift solenoid valves and park/neutral position switch are normal.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in speed sensor NT circuit</ptxt>
</item>
<item>
<ptxt>Speed sensor NT</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake, gear, etc.)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Reference: Inspect using an oscilloscope.</ptxt>
<ptxt>Check the waveform of the ECM connector.</ptxt>
<figure>
<graphic graphicname="C162170E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard</title>
<table>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COL3" colwidth="1.03in"/>
<colspec colname="COL4" colwidth="1.04in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C45-124 (NT+) - C45-123 (NT-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 2 msec./DIV.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine is idling (Shift lever in P or N)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Refer to illustration</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</content5>
</subpara>
<subpara id="RM000000W7Y0B3X_02" type-id="64" category="03" proc-id="RM22W0E___00008BM00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates that a pulse is not output from the speed sensor NT (turbine (input) speed sensor) or is output only a little. The NT terminal of the ECM detects the pulse signal from the speed sensor NT (input turbine speed). The ECM outputs a gear shift signal by comparing the input speed sensor NT signal with the output speed sensor SP2 signal.</ptxt>
<ptxt>While the vehicle is operating in the 4th, 5th or 6th gear position with the shift lever in D, if the input shaft revolution is less than 300 rpm*1 although the output shaft speed is more than 1000 rpm or more*2, the ECM detects the trouble, illuminates the MIL and stores the DTC.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: Pulse is not output or is irregularly output.</ptxt>
</item>
<item>
<ptxt>*2: The vehicle speed is approximately 50 km/h (30 mph) or more.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000W7Y0B3X_07" type-id="32" category="03" proc-id="RM22W0E___00008BN00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C155187E85" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W7Y0B3X_08" type-id="51" category="05" proc-id="RM22W0E___00008BO00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>SPD (NT)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Input shaft speed/</ptxt>
<ptxt>Min.: 0 rpm</ptxt>
<ptxt>Max.: 12750 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up is:</ptxt>
<list1 type="unordered">
<item>
<ptxt>ON (after warming up engine): Input turbine speed (NT) equal to engine speed</ptxt>
</item>
<item>
<ptxt>OFF (idling with shift lever in N): Input turbine speed (NT) nearly equal to engine speed</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Data is displayed in increments of 50 rpm.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>SPD (NT) is always 0 while driving:</ptxt>
<ptxt>Open or short in the sensor or circuit.</ptxt>
</item>
<item>
<ptxt>SPD (NT) is always more than 0 and less than 300 rpm while driving the vehicle at 50 km/h (30 mph) or more:</ptxt>
<ptxt>Sensor trouble, improper installation or intermittent connection trouble of the circuit.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000000W7Y0B3X_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W7Y0B3X_09_0001" proc-id="RM22W0E___00008BP00001">
<testtitle>INSPECT SPEED SENSOR NT INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the speed sensor NT installation.</ptxt>
<figure>
<graphic graphicname="C211922E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>The installation bolt is tightened properly and there is no clearance between the sensor and transmission case.</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W7Y0B3X_09_0002" fin="false">OK</down>
<right ref="RM000000W7Y0B3X_09_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W7Y0B3X_09_0002" proc-id="RM22W0E___00008BQ00001">
<testtitle>INSPECT SPEED SENSOR NT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the speed sensor connector.</ptxt>
<figure>
<graphic graphicname="C206284E07" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>560 to 680 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Speed Sensor NT)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W7Y0B3X_09_0003" fin="false">OK</down>
<right ref="RM000000W7Y0B3X_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W7Y0B3X_09_0003" proc-id="RM22W0E___00008BR00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SPEED SENSOR NT - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="C219456E17" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C45-124 (NT+) - C45-123 (NT-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>560 to 680 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-124 (NT+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-123 (NT-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W7Y0B3X_09_0005" fin="true">OK</down>
<right ref="RM000000W7Y0B3X_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W7Y0B3X_09_0004">
<testtitle>SECURELY INSTALL OR REPLACE SPEED SENSOR NT</testtitle>
</testgrp>
<testgrp id="RM000000W7Y0B3X_09_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292039X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W7Y0B3X_09_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W7Y0B3X_09_0007">
<testtitle>REPLACE SPEED SENSOR NT<xref label="Seep01" href="RM000002BL403LX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>