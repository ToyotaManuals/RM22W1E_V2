<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM000003WPY071X" category="C" type-id="803MJ" name-id="NS8IL-30" from="201308">
<dtccode>B15E7</dtccode>
<dtcname>Harddisk Reading/Writing Malfunction(Low Temperature)</dtcname>
<dtccode>B15E8</dtccode>
<dtcname>Harddisk Reading/Writing Malfunction(High Temperature)</dtcname>
<subpara id="RM000003WPY071X_01" type-id="60" category="03" proc-id="RM22W0E___0000CL600001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>These DTCs are stored when the temperature is higher or lower than the operation range of the hard disc built into the multi-media module receiver assembly, and hard disc read/write errors occur more than a specified number of times.</ptxt>
<atten4>
<ptxt>The hard disc may not operate normally when the temperature is -20°C (-4°F) or lower, or 65°C (149°F) or higher.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B15E7</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Hard disc read/write error at low temperature.</ptxt>
</entry>
<entry morerows="1" valign="middle" align="left">
<ptxt>Hard disc</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B15E8</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Hard disc read/write error at high temperature.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003WPY071X_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003WPY071X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003WPY071X_04_0004" proc-id="RM22W0E___0000CL800001">
<testtitle>CHECK CABIN</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Move the vehicle to an area that is at room temperature and allow the cabin temperature to adjust to between -20°C (-4°F) and 65°C (149°F).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The hard disc may not operate normally at very high or low temperatures.</ptxt>
</item>
<item>
<ptxt>Perform the DTC check at room temperature.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003WPY071X_04_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WPY071X_04_0008" proc-id="RM22W0E___0000CL900001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0PQX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WPY071X_04_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WPY071X_04_0001" proc-id="RM22W0E___0000CL700001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTCs are output again (See page <xref label="Seep01" href="RM0000011BU0PQX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WPY071X_04_0007" fin="true">OK</down>
<right ref="RM000003WPY071X_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WPY071X_04_0006">
<testtitle>REPLACE HARD DISC<xref label="Seep01" href="RM000002V0L01HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WPY071X_04_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>