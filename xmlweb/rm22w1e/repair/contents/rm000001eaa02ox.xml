<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000001EAA02OX" category="C" type-id="804R0" name-id="BCDX7-05" from="201308">
<dtccode>C1257</dtccode>
<dtcname>Power Supply Drive Circuit</dtcname>
<subpara id="RM000001EAA02OX_01" type-id="60" category="03" proc-id="RM22W0E___0000AKN00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The motor relay (semiconductor relay) is built into the master cylinder solenoid and drives the pump motor based on a signal from the skid control ECU (master cylinder solenoid).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1257</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is an open in the motor system circuit (motor input circuit).</ptxt>
</entry>
<entry valign="middle">
<ptxt>Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001EAA02OX_02" type-id="51" category="05" proc-id="RM22W0E___0000AKO00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001EAA02OX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001EAA02OX_03_0001" proc-id="RM22W0E___0000AKP00001">
<testtitle>CHECK PUMP MOTOR OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off. </ptxt>
</test1>
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal more than 40 times. </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check pump motor operation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt> Pump motor operates normally.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001EAA02OX_03_0002" fin="false">A</down>
<right ref="RM000001EAA02OX_03_0003" fin="true">B</right>
<right ref="RM000001EAA02OX_03_0006" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001EAA02OX_03_0002" proc-id="RM22W0E___0000AKQ00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off. </ptxt>
</test1>
<test1>
<ptxt>Reconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off. </ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001EAA02OX_03_0004" fin="true">A</down>
<right ref="RM000001EAA02OX_03_0003" fin="true">B</right>
<right ref="RM000001EAA02OX_03_0006" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001EAA02OX_03_0003">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001EAA02OX_03_0004">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001EAA02OX_03_0006">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>