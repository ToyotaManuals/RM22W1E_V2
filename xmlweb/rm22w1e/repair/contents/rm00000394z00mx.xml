<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3V6_T00O9" variety="T00O9">
<name>CLIMATE CONTROL SEAT SWITCH</name>
<para id="RM00000394Z00MX" category="A" type-id="80001" name-id="SE92V-02" from="201301">
<name>REMOVAL</name>
<subpara id="RM00000394Z00MX_02" type-id="11" category="10" proc-id="RM22W0E___0000GF000000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000394Z00MX_01" type-id="01" category="01">
<s-1 id="RM00000394Z00MX_01_0012" proc-id="RM22W0E___0000AB300000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E155426E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the panel cushion.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000394Z00MX_01_0013" proc-id="RM22W0E___0000AB400000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip.</ptxt>
<figure>
<graphic graphicname="B181910" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 8 claws.</ptxt>
</s2>
<s2>
<ptxt>Remove the panel pad and disconnect the connectors and 2 clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000394Z00MX_01_0014" proc-id="RM22W0E___0000GEZ00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL FINISH CUSHION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B180004E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the panel cushion.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000394Z00MX_01_0015" proc-id="RM22W0E___0000AB600000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip.</ptxt>
<figure>
<graphic graphicname="B180005" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 7 claws and remove the panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000394Z00MX_01_0016" proc-id="RM22W0E___0000AB700000">
<ptxt>REMOVE SHIFT LEVER KNOB SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Twist the shift lever knob in the direction indicated by the arrow and remove it.</ptxt>
<figure>
<graphic graphicname="B186290E01" width="7.106578999in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000394Z00MX_01_0017" proc-id="RM22W0E___0000AB800000">
<ptxt>REMOVE LOWER CENTER INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 7 claws.</ptxt>
<figure>
<graphic graphicname="B180006" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000394Z00MX_01_0018" proc-id="RM22W0E___0000AB900000">
<ptxt>REMOVE REAR UPPER CONSOLE PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 9 claws and remove the panel.</ptxt>
<figure>
<graphic graphicname="B180009" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000394Z00MX_01_0019" proc-id="RM22W0E___0000ABA00000">
<ptxt>REMOVE CONSOLE CUP HOLDER BOX SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 claws and remove the box.</ptxt>
<figure>
<graphic graphicname="B180010" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000394Z00MX_01_0020" proc-id="RM22W0E___0000ABB00000">
<ptxt>REMOVE UPPER CONSOLE PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 14 claws.</ptxt>
<figure>
<graphic graphicname="B180011" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the console panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000394Z00MX_01_0011" proc-id="RM22W0E___0000GEY00000">
<ptxt>REMOVE SUSPENSION CONTROL SWITCH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181867" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the suspension control switch.</ptxt>
<figure>
<graphic graphicname="B181868" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000394Z00MX_01_0010" proc-id="RM22W0E___0000GEX00000">
<ptxt>REMOVE SEAT HEATER SWITCH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181869" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws and remove the seat heater switch.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>