<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SM_T00LP" variety="T00LP">
<name>WIRELESS DOOR LOCK CONTROL SYSTEM (w/ Entry and Start System)</name>
<para id="RM000000XXC06SX" category="D" type-id="303FN" name-id="DL0022-121" from="201301">
<name>CUSTOMIZE PARAMETERS</name>
<subpara id="RM000000XXC06SX_z0" proc-id="RM22W0E___0000E7O00000">
<content5 releasenbr="1">
<step1>
<ptxt>CUSTOMIZING FUNCTION WITH INTELLIGENT TESTER (REFERENCE)</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When the customer requests a change in a function, first make sure that the function can be customized.</ptxt>
</item>
<item>
<ptxt>Be sure to make a note of the current settings before customizing.</ptxt>
</item>
<item>
<ptxt>When troubleshooting a function, first make sure that the function is set to the default setting.</ptxt>
</item>
</list1>
</atten3>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Customize Setting.</ptxt>
</step2>
<step2>
<ptxt>Select the setting by referring to the table below.</ptxt>
<table pgwide="1">
<title>Wireless Door Lock</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Default</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Contents</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Setting</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Hazard Answer Back</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the lock switch on the transmitter is pressed, this function illuminates the hazard warning lights once. When the unlock switch is pressed, the hazard warning lights illuminate twice.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function that turns the wireless door lock function ON or OFF.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Open Door Warning*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>If a door is not completely closed and the transmitter lock switch is pressed, this function makes the buzzer sound for 5 seconds.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Panic Alarm Function*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function to operate the theft deterrent system by continuously pressing the panic switch on transmitter for 2.5 seconds.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Unlock 2 Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function that unlocks the driver side door when the unlock switch on the transmitter is pressed once, and unlocks all doors when the switch is pressed twice. If the setting is OFF, pressing the unlock switch once makes all the doors unlock.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Auto Lock Time</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>30 s</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function to change the time until relocking after unlocking with the wireless door lock function.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>30 s/60 s/120 s</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless Auto Lock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function to control the automatic relocking of doors after unlocking with the wireless door lock function.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless Buzzer Resp</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wireless buzzer response function.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless Buzzer Volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Level 7</ptxt>
</entry>
<entry valign="middle">
<ptxt>This function changes the wireless buzzer volume.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Level 0/Level 1/Level 2/Level 3/Level 4/Level 5/Level6/Level 7</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: Except China</ptxt>
<ptxt>*2: w/ Panic Switch</ptxt>
<ptxt>*3: Except Europe and China</ptxt>
</atten4>
<table pgwide="1">
<title>PSD &amp; PBD Operation (w/ Power Back Door System) </title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Default</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Contents</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Setting</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless PBD Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.8 s</ptxt>
<ptxt>1Motion*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function that operates power back door when door control transmitter is pressed in specified way (1-press, 2-presses, 0.8 sec.- press or off).</ptxt>
<ptxt>Function to turn on/off the power back door wireless one motion open operation.*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 Oper or 2 Oper or 0.8 s or OFF</ptxt>
<ptxt>1Motion or 1 Oper or 2 Oper or 0.8 s or OFF*</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: Except Europe</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>CUSTOMIZING FUNCTION WITH MULTI-DISPLAY (w/ Multi-display)</ptxt>
<atten4>
<ptxt>The following items can be customized.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When the customer requests a change in a function, first make sure that the function can be customized.</ptxt>
</item>
<item>
<ptxt>Record the current settings before customizing.</ptxt>
</item>
</list1>
</atten3>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Setup / Vehicle.</ptxt>
</step2>
<step2>
<ptxt>Select the setting by referring to the table below.</ptxt>
<table pgwide="1">
<title>Door lock settings</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Default</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Contents</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Setting</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Remote 2-press unlock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function that unlocks the driver side door when the unlock switch on the transmitter is pressed once, and unlocks all doors when the switch is pressed twice. If the setting is OFF, pressing the unlock switch once makes all the doors unlock.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lock/unlock feedback lights</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the lock switch on the transmitter is pressed, this function illuminates the hazard warning lights once. When the unlock switch is pressed, the hazard warning lights illuminate twice.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lock feedback volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Level 7</ptxt>
</entry>
<entry valign="middle">
<ptxt>This function changes the wireless buzzer volume.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Level 0/Level 1/Level 2/Level 3/Level 4/Level 5/Level6/Level 7</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>