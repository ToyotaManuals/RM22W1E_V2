<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002K" variety="S002K">
<name>WIPER / WASHER</name>
<ttl id="12068_S002K_7C3Y9_T00RC" variety="T00RC">
<name>WIPER AND WASHER SYSTEM (w/ Rain Sensor)</name>
<para id="RM000002OXM014X" category="J" type-id="3009M" name-id="WW1KL-06" from="201301">
<dtccode/>
<dtcname>ECU Power Source Circuit</dtcname>
<subpara id="RM000002OXM014X_01" type-id="60" category="03" proc-id="RM22W0E___0000IQQ00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit provides power to the windshield wiper ECU and operates the front wiper motor.</ptxt>
</content5>
</subpara>
<subpara id="RM000002OXM014X_02" type-id="32" category="03" proc-id="RM22W0E___0000IQR00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E138462E03" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002OXM014X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002OXM014X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002OXM014X_04_0006" proc-id="RM22W0E___0000IQT00000">
<testtitle>INSPECT FUSE (ECU-IG No. 1)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the ECU-IG No. 1 fuse from the main body ECU.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ECU-IG No. 1 fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002OXM014X_04_0003" fin="false">OK</down>
<right ref="RM000002OXM014X_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002OXM014X_04_0003" proc-id="RM22W0E___0000IQS00000">
<testtitle>CHECK HARNESS AND CONNECTOR (WINDSHIELD WIPER ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E83 and E85 windshield wiper ECU connectors.</ptxt>
<figure>
<graphic graphicname="E157235E01" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E83-5 (IG1L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E83-5 (IG1L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E85-10 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002OXM014X_04_0008" fin="true">OK</down>
<right ref="RM000002OXM014X_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002OXM014X_04_0008">
<testtitle>REPLACE WINDSHIELD WIPER ECU<xref label="Seep01" href="RM000003CET006X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002OXM014X_04_0007">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000002OXM014X_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>