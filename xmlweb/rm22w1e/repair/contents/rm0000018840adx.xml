<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM0000018840ADX" category="C" type-id="3038U" name-id="ES17JM-001" from="201308">
<dtccode>P0088</dtccode>
<dtcname>Fuel Rail / System Pressure - Too High</dtcname>
<subpara id="RM0000018840ADX_01" type-id="60" category="03" proc-id="RM22W0E___00002RE00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to System Description (See page <xref label="Seep01" href="RM0000012WW03ZX"/>).</ptxt>
<table pgwide="1">
<title>P0088</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>After idling for 60 seconds, repeat quick engine speed accelerations (to 2500 rpm) for 30 seconds</ptxt>
</entry>
<entry>
<ptxt>The fuel pressure of common rail is too high</ptxt>
<ptxt>The fuel pressure exceeds 230 MPa (2345 kgf/cm<sup>2</sup>, 33358 psi) (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Fuel supply pump (suction control valve)</ptxt>
</item>
<item>
<ptxt>Pressure limiter (common rail (for Bank 2))</ptxt>
</item>
<item>
<ptxt>Open or short in fuel supply pump (suction control valve) circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0088</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Fuel Press</ptxt>
</item>
<item>
<ptxt>Target Common Rail Pressure</ptxt>
</item>
<item>
<ptxt>Injection Pressure Correction</ptxt>
</item>
<item>
<ptxt>Target Pump SCV Current</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>For more information on the fuel supply pump (suction control valve) and common rail system, refer to System Description (See page <xref label="Seep02" href="RM0000012WW03ZX"/>).</ptxt>
</item>
<item>
<ptxt>When DTC P0088 is stored, check the internal fuel pressure of the common rail by entering the following menus: Powertrain / Engine / Data List / Fuel Press.</ptxt>
<table pgwide="1">
<title>Reference</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fuel Press</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approximately 27 to 37 MPa</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2500 rpm (No engine load)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approximately 50 to 70 MPa</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
<item>
<ptxt>When there is an operating problem with the fuel supply pump (when Fuel Press is more than Target Common Rail Pressure due to a problem with the closing of the suction valve), the values of Injection Pressure Correction and Target Pump SCV Current decrease.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM0000018840ADX_02" type-id="64" category="03" proc-id="RM22W0E___00002RF00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<topic>
<title>P0088 (Internal fuel pressure too high):</title>
<ptxt>The ECM stores this DTC if the fuel pressure inside the common rail is more than 230 MPa (2345 kgf/cm<sup>2</sup>, 33350 psi). This DTC indicates that: 1) the suction control valve may be stuck open, or 2) there may be a short in the suction control valve circuit.</ptxt>
<ptxt>If this DTC is stored, the ECM enters fail-safe mode and limits the engine power. The fail-safe mode continues until the ignition switch is turned off.</ptxt>
</topic>
</content5>
</subpara>
<subpara id="RM0000018840ADX_06" type-id="32" category="03" proc-id="RM22W0E___00002RG00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A184082E08" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000018840ADX_07" type-id="51" category="05" proc-id="RM22W0E___00002RH00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08YX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06NX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06NX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000018840ADX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000018840ADX_08_0010" proc-id="RM22W0E___00002RK00001">
<testtitle>INSPECT FUEL SUPPLY PUMP (SUCTION CONTROL VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the suction control valve connector and then start the engine.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Engine does not start.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000018840ADX_08_0022" fin="false">OK</down>
<right ref="RM0000018840ADX_08_0014" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018840ADX_08_0022" proc-id="RM22W0E___00002RO00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / Target Pump SCV Current.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<table pgwide="1">
<title>Standard</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Engine Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Target Pump SCV Current</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle">
<ptxt>923 to 1123 mA</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2500 rpm (No engine load)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1013 to 1212 mA</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000018840ADX_08_0019" fin="false">OK</down>
<right ref="RM0000018840ADX_08_0014" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018840ADX_08_0019" proc-id="RM22W0E___00002RM00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (FUEL PRESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / Fuel Press, Target Common Rail Pressure.</ptxt>
</test1>
<test1>
<ptxt>Check the difference in pressure between Fuel Press and Target Common Rail Pressure.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Difference in pressure between Fuel Press and Target Common Rail Pressure is within +/-5 MPa.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000018840ADX_08_0020" fin="false">OK</down>
<right ref="RM0000018840ADX_08_0023" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018840ADX_08_0020" proc-id="RM22W0E___00002RN00001">
<testtitle>REPLACE PRESSURE LIMITER (COMMON RAIL (for Bank 2))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the common rail (for Bank 2) (See page <xref label="Seep01" href="RM0000031FK00ZX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM0000018840ADX_08_0006" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM0000018840ADX_08_0023" proc-id="RM22W0E___00002RP00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SUCTION CONTROL VALVE CONNECTOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the suction control valve connector.</ptxt>
<figure>
<graphic graphicname="A184081E04" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for open)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>z64-2 (PCV) - C45-105 (PCV-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>z64-1 (+B) - C45-106 (PCV+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>z64-2 (PCV) - C46-105 (PCV-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>z64-1 (+B) - C46-106 (PCV+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for short)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>z64-2 (PCV) or C45-105 (PCV-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>z64-1 (+B) or C45-106 (PCV+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>z64-2 (PCV) or C46-105 (PCV-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>z64-1 (+B) or C46-106 (PCV+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000018840ADX_08_0014" fin="false">OK</down>
<right ref="RM0000018840ADX_08_0025" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000018840ADX_08_0014" proc-id="RM22W0E___00002RL00001">
<testtitle>REPLACE FUEL SUPPLY PUMP</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the fuel supply pump (See page <xref label="Seep01" href="RM0000031EL007X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000018840ADX_08_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018840ADX_08_0004" proc-id="RM22W0E___00002RI00001">
<testtitle>CHECK DTC OUTPUT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK18AX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>After idling for 60 seconds, repeat engine speed accelerations (from idling to 2500 rpm) for 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Display (DTC output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0088 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000018840ADX_08_0024" fin="false">A</down>
<right ref="RM0000018840ADX_08_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000018840ADX_08_0024" proc-id="RM22W0E___00002RQ00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329203AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM0000018840ADX_08_0006" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM0000018840ADX_08_0025">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<res>
<down ref="RM0000018840ADX_08_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018840ADX_08_0006" proc-id="RM22W0E___00002RJ00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK18AX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and start the engine.</ptxt>
</test1>
<test1>
<ptxt>After idling for 60 seconds, repeat engine speed accelerations (from idling to 2500 rpm) for 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the tester to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Powertrain / Engine / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P0088.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or UNKNOWN, race the engine to 2500 rpm repeatedly for 30 seconds and increase idling time.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000018840ADX_08_0012" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018840ADX_08_0012">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>