<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001F" variety="S001F">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001F_7C3O6_T00H9" variety="T00H9">
<name>FRONT AXLE HUB</name>
<para id="RM000003BNP00KX" category="A" type-id="30014" name-id="AD01M-07" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000003BNP00KX_01" type-id="11" category="10" proc-id="RM22W0E___00009JV00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the LH side and RH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003BNP00KX_02" type-id="01" category="01">
<s-1 id="RM000003BNP00KX_02_0001" proc-id="RM22W0E___00009JW00000">
<ptxt>INSTALL FRONT AXLE HUB SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply MP grease to a new O-ring.</ptxt>
</s2>
<s2>
<ptxt>Install the O-ring to the axle hub.</ptxt>
<atten3>
<ptxt>Do not damage the O-ring.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the front drive shaft to the front axle hub.</ptxt>
<atten3>
<ptxt>Be careful not to damage the front drive shaft boot.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the dust cover and axle hub to the steering knuckle with the 4 bolts.</ptxt>
<figure>
<graphic graphicname="C171978" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>99</t-value1>
<t-value2>1010</t-value2>
<t-value4>73</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BNP00KX_02_0009" proc-id="RM22W0E___00009JJ00000">
<ptxt>INSTALL FRONT DISC
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the matchmarks, and then install the front disc.</ptxt>
<figure>
<graphic graphicname="C170927E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When replacing the front disc with a new one, select the installation position where the front disc has the minimum runout.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003BNP00KX_02_0010" proc-id="RM22W0E___00009K200000">
<ptxt>CONNECT FRONT DISC BRAKE CALIPER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the front disc brake caliper and install 2 new bolts.</ptxt>
<figure>
<graphic graphicname="C171974" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>99</t-value1>
<t-value2>1010</t-value2>
<t-value4>73</t-value4>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not twist the flexible hose.</ptxt>
</item>
<item>
<ptxt>Make sure that the bolts are free from damage and foreign matter.</ptxt>
</item>
<item>
<ptxt>Do not overtighten the bolts.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BNP00KX_02_0002" proc-id="RM22W0E___00009JX00000">
<ptxt>INSTALL FRONT AXLE SHAFT NUT LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the threaded parts on the drive shaft and axle shaft nut using a non-residue solvent.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to perform this work for a new drive shaft.</ptxt>
</item>
<item>
<ptxt>Keep the threaded parts free of oil and foreign objects.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Using a 39 mm socket wrench, install the axle shaft nut.</ptxt>
<figure>
<graphic graphicname="C171977" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>200</t-value1>
<t-value2>2039</t-value2>
<t-value4>148</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a 39 mm socket wrench, loosen the axle shaft nut.</ptxt>
</s2>
<s2>
<ptxt>Using a 39 mm socket wrench, retighten the axle shaft nut.</ptxt>
<torque>
<torqueitem>
<t-value1>340</t-value1>
<t-value2>3467</t-value2>
<t-value4>251</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the front wheel adjusting lock cap and a new cotter pin.</ptxt>
<figure>
<graphic graphicname="C171976" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BNP00KX_02_0011" proc-id="RM22W0E___00009K300000">
<ptxt>DISCONNECT FRONT DISC BRAKE CALIPER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and disconnect the disc brake caliper from the steering knuckle.</ptxt>
<figure>
<graphic graphicname="C171974" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not disconnect the flexible hose from the disc brake caliper.</ptxt>
</item>
<item>
<ptxt>Do not twist or bend the flexible hose.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BNP00KX_02_0015" proc-id="RM22W0E___00009JI00000">
<ptxt>REMOVE FRONT DISC
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put matchmarks on the front disc and axle hub if planning to reuse the disc.</ptxt>
<figure>
<graphic graphicname="C170927E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the front disc.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BNP00KX_02_0003" proc-id="RM22W0E___00009JY00000">
<ptxt>INSPECT FRONT AXLE HUB</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the front axle hub (See page <xref label="Seep01" href="RM000001IWW04VX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BNP00KX_02_0004" proc-id="RM22W0E___00009JZ00000">
<ptxt>INSTALL FRONT AXLE HUB GREASE CAP LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the axle hub grease cap.</ptxt>
<atten3>
<ptxt>Make sure to securely fit the grease cap to the axle hub.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BNP00KX_02_0016" proc-id="RM22W0E___00009JJ00000">
<ptxt>INSTALL FRONT DISC
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the matchmarks, and then install the front disc.</ptxt>
<figure>
<graphic graphicname="C170927E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When replacing the front disc with a new one, select the installation position where the front disc has the minimum runout.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003BNP00KX_02_0006" proc-id="RM22W0E___00009JS00000">
<ptxt>CONNECT FRONT DISC BRAKE CALIPER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the front disc brake caliper and install 2 new bolts.</ptxt>
<figure>
<graphic graphicname="C171974" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>99</t-value1>
<t-value2>1010</t-value2>
<t-value4>73</t-value4>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not twist the flexible hose.</ptxt>
</item>
<item>
<ptxt>Make sure that the bolts are free from damage and foreign matter.</ptxt>
</item>
<item>
<ptxt>Do not overtighten the bolts.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Connect the brake tube bracket to the steering knuckle with the bolt and nut.</ptxt>
<torque>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>31</t-value1>
<t-value2>316</t-value2>
<t-value4>23</t-value4>
</torqueitem>
<subtitle>for nut</subtitle>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>132</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="C171973E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003BNP00KX_02_0007" proc-id="RM22W0E___00009K000000">
<ptxt>INSTALL FRONT WHEEL</ptxt>
<content1 releasenbr="1">
<torque>
<subtitle>for Aluminum Wheel</subtitle>
<torqueitem>
<t-value1>131</t-value1>
<t-value2>1336</t-value2>
<t-value4>97</t-value4>
</torqueitem>
<subtitle>for Steel Wheel</subtitle>
<torqueitem>
<t-value1>209</t-value1>
<t-value2>2131</t-value2>
<t-value4>154</t-value4>
</torqueitem>
</torque>
</content1>
</s-1>
<s-1 id="RM000003BNP00KX_02_0013" proc-id="RM22W0E___00009JQ00000">
<ptxt>MEASURE VEHICLE HEIGHT (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the inspection on a level surface.</ptxt>
</item>
<item>
<ptxt>Ensure that the wheels are on the ground and facing straight ahead.</ptxt>
</item>
<item>
<ptxt>Perform the inspection with the vehicle load completely on the suspension.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Perform this step with the fuel tank full.</ptxt>
</item>
<item>
<ptxt>If there are any parts installed to the vehicle which place any unbalanced load on the left or right side of the vehicle, remove them.</ptxt>
</item>
</list1>
</atten4>
<s2>
<ptxt>Set the tire pressure to the specified value(s) (See page <xref label="Seep01" href="RM000003DGS00LX"/>).</ptxt>
</s2>
<s2>
<ptxt>Bounce the vehicle to stabilize the suspension.</ptxt>
</s2>
<s2>
<ptxt>Measure the distance from the ground to the top of the bumper and calculate the difference in the vehicle height between left and right. Perform this procedure for both the front and rear wheels.</ptxt>
<figure>
<graphic graphicname="C171398" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Height difference of left and right sides</title>
<specitem>
<ptxt>15 mm (0.591 in.) or less</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>If not as specified, perform the vehicle tilt calibration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003BNP00KX_02_0014" proc-id="RM22W0E___00009W800000">
<ptxt>CLOSE STABILIZER CONTROL WITH ACCUMULATOR HOUSING SHUTTER VALVE (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the inspection on a level surface.</ptxt>
</item>
<item>
<ptxt>Ensure that the wheels are on the ground and facing straight ahead.</ptxt>
</item>
<item>
<ptxt>Perform the inspection with the vehicle load completely on the suspension.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Perform this step with the fuel tank full.</ptxt>
</item>
<item>
<ptxt>If there are any parts installed to the vehicle which place any unbalanced load on the left or right side of the vehicle, remove them.</ptxt>
</item>
</list1>
</atten4>
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, tighten the lower and upper chamber shutter valves of the stabilizer control with accumulator housing.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003BNP00KX_02_0017" proc-id="RM22W0E___00009VZ00000">
<ptxt>INSTALL STABILIZER CONTROL VALVE PROTECTOR (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172095" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the valve protector with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the clamp, and connect the connector to the valve protector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003BNP00KX_02_0008" proc-id="RM22W0E___00009K100000">
<ptxt>CHECK SPEED SENSOR SIGNAL</ptxt>
<content1 releasenbr="1">
<s2>

<ptxt>w/ VSC:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM00000452K00NX"/>)</ptxt>
</s2>
<s2>
<ptxt>w/ ABS:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000054KD001X"/>)</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>