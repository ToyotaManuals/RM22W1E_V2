<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001S" variety="S001S">
<name>STEERING COLUMN</name>
<ttl id="12038_S001S_7C3QD_T00JG" variety="T00JG">
<name>POWER TILT AND POWER TELESCOPIC STEERING COLUMN SYSTEM</name>
<para id="RM000000XZ504EX" category="U" type-id="3001G" name-id="SR02U-22" from="201301">
<name>TERMINALS OF ECU</name>
<subpara id="RM000000XZ504EX_z0" proc-id="RM22W0E___0000BDX00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK MULTIPLEX TILT AND TELESCOPIC ECU</ptxt>
<figure>
<graphic graphicname="F044288E13" width="7.106578999in" height="1.771723296in"/>
</figure>
<table>
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>A-2 (TEM+) - E27-11 (GND)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>- (Not available) - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Telescopic motor output</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Telescopic steering contracts</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Telescopic motor not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-3 (VCE) - A-5 (E2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>- (Not available)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Telescopic sensor power source</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 16 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-4 (TES) - A-5 (E2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>- (Not available) </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Telescopic sensor signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Telescopic motor operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
<ptxt>High: 8 to 16 V</ptxt>
<ptxt>Low: Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-5 (E2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>- (Not available) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Telescopic sensor ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>A-6 (TEM-) - E27-11 (GND)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>- (Not available) - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Telescopic motor output</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Telescopic steering extends</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Telescopic motor not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E27-1 (TIM-) - E27-11 (GND)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>LG - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Tilt motor output</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Steering tilts down</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tilt motor not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E27-2 (+B) - E27-11 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Motor power source</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E27-3 (MSW) - E27-4 (VC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - Y</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tilt and telescopic manual switch signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.9 to 5.1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E27-5 (CANP) - E27-11 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>CAN communication</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E27-6 (VCI) - E27-17 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Y - G</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tilt sensor power source</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 16 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E27-7 (TIS) - E27-17 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W - G</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tilt sensor signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tilt motor operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
<ptxt>High: 8 to 16 V</ptxt>
<ptxt>Low: Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E27-8 (IG) - E27-11 (GND) </ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>IG signal/IG power source</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E27-9 (ECUB) - E27-11 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ECU power source</ptxt>
</entry>

<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E27-10 (TIM+) - E27-11 (GND)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>GR - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Tilt motor output</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Steering tilts up</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tilt motor not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E27-11 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ECU ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E27-14 (CANN) - E27-11 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>CAN communication</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E27-17 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tilt sensor ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>