<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000PFA0VKX" category="C" type-id="302GX" name-id="ESUA3-07" from="201308">
<dtccode>P0037</dtccode>
<dtcname>Oxygen Sensor Heater Control Circuit Low (Bank 1 Sensor 2)</dtcname>
<dtccode>P0038</dtccode>
<dtcname>Oxygen Sensor Heater Control Circuit High (Bank 1 Sensor 2)</dtcname>
<dtccode>P0057</dtccode>
<dtcname>Oxygen Sensor Heater Control Circuit Low (Bank 2 Sensor 2)</dtcname>
<dtccode>P0058</dtccode>
<dtcname>Oxygen Sensor Heater Control Circuit High (Bank 2 Sensor 2)</dtcname>
<dtccode>P102D</dtccode>
<dtcname>O2 Sensor Heater Circuit Performance Bank 1 Sensor 2 Stuck ON</dtcname>
<dtccode>P105D</dtccode>
<dtcname>O2 Sensor Heater Circuit Performance Bank 2 Sensor 2 Stuck ON</dtcname>
<subpara id="RM000000PFA0VKX_01" type-id="60" category="03" proc-id="RM22W0E___00001F400001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>Refer to DTC P0136 (See page <xref label="Seep01" href="RM000000TEO0EVX_01"/>).</ptxt>
</item>
</list1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When any of these DTCs are stored, the ECM enters fail-safe mode. The ECM turns off the heated oxygen sensor heater in fail-safe mode. The ECM continues operating in fail-safe mode until the engine switch is turned off.</ptxt>
</item>
<item>
<ptxt>The ECM provides a pulse-width modulated control circuit to adjust the current through the heater. The heated oxygen sensor heater circuit uses a relay on the +B side of the circuit.</ptxt>
<figure>
<graphic graphicname="A164717E22" width="7.106578999in" height="3.779676365in"/>
</figure>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0037</ptxt>
<ptxt>P0057</ptxt>
</entry>
<entry valign="middle">
<ptxt>The heater current is below the specified value while the heater is operating (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Open in heated oxygen sensor heater circuit</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor heater (for Sensor 2)</ptxt>
</item>
<item>
<ptxt>No. 1 integration relay</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0038</ptxt>
<ptxt>P0058</ptxt>
</entry>
<entry valign="middle">
<ptxt>The heater current is higher than the specified value while the heater is operating (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Short in heated oxygen sensor heater circuit</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor heater (for Sensor 2)</ptxt>
</item>
<item>
<ptxt>No. 1 integration relay</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P102D</ptxt>
<ptxt>P105D</ptxt>
</entry>
<entry valign="middle">
<ptxt>The heater current is higher than the specified value while the heater is not operating (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000PFA0VKX_02" type-id="64" category="03" proc-id="RM22W0E___00001F500001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The sensing portion of the heated oxygen sensor has a zirconia element which is used to detect the oxygen concentration in the exhaust gas. If the zirconia element is at the appropriate temperature and the difference between the oxygen concentrations surrounding the inside and outside surfaces of the sensor is large, the zirconia element generates voltage signals. In order to increase the oxygen concentration detecting capacity of the zirconia element, the ECM supplements the heat from the exhaust with heat from a heating element inside the sensor.</ptxt>
<topic>
<title>Heated oxygen sensor heater range check (P0037, P0038, P0057, P0058, P102D and P105D):</title>
<ptxt>The ECM monitors the current applied to the heated oxygen sensor heater to check the heater for malfunctions.</ptxt>
<ptxt>If the heater current is outside the normal range, the signal transmitted by the heated oxygen sensor becomes inaccurate. When the current in the heated oxygen sensor heater is outside the normal operating range, the ECM interprets this as a malfunction in the sensor heater and stores a DTC.</ptxt>
</topic>
</content5>
</subpara>
<subpara id="RM000000PFA0VKX_14" type-id="73" category="03" proc-id="RM22W0E___00001FD00001">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A198819E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on [A].</ptxt>
</item>
<item>
<ptxt>Start the engine and idle it for 5 minutes or more [B].</ptxt>
</item>
<item>
<ptxt>With the vehicle stationary, depress the accelerator pedal and maintain an engine speed of 3000 rpm for 1 minute [C].</ptxt>
</item>
<item>
<ptxt>Idle the engine for 5 minutes or more [D].</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes [E].</ptxt>
</item>
<item>
<ptxt>Read the pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a pending DTC is output, the system is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If a pending DTC is not output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P0037, P0038, P0057, P0058, P102D or P105D.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows INCOMPLETE or N/A, perform steps [B] through [E] again.</ptxt>
</item>
</list1>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000PFA0VKX_08" type-id="32" category="03" proc-id="RM22W0E___00001F600001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0136 (See page <xref label="Seep01" href="RM000000TEO0EVX_08"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFA0VKX_09" type-id="51" category="05" proc-id="RM22W0E___00001F700001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Sensor 2 refers to the sensor mounted behind the Three-way Catalytic Converter (TWC) and located far from the engine assembly.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Refer to "Data List / Active Test" [O2 Heater B1S2, O2 Heater B2S2, O2 Heater Curr Val B1S2 and O2 Heater Curr Val B2S2] (See page <xref label="Seep01" href="RM000000SXS0B1X"/>).</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Sensor 1 refers to the sensor closest to the engine assembly.</ptxt>
</item>
<item>
<ptxt>Sensor 2 refers to the sensor farthest away from the engine assembly.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFA0VKX_10" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PFA0VKX_10_0001" proc-id="RM22W0E___000019E00001">
<testtitle>INSPECT HEATED OXYGEN SENSOR (HEATER RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the heated oxygen sensor (See page <xref label="Seep01" href="RM0000031YM01OX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFA0VKX_10_0002" fin="false">OK</down>
<right ref="RM000000PFA0VKX_10_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0002" proc-id="RM22W0E___00001F800001">
<testtitle>CHECK TERMINAL VOLTAGE (+B OF HEATED OXYGEN SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A218939E05" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the heated oxygen sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC3" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C20-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Heated Oxygen Sensor)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bank 1 sensor 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bank 2 sensor 2</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PFA0VKX_10_0007" fin="false">OK</down>
<right ref="RM000000PFA0VKX_10_0015" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0007" proc-id="RM22W0E___00001FA00001">
<testtitle>CHECK HARNESS AND CONNECTOR (HEATED OXYGEN SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the heated oxygen sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C20-1 (HT1B) - C46-45 (HT1B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-1 (HT2B) - C46-44 (HT2B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C20-1 (HT1B) or C46-45 (HT1B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-1 (HT2B) or C46-44 (HT2B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C20-1 (HT1B) - C45-45 (HT1B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-1 (HT2B) - C45-44 (HT2B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C20-1 (HT1B) or C45-45 (HT1B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-1 (HT2B) or C45-44 (HT2B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PFA0VKX_10_0008" fin="false">OK</down>
<right ref="RM000000PFA0VKX_10_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0008" proc-id="RM22W0E___00001FB00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK187X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle in accordance with the driving pattern described in Confirmation Driving Pattern.</ptxt>
</test1>
<test1>
<ptxt>Read the output pending DTCs using the GTS.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No pending DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Pending DTC P0037, P0038, P0057, P0058, P102D and/or P105D is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PFA0VKX_10_0009" fin="true">A</down>
<right ref="RM000000PFA0VKX_10_0011" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0015" proc-id="RM22W0E___00001FC00001">
<testtitle>INSPECT NO. 1 INTEGRATION RELAY (EFI)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the No. 1 integration relay (EFI) (See page <xref label="Seep01" href="RM000003BLB02WX_01_0003"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFA0VKX_10_0004" fin="false">OK</down>
<right ref="RM000000PFA0VKX_10_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0004" proc-id="RM22W0E___00001F900001">
<testtitle>CHECK HARNESS AND CONNECTOR (HEATED OXYGEN SENSOR - NO. 1 INTEGRATION RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the heated oxygen sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C20-2 (+B) - 1B-4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-2 (+B) - 1B-4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C20-2 (+B) or 1B-4 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C56-2 (+B) or 1B-4 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PFA0VKX_10_0010" fin="true">OK</down>
<right ref="RM000000PFA0VKX_10_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0013">
<testtitle>REPLACE HEATED OXYGEN SENSOR<xref label="Seep01" href="RM0000031YL01OX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0011">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0006">
<testtitle>REPLACE NO. 1 INTEGRATION RELAY (EFI)</testtitle>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0009">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ13TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFA0VKX_10_0010">
<testtitle>CHECK ECM POWER SOURCE CIRCUIT<xref label="Seep01" href="RM000001DN80A7X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>