<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12042_S001W" variety="S001W">
<name>G-BOOK</name>
<ttl id="12042_S001W_7C3RH_T00KK" variety="T00KK">
<name>G-BOOK SYSTEM</name>
<para id="RM000003YUH029X" category="T" type-id="3001H" name-id="GB04M-43" from="201301" to="201308">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM000003YUH029X_z0" proc-id="RM22W0E___0000CP200000">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses and relays related to this system before inspecting the suspected areas below.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>G-BOOK On-screen Message</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row>
<entry colsep="1" valign="middle">
<ptxt>A message is displayed indicating to check the cellular phone every time the phone is connected. (When connecting via Bluetooth.)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Ask the customer to confirm if the cellular phone is G-BOOK compatible as there is a high possibility that it is not.</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry morerows="1" colsep="1" valign="middle">
<ptxt>A message is displayed indicating to check the cellular phone. (When connecting via Bluetooth.)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Proceed to "Communication Access Failed".</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003ZGD04OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Multi-media module receiver assembly</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003AHY01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="1" colsep="1" valign="middle">
<ptxt>A message is displayed indicating that a cellular phone is not connected. (When connecting via Bluetooth.)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Proceed to "Communication Access Failed".</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003ZGD04OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Multi-media module receiver assembly</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003AHY01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>A message is displayed temporarily indicating to try again after waiting for a while because the lines are busy.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>This is caused when there is a problem with the radio wave signal conditions or when the base station is busy. This is not a malfunction (inform the customer that this is not a malfunction and ask them to use the system as is).</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>A message is displayed indicating to try again after waiting for a while because the lines are busy.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Telephone calls are being restricted by the communication provider. Explain to the customer that they need to wait for a while before using the service.</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>A message is displayed temporarily indicating to try again after moving the vehicle because the center cannot be accessed, as the screen cannot be displayed due to a busy line, etc. </ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>This is caused when there is a problem with the radio wave signal conditions or when the base station is busy. This is not a malfunction (inform the customer that this is not a malfunction and ask them to use the system as is).</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>A message is displayed continuously indicating to try again after moving the vehicle because the center cannot be accessed, as the screen cannot be displayed due to a busy line, etc. </ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Proceed to "Communication Access Failed".</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003ZGD04OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="1" colsep="1" valign="middle" align="left">
<ptxt>A message is displayed indicating to try again after waiting for a while because the information cannot be retrieved due to a busy line, etc.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Wait for a while and perform the operation again in a different location.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Communication Access Failed".</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003ZGD04OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colsep="1" valign="middle" align="left">
<ptxt>A message is displayed indicating to try again because the information was not able to be retrieved.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Communication Access Failed".</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003ZGD04OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="1" colsep="1" valign="middle" align="left">
<ptxt>A message is displayed indicating to move the vehicle to a communication service area as it is outside a service area.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Wait for a while and perform the operation again in a different location (within the communication service area).</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Communication Access Failed".</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003ZGD04OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle" align="left">
<ptxt>A message is displayed indicating to move the vehicle to where a GPS signal can be received because the time data is old as communication is impossible.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to Problem Symptoms Table (GPS mark is not displayed) of the Navigation System section.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000011BR0KYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>If the GPS mark is displayed, wait for a while and perform the operation again in a different location.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>If the problem symptom is duplicated, replace the multi-media module receiver assembly.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003AHY01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>A message is displayed indicating that G-BOOK is not available as a microphone is not connected, which is required to use G-BOOK.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Microphone Circuit between Microphone and Radio Receiver" of the Navigation System section.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002NND0LOX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="2" colsep="1" valign="middle">
<ptxt>A message is displayed indicating that bookmark registration failed.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Proceed to Operation Check (Check Hard Disk Drive) of the Navigation System section.</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003SKF0DCX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Multi-media module receiver assembly</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003AHY01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Hard disc</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002V0L01CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="2" colsep="1" valign="middle">
<ptxt>A message is displayed indicating that bookmark deletion failed.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Proceed to Operation Check (Check Hard Disk Drive) of the Navigation System section.</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003SKF0DCX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Multi-media module receiver assembly</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003AHY01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Hard disc</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002V0L01CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>G-BOOK Announcement Sound</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>G-BOOK announcement sound is not output.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Adjust the volume by referring to the owner's manual. G-BOOK announcement sound is not linked with the audio volume.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Contract</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>A message is displayed indicating to apply for G-BOOK via G-BOOK.com on the main menu screen.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Explain to the customer that the appropriate G-BOOK setup must be performed in order to use the G-BOOK service. Ask the customer to perform the appropriate G-BOOK setup by referring to the owner's manual.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>A message is displayed indicating that it is necessary to apply for G-BOOK.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Explain to the customer that the appropriate G-BOOK setup must be performed in order to use the G-BOOK service. Ask the customer to perform the appropriate G-BOOK setup by referring to the owner's manual.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>A message is displayed indicating to ask the dealer about applying for the service as it is necessary to create a contract before using the service.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Fill in the vehicle contract setting request form and contact the G-BOOK support center.</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>A message is displayed indicating to perform communication setting via G-BOOK on the setting/editing screen as the communication settings of the cellular phone are required.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>As this message may be displayed when G-BOOK.com is selected just after the cancellation procedure is completed, select G-BOOK.com again after waiting for 6 minutes or more or after turning the engine switch off.</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>My Request On-screen Message</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>A message is displayed indicating that the service is not available now.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "G-BOOK Service cannot be Used".</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003ZGF04LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>