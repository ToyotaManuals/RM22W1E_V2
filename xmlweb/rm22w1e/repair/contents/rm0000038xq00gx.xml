<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3YZ_T00S2" variety="T00S2">
<name>RELAY</name>
<para id="RM0000038XQ00GX" category="G" type-id="8000T" name-id="LE36W-02" from="201301">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM0000038XQ00GX_01" type-id="01" category="01">
<s-1 id="RM0000038XQ00GX_01_0001" proc-id="RM22W0E___0000IYD00000">
<ptxt>INSPECT HEADLIGHT RELAY (HEAD HI, HEAD LO)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the headlight relay from the engine room relay box.</ptxt>
<figure>
<graphic graphicname="E155877" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="A092673E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>When battery voltage is not applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>When battery voltage is applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the relay.</ptxt>
</s2>
<s2>
<ptxt>Install the headlight relay to the engine room relay box.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038XQ00GX_01_0004" proc-id="RM22W0E___0000IYE00000">
<ptxt>INSPECT FOG LIGHT RELAY (FR FOG)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fog light relay from the engine room relay box.</ptxt>
<figure>
<graphic graphicname="E155878" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E145692E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>When battery voltage is not applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>When battery voltage is applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the relay.</ptxt>
</s2>
<s2>
<ptxt>Install the fog light relay to the engine room relay box.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038XQ00GX_01_0007" proc-id="RM22W0E___0000IYF00000">
<ptxt>INSPECT STOP LIGHT CONTROL RELAY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Floor Under Cover:</ptxt>
<ptxt>Remove the No. 1 under cover (See page <xref label="Seep01" href="RM0000039OQ00KX_01_0024"/>).</ptxt>
</s2>
<s2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E159754E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A39-6 (+B) - A39-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>A39-2 (STP) - Body grand</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Stop light switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Stop light switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>A39-8 (OUT) - Body grand</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Stop light switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Stop light switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the stop light control relay.</ptxt>
</s2>
<s2>
<ptxt>w/ Floor Under Cover:</ptxt>
<ptxt>Install the No. 1 under cover (See page <xref label="Seep02" href="RM0000039ON00KX_01_0035"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>