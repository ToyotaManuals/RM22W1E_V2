<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S002G" variety="S002G">
<name>WINDOW / GLASS</name>
<ttl id="12064_S002G_7C3X2_T00Q5" variety="T00Q5">
<name>POWER WINDOW CONTROL SYSTEM (for Models with Jam Protection Function on 4 Windows)</name>
<para id="RM000000PY204XX" category="D" type-id="3001B" name-id="WS01V-89" from="201301">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM000000PY204XX_z0" proc-id="RM22W0E___0000HW500000">
<content5 releasenbr="2">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use these procedures to troubleshoot the power window control system.</ptxt>
</item>
<item>
<ptxt>*: Use the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<spec>
<title>Standard Voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding.</ptxt>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK FOR DTC*</testtitle>
<test1>
<ptxt>Check for DTCs and make a note of any codes that are output.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs.</ptxt>
</test1>
<test1>
<ptxt>Recheck for DTCs. Based on the DTCs output in the first step, try to force output of the same DTC by simulating the symptoms indicated by the DTC.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="2.79in"/>
<colspec colname="COL2" colwidth="1.34in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No DTCs output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B2311 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B2312 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B2313 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>LIN communication system DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO DIAGNOSTIC TROUBLE CODE CHART (See page <xref label="Seep01" href="RM0000027KN01PX"/>)</action-ci-right>
<result>C</result>
<action-ci-right>GO TO LIN COMMUNICATION SYSTEM (See page <xref label="Seep03" href="RM000002S88023X"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to the Problem Symptoms Table (See page <xref label="Seep08" href="RM0000027QG03FX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="2.78in"/>
<colspec colname="COL2" colwidth="1.35in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Fault not listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fault listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO STEP 6</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>OVERALL ANALYSIS AND TROUBLESHOOTING*</testtitle>
<test1>
<ptxt>Operation Check (See page <xref label="Seep04" href="RM0000016810CPX"/>).</ptxt>
</test1>
<test1>
<ptxt>Terminals of ECU (See page <xref label="Seep05" href="RM0000027QN02AX"/>).</ptxt>
</test1>
<test1>
<ptxt>Data List / Active Test (See page <xref label="Seep06" href="RM0000027QL03DX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>REPAIR OR REPLACE</testtitle>
<table>
<title>Result</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="2.78in"/>
<colspec colname="COL2" colwidth="1.35in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Door window regulator and power window regulator motor assembly is replaced or removed/installed.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Door window regulator and power window regulator motor assembly is not replaced or removed/installed.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<results>
<result>B</result>
<action-ci-right>GO TO STEP 8</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>RESET POWER WINDOW REGULATOR MOTOR</testtitle>
<test1>
<ptxt>If a power window switch LED is blinking, initialize the power window regulator motor (See page <xref label="Seep07" href="RM000000PYB02SX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>