<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000000SVT0RCX" category="C" type-id="302B4" name-id="ES11GZ-001" from="201301">
<dtccode>P0327</dtccode>
<dtcname>Knock Sensor 1 Circuit Low Input (Bank 1 or Single Sensor)</dtcname>
<dtccode>P0328</dtccode>
<dtcname>Knock Sensor 1 Circuit High Input (Bank 1 or Single Sensor)</dtcname>
<dtccode>P0332</dtccode>
<dtcname>Knock Sensor 2 Circuit Low Input (Bank 2)</dtcname>
<dtccode>P0333</dtccode>
<dtcname>Knock Sensor 2 Circuit High Input (Bank 2)</dtcname>
<subpara id="RM000000SVT0RCX_01" type-id="60" category="03" proc-id="RM22W0E___00000GA00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Flat-type knock sensors (non-resonant type) have structures that can detect vibrations between approximately 6 kHz and 15 kHz.</ptxt>
<ptxt>2 knock sensors are fitted onto the engine block to detect engine knocking.</ptxt>
<ptxt>Each knock sensor contains a piezoelectric element which generates a voltage when it becomes deformed.</ptxt>
<ptxt>The voltage is generated when the engine block vibrates due to knocking. Any occurrence of engine knocking can be suppressed by delaying the ignition timing.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0327</ptxt>
<ptxt>P0332</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output voltage of the knock sensor is below 0.5 V (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Short in knock sensor circuit</ptxt>
</item>
<item>
<ptxt>Knock sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0328</ptxt>
<ptxt>P0333</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output voltage of the knock sensor is higher than 4.5 V (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Open in knock sensor circuit</ptxt>
</item>
<item>
<ptxt>Knock sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When DTC P0327, P0328, P0332 or P0333 is stored, the ECM enters fail-safe mode. During fail-safe mode, the ignition timing is delayed to its maximum retardation. The ECM continues operating in fail-safe mode until the ignition switch is turned off.</ptxt>
</atten4>
<ptxt>Reference: Inspection using an oscilloscope</ptxt>
<figure>
<graphic graphicname="A085286E27" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>The correct waveform is as shown.</ptxt>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal</ptxt>
</entry>
<entry valign="middle">
<ptxt>KNK1 - EKNK</ptxt>
<ptxt>KNK2 - EKN2</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Equipment Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 V/DIV.</ptxt>
<ptxt>0.01 to 1 msec./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine speed at 4000 rpm with warm engine</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000SVT0RCX_02" type-id="64" category="03" proc-id="RM22W0E___00000GB00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the output voltage transmitted by the knock sensor remains low or high for more than 1 second, the ECM interprets this as a malfunction in the sensor circuit and stores a DTC.</ptxt>
<ptxt>The monitor for DTCs P0327, P0328, P0332 and P0333 begins to run 5 seconds after the engine is started.</ptxt>
<ptxt>If the malfunction is not repaired successfully, DTC P0327, P0328, P0332 or P0333 is stored 5 seconds after the engine is next started.</ptxt>
</content5>
</subpara>
<subpara id="RM000000SVT0RCX_06" type-id="32" category="03" proc-id="RM22W0E___00000GC00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A247758E02" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000SVT0RCX_07" type-id="51" category="05" proc-id="RM22W0E___00000GD00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTCs P0327 and P0328 are for the bank 1 knock sensor circuit.</ptxt>
</item>
<item>
<ptxt>DTCs P0332 and P0333 are for the bank 2 knock sensor circuit.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000SVT0RCX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000SVT0RCX_09_0007" proc-id="RM22W0E___00000GG00000">
<testtitle>CHECK KNOCK SENSOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A159981E32" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C45-94 (KNK1) - C45-93 (EKNK)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>120 to 280 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-117 (KNK2) - C45-116 (EKN2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>120 to 280 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C46-94 (KNK1) - C46-93 (EKNK)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>120 to 280 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-117 (KNK2) - C46-116 (EKN2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>120 to 280 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000SVT0RCX_09_0003" fin="false">OK</down>
<right ref="RM000000SVT0RCX_09_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000SVT0RCX_09_0003" proc-id="RM22W0E___00000GF00000">
<testtitle>CHECK TERMINAL VOLTAGE (ECM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A108338E21" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the CU1 connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.51in"/>
<colspec colname="COLSPEC1" colwidth="1.31in"/>
<colspec colname="COL2" colwidth="1.31in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>CU1 female connector 2 - 5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CU1 female connector 4 - 6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Female</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to ECM Wire)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000SVT0RCX_09_0006" fin="true">OK</down>
<right ref="RM000000SVT0RCX_09_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000SVT0RCX_09_0002" proc-id="RM22W0E___00000GE00000">
<testtitle>CHECK HARNESS AND CONNECTOR (KNOCK SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the knock sensor connectors.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U2-2 - C45-94 (KNK1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U2-1 - C45-93 (EKNK)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U1-2 - C45-117 (KNK2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U1-1 - C45-116 (EKN2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U2-2 or C45-94 (KNK1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U2-1 or C45-93 (EKNK) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U1-2 or C45-117 (KNK2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U1-1 or C45-116 (EKN2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U2-2 - C46-94 (KNK1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U2-1 - C46-93 (EKNK)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U1-2 - C46-117 (KNK2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U1-1 - C46-116 (EKN2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U2-2 or C46-94 (KNK1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U2-1 or C46-93 (EKNK) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U1-2 or C46-117 (KNK2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U1-1 or C46-116 (EKN2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000SVT0RCX_09_0011" fin="true">OK</down>
<right ref="RM000000SVT0RCX_09_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000SVT0RCX_09_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000SVT0RCX_09_0013">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000SVT0RCX_09_0006">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ107X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000SVT0RCX_09_0011">
<testtitle>REPLACE KNOCK SENSOR<xref label="Seep01" href="RM0000028AW016X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>