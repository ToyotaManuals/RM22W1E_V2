<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12018_S001B" variety="S001B">
<name>CLUTCH</name>
<ttl id="12018_S001B_7C3N0_T00G3" variety="T00G3">
<name>CLUTCH MASTER CYLINDER (for RHD)</name>
<para id="RM0000032QZ007X" category="A" type-id="30014" name-id="CL2BY-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000032QZ007X_01" type-id="01" category="01">
<s-1 id="RM0000032QZ007X_01_0003" proc-id="RM22W0E___00008W700000">
<ptxt>INSTALL CLUTCH MASTER CYLINDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the master cylinder.</ptxt>
</s2>
<s2>
<ptxt>Apply MP grease to the contact surface of the clevis and clutch pedal.</ptxt>
</s2>
<s2>
<ptxt>Connect the clutch pedal to the clevis, and then install the master cylinder with the 2 nuts.</ptxt>
<figure>
<graphic graphicname="C173260" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>120</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the clutch pedal support bolt.</ptxt>
<figure>
<graphic graphicname="C173249" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032QZ007X_01_0012" proc-id="RM22W0E___00008W800000">
<ptxt>CONNECT CLUTCH RESERVOIR TUBE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C173237" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the clutch reservoir tube with a new clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032QZ007X_01_0002" proc-id="RM22W0E___00008W600000">
<ptxt>CONNECT CLUTCH MASTER CYLINDER TO FLEXIBLE HOSE TUBE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C250845" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a union nut wrench, connect the tube.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032QZ007X_01_0011" proc-id="RM22W0E___000034P00000">
<ptxt>FILL RESERVOIR WITH BRAKE FLUID
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fill the reservoir with brake fluid.</ptxt>
<spec>
<title>Brake Fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT 3</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM0000032QZ007X_01_0004" proc-id="RM22W0E___000034Q00000">
<ptxt>BLEED CLUTCH LINE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172957" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the release cylinder bleeder plug cap.</ptxt>
</s2>
<s2>
<ptxt>Connect a vinyl tube to the bleeder plug.</ptxt>
</s2>
<s2>
<ptxt>Depress the clutch pedal several times, and then loosen the bleeder plug while the pedal is depressed.</ptxt>
</s2>
<s2>
<ptxt>When fluid no longer comes out, tighten the bleeder plug, and then release the clutch pedal.</ptxt>
</s2>
<s2>
<ptxt>Repeat the previous 2 steps until all the air in the fluid is completely bled.</ptxt>
</s2>
<s2>
<ptxt>Tighten the bleeder plug. </ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the bleeder plug cap.</ptxt>
</s2>
<s2>
<ptxt>Check that all the air has been bled from the clutch line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QZ007X_01_0008" proc-id="RM22W0E___000034R00000">
<ptxt>CHECK FLUID LEVEL IN RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the fluid level.</ptxt>
<ptxt>If the brake fluid level is low, check for leaks and inspect the disc brake pad. If necessary, refill the reservoir with brake fluid after repair or replacement.</ptxt>
<spec>
<title>Brake fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT 3</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM0000032QZ007X_01_0009">
<ptxt>INSPECT FOR CLUTCH FLUID LEAK</ptxt>
</s-1>
<s-1 id="RM0000032QZ007X_01_0010" proc-id="RM22W0E___00008V100000">
<ptxt>INSPECT AND ADJUST CLUTCH PEDAL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the clutch pedal height is correct.</ptxt>
<figure>
<graphic graphicname="C173230E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Clutch Pedal Height from Tibia Pad</title>
<specitem>
<ptxt>164.8 to 174.8 mm (6.49 to 6.88 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Adjust the clutch pedal height.</ptxt>
<s3>
<ptxt>Loosen the lock nut and turn the stopper bolt or clutch switch until the height is correct. Tighten the lock nut.</ptxt>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>160</t-value2>
<t-value4>12</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
<s2>
<ptxt>Check that the clutch pedal free play and push rod play are correct.</ptxt>
<atten4>
<ptxt>Pay close attention to the change in resistance to distinguish between pedal free play and push rod play while performing the inspection.</ptxt>
</atten4>
<figure>
<graphic graphicname="D031462E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Depress the clutch pedal until the clutch resistance begins to be felt.</ptxt>
<spec>
<title>Standard Clutch Pedal Free Play</title>
<specitem>
<ptxt>5.0 to 15.0 mm (0.197 to 0.591 in.)</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Gently depress the clutch pedal until the resistance begins to increase a little.</ptxt>
<spec>
<title>Standard Push Rod Play at Pedal Top</title>
<specitem>
<ptxt>1.0 to 5.0 mm (0.0394 to 0.197 in.)</ptxt>
</specitem>
</spec>
</s3>
</s2>
<s2>
<ptxt>Adjust the clutch pedal free play and push rod play.</ptxt>
<atten4>
<ptxt>The push rod play can be adjusted by changing the length of the push rod. Pedal free play changes together with push rod play.</ptxt>
</atten4>
<s3>
<ptxt>Loosen the lock nut and turn the push rod until the clutch pedal free play and push rod play are correct.</ptxt>
<atten3>
<ptxt>If pedal free play and push rod play are not within the standard range even after adjustment, inspect the related parts.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Tighten the lock nut.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>120</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>After adjusting the clutch pedal free play and push rod play, check the clutch pedal height.</ptxt>
<spec>
<title>Standard Clutch Pedal Height from Tibia Pad</title>
<specitem>
<ptxt>164.8 to 174.8 mm (6.49 to 6.88 in.)</ptxt>
</specitem>
</spec>
</s3>
</s2>
<s2>
<ptxt>Check the clutch release point.</ptxt>
<figure>
<graphic graphicname="CL00512E17" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Pull the parking brake lever and chock the wheels.</ptxt>
</s3>
<s3>
<ptxt>Start the engine and allow it to idle.</ptxt>
</s3>
<s3>
<ptxt>Without depressing the clutch pedal, slowly move the shift lever to R until the gears engage.</ptxt>
</s3>
<s3>
<ptxt>Gradually depress the clutch pedal and measure the stroke distance from the point that the gear noise stops (release point) up to the full stroke end position.</ptxt>
<spec>
<title>Standard Distance from Pedal Stroke End Position to Release Point</title>
<specitem>
<ptxt>20 mm (0.787 in.) or more</ptxt>
</specitem>
</spec>
<ptxt>If the distance is not as specified, perform the following operations:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Check the clutch pedal height.</ptxt>
</item>
<item>
<ptxt>Check the push rod play and pedal free play.</ptxt>
</item>
<item>
<ptxt>Bleed the clutch line.</ptxt>
</item>
<item>
<ptxt>Check the clutch cover and disc.</ptxt>
</item>
</list1>
</s3>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>