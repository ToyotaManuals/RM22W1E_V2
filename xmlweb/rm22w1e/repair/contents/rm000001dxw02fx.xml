<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000001DXW02FX" category="C" type-id="803KU" name-id="BCDWT-02" from="201301" to="201308">
<dtccode>C1241</dtccode>
<dtcname>Low Power Supply Voltage Malfunction</dtcname>
<subpara id="RM000001DXW02FX_01" type-id="60" category="03" proc-id="RM22W0E___0000ADV00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the voltage supplied to the IG1 terminal is within the DTC detection range due to malfunctions in components such as the battery and generator circuit, this DTC is stored.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1241</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<ptxt>1. Both of the following conditions continue for at least 10 seconds.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The vehicle speed is more than 3 km/h (2 mph). </ptxt>
</item>
<item>
<ptxt>The IG1 terminal voltage is below 9.5 V.</ptxt>
</item>
</list1>
<ptxt>2. All of the following conditions continue for at least 0.2 seconds.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The solenoid relay remains on. </ptxt>
</item>
<item>
<ptxt>The IG1 terminal voltage is below 9.5 V. </ptxt>
</item>
<item>
<ptxt>The relay contact is open.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery </ptxt>
</item>
<item>
<ptxt>ECU-IG No. 2 fuse</ptxt>
</item>
<item>
<ptxt>Charging system </ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Skid control ECU (Master cylinder solenoid)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001DXW02FX_02" type-id="32" category="03" proc-id="RM22W0E___0000ADW00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C173289E10" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001DXW02FX_03" type-id="51" category="05" proc-id="RM22W0E___0000ADX00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001DXW02FX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001DXW02FX_04_0003" proc-id="RM22W0E___0000ADY00000">
<testtitle>READ VALUE USING GTS (IG1 VOLTAGE VALUE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3. </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>IG1 Voltage Value</ptxt>
</entry>
<entry valign="middle">
<ptxt>IG1 voltage value/ Min.: 0.00 V, Max.: 20.00 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON: 11 to 14 V </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check the voltage output from the skid control ECU (master cylinder solenoid) displayed on the GTS.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The output voltage displayed on the GTS is within 11 to 14 V.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001DXW02FX_04_0014" fin="false">OK</down>
<right ref="RM000001DXW02FX_04_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXW02FX_04_0014" proc-id="RM22W0E___0000AE000000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00MX"/>). </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTCs are output (See page <xref label="Seep02" href="RM0000046KV00MX"/>).</ptxt>
<atten4>
<ptxt>Reinstall the sensors, connectors, etc. and restore the previous vehicle conditions before rechecking for DTCs.</ptxt>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.74in"/>
<colspec colname="COL2" colwidth="1.34in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output (When troubleshooting in accordance with Diagnostic Trouble Code Chart) </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output (When troubleshooting in accordance with Problem Symptoms Table)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXW02FX_04_0007" fin="true">A</down>
<right ref="RM000001DXW02FX_04_0018" fin="true">B</right>
<right ref="RM000001DXW02FX_04_0011" fin="true">C</right>
<right ref="RM000001DXW02FX_04_0012" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000001DXW02FX_04_0004" proc-id="RM22W0E___0000ABV00000">
<testtitle>CHECK HARNESS AND CONNECTOR (IG1 TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the skid control ECU (master cylinder solenoid) connector. </ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214161E68" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-46 (IG1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU (Master Cylinder Solenoid))</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXW02FX_04_0016" fin="false">OK</down>
<right ref="RM000001DXW02FX_04_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXW02FX_04_0016" proc-id="RM22W0E___0000ACJ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (GND1, GND2 AND GND3 TERMINAL)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<figure>
<graphic graphicname="C124799E58" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the skid control ECU (master cylinder solenoid) connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COLSPEC0" colwidth="1.11in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-1 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-32 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A25-4 (GND3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU [Master Cylinder Solenoid])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001DXW02FX_04_0006" fin="false">OK</down>
<right ref="RM000001DXW02FX_04_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXW02FX_04_0006" proc-id="RM22W0E___0000ADZ00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00MX"/>). </ptxt>
</test1>
<test1>
<ptxt>Check if the same DTCs are output (See page <xref label="Seep02" href="RM0000046KV00MX"/>).</ptxt>
<atten4>
<ptxt>Reinstall the sensors, connectors, etc. and restore the previous vehicle conditions before rechecking for DTCs.</ptxt>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.83in"/>
<colspec colname="COL2" colwidth="1.25in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output (When troubleshooting in accordance with Diagnostic Trouble Code Chart) </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output (When troubleshooting in accordance with Problem Symptoms Table)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXW02FX_04_0007" fin="true">A</down>
<right ref="RM000001DXW02FX_04_0018" fin="true">B</right>
<right ref="RM000001DXW02FX_04_0011" fin="true">C</right>
<right ref="RM000001DXW02FX_04_0012" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000001DXW02FX_04_0011">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXW02FX_04_0012">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000OS704BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXW02FX_04_0007">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXW02FX_04_0015">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DXW02FX_04_0018">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>