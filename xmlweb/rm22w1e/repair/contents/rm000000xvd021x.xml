<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12067_S002J" variety="S002J">
<name>MIRROR (EXT)</name>
<ttl id="12067_S002J_7C3Y2_T00R5" variety="T00R5">
<name>OUTER REAR VIEW MIRROR</name>
<para id="RM000000XVD021X" category="G" type-id="8000T" name-id="MI0OZ-03" from="201301">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM000000XVD021X_01" type-id="01" category="01">
<s-1 id="RM000000XVD021X_01_0002" proc-id="RM22W0E___0000IO700000">
<ptxt>CHECK POWER RETRACTABLE MIRROR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the engine switch on (ACC).</ptxt>
</s2>
<s2>
<ptxt>At each position of the outer mirror body, check the retractable mirror operation when operating the retract switch of the outer mirror.</ptxt>
<figure>
<graphic graphicname="B181037E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Move the mirrors to the driving position.</ptxt>
</s3>
<s3>
<ptxt>Press the retract switch.</ptxt>
</s3>
<s3>
<ptxt>Check that the right and left mirrors move from the driving position to the retracted position.</ptxt>
</s3>
<s3>
<ptxt>Move the mirrors to the driving position.</ptxt>
<figure>
<graphic graphicname="B181038E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Move one of the mirrors to the forward position by hand.</ptxt>
</s3>
<s3>
<ptxt>Press the retract switch.</ptxt>
</s3>
<s3>
<ptxt>Check that the mirror moves from the forward position to the retracted position.</ptxt>
</s3>
<s3>
<ptxt>Check that the other mirror moves from the driving position to the retracted position.</ptxt>
</s3>
<s3>
<ptxt>Move the mirrors to the driving position.</ptxt>
<figure>
<graphic graphicname="B181039E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Move one of the mirrors to the retracted position by hand.</ptxt>
</s3>
<s3>
<ptxt>Press the retract switch.</ptxt>
</s3>
<s3>
<ptxt>Check that the mirror moves from the driving position to the retracted position.</ptxt>
</s3>
<s3>
<ptxt>Move the mirrors to the retracted position.</ptxt>
<figure>
<graphic graphicname="B181040E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Press the retract switch.</ptxt>
</s3>
<s3>
<ptxt>Check that the right and left mirrors move from the retracted position to the driving position.</ptxt>
</s3>
<s3>
<ptxt>Move the mirrors to the retracted position.</ptxt>
<figure>
<graphic graphicname="B181041E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Move one of the mirrors to the driving position by hand.</ptxt>
</s3>
<s3>
<ptxt>Press the retract switch.</ptxt>
</s3>
<s3>
<ptxt>Check that the mirror moves from the retracted position to the driving position.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the operation of the outer mirror in relation to retract switch operations and engine switch position changes.</ptxt>
<s3>
<ptxt>When the mirror is operating, turn the engine switch off. Check that the mirror operation does not stop and continues until the operation has finished.</ptxt>
</s3>
<s3>
<ptxt>Repeat the step above. This time, before the mirror operation completes, turn the engine switch on (ACC) and press the retract switch. Check that the mirror operates in the opposite direction.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the operation of the outer mirror when it is restrained by an obstacle.</ptxt>
<s3>
<ptxt>When the mirror is moving to the retracted or driving position, restrain the mirror by hand. Check that the mirror stops moving.</ptxt>
</s3>
<s3>
<ptxt>With the mirror stopped partway, push the retract switch. Check that the mirror moves in the opposite direction it was moving when it was restrained.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000XVD021X_01_0003" proc-id="RM22W0E___0000IO800000">
<ptxt>CHECK REVERSE-SHIFT LINKED OPERATION OF MIRRORS</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181042E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</s2>
<s2>
<ptxt>Set the mirror switch to L or R.</ptxt>
</s2>
<s2>
<ptxt>Check that the mirror faces downward when the shift lever is moved to R.</ptxt>
</s2>
<s2>
<ptxt>When the shift lever is not in P, or the mirror switch is in the neutral position (OFF), check that the outer rear view mirror returns to the driving position.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000XVD021X_01_0004" proc-id="RM22W0E___0000IO900000">
<ptxt>CHECK MIRROR HEATER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</s2>
<s2>
<ptxt>Check that pressing the rear defogger switch illuminates the indicator and warms the mirror surface.</ptxt>
</s2>
<s2>
<ptxt>Check that after approximately 15 minutes, the indicator light turns off and the mirror heater deactivates.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>