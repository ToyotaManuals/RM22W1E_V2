<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MR_T00FU" variety="T00FU">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 3UR-FE)</name>
<para id="RM000000W6Z12AX" category="D" type-id="3001B" name-id="AT002X-844" from="201308">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM000000W6Z12AX_z0" proc-id="RM22W0E___00008BA00001">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>The ECM is connected to the CAN communication system. Therefore, before starting troubleshooting, make sure to check that there is no trouble in the CAN communication system.</ptxt>
</item>
<item>
<ptxt>*: Use the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CUSTOMER PROBLEM ANALYSIS</testtitle>
<test1>
<ptxt>Refer to How to Troubleshoot ECU Controlled Systems in the Introduction section (See page <xref label="Seep16" href="RM000002V5U015X"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding.</ptxt>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONNECT INTELLIGENT TESTER TO DLC3*</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK AND CLEAR DTCS AND FREEZE FRAME DATA*</testtitle>
<test1>
<ptxt>Refer to DTC Check/Clear (See page <xref label="Seep01" href="RM000000W770Y5X"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>VISUAL INSPECTION</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>SETTING CHECK MODE DIAGNOSIS*</testtitle>
<test1>
<ptxt>Refer to Check Mode Procedure (See page <xref label="Seep02" href="RM000000W780TEX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOM CONFIRMATION</testtitle>
<test1>
<ptxt>Refer to Road Test (See page <xref label="Seep03" href="RM000000W790OPX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Symptom does not occur</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Symptom occurs</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO STEP 10</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>SYMPTOM SIMULATION</testtitle>
<test1>
<ptxt>Refer to Electronic Circuit Inspection Procedure (See page <xref label="Seep04" href="RM000000UZ30DCX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>DTC CHECK*</testtitle>
<test1>
<ptxt>Refer to DTC Check/Clear (See page <xref label="Seep05" href="RM000000W770Y5X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO STEP 18</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>BASIC INSPECTION</testtitle>
<test1>
<ptxt>Refer to Park/Neutral Position Switch (See page <xref label="Seep07" href="RM0000010ND06KX"/>).</ptxt>
</test1>
<test1>
<ptxt>Refer to Shift Lever Assembly (See page <xref label="Seep09" href="RM000002M9A06QX"/>).</ptxt>
</test1>
<results>
<result>NG</result>
<action-ci-right>GO TO STEP 21</action-ci-right>
<result-ci-down>OK</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>MECHANICAL SYSTEM TESTS</testtitle>
<test1>
<ptxt>Refer to Mechanical System Tests (See page <xref label="Seep10" href="RM000000W7A0QTX"/>).</ptxt>
</test1>
<results>
<result>NG</result>
<action-ci-right>GO TO STEP 17</action-ci-right>
<result-ci-down>OK</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>HYDRAULIC TEST</testtitle>
<test1>
<ptxt>Refer to Hydraulic Test (See page <xref label="Seep11" href="RM000000W7B0PPX"/>).</ptxt>
</test1>
<results>
<result>NG</result>
<action-ci-right>GO TO STEP 17</action-ci-right>
<result-ci-down>OK</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>MANUAL SHIFTING TEST</testtitle>
<test1>
<ptxt>Refer to Manual Shifting Test (See page <xref label="Seep12" href="RM000000W7C0WDX"/>).</ptxt>
</test1>
<results>
<result>NG</result>
<action-ci-right>GO TO STEP 16</action-ci-right>
<result-ci-down>OK</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE CHAPTER 1</testtitle>
<test1>
<ptxt>Refer to Problem Symptoms Table (See page <xref label="Seep13" href="RM000000W730ZKX"/>).</ptxt>
</test1>
<results>
<result>NG</result>
<action-ci-right>GO TO STEP 19</action-ci-right>
<result-ci-down>OK</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE CHAPTER 2</testtitle>
<test1>
<ptxt>Refer to Problem Symptoms Table (See page <xref label="Seep14" href="RM000000W730ZKX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PART INSPECTION</testtitle>
<results>
<result>NG</result>
<action-ci-right>GO TO STEP 21</action-ci-right>
<result-ci-down>OK</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>DTC CHART</testtitle>
<test1>
<ptxt>Refer to Diagnostic Trouble Code Chart (See page <xref label="Seep15" href="RM0000030G909GX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CIRCUIT INSPECTION</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>IDENTIFICATION OF PROBLEM</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>REPAIR OR REPLACE</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>