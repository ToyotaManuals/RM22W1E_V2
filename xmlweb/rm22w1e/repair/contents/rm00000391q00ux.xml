<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UN_T00NQ" variety="T00NQ">
<name>REAR NO. 2 SEAT ASSEMBLY (except Face to Face Seat Type)</name>
<para id="RM00000391Q00UX" category="A" type-id="30014" name-id="SE9DX-01" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM00000391Q00UX_02" type-id="11" category="10" proc-id="RM22W0E___0000FYV00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000391Q00UX_01" type-id="01" category="01">
<s-1 id="RM00000391Q00UX_01_0001" proc-id="RM22W0E___0000FYM00000">
<ptxt>INSTALL REAR SEAT LOCK STRIKER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 4 seat lock strikers with 8 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>37</t-value1>
<t-value2>377</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Return the floor carpet to its original position.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391Q00UX_01_0002" proc-id="RM22W0E___0000FYN00000">
<ptxt>INSTALL NO. 3 SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 16 claws and 4 clips to install the 4 seat cushion covers.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391Q00UX_01_0003" proc-id="RM22W0E___0000FYO00000">
<ptxt>INSTALL FOLD SEAT LEG LOCK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fold seat leg lock with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 2 cable clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391Q00UX_01_0014" proc-id="RM22W0E___0000FYT00000">
<ptxt>INSTALL REAR ROOF NO. 1 AIR DUCT (w/ Rear Air Conditioning System)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154870" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the duct with the clip.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 claws to install the cooler plate.</ptxt>
<figure>
<graphic graphicname="E154869" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000391Q00UX_01_0004" proc-id="RM22W0E___0000FYP00000">
<ptxt>INSTALL REAR SEAT LOCK CONTROL LEVER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws to install the seat lock control lever.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391Q00UX_01_0017" proc-id="RM22W0E___0000FYU00000">
<ptxt>INSTALL FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Install the front quarter trim panel assembly LH (See page <xref label="Seep01" href="RM0000038MM00QX_02_0020"/>).</ptxt>
</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>Install the front quarter trim panel assembly LH (See page <xref label="Seep02" href="RM0000038MM00PX_02_0020"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391Q00UX_01_0018" proc-id="RM22W0E___000067B00000">
<ptxt>INSTALL REAR SEAT COVER CAP
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190214" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the rear seat cover cap on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 3 claws to install the rear seat cover cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391Q00UX_01_0020" proc-id="RM22W0E___000067C00000">
<ptxt>INSTALL REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 6 clips to install the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391Q00UX_01_0021" proc-id="RM22W0E___000067D00000">
<ptxt>INSTALL REAR DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181674" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws and 4 clips to install the scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391Q00UX_01_0022" proc-id="RM22W0E___000067F00000">
<ptxt>INSTALL REAR STEP COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181673" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the step cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the step cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391Q00UX_01_0010" proc-id="RM22W0E___0000FYQ00000">
<ptxt>INSTALL REAR NO. 2 SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181811E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Temporarily install the seat assembly with the 4 bolts.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Tighten the bolts in the order shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>37</t-value1>
<t-value2>377</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391Q00UX_01_0011" proc-id="RM22W0E___0000FYR00000">
<ptxt>INSTALL REAR NO. 2 SEAT HINGE COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and 2 claws to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391Q00UX_01_0012" proc-id="RM22W0E___0000FYS00000">
<ptxt>INSTALL REAR SEAT CUSHION HINGE COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Return the seat to the upright position.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391Q00UX_01_0023">
<ptxt>INSTALL REAR NO. 2 SEAT HEADREST ASSEMBLY LH (w/ Rear Center Seat Headrest)</ptxt>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>