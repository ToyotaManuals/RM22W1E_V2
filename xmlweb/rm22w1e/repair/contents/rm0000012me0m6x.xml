<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM0000012ME0M6X" category="C" type-id="302I1" name-id="ES117B-003" from="201308">
<dtccode>P0500</dtccode>
<dtcname>Vehicle Speed Sensor "A"</dtcname>
<subpara id="RM0000012ME0M6X_01" type-id="60" category="03" proc-id="RM22W0E___00002LZ00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Vehicles which are equipped with a manual transmission detect the vehicle speed using the speed sensor. The speed sensor transmits a 4-pulse signal for every revolution of the rotor shaft, which is rotated by the transmission output shaft via the driven gear. The 4-pulse signal is converted into a more precise rectangular waveform by the waveform shaping circuit inside the combination meter. The signal is then transmitted to the ECM. The ECM determines the vehicle speed based on the frequency of the pulse signal.</ptxt>
<figure>
<graphic graphicname="A115899E07" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>P0500</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry>
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry>
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Vehicle being driven</ptxt>
</entry>
<entry>
<ptxt>Conditions (a), (b), (c) and (d) are met for 8 seconds or more (2 trip detection logic):</ptxt>
<ptxt>(a) Engine coolant temperature is more than 70°C (158°F).</ptxt>
<ptxt>(b) Engine speed is between 1500 and 4000 rpm.</ptxt>
<ptxt>(c) Vehicle is running.</ptxt>
<ptxt>(d) No speed signal is input to the ECM.</ptxt>
</entry>
<entry morerows="1">
<list1 type="unordered">
<item>
<ptxt>Open or short in speedometer circuit</ptxt>
</item>
<item>
<ptxt>Speedometer assembly </ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry>
<ptxt>Drive vehicle during fuel-cut operation</ptxt>
</entry>
<entry>
<ptxt>Conditions (a), (b) and (c) are met for 5 seconds or more (2 trip detection logic):</ptxt>
<ptxt>(a) Vehicle is running.</ptxt>
<ptxt>(b) During the fuel-cut operation.</ptxt>
<ptxt>(c) No speed signal is input to the ECM.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>DTC No.</ptxt>
</entry>
<entry>
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0500</ptxt>
</entry>
<entry>
<ptxt>Vehicle Speed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000012ME0M6X_06" type-id="32" category="03" proc-id="RM22W0E___00002M000001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A160610E29" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000012ME0M6X_07" type-id="51" category="05" proc-id="RM22W0E___00002M100001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08YX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06NX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000012ME0M6X_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012ME0M6X_08_0001" proc-id="RM22W0E___00002M200001">
<testtitle>CHECK OPERATION OF SPEEDOMETER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Drive the vehicle and check whether the operation of the speedometer in the combination meter is normal.</ptxt>
<atten4>
<ptxt>The vehicle speed sensor is operating normally if the speedometer reading is normal.</ptxt>
<ptxt>If the speedometer does not operate, check it by following the procedure described in Speedometer Malfunction.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000012ME0M6X_08_0002" fin="false">OK</down>
<right ref="RM0000012ME0M6X_08_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0002" proc-id="RM22W0E___00002M300001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (VEHICLE SPEED)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / All Data / Vehicle Speed.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Vehicle speeds displayed on tester and speedometer display are equal.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<right ref="RM0000012ME0M6X_08_0007" fin="true">OK</right>
<right ref="RM0000012ME0M6X_08_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0005" proc-id="RM22W0E___00002M600001">
<testtitle>CHECK HARNESS AND CONNECTOR (COMBINATION METER ASSEMBLY - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COLSPEC2" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E7-4 (+S) - A38-8 (SPD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COLSPEC2" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E7-4 (+S) - A52-8 (SPD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COLSPEC2" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E7-4 (+S) or A38-8 (SPD) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COLSPEC2" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E7-4 (+S) or A52-8 (SPD) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012ME0M6X_08_0003" fin="false">OK</down>
<right ref="RM0000012ME0M6X_08_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0003" proc-id="RM22W0E___00002M400001">
<testtitle>CHECK COMBINATION METER ASSEMBLY (+S VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E154777E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-4 (+S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the combination meter connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012ME0M6X_08_0004" fin="false">OK</down>
<right ref="RM0000012ME0M6X_08_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0004" proc-id="RM22W0E___00002M500001">
<testtitle>CHECK COMBINATION METER ASSEMBLY (SPD SIGNAL OUTPUT WAVEFORM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A179960E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Remove the combination meter.</ptxt>
</test1>
<test1>
<ptxt>Connect the combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Move the shift lever to N.</ptxt>
</test1>
<test1>
<ptxt>Jack up the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Check the voltage between the terminal of the combination meter and the body ground while a wheel is turned slowly.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E7-4 (+S) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Voltage generated intermittently</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>The output voltage should fluctuate up and down, similarly to the diagram, when the wheel is turned slowly.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Reinstall the combination meter.</ptxt>
</test1>
</content6>
<res>
<right ref="RM0000012ME0M6X_08_0010" fin="true">OK</right>
<right ref="RM0000012ME0M6X_08_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0012" proc-id="RM22W0E___00002M700001">
<testtitle>CHECK COMBINATION METER ASSEMBLY (SPD SIGNAL INPUT WAVEFORM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A179960E10" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Remove the combination meter.</ptxt>
</test1>
<test1>
<ptxt>Connect the combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Move the shift lever to N.</ptxt>
</test1>
<test1>
<ptxt>Jack up the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Check the voltage between the terminal of the combination meter and the body ground while a wheel is turned slowly.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E7-3 (SI) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Voltage generated intermittently</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>The output voltage should fluctuate up and down, similarly to the diagram, when the wheel is turned slowly.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Reinstall the combination meter.</ptxt>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="2.07in"/>
<colspec colname="COL2" align="center" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Test result</ptxt>
</entry>
<entry>
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK (w/ Multi-information Display)</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (w/o Multi-information Display)</ptxt>
</entry>
<entry>
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG</ptxt>
</entry>
<entry>
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<right ref="RM0000012ME0M6X_08_0011" fin="true">A</right>
<right ref="RM0000012ME0M6X_08_0008" fin="true">B</right>
<right ref="RM0000012ME0M6X_08_0013" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0013" proc-id="RM22W0E___00002M800001">
<testtitle>CHECK HARNESS AND CONNECTOR (COMBINATION METER - BRAKE ACTUATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the brake actuator connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COLSPEC2" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E7-3 (SI) - A24-12 (SP1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COLSPEC2" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E7-3 (SI) or A24-12 (SP1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the brake actuator connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012ME0M6X_08_0015" fin="true">OK</down>
<right ref="RM0000012ME0M6X_08_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0006">
<testtitle>GO TO SPEEDOMETER MALFUNCTION<xref label="Seep01" href="RM000002ZM601EX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0007">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ13WX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (OTHER SYSTEMS RELATED TO SPEED SIGNAL)</testtitle>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329203AX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0011">
<testtitle>REPLACE COMBINATION METER ASSEMBLY (w/ Multi-information Display)<xref label="Seep01" href="RM0000038ID00LX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0008">
<testtitle>REPLACE COMBINATION METER ASSEMBLY (w/o Multi-information Display)<xref label="Seep01" href="RM0000039M200BX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0015">
<testtitle>REPLACE BRAKE ACTUATOR</testtitle>
</testgrp>
<testgrp id="RM0000012ME0M6X_08_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>