<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3XJ_T00QM" variety="T00QM">
<name>BACK DOOR</name>
<para id="RM000001BLF015X" category="N" type-id="3000G" name-id="DH2PA-02" from="201301">
<name>ADJUSTMENT</name>
<subpara id="RM000001BLF015X_01" type-id="01" category="01">
<s-1 id="RM000001BLF015X_01_0008" proc-id="RM22W0E___0000I9O00000">
<ptxt>INSPECT TAIL GATE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the clearance measurements of areas H to I are within the standard range.</ptxt>
<figure>
<graphic graphicname="B189146E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Measurement</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.8 to 6.8 mm (0.150 to 0.268 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.5 to 6.5 mm (0.138 to 0.256 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLF015X_01_0006" proc-id="RM22W0E___0000I9M00000">
<ptxt>ADJUST TAIL GATE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the hinge bolts.</ptxt>
</s2>
<s2>
<ptxt>Adjust the tail gate by moving it up and down and left and right so that the dimensions are within the specified range.</ptxt>
<figure>
<graphic graphicname="B181348" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Tighten the hinge bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>28</t-value1>
<t-value2>286</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Loosen the hinge bolts.</ptxt>
<figure>
<graphic graphicname="B181347" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Adjust the tail gate by moving it up and down and forward and backward so that the dimensions are within the specified range.</ptxt>
</s2>
<s2>
<ptxt>	Tighten the hinge bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>31</t-value1>
<t-value2>316</t-value2>
<t-value4>23</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLF015X_01_0007" proc-id="RM22W0E___0000I9N00000">
<ptxt>ADJUST DOOR LOCK STRIKER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T40 "TORX" socket, adjust the striker position by slightly loosening the striker mounting screws and hitting the striker with a plastic-faced hammer.</ptxt>
<figure>
<graphic graphicname="B181355" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a T40 "TORX" socket, tighten the striker mounting screws after the adjustment.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLF015X_01_0009" proc-id="RM22W0E___0000I9P00000">
<ptxt>INSPECT BACK DOOR PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the clearance measurements of areas A to G are within the standard range.</ptxt>
<figure>
<graphic graphicname="B182258E01" width="7.106578999in" height="4.7836529in"/>
</figure>
<spec>
<title>Standard Measurement</title>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.7 to 8.7 mm (0.224 to 0.343 in.)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.7 to 5.7 mm (0.106 to 0.224 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.0 to 3.0 mm (0.000 to 0.118 in.)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>F</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-1.4 to 1.6 mm (-0.0551 to 0.0630 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.75 to 6.75 mm (0.148 to 0.266 in.)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.0 mm (0.197 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.75 to 6.75 mm (0.148 to 0.266 in.)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLF015X_01_0003" proc-id="RM22W0E___0000I9L00000">
<ptxt>ADJUST REAR SWING GATE LOCK STRIKER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the door fit and door linkages are adjusted correctly.</ptxt>
<figure>
<graphic graphicname="B181346" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a T40 "TORX" socket, adjust the striker position by slightly loosening the striker mounting screws and hitting the striker with a plastic-faced hammer.</ptxt>
</s2>
<s2>
<ptxt>Using a T40 "TORX" socket, tighten the striker mounting screws after the adjustment.</ptxt>
<torque>
<torqueitem>
<t-value1>23</t-value1>
<t-value2>235</t-value2>
<t-value4>17</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLF015X_01_0001" proc-id="RM22W0E___0000I9K00000">
<ptxt>ADJUST BACK DOOR PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the hinge bolts.</ptxt>
<figure>
<graphic graphicname="B181344" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Adjust the back door by moving it up and down and left and right so that the dimensions are within the specified range.</ptxt>
</s2>
<s2>
<ptxt>Tighten the hinge bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>194</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Partially remove the roof headlining.</ptxt>
<s3>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000038MO00QX"/>)</ptxt>
</s3>
<s3>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000038MO00PX"/>)</ptxt>
</s3>
</s2>
<s2>
<ptxt>Loosen the hinge nuts.</ptxt>
<figure>
<graphic graphicname="B189860" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Adjust the back door by moving it up and down and forward and backward so that the dimensions are within the specified range.</ptxt>
</s2>
<s2>
<ptxt>Tighten the hinge nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>194</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the roof headlining.</ptxt>
<s3>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep03" href="RM0000038MM00QX"/>)</ptxt>
</s3>
<s3>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>(See page <xref label="Seep04" href="RM0000038MM00PX"/>)</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>