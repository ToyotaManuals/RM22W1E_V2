<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000XH40VYX" category="C" type-id="302BA" name-id="ES17MP-001" from="201308">
<dtccode>P0351</dtccode>
<dtcname>Ignition Coil "A" Primary / Secondary Circuit</dtcname>
<dtccode>P0352</dtccode>
<dtcname>Ignition Coil "B" Primary / Secondary Circuit</dtcname>
<dtccode>P0353</dtccode>
<dtcname>Ignition Coil "C" Primary / Secondary Circuit</dtcname>
<dtccode>P0354</dtccode>
<dtcname>Ignition Coil "D" Primary / Secondary Circuit</dtcname>
<dtccode>P0355</dtccode>
<dtcname>Ignition Coil "E" Primary / Secondary Circuit</dtcname>
<dtccode>P0356</dtccode>
<dtcname>Ignition Coil "F" Primary / Secondary Circuit</dtcname>
<dtccode>P0357</dtccode>
<dtcname>Ignition Coil "G" Primary / Secondary Circuit</dtcname>
<dtccode>P0358</dtccode>
<dtcname>Ignition Coil "H" Primary / Secondary Circuit</dtcname>
<subpara id="RM000000XH40VYX_01" type-id="60" category="03" proc-id="RM22W0E___00001D500001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>These DTCs indicate malfunctions relating to the primary circuit.</ptxt>
</item>
<item>
<ptxt>If DTC P0351 is stored, check the No. 1 ignition coil circuit.</ptxt>
</item>
<item>
<ptxt>If DTC P0352 is stored, check the No. 2 ignition coil circuit.</ptxt>
</item>
<item>
<ptxt>If DTC P0353 is stored, check the No. 3 ignition coil circuit.</ptxt>
</item>
<item>
<ptxt>If DTC P0354 is stored, check the No. 4 ignition coil circuit.</ptxt>
</item>
<item>
<ptxt>If DTC P0355 is stored, check the No. 5 ignition coil circuit.</ptxt>
</item>
<item>
<ptxt>If DTC P0356 is stored, check the No. 6 ignition coil circuit.</ptxt>
</item>
<item>
<ptxt>If DTC P0357 is stored, check the No. 7 ignition coil circuit.</ptxt>
</item>
<item>
<ptxt>If DTC P0358 is stored, check the No. 8 ignition coil circuit.</ptxt>
</item>
</list1>
</atten4>
<ptxt>A Direct Ignition System (DIS) is used on this vehicle.</ptxt>
<ptxt>The DIS is a 1-cylinder ignition system in which each cylinder is ignited by one ignition coil and one spark plug is connected to the end of each secondary wiring. A powerful voltage, generated in the secondary wiring, is applied directly to each spark plug. The sparks of the spark plugs pass from the center electrode to the ground electrodes.</ptxt>
<ptxt>The ECM determines the ignition timing and transmits the ignition (IGT) signals to each cylinder. Using the IGT signal, the ECM turns the power transistor inside the igniter on and off. The power transistor, in turn, switches on and off the current to the primary coil. When the current to the primary coil is cut off, a powerful voltage is generated in the secondary coil. This voltage is applied to the spark plugs, causing them to spark inside the cylinders. As the ECM cuts the current to the primary coil, the igniter sends back an ignition confirmation (IGF) signal to the ECM for each cylinder ignition.</ptxt>
<figure>
<graphic graphicname="A147863E21" width="7.106578999in" height="8.799559038in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No. </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0351</ptxt>
<ptxt>P0352</ptxt>
<ptxt>P0353</ptxt>
<ptxt>P0354</ptxt>
<ptxt>P0355</ptxt>
<ptxt>P0356</ptxt>
<ptxt>P0357</ptxt>
<ptxt>P0358</ptxt>
</entry>
<entry valign="middle">
<ptxt>No IGF signal is sent to the ECM while the engine is running (ECM does not receive any IGF signals despite the ECM sending an IGT signal to the igniter) (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition system</ptxt>
</item>
<item>
<ptxt>Open or short in IGF1, IGT2 or IGT circuit (1 to 8) between ignition coil and ECM</ptxt>
</item>
<item>
<ptxt>No. 1 to No. 8 ignition coil assemblies</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>Reference: Inspection using an oscilloscope.</ptxt>
</item>
<item>
<figure>
<graphic graphicname="A093278E24" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C46-40 (IGT1) - C46-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-33 (IGT2) - C46-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-37 (IGT3) - C46-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-34 (IGT4) - C46-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-35 (IGT5) - C46-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-36 (IGT6) - C46-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-38 (IGT7) - C46-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-39 (IGT8) - C46-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-104 (IGF1) - C46-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C46-105 (IGF2) - C46-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C45-40 (IGT1) - C45-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-33 (IGT2) - C45-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-37 (IGT3) - C45-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-34 (IGT4) - C45-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-35 (IGT5) - C45-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-36 (IGT6) - C45-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-38 (IGT7) - C45-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-39 (IGT8) - C45-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-104 (IGF1) - C45-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-105 (IGF2) - C45-81 (E1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 20 msec./DIV.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>While cranking or idling the engine, check the waveform between terminals IGT (1 to 8) and E1, and IGF1 or IGF2 and E1 of the ECM connector.</ptxt>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000XH40VYX_02" type-id="64" category="03" proc-id="RM22W0E___00001D600001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A115898E15" width="7.106578999in" height="5.787629434in"/>
</figure>
<ptxt>If the ECM does not receive any IGF signals despite transmitting the IGT signal, it interprets this as a fault in the igniter and stores a DTC.</ptxt>
<ptxt>If the malfunction is not repaired successfully, a DTC is stored 1 second after the engine is next started.</ptxt>
</content5>
</subpara>
<subpara id="RM000000XH40VYX_10" type-id="73" category="03" proc-id="RM22W0E___00001DC00001">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<list1 type="ordered">
<item>
<ptxt>Start the engine and run it at idle for 10 seconds or more.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000XH40VYX_07" type-id="32" category="03" proc-id="RM22W0E___00001D700001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0300 (See page <xref label="Seep01" href="RM000000XH30Y4X_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000XH40VYX_08" type-id="51" category="05" proc-id="RM22W0E___00001D800001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XH40VYX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XH40VYX_09_0001" proc-id="RM22W0E___00001D900001">
<testtitle>INSPECT IGNITION COIL ASSEMBLY (POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ignition coil connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C4-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C134-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C130-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C13-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C8-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C133-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C131-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C53-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<figure>
<graphic graphicname="A157860E59" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C4-1 (+B) - C4-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C134-1 (+B) - C134-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C130-1 (+B) - C130-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C13-1 (+B) - C13-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C8-1 (+B) - C8-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C133-1 (+B) - C133-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C131-1 (+B) - C131-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C53-1 (+B) - C53-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Ignition Coil Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XH40VYX_09_0002" fin="false">OK</down>
<right ref="RM000000XH40VYX_09_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XH40VYX_09_0002" proc-id="RM22W0E___00001DA00001">
<testtitle>CHECK HARNESS AND CONNECTOR (IGNITION COIL ASSEMBLY - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ignition coil connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" align="center" colwidth="1.77in"/>
<colspec colname="COL2" align="center" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C4-2 (IGF1) - C46-104 (IGF1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C134-2 (IGF2) - C46-105 (IGF2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C130-2 (IGF2) - C46-105 (IGF2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C13-2 (IGF1) - C46-104 (IGF1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C8-2 (IGF2) - C46-105 (IGF2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C133-2 (IGF1) - C46-104 (IGF1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C131-2 (IGF1) - C46-104 (IGF1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C53-2 (IGF2) - C46-105 (IGF2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-3 (IGT1) - C46-40 (IGT1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C134-3 (IGT2) - C46-33 (IGT2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C130-3 (IGT3) - C46-37 (IGT3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C13-3 (IGT4) - C46-34 (IGT4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C8-3 (IGT5) - C46-35 (IGT5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C133-3 (IGT6) - C46-36 (IGT6)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C131-3 (IGT7) - C46-38 (IGT7)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C53-3 (IGT8) - C46-39 (IGT8)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-2 (IGF1) or C46-104 (IGF1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C134-2 (IGF2) or C46-105 (IGF2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C130-2 (IGF2) or C46-105 (IGF2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C13-2 (IGF1) or C46-104 (IGF1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C8-2 (IGF2) or C46-105 (IGF2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C133-2 (IGF1) or C46-104 (IGF1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C131-2 (IGF1) or C46-104 (IGF1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C53-2 (IGF2) or C46-105 (IGF2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-3 (IGT1) or C46-40 (IGT1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C134-3 (IGT2) or C46-33 (IGT2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C130-3 (IGT3) or C46-37 (IGT3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C13-3 (IGT4) or C46-34 (IGT4) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C8-3 (IGT5) or C46-35 (IGT5) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C133-3 (IGT6) or C46-36 (IGT6) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C131-3 (IGT7) or C46-38 (IGT7) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C53-3 (IGT8) or C46-39 (IGT8) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" align="center" colwidth="1.77in"/>
<colspec colname="COL2" align="center" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C4-2 (IGF1) - C45-104 (IGF1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C134-2 (IGF2) - C45-105 (IGF2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C130-2 (IGF2) - C45-105 (IGF2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C13-2 (IGF1) - C45-104 (IGF1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C8-2 (IGF2) - C45-105 (IGF2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C133-2 (IGF1) - C45-104 (IGF1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C131-2 (IGF1) - C45-104 (IGF1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C53-2 (IGF2) - C45-105 (IGF2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-3 (IGT1) - C45-40 (IGT1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C134-3 (IGT2) - C45-33 (IGT2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C130-3 (IGT3) - C45-37 (IGT3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C13-3 (IGT4) - C45-34 (IGT4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C8-3 (IGT5) - C45-35 (IGT5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C133-3 (IGT6) - C45-36 (IGT6)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C131-3 (IGT7) - C45-38 (IGT7)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C53-3 (IGT8) - C45-39 (IGT8)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-2 (IGF1) or C45-104 (IGF1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C134-2 (IGF2) or C45-105 (IGF2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C130-2 (IGF2) or C45-105 (IGF2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C13-2 (IGF1) or C45-104 (IGF1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C8-2 (IGF2) or C45-105 (IGF2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C133-2 (IGF1) or C45-104 (IGF1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C131-2 (IGF1) or C45-104 (IGF1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C53-2 (IGF2) or C45-105 (IGF2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-3 (IGT1) or C45-40 (IGT1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C134-3 (IGT2) or C45-33 (IGT2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C130-3 (IGT3) or C45-37 (IGT3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C13-3 (IGT4) or C45-34 (IGT4) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C8-3 (IGT5) or C45-35 (IGT5) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C133-3 (IGT6) or C45-36 (IGT6) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C131-3 (IGT7) or C45-38 (IGT7) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C53-3 (IGT8) or C45-39 (IGT8) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XH40VYX_09_0003" fin="false">OK</down>
<right ref="RM000000XH40VYX_09_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XH40VYX_09_0003" proc-id="RM22W0E___00001DB00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0351, P0352, P0353, P0354, P0355, P0356, P0357 OR P0358)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK187X"/>).</ptxt>
</test1>
<test1>
<ptxt>Shuffle the arrangement of the ignition coil assemblies (among the No. 1 to No. 8 cylinders).</ptxt>
<atten3>
<ptxt>Do not shuffle the connectors.</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Drive the vehicle in accordance with the driving pattern described in Confirmation Driving Pattern.</ptxt>
</test1>
<test1>
<ptxt>Check the DTCs output on the GTS.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Same DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Different ignition coil DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XH40VYX_09_0007" fin="true">A</down>
<right ref="RM000000XH40VYX_09_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XH40VYX_09_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XH40VYX_09_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XH40VYX_09_0006">
<testtitle>REPLACE IGNITION COIL ASSEMBLY<xref label="Seep01" href="RM000002I2Y02QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XH40VYX_09_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>