<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000YTB0HQX" category="C" type-id="305UJ" name-id="ES11IB-002" from="201301" to="201308">
<dtccode>P0365</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit (Bank 1)</dtcname>
<dtccode>P0367</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit Low Input (Bank 1)</dtcname>
<dtccode>P0368</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit High Input (Bank 1)</dtcname>
<dtccode>P0390</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit (Bank 2)</dtcname>
<dtccode>P0392</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit Low Input (Bank 2)</dtcname>
<dtccode>P0393</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit High Input (Bank 2)</dtcname>
<subpara id="RM000000YTB0HQX_01" type-id="60" category="03" proc-id="RM22W0E___00002E500000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The exhaust camshaft Variable Valve Timing (VVT) sensor (EV1, 2 signal) consists of a magnet and MRE (Magnetic Resistance Element).</ptxt>
<ptxt>The exhaust camshaft has a sensor plate with 3 teeth on its outer circumference. When the exhaust camshaft rotates, changes occur in the air gaps between the 3 teeth and MRE, which affects the magnet. As a result, the resistance of the MRE material fluctuates. The VVT sensor converts the exhaust camshaft rotation data into pulse signals, uses the pulse signals to determine the camshaft angle, and sends it to the ECM.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0365</ptxt>
<ptxt>P0390</ptxt>
</entry>
<entry valign="middle">
<ptxt>When either condition below is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The input voltage to the ECM remains at below 0.3 V or higher than 4.7 V for more than 4 seconds when 2 or more seconds have elapsed after turning the engine switch on (IG) (1 trip detection logic).</ptxt>
</item>
<item>
<ptxt>No VVT sensor signal is sent to the ECM during cranking (1 trip detection logic).</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in VVT sensor (for exhaust side) circuit</ptxt>
</item>
<item>
<ptxt>VVT sensor (for exhaust side)</ptxt>
</item>
<item>
<ptxt>Exhaust camshaft</ptxt>
</item>
<item>
<ptxt>Jumped tooth of timing chain</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0367</ptxt>
<ptxt>P0392</ptxt>
</entry>
<entry valign="middle">
<ptxt>The output voltage of the VVT sensor is below 0.3 V for 4 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<title/>
<item>
<ptxt>Open or short in VVT sensor (for exhaust side) circuit</ptxt>
</item>
<item>
<ptxt>VVT sensor (for exhaust side)</ptxt>
</item>
<item>
<ptxt>Exhaust camshaft</ptxt>
</item>
<item>
<ptxt>Jumped tooth of timing chain</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0368</ptxt>
<ptxt>P0393</ptxt>
</entry>
<entry valign="middle">
<ptxt>The output voltage of the VVT sensor is higher than 4.7 V for 4 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<title/>
<item>
<ptxt>Open or short in VVT sensor (for exhaust side) circuit</ptxt>
</item>
<item>
<ptxt>VVT sensor (for exhaust side)</ptxt>
</item>
<item>
<ptxt>Exhaust camshaft</ptxt>
</item>
<item>
<ptxt>Jumped tooth of timing chain</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>Reference: Inspection using an oscilloscope.</ptxt>
<figure>
<graphic graphicname="A149747E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard</title>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>C45-110 (NE+) - C45-111 (NE-)</ptxt>
</entry>
<entry>
<ptxt>5 V/DIV. </ptxt>
<ptxt>20 msec./DIV.</ptxt>
</entry>
<entry>
<ptxt>Cranking or idling</ptxt>
</entry>
<entry>
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C45-69 (EV1+) - C45-68 (EV1-)</ptxt>
</entry>
<entry>
<ptxt>5 V/DIV. </ptxt>
<ptxt>20 msec./DIV.</ptxt>
</entry>
<entry>
<ptxt>Cranking or idling</ptxt>
</entry>
<entry>
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C45-64 (EV2+) - C45-65 (EV2-)</ptxt>
</entry>
<entry>
<ptxt>5 V/DIV. </ptxt>
<ptxt>20 msec./DIV.</ptxt>
</entry>
<entry>
<ptxt>Cranking or idling</ptxt>
</entry>
<entry>
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>EV1+ and EV2+ stand for the VVT sensor (for exhaust side) signal, and NE+ stands for the CKP sensor signal.</ptxt>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000YTB0HQX_02" type-id="64" category="03" proc-id="RM22W0E___00002E600000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If no signal is transmitted by the VVT sensor despite the engine revolving, or the rotations of the camshaft and the crankshaft are not synchronized, the ECM interprets this as a malfunction of the sensor.</ptxt>
</content5>
</subpara>
<subpara id="RM000000YTB0HQX_07" type-id="32" category="03" proc-id="RM22W0E___00002E700000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A179606E08" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000YTB0HQX_08" type-id="51" category="05" proc-id="RM22W0E___00002E800000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000YTB0HQX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YTB0HQX_09_0002" proc-id="RM22W0E___00002E900000">
<testtitle>CHECK HARNESS AND CONNECTOR (SENSOR POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C121 or C122 VVT sensor (for exhaust side) connector.</ptxt>
<figure>
<graphic graphicname="A241171E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>C121-3 (VC2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>C122-3 (VC2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HQX_09_0003" fin="false">OK</down>
<right ref="RM000000YTB0HQX_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0003" proc-id="RM22W0E___00002EA00000">
<testtitle>CHECK HARNESS AND CONNECTOR (VVT SENSOR (for Exhaust Side) - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C121 or C122 VVT (for exhaust side) sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C45 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.89in"/>
<colspec colname="COLSPEC1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C121-1 (EX+) - C45-69 (EV1+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C121-2 (EX-) - C45-68 (EV1-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-1 (EX+) - C45-64 (EV2+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-2 (EX-) - C45-65 (EV2-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C121-1 (EX+) or C45-69 (EV1+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C121-2 (EX-) or C45-68 (EV1-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-1 (EX+) or C45-64 (EV2+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-2 (EX-) or C45-65 (EV2-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HQX_09_0004" fin="false">OK</down>
<right ref="RM000000YTB0HQX_09_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0004" proc-id="RM22W0E___00002EB00000">
<testtitle>CHECK SENSOR INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the VVT sensor (for exhaust side) installation.</ptxt>
<figure>
<graphic graphicname="BR03795E70" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>Sensor is installed correctly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HQX_09_0005" fin="false">OK</down>
<right ref="RM000000YTB0HQX_09_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0005" proc-id="RM22W0E___00002EC00000">
<testtitle>CHECK EXHAUST CAMSHAFT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the teeth of the camshaft.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Teeth do not have any cracks or deformation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HQX_09_0006" fin="false">OK</down>
<right ref="RM000000YTB0HQX_09_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0006" proc-id="RM22W0E___00002ED00000">
<testtitle>REPLACE VVT SENSOR (for Exhaust Side)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the VVT sensor (for exhaust side) (See page <xref label="Seep01" href="RM0000031Y701QX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HQX_09_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0012" proc-id="RM22W0E___00002EE00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK14HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="4.25in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0365, P0367, P0368, P0390, P0392 or P0393</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the engine does not start, replace the ECM.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HQX_09_0014" fin="true">A</down>
<right ref="RM000000YTB0HQX_09_0013" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0009">
<testtitle>SECURELY REINSTALL SENSOR<xref label="Seep01" href="RM0000031Y501QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0010">
<testtitle>REPLACE EXHAUST CAMSHAFT<xref label="Seep01" href="RM000002OOY01OX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0013">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTB0HQX_09_0014">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>