<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TW_T00MZ" variety="T00MZ">
<name>AIRBAG SYSTEM</name>
<para id="RM000002C9O054X" category="C" type-id="8020H" name-id="RSGTN-01" from="201308">
<dtccode>B1610/13</dtccode>
<dtcname>Front Airbag Sensor RH Malfunction</dtcname>
<subpara id="RM000002C9O054X_01" type-id="60" category="03" proc-id="RM22W0E___0000FDJ00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The front airbag sensor RH consists of the diagnostic circuit and the frontal deceleration sensor, etc.</ptxt>
<ptxt>If the center airbag sensor receives signals from the frontal deceleration sensor, it determines whether the SRS should be activated.</ptxt>
<ptxt>DTC B1610/13 is stored when a malfunction is detected in the front airbag sensor RH circuit.</ptxt>
<table frame="all" pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="col1" colwidth="2.34in"/>
<colspec colname="col2" colwidth="2.34in"/>
<colspec colname="col3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1610/13</ptxt>
</entry>
<entry valign="middle">
<ptxt>When one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>A front airbag sensor RH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Front airbag sensor RH</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002C9O054X_02" type-id="32" category="03" proc-id="RM22W0E___0000FDK00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C246840E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002C9O054X_03" type-id="51" category="05" proc-id="RM22W0E___0000FDL00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000002C9O054X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002C9O054X_04_0001" proc-id="RM22W0E___0000FDM00001">
<testtitle>CHECK FRONT AIRBAG SENSOR RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C246887E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Interchange the front airbag sensor RH with LH and connect the connectors to them.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs stored in the memory (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC B1610 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC B1615 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC B1610 and B1615 are not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Codes other than DTC B1610 and B1615 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002C9O054X_04_0004" fin="true">C</down>
<right ref="RM000002C9O054X_04_0002" fin="true">A</right>
<right ref="RM000002C9O054X_04_0003" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002C9O054X_04_0002">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000002Y2N00ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002C9O054X_04_0003">
<testtitle>REPLACE FRONT AIRBAG SENSOR RH<xref label="Seep01" href="RM000002OF001BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002C9O054X_04_0004">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0KMX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>