<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S0011" variety="S0011">
<name>3UR-FE LUBRICATION</name>
<ttl id="12012_S0011_7C3KU_T00DX" variety="T00DX">
<name>OIL PUMP</name>
<para id="RM000002S4X03OX" category="A" type-id="80001" name-id="LU3VZ-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM000002S4X03OX_01" type-id="01" category="01">
<s-1 id="RM000002S4X03OX_01_0058" proc-id="RM22W0E___00004SR00000">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE
</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000028RU03XX"/>)</ptxt>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0169" proc-id="RM22W0E___000070P00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0003" proc-id="RM22W0E___000070L00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0163" proc-id="RM22W0E___000049F00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 7 clips and radiator support seal.</ptxt>
<figure>
<graphic graphicname="B180890E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0160" proc-id="RM22W0E___000049G00000">
<ptxt>REMOVE RADIATOR GRILLE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the radiator grille assembly.</ptxt>
<figure>
<graphic graphicname="B302174E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clips and 8 claws, and remove the radiator grille assembly.</ptxt>
</s2>
<s2>
<ptxt>w/ Wide View Front Monitor System:</ptxt>
<s3>
<ptxt>Disconnect the connector.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0161" proc-id="RM22W0E___00004SS00000">
<ptxt>REMOVE FRONT BUMPER COVER
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>For the front bumper cover with garnish, use the procedure described below.</ptxt>
</atten4>
<s2>
<ptxt>Put protective tape around the front bumper cover.</ptxt>
<figure>
<graphic graphicname="B302149E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a T30 "TORX" socket, remove the 6 screws.</ptxt>
<figure>
<graphic graphicname="B313279E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>LH Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>RH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 3 clips, 4 screws and 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 10 claws and remove the front bumper cover.</ptxt>
<figure>
<graphic graphicname="B302150" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System or w/ Fog Light:</ptxt>
<ptxt>Disconnect the No. 4 engine room wire connector and remove the front bumper cover.</ptxt>
</s2>
<s2>
<ptxt>w/ Headlight Cleaner System:</ptxt>
<figure>
<graphic graphicname="B181927" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the headlight cleaner hose and remove the front bumper cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0150" proc-id="RM22W0E___00004ST00000">
<ptxt>REMOVE TRANSMISSION OIL COOLER AIR DUCT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and oil cooler air duct.</ptxt>
<figure>
<graphic graphicname="A271852" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0152" proc-id="RM22W0E___00004SU00000">
<ptxt>DISCONNECT RADIATOR SIDE DEFLECTOR LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, remove the 4 clips and disconnect the side deflector.</ptxt>
<figure>
<graphic graphicname="A271854" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0145" proc-id="RM22W0E___000070N00000">
<ptxt>REMOVE COWL TOP VENTILATOR LOUVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000002M66014X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0165" proc-id="RM22W0E___00004U000000">
<ptxt>REMOVE FRONT FENDER APRON SEAL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, remove the 3 clips and fender apron seal.</ptxt>
<figure>
<graphic graphicname="A177004" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0166" proc-id="RM22W0E___00001ZM00000">
<ptxt>REMOVE FRONT FENDER APRON SEAL FRONT RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, remove the 3 clips and fender apron seal.</ptxt>
<figure>
<graphic graphicname="A177003" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0157" proc-id="RM22W0E___000011Z00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0158" proc-id="RM22W0E___000011Y00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0154" proc-id="RM22W0E___00001X600000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A238241" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 10 bolts and No. 1 engine under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0146" proc-id="RM22W0E___00004SV00000">
<ptxt>REMOVE NO. 2 ENGINE UNDER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A178462" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and No. 2 engine under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0101" proc-id="RM22W0E___00004SW00000">
<ptxt>DRAIN ENGINE OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the oil filler cap.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A174194" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and No. 2 engine under cover seal.</ptxt>
</s2>
<s2>
<ptxt>Remove the oil pan drain plug and gasket, and drain the engine oil into a container.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket and the oil pan drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0066" proc-id="RM22W0E___00001X300000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the radiator cap. Then drain the coolant from the radiator.</ptxt>
</s2>
<s2>
<ptxt>Loosen the 2 cylinder block drain cock plugs. Then drain the coolant from the engine.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 2 cylinder block drain cock plugs.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A174123E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0156" proc-id="RM22W0E___00001X400000">
<ptxt>REMOVE V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A174196E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Raise the front of the V-bank cover to detach the 3 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0155" proc-id="RM22W0E___00001X500000">
<ptxt>REMOVE AIR CLEANER HOSE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A243375" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the vacuum hose and No. 2 ventilation hose.</ptxt>
</s2>
<s2>
<ptxt>Loosen the 2 hose clamps.</ptxt>
</s2>
<s2>
<ptxt>Remove the air cleaner hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0134" proc-id="RM22W0E___00001ZK00000">
<ptxt>REMOVE AIR CLEANER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A174195" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 bolts and air cleaner.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0021" proc-id="RM22W0E___000070M00000">
<ptxt>REMOVE RADIATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000002BGI038X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0023" proc-id="RM22W0E___00004SX00000">
<ptxt>DISCONNECT ENGINE WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Engine Room LH Side:</ptxt>
<s3>
<ptxt>Remove the engine room relay block cover.</ptxt>
<figure>
<graphic graphicname="A237015" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the 2 connectors and 2 clips from the engine room junction block.</ptxt>
</s3>
<s3>
<figure>
<graphic graphicname="A178453" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the injector connector.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 4 ignition coil connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 2 VVT sensor connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 4 clamps.</ptxt>
</s3>
<s3>
<ptxt>Remove the 2 bolts and ground wire.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the noise filter connector.</ptxt>
</s3>
<s3>
<figure>
<graphic graphicname="A174202" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Disconnect the engine coolant temperature sensor connector.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 2 camshaft timing oil control valve connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the camshaft position sensor connector.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 3 clamps.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the cooler compressor connector.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="A237034" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Engine Room RH Side:</ptxt>
<s3>
<ptxt>Disconnect the 2 camshaft timing oil control valve connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 4 ignition coil connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the injector connector.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 2 VVT sensor connectors.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the noise filter connector.</ptxt>
</s3>
<s3>
<ptxt>Remove the 2 bolts and ground wire.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the throttle position sensor and throttle control motor connector.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the 5 clamps.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="A174204" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the 2 clamps and power steering oil pressure switch connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0127" proc-id="RM22W0E___00004SY00000">
<ptxt>DISCONNECT WATER PIPE AND HOSE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A239168" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the 3 hoses.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the water pipe and hose from the cylinder head cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0076" proc-id="RM22W0E___00004SZ00000">
<ptxt>DISCONNECT COOLER COMPRESSOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A178491" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 bolts, nut and stud bolt, and disconnect the cooler compressor. </ptxt>
<atten4>
<ptxt>It is not necessary to completely remove the compressor. With the hoses connected to the compressor, hang the compressor on the vehicle body with a rope.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0143" proc-id="RM22W0E___00004T000000">
<ptxt>DISCONNECT NO. 2 FUEL TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A165142E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Remove the 2 bolts and disconnect the fuel tube.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>LH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>RH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0104" proc-id="RM22W0E___00004T100000">
<ptxt>REMOVE OIL FILTER ELEMENT
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A094910E10" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Connect a hose with an inside diameter of 15 mm (0.591 in.) to the pipe.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hose</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<figure>
<graphic graphicname="A207196" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the oil filter drain plug.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A237032E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Install the pipe to the oil filter cap.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hose</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>If the O-ring is removed with the drain plug, install the O-ring together with the pipe.</ptxt>
</atten3>
<atten4>
<ptxt>Use a container to catch the draining oil.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A207198" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Check that oil is drained from the oil filter. Then disconnect the pipe and remove the O-ring as shown in the illustration.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A207199E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using SST, remove the oil filter cap.</ptxt>
<sst>
<sstitem>
<s-number>09228-06501</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Filter Bracket Clip</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not remove the oil filter bracket clip.</ptxt>
</atten3>
</s2>
<s2>
<figure>
<graphic graphicname="A209947E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the oil filter element and O-ring from the oil filter cap.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Filter Element</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be sure to remove the cap O-ring by hand, without using any tools, to prevent damage to the cap O-ring groove.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0126" proc-id="RM22W0E___00004T200000">
<ptxt>REMOVE ENGINE OIL LEVEL DIPSTICK GUIDE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A174205" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the dipstick.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and dipstick guide.</ptxt>
</s2>
<s2>
<ptxt>Remove the O-ring from the dipstick guide.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0130" proc-id="RM22W0E___00004T300000">
<ptxt>REMOVE OIL PRESSURE SENDER GAUGE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A163657" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the sender gauge connector.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A162467" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the oil pressure sender gauge.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0129" proc-id="RM22W0E___00004T400000">
<ptxt>REMOVE NO. 2 WATER BY-PASS PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A165547" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Remove the 3 bolts.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 4 hoses and remove the water by-pass pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0142" proc-id="RM22W0E___00004T500000">
<ptxt>REMOVE NO. 1 OIL COOLER BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A163672" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 nuts and bracket.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the ground wire from the cylinder block.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0120" proc-id="RM22W0E___00004UW00000">
<ptxt>REMOVE OIL FILTER BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A162469" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts, 2 nuts and filter bracket.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A162470" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 O-rings.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0164" proc-id="RM22W0E___00004TX00000">
<ptxt>REMOVE INTAKE MANIFOLD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the ventilation hose from the ventilation pipe of the cylinder head cover LH and RH.</ptxt>
<figure>
<graphic graphicname="A162091E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the 2 water by-pass hoses.</ptxt>
<figure>
<graphic graphicname="A162087" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the throttle body connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 1 ventilation hose.</ptxt>
<figure>
<graphic graphicname="A184056" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the purge VSV connector.</ptxt>
<figure>
<graphic graphicname="A162082" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the purge line hose from the purge VSV.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the vacuum switching valve connector (for ACIS).</ptxt>
</s2>
<s2>
<ptxt>Remove the No. 1 engine cover sub-assembly.</ptxt>
<figure>
<graphic graphicname="A162089" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 3 engine cover.</ptxt>
<figure>
<graphic graphicname="A238871" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the 3 wire clamps from the 3 wire brackets.</ptxt>
<figure>
<graphic graphicname="A238834" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and wire bracket from the intake manifold.</ptxt>
<figure>
<graphic graphicname="A238832" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts, 8 bolts, intake manifold and 2 gaskets.</ptxt>
<figure>
<graphic graphicname="A184053" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0118" proc-id="RM22W0E___00004T600000">
<ptxt>DISCONNECT VANE PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A163289" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and disconnect the vane pump.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0167" proc-id="RM22W0E___00004TY00000">
<ptxt>DISCONNECT OIL COOLER PIPE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A174185" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and disconnect the oil cooler pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0168" proc-id="RM22W0E___00004TZ00000">
<ptxt>REMOVE GENERATOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the generator connector.</ptxt>
<figure>
<graphic graphicname="A177892" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Open the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and disconnect the generator wire.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the wire harness bracket from the generator.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts, nut and generator.</ptxt>
<figure>
<graphic graphicname="A177893" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the stud bolt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0121" proc-id="RM22W0E___00004T700000">
<ptxt>REMOVE NO. 1 WATER BY-PASS HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A237017" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the No. 1 water by-pass hose by disconnecting the hose from the water inlet housing and front water by-pass joint.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0122" proc-id="RM22W0E___00004T800000">
<ptxt>REMOVE WATER BY-PASS PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A162551" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the 2 hoses.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and water by-pass pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0103" proc-id="RM22W0E___00004T900000">
<ptxt>REMOVE FRONT WATER BY-PASS JOINT</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A162552" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the No. 2 water by-pass hose from the water by-pass joint.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 nuts, water by-pass joint and 2 gaskets.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0138" proc-id="RM22W0E___00001YR00000">
<ptxt>REMOVE NO. 2 ENGINE COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A237721" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0139" proc-id="RM22W0E___00001YS00000">
<ptxt>REMOVE NO. 1 ENGINE COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A237722" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0042" proc-id="RM22W0E___00004TA00000">
<ptxt>REMOVE WATER INLET HOUSING</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A162554" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 bolts, water inlet housing and gasket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0086" proc-id="RM22W0E___00004TB00000">
<ptxt>REMOVE WATER PUMP PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A162556E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using SST, hold the water pump pulley.</ptxt>
<sst>
<sstitem>
<s-number>09960-10010</s-number>
<s-subnumber>09962-01000</s-subnumber>
<s-subnumber>09963-01000</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Remove the 4 bolts and water pump pulley.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0088" proc-id="RM22W0E___00004TC00000">
<ptxt>REMOVE NO. 1 IDLER PULLEY SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A162557" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and idler pulley.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0124" proc-id="RM22W0E___00004TD00000">
<ptxt>REMOVE FLUID COUPLING BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A162558" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 4 bolts and fluid coupling bracket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0089" proc-id="RM22W0E___00004TE00000">
<ptxt>REMOVE V-RIBBED BELT TENSIONER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the standard bolt, 6 mm hexagon wrench bolt and belt tensioner.</ptxt>
<figure>
<graphic graphicname="A162527" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0105" proc-id="RM22W0E___00004TF00000">
<ptxt>REMOVE IGNITION COIL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 8 bolts and 8 ignition coils.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0055" proc-id="RM22W0E___00004TG00000">
<ptxt>REMOVE CYLINDER HEAD COVER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 14 bolts, seal washer, cylinder head cover and gasket.</ptxt>
<figure>
<graphic graphicname="A162523" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure the removed parts are returned to the same places they were removed from.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the 5 gaskets from the camshaft bearing caps (No. 2, No. 3).</ptxt>
<figure>
<graphic graphicname="A162524" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0056" proc-id="RM22W0E___00004TH00000">
<ptxt>REMOVE CYLINDER HEAD COVER SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A163381" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and noise filter.</ptxt>
</s2>
<s2>
<ptxt>Remove the 14 bolts, seal washer, cylinder head cover and gasket.</ptxt>
<figure>
<graphic graphicname="A162525" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure the removed parts are returned to the same places they were removed from.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the 5 gaskets from the camshaft bearing caps (No. 1, No. 3).</ptxt>
<figure>
<graphic graphicname="A162526" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0162" proc-id="RM22W0E___00004TI00000">
<ptxt>REMOVE SPARK PLUG TUBE GASKET
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A164315E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Bend the 4 ventilation baffle plate claws on the cylinder head cover to an angle of 90° or more.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, pry out the gaskets.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tape</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pry</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cylinder head cover.</ptxt>
</item>
<item>
<ptxt>Be careful not to damage the gasket when removing it, as the removed gasket needs to be used when installing a new one.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002S4X03OX_01_0053" proc-id="RM22W0E___00004TJ00000">
<ptxt>REMOVE CRANKSHAFT PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A164228E04" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Using SST, loosen the crankshaft pulley set bolt until 2 or 3 threads are engaged.</ptxt>
<sst>
<sstitem>
<s-number>09213-70011</s-number>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using the pulley set bolt and SST, remove the crankshaft pulley.</ptxt>
<figure>
<graphic graphicname="A164229E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-50013</s-number>
<s-subnumber>09951-05010</s-subnumber>
<s-subnumber>09952-05010</s-subnumber>
<s-subnumber>09953-05010</s-subnumber>
<s-subnumber>09954-05011</s-subnumber>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0135" proc-id="RM22W0E___00004TK00000">
<ptxt>DISCONNECT WIRE HARNESS CLAMP BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A163733" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and disconnect the bracket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0159" proc-id="RM22W0E___000070O00000">
<ptxt>REMOVE CRANKSHAFT TIMING GEAR KEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, remove the timing gear key from the crankshaft.</ptxt>
<figure>
<graphic graphicname="A163993" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0057" proc-id="RM22W0E___00004TM00000">
<ptxt>REMOVE TIMING CHAIN COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 28 bolts and nut shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A237019" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Bolt</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Nut</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the timing chain cover by prying between the timing chain cover and cylinder head or cylinder block with a screwdriver as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A162530" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<ptxt>Be careful not to damage the contact surfaces of the cylinder head, cylinder block and chain cover.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the oil pump gasket from the cylinder block.</ptxt>
<figure>
<graphic graphicname="A162531" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<figure>
<graphic graphicname="A164415" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the O-ring from the oil pan.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S4X03OX_01_0093" proc-id="RM22W0E___00004TN00000">
<ptxt>REMOVE WATER INLET PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the water inlet pipe.</ptxt>
<figure>
<graphic graphicname="A162532" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 O-rings from the water inlet pipe.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>