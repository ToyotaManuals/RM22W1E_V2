<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000001EA902QX" category="C" type-id="804QW" name-id="BCGGM-01" from="201308">
<dtccode>C1252</dtccode>
<dtcname>Brake Booster Pump Motor on Time Abnormally Long</dtcname>
<subpara id="RM000001EA902QX_01" type-id="60" category="03" proc-id="RM22W0E___0000AR700001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The motor relay (semiconductor relay) is built into the master cylinder solenoid and drives the pump motor based on a signal from the skid control ECU (master cylinder solenoid).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1252</ptxt>
</entry>
<entry valign="middle">
<ptxt>The motor operates for 3 minutes or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Brake booster with accumulator pump assembly</ptxt>
</item>
<item>
<ptxt>Pump motor wire harness condition</ptxt>
</item>
<item>
<ptxt>Skid control ECU (Master cylinder solenoid)</ptxt>
</item>
<item>
<ptxt>Hydraulic brake booster assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The pump motor continues operation for the first 3 minutes, and then starts and stops repeatedly.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001EA902QX_03" type-id="51" category="05" proc-id="RM22W0E___0000AR800001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration (See page <xref label="Seep01" href="RM000001DWZ01OX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>When DTC C1253, C1254, C1256 or C1452 is output together with DTC C1252, inspect and repair the trouble area indicated by DTC C1253, C1254, C1256 or C1452 first.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001EA902QX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001EA902QX_04_0001" proc-id="RM22W0E___0000AR900001">
<testtitle>CHECK PUMP MOTOR OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal more than 40 times.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON. </ptxt>
</test1>
<test1>
<ptxt>Check how the pump motor operates.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.35in"/>
<colspec colname="COL2" colwidth="1.73in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Pump motor does not operate </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pump motor operates continuously and does not stop</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pump motor operates intermittently </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pump motor operates, and then stops</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<atten4>
<ptxt>Refer to the hydraulic brake booster assembly installation procedures:</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD (See page <xref label="Seep01" href="RM00000171T02MX"/>)</ptxt>
</item>
<item>
<ptxt>for RHD (See page <xref label="Seep02" href="RM00000171T02NX"/>)</ptxt>
</item>
</list1>
</atten4>
</content6>
<res>
<down ref="RM000001EA902QX_04_0016" fin="false">A</down>
<right ref="RM000001EA902QX_04_0010" fin="true">B</right>
<right ref="RM000001EA902QX_04_0004" fin="false">C</right>
<right ref="RM000001EA902QX_04_0005" fin="false">D</right>
</res>
</testgrp>
<testgrp id="RM000001EA902QX_04_0016" proc-id="RM22W0E___0000AQZ00001">
<testtitle>CHECK CONNECTION OF PUMP MOTOR WIRE HARNESS
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the hydraulic brake booster assembly (for LHD: See page <xref label="Seep03" href="RM00000171T02MX"/>, for RHD: See page <xref label="Seep04" href="RM00000171T02NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the tightening torque of the 2 screws which secure the wire harness connecting the master cylinder solenoid and brake booster with accumulator pump assembly (for LHD: See page <xref label="Seep01" href="RM00000171V01UX"/>, for RHD: See page <xref label="Seep02" href="RM00000171V01VX"/>).</ptxt>
</test1>
<spec>
<title>OK</title>
<specitem>
<ptxt>The harness is tightened to the specified torque.</ptxt>
</specitem>
</spec>
</content6><res>
<down ref="RM000001EA902QX_04_0017" fin="false">OK</down>
<right ref="RM000001EA902QX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001EA902QX_04_0017" proc-id="RM22W0E___0000AR000001">
<testtitle>CHECK RESISTANCE OF PUMP MOTOR WIRE HARNESS
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using a screwdriver, remove the 2 screws and pull out the wire harness from the master cylinder solenoid.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C173363E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.88in"/>
<colspec colname="COL2" colwidth="1.26in"/>
<colspec colname="COL3" colwidth="0.99in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Red wire terminal - Black wire terminal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 2 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pump motor wire harness</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Red wire</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Black wire</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<atten4>
<list1 type="unordered">
<title>Refer to the brake booster with accumulator pump assembly installation procedures:</title>
<item>
<ptxt>for LHD (See page <xref label="Seep01" href="RM00000171U01SX"/>)</ptxt>
</item>
<item>
<ptxt>for RHD (See page <xref label="Seep02" href="RM00000171U01TX"/>)</ptxt>
</item>
</list1>
</atten4>
</content6><res>
<down ref="RM000001EA902QX_04_0004" fin="false">OK</down>
<right ref="RM000001EA902QX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001EA902QX_04_0004" proc-id="RM22W0E___0000ARA00001">
<testtitle>READ VALUE USING GTS (ACCUMULATOR SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3. </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Accumulator Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accumulator pressure sensor reading/ Min.: 0.00 V, Max.: 5.00 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.58 to 5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>If the value is constant regardless of the pump operation, an accumulator pressure sensor malfunction is suspected.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check the accumulator output value.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.25in"/>
<colspec colname="COL2" colwidth="1.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Output value is within "Normal Condition" range </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Output value is out of "Normal Condition" range </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Output value is constant regardless of pump operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Refer to the hydraulic brake booster assembly installation procedures:</ptxt>
<ptxt>for LHD (See page <xref label="Seep03" href="RM00000171T02MX"/>)</ptxt>
<ptxt>for RHD (See page <xref label="Seep04" href="RM00000171T02NX"/>)</ptxt>
</item>
<item>
<ptxt>Refer to the master cylinder solenoid installation procedures:</ptxt>
<ptxt>for LHD (See page <xref label="Seep01" href="RM00000171U01SX"/>)</ptxt>
<ptxt>for RHD (See page <xref label="Seep02" href="RM00000171U01TX"/>)</ptxt>
</item>
</list1>
</atten4>
</content6>
<res>
<down ref="RM000001EA902QX_04_0005" fin="false">A</down>
<right ref="RM000001EA902QX_04_0014" fin="true">B</right>
<right ref="RM000001EA902QX_04_0010" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001EA902QX_04_0005" proc-id="RM22W0E___0000ARB00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00VX"/>). </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off. </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON. </ptxt>
</test1>
<test1>
<ptxt>Wait for more than 5 minutes. </ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00VX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Reinstall the sensors, reconnect the connectors, etc. and restore the previous vehicle conditions before rechecking for DTCs.</ptxt>
</item>
<item>
<ptxt>Refer to the hydraulic brake booster assembly installation procedures:</ptxt>
<ptxt>for LHD (See page <xref label="Seep03" href="RM00000171T02MX"/>)</ptxt>
<ptxt>for RHD (See page <xref label="Seep04" href="RM00000171T02NX"/>)</ptxt>
</item>
<item>
<ptxt>Refer to the brake booster with accumulator pump assembly installation procedures:</ptxt>
<ptxt>for LHD (See page <xref label="Seep05" href="RM00000171U01SX"/>)</ptxt>
<ptxt>for RHD (See page <xref label="Seep06" href="RM00000171U01TX"/>)</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000001EA902QX_04_0010" fin="true">A</down>
<right ref="RM000001EA902QX_04_0013" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000001EA902QX_04_0010">
<testtitle>REPLACE HYDRAULIC BRAKE BOOSTER ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000001EA902QX_04_0008">
<testtitle>RETIGHTEN SCREWS</testtitle>
</testgrp>
<testgrp id="RM000001EA902QX_04_0013">
<testtitle>REPLACE BRAKE BOOSTER WITH ACCUMULATOR PUMP ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000001EA902QX_04_0014">
<testtitle>REPLACE MASTER CYLINDER SOLENOID</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>