<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12021_S001D" variety="S001D">
<name>JF2A TRANSFER / 4WD / AWD</name>
<ttl id="12021_S001D_7C3NM_T00GP" variety="T00GP">
<name>4WD CONTROL ECU</name>
<para id="RM0000030J200ZX" category="A" type-id="80001" name-id="TF1HV-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM0000030J200ZX_01" type-id="01" category="01">
<s-1 id="RM0000030J200ZX_01_0038" proc-id="RM22W0E___0000JZ400000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the negative (-) battery terminal. Therefore, make sure to read the disconnecting the cable from the negative (-) battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000030J200ZX_01_0039" proc-id="RM22W0E___0000JZ500000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000030J200ZX_01_0036" proc-id="RM22W0E___0000BBP00000">
<ptxt>REMOVE FRONT DOOR SCUFF PLATE RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0040" proc-id="RM22W0E___0000BBR00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL FINISH PANEL CUSHION
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180004E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the No. 1 instrument panel finish panel cushion.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the No. 1 instrument panel finish panel cushion.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0031" proc-id="RM22W0E___0000BBS00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180005" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the clip and screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 7 claws and remove the lower instrument panel pad sub-assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0011" proc-id="RM22W0E___000097900000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY (for LHD)</ptxt>
<content1 releasenbr="1">
<ptxt>w/ Floor Under Cover: (See page <xref label="Seep01" href="RM0000039OQ00KX_01_0032"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000030J200ZX_01_0026" proc-id="RM22W0E___0000D7M00000">
<ptxt>REMOVE FRONT PASSENGER SIDE KNEE AIRBAG ASSEMBLY (for LHD)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
<figure>
<graphic graphicname="B189606E01" width="2.775699831in" height="6.791605969in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the front passenger side knee airbag.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0032" proc-id="RM22W0E___0000BB400000">
<ptxt>REMOVE NO. 1 INSTRUMENT CLUSTER FINISH PANEL GARNISH (for RHD)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154744E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 3 claws and remove the No. 1 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0033" proc-id="RM22W0E___0000BB500000">
<ptxt>REMOVE NO. 2 INSTRUMENT CLUSTER FINISH PANEL GARNISH (for RHD)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws and remove the No. 2 instrument cluster finish panel garnish.</ptxt>
<figure>
<graphic graphicname="B291251E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Entry and Start System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0016" proc-id="RM22W0E___000097A00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY (for RHD)</ptxt>
<content1 releasenbr="1">
<ptxt>w/ Floor Under Cover: (See page <xref label="Seep01" href="RM0000039OQ00KX_01_0024"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000030J200ZX_01_0030" proc-id="RM22W0E___0000A9S00000">
<ptxt>REMOVE LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL (for RHD)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180295E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the hole cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 16 claws.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 9 claws.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Detach the 2 claws and remove the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Detach the 2 claws and disconnect the 2 control cables.</ptxt>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the lower No. 1 instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0035" proc-id="RM22W0E___0000ATC00000">
<ptxt>REMOVE DRIVER SIDE KNEE AIRBAG ASSEMBLY (for RHD)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and driver side knee airbag.</ptxt>
<figure>
<graphic graphicname="B189602E01" width="2.775699831in" height="6.791605969in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0025" proc-id="RM22W0E___0000AU200000">
<ptxt>REMOVE COWL SIDE TRIM BOARD RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180022" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the cap nut.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clips and remove the cowl side trim board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0027" proc-id="RM22W0E___0000D7O00000">
<ptxt>REMOVE NO. 3 INSTRUMENT CLUSTER FINISH PANEL GARNISH (for LHD)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180303E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 6 claws and remove the No. 3 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0028" proc-id="RM22W0E___0000D7Q00000">
<ptxt>REMOVE INSTRUMENT PANEL BOX DOOR KNOB (for LHD)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182302" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws and remove the instrument panel box door knob.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0029" proc-id="RM22W0E___0000D7P00000">
<ptxt>REMOVE LOWER NO. 2 INSTRUMENT PANEL FINISH PANEL (for LHD)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180304" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the lower No. 2 instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030J200ZX_01_0007" proc-id="RM22W0E___000097800000">
<ptxt>REMOVE FOUR WHEEL DRIVE CONTROL ECU</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 ECU connectors.</ptxt>
<figure>
<graphic graphicname="C172064" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and ECU.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>