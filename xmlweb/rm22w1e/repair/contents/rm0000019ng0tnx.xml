<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R3_T00K6" variety="T00K6">
<name>AUDIO AND VISUAL SYSTEM (w/ Navigation System)</name>
<para id="RM0000019NG0TNX" category="J" type-id="302YQ" name-id="AVF62-01" from="201308">
<dtccode/>
<dtcname>AVC-LAN Circuit</dtcname>
<subpara id="RM0000019NG0TNX_01" type-id="60" category="03" proc-id="RM22W0E___0000BXA00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Each audio system component connected to the AVC-LAN (communication bus) transfers switch signals using the audio visual communication local area network.</ptxt>
<ptxt>If a short to +B or short to ground occurs in the AVC-LAN, the audio system will not function normally because communication is not possible.</ptxt>
</content5>
</subpara>
<subpara id="RM0000019NG0TNX_04" type-id="32" category="03" proc-id="RM22W0E___0000BXE00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C260263E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000019NG0TNX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000019NG0TNX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000019NG0TNX_03_0001" proc-id="RM22W0E___0000BXB00001">
<testtitle>INSPECT MULTI-MEDIA MODULE RECEIVER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the multi-media module receiver assembly (See page <xref label="Seep01" href="RM000003AHY024X"/>).</ptxt>
<figure>
<graphic graphicname="E238990E12" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>45 (TX3+) - 46 (TX3-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000019NG0TNX_03_0002" fin="false">OK</down>
<right ref="RM0000019NG0TNX_03_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000019NG0TNX_03_0002" proc-id="RM22W0E___0000BXC00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA MODULE RECEIVER ASSEMBLY - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>For details of the connectors, refer to Terminals of ECU (See page <xref label="Seep01" href="RM0000012A70EZX"/>).</ptxt>
</atten4>
<test1>
<ptxt>Referring to the following AVC-LAN wiring diagram, check all AVC-LAN circuits.</ptxt>
<test2>
<ptxt>Disconnect all connectors in all AVC-LAN circuits.</ptxt>
</test2>
<test2>
<ptxt>Check for an open or short in all AVC-LAN circuits.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There is no open or short circuit.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000019NG0TNX_03_0006" fin="false">OK</down>
<right ref="RM0000019NG0TNX_03_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000019NG0TNX_03_0003">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000011BR0O2X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000019NG0TNX_03_0004">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY024X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000019NG0TNX_03_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000019NG0TNX_03_0006" proc-id="RM22W0E___0000BXD00001">
<testtitle>INSPECT MALFUNCTIONING PARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect and reconnect each slave unit one by one until the master unit returns to normal operation.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Check all slave units.</ptxt>
</item>
<item>
<ptxt>When disconnecting a slave unit causes the master unit to return to normal operation, this indicates that the slave unit is malfunctioning. Replace the malfunctioning slave unit.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Master unit returns to normal operation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000019NG0TNX_03_0007" fin="true">OK</down>
<right ref="RM0000019NG0TNX_03_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000019NG0TNX_03_0007">
<testtitle>REPLACE MALFUNCTIONING PARTS</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>