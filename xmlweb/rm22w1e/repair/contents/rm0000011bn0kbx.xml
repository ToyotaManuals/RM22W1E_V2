<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R1_T00K4" variety="T00K4">
<name>REAR SEAT ENTERTAINMENT SYSTEM</name>
<para id="RM0000011BN0KBX" category="D" type-id="3001B" name-id="NS0087-335" from="201301" to="201308">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM0000011BN0KBX_z0" proc-id="RM22W0E___0000BSH00000">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the following procedure to troubleshoot the rear seat entertainment system.</ptxt>
</item>
<item>
<ptxt>*: Use the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<test1>
<ptxt>Inspect the battery voltage.</ptxt>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding to next step.</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK FOR DTC*</testtitle>
<test1>
<ptxt>Refer to DTC Check / Clear (See page <xref label="Seep06" href="RM000003XO002FX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is not output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<atten4>
<ptxt>If more information is needed, perform a recheck using the "System Check Mode" screen after rechecking for DTCs using the intelligent tester (See page <xref label="Seep01" href="RM000003XO002FX"/>).</ptxt>
</atten4>
<results>
<result>B</result>
<action-ci-right>Go to step 7</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CLEAR DTC*</testtitle>
<test1>
<ptxt>Clear the DTCs and finish diagnostic mode.</ptxt>
<atten4>
<ptxt>The current DTCs may not indicate actual malfunctions depending on the vehicle operating conditions.</ptxt>
</atten4>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>RECHECK FOR DTC*</testtitle>
<test1>
<ptxt>Recheck for DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is not output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Even if the malfunction symptom cannot be confirmed, check for DTCs. This is because the system stores history DTCs.</ptxt>
</item>
<item>
<ptxt>If more information is needed, perform a recheck using the "System Check Mode" screen after rechecking for DTCs using the intelligent tester (See page <xref label="Seep05" href="RM000003XO002FX"/>).</ptxt>
</item>
<item>
<ptxt>Check the DTC and inspect the area the code indicates.</ptxt>
</item>
</list1>
</atten4>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to step 7</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>DIAGNOSTIC TROUBLE CODE CHART</testtitle>
<test1>
<ptxt>Find the output code in the diagnostic trouble code chart (See page <xref label="Seep03" href="RM000003WTP02HX"/>).</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The rear seat entertainment system outputs DTCs for the following system.</ptxt>
</item>
<item>
<ptxt>When DTCs other than those in Diagnostic Trouble Code Chart for the rear seat entertainment system are output, refer to Diagnostic Trouble Code Chart for the relevant system.</ptxt>
</item>
<item>
<ptxt>Refer to Navigation System (See page <xref label="Seep08" href="RM0000011BO0HUX"/>)</ptxt>
</item>
<item>
<ptxt>Refer to Audio and Visual System (w/ Navigation System) (See page <xref label="Seep09" href="RM0000017YP0AXX"/>)</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTCs for the rear seat entertainment system are output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs for the navigation system are output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs for the audio and visual system (w/ Navigation System) are output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>A</result>
<action-ci-right>Go to step 9</action-ci-right>
<result>B</result>
<action-ci-right>GO TO NAVIGATION SYSTEM (See page <xref label="Seep10" href="RM0000011BO0HUX"/>)</action-ci-right>
<result>C</result>
<action-ci-right>GO TO AUDIO AND VISUAL SYSTEM (See page <xref label="Seep11" href="RM0000017YP0AXX"/>)</action-ci-right>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to Problem Symptoms Table (See page <xref label="Seep02" href="RM00000158R02SX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Fault is not listed in Problem Symptoms Table.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Fault is listed in Problem Symptoms Table.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the symptom does not recur and no DTC is output, perform the symptom simulation method (See page <xref label="Seep04" href="RM000002V5U015X"/>).</ptxt>
</atten4>
</test1>
<results>
<result>B</result>
<action-ci-right>ADJUST, REPAIR OR REPLACE IN ACCORDANCE WITH PROBLEM SYMPTOMS TABLE</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK ECU TERMINAL ARRANGEMENT BASED ON MALFUNCTION SYMPTOM</testtitle>
<test1>
<ptxt>Refer to Terminals of ECU (See page <xref label="Seep07" href="RM000002IPS022X"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK CIRCUIT</testtitle>
<test1>
<ptxt>Adjust, repair or replace as necessary.</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>RECHECK FOR DIAGNOSTIC TROUBLE CODE*</testtitle>
<atten4>
<ptxt>After clearing the DTCs, recheck for DTCs.</ptxt>
</atten4>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PERFORM CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>