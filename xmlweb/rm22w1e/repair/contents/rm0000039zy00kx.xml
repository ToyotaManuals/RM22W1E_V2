<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3OG_T00HJ" variety="T00HJ">
<name>DAMPING FORCE CONTROL ACTUATOR (for Front Side)</name>
<para id="RM0000039ZY00KX" category="A" type-id="30014" name-id="SC0SK-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000039ZY00KX_02" type-id="11" category="10" proc-id="RM22W0E___00009SW00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the RH side and LH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0DSX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039ZY00KX_01" type-id="01" category="01">
<s-1 id="RM0000039ZY00KX_01_0002" proc-id="RM22W0E___00009SM00000">
<ptxt>INSTALL FRONT SUSPENSION CONTROL VALVE ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C172615" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the control valve to the vehicle with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>31</t-value1>
<t-value2>316</t-value2>
<t-value4>23</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="C172805" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Connect the control valve connector.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C176190" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Apply a small amount of suspension fluid to a new O-ring and new back-up ring and install then to the accumulator.</ptxt>
</s2>
<s2>
<ptxt>Install the accumulator to the control valve.</ptxt>
<torque>
<torqueitem>
<t-value1>150</t-value1>
<t-value2>1530</t-value2>
<t-value4>111</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZY00KX_01_0013" proc-id="RM22W0E___00009ST00000">
<ptxt>INSTALL FRONT SHOCK ABSORBER CONTROL VALVE ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the bleeder plug and bleeder plug cap to the control accumulator.</ptxt>
<torque>
<torqueitem>
<t-value1>8.3</t-value1>
<t-value2>85</t-value2>
<t-value3>73</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="C172616" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the control valve to the vehicle with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>31</t-value1>
<t-value2>316</t-value2>
<t-value4>23</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="C172811" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZY00KX_01_0003" proc-id="RM22W0E___00009SN00000">
<ptxt>INSTALL FRONT NO. 2 SUSPENSION CONTROL ACCUMULATOR ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a small amount of suspension fluid to a new O-ring and install it to the accumulator.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C179741" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the accumulator to the shock absorber control valve.</ptxt>
<torque>
<torqueitem>
<t-value1>57</t-value1>
<t-value2>581</t-value2>
<t-value4>42</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZY00KX_01_0004" proc-id="RM22W0E___00009SO00000">
<ptxt>CONNECT NO. 2 SUSPENSION CONTROL PRESSURE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the No. 2 suspension control pressure hose, then connect the pressure hose to the control valve with the union bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>50</t-value1>
<t-value2>510</t-value2>
<t-value4>37</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZY00KX_01_0005" proc-id="RM22W0E___00009SP00000">
<ptxt>INSTALL NO. 2 HEIGHT CONTROL TUBE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a union nut wrench, connect the No. 2 control tube to the No. 3 height control tube, shock absorber control valve and control valve.</ptxt>
<torque>
<subtitle>without union nut wrench</subtitle>
<torqueitem>
<t-value1>44</t-value1>
<t-value2>450</t-value2>
<t-value4>33</t-value4>
</torqueitem>
<subtitle>with union nut wrench</subtitle>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use a torque wrench with a fulcrum length of 300 mm (11.8 in.).</ptxt>
</item>
<item>
<ptxt>The torque value for use with a union nut wrench is effective when the union nut wrench is parallel to the torque wrench.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Install the No. 2 control tube with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>31</t-value1>
<t-value2>316</t-value2>
<t-value4>23</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="C178064E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZY00KX_01_0006" proc-id="RM22W0E___00009SQ00000">
<ptxt>INSTALL FRONT NO. 1 SUSPENSION CONTROL ACCUMULATOR ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C172813" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the No. 1 suspension control accumulator to the frame with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>31</t-value1>
<t-value2>316</t-value2>
<t-value4>23</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZY00KX_01_0007" proc-id="RM22W0E___00009SR00000">
<ptxt>INSTALL NO. 1 HEIGHT CONTROL TUBE LH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C180072" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install a new gasket to the No. 1 height control tube, then temporarily install No. 1 height control tube to the control valve and control accumulator with the union bolt and union nut.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C178066E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Using a union nut wrench, tighten the No. 1 height control tube.</ptxt>
<torque>
<subtitle>without union nut wrench</subtitle>
<torqueitem>
<t-value1>44</t-value1>
<t-value2>450</t-value2>
<t-value4>33</t-value4>
</torqueitem>
<subtitle>with union nut wrench</subtitle>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use a torque wrench with a fulcrum length of 300 mm (11.8 in.).</ptxt>
</item>
<item>
<ptxt>The torque value for use with a union nut wrench is effective when the union nut wrench is parallel to the torque wrench.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="C172812E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Tighten the union bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>43</t-value1>
<t-value2>438</t-value2>
<t-value4>32</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZY00KX_01_0014" proc-id="RM22W0E___00009R500000">
<ptxt>BLEED AIR FROM SUSPENSION FLUID
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C176402" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>With the engine stopped, fill the reservoir tank with fluid.</ptxt>
<atten3>
<ptxt>When the engine starts, the pump operates and fluid is supplied to each cylinder from the reservoir tank. Therefore, add the necessary amount of fluid so that the reservoir tank does not become empty.</ptxt>
</atten3>
<atten4>
<ptxt>At this point, the vehicle height is low because the pressure of the cylinders is low.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>With the vehicle on a level surface, start the engine and set the vehicle height to NORMAL with the suspension control switch.</ptxt>
</s2>
<s2>
<ptxt>When the vehicle height becomes NORMAL and the pump stops, stop the engine.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C172939" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Connect a hose to the bleeder plug of the front left side or right side control valve, then loosen the bleeder plug.</ptxt>
<atten2>
<ptxt>Be careful when loosening the control valve bleeder plug because the front vehicle height drops rapidly.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>After the fluid containing air stops coming out, retighten the bleeder plug.</ptxt>
<torque>
<torqueitem>
<t-value1>8.3</t-value1>
<t-value2>85</t-value2>
<t-value3>73</t-value3>
</torqueitem>
</torque>
<atten4>
<ptxt>If the procedures are performed for the first time on the left side, perform the procedures on the right side for the second time.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="C172940" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Connect a hose to the bleeder plug of the rear left side or right side control valve, then loosen the bleeder plug.</ptxt>
<atten2>
<ptxt>Be careful when loosening the control valve bleeder plug because the rear vehicle height drops rapidly.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>After the fluid containing air stops coming out, retighten the bleeder plug.</ptxt>
<torque>
<torqueitem>
<t-value1>8.3</t-value1>
<t-value2>85</t-value2>
<t-value3>73</t-value3>
</torqueitem>
</torque>
<atten4>
<ptxt>If the procedures are performed for the first time on the left side, perform the procedures on the right side for the second time.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Repeat the previous 4 procedures until the fluid containing air stop coming out.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039ZY00KX_01_0015" proc-id="RM22W0E___00009R600000">
<ptxt>CHECK FLUID LEVEL IN RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C176461" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>With the vehicle empty, after setting the vehicle height to NORMAL from LO, check the indicator to make sure the vehicle height is NORMAL and check that the fluid level in the reservoir tank is within the specified range (MAX, MIN).</ptxt>
<atten4>
<ptxt>After changing the vehicle height from LO to NORMAL, do not stop the engine for 25 seconds because the pressure control for the main accumulator is operating. After that, check the fluid level.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039ZY00KX_01_0016" proc-id="RM22W0E___00009R900000">
<ptxt>INSPECT FOR SUSPENSION FLUID LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the union nuts shown in the illustration are tightened to the specified torque.</ptxt>
<torque>
<subtitle>without union nut wrench</subtitle>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
<subtitle>with union nut wrench</subtitle>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use a torque wrench with a fulcrum length of 300 mm (11.8 in.).</ptxt>
</item>
<item>
<ptxt>The torque value for use with a union nut wrench is effective when the union nut wrench is parallel to the torque wrench.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Check for fluid leakage from the parts and connections.</ptxt>
<figure>
<graphic graphicname="C176381E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<atten4>
<ptxt>For union nuts and union bolts not shown in the illustration, refer to the installation procedures for each title.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039ZY00KX_01_0018" proc-id="RM22W0E___00009SV00000">
<ptxt>INSTALL HEIGHT CONTROL CYLINDER BRACKET ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 3 brackets with the 6 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>31</t-value1>
<t-value2>316</t-value2>
<t-value4>23</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="C179787" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZY00KX_01_0009" proc-id="RM22W0E___00009SS00000">
<ptxt>INSTALL HEIGHT CONTROL PROTECTOR PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the protector pipe with the 6 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>163</t-value2>
<t-value4>12</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="C179788" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZY00KX_01_0017" proc-id="RM22W0E___00009SU00000">
<ptxt>INSTALL SIDE STEP ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the side step with the 6 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>306</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B180949" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>