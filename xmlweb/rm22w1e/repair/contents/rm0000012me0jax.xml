<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM0000012ME0JAX" category="C" type-id="302I1" name-id="ES11ID-002" from="201301" to="201308">
<dtccode>P0500</dtccode>
<dtcname>Vehicle Speed Sensor "A"</dtcname>
<subpara id="RM0000012ME0JAX_01" type-id="60" category="03" proc-id="RM22W0E___000026D00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The speed sensor detects the wheel speed and sends the appropriate signals to the skid control ECU. The skid control ECU converts these wheel speed signals into a 4-pulse signal and outputs it to the ECM via the combination meter. The ECM determines the vehicle speed based on the frequency of these pulse signals.</ptxt>
<figure>
<graphic graphicname="A115899E04" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0500</ptxt>
</entry>
<entry valign="middle">
<ptxt>While the vehicle is being driven, no vehicle speed sensor signal is transmitted to the ECM (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in speed signal circuit</ptxt>
</item>
<item>
<ptxt>Combination meter</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000012ME0JAX_02" type-id="64" category="03" proc-id="RM22W0E___000026E00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<topic>
<ptxt>The ECM assumes that the vehicle is being driven when the indicated vehicle speed is more than 9 km/h (5.6 mph). If there is no speed signal from the combination meter despite these conditions being met, the ECM interprets this as a malfunction in the speed signal circuit. The ECM then illuminates the MIL and stores the DTC.</ptxt>
</topic>
</content5>
</subpara>
<subpara id="RM0000012ME0JAX_06" type-id="32" category="03" proc-id="RM22W0E___000026F00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A238382E06" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000012ME0JAX_07" type-id="51" category="05" proc-id="RM22W0E___000026G00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000012ME0JAX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012ME0JAX_08_0001" proc-id="RM22W0E___000026H00000">
<testtitle>CHECK OPERATION OF SPEEDOMETER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Drive the vehicle and check whether the operation of the speedometer in the combination meter is normal.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The vehicle speed sensor is operating normally if the speedometer reading is normal.</ptxt>
</item>
<item>
<ptxt>If the speedometer does not operate, check it by following the procedure described in Speedometer Malfunction (See page <xref label="Seep01" href="RM000002ZM601EX"/>).</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000012ME0JAX_08_0002" fin="false">OK</down>
<right ref="RM0000012ME0JAX_08_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0002" proc-id="RM22W0E___000026I00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (VEHICLE SPD)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Vehicle Speed.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Vehicle speeds displayed on tester and speedometer display are equal.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012ME0JAX_08_0007" fin="true">OK</down>
<right ref="RM0000012ME0JAX_08_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0005" proc-id="RM22W0E___000026L00000">
<testtitle>CHECK HARNESS AND CONNECTOR (COMBINATION METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E7 combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A38 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.89in"/>
<colspec colname="COLSPEC2" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-4 (+S) - A38-13 (SPD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E7-4 (+S) or A38-13 (SPD) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012ME0JAX_08_0003" fin="false">OK</down>
<right ref="RM0000012ME0JAX_08_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0003" proc-id="RM22W0E___000026J00000">
<testtitle>CHECK COMBINATION METER ASSEMBLY (+S VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E154777E05" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E7 combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>E7-4 (+S) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012ME0JAX_08_0004" fin="false">OK</down>
<right ref="RM0000012ME0JAX_08_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0004" proc-id="RM22W0E___000026K00000">
<testtitle>CHECK COMBINATION METER ASSEMBLY (SPD SIGNAL OUTPUT WAVEFORM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A179960E14" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Move the shift lever to N.</ptxt>
</test1>
<test1>
<ptxt>Jack up the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-4 (+S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Wheel turned slowly</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Voltage generated intermittently</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>The output voltage should fluctuate up and down, similarly to the diagram, when the wheel is turned slowly.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000012ME0JAX_08_0010" fin="false">OK</down>
<right ref="RM0000012ME0JAX_08_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0011" proc-id="RM22W0E___000026M00000">
<testtitle>CHECK COMBINATION METER ASSEMBLY (SPD SIGNAL INPUT WAVEFORM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A179960E16" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Move the shift lever to N.</ptxt>
</test1>
<test1>
<ptxt>Jack up the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-3 (SI) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Wheel turned slowly</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Voltage generated intermittently</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>The output voltage should fluctuate up and down, similarly to the diagram, when the wheel is turned slowly.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000012ME0JAX_08_0013" fin="true">OK</down>
<right ref="RM0000012ME0JAX_08_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0012" proc-id="RM22W0E___000026N00000">
<testtitle>CHECK HARNESS AND CONNECTOR (COMBINATION METER - SKID CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the combination meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according the value(s) in the table below.</ptxt>
</test1>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-3 (SI) - A24-12 (SP1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-3 (SI) or A24-12 (SP1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</content6>
<res>
<down ref="RM0000012ME0JAX_08_0014" fin="true">OK</down>
<right ref="RM0000012ME0JAX_08_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0006">
<testtitle>GO TO MALFUNCTION IN SPEEDOMETER<xref label="Seep01" href="RM000002ZM601EX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0007">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ109X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0013">
<testtitle>REPLACE COMBINATION METER ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM0000012ME0JAX_08_0014">
<testtitle>REPLACE MASTER CYLINDER SOLENOID</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>