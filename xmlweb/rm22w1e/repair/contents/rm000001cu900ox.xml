<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3OC_T00HF" variety="T00HF">
<name>ACTIVE HEIGHT CONTROL SUSPENSION</name>
<para id="RM000001CU900OX" category="C" type-id="803R6" name-id="SC0QP-06" from="201308">
<dtccode>C1763</dtccode>
<dtcname>Abnormal Pump Motor Oil Pressure</dtcname>
<subpara id="RM000001CU900OX_01" type-id="60" category="03" proc-id="RM22W0E___00009Q300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.10in"/>
<colspec colname="COL2" colwidth="3.02in"/>
<colspec colname="COL3" colwidth="2.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1763</ptxt>
</entry>
<entry valign="middle">
<ptxt>While the motor relay is in operation, the condition that the fluid pressure is 0.6 MPa (6.1 kgf/cm<sup>2</sup>, 87 psi) or less continues for 0.6 seconds.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Suspension control relay</ptxt>
</item>
<item>
<ptxt>Height control pump and motor</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001CU900OX_04" type-id="32" category="03" proc-id="RM22W0E___00009QA00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C176979E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001CU900OX_02" type-id="51" category="05" proc-id="RM22W0E___00009Q400001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before performing troubleshooting, inspect the connectors of related circuits.</ptxt>
</item>
<item>
<ptxt>If the suspension control ECU or height control sensor is replaced, the vehicle height offset calibration must be performed (See page <xref label="Seep01" href="RM000003AG300EX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001CU900OX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001CU900OX_03_0004" proc-id="RM22W0E___00009Q600001">
<testtitle>INSPECT SUSPENSION CONTROL RELAY (AHC)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the suspension control relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<figure>
<graphic graphicname="C149624E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.97in"/>
<colspec colname="COL3" colwidth="1.13in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery voltage is not applied to terminal 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery voltage is applied to terminal 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001CU900OX_03_0005" fin="false">OK</down>
<right ref="RM000001CU900OX_03_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001CU900OX_03_0005" proc-id="RM22W0E___00009Q700001">
<testtitle>CHECK HARNESS AND CONNECTOR (SUSPENSION CONTROL ECU - RELAY (AHC) AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C176974E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Remove the suspension control relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the K36 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.11in"/>
<colspec colname="COL2" colwidth="0.89in"/>
<colspec colname="COL3" colwidth="1.13in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>K36-16 (RC) - Relay block suspension control relay terminal 1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K36-16 (RC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Relay block suspension control relay terminal 2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001CU900OX_03_0013" fin="false">OK</down>
<right ref="RM000001CU900OX_03_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001CU900OX_03_0013" proc-id="RM22W0E___00009Q800001">
<testtitle>CHECK HARNESS AND CONNECTOR (BATTERY - SUSPENSION CONTROL RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the suspension control relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C177031E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.11in"/>
<colspec colname="COL2" colwidth="0.89in"/>
<colspec colname="COL3" colwidth="1.13in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Relay block suspension control relay terminal 5 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001CU900OX_03_0014" fin="false">OK</down>
<right ref="RM000001CU900OX_03_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001CU900OX_03_0014" proc-id="RM22W0E___00009Q900001">
<testtitle>CHECK HARNESS AND CONNECTOR (SUSPENSION CONTROL RELAY - PUMP AND MOTOR AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the suspension control relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the L36 pump and motor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C177032E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.11in"/>
<colspec colname="COL2" colwidth="0.89in"/>
<colspec colname="COL3" colwidth="1.13in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Relay block suspension control relay terminal 3 - L36-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Relay block suspension control relay terminal 3 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L36-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001CU900OX_03_0002" fin="false">OK</down>
<right ref="RM000001CU900OX_03_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001CU900OX_03_0002" proc-id="RM22W0E___00009Q500001">
<testtitle>CHECK OPERATION OF HEIGHT CONTROL PUMP AND MOTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the height control pump and motor (See page <xref label="Seep01" href="RM0000039ZE00AX"/>).</ptxt>
<figure>
<graphic graphicname="C177053E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Apply battery voltage to the height control pump and motor and check the operation of the motor. </ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.64in"/>
<colspec colname="COL2" colwidth="1.49in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 2</ptxt>
<ptxt>Battery negative (-) → Terminal 1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Motor operates</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful of fluid spray when the height control pump and motor is operating.</ptxt>
</item>
<item>
<ptxt>Do not allow the motor to operate for 60 seconds or more.</ptxt>
</item>
<item>
<ptxt>If the height control pump and motor is shorted, locked, or has a similar type of malfunction, a large amount of current will be flowing. Therefore, if the motor does not operate, immediately stop this inspection.</ptxt>
</item>
</list1>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000001CU900OX_03_0012" fin="true">OK</down>
<right ref="RM000001CU900OX_03_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001CU900OX_03_0008">
<testtitle>REPLACE HEIGHT CONTROL PUMP AND MOTOR<xref label="Seep01" href="RM0000039ZE00AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001CU900OX_03_0009">
<testtitle>REPLACE SUSPENSION CONTROL RELAY</testtitle>
</testgrp>
<testgrp id="RM000001CU900OX_03_0010">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001CU900OX_03_0012">
<testtitle>REPLACE SUSPENSION CONTROL ECU<xref label="Seep01" href="RM000003A0D00GX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>