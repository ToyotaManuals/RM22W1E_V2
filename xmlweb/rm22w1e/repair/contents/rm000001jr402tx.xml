<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3W0_T00P3" variety="T00P3">
<name>REAR EVAPORATOR TEMPERATURE SENSOR (w/ Heater)</name>
<para id="RM000001JR402TX" category="A" type-id="8000E" name-id="AC47S-01" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM000001JR402TX_02" type-id="01" category="01">
<s-1 id="RM000001JR402TX_02_0025">
<ptxt>INSTALL WIRING AIR CONDITIONING HARNESS SUB-ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM000001JR402TX_02_0003" proc-id="RM22W0E___0000GSX00000">
<ptxt>INSTALL REAR EVAPORATOR TEMPERATURE SENSOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154801" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the sensor.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0004" proc-id="RM22W0E___0000GSY00000">
<ptxt>INSTALL EVAPORATOR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E131047" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Sufficiently apply compressor oil to 2 new O-rings and the fitting surface of the hose joint.</ptxt>
<spec>
<title>Compressor oil</title>
<specitem>
<ptxt>ND-OIL 8 or equivalent</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Install the 2 O-rings to the evaporator.</ptxt>
</s2>
<s2>
<ptxt>Install the evaporator.</ptxt>
</s2>
<s2>
<ptxt>Install the rear cooling unit case LH with the 12 screws.</ptxt>
<figure>
<graphic graphicname="E154800" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0007" proc-id="RM22W0E___0000GSZ00000">
<ptxt>INSTALL BLOWER MOTOR CONTROLLER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154812" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the blower motor controller with the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0008" proc-id="RM22W0E___0000GT000000">
<ptxt>INSTALL REAR BLOWER MOTOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154799" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the blower motor with the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0009" proc-id="RM22W0E___0000GT100000">
<ptxt>INSTALL HEATER RADIATOR UNIT SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154798" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the radiator unit to the rear cooling unit.</ptxt>
</s2>
<s2>
<ptxt>Install the heater clamp with the screw and attach the claw.</ptxt>
<figure>
<graphic graphicname="E154797" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0021" proc-id="RM22W0E___0000GT600000">
<ptxt>INSTALL REAR COOLING UNIT EXPANSION VALVE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154796" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a 4 mm hexagon wrench, install the expansion valve with the 2 hexagon bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>3.5</t-value1>
<t-value2>36</t-value2>
<t-value3>31</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0017" proc-id="RM22W0E___0000GT200000">
<ptxt>INSTALL AIR CONDITIONER ACCESSORY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154336" width="2.775699831in" height="4.7836529in"/>
</figure>
<s2>
<ptxt>Sufficiently apply compressor oil to 2 new O-rings and the fitting surface of the hose joint.</ptxt>
<spec>
<title>Compressor oil</title>
<specitem>
<ptxt>ND-OIL 8 or equivalent</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Install the 2 O-rings to the air conditioner accessory assembly.</ptxt>
</s2>
<s2>
<ptxt>Install the air conditioner accessory assembly.</ptxt>
</s2>
<s2>
<ptxt>Using pliers, grip the claws of the clips and slide the 2 clips. </ptxt>
</s2>
<s2>
<ptxt>Install the bolt and 4 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>5.4</t-value1>
<t-value2>55</t-value2>
<t-value3>48</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="E154795" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install new packing as shown in the illustration.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0018" proc-id="RM22W0E___0000GT300000">
<ptxt>INSTALL REAR MODE DAMPER SERVO SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E132093E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Align the damper servo as shown in the illustration, and install it with the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="E161096" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the plate cover with the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0022" proc-id="RM22W0E___0000GT700000">
<ptxt>INSTALL NO. 2 AIR MIX DAMPER SERVO SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E155954E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Align the damper servo as shown in the illustration, and install it with the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0019" proc-id="RM22W0E___0000GT400000">
<ptxt>INSTALL REAR AIR MIX DAMPER SERVO SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E132092E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Align the damper servo as shown in the illustration, and install it with the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0020" proc-id="RM22W0E___0000GT500000">
<ptxt>INSTALL DRAIN COOLER HOSE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154791" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the drain hose.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0023" proc-id="RM22W0E___0000GT800000">
<ptxt>INSTALL REAR SIDE NO. 3 AIR DUCT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154789" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the duct with the 2 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>5.4</t-value1>
<t-value2>55</t-value2>
<t-value3>48</t-value3>
</torqueitem>
</torque>
</s2>

</content1>
</s-1>
<s-1 id="RM000001JR402TX_02_0024" proc-id="RM22W0E___0000GT900000">
<ptxt>INSTALL REAR SIDE NO. 2 AIR DUCT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154788" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the duct with the 3 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>5.4</t-value1>
<t-value2>55</t-value2>
<t-value3>48</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>