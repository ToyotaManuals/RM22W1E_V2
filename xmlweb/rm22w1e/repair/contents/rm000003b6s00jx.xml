<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S002F" variety="S002F">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S002F_7C3WY_T00Q1" variety="T00Q1">
<name>FRONT CONSOLE BOX (w/ Cool Box)</name>
<para id="RM000003B6S00JX" category="A" type-id="80001" name-id="IT22Y-02" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM000003B6S00JX_01" type-id="01" category="01">
<s-1 id="RM000003B6S00JX_01_0014" proc-id="RM22W0E___00004A400000">
<ptxt>RECOVER REFRIGERANT FROM REFRIGERATION SYSTEM
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>Turn the A/C switch on.</ptxt>
</s2>
<s2>
<ptxt>Operate the cooler compressor while the engine speed is approximately 1000 rpm for 5 to 6 minutes to circulate the refrigerant and collect the compressor oil remaining in each component into the cooler compressor.</ptxt>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Recover the refrigerant from the A/C system using a refrigerant recovery unit.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6S00JX_01_0015" proc-id="RM22W0E___0000HSC00000">
<ptxt>REMOVE FRONT SEAT ASSEMBLY LH (for Power Seat)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front seat assembly LH (See page <xref label="Seep01" href="RM00000390S00SX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0016" proc-id="RM22W0E___0000HSD00000">
<ptxt>REMOVE FRONT SEAT ASSEMBLY LH (for Manual Seat)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front seat assembly LH (See page <xref label="Seep01" href="RM00000390S00RX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0002" proc-id="RM22W0E___0000AB300000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E155426E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the panel cushion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0003" proc-id="RM22W0E___0000AB400000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip.</ptxt>
<figure>
<graphic graphicname="B181910" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 8 claws.</ptxt>
</s2>
<s2>
<ptxt>Remove the panel pad and disconnect the connectors and 2 clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0004" proc-id="RM22W0E___0000GEZ00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL FINISH CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B180004E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the panel cushion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0005" proc-id="RM22W0E___0000AB600000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip.</ptxt>
<figure>
<graphic graphicname="B180005" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 7 claws and remove the panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0006" proc-id="RM22W0E___0000AB700000">
<ptxt>REMOVE SHIFT LEVER KNOB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Twist the shift lever knob in the direction indicated by the arrow and remove it.</ptxt>
<figure>
<graphic graphicname="B186290E01" width="7.106578999in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0007" proc-id="RM22W0E___0000AB800000">
<ptxt>REMOVE LOWER CENTER INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 7 claws.</ptxt>
<figure>
<graphic graphicname="B180006" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0008" proc-id="RM22W0E___0000AB900000">
<ptxt>REMOVE REAR UPPER CONSOLE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 9 claws and remove the panel.</ptxt>
<figure>
<graphic graphicname="B180009" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0009" proc-id="RM22W0E___0000ABA00000">
<ptxt>REMOVE CONSOLE CUP HOLDER BOX SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 claws and remove the box.</ptxt>
<figure>
<graphic graphicname="B180010" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0010" proc-id="RM22W0E___0000ABB00000">
<ptxt>REMOVE UPPER CONSOLE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 14 claws.</ptxt>
<figure>
<graphic graphicname="B180011" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the console panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0013" proc-id="RM22W0E___0000HSB00000">
<ptxt>REMOVE NO. 1 COOLER COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp on the lower part of the cooler cover, and then pull the cooler cover upward to remove it.</ptxt>
<figure>
<graphic graphicname="B183312" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0011" proc-id="RM22W0E___0000HS900000">
<ptxt>REMOVE REAR CONSOLE END PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B180013E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 8 claws.</ptxt>
</s2>
<s2>
<ptxt>Remove the end panel and disconnect the connectors and 2 clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6S00JX_01_0012" proc-id="RM22W0E___0000HSA00000">
<ptxt>REMOVE COOLING BOX ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182475" width="7.106578999in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the 2 cooler pipes.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 O-rings from the cooler pipes.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the cooling box.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>