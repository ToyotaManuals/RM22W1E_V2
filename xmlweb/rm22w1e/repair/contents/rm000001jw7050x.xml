<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SR_T00LU" variety="T00LU">
<name>KEY REMINDER WARNING SYSTEM</name>
<para id="RM000001JW7050X" category="U" type-id="3001G" name-id="DL37U-02" from="201301">
<name>TERMINALS OF ECU</name>
<subpara id="RM000001JW7050X_z0" proc-id="RM22W0E___0000EAS00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</ptxt>
<figure>
<graphic graphicname="C172609E03" width="7.106578999in" height="8.799559038in"/>
</figure>
<step2>
<ptxt>Disconnect the 2A, 2B, 2D and E4 ECU connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2B-20 (BATB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2A-1 (ACC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ACC power supply</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2A-1 (ACC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ACC power supply</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2D-62 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E4-5 (KSW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Unlock warning switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No key in ignition key cylinder</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E4-5 (KSW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Unlock warning switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Reconnect the 2A, 2B, 2D and E4 ECU connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.43in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E1-24 (DCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L*1 - Body ground</ptxt>
<ptxt>Y*2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door courtesy switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E1-24 (DCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L*1 - Body ground</ptxt>
<ptxt>Y*2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Driver side door courtesy switch input</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch off, and driver side door courtesy switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
<ptxt>If the result is not as specified, the ECU may have a malfunction.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>