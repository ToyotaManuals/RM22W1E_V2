<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000000XI90T9X" category="C" type-id="803KW" name-id="BCDWM-01" from="201301" to="201308">
<dtccode>C1401</dtccode>
<dtcname>Front Speed Sensor RH Malfunction</dtcname>
<dtccode>C1271</dtccode>
<dtcname>Low Output Signal of Front Speed Sensor RH (Test Mode DTC)</dtcname>
<dtccode>C1272</dtccode>
<dtcname>Low Output Signal of Front Speed Sensor LH (Test Mode DTC)</dtcname>
<dtccode>C1402</dtccode>
<dtcname>Front Speed Sensor LH Malfunction</dtcname>
<subpara id="RM000000XI90T9X_01" type-id="60" category="03" proc-id="RM22W0E___0000AOA00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The speed sensor detects the wheel speed and sends the appropriate signals to the skid control ECU (master cylinder solenoid). These signals are used for brake control.</ptxt>
<atten4>
<ptxt>When the connectors between the speed sensor and skid control ECU (master cylinder solenoid) are connected, the following waveform is output.</ptxt>
</atten4>
<figure>
<graphic graphicname="C232084E02" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.93in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1401</ptxt>
<ptxt>C1402</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>At a vehicle speed of 10 km/h (6 mph) or more, the output voltage from one of the speed sensors is less than that from the other sensors for 15 seconds or more.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of less than 10 km/h (6 mph), the output from one of the speed sensors is 0 km/h (0 mph) for 1 second or more.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 10 km/h (6 mph) or more, the outputs from both front speed sensors are 0 km/h (0 mph) for 15 seconds or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Front speed sensor RH/LH</ptxt>
</item>
<item>
<ptxt>Front speed sensor rotor RH/LH</ptxt>
</item>
<item>
<ptxt>Sensor installation</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1271</ptxt>
<ptxt>C1272</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stored only during test mode.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Front speed sensor RH/LH</ptxt>
</item>
<item>
<ptxt>Front speed sensor rotor RH/LH</ptxt>
</item>
<item>
<ptxt>Sensor installation</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTCs C1401 and C1271 are for the front speed sensor RH.</ptxt>
</item>
<item>
<ptxt>DTCs C1402 and C1272 are for the front speed sensor LH.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XI90T9X_03" type-id="51" category="05" proc-id="RM22W0E___0000AOB00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Check the speed sensor signal after cleaning or replacement (See page <xref label="Seep01" href="RM0000054KD001X"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XI90T9X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XI90T9X_04_0005" proc-id="RM22W0E___0000AOD00000">
<testtitle>CHECK FRONT SPEED SENSOR INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Check the speed sensor installation (See page <xref label="Seep01" href="RM000001B2H01PX"/>).</ptxt>
<figure>
<graphic graphicname="C144929E49" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>There is no clearance between the sensor and the front steering knuckle.</ptxt>
</specitem>
<specitem>
<ptxt>The installation bolt is tightened properly.</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Speed Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>No clearance</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XI90T9X_04_0006" fin="false">OK</down>
<right ref="RM000000XI90T9X_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0006" proc-id="RM22W0E___0000AOE00000">
<testtitle>INSPECT FRONT SPEED SENSOR TIP</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the front speed sensor (See page <xref label="Seep01" href="RM000001B2J01KX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the speed sensor tip.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No scratches, oil, or foreign matter on the sensor tip.</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>If no damage to the speed sensor tip is found during this inspection, do not replace the speed sensor.</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000000XI90T9X_04_0029" fin="false">OK</down>
<right ref="RM000000XI90T9X_04_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0029" proc-id="RM22W0E___0000AOH00000">
<testtitle>INSPECT FRONT SPEED SENSOR ROTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the front axle hub sub-assembly (See page <xref label="Seep01" href="RM000003BNQ00HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the speed sensor rotor.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No scratches, oil, or foreign matter on the rotors.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The front speed sensor rotor is incorporated into the front axle with ABS rotor bearing assembly.</ptxt>
<ptxt>If the front speed sensor rotor needs to be replaced, replace it together with the front axle with ABS rotor bearing assembly.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000XI90T9X_04_0002" fin="false">OK</down>
<right ref="RM000000XI90T9X_04_0030" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0002" proc-id="RM22W0E___0000AOC00000">
<testtitle>READ VALUE USING GTS (FR/FL WHEEL SPEED)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.16in"/>
<colspec colname="COL2" colwidth="2.22in"/>
<colspec colname="COL3" colwidth="1.74in"/>
<colspec colname="COLSPEC2" colwidth="1.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that there is no difference between the speed value output from the speed sensor displayed on the GTS and the speed value displayed on the speedometer when driving the vehicle.</ptxt>
<atten4>
<ptxt>Factors that affect the indicated vehicle speed include tire size, tire inflation and tire wear. The speed indicated on the speedometer has an allowable margin of error. This can be tested using a speedometer tester (calibrated chassis dynamometer). For details about testing and the margin of error, refer to the reference chart (See page <xref label="Seep01" href="RM000002Z4Q02RX"/>).</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The speed value output from the speed sensor displayed on the GTS is the same as the actual vehicle speed measured using a speedometer tester (calibrated chassis dynamometer).</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XI90T9X_04_0023" fin="true">OK</down>
<right ref="RM000000XI90T9X_04_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0010" proc-id="RM22W0E___0000AOF00000">
<testtitle>REPLACE FRONT SPEED SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the front speed sensor (See page <xref label="Seep01" href="RM000001B2J01KX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XI90T9X_04_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0011" proc-id="RM22W0E___0000AOG00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 40 km/h (25 mph) or more for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00NX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XI90T9X_04_0013" fin="true">A</down>
<right ref="RM000000XI90T9X_04_0027" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0023">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0013">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0014">
<testtitle>INSTALL FRONT SPEED SENSOR CORRECTLY</testtitle>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0015">
<testtitle>CLEAN OR REPLACE FRONT SPEED SENSOR</testtitle>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0030">
<testtitle>CLEAN OR REPLACE FRONT SPEED SENSOR ROTOR</testtitle>
</testgrp>
<testgrp id="RM000000XI90T9X_04_0027">
<testtitle>REPLACE FRONT AXLE WITH ABS ROTOR BEARING ASSEMBLY<xref label="Seep01" href="RM0000016X7029X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>