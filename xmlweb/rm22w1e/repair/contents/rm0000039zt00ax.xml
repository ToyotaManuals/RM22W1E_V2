<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3OF_T00HI" variety="T00HI">
<name>HEIGHT CONTROL ACCUMULATOR</name>
<para id="RM0000039ZT00AX" category="A" type-id="30014" name-id="SC0SC-02" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000039ZT00AX_01" type-id="01" category="01">
<s-1 id="RM0000039ZT00AX_01_0001" proc-id="RM22W0E___00009S500000">
<ptxt>INSTALL SUSPENSION CONTROL PUMP ACCUMULATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172584" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the accumulator with the 3 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>31</t-value1>
<t-value2>316</t-value2>
<t-value4>23</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZT00AX_01_0002" proc-id="RM22W0E___00009S600000">
<ptxt>INSTALL NO. 7 HEIGHT CONTROL TUBE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174938E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Using a union nut wrench, install the No. 7 control tube to the control valve and accumulator.</ptxt>
<torque>
<subtitle>without union nut wrench</subtitle>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
<subtitle>with union nut wrench</subtitle>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use a torque wrench with a fulcrum length of 300 mm (11.8 in.).</ptxt>
</item>
<item>
<ptxt>The torque value for use with a union nut wrench is effective when the union nut wrench is parallel to the torque wrench.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZT00AX_01_0009" proc-id="RM22W0E___00009S800000">
<ptxt>INSTALL CENTER EXHAUST PIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>Install the center exhaust pipe (See page <xref label="Seep01" href="RM000002HA500MX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1UR-FE:</ptxt>
<ptxt>Install the center exhaust pipe (See page <xref label="Seep02" href="RM00000456H00JX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 3UR-FE:</ptxt>
<ptxt>Install the center exhaust pipe (See page <xref label="Seep03" href="RM000002H9Y02GX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1VD-FTV:</ptxt>
<ptxt>Install the center exhaust pipe (See page <xref label="Seep04" href="RM000003B0N00EX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZT00AX_01_0010" proc-id="RM22W0E___00009S900000">
<ptxt>INSTALL TAILPIPE ASSEMBLY</ptxt>
<content1 releasenbr="3">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>Install the tailpipe (See page <xref label="Seep01" href="RM000002HA500MX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1UR-FE:</ptxt>
<ptxt>Install the tailpipe (See page <xref label="Seep02" href="RM00000456H00JX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 3UR-FE:</ptxt>
<ptxt>Install the tailpipe (See page <xref label="Seep03" href="RM000002H9Y02GX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1VD-FTV:</ptxt>
<ptxt>Install the tailpipe (See page <xref label="Seep04" href="RM000003B0N00EX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039ZT00AX_01_0006" proc-id="RM22W0E___00009R500000">
<ptxt>BLEED AIR FROM SUSPENSION FLUID
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C176402" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>With the engine stopped, fill the reservoir tank with fluid.</ptxt>
<atten3>
<ptxt>When the engine starts, the pump operates and fluid is supplied to each cylinder from the reservoir tank. Therefore, add the necessary amount of fluid so that the reservoir tank does not become empty.</ptxt>
</atten3>
<atten4>
<ptxt>At this point, the vehicle height is low because the pressure of the cylinders is low.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>With the vehicle on a level surface, start the engine and set the vehicle height to NORMAL with the suspension control switch.</ptxt>
</s2>
<s2>
<ptxt>When the vehicle height becomes NORMAL and the pump stops, stop the engine.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C172939" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Connect a hose to the bleeder plug of the front left side or right side control valve, then loosen the bleeder plug.</ptxt>
<atten2>
<ptxt>Be careful when loosening the control valve bleeder plug because the front vehicle height drops rapidly.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>After the fluid containing air stops coming out, retighten the bleeder plug.</ptxt>
<torque>
<torqueitem>
<t-value1>8.3</t-value1>
<t-value2>85</t-value2>
<t-value3>73</t-value3>
</torqueitem>
</torque>
<atten4>
<ptxt>If the procedures are performed for the first time on the left side, perform the procedures on the right side for the second time.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="C172940" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Connect a hose to the bleeder plug of the rear left side or right side control valve, then loosen the bleeder plug.</ptxt>
<atten2>
<ptxt>Be careful when loosening the control valve bleeder plug because the rear vehicle height drops rapidly.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>After the fluid containing air stops coming out, retighten the bleeder plug.</ptxt>
<torque>
<torqueitem>
<t-value1>8.3</t-value1>
<t-value2>85</t-value2>
<t-value3>73</t-value3>
</torqueitem>
</torque>
<atten4>
<ptxt>If the procedures are performed for the first time on the left side, perform the procedures on the right side for the second time.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Repeat the previous 4 procedures until the fluid containing air stop coming out.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039ZT00AX_01_0007" proc-id="RM22W0E___00009R600000">
<ptxt>CHECK FLUID LEVEL IN RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C176461" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>With the vehicle empty, after setting the vehicle height to NORMAL from LO, check the indicator to make sure the vehicle height is NORMAL and check that the fluid level in the reservoir tank is within the specified range (MAX, MIN).</ptxt>
<atten4>
<ptxt>After changing the vehicle height from LO to NORMAL, do not stop the engine for 25 seconds because the pressure control for the main accumulator is operating. After that, check the fluid level.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039ZT00AX_01_0008" proc-id="RM22W0E___00009R900000">
<ptxt>INSPECT FOR SUSPENSION FLUID LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the union nuts shown in the illustration are tightened to the specified torque.</ptxt>
<torque>
<subtitle>without union nut wrench</subtitle>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
<subtitle>with union nut wrench</subtitle>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use a torque wrench with a fulcrum length of 300 mm (11.8 in.).</ptxt>
</item>
<item>
<ptxt>The torque value for use with a union nut wrench is effective when the union nut wrench is parallel to the torque wrench.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Check for fluid leakage from the parts and connections.</ptxt>
<figure>
<graphic graphicname="C176381E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<atten4>
<ptxt>For union nuts and union bolts not shown in the illustration, refer to the installation procedures for each title.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039ZT00AX_01_0005" proc-id="RM22W0E___00009S700000">
<ptxt>INSTALL HEIGHT CONTROL UNIT INSULATOR
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172582" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the insulator to the height control unit with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>31</t-value1>
<t-value2>316</t-value2>
<t-value4>23</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000039ZT00AX_01_0011" proc-id="RM22W0E___00009SA00000">
<ptxt>CHECK FOR EXHAUST GAS LEAK</ptxt>
<content1 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>If gas is leaking, tighten the areas necessary to stop the leak. Replace damaged parts as necessary.</ptxt>
</item>
</list1>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>