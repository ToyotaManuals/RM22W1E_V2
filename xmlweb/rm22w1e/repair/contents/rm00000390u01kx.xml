<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3UD_T00NG" variety="T00NG">
<name>FRONT SEAT SIDE AIRBAG ASSEMBLY (for Power Seat)</name>
<para id="RM00000390U01KX" category="A" type-id="30014" name-id="RSDVR-01" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM00000390U01KX_02" type-id="11" category="10" proc-id="RM22W0E___0000FMC00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for LHD and RHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000390U01KX_01" type-id="01" category="01">
<s-1 id="RM00000390U01KX_01_0033" proc-id="RM22W0E___0000FM100000">
<ptxt>INSTALL FRONT SEAT AIRBAG ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Make sure that the seat frame assembly is not deformed. If it is, replace it with a new one.</ptxt>
</atten2>
<s2>
<ptxt>Install the front seat airbag assembly with 2 nuts.</ptxt>
<figure>
<graphic graphicname="B102199" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 2 clamps to connect the wire harness.</ptxt>
</s2>
<s2>
<ptxt>Attach the 5 clamps to connect the wire harness.</ptxt>
<figure>
<graphic graphicname="B102196" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390U01KX_01_0043" proc-id="RM22W0E___0000FMA00000">
<ptxt>INSTALL SEAT CLIMATE CONTROL CONTROLLER LH (w/ Climate Control Seat System)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B299481" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seat climate control controller LH with the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 claws to connect the seat climate control blower LH.</ptxt>
<figure>
<graphic graphicname="B299480" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01KX_01_0044" proc-id="RM22W0E___0000FMB00000">
<ptxt>INSTALL FRONT SEATBACK PROTECTOR (w/ Climate Control Seat System)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B299469E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Apply double-sided tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double-sided tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the front seatback protector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01KX_01_0034" proc-id="RM22W0E___0000FM200000">
<ptxt>INSTALL SEATBACK COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the hog rings.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B180713E02" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut and detach the seatback cover bracket.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Cover Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B180716E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the hooks and remove the seatback cover with pad.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01KX_01_0035" proc-id="RM22W0E___0000FM300000">
<ptxt>INSTALL FRONT SEAT HEADREST SUPPORT
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B298589" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the 2 front seat headrest supports.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01KX_01_0036" proc-id="RM22W0E___0000FM400000">
<ptxt>INSTALL FRONT SEATBACK BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182646" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Move the front seatback board sub-assembly in the direction of the arrow to attach the 2 hooks and install it.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01KX_01_0037" proc-id="RM22W0E___0000FM500000">
<ptxt>INSTALL FRONT INNER SEAT CUSHION SHIELD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180703" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws and clip to install the front inner seat cushion shield LH.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01KX_01_0031" proc-id="RM22W0E___0000FM000000">
<ptxt>INSTALL FRONT SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185875E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install the front seat inner belt assembly with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not allow the anchor part of the front seat inner belt assembly to overlap the protruding parts of the front seat adjuster.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the connectors and attach the clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01KX_01_0039" proc-id="RM22W0E___0000FM600000">
<ptxt>INSTALL FRONT SEAT CUSHION SHIELD LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Driver Side:</ptxt>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 claws to install the front seat cushion shield LH together with the front inner No. 1 cushion shield LH.</ptxt>
<figure>
<graphic graphicname="B180700" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 rubber bands.</ptxt>
<figure>
<graphic graphicname="B180699" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01KX_01_0040" proc-id="RM22W0E___0000FM700000">
<ptxt>INSTALL RECLINING POWER SEAT SWITCH KNOB
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180734" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the reclining power seat switch knob.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01KX_01_0041" proc-id="RM22W0E___0000FM800000">
<ptxt>INSTALL SLIDE AND VERTICAL POWER SEAT SWITCH KNOB
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180735" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the slide and vertical power seat switch knob.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390U01KX_01_0042" proc-id="RM22W0E___0000FM900000">
<ptxt>INSTALL FRONT SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front seat assembly LH (See page <xref label="Seep01" href="RM00000390Q00WX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>