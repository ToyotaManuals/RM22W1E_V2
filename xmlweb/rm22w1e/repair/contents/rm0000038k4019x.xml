<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C40G_T00TJ" variety="T00TJ">
<name>REAR BUMPER (for Double Swing Out Type)</name>
<para id="RM0000038K4019X" category="A" type-id="8000E" name-id="ET7HB-01" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM0000038K4019X_01" type-id="01" category="01">
<s-1 id="RM0000038K4019X_01_0001" proc-id="RM22W0E___0000JQQ00000">
<ptxt>INSTALL REAR NO. 2 STEP COVER (w/ Towing Hitch)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws to install the rear No. 2 step cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws and 2 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0002" proc-id="RM22W0E___0000JQR00000">
<ptxt>INSTALL REAR LOWER BUMPER COVER (w/ Towing Hitch)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear lower bumper cover with the clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0015" proc-id="RM22W0E___0000JQV00000">
<ptxt>INSTALL REAR NO. 2 STEP COVER (w/ Pintle Hook)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 10 claws to install the rear No. 2 step cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0016" proc-id="RM22W0E___0000JQW00000">
<ptxt>INSTALL REAR BUMPER PAD LH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>When installing the rear bumper pad, heat the rear bumper cover using a heat light.</ptxt>
</atten4>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Rear Bumper Cover</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not heat the rear bumper cover excessively.</ptxt>
</atten3>
<s2>
<ptxt>When using a new rear bumper cover:</ptxt>
<s3>
<ptxt>Clean the surface of a new rear bumper cover.</ptxt>
</s3>
<s3>
<ptxt>Using a heat light, heat the rear bumper cover surface.</ptxt>
</s3>
</s2>
<s2>
<ptxt>When reusing the rear bumper cover:</ptxt>
<s3>
<ptxt>Using a heat light, heat the rear bumper cover surface.</ptxt>
</s3>
<s3>
<ptxt>Remove the double-sided tape from the rear bumper cover.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any tape adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install a new rear bumper pad.</ptxt>
<figure>
<graphic graphicname="B292677" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the peeling paper from the face of a new rear bumper pad.</ptxt>
<atten4>
<ptxt>After removing the peeling paper, keep the exposed adhesive free from foreign matter.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Align the rear bumper pad with the mark-off line of the rear bumper.</ptxt>
</s3>
<s3>
<ptxt>Install the rear bumper pad.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0017" proc-id="RM22W0E___0000JQX00000">
<ptxt>INSTALL REAR BUMPER PAD RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0022" proc-id="RM22W0E___0000JAH00000">
<ptxt>INSTALL BACK-UP LIGHT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the back-up light assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 nuts.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038K4019X_01_0004" proc-id="RM22W0E___0000JQS00000">
<ptxt>INSTALL BACK-UP LIGHT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0018" proc-id="RM22W0E___0000JQY00000">
<ptxt>INSTALL REAR BUMPER NO. 2 SIDE BRACKET (w/ Tire Carrier)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws to install the rear bumper No. 2 side bracket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0019" proc-id="RM22W0E___0000JQZ00000">
<ptxt>INSTALL REAR BUMPER NO. 3 SIDE BRACKET (w/ Tire Carrier)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear bumper No. 3 side bracket with the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0020" proc-id="RM22W0E___0000JR000000">
<ptxt>INSTALL REAR BUMPER UPPER COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws to install the rear bumper upper cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0021" proc-id="RM22W0E___0000JR100000">
<ptxt>INSTALL REAR BUMPER UPPER COVER RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0005" proc-id="RM22W0E___0000JQT00000">
<ptxt>INSTALL REAR BUMPER ENERGY ABSORBER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 claws to install the rear bumper energy absorber.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4019X_01_0010" proc-id="RM22W0E___0000JQU00000">
<ptxt>INSTALL NO. 2 FRAME WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 13 clamps to install the No. 2 frame wire.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 connectors.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>