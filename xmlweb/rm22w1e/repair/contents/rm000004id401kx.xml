<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000004ID401KX" category="C" type-id="805CB" name-id="ES17HJ-001" from="201308">
<dtccode>P20CF</dtccode>
<dtcname>Exhaust Aftertreatment Fuel Injector "A" Stuck Open</dtcname>
<dtccode>P20D5</dtccode>
<dtcname>Exhaust Aftertreatment Fuel Injector "B" Stuck Open</dtcname>
<subpara id="RM000004ID401KX_05" type-id="60" category="03" proc-id="RM22W0E___00003ZW00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P200C (See page <xref label="Seep01" href="RM000004ID301KX_05"/>).</ptxt>
<table pgwide="1">
<title>P20CF (Bank 1), P20D5 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>PM forced regeneration</ptxt>
</entry>
<entry valign="middle">
<ptxt>AF Lambda B1S1, B2S1 is less than 0.85 for a certain time while the engine is running</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in exhaust fuel addition injector assembly circuit</ptxt>
</item>
<item>
<ptxt>Exhaust fuel addition injector assembly</ptxt>
</item>
<item>
<ptxt>Fuel filter element sub-assembly</ptxt>
</item>
</list1>
<list1 type="unordered">
<title>Common Inspection Item:</title>
<item>
<ptxt>Fuel injection system</ptxt>
</item>
<item>
<ptxt>Intake/exhaust system</ptxt>
</item>
<item>
<ptxt>After treatment control system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P20CF</ptxt>
</entry>
<entry valign="middle">
<ptxt>AF Lambda B1S1</ptxt>
<ptxt>Exhaust Temperature B1S2</ptxt>
<ptxt>Exhaust Temperature B1S3</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P20D5</ptxt>
</entry>
<entry valign="middle">
<ptxt>AF Lambda B2S1</ptxt>
<ptxt>Exhaust Temperature B2S2</ptxt>
<ptxt>Exhaust Temperature B2S3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000004ID401KX_06" type-id="64" category="03" proc-id="RM22W0E___00003ZX00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM monitors the air fuel ratio to detect whether the exhaust fuel addition injector assembly is stuck open.</ptxt>
<ptxt>If the air fuel ratio becomes too rich, the ECM determines that the exhaust fuel addition injector assembly is stuck open, illuminates the MIL and limits engine output.</ptxt>
<figure>
<graphic graphicname="A248903E03" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000004ID401KX_03" type-id="51" category="05" proc-id="RM22W0E___00003ZJ00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08XX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK08XX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000004ID401KX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004ID401KX_04_0016" proc-id="RM22W0E___00003ZT00001">
<testtitle>CHECK DTC OUTPUT (IN ADDITION TO DTC P20CF OR P20D5)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P20CF or P20D5 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P20CF or P20D5 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When DTC P20CF or P20D5 and the following DTCs are output, uncombusted fuel adhesion to the air fuel ratio sensor due to air fuel ratio sensor malfunction, fuel leaks, insufficient combustion due to malfunctions of parts (intake, exhaust or injection system), or an exhaust fuel addition injector assembly wiring malfunction is may be the cause. Therefore, diagnose the following DTCs first.</ptxt>
</item>
<item>
<ptxt>DTCs for the air fuel ratio sensor (for bank 1) are P2237, P2238, P2239, P2252 and P2253 (See page <xref label="Seep01" href="RM0000018WZ02WX"/>).</ptxt>
</item>
<item>
<ptxt>DTCs for the air fuel ratio sensor (for bank 2) are P2240, P2241, P2242, P2255 and P2256 (See page <xref label="Seep06" href="RM0000018WZ02WX"/>).</ptxt>
</item>
<item>
<ptxt>A DTC for fuel leakage is P0093 (See page <xref label="Seep02" href="RM0000012X906HX"/>).</ptxt>
</item>
<item>
<ptxt>DTCs an the exhaust fuel addition injector assembly wiring malfunction are P20CD and P20D3 (See page <xref label="Seep03" href="RM0000018X702YX"/>).</ptxt>
</item>
<item>
<ptxt>A DTC for an EGR valve flow malfunction is P0400 (See page <xref label="Seep04" href="RM0000041P804UX"/>).</ptxt>
</item>
<item>
<ptxt>DTCs for an EGR valve stuck being open are P042E and P045E (See page <xref label="Seep05" href="RM000004KMK01EX"/>).</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID401KX_04_0002" fin="false">A</down>
<right ref="RM000004ID401KX_04_0018" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0002" proc-id="RM22W0E___00003ZL00001">
<testtitle>CHECK FOR WHITE SMOKE (WHITE SMOKE AND ODOR)</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>If a fuel odor is noted from the white smoke during the inspection, there may be fuel leaks from the exhaust fuel addition injector assembly.</ptxt>
</atten3>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal or remove the EFI MAIN fuse for 1 minute or more.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and race it to 3000 rpm several times.</ptxt>
</test1>
<test1>
<ptxt>Check for white smoke in the exhaust gas. If found, check for a fuel odor or the smell of burning oil.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>White smoke and a fuel odor are present.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>White smoke and a smell of burning oil are present.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004ID401KX_04_0001" fin="false">A</down>
<right ref="RM000004ID401KX_04_0019" fin="false">B</right>
<right ref="RM000004ID401KX_04_0008" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0001" proc-id="RM22W0E___00003ZK00001">
<testtitle>PERFORM CONFIRMATION DRIVING PATTERN (WHITE SMOKE AND SENSOR &#13;
INSPECTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / AF Lambda B1S1, AF Lambda B2S1, Exhaust Temperature B1S3 and Exhaust Temperature B2S3.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle until the values of "Exhaust Temperature B1S3" and "Exhaust Temperature B2S3" become 300°C (572°F) or more to activate the air fuel ratio sensors.</ptxt>
<atten4>
<ptxt>Activation of the air fuel ratio sensor can be checked by confirming whether the value of "AF Lambda B1S1" or "AF Lambda B2S1" has changed from 1.99.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Check that there is no problem with driving performance such as hesitation, knocking, rough idling, insufficient power and acceleration stumbling, or white smoke in emissions.</ptxt>
<figure>
<graphic graphicname="A241156E02" width="7.106578999in" height="4.7836529in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The air fuel ratio sensor does not operate until the vehicle has been driven for a period of time with an exhaust gas temperature of 200°C (392°F) or more.</ptxt>
</item>
<item>
<ptxt>Perform diagnosis when the air fuel ratio sensor is operating and active.</ptxt>
</item>
<item>
<ptxt>The normal condition in the illustration is only an example. "AF Lambda B1S1" and "AF Lambda B2S1" change depending on driving conditions. If they change within the range of values more than 1.0, they are normal.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>There is no problem with driving performance such as hesitation, knocking, rough idling, insufficient power or acceleration stumbling, but excessive white smoke is present in the exhaust gas</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If white smoke is present in the exhaust gas without any driving performance problems such as hesitation, knocking, rough idling, insufficient power or acceleration stumbling, fuel injected due to a malfunction in the exhaust fuel addition injector assembly should be suspected as the cause of the white smoke.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID401KX_04_0005" fin="false">A</down>
<right ref="RM000004ID401KX_04_0008" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0005" proc-id="RM22W0E___00003ZM00001">
<testtitle>REPLACE EXHAUST FUEL ADDITION INJECTOR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<ptxt>When DTC P20CF is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the exhaust fuel addition injector assembly (for bank 1) (See page <xref label="Seep01" href="RM000003TI801FX"/>).</ptxt>
</item>
</list1>
<ptxt>When DTC P20D5 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the exhaust fuel addition injector assembly (for bank 2) (See page <xref label="Seep02" href="RM000003TI801FX"/>).</ptxt>
</item>
</list1>
</content6>
<res>
<down ref="RM000004ID401KX_04_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0006" proc-id="RM22W0E___00003ZN00001">
<testtitle>CLEAN FUEL FILTER CASE AND REPLACE FUEL FILTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clean the fuel filter case and replace the fuel filter.</ptxt>
<atten4>
<ptxt>Be sure to clean the inside of the fuel filter case as the fuel injectors may not operate properly if the fuel filter is installed with foreign matter remaining inside the fuel filter case.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID401KX_04_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0007" proc-id="RM22W0E___00003ZO00001">
<testtitle>PERFORM PM FORCED REGENERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK189X"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform PM forced regeneration (See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>).</ptxt>
<atten4>
<ptxt>When the exhaust fuel addition injector assembly is replaced, air may enter the fuel lines, leading to engine starting trouble. Therefore, perform forced regeneration and bleed the air from the fuel lines.</ptxt>
</atten4>
<spec>
<title>Result</title>
<specitem>
<ptxt>PM forced regeneration completes normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004ID401KX_04_0014" fin="true">OK</down>
<right ref="RM000004ID401KX_04_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0008" proc-id="RM22W0E___00003ZP00001">
<testtitle>CHECK FUEL INJECTION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the fuel injection system (See page <xref label="Seep01" href="RM000004KNK01GX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID401KX_04_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0009" proc-id="RM22W0E___00003ZQ00001">
<testtitle>CHECK INTAKE / EXHAUST SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the intake/exhaust system (See page <xref label="Seep01" href="RM000004KNL016X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID401KX_04_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0011" proc-id="RM22W0E___00003ZR00001">
<testtitle>CHECK AFTER TREATMENT CONTROL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the after treatment control system (See page <xref label="Seep01" href="RM000004KNM00YX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000004ID401KX_04_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0018" proc-id="RM22W0E___00003ZU00001">
<testtitle>GO TO DTC CHART</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform inspection and repair of relevant DTCs (See page <xref label="Seep01" href="RM0000031HW065X"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000004ID401KX_04_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0019" proc-id="RM22W0E___00003ZV00001">
<testtitle>CHECK AND REPLACE TURBOCHARGER SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for oil leaks and white smoke from the turbocharger sub-assembly (for bank 1 or bank 2) and perform any necessary repairs (See page <xref label="Seep01" href="RM00000416R0AIX"/>).</ptxt>
<atten4>
<ptxt>The cause of the problem may be a malfunction of the turbocharger sub-assembly, but it also may be exhaust valve stem oil leaks.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID401KX_04_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0013" proc-id="RM22W0E___00003ZS00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK189X"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform PM forced regeneration (See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID401KX_04_0014" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID401KX_04_0014">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>