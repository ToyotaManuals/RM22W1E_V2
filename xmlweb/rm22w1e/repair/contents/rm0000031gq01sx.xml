<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000Q" variety="S000Q">
<name>1UR-FE INTAKE / EXHAUST</name>
<ttl id="12010_S000Q_7C3J4_T00C7" variety="T00C7">
<name>INTAKE MANIFOLD</name>
<para id="RM0000031GQ01SX" category="A" type-id="80001" name-id="IE246-02" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM0000031GQ01SX_01" type-id="01" category="01">
<s-1 id="RM0000031GQ01SX_01_0027" proc-id="RM22W0E___00006NS00000">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000028RU03WX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0054" proc-id="RM22W0E___00006NW00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0034" proc-id="RM22W0E___00006NT00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0042" proc-id="RM22W0E___000013G00000">
<ptxt>REMOVE V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Raise the front of the V-bank cover to detach the 3 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<figure>
<graphic graphicname="A274415E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01SX_01_0043" proc-id="RM22W0E___000013H00000">
<ptxt>REMOVE AIR CLEANER CAP AND HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 PCV hose and No. 1 air hose.</ptxt>
<figure>
<graphic graphicname="A272589" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the mass air flow meter connector and detach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Loosen the hose clamp and remove the air cleaner cap and hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01SX_01_0052" proc-id="RM22W0E___000011Z00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01SX_01_0053" proc-id="RM22W0E___000011Y00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01SX_01_0045" proc-id="RM22W0E___000013S00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts and No. 1 engine under cover sub-assembly.</ptxt>
<figure>
<graphic graphicname="A178461" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01SX_01_0046" proc-id="RM22W0E___000013R00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the radiator cap and drain the coolant.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Loosen the 2 cylinder block drain cock plugs and drain the coolant from the engine.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 2 cylinder block drain cock plugs.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
<figure>
<graphic graphicname="A266163E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reservoir Cap</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01SX_01_0050" proc-id="RM22W0E___00004KB00000">
<ptxt>REMOVE NO. 5 WATER BY-PASS PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 water by-pass hoses and remove the 2 bolts and No. 5 water by-pass pipe.</ptxt>
<figure>
<graphic graphicname="A268559" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0047" proc-id="RM22W0E___00004JP00000">
<ptxt>REMOVE EGR VALVE BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 wire harness clamps and PCV hose clamp.</ptxt>
<figure>
<graphic graphicname="A268560" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and EGR valve bracket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0022" proc-id="RM22W0E___00004JQ00000">
<ptxt>REMOVE PCV HOSE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the PCV hose from the PCV pipe of the cylinder head cover LH and RH.</ptxt>
<figure>
<graphic graphicname="A268558" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and PCV hose.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0025" proc-id="RM22W0E___00004JR00000">
<ptxt>REMOVE AIR TUBE SUB-ASSEMBLY (w/ Secondary Air Injection System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 air injection system hose from the air switching valve.</ptxt>
<figure>
<graphic graphicname="A272661" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the manifold absolute pressure sensor connector and detach the clamp.</ptxt>
<figure>
<graphic graphicname="A271405" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and bracket.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and air tube.</ptxt>
<figure>
<graphic graphicname="A218662" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0006" proc-id="RM22W0E___00004JS00000">
<ptxt>REMOVE INTAKE MANIFOLD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Secondary Air Injection System:</ptxt>
<s3>
<ptxt>Disconnect the manifold absolute pressure sensor connector and detach the clamp.</ptxt>
<figure>
<graphic graphicname="A271404" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the 2 bolts and bracket.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Disconnect the No. 4 water by-pass hose.</ptxt>
<figure>
<graphic graphicname="A215281" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the throttle position sensor and throttle control motor connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the PCV valve hose.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the purge VSV connector.</ptxt>
<figure>
<graphic graphicname="A230824" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the purge line hose from the purge VSV.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the vacuum switching valve connector (for ACIS).</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt.</ptxt>
<figure>
<graphic graphicname="A271406" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 3 wire harness clamps from the 3 wire harness brackets.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the fuel tube from the fuel delivery pipe (See page <xref label="Seep01" href="RM0000028RU03WX"/>).</ptxt>
<figure>
<graphic graphicname="A218653" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the fuel tube from the No. 2 fuel delivery pipe (See page <xref label="Seep02" href="RM0000028RU03WX"/>).</ptxt>
<figure>
<graphic graphicname="A218654" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts, 8 bolts, intake manifold and 2 gaskets.</ptxt>
<figure>
<graphic graphicname="A230830" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Bolt</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Nut</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0017" proc-id="RM22W0E___00006NQ00000">
<ptxt>REMOVE FUEL TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and fuel tube from the intake manifold.</ptxt>
<figure>
<graphic graphicname="A218656" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0051" proc-id="RM22W0E___00006NV00000">
<ptxt>REMOVE BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and 2 brackets.</ptxt>
<figure>
<graphic graphicname="A218660" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0010" proc-id="RM22W0E___00006NN00000">
<ptxt>REMOVE V-BANK COVER BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and V-bank cover bracket.</ptxt>
<figure>
<graphic graphicname="A215290" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0011" proc-id="RM22W0E___00006NO00000">
<ptxt>REMOVE V-BANK COVER PIN</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the V-bank cover pin from the intake manifold.</ptxt>
<figure>
<graphic graphicname="A215291" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0049" proc-id="RM22W0E___00006NU00000">
<ptxt>REMOVE INTAKE FLANGE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and intake flange.</ptxt>
<figure>
<graphic graphicname="A268561" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0026" proc-id="RM22W0E___00006NR00000">
<ptxt>REMOVE STUD BOLT</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>If a stud bolt is deformed or its threads are damaged, replace it.</ptxt>
</atten3>
<s2>
<ptxt>Using an E5 "TORX" socket wrench, remove the 2 stud bolts.</ptxt>
<figure>
<graphic graphicname="A230826" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0016" proc-id="RM22W0E___00006NP00000">
<ptxt>REMOVE VACUUM SWITCHING VALVE ASSEMBLY (for ACIS)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 vacuum hoses and detach the 3 clamps.</ptxt>
<figure>
<graphic graphicname="A218659E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and vacuum switching valve.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0008" proc-id="RM22W0E___00006NM00000">
<ptxt>REMOVE PURGE VSV</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the purge line hose from the intake manifold.</ptxt>
<figure>
<graphic graphicname="A215294" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and purge VSV.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000031GQ01SX_01_0028" proc-id="RM22W0E___000015U00000">
<ptxt>REMOVE MANIFOLD ABSOLUTE PRESSURE SENSOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the manifold absolute pressure sensor connector.</ptxt>
<figure>
<graphic graphicname="A272561" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and manifold absolute pressure sensor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000031GQ01SX_01_0007" proc-id="RM22W0E___00006NL00000">
<ptxt>REMOVE THROTTLE BODY WITH MOTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts, throttle body with motor assembly and gasket.</ptxt>
<figure>
<graphic graphicname="A215292" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>