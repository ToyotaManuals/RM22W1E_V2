<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WO_T00PR" variety="T00PR">
<name>AIR CONDITIONING SYSTEM (for Manual Air Conditioning System)</name>
<para id="RM000002LJ404NX" category="C" type-id="305CT" name-id="ACC3O-04" from="201301" to="201308">
<dtccode>B1412</dtccode>
<dtcname>Ambient Temperature Sensor Circuit</dtcname>
<subpara id="RM000002LJ404NX_01" type-id="60" category="03" proc-id="RM22W0E___0000HBD00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The cooler thermistor (ambient temperature sensor) is installed in the front part of the cooler condenser assembly. This sensor is connected to the air conditioning amplifier assembly and detects fluctuations in the ambient temperature. The sensor sends a signal to the air conditioning amplifier assembly. The resistance of the cooler thermistor (ambient temperature sensor) changes in accordance with the ambient temperature. As the temperature decreases, the resistance increases. As the temperature increases, the resistance decreases.</ptxt>
<ptxt>The air conditioning amplifier assembly applies a voltage (5 V) to the cooler thermistor (ambient temperature sensor) and reads voltage changes as changes in the resistance of the cooler thermistor (ambient temperature sensor).</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1412</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>An open or short in the cooler thermistor (ambient temperature sensor) circuit is detected for 8.5 minutes or more.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Cooler thermistor (ambient temperature sensor)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002LJ404NX_02" type-id="32" category="03" proc-id="RM22W0E___0000HBE00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E193111E23" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002LJ404NX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002LJ404NX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002LJ404NX_05_0001" proc-id="RM22W0E___0000HBF00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (AMBIENT TEMP SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the cooler thermistor (ambient temperature sensor) is functioning properly (See page <xref label="Seep01" href="RM000002LIV02PX"/>).</ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.73in"/>
<colspec colname="COL3" colwidth="1.81in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ambient Temp Sensor</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Cooler thermistor (ambient temperature sensor) /</ptxt>
<ptxt>Min: -23.3°C (-9.94°F)</ptxt>
<ptxt>Max: 65.95°C (150.71°F)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Actual ambient temperature displayed</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Open in the circuit: -23.3°C (-9.94°F).</ptxt>
<ptxt>Short in the circuit: 65.95°C (150.71°F).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition column.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting according to problem symptoms table)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting according to the DTC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002LJ404NX_05_0012" fin="true">A</down>
<right ref="RM000002LJ404NX_05_0004" fin="true">B</right>
<right ref="RM000002LJ404NX_05_0002" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000002LJ404NX_05_0002" proc-id="RM22W0E___0000HBG00000">
<testtitle>INSPECT COOLER THERMISTOR (AMBIENT TEMPERATURE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the cooler thermistor (ambient temperature sensor) (See page <xref label="Seep01" href="RM000002VYK02HX"/>).</ptxt>
<figure>
<graphic graphicname="E184843E02" width="2.775699831in" height="5.787629434in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="10" valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>10°C (50°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.00 to 3.73 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>15°C (59°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.45 to 2.88 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.95 to 2.30 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>25°C (77°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.60 to 1.80 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>30°C (86°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.28 to 1.47 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>35°C (95°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.00 to 1.22 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>40°C (104°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.80 to 1.00 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>45°C (113°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.65 to 0.85 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>50°C (122°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.50 to 0.70 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>55°C (131°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.44 to 0.60 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>60°C (140°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.36 to 0.50 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sensing Portion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Resistance</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Allowable Range</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Even slightly touching the sensor may change the resistance value. Be sure to hold the connector of the sensor.</ptxt>
</item>
<item>
<ptxt>When measuring, the sensor temperature must be the same as the ambient temperature.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>As the temperature increases, the resistance decreases (refer to the graph).</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002LJ404NX_05_0003" fin="false">OK</down>
<right ref="RM000002LJ404NX_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LJ404NX_05_0003" proc-id="RM22W0E___0000HBH00000">
<testtitle>CHECK HARNESS AND CONNECTOR (COOLER THERMISTOR - AIR CONDITIONING AMPLIFIER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A14 cooler thermistor (ambient temperature sensor) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E81 air conditioning amplifier assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A14-1 - E81-5 (TAM)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A14-2 - E81-13 (SG-2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-5 (TAM) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E81-13 (SG-2) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LJ404NX_05_0011" fin="true">OK</down>
<right ref="RM000002LJ404NX_05_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LJ404NX_05_0004">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LJ404NX_05_0006">
<testtitle>REPLACE COOLER THERMISTOR (AMBIENT TEMPERATURE SENSOR)<xref label="Seep01" href="RM000002VYK02HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LJ404NX_05_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002LJ404NX_05_0011">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LJ404NX_05_0012">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002LIQ023X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>