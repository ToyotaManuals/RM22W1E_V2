<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM0000013HE04MX" category="C" type-id="305O0" name-id="ESUB2-04" from="201301" to="201308">
<dtccode>P1613</dtccode>
<dtcname>Secondary Air Injection Driver Malfunction</dtcname>
<subpara id="RM0000013HE04MX_01" type-id="60" category="03" proc-id="RM22W0E___00001QD00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0412 (See page <xref label="Seep01" href="RM0000012FY046X_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.28in"/>
<colspec colname="COL2" colwidth="2.89in"/>
<colspec colname="COL3" colwidth="2.91in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>P1613</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition (1) or (2) is met:</ptxt>
<ptxt>(1) All conditions are met for 3 seconds or more (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>Either the air pump or the air switching valve is not operating.</ptxt>
</item>
<item>
<ptxt>Diagnostic signal from the air injection control driver is 80%.</ptxt>
</item>
<item>
<ptxt>Battery voltage is 8 V or higher.</ptxt>
</item>
</list1>
<ptxt>(2) Both conditions are met for 3 seconds or more (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>Battery voltage is 8 V or higher.</ptxt>
</item>
<item>
<ptxt>Diagnostic signal from the air injection control driver is abnormal (duty signal other than 0, 20, 40, 80 or 100%).</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air injection control driver</ptxt>
</item>
<item>
<ptxt>Open in air injection control driver ground circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="nonmark">
<item>
<ptxt>All conditions met for 3 seconds or more (1 trip detection logic):</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Air injection system is operating (the air switching valve and air pump are on).</ptxt>
</item>
<item>
<ptxt>Diagnostic signal from the air injection control driver is 0%.</ptxt>
</item>
<item>
<ptxt>Battery voltage is 8 V or higher.</ptxt>
</item>
</list2>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in diagnostic information signal circuit (Air injection control driver - ECM)</ptxt>
</item>
<item>
<ptxt>Open or short in air pump and air switching valve command signal circuit (Air injection control driver - ECM)</ptxt>
</item>
<item>
<ptxt>Open in air injection control driver ground circuit</ptxt>
</item>
<item>
<ptxt>Air injection control driver</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="nonmark">
<item>
<ptxt>Both conditions met for 3 seconds or more (1 trip detection logic):</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Battery voltage is 8 V or higher.</ptxt>
</item>
<item>
<ptxt>Diagnostic signal from the air injection control driver is 100%.</ptxt>
</item>
</list2>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air injection control driver +B circuit</ptxt>
</item>
<item>
<ptxt>Open in diagnostic information signal circuit (Air injection control driver - ECM)</ptxt>
</item>
<item>
<ptxt>Air injection control driver</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000013HE04MX_02" type-id="64" category="03" proc-id="RM22W0E___00001QE00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates an open or short circuit in the circuit containing the air pump of the secondary air injection system. The air injection control driver performs diagnosis of the air pump, air switching valve and itself, and sends the results of this diagnosis to the ECM as a duty signal. When the ECM receives a signal indicating a malfunction in the air pump, air switching valve or air injection control driver, it immediately illuminates the MIL and stores a DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM0000013HE04MX_10" type-id="73" category="03" proc-id="RM22W0E___00001QN00000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>This Secondary Air Injection Check only allows technicians to operate the Secondary air injection system for a maximum of 5 seconds. Furthermore, the check can only be performed up to 4 times per trip. If the test is repeated, intervals of at least 30 seconds are required between checks. While Secondary air injection system operation using the GTS is prohibited, the GTS display indicates the prohibition (WAIT or ERROR). If ERROR is displayed on the GTS during the test, stop the engine for 10 minutes, and then try again.</ptxt>
</item>
<item>
<ptxt>Performing Secondary Air Injection Check repeatedly may cause damage to the secondary air injection system. If it is necessary to repeat the check, leave an interval of several minutes between System Check operations to prevent the system from overheating.</ptxt>
</item>
<item>
<ptxt>When performing the Secondary Air Injection Check operation after the battery cable has been reconnected, wait for 7 minutes with the engine switch turned on (IG) or the engine running.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off when the Secondary Air Injection Check operation finishes.</ptxt>
</item>
</list1>
</atten3>
<list1 type="ordered">
<item>
<ptxt>Start the engine and warm it up.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off.</ptxt>
</item>
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG).</ptxt>
</item>
<item>
<ptxt>Turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation) (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / Secondary air injection check / Automatic Mode.</ptxt>
</item>
<item>
<ptxt>Start the engine after the GTS initialization is finished.</ptxt>
</item>
<item>
<ptxt>Perform the System Check operation by pressing ENTER (Next).</ptxt>
</item>
<item>
<ptxt>Perform the following to confirm the Secondary air injection system pending codes: Press ENTER (Exit).</ptxt>
</item>
<item>
<ptxt>Check for pending DTCs.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No pending DTC is output.</ptxt>
</specitem>
</spec>
</item>
<item>
<ptxt>After "Secondary air injection check" is completed, check for All Readiness by entering the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P1613.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Turn the engine switch off.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM0000013HE04MX_07" type-id="32" category="03" proc-id="RM22W0E___00001QF00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0412 (See page <xref label="Seep01" href="RM0000012FY046X_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000013HE04MX_08" type-id="51" category="05" proc-id="RM22W0E___00001QG00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>By using the GTS to perform the Secondary Air Injection Check operation in the System Check, the air-fuel ratio and the pressure in the secondary air injection system passage can be checked while the secondary air injection system is operating. This helps technicians to troubleshoot the system when it malfunctions.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM0000013HE04MX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000013HE04MX_09_0005" proc-id="RM22W0E___00001QI00000">
<testtitle>CHECK AIR INJECTION CONTROL DRIVER (POWER SOURCE CIRCUIT)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A200902E48" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the air injection control driver connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C61-5 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V (near battery voltage)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Air Injection Control Driver)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000013HE04MX_09_0004" fin="false">OK</down>
<right ref="RM0000013HE04MX_09_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0004" proc-id="RM22W0E___00001QH00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR INJECTION CONTROL DRIVER - ECM, BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the air injection control driver connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" align="right" colwidth="3.54in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C61-4 (SIP) - C46-54 (AIRP)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-3 (SIV) - C46-28 (AIRV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-2 (DI) - C46-30 (AIDI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-4 (SIP) or C46-54 (AIRP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-3 (SIV) or C46-28 (AIRV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-2 (DI) or C46-30 (AIDI) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" align="right" colwidth="3.54in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C61-4 (SIP) - C45-54 (AIRP)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-3 (SIV) - C45-28 (AIRV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-2 (DI) - C45-30 (AIDI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-4 (SIP) or C45-54 (AIRP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-3 (SIV) or C45-28 (AIRV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C61-2 (DI) or C45-30 (AIDI) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000013HE04MX_09_0006" fin="false">OK</down>
<right ref="RM0000013HE04MX_09_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0006" proc-id="RM22W0E___00001QJ00000">
<testtitle>INSPECT AIR INJECTION CONTROL DRIVER (DI TERMINAL VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A200902E49" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the air injection control driver connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC3" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C61-2 (DI) - C61-1 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="bottom" align="center">
<ptxt>11 to 14 V (near battery voltage)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Air Injection Control Driver)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000013HE04MX_09_0020" fin="false">OK</down>
<right ref="RM0000013HE04MX_09_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0020" proc-id="RM22W0E___00001QM00000">
<testtitle>PERFORM ACTIVE TEST USING GTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air injection control driver connector.</ptxt>
</test1>
<figure>
<graphic graphicname="A229922E32" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Connect terminals DI and SIV of the wire harness connector for the air injection control driver.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Air Injection Control Driver</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Air Injection Control Driver)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / Secondary Air Injection Check / Manual Mode / AIR PUMP: ON, ASV 1: OPEN, ASV 2: OPEN.</ptxt>
<atten4>
<ptxt>When Manual Mode is selected, the GTS initialization (atmospheric pressure measurement) is performed automatically. The initialization takes 10 seconds. After the initialization, AIR PUMP and ASV operation can be selected.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Perform the secondary air injection system forced operation while the engine is idling.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage between the SIV and E terminals of the ECM connector when the secondary air injection system is ON and OFF.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Performing Secondary Air Injection Check repeatedly may cause damage to the secondary air injection system. If it is necessary to repeat the check, leave an interval of several minutes between System Check operations to prevent the system from overheating.</ptxt>
</item>
<item>
<ptxt>When performing the Secondary Air Injection Check operation after the battery cable has been reconnected, wait for 7 minutes with the engine switch turned on (IG) or the engine running.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off when the Secondary Air Injection Check operation finishes.</ptxt>
</item>
</list1>
</atten3>
<spec>
<title>Standard Voltage</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>C61-3 (SIV) - C61-1 (E)</ptxt>
</entry>
<entry>
<ptxt>AIR PUMP: ON, ASV: OPEN</ptxt>
</entry>
<entry>
<ptxt>0.5 to 2 V</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C61-3 (SIV) - C61-1 (E)</ptxt>
</entry>
<entry>
<ptxt>AIR PUMP: OFF, ASV: CLOSE</ptxt>
</entry>
<entry>
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<figure>
<graphic graphicname="A229922E33" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Connect terminals DI and SIP of the wire harness connector for the air injection control driver.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Air Injection Control Driver</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Air Injection Control Driver)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / Secondary Air Injection Check / Manual Mode / AIR PUMP: ON, ASV 1: OPEN, ASV 2: OPEN.</ptxt>
<atten4>
<ptxt>When Manual Mode is selected, the GTS initialization (atmospheric pressure measurement) is performed automatically. The initialization takes 10 seconds. After the initialization, AIR PUMP and ASV operation can be selected.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Perform the secondary air injection system forced operation while the engine is idling.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage between the SIP and E terminals of the ECM connector when the secondary air injection system is ON and OFF.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Performing Secondary Air Injection Check repeatedly may cause damage to the secondary air injection system. If it is necessary to repeat the check, leave an interval of several minutes between System Check operations to prevent the system from overheating.</ptxt>
</item>
<item>
<ptxt>When performing the Secondary Air Injection Check operation after the battery cable has been reconnected, wait for 7 minutes with the engine switch turned on (IG) or the engine running.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off when the Secondary Air Injection Check operation finishes.</ptxt>
</item>
</list1>
</atten3>
<spec>
<title>Standard Voltage</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>C61-4 (SIP) - C61-1 (E)</ptxt>
</entry>
<entry>
<ptxt>AIR PUMP: ON, ASV: OPEN</ptxt>
</entry>
<entry>
<ptxt>0.5 to 2 V</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C61-4 (SIP) - C61-1 (E)</ptxt>
</entry>
<entry>
<ptxt>AIR PUMP: OFF, ASV: CLOSE</ptxt>
</entry>
<entry>
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the air injection control driver connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013HE04MX_09_0015" fin="false">OK</down>
<right ref="RM0000013HE04MX_09_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0015" proc-id="RM22W0E___00001QK00000">
<testtitle>REPLACE AIR INJECTION CONTROL DRIVER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air injection control driver (See page <xref label="Seep01" href="RM00000398Q009X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013HE04MX_09_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0017" proc-id="RM22W0E___00001QL00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / Secondary air injection check / Automatic Mode.</ptxt>
</test1>
<test1>
<ptxt>Start the engine after the GTS initialization is finished.</ptxt>
</test1>
<test1>
<ptxt>Perform the System Check operation by pressing ENTER (Next).</ptxt>
</test1>
<test1>
<ptxt>After operating the secondary air injection system, perform the following to confirm the secondary air injection system pending codes: Press the Exit button.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC / Pending.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When performing the Secondary Air Injection Check operation after the battery cable has been reconnected, wait for 7 minutes with the engine switch turned on (IG) or the engine running.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off when the Secondary Air Injection Check operation finishes.</ptxt>
</item>
</list1>
</atten3>
</test1>
</content6>
<res>
<down ref="RM0000013HE04MX_09_0011" fin="true">A</down>
<right ref="RM0000013HE04MX_09_0022" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0018">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0013">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0021">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0022">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000013HE04MX_09_0011">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>