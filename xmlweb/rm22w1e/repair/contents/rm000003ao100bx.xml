<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3VZ_T00P2" variety="T00P2">
<name>HEATER WATER PUMP (w/ Viscous Heater)</name>
<para id="RM000003AO100BX" category="A" type-id="30014" name-id="ACGCD-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000003AO100BX_01" type-id="01" category="01">
<s-1 id="RM000003AO100BX_01_0001" proc-id="RM22W0E___0000GRQ00000">
<ptxt>INSTALL IDLER PULLEY BRACKET (w/ Belt Tension Side)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154425" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the bracket with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>48.5</t-value1>
<t-value2>495</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0002" proc-id="RM22W0E___0000GRR00000">
<ptxt>INSTALL IDLER PULLEY BRACKET</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154427" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the bracket with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>48.5</t-value1>
<t-value2>495</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0003" proc-id="RM22W0E___000059P00000">
<ptxt>INSTALL VISCOUS WITH MAGNET CLUTCH HEATER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154426" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the heater assembly with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>48.5</t-value1>
<t-value2>495</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 2 heater hoses.</ptxt>
</s2>
<s2>
<ptxt>Using pliers, grip the claws of the clips and slide the 2 clips.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector and attach the clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0004" proc-id="RM22W0E___0000GRS00000">
<ptxt>INSTALL IDLER PULLEY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154424" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the 2 idler pulleys with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>48.5</t-value1>
<t-value2>495</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0027" proc-id="RM22W0E___0000GS000000">
<ptxt>INSTALL RADIATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radiator assembly (See page <xref label="Seep01" href="RM000002BGI039X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0028" proc-id="RM22W0E___000033N00000">
<ptxt>INSTALL RADIATOR RESERVE TANK ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radiator reservoir with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A174738E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the 2 hoses to the upper radiator tank and water inlet.</ptxt>
<atten4>
<ptxt>Make sure the directions of the hose clamps are as shown in the illustration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003AO100BX_01_0029" proc-id="RM22W0E___000054G00000">
<ptxt>INSTALL FAN SHROUD WITH FAN
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the shroud together with the fluid coupling fan between the radiator and engine components.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Temporarily install the fluid coupling fan to the fan bracket with the 4 nuts.</ptxt>
<figure>
<graphic graphicname="A179253" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tighten the nuts as much as possible by hand.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the shroud claws to the radiator.</ptxt>
<figure>
<graphic graphicname="A174737" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Install the shroud with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the 4 nuts of the fluid coupling fan.</ptxt>
<figure>
<graphic graphicname="A179253" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003AO100BX_01_0030" proc-id="RM22W0E___000054H00000">
<ptxt>INSTALL OIL COOLER HOSE (w/ Air Cooled Transmission Oil Cooler)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the oil cooler tube to the fan shroud with the bolt labeled A. Install the bolt labeled B. Then tighten the bolt labeled A to the specified torque.</ptxt>
<figure>
<graphic graphicname="C167866E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the inlet No. 4 oil cooler hose as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C175576E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure the pinching portion of each clip is facing the direction shown in the illustration.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the inlet No. 2 and No. 3 oil cooler hoses as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C175577" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>Make sure the pinching portion of each clip is facing the direction shown in the illustration.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the inlet and outlet No. 1 oil cooler hoses as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C167862E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure the pinching portion of each clip is facing the direction shown in the illustration.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Pass the hose through the flexible hose clamp and close the clamp shown in the illustration.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003AO100BX_01_0020">
<ptxt>INSTALL NO. 2 RADIATOR HOSE</ptxt>
</s-1>
<s-1 id="RM000003AO100BX_01_0021">
<ptxt>INSTALL NO. 1 RADIATOR HOSE</ptxt>
</s-1>
<s-1 id="RM000003AO100BX_01_0031" proc-id="RM22W0E___00005JJ00000">
<ptxt>INSTALL VANE PUMP ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to a new O-ring, and install it to the vane pump.</ptxt>
<figure>
<graphic graphicname="C172309" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the vane pump with 2 new nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003AO100BX_01_0032" proc-id="RM22W0E___000033O00000">
<ptxt>INSTALL VANE PUMP OIL RESERVOIR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the vane pump oil reservoir to the bracket.</ptxt>
<figure>
<graphic graphicname="A177429" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003AO100BX_01_0033" proc-id="RM22W0E___000033K00000">
<ptxt>INSTALL V-RIBBED BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the V-ribbed belt as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A174851" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Temporarily install the lock nut, and turn the bolt clockwise.</ptxt>
<figure>
<graphic graphicname="A174848" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Using a belt tension gauge, inspect the belt tension.</ptxt>
<figure>
<graphic graphicname="A271744E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Belt Tension</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New belt</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 to 35°C (41 to 95°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>550 to 800 N (56 to 82 kgf, 123.6 to 179.8 lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Used belt</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 to 35°C (41 to 95°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>300 to 500 N (31 to 51 kgf, 67.4 to 112.4 lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Measuring Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When measuring the tension of a new belt, measure the tension immediately after installing it to the engine but before starting the engine.</ptxt>
</item>
<item>
<ptxt>A "new belt" is a belt which has been used for less than 5 minutes on a running engine.</ptxt>
</item>
<item>
<ptxt>A "used belt" is a belt which has been used on a running engine for 5 minutes or more.</ptxt>
</item>
<item>
<ptxt>After installing a new belt, run the engine for approximately 5 minutes and then recheck the tension.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Tighten the nut.</ptxt>
<figure>
<graphic graphicname="A181677" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Check that the belt fits properly in the ribbed grooves.</ptxt>
<figure>
<graphic graphicname="A185945E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Check with your hand to confirm that the belt has not slipped out of the groove on the bottom of the pulley.</ptxt>
<ptxt>If it has slipped out, replace the V-ribbed belt. Install a new V-ribbed belt.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003AO100BX_01_0007" proc-id="RM22W0E___0000GRT00000">
<ptxt>INSTALL INTAKE AIR CONNECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the intake air connector to the No. 1 and No. 2 air cleaner pipes.</ptxt>
<figure>
<graphic graphicname="A174710" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the connector with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the 2 hose clamps.</ptxt>
<torque>
<torqueitem>
<t-value1>6.3</t-value1>
<t-value2>64</t-value2>
<t-value3>56</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 3 wire harness clamps.</ptxt>
<figure>
<graphic graphicname="A174749E03" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the 2 connectors to the water temperature sensor and viscous heater.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0008" proc-id="RM22W0E___0000GRU00000">
<ptxt>TEMPORARILY INSTALL NO. 1 AIR CLEANER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the air cleaner hose to the intake air connector.</ptxt>
<figure>
<graphic graphicname="A177380" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0009" proc-id="RM22W0E___0000GRV00000">
<ptxt>INSTALL AIR CLEANER CAP SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the protrusion of the air cleaner cap and concave portion of the air cleaner hose.</ptxt>
<figure>
<graphic graphicname="A174698" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the air cleaner cap to the air cleaner hose, and install the air cleaner cap with the 3 clamps.</ptxt>
<figure>
<graphic graphicname="A177377" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Tighten the 2 hose clamps and connect the connector.</ptxt>
<torque>
<torqueitem>
<t-value1>2.5</t-value1>
<t-value2>25</t-value2>
<t-value3>22</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0010" proc-id="RM22W0E___0000GRW00000">
<ptxt>INSTALL NO. 1 ENGINE COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the engine cover with the 2 nuts.</ptxt>
<figure>
<graphic graphicname="A174701" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0034" proc-id="RM22W0E___000054I00000">
<ptxt>INSTALL NO. 3 ENGINE ROOM WIRE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 3 engine room wire to the main battery and sub-battery positive terminals with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>7.6</t-value1>
<t-value2>77</t-value2>
<t-value3>67</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the No. 3 engine room wire with the 4 clamps.</ptxt>
<figure>
<graphic graphicname="A174837" width="7.106578999in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003AO100BX_01_0035" proc-id="RM22W0E___000047Y00000">
<ptxt>INSTALL UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radiator support seal with the 7 clips.</ptxt>
<figure>
<graphic graphicname="B180890E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003AO100BX_01_0036" proc-id="RM22W0E___000032D00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the engine air bleed cap.</ptxt>
<figure>
<graphic graphicname="A179103" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect a clear hose to the engine air bleed pipe.</ptxt>
<figure>
<graphic graphicname="A185226" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a wrench, remove the vent plug.</ptxt>
<figure>
<graphic graphicname="A177426" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Fill the radiator with TOYOTA SLLC to the radiator reservoir filler neck.</ptxt>
<atten4>
<ptxt>Pour TOYOTA SLLC until it spills out of the engine air bleed pipe.</ptxt>
</atten4>
<figure>
<graphic graphicname="A177425" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Capacity (for Automatic Transmission)</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>Front heater only</ptxt>
</entry>
<entry valign="middle">
<ptxt>14.8 liters (15.6 US qts, 13.0 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Front heater and rear heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>17.6 liters (18.6 US qts, 15.5 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Front heater with viscous heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>15.2 liters (16.1 US qts, 13.4 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Front heater and rear heater with viscous heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>18.0 liters (19.0 US qts, 15.4 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard capacity (for Manual Transmission)</title>
<specitem>
<ptxt>15.4 liters (16.3 US qts, 13.5 Imp. qts)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the vent plug.</ptxt>
<figure>
<graphic graphicname="A177426" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>2.0</t-value1>
<t-value2>20</t-value2>
<t-value3>18</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not tighten the plug to 5.0 N*m (51 kgf*cm, 44 in.*lbf) or more, as the plug will be damaged.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Disconnect the clear hose from the engine air bleed pipe.</ptxt>
<figure>
<graphic graphicname="A185226" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the engine air bleed cap when coolant comes out.</ptxt>
<figure>
<graphic graphicname="A179103" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the radiator reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
<atten3>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the FULL line.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Maintain an engine speed of 3000 rpm for approximately 10 minutes so that the thermostat opens and air bleeding is performed.</ptxt>
<atten2>
<list1 type="unordered">
<title>When pressing the radiator hoses:</title>
<item>
<ptxt>Wear protective gloves.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the radiator fan</ptxt>
</item>
</list1>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Pay attention to the needle of the water temperature meter. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand, and checking when the engine coolant starts to flow inside the hose.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Stop the engine, and wait until the engine coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Check that the coolant level is between the FULL and LOW lines.</ptxt>
<figure>
<graphic graphicname="A174740" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If the coolant level is above the FULL line, drain coolant so that the coolant level is between the FULL and LOW lines.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003AO100BX_01_0037" proc-id="RM22W0E___000032F00000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover with the 10 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003AO100BX_01_0013" proc-id="RM22W0E___0000GRX00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Standard:</ptxt>
<ptxt>Install the front fender splash shield (See page <xref label="Seep01" href="RM0000038JV01HX_01_0013"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Winch:</ptxt>
<ptxt>Install the front fender splash shield (See page <xref label="Seep02" href="RM0000038JV01GX_01_0013"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0014" proc-id="RM22W0E___0000GRY00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Standard:</ptxt>
<ptxt>Install the front fender splash shield (See page <xref label="Seep01" href="RM0000038JV01HX_01_0014"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Winch:</ptxt>
<ptxt>Install the front fender splash shield (See page <xref label="Seep02" href="RM0000038JV01GX_01_0014"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0015" proc-id="RM22W0E___0000GRZ00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003AO100BX_01_0038" proc-id="RM22W0E___000032E00000">
<ptxt>CHECK FOR ENGINE COOLANT LEAKS
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<figure>
<graphic graphicname="A174739" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester to the radiator reservoir.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 17.8 psi), and check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and water pump for leaks.</ptxt>
<ptxt>If no external leaks are found, check the cylinder block and cylinder head.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>