<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MT_T00FW" variety="T00FW">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1UR-FE)</name>
<para id="RM000000W7B0PQX" category="D" type-id="303FC" name-id="AT0030-322" from="201301">
<name>HYDRAULIC TEST</name>
<subpara id="RM000000W7B0PQX_z0" proc-id="RM22W0E___00008LS00000">
<content5 releasenbr="1">
<step1>
<ptxt>PERFORM HYDRAULIC TEST</ptxt>
<step2>
<ptxt>Measure the line pressure.</ptxt>
<atten2>
<ptxt>The line pressure test should always be performed with at least 2 people. One person should observe the condition of the wheels and wheel chocks while the other is performing the test.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the test while the ATF (Automatic Transmission Fluid) temperature is between 50 and 80°C (122 and 176°F).</ptxt>
</item>
<item>
<ptxt>Be careful to prevent the hose of SST from interfering with the exhaust pipe.</ptxt>
</item>
<item>
<ptxt>This check must be conducted after checking and adjusting the engine.</ptxt>
</item>
<item>
<ptxt>Perform the test with the A/C off.</ptxt>
</item>
<item>
<ptxt>When conducting the stall test, do not continue for more than 5 seconds.</ptxt>
</item>
</list1>
</atten3>
<step3>
<ptxt>Warm up the ATF (Automatic Transmission Fluid).</ptxt>
</step3>
<step3>
<ptxt>Turn the engine switch off.</ptxt>
</step3>
<step3>
<ptxt>Lift the vehicle up.</ptxt>
</step3>
<step3>
<ptxt>Remove the test plug from the transmission case and connect SST.</ptxt>
<figure>
<graphic graphicname="C219461E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09992-00095</s-number>
<s-subnumber>09992-00231</s-subnumber>
<s-subnumber>09992-00271</s-subnumber>
</sstitem>
</sst>
</step3>
<step3>
<ptxt>Lower the vehicle.</ptxt>
</step3>
<step3>
<ptxt>Fully apply the parking brake and chock the 4 wheels.</ptxt>
</step3>
<step3>
<ptxt>Start the engine and check the idling speed.</ptxt>
</step3>
<step3>
<ptxt>Keep your left foot pressed firmly on the brake pedal and move the shift lever to D.</ptxt>
</step3>
<step3>
<ptxt>Fully depress the accelerator pedal with your right foot. Quickly read the highest line pressure when the engine speed reaches the stall speed.</ptxt>
</step3>
<step3>
<ptxt>In the same manner, perform the test with the shift lever in R.</ptxt>
<table pgwide="1">
<title>Specified Line Pressure</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R Position</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Stall speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1370 to 1530 kPa</ptxt>
<ptxt>(14.0 to 15.6 kgf/cm<sup>2</sup>, 199 to 222 psi)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1540 to 1750 kPa</ptxt>
<ptxt>(15.7 to 17.8 kgf/cm<sup>2</sup>, 223 to 254 psi)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Evaluation</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Problem</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Possible Cause</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Measured values at both positions are higher than specified pressure</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift solenoid valve SLT defective</ptxt>
</item>
<item>
<ptxt>Regulator valve defective</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Measured values at both positions are below specified pressure</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift solenoid valve SLT defective</ptxt>
</item>
<item>
<ptxt>Regulator valve defective</ptxt>
</item>
<item>
<ptxt>Oil pump defective</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pressure is low in D position only</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>D position circuit fluid leak</ptxt>
</item>
<item>
<ptxt>Clutch No. 1 (C1) defective</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pressure is low in R position only</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>R position circuit fluid leak</ptxt>
</item>
<item>
<ptxt>Clutch No. 3 (C3) defective</ptxt>
</item>
<item>
<ptxt>Brake No. 4 (B4) defective</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>