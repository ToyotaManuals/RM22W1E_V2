<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SC_T00LF" variety="T00LF">
<name>CAN COMMUNICATION SYSTEM (for LHD)</name>
<para id="RM000003A8Z005X" category="T" type-id="3001H" name-id="NW00R-05" from="201301" to="201308">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM000003A8Z005X_z0" proc-id="RM22W0E___0000D8200000">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses and relays related to this system before inspecting the suspected areas below.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Result List of Check CAN Bus Line</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>The result of "How to Proceed with Troubleshooting" is "Open in CAN Main Wire".</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Open in CAN Main Wire</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002L8204DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>The result of "How to Proceed with Troubleshooting" is "Short in CAN Bus Lines".</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Short in CAN Bus Lines</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002L7Z0AFX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>The result of "How to Proceed with Troubleshooting" is "Short to +B in CAN Bus Line".</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Short to B+ in CAN Bus Line</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002L80035X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>The result of "How to Proceed with Troubleshooting" is "Short to GND in CAN Bus Line".</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Short to GND in CAN Bus Line</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002L80036X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>The result of "How to Proceed with Troubleshooting" is "Open in One Side of CAN Branch Line".</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Open in One Side of CAN Branch Line</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001YZM05LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Communication Stop Mode Table</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.83in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.13in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row>
<entry colsep="1" valign="middle">
<ptxt>"Skid Control (ABS/VSC/TRAC)" not displayed on the intelligent tester.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Skid Control ECU Communication Stop Mode</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001V2C06AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>"Clearance Warning (Clearance Sonar)" not displayed on the intelligent tester. (w/ TOYOTA Parking Assist-sensor System)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Clearance Warning ECU Communication Stop Mode</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001V2C06BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"Air Conditioning Amplifier" not displayed on the intelligent tester.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Air Conditioning Amplifier Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RTD03GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"CAN Gateway" not displayed on the intelligent tester. (w/ Network Gateway ECU)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Gateway ECU Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000039LU03AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"ECM (Engine)" not displayed on the intelligent tester.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>ECM Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSZ04KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"Main Body" not displayed on the intelligent tester.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Main Body ECU Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RTG04MX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"Combination Meter" not displayed on the intelligent tester.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Combination Meter ECU Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001TNA04CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"Airbag" not displayed on the intelligent tester. (w/ Airbag System)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Center Airbag Sensor Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001TN904EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>"Headlight swivel (AFS)" not displayed on the intelligent tester. (w/ Dynamic Headlight Auto Leveling)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>AFS ECU Communication Stop Mode</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000552M002X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>"Four Wheel Drive Control" not displayed on the intelligent tester.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>4WD Control ECU Communication Stop Mode</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RTF00PX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"Display and Navigation (AVN)" not displayed on the intelligent tester. (w/ Navigation System)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Multi-media Module Receiver Assembly Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002TGF04CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>