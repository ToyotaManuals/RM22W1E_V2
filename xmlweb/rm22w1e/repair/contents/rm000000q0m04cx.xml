<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CU_T005X" variety="T005X">
<name>THROTTLE BODY</name>
<para id="RM000000Q0M04CX" category="A" type-id="30014" name-id="ES11GC-001" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000000Q0M04CX_01" type-id="01" category="01">
<s-1 id="RM000000Q0M04CX_01_0001" proc-id="RM22W0E___000011M00000">
<ptxt>INSTALL THROTTLE BODY WITH MOTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the throttle body with motor with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the throttle position sensor and throttle control motor connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 4 water by-pass hose.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 5 water by-pass hose.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000Q0M04CX_01_0033" proc-id="RM22W0E___00000Z100000">
<ptxt>INSTALL NO. 1 AIR CLEANER HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 air cleaner hose with the 2 hose clamps.</ptxt>
<figure>
<graphic graphicname="A267635E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<torqueitem>
<t-value1>2.5</t-value1>
<t-value2>25</t-value2>
<t-value3>22</t-value3>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Groove</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Top</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the vacuum hose and No. 2 PCV hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000Q0M04CX_01_0019" proc-id="RM22W0E___000011N00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add engine coolant.</ptxt>
<spec>
<title>Standard Capacity (for Manual transmission)</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>14.6 liters (15.4 US qts, 12.8 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>11.7 liters (12.4 US qts, 10.3 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard capacity (for Automatic transmission)</title>
<specitem>
<ptxt>11.2 liters (11.8 US qts, 9.9 Imp. qts)</ptxt>
</specitem>
</spec>
<spec>
<title>Standard Capacity (for China)</title>
<specitem>
<ptxt>14.4 liters (15.2 US qts, 12.7 Imp. qts)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Slowly pour coolant into the radiator reservoir until it reaches the F line.</ptxt>
</s2>
<s2>
<ptxt>Install the reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand, and then check the coolant level. If the coolant level is low, add coolant.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Set the air conditioning as follows while warming up the engine.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Fan speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Any setting except off</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Toward WARM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Air conditioning switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Start the engine and warm it up until the thermostat opens.</ptxt>
<atten4>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand, and checking when the engine coolant starts to flow inside the hose.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Maintain the engine speed at 2000 to 2500 rpm.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the radiator reservoir still has some coolant in it.</ptxt>
</item>
<item>
<ptxt>Pay attention to the needle of the water temperature meter. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
<item>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the F line.</ptxt>
</item>
<item>
<ptxt>Run the engine at 2000 rpm until the coolant level has stabilized.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand to bleed air.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves. Hot areas on the parts may injure your hands.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the fan.</ptxt>
</item>
</list1>
</atten2>
</s2>
<s2>
<ptxt>Stop the engine, and wait until the engine coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Check that the coolant level is between the F and L lines.</ptxt>
<ptxt>If the coolant level is below the L line, repeat all of the procedures above.</ptxt>
<ptxt>If the coolant level is above the F line, drain coolant so that the coolant level is between the F and L lines.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000Q0M04CX_01_0020" proc-id="RM22W0E___000011O00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>To avoid being burned, do not remove the radiator reservoir cap while the engine and radiator are still hot. Thermal expansion may cause hot engine coolant and steam to blow out from the radiator.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with engine coolant, and then attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and then check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and engine water pump for leakage. If there are no signs or traces of external engine coolant leakage, check the heater core, cylinder block and head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000Q0M04CX_01_0023" proc-id="RM22W0E___00000Z200000">
<ptxt>INSTALL V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 2 V-bank cover grommets with the 2 pins and press down on the V-bank cover to attach the pins.</ptxt>
<figure>
<graphic graphicname="A271365E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000000Q0M04CX_01_0026" proc-id="RM22W0E___000011P00000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover with the 10 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000Q0M04CX_01_0031" proc-id="RM22W0E___000011Q00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly RH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000Q0M04CX_01_0034" proc-id="RM22W0E___000011S00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000Q0M04CX_01_0032" proc-id="RM22W0E___000011R00000">
<ptxt>PERFORM INITIALIZATION</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to perform this procedure after reassembling the throttle body or removing and reinstalling any throttle body component.</ptxt>
</item>
<item>
<ptxt>Perform the following procedure after replacing the ECM, throttle body or any throttle body components. The following procedure should also be performed if the throttle body is cleaned.</ptxt>
</item>
<item>
<ptxt>Be sure to perform this procedure after replacing the ECM and reconnecting the battery cable.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Disconnect the EFI and ETCS fuses at the same time. Wait at least 60 seconds, and then reconnect the fuses.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch to ON without operating the accelerator pedal.</ptxt>
<atten3>
<ptxt>If the accelerator pedal is operated, perform the above steps again.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the GTS to the DLC3 and clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14FX"/>).</ptxt>
</s2>
<s2>
<ptxt>Start the engine and check that the MIL is not illuminated and that the idle speed is within the specified range when the A/C is switched off after the engine is warmed up.</ptxt>
<spec>
<title>Standard idle speed</title>
<specitem>
<ptxt>690 to 790 rpm</ptxt>
</specitem>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to perform this step with all accessories off.</ptxt>
</item>
<item>
<ptxt>Make sure that the shift lever is in neutral.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Throttle Sensor Position. Fully depress the accelerator pedal and check that the value is 60% or more.</ptxt>
</s2>
<s2>
<ptxt>Perform a road test and confirm that there are no abnormalities.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>