<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7C3BO_T004R" variety="T004R">
<name>3UR-FE LUBRICATION</name>
<para id="RM000003CXL00IX" category="F" type-id="30028" name-id="SS42X-08" from="201301">
<name>TORQUE SPECIFICATIONS</name>
<subpara id="RM000003CXL00IX_z0" proc-id="RM22W0E___00000A800000">
<content5 releasenbr="1">
<table pgwide="1">
<title>Oil and Oil Filter</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry>
<ptxt>Part Tightened</ptxt>
</entry>
<entry>
<ptxt>N*m</ptxt>
</entry>
<entry>
<ptxt>kgf*cm</ptxt>
</entry>
<entry>
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Oil pan drain plug x No. 2 oil pan</ptxt>
</entry>
<entry>
<ptxt>40</ptxt>
</entry>
<entry>
<ptxt>408</ptxt>
</entry>
<entry>
<ptxt>30</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Oil filter cap x Oil filter bracket</ptxt>
</entry>
<entry>
<ptxt>25</ptxt>
</entry>
<entry>
<ptxt>255</ptxt>
</entry>
<entry>
<ptxt>18</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Oil filter drain plug x Oil filter cap</ptxt>
</entry>
<entry>
<ptxt>13</ptxt>
</entry>
<entry>
<ptxt>127</ptxt>
</entry>
<entry>
<ptxt>9</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 2 engine under cover seal x No. 2 engine under cover</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 1 engine under cover x Body</ptxt>
</entry>
<entry>
<ptxt>29</ptxt>
</entry>
<entry>
<ptxt>296</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Oil Pressure Sensor</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry>
<ptxt>Part Tightened</ptxt>
</entry>
<entry>
<ptxt>N*m</ptxt>
</entry>
<entry>
<ptxt>kgf*cm</ptxt>
</entry>
<entry>
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Oil pressure sender gauge x Oil filter bracket</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
<entry>
<ptxt>153</ptxt>
</entry>
<entry>
<ptxt>11</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Oil Level Sensor</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry>
<ptxt>Part Tightened</ptxt>
</entry>
<entry>
<ptxt>N*m</ptxt>
</entry>
<entry>
<ptxt>kgf*cm</ptxt>
</entry>
<entry>
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Oil cooler pipe x No. 1 oil pan and bracket</ptxt>
</entry>
<entry>
<ptxt>14</ptxt>
</entry>
<entry>
<ptxt>143</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Engine oil level sensor x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>7.0</ptxt>
</entry>
<entry>
<ptxt>71</ptxt>
</entry>
<entry>
<ptxt>62 in.*lbf</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Oil Pump</title>
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" align="center">
<ptxt>Part Tightened</ptxt>
</entry>
<entry align="center">
<ptxt>N*m</ptxt>
</entry>
<entry align="center">
<ptxt>kgf*cm</ptxt>
</entry>
<entry align="center">
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Relief valve plug x Oil pump cover</ptxt>
</entry>
<entry>
<ptxt>53</ptxt>
</entry>
<entry>
<ptxt>540</ptxt>
</entry>
<entry>
<ptxt>39</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Oil pump cover x Timing chain cover</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>105</ptxt>
</entry>
<entry>
<ptxt>8</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Timing chain cover x Cylinder block and cylinder head</ptxt>
</entry>
<entry>
<ptxt>for bolt A, B, C, nut</ptxt>
</entry>
<entry>
<ptxt>23</ptxt>
</entry>
<entry>
<ptxt>235</ptxt>
</entry>
<entry>
<ptxt>17</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>for bolt D, E, F</ptxt>
</entry>
<entry>
<ptxt>47</ptxt>
</entry>
<entry>
<ptxt>479</ptxt>
</entry>
<entry>
<ptxt>35</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Cylinder head cover LH x Camshaft housing LH and timing chain cover</ptxt>
</entry>
<entry>
<ptxt>for bolt A</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>except bolt A</ptxt>
</entry>
<entry>
<ptxt>12</ptxt>
</entry>
<entry>
<ptxt>122</ptxt>
</entry>
<entry>
<ptxt>9</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Cylinder head cover RH x Camshaft housing RH and timing chain cover</ptxt>
</entry>
<entry>
<ptxt>for bolt A</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>except bolt A</ptxt>
</entry>
<entry>
<ptxt>12</ptxt>
</entry>
<entry>
<ptxt>122</ptxt>
</entry>
<entry>
<ptxt>9</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Cylinder head cover RH x Noise filter</ptxt>
</entry>
<entry>
<ptxt>7.0</ptxt>
</entry>
<entry>
<ptxt>71</ptxt>
</entry>
<entry>
<ptxt>62 in.*lbf</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Ignition coil x Cylinder head cover</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Crankshaft pulley x Crankshaft</ptxt>
</entry>
<entry>
<ptxt>300</ptxt>
</entry>
<entry>
<ptxt>3059</ptxt>
</entry>
<entry>
<ptxt>221</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Wire harness clamp bracket x Timing chain cover</ptxt>
</entry>
<entry>
<ptxt>8.0</ptxt>
</entry>
<entry>
<ptxt>82</ptxt>
</entry>
<entry>
<ptxt>71 in.*lbf</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 1 idler pulley x Timing chain cover</ptxt>
</entry>
<entry>
<ptxt>43</ptxt>
</entry>
<entry>
<ptxt>438</ptxt>
</entry>
<entry>
<ptxt>32</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Water pump pulley x Water pump</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Water inlet housing x Timing chain cover</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Front water by-pass joint x Cylinder head</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Water by-pass pipe x Cylinder head cover and timing chain cover</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Vane pump x Cylinder head</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 2 water by-pass pipe sub-assembly x Timing chain cover and front water by-pass joint</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 2 fuel tube x Cylinder head cover</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>Cooler compressor x Timing chain cover and Cylinder block</ptxt>
</entry>
<entry>
<ptxt>for stud bolt</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>for bolt and nut</ptxt>
</entry>
<entry>
<ptxt>25</ptxt>
</entry>
<entry>
<ptxt>250</ptxt>
</entry>
<entry>
<ptxt>18</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 2 water by-pass pipe x Cylinder head cover RH and Automatic transmission</ptxt>
</entry>
<entry>
<ptxt>18</ptxt>
</entry>
<entry>
<ptxt>184</ptxt>
</entry>
<entry>
<ptxt>13</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Ground wire x Body</ptxt>
</entry>
<entry>
<ptxt>8.0</ptxt>
</entry>
<entry>
<ptxt>82</ptxt>
</entry>
<entry>
<ptxt>71 in.*lbf</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Engine Oil Cooler</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry>
<ptxt>Part Tightened</ptxt>
</entry>
<entry>
<ptxt>N*m</ptxt>
</entry>
<entry>
<ptxt>kgf*cm</ptxt>
</entry>
<entry>
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Relief valve plug x Oil filter bracket</ptxt>
</entry>
<entry>
<ptxt>37</ptxt>
</entry>
<entry>
<ptxt>375</ptxt>
</entry>
<entry>
<ptxt>27</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Oil cooler spacer x Oil filter bracket</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Oil cooler x Oil filter bracket</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Oil filter bracket x Timing chain cover</ptxt>
</entry>
<entry>
<ptxt>35</ptxt>
</entry>
<entry>
<ptxt>357</ptxt>
</entry>
<entry>
<ptxt>26</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 1 oil cooler bracket x Oil filter bracket and No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>