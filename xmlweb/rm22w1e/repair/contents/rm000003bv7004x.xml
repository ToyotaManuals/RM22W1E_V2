<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SC_T00LF" variety="T00LF">
<name>CAN COMMUNICATION SYSTEM (for LHD)</name>
<para id="RM000003BV7004X" category="C" type-id="802N0" name-id="NW487-01" from="201301" to="201308">
<dtccode>U0327</dtccode>
<dtcname>Software Incompatibility with Vehicle Security Control Module</dtcname>
<subpara id="RM000003BV7004X_01" type-id="60" category="03" proc-id="RM22W0E___0000DI400000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U0327</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is no communication from the certification ECU (smart key ECU assembly).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Power source circuit of certification ECU (smart key ECU assembly)</ptxt>
</item>
<item>
<ptxt>Certification ECU (smart key ECU assembly) CAN branch wire or connector</ptxt>
</item>
<item>
<ptxt>Certification ECU (smart key ECU assembly)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>For vehicles with an entry and start system.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000003BV7004X_02" type-id="32" category="03" proc-id="RM22W0E___0000DI500000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C176044E07" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003BV7004X_03" type-id="51" category="05" proc-id="RM22W0E___0000DI600000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Operating the ignition switch, any switches or any doors triggers related ECU and sensor communication with the CAN, which causes resistance variation.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000003BV7004X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003BV7004X_04_0007" proc-id="RM22W0E___0000DIA00000">
<testtitle>PRECAUTION</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content6>
<res>
<down ref="RM000003BV7004X_04_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003BV7004X_04_0001" proc-id="RM22W0E___0000DI700000">
<testtitle>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal before measuring the resistances of the main wire and the branch wire.</ptxt>
<atten2>
<ptxt>For vehicles with an SRS system:</ptxt>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000003BV7004X_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003BV7004X_04_0002" proc-id="RM22W0E___0000DI800000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (CERTIFICATION ECU CAN BRANCH WIRE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E29 certification ECU (smart key ECU assembly) connector.</ptxt>
<figure>
<graphic graphicname="E191907E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-27 (CANH) - E29-28 (CANL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>54 to 69 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Certification ECU [Smart Key ECU Assembly])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003BV7004X_04_0003" fin="false">OK</down>
<right ref="RM000003BV7004X_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003BV7004X_04_0003" proc-id="RM22W0E___0000DI900000">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
<figure>
<graphic graphicname="E191907E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-17 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-1(+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-19 (ACC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-18 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Certification ECU [Smart Key ECU Assembly])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000003BV7004X_04_0004" fin="true">OK</down>
<right ref="RM000003BV7004X_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003BV7004X_04_0004">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM000003BV7004X_04_0005">
<testtitle>REPAIR OR REPLACE CERTIFICATION ECU CAN BRANCH WIRE OR CONNECTOR (CANH, CANL)</testtitle>
</testgrp>
<testgrp id="RM000003BV7004X_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>