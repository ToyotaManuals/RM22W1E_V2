<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3ST_T00LW" variety="T00LW">
<name>UNLOCK WARNING SWITCH</name>
<para id="RM000002ZQ6044X" category="A" type-id="80001" name-id="DL9F3-01" from="201308">
<name>REMOVAL</name>
<subpara id="RM000002ZQ6044X_01" type-id="01" category="01">
<s-1 id="RM000002ZQ6044X_01_0017" proc-id="RM22W0E___0000EBT00001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002ZQ6044X_01_0018" proc-id="RM22W0E___0000EBU00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002ZQ6044X_01_0020" proc-id="RM22W0E___0000A9B00000">
<ptxt>REMOVE LOWER STEERING COLUMN COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="C179481" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws to remove the lower steering column cover.</ptxt>
<atten3>
<ptxt>Do not damage the tilt and telescopic switch.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002ZQ6044X_01_0021" proc-id="RM22W0E___0000A9C00000">
<ptxt>REMOVE UPPER STEERING COLUMN COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 clips.</ptxt>
<figure>
<graphic graphicname="C179482" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw to remove the upper steering column cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002ZQ6044X_01_0026" proc-id="RM22W0E___0000EBV00001">
<ptxt>REMOVE TRANSPONDER KEY AMPLIFIER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, widen the claw attached to the upper bracket by approximately 1.0 mm (0.039 in.).</ptxt>
<figure>
<graphic graphicname="E234230" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Pull out the transponder key amplifier with the claw open.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the transponder key amplifier.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002ZQ6044X_01_0024" proc-id="RM22W0E___0000BIP00000">
<ptxt>REMOVE IGNITION SWITCH LOCK CYLINDER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the ignition switch lock cylinder assembly to ACC.</ptxt>
</s2>
<s2>
<ptxt>Insert a thin-bladed screwdriver into the hole of the steering column upper bracket assembly as shown in the illustration and pull out the key until the claw of the ignition switch lock cylinder assembly contacts the stopper of the steering column upper bracket assembly.</ptxt>
<figure>
<graphic graphicname="C214493E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>Be sure to pull out the key until the claw firmly contacts the stopper. There will be problems later if the key is not pulled out until the claw of the ignition switch lock cylinder assembly contacts the stopper of the steering column upper bracket assembly.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Insert a thin-bladed screwdriver into the hole of the steering column upper bracket assembly as shown in the illustration, push the thin-bladed screwdriver in the direction shown in the illustration to detach the claw and pull out the ignition switch lock cylinder assembly.</ptxt>
<figure>
<graphic graphicname="C105483" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002ZQ6044X_01_0003" proc-id="RM22W0E___0000EBS00001">
<ptxt>REMOVE UNLOCK WARNING SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the unlock warning switch connector.</ptxt>
<figure>
<graphic graphicname="B292784" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and slide the unlock warning switch as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Push in the pin, slide the unlock warning switch as shown in the illustration and remove it.</ptxt>
<figure>
<graphic graphicname="B292785E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>