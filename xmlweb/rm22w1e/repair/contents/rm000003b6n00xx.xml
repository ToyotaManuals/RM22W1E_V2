<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000I" variety="S000I">
<name>1VD-FTV FUEL</name>
<ttl id="12008_S000I_7C3HI_T00AL" variety="T00AL">
<name>FUEL SENDER GAUGE ASSEMBLY (for Fuel Sub Tank)</name>
<para id="RM000003B6N00XX" category="A" type-id="30014" name-id="FU4Y8-05" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000003B6N00XX_01" type-id="01" category="01">
<s-1 id="RM000003B6N00XX_01_0001" proc-id="RM22W0E___00006DF00001">
<ptxt>INSTALL FUEL SENDER GAUGE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the sender gauge.</ptxt>
<figure>
<graphic graphicname="A176327" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the sender gauge with the 5 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>1.5</t-value1>
<t-value2>15</t-value2>
<t-value3>13</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the sender gauge connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00XX_01_0002" proc-id="RM22W0E___00006DG00001">
<ptxt>INSTALL REAR FLOOR SERVICE HOLE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the service hole cover with new butyl tape.</ptxt>
<figure>
<graphic graphicname="A176326" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00XX_01_0005" proc-id="RM22W0E___00006DI00001">
<ptxt>INSTALL REAR NO. 7 AIR DUCT (w/ Rear Air Conditioning System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 clips to install the air duct.</ptxt>
<atten4>
<ptxt>Attach the clips in the order shown in the illustration.</ptxt>
</atten4>
<figure>
<graphic graphicname="A176392E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00XX_01_0003" proc-id="RM22W0E___00006DH00001">
<ptxt>INSTALL REAR FLOOR MAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 5 clips to install the floor mat.</ptxt>
<figure>
<graphic graphicname="A176328" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00XX_01_0023" proc-id="RM22W0E___000067700001">
<ptxt>INSTALL REAR AIR DUCT GUIDE (w/ Rear Air Conditioning System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the guide.</ptxt>
<figure>
<graphic graphicname="B183334" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
<atten4>
<ptxt>Use the same procedures for both sides.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003B6N00XX_01_0024" proc-id="RM22W0E___000067800001">
<ptxt>INSTALL AIR DUCT PLUG (w/ Rear Air Conditioning System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the plug.</ptxt>
<atten4>
<ptxt>Use the same procedures for both sides.</ptxt>
</atten4>
<figure>
<graphic graphicname="B183333" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003B6N00XX_01_0025" proc-id="RM22W0E___00006DL00001">
<ptxt>INSTALL FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Install the front quarter trim panel assembly LH (See page <xref label="Seep01" href="RM0000038MM00UX_02_0020"/>).</ptxt>
</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>Install the front quarter trim panel assembly LH (See page <xref label="Seep02" href="RM0000038MM00VX_02_0020"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00XX_01_0027" proc-id="RM22W0E___00006DM00001">
<ptxt>INSTALL FRONT QUARTER TRIM PANEL ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Install the front quarter trim panel assembly RH (See page <xref label="Seep01" href="RM0000038MM00UX_02_0022"/>).</ptxt>
</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>Install the front quarter trim panel assembly RH (See page <xref label="Seep02" href="RM0000038MM00VX_02_0022"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00XX_01_0028" proc-id="RM22W0E___000067B00001">
<ptxt>INSTALL REAR SEAT COVER CAP (w/ Rear No. 2 Seat, except Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190214" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the rear seat cover cap on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 3 claws to install the rear seat cover cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6N00XX_01_0029" proc-id="RM22W0E___000067C00001">
<ptxt>INSTALL REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 6 clips to install the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6N00XX_01_0030" proc-id="RM22W0E___000067D00001">
<ptxt>INSTALL REAR DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181674" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws and 4 clips to install the scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6N00XX_01_0031" proc-id="RM22W0E___000067E00001">
<ptxt>INSTALL REAR DOOR SCUFF PLATE RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM000003B6N00XX_01_0032" proc-id="RM22W0E___000067F00001">
<ptxt>INSTALL REAR STEP COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181673" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the step cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the step cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6N00XX_01_0035" proc-id="RM22W0E___00006DN00001">
<ptxt>INSTALL REAR NO. 2 SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Face to Face Seat Type:</ptxt>
<ptxt>Install the rear No. 2 seat assembly (See page <xref label="Seep01" href="RM00000311A003X_01_0001"/>).</ptxt>
</s2>
<s2>
<ptxt>except Face to Face Seat Type:</ptxt>
<ptxt>Install the rear No. 2 seat assembly (See page <xref label="Seep02" href="RM00000391Q00YX_01_0010"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00XX_01_0036" proc-id="RM22W0E___00006DO00001">
<ptxt>INSTALL REAR NO. 1 SEAT ASSEMBLY RH (for 60/40 Split Seat Type 40 Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear No. 1 seat assembly RH (See page <xref label="Seep01" href="RM00000391200SX_01_0004"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00XX_01_0037" proc-id="RM22W0E___00006DP00001">
<ptxt>INSTALL REAR NO. 1 SEAT ASSEMBLY LH (for 60/40 Split Seat Type 60 Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear No. 1 seat assembly LH (See page <xref label="Seep01" href="RM00000390X00SX_01_0004"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00XX_01_0021" proc-id="RM22W0E___00006DJ00001">
<ptxt>INSTALL CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
<s2>
<ptxt>Connect the cables to the negative (-) main battery and sub-battery terminals.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6N00XX_01_0022" proc-id="RM22W0E___00006DK00001">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0KMX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>