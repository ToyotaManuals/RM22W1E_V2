<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001F" variety="S001F">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001F_7C3O3_T00H6" variety="T00H6">
<name>REAR DIFFERENTIAL CARRIER OIL SEAL</name>
<para id="RM00000318400LX" category="A" type-id="30019" name-id="AD1S7-01" from="201301">
<name>REPLACEMENT</name>
<subpara id="RM00000318400LX_01" type-id="01" category="01">
<s-1 id="RM00000318400LX_01_0005" proc-id="RM22W0E___00009A400000">
<ptxt>DRAIN DIFFERENTIAL OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Stop the vehicle on a level place.</ptxt>
</s2>
<s2>
<ptxt>for Front Differential:</ptxt>
<figure>
<graphic graphicname="C174596" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the engine under cover.</ptxt>
</s3>
<s3>
<ptxt>Using a 10 mm hexagon wrench, remove the filler plug and gasket.</ptxt>
</s3>
<s3>
<ptxt>Using a 10 mm hexagon wrench, remove the drain plug and gasket, and drain the oil.</ptxt>
</s3>
<s3>
<ptxt>Using a 10 mm hexagon wrench, install a new gasket and the drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>39</t-value1>
<t-value2>400</t-value2>
<t-value4>29</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
<s2>
<ptxt>for Rear Differential:</ptxt>
<s3>
<ptxt>Remove the filler plug and gasket.</ptxt>
</s3>
<s3>
<ptxt>Remove the drain plug and gasket, and drain the oil.</ptxt>
</s3>
<s3>
<ptxt>Install a new gasket and the drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>49</t-value1>
<t-value2>500</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM00000318400LX_01_0020" proc-id="RM22W0E___00009HZ00000">
<ptxt>DISCONNECT PROPELLER SHAFT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place matchmarks on the rear propeller shaft and companion flange.</ptxt>
<figure>
<graphic graphicname="C172482E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 nuts, 4 washers and 4 bolts, and disconnect the propeller shaft from the differential side.  </ptxt>
</s2>
<s2>
<ptxt>Support the propeller shaft securely.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000318400LX_01_0004" proc-id="RM22W0E___00009HP00000">
<ptxt>REMOVE DRIVE PINION COMPANION FLANGE REAR NUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, loosen the staked part of the nut.</ptxt>
<sst>
<sstitem>
<s-number>09930-00010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C089309E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST to hold the companion flange, remove the nut.</ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
<s-subnumber>09330-00030</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="B087842E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318400LX_01_0006" proc-id="RM22W0E___00009HQ00000">
<ptxt>REMOVE REAR DRIVE PINION COMPANION FLANGE REAR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the companion flange.</ptxt>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03040</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161823E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318400LX_01_0007" proc-id="RM22W0E___00009HR00000">
<ptxt>REMOVE REAR DIFFERENTIAL CARRIER OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the oil seal.</ptxt>
<sst>
<sstitem>
<s-number>09308-10010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161824E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318400LX_01_0008">
<ptxt>REMOVE REAR DIFFERENTIAL DRIVE PINION OIL SLINGER</ptxt>
</s-1>
<s-1 id="RM00000318400LX_01_0009" proc-id="RM22W0E___00009HS00000">
<ptxt>REMOVE REAR DRIVE PINION FRONT TAPERED ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the front bearing (inner race) from the drive pinion.</ptxt>
<sst>
<sstitem>
<s-number>09556-22010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161825E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, remove the front bearing (outer race) from the drive pinion.</ptxt>
<sst>
<sstitem>
<s-number>09308-00010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161826E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318400LX_01_0011" proc-id="RM22W0E___00009HT00000">
<ptxt>REPLACE REAR DIFFERENTIAL DRIVE PINION BEARING SPACER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Replace the bearing spacer with a new one.</ptxt>
<figure>
<graphic graphicname="C163578" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318400LX_01_0012" proc-id="RM22W0E___00009HU00000">
<ptxt>INSTALL REAR DRIVE PINION FRONT TAPERED ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, tap in the bearing (outer race).</ptxt>
<sst>
<sstitem>
<s-number>09316-60011</s-number>
<s-subnumber>09316-00011</s-subnumber>
<s-subnumber>09316-00021</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161827E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the bearing (inner race).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318400LX_01_0013">
<ptxt>INSTALL REAR DIFFERENTIAL DRIVE PINION OIL SLINGER</ptxt>
</s-1>
<s-1 id="RM00000318400LX_01_0019" proc-id="RM22W0E___00009HY00000">
<ptxt>INSTALL REAR DIFFERENTIAL CARRIER OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, tap in a new oil seal.</ptxt>
<sst>
<sstitem>
<s-number>09214-76011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161828E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard oil seal depth</title>
<specitem>
<ptxt>0.05 to 0.95 mm (0.00197 to 0.0374 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Coat the lip of the oil seal with MP grease.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318400LX_01_0014" proc-id="RM22W0E___00009HV00000">
<ptxt>INSTALL REAR DRIVE PINION COMPANION FLANGE REAR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, install the companion flange to the drive pinion.</ptxt>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03040</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161829E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318400LX_01_0015" proc-id="RM22W0E___00009HW00000">
<ptxt>ADJUST DRIVE PINION PRELOAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the threads of a new nut with hypoid gear oil.</ptxt>
</s2>
<s2>
<ptxt>Using SST to hold the companion flange, install the nut by tightening it until the standard preload is reached. </ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
<s-subnumber>09330-00030</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C088916E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>441</t-value1>
<t-value2>4497</t-value2>
<t-value4>325</t-value4>
<ptxt>or less</ptxt>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a torque wrench, measure the preload.</ptxt>
<figure>
<graphic graphicname="C124126" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry align="center">
<ptxt>1.0 to 1.7 N*m (11 to 17 kgf*cm, 10 to 14 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry align="center">
<ptxt>0.9 to 1.4 N*m (9 to 13 kgf*cm, 8 to 12 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the preload is not within the specification, adjust the preload.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318400LX_01_0016" proc-id="RM22W0E___00009HX00000">
<ptxt>STAKE DRIVE PINION COMPANION FLANGE REAR NUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a chisel and hammer, stake the nut.</ptxt>
<figure>
<graphic graphicname="C089310" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318400LX_01_0021" proc-id="RM22W0E___00009B600000">
<ptxt>CONNECT PROPELLER SHAFT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the matchmarks on the propeller shaft flange and transfer.</ptxt>
<figure>
<graphic graphicname="C172481E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the propeller shaft with the 4 washers and 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>88</t-value1>
<t-value2>900</t-value2>
<t-value4>65</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Align the matchmarks on the propeller shaft flange and rear differential.</ptxt>
<figure>
<graphic graphicname="C178527E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the propeller shaft with the 4 bolts, 4 washers and 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>88</t-value1>
<t-value2>900</t-value2>
<t-value4>65</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000318400LX_01_0018" proc-id="RM22W0E___000099V00000">
<ptxt>ADD DIFFERENTIAL OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add differential oil so that the oil level is between 0 to 5 mm (0 to 0.197 in.) from the bottom lip of the differential filler plug hole.</ptxt>
<figure>
<graphic graphicname="D025304E27" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Too much or too little oil will lead to differential problems.</ptxt>
</item>
<item>
<ptxt>After changing the oil, drive the vehicle and then check the oil level again.</ptxt>
</item>
</list1>
</atten3>
<spec>
<title>Front differential oil type and viscosity</title>
<specitem>
<ptxt>Toyota Genuine Differential gear oil LT 75W-85 GL-5 or equivalent</ptxt>
</specitem>
</spec>
<spec>
<title>Rear Differential Oil Type and Viscosity</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Type and Viscosity</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/o LSD</ptxt>
</entry>
<entry valign="middle">
<ptxt>Toyota Genuine Differential gear oil LT 75W-85 GL-5 or equivalent</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ LSD</ptxt>
</entry>
<entry>
<ptxt>Toyota Genuine Differential gear oil LX 75W-85 GL-5 or equivalent</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Front differential capacity</title>
<specitem>
<ptxt>1.85 to 1.95 liters (1.96 to 2.06 US qts., 1.63 to 1.71 Imp. qts.)</ptxt>
</specitem>
</spec>
<spec>
<title>Rear Differential Capacity</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.15 to 4.25 liters (4.39 to 4.49 US qts., 3.66 to 3.74 Imp. qts.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ LSD</ptxt>
</entry>
<entry morerows="1">
<ptxt>4.10 to 4.20 liters (4.34 to 4.43 US qts., 3.60 to 3.69 Imp. qts.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ Differential lock</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>for Front Differential:</ptxt>
<s3>
<ptxt>Using a 10 mm hexagon wrench, install a new gasket and the filler plug.</ptxt>
<torque>
<torqueitem>
<t-value1>39</t-value1>
<t-value2>400</t-value2>
<t-value4>29</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Install the engine under cover.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for Rear Differential:</ptxt>
<s3>
<ptxt>Install a new gasket and the filler plug.</ptxt>
<torque>
<torqueitem>
<t-value1>49</t-value1>
<t-value2>500</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>