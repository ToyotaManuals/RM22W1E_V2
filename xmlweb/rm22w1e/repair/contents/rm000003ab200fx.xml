<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000V" variety="S000V">
<name>1UR-FE COOLING</name>
<ttl id="12011_S000V_7C3JY_T00D1" variety="T00D1">
<name>RADIATOR</name>
<para id="RM000003AB200FX" category="A" type-id="30014" name-id="CO6CA-01" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM000003AB200FX_01" type-id="01" category="01">
<s-1 id="RM000003AB200FX_01_0017" proc-id="RM22W0E___00004H700000">
<ptxt>INSTALL RADIATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A178882" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Insert the radiator bracket hooks into the radiator support holes.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AB200FX_01_0045">
<ptxt>INSTALL NO. 2 RADIATOR HOSE</ptxt>
</s-1>
<s-1 id="RM000003AB200FX_01_0019" proc-id="RM22W0E___00004H800000">
<ptxt>INSTALL FAN SHROUD</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Install the fan pulley to the engine water pump.</ptxt>
</s2>
<s2>
<ptxt>Place the shroud together with the fluid coupling fan between the radiator and engine.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Temporarily install the fluid coupling fan to the fluid coupling bracket with the 4 nuts. Tighten the nuts as much as possible by hand.</ptxt>
</s2>
<s2>
<ptxt>Attach the claws of the shroud to the radiator as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A241407" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the shroud with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the oil cooler tube to the fan shroud with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>w/ Air Cooled Transmission Oil Cooler:</ptxt>
<ptxt>Pass the hose through the flexible hose clamp and close the clamp as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Connect the reservoir hose to the upper radiator tank.</ptxt>
</s2>
<s2>
<ptxt>Install the fan and generator V belt (See page <xref label="Seep01" href="RM000002BOF023X"/>).</ptxt>
</s2>
<s2>
<ptxt>Tighten the 4 nuts of the fluid coupling fan.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AB200FX_01_0046">
<ptxt>INSTALL NO. 1 RADIATOR HOSE</ptxt>
</s-1>
<s-1 id="RM000003AB200FX_01_0022" proc-id="RM22W0E___000013700000">
<ptxt>INSTALL AIR CLEANER CAP AND HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the air cleaner cap and hose, and then tighten the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>2.5</t-value1>
<t-value2>25</t-value2>
<t-value3>22</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Connect the mass air flow meter connector and attach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 2 PCV hose and No. 1 air hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003AB200FX_01_0035" proc-id="RM22W0E___000013800000">
<ptxt>INSTALL V-BANK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 3 V-bank cover grommets with the 3 pins, and press down on the V-bank cover to attach the pins.</ptxt>
<figure>
<graphic graphicname="A274416E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Grommet</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003AB200FX_01_0024" proc-id="RM22W0E___000013K00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add engine coolant.</ptxt>
<spec>
<title>Standard Capacity (w/o ATF Warmer)</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL2" colwidth="2.07in"/>
<colspec colname="COL3" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>16.5 liters (17.4 US qts, 14.5 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>13.8 liters (14.6 US qts, 12.1 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Capacity (w/ ATF Warmer)</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL2" colwidth="2.07in"/>
<colspec colname="COL3" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>17.0 liters (18.0 US qts, 15.0 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>14.2 liters (15.0 US qts, 12.5 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</item>
<item>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand, and then check the coolant level. If the coolant level is low, add coolant.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Slowly pour coolant into the radiator reservoir until it reaches the F line.</ptxt>
</s2>
<s2>
<ptxt>Install the reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.*1</ptxt>
</s2>
<s2>
<ptxt>Start the engine and stop it immediately.*2</ptxt>
</s2>
<s2>
<ptxt>Allow approximately 10 seconds to pass. Then remove the radiator cap and check the coolant level. If the coolant level has decreased, add coolant.*3</ptxt>
</s2>
<s2>
<ptxt>Repeat steps *1, *2 and *3 until the coolant level does not decrease.</ptxt>
<atten4>
<ptxt>Be sure to perform this step while the engine is cold, as air in the No. 1 radiator hose will flow into the radiator if the engine is warmed up and the thermostat opens.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the radiator cap.*4</ptxt>
</s2>
<s2>
<ptxt>Set the air conditioning as follows.*5</ptxt>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Fan speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Any setting except off</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
<entry valign="middle">
<ptxt>Toward WARM</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air conditioning switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Start the engine, warm it up until the thermostat opens, and then continue to run the engine for several minutes to circulate the coolant.*6</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves. Hot areas on the parts may injure your hands.</ptxt>
</item>
<item>
<ptxt>Be careful of the fan.</ptxt>
</item>
<item>
<ptxt>Be careful as the engine, radiator and radiator hoses are hot and can cause burns.</ptxt>
</item>
</list1>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the F line.</ptxt>
</item>
<item>
<ptxt>Do not start the engine when there is no coolant in the radiator reservoir.</ptxt>
</item>
<item>
<ptxt>Pay attention to the needle of the engine coolant temperature receiver gauge. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand to bleed air while warming up the engine.</ptxt>
</item>
<item>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand and checking when the engine coolant starts to flow inside the hose.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Stop the engine, wait until the engine coolant cools down to ambient temperature. Then remove the radiator cap and check the coolant level.*7</ptxt>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>If the coolant level has decreased, add coolant and warm up the engine until the thermostat opens.*8</ptxt>
</s2>
<s2>
<ptxt>If the coolant level has not decreased, check that the coolant level in the radiator reservoir is at the F line.</ptxt>
<ptxt>If the coolant level is below the F line, repeat steps *4 through *8.</ptxt>
<ptxt>If the coolant level is above the F line, drain coolant until the coolant level reaches the F line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003AB200FX_01_0025" proc-id="RM22W0E___000013L00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>To avoid being burned, do not remove the radiator reservoir cap while the engine and radiator are still hot. Thermal expansion may cause hot engine coolant and steam to blow out from the radiator.</ptxt>
</atten2>
<s2>
<ptxt>Remove the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Check for excessive deposits of rust or scales around the radiator reservoir cap and radiator reservoir filler hole. Also, the engine coolant should be free of oil.</ptxt>
<ptxt>If excessively dirty, replace the engine coolant.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003AB200FX_01_0029" proc-id="RM22W0E___00004H900000">
<ptxt>INSTALL RADIATOR SIDE DEFLECTOR LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the deflector with the 4 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AB200FX_01_0030" proc-id="RM22W0E___00004HA00000">
<ptxt>INSTALL RADIATOR SIDE DEFLECTOR RH (w/o Air Cooled Transmission Oil Cooler)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the deflector with the 4 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AB200FX_01_0047" proc-id="RM22W0E___00004IG00000">
<ptxt>INSTALL TRANSMISSION OIL COOLER AIR DUCT (w/ Air Cooled Transmission Oil Cooler)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the oil cooler air duct with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>4.9</t-value1>
<t-value2>50</t-value2>
<t-value3>43</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003AB200FX_01_0043" proc-id="RM22W0E___000013N00000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover sub-assembly with the 10 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003AB200FX_01_0048" proc-id="RM22W0E___00006VC00000">
<ptxt>INSTALL FRONT BUMPER COVER (w/ Winch)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000038JV01GX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000003AB200FX_01_0036" proc-id="RM22W0E___00006VB00000">
<ptxt>INSTALL FRONT BUMPER COVER (for Standard)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000038JV01HX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>