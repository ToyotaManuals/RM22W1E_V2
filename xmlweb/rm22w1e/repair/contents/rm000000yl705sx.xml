<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000H" variety="S000H">
<name>3UR-FE FUEL</name>
<ttl id="12008_S000H_7C3GW_T009Z" variety="T009Z">
<name>FUEL SYSTEM</name>
<para id="RM000000YL705SX" category="G" type-id="8000T" name-id="FU6FC-03" from="201308">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM000000YL705SX_01" type-id="01" category="01">
<s-1 id="RM000000YL705SX_01_0001" proc-id="RM22W0E___000061500001">
<ptxt>CHECK FUEL PUMP OPERATION AND FOR FUEL LEAK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the engine switch on (IG).</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Turn the intelligent tester main switch on.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Fuel Pump / Speed.</ptxt>
</s2>
<s2>
<ptxt>Check the fuel pump operation.</ptxt>
<s3>
<ptxt>Check for pressure in the fuel inlet tube from the fuel line. Check that the sound of fuel flowing in the fuel tank can be heard.</ptxt>
<ptxt>If there is no sound, check the integration relay, fuel pump, ECM and wiring connector.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check for fuel leak.</ptxt>
<s3>
<ptxt>Check that there are no fuel leaks after performing maintenance anywhere on the system.</ptxt>
<ptxt>If there are fuel leaks, repair or replace parts as necessary.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000YL705SX_01_0002" proc-id="RM22W0E___000061600001">
<ptxt>CHECK FUEL PRESSURE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the battery voltage is 11 to 14 V.</ptxt>
</s2>
<s2>
<ptxt>Perform the "Discharge Fuel System Pressure" procedures (See page <xref label="Seep01" href="RM0000028RU04QX"/>).</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep03" href="RM000003C32006X"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the fuel pipe clamp from the fuel tube connector.</ptxt>
<figure>
<graphic graphicname="C157056E13" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Pipe Clamp</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Pinch and pull the fuel tube (fuel tube connector) to disconnect it.</ptxt>
<figure>
<graphic graphicname="A237020" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pinch</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Always read the precautions (See page <xref label="Seep02" href="RM0000028RU04QX"/>) before disconnecting the fuel tube connector (quick type).</ptxt>
</item>
<item>
<ptxt>The fuel tube may spray fuel as a result of pressure that remains in it. Do not allow fuel to be sprayed in the engine compartment.</ptxt>
</item>
</list1>
</atten2>
</s2>
<s2>
<ptxt>Install SST (pressure gauge) as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A274077E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<sst>
<sstitem>
<s-number>09268-45101</s-number>
<s-subnumber>09268-41250</s-subnumber>
<s-subnumber>09268-41260</s-subnumber>
<s-subnumber>09268-41280</s-subnumber>
<s-subnumber>09268-41500</s-subnumber>
<s-subnumber>09268-41700</s-subnumber>
<s-subnumber>95336-08070</s-subnumber>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>SST (T Joint)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>SST (Hose Band)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>SST (Hose Joint)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>SST (Gauge Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Fuel Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry>
<ptxt>SST (Fuel Tube Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Wipe off any gasoline.</ptxt>
</s2>
<s2>
<ptxt>Reconnect the cable to the negative (-) battery terminal.</ptxt>
</s2>
<s2>
<ptxt>Operate the fuel pump.</ptxt>
<s3>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the engine switch on (IG).</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Turn the intelligent tester main switch on.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Fuel Pump / Speed.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Measure the fuel pressure.</ptxt>
<spec>
<title>Standard fuel pressure</title>
<specitem>
<ptxt>281 to 287 kPa (2.87 to 2.93 kgf/cm<sup>2</sup>, 41 to 42 psi)</ptxt>
</specitem>
</spec>
<ptxt>If the pressure is higher than the specification, replace the fuel pressure regulator.</ptxt>
<ptxt>If the pressure is below the specification, check the fuel hoses and connections, fuel pump, fuel filter and fuel pressure regulator assembly.</ptxt>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>Measure the fuel pressure.</ptxt>
<spec>
<title>Standard fuel pressure</title>
<specitem>
<ptxt>281 to 287 kPa (2.87 to 2.93 kgf/cm<sup>2</sup>, 41 to 42 psi)</ptxt>
</specitem>
</spec>
<ptxt>If the pressure is not as specified, check the fuel pump, pressure regulator and/or injectors.</ptxt>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Check that the fuel pressure remains as specified for 5 minutes after the engine has stopped.</ptxt>
<spec>
<title>Standard fuel pressure</title>
<specitem>
<ptxt>147 kPa (1.5 kgf/cm<sup>2</sup>, 21 psi) or more</ptxt>
</specitem>
</spec>
<ptxt>If the pressure is not as specified, check the fuel pump, pressure regulator and/or injectors.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal and carefully remove SST and the fuel tube connector to prevent gasoline from spraying.</ptxt>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep04" href="RM000003C32006X"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Reconnect the fuel tube (fuel tube connector).</ptxt>
</s2>
<s2>
<ptxt>Check for fuel leaks.</ptxt>
<s3>
<ptxt>Check that there are no fuel leaks after performing maintenance anywhere on the system.</ptxt>
<ptxt>If there are fuel leaks, repair or replace parts as necessary.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>