<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7C3BV_T004Y" variety="T004Y">
<name>1UR-FE FUEL</name>
<para id="RM000002J1X01FX" category="F" type-id="30027" name-id="SS2V8-14" from="201301">
<name>SERVICE DATA</name>
<subpara id="RM000002J1X01FX_z0" proc-id="RM22W0E___00000AL00000">
<content5 releasenbr="1">
<table pgwide="1">
<title>FUEL SYSTEM</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" align="left" colwidth="1.77in"/>
<colspec colname="COL4" align="left" colwidth="1.77in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" morerows="1" valign="middle">
<ptxt>Standard fuel pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>at idle</ptxt>
</entry>
<entry valign="middle">
<ptxt>321 to 327 kPa (3.27 to 3.33 kgf/cm<sup>2</sup>, 46.5 to 47.4 psi)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>remains for 5 minutes after engine has stopped</ptxt>
</entry>
<entry valign="middle">
<ptxt>147 kPa (1.5 kgf/cm<sup>2</sup>, 21 psi) or more</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>FUEL INJECTOR</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" align="left" colwidth="1.77in"/>
<colspec colname="COL4" align="left" colwidth="1.77in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Standard resistance</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11.6 to 12.4 Ω</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Standard injection volume</ptxt>
</entry>
<entry namest="COL3" nameend="COL4" valign="middle">
<ptxt>82 to 99 cc (5.0 to 6.0 cu. in.) per 15 seconds</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Difference between each fuel injector</ptxt>
</entry>
<entry namest="COL3" nameend="COL4" valign="middle">
<ptxt>17 cc (1.2 cu. in.) or less</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Standard fuel drop</ptxt>
</entry>
<entry namest="COL3" nameend="COL4" valign="middle">
<ptxt>1 drop or less in 12 minutes</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>FUEL PUMP</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" align="left" colwidth="1.77in"/>
<colspec colname="COL4" align="left" colwidth="1.77in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Standard resistance</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.2 to 3.0 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>FUEL SENDER GAUGE ASSEMBLY</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" align="left" colwidth="1.77in"/>
<colspec colname="COL4" align="left" colwidth="1.77in"/>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Standard resistance</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>2 - 3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Float level is F (Upper)</ptxt>
</entry>
<entry valign="middle">
<ptxt>13.5 to 16.5 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Float level is E (Lower)</ptxt>
</entry>
<entry valign="middle">
<ptxt>405.5 to 414.5 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>FUEL SENDER GAUGE ASSEMBLY (for Fuel Sub Tank)</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" align="left" colwidth="1.77in"/>
<colspec colname="COL4" align="left" colwidth="1.77in"/>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Standard resistance</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>2 - 3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Float level is F (Upper)</ptxt>
</entry>
<entry valign="middle">
<ptxt>12 to 18 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Float level is E (Lower)</ptxt>
</entry>
<entry valign="middle">
<ptxt>405 to 415 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>