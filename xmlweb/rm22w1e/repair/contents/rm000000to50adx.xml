<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SN_T00LQ" variety="T00LQ">
<name>POWER DOOR LOCK CONTROL SYSTEM</name>
<para id="RM000000TO50ADX" category="J" type-id="305JM" name-id="DL8CC-01" from="201301" to="201308">
<dtccode/>
<dtcname>All Doors LOCK/UNLOCK Functions do not Operate Via Master Switch, Driver Side Door Key Cylinder</dtcname>
<subpara id="RM000000TO50ADX_01" type-id="60" category="03" proc-id="RM22W0E___0000E8B00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The main body ECU receives switch signals from the multiplex network master switch, and driver side door key cylinder switch signals from the front door lock. The main body ECU activates the door lock motor on each door according to these signals.</ptxt>
</content5>
</subpara>
<subpara id="RM000000TO50ADX_02" type-id="32" category="03" proc-id="RM22W0E___0000E8C00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B292485E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TO50ADX_03" type-id="51" category="05" proc-id="RM22W0E___0000E8D00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Since the power door lock control system has functions that use LIN communication, first confirm that there is no malfunction in the communication system by inspecting the LIN communication functions in accordance with the "How to Proceed with Troubleshooting" procedures. Then, conduct the following inspection procedure.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TO50ADX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TO50ADX_04_0003" proc-id="RM22W0E___0000E8E00000">
<testtitle>CHECK DOOR LOCK OPERATION</testtitle>
<content6 releasenbr="1">
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>All doors cannot be locked through multiplex network master switch and driver side door key cylinder</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>All doors cannot be locked through multiplex network master switch (for Models with Jam Protection Function on Driver Door Window Only)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>All doors cannot be locked through multiplex network master switch (for Models with Jam Protection Function on 4 Windows)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>All doors cannot be locked through driver side door key cylinder</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000TO50ADX_04_0022" fin="false">A</down>
<right ref="RM000000TO50ADX_04_0024" fin="false">B</right>
<right ref="RM000000TO50ADX_04_0004" fin="false">C</right>
<right ref="RM000000TO50ADX_04_0005" fin="false">D</right>
</res>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0022" proc-id="RM22W0E___0000E8J00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the 2A, 2B and 2D main body ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage </title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2A-1 (ALTB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2B-20 (BATB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2D-62 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TO50ADX_04_0012" fin="true">OK</down>
<right ref="RM000000TO50ADX_04_0023" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0024" proc-id="RM22W0E___0000E8K00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTIPLEX NETWORK MASTER SWITCH - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LHD:</ptxt>
<test2>
<ptxt>Disconnect the I31 master switch connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the 2D main body ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I31-9 (UL) - 2D-52 (UL1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I31-2 (L) - 2D-49 (L1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I31-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I31-9 (UL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I31-2 (L) - Body ground</ptxt>
</entry>
<entry valign="middle">

<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD:</ptxt>
<test2>
<ptxt>Disconnect the I26 master switch connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the 2D main body ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I26-8 (UL) - 2D-52 (UL1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I26-2 (L) - 2D-49 (L1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I26-9 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I26-8 (UL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I26-2 (L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000TO50ADX_04_0004" fin="false">OK</down>
<right ref="RM000000TO50ADX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0004" proc-id="RM22W0E___0000E8F00000">
<testtitle>CHECK MULTIPLEX NETWORK MASTER SWITCH ASSEMBLY (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the multiplex network master switch with a normally functioning one (See page <xref label="Seep01" href="RM000002STU087X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check that all doors can be locked and unlocked by using the multiplex network master switch.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>All doors can be locked and unlocked with multiplex network master switch.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TO50ADX_04_0013" fin="true">OK</down>
<right ref="RM000000TO50ADX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0005" proc-id="RM22W0E___0000E8G00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR KEY LINKED LOCK AND UNLOCK SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the door key linked lock and unlock switches are functioning properly.</ptxt>
<table pgwide="1">
<title>Main Body ECU</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Door Key Linked Lock SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door key linked lock switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Driver side door key cylinder turned to lock position</ptxt>
<ptxt>OFF: Driver side door key cylinder not turned</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Door Key Linked Unlock SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door key linked unlock switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Driver side door key cylinder turned to unlock position</ptxt>
<ptxt>OFF: Driver side door key cylinder not turned</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On tester screen, each item changes between ON and OFF according to above chart.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TO50ADX_04_0012" fin="true">OK</down>
<right ref="RM000000TO50ADX_04_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0008" proc-id="RM22W0E___0000E8I00000">
<testtitle>CHECK HARNESS AND CONNECTOR (FRONT DOOR LOCK - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LHD:</ptxt>
<test2>
<ptxt>Disconnect the I14 door lock connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E1 and 2D main body ECU connectors.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I14-10 (UL) - E1-10 (UL3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I14-9 (L) - 2D-53 (L2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I14-7 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I14-10 (UL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I14-9 (L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD:</ptxt>
<test2>
<ptxt>Disconnect the I6 door lock connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E1 and 2D main body ECU connectors.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I6-5 (UL) - E1-10 (UL3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I6-6 (L) - 2D-53 (L2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I6-8 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I6-5 (UL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I6-6 (L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000TO50ADX_04_0006" fin="false">OK</down>
<right ref="RM000000TO50ADX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0006" proc-id="RM22W0E___0000E8H00000">
<testtitle>INSPECT FRONT DOOR LOCK ASSEMBLY (DOOR KEY LINKED LOCK AND UNLOCK SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LHD (w/o Double Locking Function):</ptxt>
<figure>
<graphic graphicname="B184437E02" width="7.106578999in" height="2.775699831in"/>
</figure>
<test2>
<ptxt>Remove the front door lock LH (See page <xref label="Seep01" href="RM0000039H001QX"/>).</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>9 (L) - 7 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>9 (L) - 7 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>10 (UL) - 7 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unlock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>10 (UL) - 7 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for LHD (w/ Double Locking Function):</ptxt>
<figure>
<graphic graphicname="B144108E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<test2>
<ptxt>Remove the front door lock LH (See page <xref label="Seep02" href="RM0000039H001QX"/>).</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>9 (L) - 7 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>9 (L) - 7 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>10 (UL) - 7 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unlock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>10 (UL) - 7 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD (w/o Double Locking Function):</ptxt>
<figure>
<graphic graphicname="B144104E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<test2>
<ptxt>Remove the front door lock RH (See page <xref label="Seep03" href="RM0000039H001QX"/>).</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>5 (UL) - 8 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unlock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>5 (UL) - 8 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>6 (L) - 8 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>6 (L) - 8 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD (w/ Double Locking Function):</ptxt>
<figure>
<graphic graphicname="B144105E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<test2>
<ptxt>Remove the front door lock LH (See page <xref label="Seep04" href="RM0000039H001QX"/>).</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>5 (UL) - 8 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unlock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>5 (UL) - 8 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>6 (L) - 8 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>6 (L) - 8 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000TO50ADX_04_0012" fin="true">OK</down>
<right ref="RM000000TO50ADX_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0013">
<testtitle>END (MULTIPLEX NETWORK MASTER SWITCH ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0012">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0014">
<testtitle>REPLACE FRONT DOOR LOCK ASSEMBLY<xref label="Seep01" href="RM0000039H001QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TO50ADX_04_0023">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>