<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000001EA302CX" category="C" type-id="803LU" name-id="BCGH9-01" from="201308">
<dtccode>C1232</dtccode>
<dtcname>Acceleration Sensor Stuck Malfunction</dtcname>
<dtccode>C1243</dtccode>
<dtcname>Acceleration Sensor Stuck Malfunction</dtcname>
<dtccode>C1245</dtccode>
<dtcname>Acceleration Sensor Output Malfunction</dtcname>
<dtccode>C1279</dtccode>
<dtcname>Acceleration Sensor Output Voltage Malfunction (Test Mode DTC)</dtcname>
<subpara id="RM000001EA302CX_01" type-id="60" category="03" proc-id="RM22W0E___0000AGG00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU (master cylinder solenoid) receives signals from the yaw rate sensor assembly via the CAN communication system. </ptxt>
<ptxt>The yaw rate sensor assembly has a built-in acceleration sensor and detects the vehicle's condition using 2 circuits (GL1, GL2). If there is trouble in the bus lines between the yaw rate sensor assembly and the CAN communication system, DTCs U0123 (yaw rate sensor communication trouble) and U0124 (acceleration sensor communication trouble) are stored.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1232</ptxt>
</entry>
<entry valign="middle">
<ptxt>At a vehicle speed of 10 km/h (6 mph) or more, the fluctuation range of the signal from one of either GL1 or GL2 is below 80 mV and the fluctuation range of the signal of the other is higher than 1.9 V for 30 seconds or more.</ptxt>
</entry>
<entry morerows="3" valign="middle">
<list1 type="unordered">
<item>
<ptxt>ECU-IG No. 2 fuse</ptxt>
</item>
<item>
<ptxt>ECU-B1 fuse</ptxt>
</item>
<item>
<ptxt>Sensor installation</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Yaw rate sensor assembly</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1243</ptxt>
</entry>
<entry valign="middle">
<ptxt>The following condition repeats 16 times.</ptxt>
<list1 type="unordered">
<item>
<ptxt>GL1 and GL2 do not change when the vehicle decelerates from 30 km/h (19 mph) to 0 km/h (0 mph).</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1245</ptxt>
</entry>
<entry valign="middle">
<ptxt>The following condition continues for at least 60 seconds.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The difference between the values calculated from the acceleration sensor value and vehicle speed exceeds 0.35 G.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1279</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stored during test mode.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001EA302CX_02" type-id="32" category="03" proc-id="RM22W0E___0000AGH00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C257278E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001EA302CX_03" type-id="51" category="05" proc-id="RM22W0E___0000AGI00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the yaw rate sensor assembly, perform calibration (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001EA302CX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001EA302CX_04_0001" proc-id="RM22W0E___0000AGJ00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00UX"/>). </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check if DTCs U0073, U0123, C1210 and/or C1336 are output (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.01in"/>
<colspec colname="COL2" colwidth="2.07in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC U0073, U0123, C1210 and/or C1336 are not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC U0073 and/or U0123 are output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC U0073 and/or U0123 are output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1210 and/or C1336 are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001EA302CX_04_0008" fin="false">A</down>
<right ref="RM000001EA302CX_04_0004" fin="true">B</right>
<right ref="RM000001EA302CX_04_0011" fin="true">C</right>
<right ref="RM000001EA302CX_04_0005" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000001EA302CX_04_0008" proc-id="RM22W0E___0000AGL00001">
<testtitle>CHECK YAW RATE SENSOR ASSEMBLY INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the yaw rate sensor assembly is installed properly (See page <xref label="Seep01" href="RM000000SS206VX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The sensor is tightened to the specified torque. The sensor is not tilted.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001EA302CX_04_0002" fin="false">OK</down>
<right ref="RM000001EA302CX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001EA302CX_04_0002" proc-id="RM22W0E___0000AGK00001">
<testtitle>CHECK HARNESS AND CONNECTOR (IG/+B/GND TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the yaw rate sensor assembly connector. </ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C208309E49" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E50-4 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E50-6 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E50-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Yaw Rate Sensor Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="6.10in"/>
<colspec colname="COL2" colwidth="0.98in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK (When troubleshooting in accordance with Diagnostic Trouble Code Chart) </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting in accordance with Problem Symptoms Table) </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001EA302CX_04_0003" fin="true">A</down>
<right ref="RM000001EA302CX_04_0006" fin="true">B</right>
<right ref="RM000001EA302CX_04_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001EA302CX_04_0003">
<testtitle>REPLACE YAW RATE SENSOR ASSEMBLY<xref label="Seep01" href="RM000000SS506LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001EA302CX_04_0004">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001EA302CX_04_0005">
<testtitle>REPAIR CIRCUIT INDICATED BY OUTPUT DTC<xref label="Seep01" href="RM0000045Z6012X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001EA302CX_04_0006">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000OS704RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001EA302CX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001EA302CX_04_0009">
<testtitle>INSTALL YAW RATE SENSOR ASSEMBLY CORRECTLY<xref label="Seep01" href="RM000000SS206VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001EA302CX_04_0011">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>