<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000R" variety="S000R">
<name>3UR-FE INTAKE / EXHAUST</name>
<ttl id="12010_S000R_7C3JB_T00CE" variety="T00CE">
<name>EXHAUST PIPE</name>
<para id="RM000002H9Y02GX" category="A" type-id="30014" name-id="IE0YY-02" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000002H9Y02GX_01" type-id="01" category="01">
<s-1 id="RM000002H9Y02GX_01_0025" proc-id="RM22W0E___00001Z100000">
<ptxt>INSTALL NO. 2 HEATED OXYGEN SENSOR (for Bank 1 Sensor 2)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the sensor to the exhaust pipe by hand.</ptxt>
<figure>
<graphic graphicname="A158663" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, tighten the sensor.</ptxt>
<figure>
<graphic graphicname="A148589E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<sst>
<sstitem>
<s-number>09224-00010</s-number>
</sstitem>
</sst>
<torque>
<subtitle>without SST</subtitle>
<torqueitem>
<t-value1>44</t-value1>
<t-value2>449</t-value2>
<t-value4>32</t-value4>
</torqueitem>
<subtitle>with SST</subtitle>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fulcrum Length</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use a torque wrench with a fulcrum length of 300 mm (11.8 in.). If using a torque wrench with a length that is not 300 mm (11.8 in.), calculate the torque specification for the torque wrench and SST based on the "without SST" torque specification (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>Make sure SST and the wrench are connected in a straight line.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002H9Y02GX_01_0024" proc-id="RM22W0E___00001Z200000">
<ptxt>INSTALL HEATED OXYGEN SENSOR (for Bank 2 Sensor 2)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the sensor to the exhaust pipe by hand.</ptxt>
</s2>
<s2>
<ptxt>Using SST, tighten the sensor.</ptxt>
<figure>
<graphic graphicname="A148589E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<sst>
<sstitem>
<s-number>09224-00010</s-number>
</sstitem>
</sst>
<torque>
<subtitle>without SST</subtitle>
<torqueitem>
<t-value1>44</t-value1>
<t-value2>449</t-value2>
<t-value4>32</t-value4>
</torqueitem>
<subtitle>with SST</subtitle>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fulcrum Length</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use a torque wrench with a fulcrum length of 300 mm (11.8 in.). If using a torque wrench with a length that is not 300 mm (11.8 in.), calculate the torque specification for the torque wrench and SST based on the "without SST" torque specification (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</item>
<item>
<ptxt>Make sure SST and the wrench are connected in a straight line.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002H9Y02GX_01_0026" proc-id="RM22W0E___00001Z300000">
<ptxt>INSTALL FRONT EXHAUST PIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the front exhaust pipe to the exhaust manifold RH with 2 new nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>54</t-value1>
<t-value2>551</t-value2>
<t-value4>40</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the wire harness clamp bracket of the oxygen sensor to the transmission with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the heated oxygen sensor connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H9Y02GX_01_0016" proc-id="RM22W0E___00001Z400000">
<ptxt>INSTALL FRONT NO. 2 EXHAUST PIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the front No. 2 exhaust pipe to the exhaust manifold LH with 2 new nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>54</t-value1>
<t-value2>551</t-value2>
<t-value4>40</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the wire harness clamp bracket of the oxygen sensor to the transmission with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the heated oxygen sensor connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H9Y02GX_01_0018" proc-id="RM22W0E___00004VX00000">
<ptxt>INSTALL CENTER EXHAUST PIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 2 new gaskets to the front exhaust pipe and front No. 2 exhaust pipe.</ptxt>
</s2>
<s2>
<ptxt>Install the center exhaust pipe to the 3 exhaust pipe supports, and then install the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>48</t-value1>
<t-value2>489</t-value2>
<t-value4>35</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H9Y02GX_01_0036" proc-id="RM22W0E___00006P600000">
<ptxt>INSTALL EXHAUST PIPE DAMPER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the exhaust pipe damper to the tailpipe with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>194</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H9Y02GX_01_0019" proc-id="RM22W0E___00004VY00000">
<ptxt>INSTALL TAILPIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the tailpipe to the 2 exhaust pipe supports.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket to the center exhaust pipe.</ptxt>
</s2>
<s2>
<ptxt>Connect the tailpipe to the center exhaust pipe.</ptxt>
</s2>
<s2>
<ptxt>Attach a new clamp to the tailpipe and center exhaust pipe. Then install the bolt to the clamp.</ptxt>
<figure>
<graphic graphicname="A240602E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<torque>
<torqueitem>
<t-value1>32</t-value1>
<t-value2>326</t-value2>
<t-value4>24</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gasket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Top</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>LH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Install the clamp within the angle range shown in the illustration.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002H9Y02GX_01_0038">
<ptxt>INSPECT FOR EXHAUST GAS LEAK</ptxt>
</s-1>
<s-1 id="RM000002H9Y02GX_01_0039" proc-id="RM22W0E___00004RG00000">
<ptxt>INSTALL NO. 2 ENGINE UNDER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 engine under cover with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>