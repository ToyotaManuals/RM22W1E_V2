<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T8_T00MB" variety="T00MB">
<name>THEFT DETERRENT SYSTEM (w/ Entry and Start System)</name>
<para id="RM000000W1002UX" category="J" type-id="3011F" name-id="TD63D-02" from="201301">
<dtccode/>
<dtcname>Engine Hood Courtesy Switch Circuit</dtcname>
<subpara id="RM000000W1002UX_01" type-id="60" category="03" proc-id="RM22W0E___0000EK900000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>The engine hood courtesy switch is built into the hood lock. This switch turns on when the engine hood is opened and turns off when the engine hood is closed.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000W1002UX_02" type-id="32" category="03" proc-id="RM22W0E___0000EKA00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B159038E07" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W1002UX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000W1002UX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W1002UX_04_0001" proc-id="RM22W0E___0000EKB00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (ENGINE HOOD COURTESY SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Entry&amp;Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item / Display (Range)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Hood Courtesy Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine hood courtesy switch /</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Engine hood is open</ptxt>
<ptxt>OFF: Engine hood is closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>ON (engine hood is open) appears on screen.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W1002UX_04_0004" fin="true">OK</down>
<right ref="RM000000W1002UX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1002UX_04_0002" proc-id="RM22W0E___0000EKC00000">
<testtitle>INSPECT HOOD LOCK ASSEMBLY (ENGINE HOOD COURTESY SWITCH)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B219990E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>for Hood Courtesy Switch Connector 2 Pin Type (for LHD):</ptxt>
<test2>
<ptxt>Remove the hood lock (See page <xref label="Seep01" href="RM0000039RH00WX"/>).</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Unlock position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lock position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<figure>
<graphic graphicname="B219989E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>for Hood Courtesy Switch Connector 2 Pin Type (for RHD):</ptxt>
<test2>
<ptxt>Remove the hood lock (See page <xref label="Seep02" href="RM0000039RH00WX"/>).</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Unlock position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lock position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<figure>
<graphic graphicname="B216369E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>for Hood Courtesy Switch Connector 4 Pin Type:</ptxt>
<test2>
<ptxt>Remove the hood lock (See page <xref label="Seep03" href="RM0000039RH00WX"/>).</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2 - 4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Unlock position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 - 4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lock position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000W1002UX_04_0003" fin="false">OK</down>
<right ref="RM000000W1002UX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1002UX_04_0003" proc-id="RM22W0E___0000EKD00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU [SMART KEY ECU ASSEMBLY] - HOOD LOCK AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B184352E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>for Hood Courtesy Switch Connector 2 Pin Type:</ptxt>
<test2>
<ptxt>Disconnect the E30 ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the A8 switch connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E30-24 (HSW) - A8-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A8-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-24 (HSW) or A8-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<figure>
<graphic graphicname="B216370E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>for Hood Courtesy Switch Connector 4 Pin Type:</ptxt>
<test2>
<ptxt>Disconnect the E30 ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the A8 switch connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E30-24 (HSW) - A8-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A8-4 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-24 (HSW) or A8-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000W1002UX_04_0006" fin="true">OK</down>
<right ref="RM000000W1002UX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W1002UX_04_0004">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM000000W1002UX_04_0005">
<testtitle>REPLACE HOOD LOCK ASSEMBLY (ENGINE HOOD COURTESY SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000000W1002UX_04_0006">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM000000W1002UX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>