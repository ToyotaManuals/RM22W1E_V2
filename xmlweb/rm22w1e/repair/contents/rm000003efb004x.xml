<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12037_S001R" variety="S001R">
<name>VARIABLE GEAR RATIO STEERING</name>
<ttl id="12037_S001R_7C3QB_T00JE" variety="T00JE">
<name>STEERING CONTROL ECU (for RHD)</name>
<para id="RM000003EFB004X" category="A" type-id="30014" name-id="VG05W-03" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM000003EFB004X_01" type-id="01" category="01">
<s-1 id="RM000003EFB004X_01_0001" proc-id="RM22W0E___0000BBV00000">
<ptxt>INSTALL STEERING CONTROL ECU</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172818E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install the steering control ECU to the junction block with the 2 bolts.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000003EFB004X_01_0015" proc-id="RM22W0E___0000BC100000">
<ptxt>INSTALL STEERING CONTROL ECU WITH JUNCTION BLOCK</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C177318E02" width="2.775699831in" height="5.787629434in"/>
</figure>
<s3>
<ptxt>Front side:</ptxt>
<ptxt>Connect the 3 connectors.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Rear side:</ptxt>
<s3>
<ptxt>Install the steering control ECU with junction block with the bolt and 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Connect the 2 connectors.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003EFB004X_01_0014" proc-id="RM22W0E___0000BAN00000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL SUB-ASSEMBLY (w/o Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180299" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws and connect the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Install the lower instrument panel with the 5 bolts.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0002" proc-id="RM22W0E___0000AT300000">
<ptxt>INSTALL DRIVER SIDE KNEE AIRBAG ASSEMBLY (w/ Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B189604E01" width="2.775699831in" height="6.791605969in"/>
</figure>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the driver side knee airbag with the 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0003" proc-id="RM22W0E___0000A9H00000">
<ptxt>INSTALL NO. 1 SWITCH HOLE BASE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180298" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 claws to install the No. 1 switch hole base.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0004" proc-id="RM22W0E___0000A9I00000">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Attach the 2 claws to install the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to connect the 2 control cables.</ptxt>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 16 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 9 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B182569" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to close the hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0005" proc-id="RM22W0E___0000ATT00000">
<ptxt>INSTALL COWL SIDE TRIM BOARD RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180022" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 clips to install the cowl side trim board.</ptxt>
</s2>
<s2>
<ptxt>Install the cap nut.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0006" proc-id="RM22W0E___000014600000">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY (w/ Floor Under Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182553" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 3 claws to install the No. 1 instrument panel under cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0007" proc-id="RM22W0E___0000BBW00000">
<ptxt>INSTALL FRONT DOOR SCUFF PLATE RH (w/ Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0008" proc-id="RM22W0E___0000BBX00000">
<ptxt>INSTALL FRONT DOOR SCUFF PLATE RH (w/o Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0009" proc-id="RM22W0E___0000BAG00000">
<ptxt>INSTALL NO. 2 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the No. 2 instrument cluster finish panel garnish.</ptxt>
<figure>
<graphic graphicname="B291252E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Entry and Start System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0010" proc-id="RM22W0E___0000BAH00000">
<ptxt>INSTALL NO. 1 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E155133" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws to install the No. 1 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0011" proc-id="RM22W0E___0000BBY00000">
<ptxt>INSTALL INSTRUMENT SIDE PANEL RH (w/ Airbag Cut Off Switch)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B186358" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 6 claws to install the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0012" proc-id="RM22W0E___0000BBZ00000">
<ptxt>INSTALL INSTRUMENT SIDE PANEL RH (w/o Airbag Cut Off Switch)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182551" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 6 claws to install the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003EFB004X_01_0013" proc-id="RM22W0E___0000BC000000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>