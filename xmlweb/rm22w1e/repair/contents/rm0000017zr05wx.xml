<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3VR_T00OU" variety="T00OU">
<name>FRONT AIR CONDITIONING UNIT (for RHD)</name>
<para id="RM0000017ZR05WX" category="A" type-id="80001" name-id="ACBJI-03" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM0000017ZR05WX_01" type-id="01" category="01">
<s-1 id="RM0000017ZR05WX_01_0047" proc-id="RM22W0E___0000GO700000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Set to recirculation mode before removing the air conditioning filter.</ptxt>
</atten3>
<s2>
<ptxt>for Standard:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000038JX01HX_01_0001"/>)</ptxt>
</s2>
<s2>
<ptxt>w/ Winch:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000038JX01GX_01_0001"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0048" proc-id="RM22W0E___0000GO800000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Standard:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000038JX01HX_01_0002"/>)</ptxt>
</s2>
<s2>
<ptxt>w/ Winch:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000038JX01GX_01_0002"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0051" proc-id="RM22W0E___0000GO900000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM000002B5L01IX_01_0213"/>)</ptxt>
</s2>
<s2>
<ptxt>for 1UR-FE:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM000002B5L01JX_01_0187"/>)</ptxt>
</s2>
<s2>
<ptxt>for 3UR-FE:</ptxt>
<ptxt>(See page <xref label="Seep03" href="RM0000031RS01MX_01_0041"/>)</ptxt>
</s2>
<s2>
<ptxt>for 1VD-FTV:</ptxt>
<ptxt>(See page<xref label="Seep04" href="RM0000031FK00PX_02_0289"/> )</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0052" proc-id="RM22W0E___000049F00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 7 clips and radiator support seal.</ptxt>
<figure>
<graphic graphicname="B180890E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000017ZR05WX_01_0028" proc-id="RM22W0E___00004A400000">
<ptxt>RECOVER REFRIGERANT FROM REFRIGERATION SYSTEM
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>Turn the A/C switch on.</ptxt>
</s2>
<s2>
<ptxt>Operate the cooler compressor while the engine speed is approximately 1000 rpm for 5 to 6 minutes to circulate the refrigerant and collect the compressor oil remaining in each component into the cooler compressor.</ptxt>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Recover the refrigerant from the A/C system using a refrigerant recovery unit.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000017ZR05WX_01_0043" proc-id="RM22W0E___0000GO300000">
<ptxt>DRAIN ENGINE COOLANT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000017DS03SX_01_0001"/>)</ptxt>
</s2>
<s2>
<ptxt>for 1UR-FE:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM000003AAX00CX_01_0005"/>)</ptxt>
</s2>
<s2>
<ptxt>for 3UR-FE:</ptxt>
<ptxt>(See page <xref label="Seep03" href="RM0000017DS03TX_01_0001"/>)</ptxt>
</s2>
<s2>
<ptxt>for 1VD-FTV:</ptxt>
<ptxt>(See page <xref label="Seep04" href="RM000001X4H00AX_01_0002"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0053" proc-id="RM22W0E___0000GOA00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0001" proc-id="RM22W0E___0000GNV00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0029" proc-id="RM22W0E___0000GO000000">
<ptxt>DISCONNECT AIR CONDITIONING TUBE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt.</ptxt>
<figure>
<graphic graphicname="E154197E02" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the plate as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Plate</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Disconnect the air conditioning tube assembly.</ptxt>
<figure>
<graphic graphicname="I043960E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not use a screwdriver or similar tool to disconnect the tube.</ptxt>
</item>
<item>
<ptxt>Seal the openings of the disconnected parts using vinyl tape to prevent moisture and foreign matter from entering them.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Disconnect tube by hand</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 2 O-rings from the air conditioning tube assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0005" proc-id="RM22W0E___0000GNW00000">
<ptxt>DISCONNECT HEATER WATER INLET HOSE AND HEATER WATER OUTLET HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using pliers, grip the claws of the clips and slide the 2 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 heater water hoses.</ptxt>
<figure>
<graphic graphicname="E247110E01" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1GR-FE</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1VD-FTE</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*C</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1UR-FE, for 3UR-FE</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0041" proc-id="RM22W0E___0000GO200000">
<ptxt>REMOVE FRONT WIPER MOTOR AND BRACKET</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000002M66014X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0007" proc-id="RM22W0E___0000GNX00000">
<ptxt>REMOVE INSTRUMENT PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000039OQ00KX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0046" proc-id="RM22W0E___0000GO600000">
<ptxt>REMOVE NO. 3 AIR DUCT SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E156902" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clip.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the duct.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0030" proc-id="RM22W0E___0000GO100000">
<ptxt>REMOVE STEERING COLUMN ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Power Tilt and Power Telescopic Steering Column:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000039SI021X"/>)</ptxt>
</s2>
<s2>
<ptxt>for Manual Tilt and Manual Telescopic Steering Column:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000039SI022X"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0044" proc-id="RM22W0E___0000GO400000">
<ptxt>REMOVE HEATER TO REGISTER DUCT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 clips.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the duct.</ptxt>
<figure>
<graphic graphicname="E156935" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0021" proc-id="RM22W0E___0000GNY00000">
<ptxt>REMOVE INSTRUMENT PANEL REINFORCEMENT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 17 clamps and 2 claws and remove the 9 bolts and 5 nuts.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and wire harness.</ptxt>
<figure>
<graphic graphicname="E155239" width="7.106578999in" height="7.795582503in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the instrument panel reinforcement.</ptxt>
<s3>
<ptxt>for Driver Side:</ptxt>
<ptxt>Remove the 3 caps and 3 bolts.</ptxt>
</s3>
<s3>
<ptxt>for Passenger Side:</ptxt>
<ptxt>Remove the 2 caps.</ptxt>
</s3>
<s3>
<ptxt>for Passenger Side:</ptxt>
<ptxt>Using a T40 "TORX" socket, remove the 2 "TORX" bolts.</ptxt>
<atten4>
<ptxt>When removing the bolts, the collars may come off with the bolts.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>for Passenger Side:</ptxt>
<ptxt>Using a 12 mm hexagon wrench, remove the 2 collars.</ptxt>
</s3>
<s3>
<ptxt>Remove the 7 bolts, screw and instrument panel reinforcement.</ptxt>
<figure>
<graphic graphicname="E155490" width="7.106578999in" height="5.787629434in"/>
</figure>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0045" proc-id="RM22W0E___0000GO500000">
<ptxt>REMOVE AIR CONDITIONING HOSE AND ACCESSORY (w/ Cool Box)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154321" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt.</ptxt>
</s2>
<s2>
<ptxt>Using SST, remove the piping clamp.</ptxt>
<sst>
<sstitem>
<s-number>09870-00025</s-number>
<ptxt/>
</sstitem>
</sst>
<s3>
<ptxt>Attach SST to the piping clamp.</ptxt>
<figure>
<graphic graphicname="E112919E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Confirm the direction of the piping clamp claw and SST by referring to the illustration on the caution label.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Push down SST and release the clamp lock.</ptxt>
<figure>
<graphic graphicname="E112920E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to deform the tubes when pushing SST.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Release Lever</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Pull SST slightly and push the release lever, then remove the piping clamp with SST.</ptxt>
</s3>
<s3>
<ptxt>Remove the piping clamp from SST.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Disconnect the hose and accessory from the liquid tube to remove it.</ptxt>
<atten3>
<ptxt>Cap the open fittings immediately to keep moisture or dirt out of the system.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the 2 O-rings from the air conditioning hose and accessory.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017ZR05WX_01_0024" proc-id="RM22W0E___0000GNZ00000">
<ptxt>REMOVE AIR CONDITIONING UNIT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and air conditioning unit.</ptxt>
<figure>
<graphic graphicname="E156861" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>