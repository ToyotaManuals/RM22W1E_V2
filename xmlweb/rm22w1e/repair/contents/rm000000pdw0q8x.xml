<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM000000PDW0Q8X" category="C" type-id="302J4" name-id="ESO10-07" from="201301" to="201308">
<dtccode>P0010</dtccode>
<dtcname>Camshaft Position "A" Actuator Circuit (Bank 1)</dtcname>
<dtccode>P0020</dtccode>
<dtcname>Camshaft Position "A" Actuator Circuit (Bank 2)</dtcname>
<subpara id="RM000000PDW0Q8X_01" type-id="60" category="03" proc-id="RM22W0E___00001I900000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>These DTCs are designed to detect opens or shorts in the camshaft timing oil control valve circuit. If the camshaft timing oil control valve duty cycle is excessively high or low while the engine is running, the ECM will illuminate the MIL and store the DTC.</ptxt>
</item>
<item>
<ptxt>The VVT (variable valve timing) system adjusts the intake and exhaust valve timing to improve driveability. The engine oil pressure turns the camshaft timing gear to adjust the valve timing. The camshaft timing oil control valve is a solenoid valve and switches the engine oil line. The valve moves when the ECM applies 12 V to the solenoid. The ECM changes the energizing time of the solenoid (duty cycle) in accordance with the camshaft position, crankshaft position, throttle position, etc.</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="A192077E17" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0010</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in the camshaft timing oil control valve for intake camshaft (for Bank 1) circuit (1 trip detection logic).</ptxt>
</entry>
<entry valign="bottom">
<list1 type="unordered">
<item>
<ptxt>Open or short in camshaft timing oil control valve for intake camshaft (for Bank 1) circuit</ptxt>
</item>
<item>
<ptxt>Camshaft timing oil control valve assembly for intake camshaft (for Bank 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0020</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in the camshaft timing oil control valve for intake camshaft (for Bank 2) circuit (1 trip detection logic).</ptxt>
</entry>
<entry valign="bottom">
<list1 type="unordered">
<item>
<ptxt>Open or short in camshaft timing oil control valve for intake camshaft (for Bank 2) circuit</ptxt>
</item>
<item>
<ptxt>Camshaft timing oil control valve assembly for intake camshaft (for Bank 2)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>These DTCs relate to the camshaft timing oil control valve.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PDW0Q8X_02" type-id="64" category="03" proc-id="RM22W0E___00001IA00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>These DTCs are designed to detect opens or shorts in the camshaft timing oil control valve circuit. If the camshaft timing oil control valve duty cycle is excessively high or low while the engine is running, the ECM will illuminate the MIL and store the DTC.</ptxt>
<ptxt>The monitor for this DTC runs 5 seconds after the engine switch is turned on (IG).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PDW0Q8X_11" type-id="73" category="03" proc-id="RM22W0E___00001IG00000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<list1 type="ordered">
<item>
<ptxt>Turn the engine switch on (IG) and wait for 5 seconds or more.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000PDW0Q8X_07" type-id="32" category="03" proc-id="RM22W0E___00001IB00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A178641E08" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000PDW0Q8X_08" type-id="51" category="05" proc-id="RM22W0E___00001IC00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PDW0Q8X_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PDW0Q8X_09_0001" proc-id="RM22W0E___00001ID00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs after recording the freeze frame data and DTCs (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and allow it to idle. Then check for DTCs.</ptxt>
</test1>
<test1>
<ptxt>Check that P0010 or P0020 is not output.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>P0010 or P0020 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDW0Q8X_09_0005" fin="true">OK</down>
<right ref="RM000000PDW0Q8X_09_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDW0Q8X_09_0002" proc-id="RM22W0E___00001IE00000">
<testtitle>INSPECT CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the camshaft timing oil control valve assembly (See page <xref label="Seep01" href="RM000002PPY02VX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDW0Q8X_09_0003" fin="false">OK</down>
<right ref="RM000000PDW0Q8X_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDW0Q8X_09_0003" proc-id="RM22W0E___00001IF00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CAMSHAFT TIMING OIL CONTROL VALVE - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the camshaft timing oil control valve connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" align="left" colwidth="3.54in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C102-1 - C46-58 (OC1+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C103-1 - C46-52 (OC2+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C102-2 - C46-57 (OC1-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C103-2 - C46-51 (OC2-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C102-1 or C46-58 (OC1+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C103-1 or C46-52 (OC2+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C102-2 or C46-57 (OC1-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C103-2 or C46-51 (OC2-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" align="left" colwidth="3.54in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C102-1 - C45-58 (OC1+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C103-1 - C45-52 (OC2+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C102-2 - C45-57 (OC1-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C103-2 - C45-51 (OC2-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C102-1 or C45-58 (OC1+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C103-1 or C45-52 (OC2+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C102-2 or C45-57 (OC1-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C103-2 or C45-51 (OC2-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDW0Q8X_09_0007" fin="true">OK</down>
<right ref="RM000000PDW0Q8X_09_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDW0Q8X_09_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDW0Q8X_09_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000PDW0Q8X_09_0006">
<testtitle>REPLACE CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY<xref label="Seep01" href="RM00000321401QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDW0Q8X_09_0005">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ108X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>