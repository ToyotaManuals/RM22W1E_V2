<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3OQ_T00HT" variety="T00HT">
<name>RELAY</name>
<para id="RM000003CPI00BX" category="G" type-id="8000T" name-id="SC0T1-05" from="201301" to="201308">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM000003CPI00BX_01" type-id="01" category="01">
<s-1 id="RM000003CPI00BX_01_0001" proc-id="RM22W0E___00009ZH00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003CPI00BX_01_0002" proc-id="RM22W0E___00009ZI00000">
<ptxt>REMOVE SUSPENSION CONTROL RELAY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the suspension control relay from the engine room relay block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003CPI00BX_01_0003" proc-id="RM22W0E___00009ZJ00000">
<ptxt>INSPECT SUSPENSION CONTROL RELAY (AHC)</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C149624E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>3 - 5</ptxt>
</entry>
<entry align="left">
<ptxt>Battery voltage is not applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="left">
<ptxt>Battery voltage is applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, replace the suspension control relay.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM000003CPI00BX_01_0004" proc-id="RM22W0E___00009ZK00000">
<ptxt>INSTALL SUSPENSION CONTROL RELAY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the relay to the engine room relay block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003CPI00BX_01_0005" proc-id="RM22W0E___00009ZL00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>