<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WB_T00PE" variety="T00PE">
<name>ROOM TEMPERATURE SENSOR (for Front Side)</name>
<para id="RM000000Y0G036X" category="G" type-id="3001K" name-id="AC052-17" from="201301">
<name>INSPECTION</name>
<subpara id="RM000000Y0G036X_01" type-id="01" category="01">
<s-1 id="RM000000Y0G036X_01_0001" proc-id="RM22W0E___0000GXO00000">
<ptxt>INSPECT ROOM TEMPERATURE SENSOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E160595E01" width="2.775699831in" height="5.787629434in"/>
</figure>
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="top">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="top">
<ptxt>Condition</ptxt>
</entry>
<entry valign="top">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>10°C (50°F)</ptxt>
</entry>
<entry>
<ptxt>3.00 to 3.73 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>15°C (59°F)</ptxt>
</entry>
<entry>
<ptxt>2.45 to 2.88 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry>
<ptxt>1.95 to 2.30 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>25°C (77°F)</ptxt>
</entry>
<entry>
<ptxt>1.60 to 1.80 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>30°C (86°F)</ptxt>
</entry>
<entry>
<ptxt>1.28 to 1.47 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>35°C (95°F)</ptxt>
</entry>
<entry>
<ptxt>1.00 to 1.22 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>40°C (104°F)</ptxt>
</entry>
<entry>
<ptxt>0.80 to 1.00 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>45°C (113°F)</ptxt>
</entry>
<entry>
<ptxt>0.65 to 0.85 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>50°C (122°F)</ptxt>
</entry>
<entry>
<ptxt>0.50 to 0.70 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>55°C (131°F)</ptxt>
</entry>
<entry>
<ptxt>0.44 to 0.60 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>60°C (140°F)</ptxt>
</entry>
<entry>
<ptxt>0.36 to 0.50 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the room temperature sensor.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Touching the sensor even slightly may change the resistance value. Hold the connector of the sensor.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>When measuring the resistance, the sensor temperature must be the same as the ambient temperature.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>As the temperature increases, the resistance decreases (See the graph).</ptxt>
</atten4>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>