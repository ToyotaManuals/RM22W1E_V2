<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12048_S001Z" variety="S001Z">
<name>1GR-FE BATTERY / CHARGING</name>
<ttl id="12048_S001Z_7C3S3_T00L6" variety="T00L6">
<name>GENERATOR (for 130 A Type)</name>
<para id="RM000001ONM03WX" category="A" type-id="30014" name-id="BH18J-01" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM000001ONM03WX_01" type-id="01" category="01">
<s-1 id="RM000001ONM03WX_01_0001" proc-id="RM22W0E___000047200000">
<ptxt>INSTALL GENERATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the generator bracket to the generator with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the generator with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>43</t-value1>
<t-value2>438</t-value2>
<t-value4>32</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the generator bracket with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the wire harness clamp bracket with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the generator wire to terminal B with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Close the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Connect the generator connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001ONM03WX_01_0060" proc-id="RM22W0E___000048500000">
<ptxt>INSTALL FRONT FENDER APRON TRIM PACKING B
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ KDSS:</ptxt>
<ptxt>Install the front fender apron trim packing B with the 3 clips.</ptxt>
</s2>
<s2>
<ptxt>w/o KDSS:</ptxt>
<ptxt>Install the front fender apron trim packing B with the 4 clips.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001ONM03WX_01_0029" proc-id="RM22W0E___000042G00000">
<ptxt>INSTALL FAN AND GENERATOR V BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the V belt onto every part.</ptxt>
<figure>
<graphic graphicname="A225650E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Vane Pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Water Pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>No. 2 Idler</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Generator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Cooler Compressor or Idler Pulley</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry>
<ptxt>No. 1 Idler</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry>
<ptxt>Crankshaft</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry>
<ptxt>V-ribbed Belt Tensioner</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>While turning the belt tensioner counterclockwise, remove the pin.</ptxt>
<atten3>
<ptxt>Make sure that the V belt is properly installed to each pulley.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Check that the belt fits properly in the ribbed grooves.</ptxt>
<atten4>
<ptxt>Make sure to check by hand that the belt has not slipped out of the grooves on the bottom of the pulley.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000001ONM03WX_01_0066" proc-id="RM22W0E___000042J00000">
<ptxt>INSPECT FAN AND GENERATOR V BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the belt for wear, cracks or other signs of damage.</ptxt>
<figure>
<graphic graphicname="A139581" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If any of the following defects is found, replace the fan and generator V belt.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The belt is cracked.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>The belt is worn out to the extent that the cords are exposed.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>The belt has chunks missing from the ribs.</ptxt>
</item>
</list1>
</s2>
<s2>
<ptxt>Check that the belt fits properly in the ribbed grooves.</ptxt>
<figure>
<graphic graphicname="A139582E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Check with your hand to confirm that the belt has not slipped out of the groove on the bottom of the pulley. If it has slipped out, replace the fan and generator V belt. Install a new fan and generator V belt correctly.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000001ONM03WX_01_0067" proc-id="RM22W0E___000011P00000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover with the 10 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000001ONM03WX_01_0064" proc-id="RM22W0E___000011S00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001ONM03WX_01_0065" proc-id="RM22W0E___000011Q00000">
<ptxt>INSTALL FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the clip to install the front fender splash shield sub-assembly RH.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001ONM03WX_01_0046" proc-id="RM22W0E___00000Z200000">
<ptxt>INSTALL V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 V-bank cover hooks to the bracket. Then align the 2 V-bank cover grommets with the 2 pins and press down on the V-bank cover to attach the pins.</ptxt>
<figure>
<graphic graphicname="A271365E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000001ONM03WX_01_0044" proc-id="RM22W0E___0000D2N00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>