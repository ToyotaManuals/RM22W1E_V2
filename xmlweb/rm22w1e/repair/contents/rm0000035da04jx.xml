<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001X" variety="S001X">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001X_7C3RY_T00L1" variety="T00L1">
<name>PARKING ASSIST MONITOR SYSTEM (w/ Side Monitor System)</name>
<para id="RM0000035DA04JX" category="D" type-id="303F2" name-id="PM2A8-91" from="201308">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM0000035DA04JX_z0" proc-id="RM22W0E___0000CZS00001">
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>*: w/ Variable Gear Ratio Steering System</ptxt>
</item>
</list1>
<step1>
<ptxt>GENERAL</ptxt>
<step2>
<ptxt>This system has a rear television camera assembly mounted on the back door to display the rear view of the vehicle on the multi-display assembly. The display panel also shows a composite view consisting of the area behind the vehicle and parking guidelines to assist the driver in parking the vehicle by monitoring the area behind the vehicle.</ptxt>
</step2>
<step2>
<ptxt>This system consists of the following components:</ptxt>
<step3>
<ptxt>Parking assist ECU</ptxt>
</step3>
<step3>
<ptxt>Rear television camera assembly</ptxt>
</step3>
<step3>
<ptxt>Multi-media module receiver assembly</ptxt>
</step3>
<step3>
<ptxt>Multi-display assembly</ptxt>
</step3>
<step3>
<ptxt>Speaker</ptxt>
</step3>
<step3>
<ptxt>Steering sensor</ptxt>
</step3>
<step3>
<ptxt>Steering control ECU*</ptxt>
</step3>
<step3>
<ptxt>Master cylinder solenoid (skid control ECU assembly)</ptxt>
</step3>
<step3>
<ptxt>Four wheel drive control ECU</ptxt>
</step3>
<step3>
<ptxt>ECM</ptxt>
</step3>
<step3>
<ptxt>Main body ECU (cowl side junction block LH)*</ptxt>
</step3>
</step2>
<step2>
<ptxt>This system is equipped with a self-diagnosis system, which is operated on a designated window that appears on the display panel, just as in the navigation system.</ptxt>
</step2>
</step1>
<step1>
<ptxt>FUNCTION OF COMPONENTS</ptxt>
<step2>
<ptxt>The parking assist ECU controls the system by using information from the following components.</ptxt>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.83in"/>
<colspec colname="COL2" colwidth="4.25in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Rear Television Camera Assembly</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Mounted on the back door to transmit the rear view of the vehicle to the parking assist ECU.</ptxt>
</item>
<item>
<ptxt>Has a color video camera that uses a Complementary Metal Oxide Semiconductor (CMOS) and a wide-angle lens.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Parking Assist ECU</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Receives video signals, which contain an image of the area behind the vehicle taken with the rear television camera assembly.</ptxt>
</item>
<item>
<ptxt>Performs overall control of the system by receiving signals from the sensors.</ptxt>
</item>
<item>
<ptxt>Allows operation via the touch panel of the display when the park assist monitor screen or diagnosis screen is displayed.</ptxt>
</item>
<item>
<ptxt>Creates images and displays adjustment screens performed on the diagnosis screen.</ptxt>
</item>
<item>
<ptxt>Stops displaying the guide line and buttons displays "Check surrounding for safety" when an open signal is received for the back door.</ptxt>
</item>
<item>
<ptxt>Displays "Back door is open" on the back camera position setting screen when an open signal is received for the back door. </ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Multi-media Module Receiver Assembly</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Receives the video signals containing a composite of the rear view of the vehicle and parking assist guideline signals from the parking assist ECU, and sends them on the multi-display assembly (display panel).</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Steering Sensor</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Detects the angle of the steering wheel and sends the resulting signals to the parking assist ECU through CAN communication.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Steering Control ECU*1</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Sends the VGRS relative angle and vehicle condition to the parking assist ECU through CAN communication.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Master Cylinder Solenoid (Skid Control ECU Assembly)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Sends the inner brake information and ABS ACT information to the parking assist ECU through CAN communication.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Four Wheel Drive Control ECU</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Sends the L4 condition signal to the parking assist ECU through CAN communication.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Sends the vehicle condition, 2WD/4WD information and shift position information to the parking assist ECU through CAN communication..</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Main Body ECU (Cowl Side Junction Block LH)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Sends the back door courtesy light switch signal to the parking assist ECU through CAN communication.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>OPERATION EXPLANATION</ptxt>
<step2>
<ptxt>The reverse position signal is sent from the shift lever position switch to the parking assist ECU when the shift lever is moved to R.</ptxt>
</step2>
<step2>
<ptxt>In parallel parking assist mode, an appropriate steering angle and timing information can be provided for the driver. This is based on the information from the steering angle sensor signal and the vehicle angle data signal that are sent to the parking assist ECU.</ptxt>
<atten4>
<ptxt>The steering angle sensor signal is used to control parking assist for only estimated guide line mode.</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>DISPLAY MODE SETTING</ptxt>
<figure>
<graphic graphicname="E226209E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" align="center" colwidth="0.71in"/>
<colspec colname="COL2" align="left" colwidth="2.83in"/>
<colspec colname="COL3" align="center" colwidth="0.71in"/>
<colspec colname="COL4" align="left" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Line Mode Button</ptxt>
</entry>
<entry valign="middle">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Estimated Guide Line Mode</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Parking Guide Line Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Guide Line Deletion Mode</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Line Mode Button Pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<step2>
<ptxt>While the parking assist monitor is displayed, pressing the line mode button switches the parking assist monitor display mode.</ptxt>
<table pgwide="1">
<title>Parking Assist Monitor Display Mode</title>
<tgroup cols="5" align="center">
<colspec colname="COL3" colwidth="2.41in"/>
<colspec colname="COLSPEC4" colwidth="1.17in"/>
<colspec colname="COLSPEC5" colwidth="1.17in"/>
<colspec colname="COLSPEC6" colwidth="1.17in"/>
<colspec colname="COLSPEC0" colwidth="1.16in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Parking Assist Monitor Display Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>Distance Guide Line </ptxt>
<ptxt>(Red)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle Width Extension Line</ptxt>
<ptxt>(Blue)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Predicted Path Line</ptxt>
<ptxt>(Yellow)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Parking Guide Line</ptxt>
<ptxt>(Blue)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Estimated Guide Line Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not displayed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Parking Guide Line Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Displayed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Guide Line Deletion Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not displayed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>COMMUNICATION SYSTEM OUTLINE</ptxt>
<step2>
<ptxt>The components of the parking assist monitor system (w/ Side Monitor System) communicate with each other through the AVC-LAN. Also, parallel parking assist mode judges the vehicle angle data transmitted via the AVC-LAN from the multi-media module receiver assembly (the data is calculated by the multi-media module receiver assembly by integrating the yaw rate of the gyro sensor built into the multi-media module receiver assembly).</ptxt>
</step2>
<step2>
<ptxt>If a short circuit or open circuit occurs in the AVC-LAN, communication is interrupted and the parking assist monitor system (w/ Side Monitor System) will stop functioning.</ptxt>
</step2>
</step1>
<step1>
<ptxt>DIAGNOSTIC FUNCTION OUTLINE</ptxt>
<step2>
<ptxt>This parking assist monitor system (w/ Side Monitor System) has a diagnostic function displayed in the multi-media module receiver assembly. This function enables the calibration (adjustment and verification) of the parking assist monitor system (w/ Side Monitor System) (See page <xref label="Seep01" href="RM000003WVZ03QX"/>).</ptxt>
</step2>
<step2>
<ptxt>The parking assist monitor system (w/ Side Monitor System) can check the following items by using the intelligent tester.</ptxt>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>DTC</ptxt>
</entry>
<entry valign="middle">
<ptxt>
<xref label="Seep02" href="RM0000035DB03WX"/>
</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Data List / Active Test</ptxt>
</entry>
<entry valign="middle">
<ptxt>
<xref label="Seep03" href="RM000003WW0039X"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>