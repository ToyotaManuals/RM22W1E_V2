<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3ML_T00FO" variety="T00FO">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV)</name>
<para id="RM000000W7E0VZX" category="U" type-id="303FJ" name-id="AT01QX-232" from="201308">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM000000W7E0VZX_z0" proc-id="RM22W0E___000085R00001">
<content5 releasenbr="1">
<step1>
<ptxt>READ DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
<table pgwide="1">
<title>ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle speed/</ptxt>
<ptxt>Min.: 0 km/h (0 mph)</ptxt>
<ptxt>Max.: 255 km/h (158 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual vehicle speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine speed/</ptxt>
<ptxt>Min.: 0 rpm</ptxt>
<ptxt>Max.: 16383.75 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idling: 550 to 650 rpm</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Calculate Load</ptxt>
</entry>
<entry valign="middle">
<ptxt>Calculated load/</ptxt>
<ptxt>Min.: 0%</ptxt>
<ptxt>Max.: 100%</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Idling: 15.4 to 20.4%</ptxt>
</item>
<item>
<ptxt>Running without load (2500 rpm): 20.1 to 25.9%</ptxt>
</item>
<item>
<ptxt>Driving with the accelerator fully open at 3000 rpm: 80.3 to 99.6%</ptxt>
</item>
<item>
<ptxt>Driving with the accelerator fully open at 4000 rpm: 96.4 to 98%</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Coolant Temp</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine coolant temperature/</ptxt>
<ptxt>Min.: -40°C (-40°F)</ptxt>
<ptxt>Max.: 140°C (284°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>After warming up engine: 75 to 90°C (167 to 194°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If the value is -40°C (-40°F), the sensor circuit is open.</ptxt>
<ptxt>If the value is 140°C (284°F), the sensor circuit is shorted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine Run Time</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine run time/</ptxt>
<ptxt>Min.: 0 seconds</ptxt>
<ptxt>Max.: 65535 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Time after the ignition switch turned ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery Voltage</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery voltage/</ptxt>
<ptxt>Min.: 0 V</ptxt>
<ptxt>Max.: 65.535 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine idling: 11 to 14 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Neutral Position SW Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In P or N: ON</ptxt>
</item>
<item>
<ptxt>Not in P or N: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep01" href="RM000000W8412IX"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Brake pedal depressed</ptxt>
</item>
<item>
<ptxt>OFF: Brake pedal released</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Check Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>Check mode/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Check mode on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt># Codes (Include History)</ptxt>
</entry>
<entry valign="middle">
<ptxt># of Codes/</ptxt>
<ptxt>Min.: 0</ptxt>
<ptxt>Max.: 255</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>The number of stored DTCs.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL</ptxt>
</entry>
<entry valign="middle">
<ptxt>MIL status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: MIL on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MIL ON Run Distance</ptxt>
</entry>
<entry valign="middle">
<ptxt>MIL on run distance/</ptxt>
<ptxt>Min.: 0 km (0 mile)</ptxt>
<ptxt>Max.: 65535 km (40723 mile)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Distance after DTC stored</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Running Time from MIL ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Running time from MIL on/</ptxt>
<ptxt>Min.: 0 minutes</ptxt>
<ptxt>Max.: 65535 minutes</ptxt>
</entry>
<entry valign="middle">
<ptxt>Equivalent to running time after MIL turns on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Time after DTC Cleared</ptxt>
</entry>
<entry valign="middle">
<ptxt>Time after DTCs cleared/</ptxt>
<ptxt>Min.: 0 minutes</ptxt>
<ptxt>Max.: 65535 minutes</ptxt>
</entry>
<entry valign="middle">
<ptxt>Equivalent to time after DTCs cleared</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Distance from DTC Cleared</ptxt>
</entry>
<entry valign="middle">
<ptxt>Distance driven after DTCs cleared/</ptxt>
<ptxt>Min.: 0 km (0 mile)</ptxt>
<ptxt>Max.: 65535 km (40723 mile)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Equivalent to distance driven after DTCs cleared</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Warmup Cycle Cleared DTC</ptxt>
</entry>
<entry valign="middle">
<ptxt>Warm-up cycles after DTCs cleared/</ptxt>
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Number of warm-up cycles after DTCs are cleared.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Number of Emission DTC</ptxt>
</entry>
<entry valign="middle">
<ptxt>Emissions-related DTCs/</ptxt>
<ptxt>Min.: 0, Max.: 127</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Model Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>Model code/</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine Type</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Cylinder Number</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder number/</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Destination</ptxt>
</entry>
<entry valign="middle">
<ptxt>Destination/</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Model Year</ptxt>
</entry>
<entry valign="middle">
<ptxt>Model year/</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine Speed from EFI</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine speed/</ptxt>
<ptxt>Min.: 0 rpm</ptxt>
<ptxt>Max.: 16383.75 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idling: 550 to 650 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>The engine speed received from the ECM.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Intake-Air Temp from EFI</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake air temperature/</ptxt>
<ptxt>Min.: -40°C (-40°F)</ptxt>
<ptxt>Max.: 215°C (419°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Equivalent to temperature at intake manifold</ptxt>
</entry>
<entry valign="middle">
<ptxt>The intake air temperature received from the ECM.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Coolant Temp from EFI</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine coolant temperature/</ptxt>
<ptxt>Min.: -40°C (-40°F)</ptxt>
<ptxt>Max.: 215°C (419°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>After warming up engine: 75 to 90°C (167 to 194°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The coolant temperature received from the ECM.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Accel Position from EFI</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accelerator sensor positioning/</ptxt>
<ptxt>Min.: 0%</ptxt>
<ptxt>Max.: 100%</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idling: 0 to 10%</ptxt>
</entry>
<entry valign="middle">
<ptxt>The accelerator position received from the ECM.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Injection Volume from EFI</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injection volume/</ptxt>
<ptxt>Min.: 0 mm3/st</ptxt>
<ptxt>Max.: 639.98 mm3/st</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idling: 3.0 to 10 mm3/st</ptxt>
</entry>
<entry valign="middle">
<ptxt>The injector volume received from the ECM.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SPD (NT)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Input shaft speed/</ptxt>
<ptxt>Min.: 0 rpm</ptxt>
<ptxt>Max.: 12750 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up:</ptxt>
<list1 type="unordered">
<item>
<ptxt>ON (after warming up engine): Input turbine speed (NT) equal to engine speed</ptxt>
</item>
<item>
<ptxt>OFF (idling with shift lever in N): Input turbine speed (NT) nearly equal to engine speed</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>The data is displayed in increments of 50 rpm.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SPD (SP2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output shaft speed/</ptxt>
<ptxt>Min.: 0 km/h (0 mph)</ptxt>
<ptxt>Max.: 255 km/h (158 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle stopped: 0 km/h (0 mph) (output shaft speed equal to vehicle speed)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (P Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In P: ON</ptxt>
</item>
<item>
<ptxt>Not in P: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep03" href="RM000000W8412IX"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Pattern Switch (PWR/M)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern switch (PWR) status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern switch (PWR):</ptxt>
<list1 type="unordered">
<item>
<ptxt>Pushed: ON</ptxt>
</item>
<item>
<ptxt>Not pushed: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (R Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In R: ON</ptxt>
</item>
<item>
<ptxt>Not in R: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep02" href="RM000000W8412IX"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (N Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In N: ON</ptxt>
</item>
<item>
<ptxt>Not in N: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep04" href="RM000000W8412IX"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Sports Shift Up SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sport shift up switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Shift lever held in "+" (up-shift)</ptxt>
</item>
<item>
<ptxt>OFF: Shift lever not in "+" (up-shift)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Sports Shift Down SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sport shift down switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Shift lever held in "-" (down-shift)</ptxt>
</item>
<item>
<ptxt>OFF: Shift lever not in "-" (down-shift)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (D Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In D: ON</ptxt>
</item>
<item>
<ptxt>Not in D: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep05" href="RM000000W8412IX"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A/T Oil Temperature 1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 ATF temperature sensor value/</ptxt>
<ptxt>Min.: -40°C (-40°F)</ptxt>
<ptxt>Max.: 215°C (419°F)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>After stall test: Approximately 80°C (176°F)</ptxt>
</item>
<item>
<ptxt>With engine cold: Equal to ambient temperature</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>If the value is -40°C (-40°F) or 215°C (419°F), the No. 1 ATF temperature sensor circuit is open or shorted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A/T Oil Temperature 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 ATF temperature sensor value/</ptxt>
<ptxt>Min.: -40°C (-40°F)</ptxt>
<ptxt>Max.: 215°C (419°F)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>After stall test: Approximately 80°C (176°F)</ptxt>
</item>
<item>
<ptxt>With engine cold: Equal to ambient temperature</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>If the value is -40°C (-40°F) or 215°C (419°F), the No. 2 ATF temperature sensor circuit is open or shorted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lock Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Operating: ON</ptxt>
</item>
<item>
<ptxt>Not operating: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lock Up Solenoid Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up solenoid status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up solenoid:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Operating: ON</ptxt>
</item>
<item>
<ptxt>Not operating: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM gear shift command/</ptxt>
<ptxt>1st, 2nd, 3rd, 4th, 5th or 6th</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever:</ptxt>
<list1 type="unordered">
<item>
<ptxt>In D: 1st, 2nd, 3rd, 4th, 5th or 6th</ptxt>
</item>
<item>
<ptxt>In S: 1st, 2nd, 3rd, 4th, 5th or 6th</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SLT Solenoid Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift solenoid valve SLT status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal depressed: OFF</ptxt>
</item>
<item>
<ptxt>Accelerator pedal released: ON</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SLU Solenoid Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift solenoid valve SLU status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift solenoid valve SLU:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Operating: ON</ptxt>
</item>
<item>
<ptxt>Not operating: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Snow or 2nd Start Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>2nd Start mode status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: 2nd Start mode on</ptxt>
</item>
<item>
<ptxt>OFF: 2nd Start mode off</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>PERFORM ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / ECT / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, perform the Active Test.</ptxt>
<table pgwide="1">
<title>ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Connect the TC and TE1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turn on and off TC and TE1 connection </ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: TC and TE1 are connected.</ptxt>
</item>
<item>
<ptxt>OFF: TC and TE1 are disconnected.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SLU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve SLU</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>The engine is stopped.</ptxt>
</item>
<item>
<ptxt>The shift lever is in P or N.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SLT)*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve SLT and raise line pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>OFF: Line pressure up (when Active Test "Activate the Solenoid (SLT)" is performed, ECM commands shift solenoid valve SLT solenoid to turn OFF)</ptxt>
</item>
<item>
<ptxt>ON: No action (normal operation)</ptxt>
</item>
</list1>
</atten4>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>The vehicle is stopped.</ptxt>
</item>
<item>
<ptxt>The engine is idling.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (S1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>The engine is stopped.</ptxt>
</item>
<item>
<ptxt>The shift lever is in P or N.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (S2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve S2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>The engine is stopped.</ptxt>
</item>
<item>
<ptxt>The shift lever is in P or N.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (S3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve S3</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>The engine is stopped.</ptxt>
</item>
<item>
<ptxt>The shift lever is in P or N.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (S4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve S4</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>The engine is stopped.</ptxt>
</item>
<item>
<ptxt>The shift lever is in P or N.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Lock Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control shift solenoid valve SLU to set automatic transmission to lock-up condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>It is possible to check the shift solenoid valve SLU operation.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>The throttle valve opening angle is less than 35%.</ptxt>
</item>
<item>
<ptxt>The vehicle speed is 60 km/h (37 mph) or more.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Control the Shift Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve and set each shift position</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Press "→" button: Shift up</ptxt>
</item>
<item>
<ptxt>Press "←" button: Shift down</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>It is possible to check the operation of the shift solenoid valves.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<ptxt>The vehicle speed is 50 km/h (31 mph) or less.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve SR</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>The engine is stopped.</ptxt>
</item>
<item>
<ptxt>The shift lever is in P or N.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SL1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve SL1</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>The engine is stopped.</ptxt>
</item>
<item>
<ptxt>The shift lever is in P or N.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SL2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve SL2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>The engine is stopped.</ptxt>
</item>
<item>
<ptxt>The shift lever is in P or N.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: Activate the Solenoid (SLT) in the Active Test is performed to check the line pressure changes by connecting SST to the automatic transmission, which is used in the Hydraulic Test (See page <xref label="Seep07" href="RM000000W7B0POX"/>) as well. Note that the pressure values in the Active Test and hydraulic test are different.</ptxt>
</atten4>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>