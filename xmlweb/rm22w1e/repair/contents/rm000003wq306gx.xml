<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R3_T00K6" variety="T00K6">
<name>AUDIO AND VISUAL SYSTEM (w/ Navigation System)</name>
<para id="RM000003WQ306GX" category="C" type-id="803XU" name-id="AVCDH-01" from="201301" to="201308">
<dtccode>B15F6</dtccode>
<dtcname>Main Body ECU Vehicle Information Reading/Writing Process Malfunction</dtcname>
<subpara id="RM000003WQ306GX_01" type-id="60" category="03" proc-id="RM22W0E___0000C2F00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when items controlled by the main body ECU (cowl side junction block LH) cannot be customized via the audio and visual system vehicle customization screen.</ptxt>
<atten4>
<list1 type="unordered">
<title>The main body ECU (cowl side junction block LH) controls the items for the following systems that are customizable via the multi-display assembly screen:</title>
<item>
<ptxt>Power door lock control system</ptxt>
</item>
<item>
<ptxt>Wireless door lock control system</ptxt>
</item>
<item>
<ptxt>Lighting system</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B15F6</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Main body ECU (cowl side junction block LH) vehicle setting processing error.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>Power door lock control system</ptxt>
</item>
<item>
<ptxt>Wireless door lock control system</ptxt>
</item>
<item>
<ptxt>Lighting system</ptxt>
</item>
<item>
<ptxt>Main body ECU (cowl side junction block LH)</ptxt>
</item>
<item>
<ptxt>Multi-media module receiver assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003WQ306GX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003WQ306GX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003WQ306GX_03_0001" proc-id="RM22W0E___0000C2G00000">
<testtitle>CHECK CAN COMMUNICATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally. </ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD (See page <xref label="Seep01" href="RM000001RSO08CX"/>)</ptxt>
</item>
<item>
<ptxt>for RHD (See page <xref label="Seep02" href="RM000001RSO08DX"/>)</ptxt>
</item>
</list1>
<spec>
<title>OK</title>
<specitem>
<ptxt>CAN communication DTCs are not output.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003WQ306GX_03_0002" fin="false">A</down>
<right ref="RM000003WQ306GX_03_0005" fin="true">B</right>
<right ref="RM000003WQ306GX_03_0018" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0002" proc-id="RM22W0E___0000C2H00000">
<testtitle>CHECK POWER DOOR LOCK CONTROL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the power door lock control system is malfunctioning (See page <xref label="Seep01" href="RM000000TNQ04DX"/>).</ptxt>
<atten4>
<ptxt>Customization may not be possible depending on the malfunction in the power door lock control system.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>No malfunctions are present in the power door lock control system.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WQ306GX_03_0008" fin="false">OK</down>
<right ref="RM000003WQ306GX_03_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0008" proc-id="RM22W0E___0000C2J00000">
<testtitle>CHECK WIRELESS DOOR LOCK CONTROL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the wireless door lock control system is malfunctioning (See page <xref label="Seep01" href="RM000000XX205DX"/>).</ptxt>
<atten4>
<ptxt>Customization may not be possible depending on the malfunction in the wireless door lock control system.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>No malfunctions are present in the wireless door lock control system.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WQ306GX_03_0011" fin="false">OK</down>
<right ref="RM000003WQ306GX_03_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0011" proc-id="RM22W0E___0000C2K00000">
<testtitle>CHECK LIGHTING SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the lighting system is malfunctioning (See page <xref label="Seep01" href="RM000002WKL01CX"/>).</ptxt>
<atten4>
<ptxt>Customization may not be possible depending on the malfunction in the lighting system.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>No malfunctions are present in the lighting system.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WQ306GX_03_0003" fin="false">OK</down>
<right ref="RM000003WQ306GX_03_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0003" proc-id="RM22W0E___0000C2I00000">
<testtitle>CHECK CUSTOMIZE ITEMS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Display the "Vehicle Customization" screen.</ptxt>
</test1>
<test1>
<ptxt>Make a note of the customizable items for each main body ECU (cowl side junction block LH) related system.</ptxt>
<atten4>
<ptxt>These items shown as customizable via the multi-display assembly vehicle customization screen can also be changed using the intelligent tester. In a following step, the intelligent tester will be used.</ptxt>
</atten4>
<test2>
<ptxt>To match-up the items in the multi-display assembly with the items on the intelligent tester, check the customize parameters list for the power door lock control system related items that can be customized via the multi-display assembly screen (See page <xref label="Seep01" href="RM000000WI306OX"/>).</ptxt>
</test2>
<test2>
<ptxt>To match-up the items in the multi-display assembly with the items on the intelligent tester, check the customize parameters list for the wireless door lock control system related items that can be customized via the multi-display assembly screen (See page <xref label="Seep02" href="RM000000XXC06SX"/>).</ptxt>
</test2>
<test2>
<ptxt>To match-up the items in the multi-display assembly with the items on the intelligent tester, check the customize parameters list for the lighting system related items that can be customized via the multi-display assembly screen (See page <xref label="Seep04" href="RM000002X0L038X"/>).</ptxt>
</test2>
</test1>
<test1>
<ptxt>Using the intelligent tester, confirm that it is possible to change the settings of the customize parameters that are available for customization via the multi-display assembly screen.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The items available for customization via the multi-display assembly screen can be customized successfully using the intelligent tester.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003WQ306GX_03_0007" fin="true">OK</down>
<right ref="RM000003WQ306GX_03_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0007">
<testtitle>REPLACE MULTI-MEDIA MODULE RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0006">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM<xref label="Seep01" href="RM000000TNQ04DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0013">
<testtitle>GO TO WIRELESS DOOR LOCK CONTROL SYSTEM<xref label="Seep01" href="RM000000XX205DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0016">
<testtitle>GO TO LIGHTING SYSTEM<xref label="Seep01" href="RM000002WKL01CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0004">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000003WQ306GX_03_0018">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>