<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12037_S001R" variety="S001R">
<name>VARIABLE GEAR RATIO STEERING</name>
<ttl id="12037_S001R_7C3Q8_T00JB" variety="T00JB">
<name>VARIABLE GEAR RATIO STEERING SYSTEM</name>
<para id="RM000000YUC024X" category="C" type-id="800V7" name-id="VG00A-07" from="201301" to="201308">
<dtccode>C15C3/75</dtccode>
<dtcname>Brake System Control Module Malfunction</dtcname>
<subpara id="RM000000YUC024X_01" type-id="60" category="03" proc-id="RM22W0E___0000B9U00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The steering control ECU detects a vehicle speed error by receiving an error signal from the skid control ECU via CAN communication.</ptxt>
<ptxt>If the steering control ECU detects this error signal, it will turn the master warning light on, store this DTC, and stop VGRS operation.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.22in"/>
<colspec colname="COL2" colwidth="3.59in"/>
<colspec colname="COL3" colwidth="2.27in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C15C3/75</ptxt>
</entry>
<entry valign="middle">
<ptxt>The steering control ECU receives a vehicle speed error signal via CAN communication.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Vehicle stability control system</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>Steering control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000YUC024X_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000YUC024X_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YUC024X_03_0001" proc-id="RM22W0E___0000B9V00000">
<testtitle>CHECK DTC (VEHICLE STABILITY CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs in the vehicle stability control system (See page <xref label="Seep01" href="RM0000046KV00MX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.63in"/>
<colspec colname="COL2" colwidth="1.50in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YUC024X_03_0002" fin="false">OK</down>
<right ref="RM000000YUC024X_03_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YUC024X_03_0002" proc-id="RM22W0E___0000B9W00000">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, check if the CAN communication system is functioning normally (See page <xref label="Seep01" href="RM000001RSW03JX"/> for LHD, <xref label="Seep02" href="RM000001RSW03KX"/> for RHD).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.60in"/>
<colspec colname="COL2" colwidth="1.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YUC024X_03_0003" fin="true">A</down>
<right ref="RM000000YUC024X_03_0007" fin="true">B</right>
<right ref="RM000000YUC024X_03_0005" fin="true">C</right>
<right ref="RM000000YUC024X_03_0006" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000000YUC024X_03_0003">
<testtitle>REPLACE STEERING CONTROL ECU<xref label="Seep01" href="RM000003DZD00EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUC024X_03_0004">
<testtitle>GO TO VEHICLE STABILITY CONTROL SYSTEM (DIAGNOSTIC TROUBLE CODE CHART)<xref label="Seep01" href="RM0000045Z600TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUC024X_03_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (DIAGNOSTIC TROUBLE CODE CHART)<xref label="Seep01" href="RM000003A8W004X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUC024X_03_0006">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (DIAGNOSTIC TROUBLE CODE CHART)<xref label="Seep01" href="RM000002L7X028X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUC024X_03_0007">
<testtitle>REPLACE STEERING CONTROL ECU<xref label="Seep01" href="RM000003EFD004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>