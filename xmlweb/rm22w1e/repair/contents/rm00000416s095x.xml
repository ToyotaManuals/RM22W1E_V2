<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000416S095X" category="J" type-id="804OC" name-id="ESO7U-05" from="201308">
<dtccode/>
<dtcname>Turbocharger Noise</dtcname>
<subpara id="RM00000416S095X_01" type-id="60" category="03" proc-id="RM22W0E___000031A00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Turbocharger noise is classified into 2 types. One is whistling noise, and the other is whining noise. When troubleshooting, the type of noise should be determined first.</ptxt>
</atten4>
<table pgwide="1">
<title>Description</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Noise Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Noise Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Whistling noise</ptxt>
</entry>
<entry valign="middle">
<ptxt>The volume and pitch of this noise are proportional to turbocharger speed or engine speed. Therefore, the noise level will become more obvious when engine speed increases.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Turbocharger (e.g. shaft imbalance)</ptxt>
</item>
<item>
<ptxt>Gears inside engine</ptxt>
</item>
<item>
<ptxt>Transmission gear</ptxt>
</item>
<item>
<ptxt>Intake air system leakage</ptxt>
</item>
<item>
<ptxt>Intake air system breakage</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Whining noise</ptxt>
</entry>
<entry valign="middle">
<ptxt>This noise has a lower pitch than whistling noise. It generally occurs in the engine speed range of 1500 to 2500 rpm, and has a relatively constant pitch, independent of engine speed and vehicle speed.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbocharger</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Example: Vibration of turbocharger is transferred to exhaust pipe which causes resonation and noise.</ptxt>
</item>
<item>
<ptxt>Example: Vibration of turbocharger is transferred through exhaust pipe to vehicle body (floor) which then vibrates causing noise.</ptxt>
</item>
</list1>
</atten4>
</entry>
</row>
</tbody>
</tgroup>
</table>
<step1>
<ptxt>Faults and Symptoms of Diesel Engine Components</ptxt>
<table pgwide="1">
<title>Turbocharger System</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.82in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="4.68in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Main fault</ptxt>
</entry>
<entry>
<ptxt>Turbocharger turbine shaft imbalance</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Symptoms</ptxt>
</entry>
<entry>
<ptxt>High-pitched whistling noise</ptxt>
<atten4>
<ptxt>The pitch and volume change in proportion to the turbocharger speed.</ptxt>
</atten4>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Intake System</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.82in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="4.68in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Main fault</ptxt>
</entry>
<entry>
<ptxt>Leakage</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Symptoms</ptxt>
</entry>
<entry>
<ptxt>Leak flow noise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Gear inside Engine</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.82in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="4.68in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Symptoms</ptxt>
</entry>
<entry>
<ptxt>Gear noise</ptxt>
<atten4>
<ptxt>The pitch and volume change in proportion to the engine speed.</ptxt>
</atten4>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Transmission Gear</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.82in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="4.68in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Symptoms</ptxt>
</entry>
<entry>
<ptxt>Gear noise</ptxt>
<atten4>
<ptxt>The pitch and volume change in proportion to the transmission gear speed.</ptxt>
</atten4>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
</content5>
</subpara>
<subpara id="RM00000416S095X_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM00000416S095X_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000416S095X_03_0001" proc-id="RM22W0E___000031B00001">
<testtitle>CONFIRM CONDITION IN WHICH NOISE OCCURRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Confirm with the customer the conditions in which the noise occurred.</ptxt>
<atten4>
<ptxt>To clearly understand the conditions in which the noise occurred, the items in the table below are useful.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine speed range</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle speed range</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Transmission gear</ptxt>
</entry>
<entry valign="middle">
<ptxt>Which gear?</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Accelerator pedal position</ptxt>
</entry>
<entry valign="middle">
<ptxt>During acceleration or deceleration?</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ambient temperature</ptxt>
</item>
<item>
<ptxt>Engine temperature (cold, warmed up, etc.)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Road conditions</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>City or highway driving</ptxt>
</item>
<item>
<ptxt>Uphill or downhill</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Noise level</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Did the noise occur suddenly, or build gradually?</ptxt>
</item>
<item>
<ptxt>Has the noise gradually been getting louder?</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Other symptoms</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Lack of power</ptxt>
</item>
<item>
<ptxt>High fuel consumption, etc.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Noise recognition</ptxt>
</entry>
<entry valign="middle">
<ptxt>What made the customer determine the noise to be abnormal?</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0002" proc-id="RM22W0E___000031C00001">
<testtitle>CONFIRM NOISE CUSTOMER MENTIONED (PROCEDURE 2)</testtitle>

<content6 releasenbr="1">
<test1>
<ptxt>Check for the noise described by the customer.</ptxt>
<atten4>
<ptxt>If a noise is heard, make sure that the noise matches the noise described by the customer.</ptxt>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Obvious abnormal noise matching noise described by customer is confirmed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Obvious abnormal noise matching noise described by customer is not confirmed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0003" fin="false">A</down>
<right ref="RM00000416S095X_03_0010" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0003" proc-id="RM22W0E___000031D00001">
<testtitle>CHECK INTAKE SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for air leakage between the air cleaner and turbocharger, and between the turbocharger and intake manifold.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Check for disconnected or improperly connected hoses, and gaps between hoses and parts.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter between the air cleaner and turbocharger.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0015" fin="false">OK</down>
<right ref="RM00000416S095X_03_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0015" proc-id="RM22W0E___000031H00001">
<testtitle>CONFIRM NOISE SOURCE (FROM TURBOCHARGER OR NOT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
<atten4>
<ptxt>The software version installed on the intelligent tester must be V2009.2 or later.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Active Test / Activate the VN Turbo Open.</ptxt>
<atten4>
<ptxt>This Active Test function operates both the turbocharger actuators (bank 1 and bank 2) at the same time.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Perform the Active Test and rev the engine up several times.</ptxt>
</test1>
<test1>
<ptxt>Check whether the noise is reduced or not compared with the noise under the condition that the Active Test is not performed.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>The noise is reduced (or disappears)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>The noise is the same as in Procedure 2 (The noise does not change)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the variable nozzle of the turbocharger opens fully regardless of the engine condition, the turbocharger speed slows down and the cause of noise can be confirmed that is from the turbocharger or not.</ptxt>
</item>
<item>
<ptxt>If the turbocharger is the cause of the noise, the noise will be reduced when the variable nozzle opens because turbocharger speed does not increase.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0005" fin="false">A</down>
<right ref="RM00000416S095X_03_0016" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0005" proc-id="RM22W0E___000031E00001">
<testtitle>CONFIRM THE NOISE TYPE (PROCEDURE 5)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether the noise can be heard even when the engine speed is more than 3000 rpm, and whether the noise gradually gets louder as the engine speed increases.</ptxt>
<atten4>
<ptxt>If so, the noise is likely to be whistling noise. If not, the noise is likely to be whining noise.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>If whistling noise is confirmed on the above inspection, identify the malfunctioning turbocharger (No. 1 or No. 2) using a sound scope.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.64in"/>
<colspec colname="COL2" colwidth="2.44in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Yes (The noise is likely to be whistling noise)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No (The noise is likely to be whining noise)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0019" fin="false">A</down>
<right ref="RM00000416S095X_03_0006" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0019" proc-id="RM22W0E___000031L00001">
<testtitle>REPLACE TURBOCHARGER SUB-ASSEMBLY (NO. 1 OR NO. 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace either No. 1 or No. 2 turbocharger sub-assembly which is on the malfunctioning bank confirmed in procedure 5 (See page <xref label="Seep01" href="RM0000032A8023X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0020" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0020" proc-id="RM22W0E___000031M00001">
<testtitle>CONFIRM WHETHER THE NOISE PROBLEM HAS BEEN SUCCESSFULLY SOLVED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether the noise problem has been successfully solved.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0014" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0006" proc-id="RM22W0E___000031F00001">
<testtitle>PERFORM ALIGNMENT OF EXHAUST PIPE AND CONFIRM THE NOISE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the exhaust pipe at the ball joint and from each exhaust pipe support, and then realign the exhaust pipe and reconnect it to each exhaust pipe support and at the ball joint (See page <xref label="Seep01" href="RM0000056LO001X"/>).</ptxt>
</test1>
<figure>
<graphic graphicname="A229647E02" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="center" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="center" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gasket</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Monolithic Converter Assembly RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compression Spring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front No. 1 Exhaust Pipe Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Monolithic Converter Assembly LH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front No. 2 Exhaust Pipe Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Exhaust Pipe Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Pipe Support</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clamp</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tail Pipe Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified torque</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Non-reusable part</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Above illustration is an example.</ptxt>
</atten4>
<test1>
<ptxt>Confirm whether the noise problem is solved or not.</ptxt>
<atten4>
<ptxt>The alignment of the exhaust pipe affects whining noise.</ptxt>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>The noise problem is solved</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>The noise problem remains</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B<sup>*</sup>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>
<sup>*</sup>: If the noise problem still remains, replace both the No. 1 and No. 2 turbocharger sub-assembly.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0014" fin="true">A</down>
<right ref="RM00000416S095X_03_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0016" proc-id="RM22W0E___000031I00001">
<testtitle>CHECK FOR NOISE FROM SOURCE OTHER THAN TURBOCHARGER (PROCEDURE 9)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for the noise source with a sound scope.</ptxt>
<atten3>
<ptxt>When performing this check, make sure that the sound scope, your body, etc., do not get caught on the V-belt.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The source of the noise is not the turbocharger, but a part other than the turbocharger (e.g. a part inside the engine, transmission, or vacuum pump).</ptxt>
</item>
<item>
<ptxt>Sometimes gear noise may be mistaken for turbocharger noise. For gear noise, pay attention to the gear inside the engine and transmission gear.</ptxt>
</item>
<item>
<ptxt>Rev the engine up and check if the noise reduces at the same time the engine speed decreases. If the noise reduces at the same time the engine speed decreases, it is gear noise. If the noise reduces a short time after the engine speed decreases, it is turbocharger noise.</ptxt>
</item>
<item>
<ptxt>The following illustration is a reference for checking whether the noise source is from the engine.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="A229641E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="center" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="center" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Supply Pump Drive Gear</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Camshafts</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Crankshaft</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Scavenging Pump</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbocharger</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gear Location inside Engine</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Above illustration is an example.</ptxt>
</item>
<item>
<ptxt>Gear noise can be confirmed with a sound scope. Put the sound scope on the cylinder block near the gear, and if the noise heard through the sound scope has the same timing as the noise heard without the sound scope, it is gear noise.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0017" proc-id="RM22W0E___000031J00001">
<testtitle>REPLACE THE PARTS DETECTED IN PROCEDURE 9</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the parts detected in Procedure 9 as the noise source.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0018" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0018" proc-id="RM22W0E___000031K00001">
<testtitle>CONFIRM WHETHER THE NOISE PROBLEM HAS BEEN SUCCESSFULLY SOLVED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether the noise problem has been successfully solved.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0014" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0010" proc-id="RM22W0E___000031G00001">
<testtitle>EXPLAIN TO THE CUSTOMER THAT THE NOISE IS NORMAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Explain to the customer that the noise the customer mentioned is not abnormal.</ptxt>
<atten4>
<ptxt>To help the customer understand, test driving another car with the customer may be helpful.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000416S095X_03_0014" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000416S095X_03_0011">
<testtitle>REPAIR OR REPLACE MALFUNCTIONING PARTS</testtitle>
</testgrp>
<testgrp id="RM00000416S095X_03_0012">
<testtitle>REPLACE TURBOCHARGER SUB-ASSEMBLY<xref label="Seep01" href="RM0000032A8023X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000416S095X_03_0014">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>