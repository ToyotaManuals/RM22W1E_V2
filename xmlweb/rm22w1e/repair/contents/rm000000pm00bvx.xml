<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LY_T00F1" variety="T00F1">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM000000PM00BVX" category="D" type-id="303F3" name-id="CC41M-01" from="201301">
<name>DIAGNOSIS SYSTEM</name>
<subpara id="RM000000PM00BVX_z0" proc-id="RM22W0E___00007AV00000">
<content5 releasenbr="1">
<step1>
<ptxt>DIAGNOSIS FUNCTION</ptxt>
<step2>
<ptxt>The diagnosis function makes the master warning light and the multi-information display come on, and the cruise control indicator light goes off as shown in the illustration. When a malfunction occurs in the dynamic radar cruise control system, DTCs are stored in the ECM.</ptxt>
<figure>
<graphic graphicname="E247331E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master Warning Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Constant Speed Control Mode)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Vehicle-to-vehicle Distance Control Mode)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The master warning light goes off if the system returns to normal.</ptxt>
</item>
<item>
<ptxt>Since the stored data in the ECM is cleared by disconnecting the EFI and ETCS fuses or the cable from the negative (-) battery terminal, do not disconnect them until the inspection has been completed.</ptxt>
</item>
</list1>
</atten3>
</step2>
</step1>
<step1>
<ptxt>CHECK INDICATOR</ptxt>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
<figure>
<graphic graphicname="E247334E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</step2>
<step2>
<ptxt>Check that the cruise control indicator light and RADAR READY indicator come on when the cruise control main switch ON-OFF button is pushed on, and that the indicator light goes off when the ON-OFF button is pushed off.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Vehicle-to-vehicle Distance Control Mode)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the indicator check result shows a problem, proceed to troubleshooting for the combination meter section.</ptxt>
</item>
<item>
<ptxt>If a malfunction occurs in the vehicle speed sensors, the stop light switch, or other related parts during cruise control driving, the ECU activates AUTO CANCEL of cruise control and turns off the cruise control indicator light. At the same time, the malfunction is stored as a diagnostic trouble code.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>