<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LY_T00F1" variety="T00F1">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM000000ST00AXX" category="D" type-id="303FA" name-id="CC0009-144" from="201301">
<name>ROAD TEST</name>
<subpara id="RM000000ST00AXX_z0" proc-id="RM22W0E___00007AX00000">
<content5 releasenbr="1">
<atten4>
<ptxt>The dynamic radar cruise control system has 2 cruise control modes: the constant speed control mode and vehicle-to-vehicle distance control mode.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The vehicle-to-vehicle distance control mode is always selected when starting up the dynamic radar cruise control system.</ptxt>
</item>
<item>
<ptxt>Operation of the constant speed control mode is the same as the cruise control system.</ptxt>
</item>
</list1>
</atten4>
<step1>
<ptxt>INSPECT SET SWITCH</ptxt>
<figure>
<graphic graphicname="E107857" width="2.775699831in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Turn the cruise control main switch on.</ptxt>
</step2>
<step2>
<ptxt>Drive at the desired speed (50 km/h [30 mph] or higher).</ptxt>
</step2>
<step2>
<ptxt>Push the cruise control main switch to -SET (COAST/SET).</ptxt>
</step2>
<step2>
<ptxt>Check that the vehicle cruises at the desired speed.</ptxt>
</step2>
</step1>
<step1>
<ptxt>INSPECT "+" SWITCH</ptxt>
<figure>
<graphic graphicname="E107858" width="2.775699831in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Turn the cruise control main switch on.</ptxt>
</step2>
<step2>
<ptxt>Drive at the desired speed (50 km/h [30 mph] or higher).</ptxt>
</step2>
<step2>
<ptxt>Push the cruise control main switch to -SET (COAST/SET).</ptxt>
</step2>
<step2>
<ptxt>Check that vehicle speed increases while the cruise control main switch is pushed to +RES (ACCELERATION/RESUME), and that the vehicle cruises at the set speed when released.</ptxt>
</step2>
<step2>
<ptxt>Vehicle-to-vehicle distance control mode:</ptxt>
<ptxt>Momentarily push the cruise control main switch to +RES (ACCELERATION/RESUME), and then immediately release it. Check that the driving and the stored vehicle speed increase by 5 km/h or 5 mph (Tap-up function).</ptxt>
<atten4>
<ptxt>Pushing the cruise control main switch to +RES while following a vehicle in front with the vehicle-to-vehicle distance control mode on does not increase the actual vehicle speed, but changes only the stored vehicle speed.</ptxt>
</atten4>
</step2>
<step2>
<ptxt>Constant speed control mode:</ptxt>
<ptxt>Momentarily push the control main switch to +RES (ACCELERATION/RESUME), and then immediately release it. Check that the vehicle speed increases by approximately 1.6 km/h (1.0 mph) (Tap-up function).</ptxt>
</step2>
</step1>
<step1>
<ptxt>INSPECT "-" SWITCH</ptxt>
<figure>
<graphic graphicname="E107857" width="2.775699831in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Turn the cruise control main switch on.</ptxt>
</step2>
<step2>
<ptxt>Drive at the desired speed (50 km/h [30 mph] or higher).</ptxt>
</step2>
<step2>
<ptxt>Push the cruise control main switch to -SET (COAST/SET).</ptxt>
</step2>
<step2>
<ptxt>Check that vehicle speed decreases while the cruise control main switch is pushed to -SET (COAST/SET), and the vehicle cruises at the set speed when released.</ptxt>
</step2>
<step2>
<ptxt>Vehicle-to-vehicle distance control mode:</ptxt>
<ptxt>Momentarily push the cruise control main switch to -SET (COAST/SET), and then immediately release it. Check that the driving and the stored vehicle speed decrease by 5 km/h or 5 mph (Tap-down function).</ptxt>
</step2>
<step2>
<ptxt>Constant speed control mode:</ptxt>
<ptxt>Momentarily push the cruise control main switch to -SET (COAST/SET), and then immediately release it. Check that the vehicle speed decreases by approximately 1.6 km/h (1.0 mph) (Tap-down function).</ptxt>
</step2>
<step2>
<ptxt>Check that the set speed is displayed on the multi-information display in the combination meter assembly and that the "SET" indicator light illuminates.</ptxt>
</step2>
</step1>
<step1>
<ptxt>INSPECT CANCEL SWITCH</ptxt>
<figure>
<graphic graphicname="E107859" width="2.775699831in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Turn the cruise control main switch on.</ptxt>
</step2>
<step2>
<ptxt>Drive at the desired speed (50 km/h [30 mph] or higher).</ptxt>
</step2>
<step2>
<ptxt>Push the cruise control main switch to -SET (COAST/SET).</ptxt>
</step2>
<step2>
<ptxt>When performing one of the following, check that the cruise control system is canceled.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Depressing the brake pedal.</ptxt>
</item>
<item>
<ptxt>Moving the shift lever from D to N.</ptxt>
</item>
<item>
<ptxt>With the shift lever in S, selecting the S1, S2 or S3 range.</ptxt>
</item>
<item>
<ptxt>Turning the cruise control main switch off (the stored vehicle speed in the ECM is not maintained).</ptxt>
</item>
<item>
<ptxt>Pulling the cruise control main switch to CANCEL.</ptxt>
</item>
<item>
<ptxt>Driving the vehicle while the VSC system is operating.</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>INSPECT RES (RESUME) SWITCH</ptxt>
<figure>
<graphic graphicname="E107858" width="2.775699831in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Turn the cruise control main switch on.</ptxt>
</step2>
<step2>
<ptxt>Drive at the desired speed (50 km/h [30 mph] or higher).</ptxt>
</step2>
<step2>
<ptxt>Push the cruise control main switch to -SET (COAST/SET).</ptxt>
</step2>
<step2>
<ptxt>Cancel the cruise control system by performing any of the operations below.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Depressing the brake pedal.</ptxt>
</item>
<item>
<ptxt>Moving the shift lever from D to N.</ptxt>
</item>
<item>
<ptxt>With the shift lever in S, selecting the S1, S2 or S3 range.</ptxt>
</item>
<item>
<ptxt>Pulling the cruise control switch to CANCEL.</ptxt>
</item>
<item>
<ptxt>Driving the vehicle while the VSC system is operating.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>After the cruise control main switch is pushed to +RES (ACCELERATION/RESUME) at a driving speed of more than 40 km/h (25 mph), check that the vehicle restores the speed before the cancellation and the cruise control "SET" indicator light illuminates.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>