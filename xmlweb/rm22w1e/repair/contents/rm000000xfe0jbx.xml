<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TW_T00MZ" variety="T00MZ">
<name>AIRBAG SYSTEM</name>
<para id="RM000000XFE0JBX" category="D" type-id="303F4" name-id="RS0064-190" from="201301">
<name>DTC CHECK / CLEAR</name>
<subpara id="RM000000XFE0JBX_z0" proc-id="RM22W0E___0000F7000000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK DTC (Using SST Check Wire)</ptxt>
<step2>
<ptxt>Check the DTCs (Present trouble code).</ptxt>
<figure>
<graphic graphicname="H102392E73" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Turn the ignition switch to ON, and wait for approximately 60 seconds.</ptxt>
</step3>
<step3>
<ptxt>Using SST, connect terminals 13 (TC) and 4 (CG) of the DLC3.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>Connect the correct terminals to avoid a malfunction.</ptxt>
</atten3>
</step3>
</step2>
<step2>
<ptxt>Check the DTCs (Past trouble code).</ptxt>
<step3>
<ptxt>Using SST, connect terminals 13 (TC) and 4 (CG) of the DLC3.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>Connect the correct terminals to avoid a malfunction.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>Turn the ignition switch to ON, and wait for approximately 60 seconds.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Read the DTCs.</ptxt>
<figure>
<graphic graphicname="C148438E10" width="2.775699831in" height="5.787629434in"/>
</figure>
<step3>
<ptxt>Read the blinking patterns of the DTCs. As examples, the blinking patterns for the normal system code and trouble codes 11 and 31 are shown in the illustration.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Normal system code indication (without past trouble code):</ptxt>
<ptxt>The light blinks twice per second.</ptxt>
</item>
<item>
<ptxt>Normal system code indication (with past trouble code):</ptxt>
<ptxt>When a past trouble code is stored in the center airbag sensor, the light blinks only once per second.</ptxt>
</item>
<item>
<ptxt>Trouble code indication:</ptxt>
<ptxt>Each digit of a DTC is separated by a 1.5 second pause.</ptxt>
</item>
</list1>
<ptxt>If there is more than 1 code, there will be a 2.5 second pause between each code. After all codes are shown, there will be a 4.0 second pause, and then all codes will be repeated.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If 2 or more malfunctions are found, the indication begins with the smaller-numbered code.</ptxt>
</item>
<item>
<ptxt>If DTCs are output without connecting the terminals, proceed to "Diagnosis Circuit" (See page <xref label="Seep01" href="RM000002OOK03FX"/>).</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>CLEAR DTC (Using SST Check Wire)</ptxt>
<step2>
<ptxt>When the ignition switch is turned off, the DTCs are cleared.</ptxt>
<atten4>
<ptxt>Depending on the DTC, the code may not be cleared by turning off the ignition switch. In this case, proceed to the next procedure.</ptxt>
</atten4>
</step2>
<step2>
<ptxt>Using SST, connect terminals 13 (TC) and 4 (CG) of the DLC3, and then turn the ignition switch to ON.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
</step2>
<step2>
<ptxt>Disconnect terminal TC of the DLC3 within 3 to 10 seconds after the DTCs are output, and check if the SRS warning light comes on after 3 seconds.</ptxt>
</step2>
<step2>
<ptxt>Within 2 to 4 seconds after the SRS warning light comes on, connect terminals 13 (TC) and 4 (CG) of the DLC3.</ptxt>
</step2>
<step2>
<ptxt>The SRS warning light should go off within 2 to 4 seconds after connecting terminals 13 (TC) and 4 (CG) of the DLC3. Then, disconnect terminal 13 (TC) within 2 to 4 seconds after the SRS warning light goes off.</ptxt>
</step2>
<step2>
<ptxt>The SRS warning light comes on again within 2 to 4 seconds after disconnecting terminal 13 (TC). Then, reconnect terminals 13 (TC) and 4 (CG) within 2 to 4 seconds after the SRS warning light comes on.</ptxt>
</step2>
<step2>
<ptxt>Check if the SRS warning light goes off within 2 to 4 seconds after connecting terminals 13 (TC) and 4 (CG) of the DLC3. Also check if the normal system code is output within 1 second after the SRS warning light goes off.</ptxt>
<ptxt>If DTCs are not cleared, repeat this procedure until the codes are cleared.</ptxt>
<figure>
<graphic graphicname="C123780E46" width="7.106578999in" height="4.7836529in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>CHECK DTC (Using the Intelligent Tester)</ptxt>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Check the DTCs by following the prompts on the tester screen.</ptxt>
<atten4>
<ptxt>Refer to the intelligent tester operator's manual for further details.</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>CLEAR DTC (Using the Intelligent Tester)</ptxt>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Clear the DTCs by following the prompts on the tester screen.</ptxt>
<atten4>
<ptxt>Refer to the intelligent tester operator's manual for further details.</ptxt>
</atten4>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>