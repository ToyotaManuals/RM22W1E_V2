<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001X" variety="S001X">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001X_7C3RY_T00L1" variety="T00L1">
<name>PARKING ASSIST MONITOR SYSTEM (w/ Side Monitor System)</name>
<para id="RM000003WW202WX" category="C" type-id="8038Z" name-id="PM73N-01" from="201308">
<dtccode>C1612</dtccode>
<dtcname>IG Voltage is Low or High</dtcname>
<subpara id="RM000003WW202WX_01" type-id="60" category="03" proc-id="RM22W0E___0000D0800001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored if the parking assist ECU judges as a result of its self check that the voltage received by terminal IG is low or high.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1612</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>IG voltage is low</ptxt>
</item>
<item>
<ptxt>IG voltage is high</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Parking assist ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003WW202WX_02" type-id="32" category="03" proc-id="RM22W0E___0000D0900001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E182726E65" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003WW202WX_03" type-id="51" category="05" proc-id="RM22W0E___0000D0A00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When "System initializing" is displayed on the parking assist ECU after the battery terminal disconnected, correct the steering angle neutral point (See page <xref label="Seep01" href="RM0000035DE03CX"/>).</ptxt>
</item>
<item>
<ptxt>Depending on the parts that are replaced or operations that are performed during vehicle inspection or maintenance, calibration of other systems as well as the parking assist monitor system (w/ Side Monitor System) may be needed (See page <xref label="Seep02" href="RM0000035DD03SX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000003WW202WX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003WW202WX_04_0001" proc-id="RM22W0E___0000D0B00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000035DB03WX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000035DB03WX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC C1612 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>If the parking assist ECU cannot read the stored information when it is activated, allowing the ECU to store the information again may return the system to normal.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003WW202WX_04_0004" fin="true">OK</down>
<right ref="RM000003WW202WX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WW202WX_04_0002" proc-id="RM22W0E___0000D0C00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARKING ASSIST ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the parking assist ECU connector.</ptxt>
<figure>
<graphic graphicname="E245332E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F75-4 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F75-8 (IG) - F75-4 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="right" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Parking Assist ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000003WW202WX_04_0003" fin="true">OK</down>
<right ref="RM000003WW202WX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WW202WX_04_0003">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM0000039LH004X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WW202WX_04_0004">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WW202WX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>