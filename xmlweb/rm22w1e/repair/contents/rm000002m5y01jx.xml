<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002K" variety="S002K">
<name>WIPER / WASHER</name>
<ttl id="12068_S002K_7C3Y9_T00RC" variety="T00RC">
<name>WIPER AND WASHER SYSTEM (w/ Rain Sensor)</name>
<para id="RM000002M5Y01JX" category="U" type-id="303FJ" name-id="WW0Y4-25" from="201301">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM000002M5Y01JX_z0" proc-id="RM22W0E___0000IS600000">
<content5 releasenbr="1">
<step1>
<ptxt>READ DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / (desired system) / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, read the "Data List".</ptxt>
<table pgwide="1">
<title>Combination Switch</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F Wiper LO Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wiper LO switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front wiper switch in LO position</ptxt>
<ptxt>OFF: Front wiper switch not in LO position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F Wiper HI Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wiper HI switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front wiper switch in HI position</ptxt>
<ptxt>OFF: Front wiper switch not in HI position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F Wiper AUTO Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wiper AUTO switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front wiper switch in AUTO position</ptxt>
<ptxt>OFF: Front wiper switch not in AUTO position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F Wiper MIST Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wiper MIST switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front wiper switch in MIST position</ptxt>
<ptxt>OFF: Front wiper switch not in MIST position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F Washer Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front washer switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front washer switch on</ptxt>
<ptxt>OFF: Front washer switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>IG Switch SIG</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Engine switch on (IG)</ptxt>
<ptxt>OFF: Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>"OFF" is also displayed for this item when the engine switch is on (ACC).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Rain Sensor</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Rain Sensor Cold</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rain sensor cold / COLD or OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>COLD: Rain sensor is low temp</ptxt>
<ptxt>OK: Rain sensor is normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rain Sensor Hot</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rain sensor cold / HOT or OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>HOT: Rain sensor is high temp</ptxt>
<ptxt>OK: Rain sensor is normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Body No. 4</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Washer Level Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Washer fluid level warning switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Washer jar and pump are empty</ptxt>
<ptxt>OFF: Washer jar and pump are full</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>IG Switch SIG</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Engine switch on (IG)</ptxt>
<ptxt>OFF: Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>"OFF" is also displayed for this item when the engine switch is on (ACC).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Stop Light Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Stop light switch on</ptxt>
<ptxt>OFF: Stop light switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>PERFORM ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<atten3>
<ptxt>When a Body No. 5 (windshield wiper ECU) related front wiper switch is on, Active Test items related to the front wiper switch are prohibited (double operation prohibition). Check that the front wiper switches are off before performing Active Tests.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / (desired system) / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester. perform the "Active Test".</ptxt>
<table pgwide="1">
<title>Body No. 5</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Wiper Mot (HI)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiper motor HI operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON / OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Wiper Mot (LO)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiper motor LO operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON / OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Body No. 4</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Front Washer Nozzle Heater</ptxt>
</entry>
<entry valign="middle">
<ptxt>Washer nozzle heater operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON / OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Head Light Cleaner</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight low beam signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON / OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>