<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R4_T00K7" variety="T00K7">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM0000011FD0S3X" category="J" type-id="304TO" name-id="AV3JO-70" from="201301" to="201308">
<dtccode/>
<dtcname>Cellular Phone Registration Failure, Phone Directory Transfer Failure</dtcname>
<subpara id="RM0000011FD0S3X_01" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000011FD0S3X_02" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000011FD0S3X_02_0001" proc-id="RM22W0E___0000C7H00000">
<testtitle>CHECK CURRENT CONDITIONS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Proceed to the next step according to the table below.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Another "Bluetooth" compatible cellular phone is present.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Another "Bluetooth" compatible vehicle is present.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>None of the above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011FD0S3X_02_0002" fin="false">A</down>
<right ref="RM0000011FD0S3X_02_0004" fin="false">B</right>
<right ref="RM0000011FD0S3X_02_0006" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM0000011FD0S3X_02_0002" proc-id="RM22W0E___0000C7I00000">
<testtitle>CHECK USING ANOTHER CELLULAR PHONE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the system functions using another "Bluetooth" compatible cellular phone.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Confirm that either the same or a different version of another "Bluetooth" compatible cellular phone complies with the system.</ptxt>
</item>
<item>
<ptxt>Depending on the version, some "Bluetooth" compatible cellular phones cannot be used.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The system functions.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011FD0S3X_02_0003" fin="true">OK</down>
<right ref="RM0000011FD0S3X_02_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011FD0S3X_02_0004" proc-id="RM22W0E___0000C7J00000">
<testtitle>CHECK USING ANOTHER "BLUETOOTH" COMPATIBLE VEHICLE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Register the cellular phone with another vehicle and check if the system functions normally.</ptxt>
<atten4>
<ptxt>Depending on the version, some "Bluetooth" compatible cellular phones cannot be used.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The system functions.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011FD0S3X_02_0008" fin="true">OK</down>
<right ref="RM0000011FD0S3X_02_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011FD0S3X_02_0006" proc-id="RM22W0E___0000C7K00000">
<testtitle>CHECK CELLULAR PHONE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the cellular phone is "Bluetooth" compatible.</ptxt>
<atten4>
<ptxt>Some versions of "Bluetooth" compatible cellular phones may not function.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The phone is "Bluetooth" compatible.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011FD0S3X_02_0007" fin="false">OK</down>
<right ref="RM0000011FD0S3X_02_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011FD0S3X_02_0007" proc-id="RM22W0E___0000C7L00000">
<testtitle>CHECK CELLULAR PHONE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the cellular phone can make a call.</ptxt>
<atten4>
<ptxt>When the battery is low, registration or directory transfer cannot be done.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Cellular phone can make a call.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011FD0S3X_02_0008" fin="true">OK</down>
<right ref="RM0000011FD0S3X_02_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011FD0S3X_02_0008">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011FD0S3X_02_0003">
<testtitle>USE A "BLUETOOTH" COMPATIBLE CELLULAR PHONE</testtitle>
</testgrp>
<testgrp id="RM0000011FD0S3X_02_0010">
<testtitle>REPAIR OR REPLACE CELLULAR PHONE</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>