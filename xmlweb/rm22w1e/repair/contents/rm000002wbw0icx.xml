<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T9_T00MC" variety="T00MC">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000002WBW0ICX" category="J" type-id="800Q1" name-id="TD633-03" from="201308">
<dtccode/>
<dtcname>Rear Door RH Entry Unlock Function does not Operate</dtcname>
<subpara id="RM000002WBW0ICX_01" type-id="60" category="03" proc-id="RM22W0E___0000ENQ00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the rear door RH entry unlock function does not operate but the entry lock function operates, the communication line between the vehicle and electrical key transmitter is normal. The part at fault may be a touch sensor circuit (certification ECU (smart key ECU assembly) → rear door outside handle RH → rear door electrical key oscillator RH).</ptxt>
<atten4>
<ptxt>For vehicles with entry function for rear doors.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002WBW0ICX_02" type-id="32" category="03" proc-id="RM22W0E___0000ENR00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B186274E08" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002WBW0ICX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002WBW0ICX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002WBW0ICX_04_0001" proc-id="RM22W0E___0000ENS00001">
<testtitle>CHECK POWER DOOR LOCK OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When the master switch assembly door control switch is operated, check that the locked doors unlock.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Door locks operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002WBW0ICX_04_0017" fin="false">OK</down>
<right ref="RM000002WBW0ICX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0017" proc-id="RM22W0E___0000EO000001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR LOCK POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Main Body / Data List.</ptxt>
</test1>
<test1>
<ptxt>Read the Data List according to the display on the intelligent tester.</ptxt>
<test2>
<ptxt>for LHD</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PR Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Passenger side rear door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Passenger side rear door unlocked</ptxt>
<ptxt>OFF: Passenger side rear door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between ON and OFF as shown in the chart above.</ptxt>
</specitem>
</spec>
</test2>
<test2>
<ptxt>for RHD</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DR Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side rear door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side rear door unlocked</ptxt>
<ptxt>OFF: Driver side rear door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between ON and OFF as shown in the chart above.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002WBW0ICX_04_0002" fin="false">OK</down>
<right ref="RM000002WBW0ICX_04_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0002" proc-id="RM22W0E___0000ENT00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (TOUCH SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List.</ptxt>
<test2>
<ptxt>for LHD</ptxt>
<table pgwide="1">
<title>Entry &amp; Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Pr-Door Touch Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Passenger side rear door touch sensor / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Touch sensor touched</ptxt>
<ptxt>OFF: Touch sensor not touched</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>"ON" (sensor is touched) and "OFF" (sensor is not touched) appears on the screen.</ptxt>
</specitem>
</spec>
</test2>
<test2>
<ptxt>for RHD</ptxt>
<table pgwide="1">
<title>Entry &amp; Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Dr-Door Touch Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side rear door touch sensor / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Touch sensor touched</ptxt>
<ptxt>OFF: Touch sensor not touched</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>"ON" (sensor is touched) and "OFF" (sensor is not touched) appears on the screen.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002WBW0ICX_04_0014" fin="true">OK</down>
<right ref="RM000002WBW0ICX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0003" proc-id="RM22W0E___0000ENU00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU [SMART KEY ECU ASSEMBLY] - DOOR ELECTRICAL KEY OSCILLATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E30 ECU connector.</ptxt>
<figure>
<graphic graphicname="B187936E06" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the z26 oscillator connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E30-2 (SEL4) - z26-4 (SEL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-27 (SEN4) - z26-6 (SENS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-2 (SEL4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-27 (SEN4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E30-1 (SEL3) - z26-4 (SEL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-28 (SEN3) - z26-6 (SENS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-1 (SEL3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-28 (SEN3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002WBW0ICX_04_0004" fin="false">OK</down>
<right ref="RM000002WBW0ICX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0004" proc-id="RM22W0E___0000ENV00001">
<testtitle>CHECK HARNESS AND CONNECTOR (DOOR ELECTRICAL KEY OSCILLATOR - REAR DOOR OUTSIDE HANDLE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the z25 handle connector.</ptxt>
<figure>
<graphic graphicname="B164339E44" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the z26 oscillator connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z25-6 (SGT) - z26-2 (SGT)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z25-6 (SGT) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002WBW0ICX_04_0015" fin="false">OK</down>
<right ref="RM000002WBW0ICX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0015" proc-id="RM22W0E___0000ENY00001">
<testtitle>REPLACE DOOR ELECTRICAL KEY OSCILLATOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the door electrical key oscillator (See page <xref label="Seep01" href="RM000002M8106XX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002WBW0ICX_04_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0005" proc-id="RM22W0E___0000ENW00001">
<testtitle>CHECK DOOR ELECTRICAL KEY OSCILLATOR (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the entry function operates normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002WBW0ICX_04_0011" fin="true">OK</down>
<right ref="RM000002WBW0ICX_04_0016" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0016" proc-id="RM22W0E___0000ENZ00001">
<testtitle>REPLACE REAR DOOR OUTSIDE HANDLE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the rear door outside handle (See page <xref label="Seep01" href="RM0000039H4025X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002WBW0ICX_04_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0006" proc-id="RM22W0E___0000ENX00001">
<testtitle>CHECK REAR DOOR OUTSIDE HANDLE (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the entry function operates normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002WBW0ICX_04_0012" fin="true">OK</down>
<right ref="RM000002WBW0ICX_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0008">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM<xref label="Seep01" href="RM000002T6K05PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0010">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0011">
<testtitle>END (DOOR ELECTRICAL KEY OSCILLATOR IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0012">
<testtitle>END (REAR DOOR OUTSIDE HANDLE IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0014">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM000002WBW0ICX_04_0018">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM (Proceed to Only Rear Door RH LOCK/UNLOCK Functions do not Operate)<xref label="Seep01" href="RM000000TNU074X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>