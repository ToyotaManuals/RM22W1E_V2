<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000H" variety="S000H">
<name>3UR-FE FUEL</name>
<ttl id="12008_S000H_7C3H3_T00A6" variety="T00A6">
<name>FUEL SUB TANK</name>
<para id="RM0000039WY00LX" category="A" type-id="30014" name-id="FU6FT-02" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM0000039WY00LX_01" type-id="01" category="01">
<s-1 id="RM0000039WY00LX_01_0001" proc-id="RM22W0E___000063Y00000">
<ptxt>INSTALL FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the hose to the fuel sub tank as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A176442E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tank Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hose Side Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Align the fuel sub tank side mark with the hose side mark when installing the hose.</ptxt>
</item>
<item>
<ptxt>Tighten the hose clamp until the end of the hose clamp contacts the stopper as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A176441E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
</list1>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stopper</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0002" proc-id="RM22W0E___000063Z00000">
<ptxt>INSTALL NO. 3 FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel hose to the fuel sub tank as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A176443E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tank Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hose Side Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Align the fuel sub tank side mark with the hose side mark when installing the hose.</ptxt>
</item>
<item>
<ptxt>Tighten the hose clamp until the end of the hose clamp contacts the stopper as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A176441E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
</list1>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stopper</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0003" proc-id="RM22W0E___000064000000">
<ptxt>INSTALL FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the breather hose to the fuel sub tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0004" proc-id="RM22W0E___000064100000">
<ptxt>INSTALL FUEL AND EVAPORATION VENT TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the fuel and evaporation vent tube.</ptxt>
</s2>
<s2>
<ptxt>Install the fuel and evaporation vent tube with the 6 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>3.5</t-value1>
<t-value2>36</t-value2>
<t-value3>31</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0014" proc-id="RM22W0E___000064900000">
<ptxt>INSTALL FUEL TANK EVAPORATION VENT TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 fuel tank evaporation vent tubes.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0028">
<ptxt>INSTALL FUEL HOSE</ptxt>
</s-1>
<s-1 id="RM0000039WY00LX_01_0005" proc-id="RM22W0E___000064200000">
<ptxt>INSTALL FUEL SENDER GAUGE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the sender gauge.</ptxt>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the sender gauge with the 5 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>1.5</t-value1>
<t-value2>15</t-value2>
<t-value3>13</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0006" proc-id="RM22W0E___000064300000">
<ptxt>INSTALL FLOOR NO. 3 WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the wire harness clamp to the fuel sub tank.</ptxt>
</s2>
<s2>
<ptxt>Connect the sender gauge connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0007" proc-id="RM22W0E___000064400000">
<ptxt>INSTALL FUEL SUB TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the fuel sub tank on a transmission jack and raise the fuel sub tank.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 fuel tank bands with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the floor No. 3 wire connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 3 wire harness clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0009" proc-id="RM22W0E___000064500000">
<ptxt>CONNECT FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the hose to the filler pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0010" proc-id="RM22W0E___000064600000">
<ptxt>CONNECT NO. 3 FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel hose to the filler pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0011" proc-id="RM22W0E___000064700000">
<ptxt>CONNECT FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the breather hose to the filler pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0027">
<ptxt>CONNECT FUEL HOSE</ptxt>
</s-1>
<s-1 id="RM0000039WY00LX_01_0013" proc-id="RM22W0E___000064800000">
<ptxt>CONNECT FUEL TANK EVAPORATION VENT TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 2 fuel tubes.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0019">
<ptxt>INSTALL FUEL TANK CAP ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM0000039WY00LX_01_0020" proc-id="RM22W0E___000064A00000">
<ptxt>INSTALL NO. 1 SPARE WHEEL STOPPER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the spare wheel stopper with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>32</t-value1>
<t-value2>326</t-value2>
<t-value4>24</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0021" proc-id="RM22W0E___000064B00000">
<ptxt>INSTALL SPARE WHEEL CARRIER CROSSMEMBER AND SPARE WHEEL CARRIER BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the spare wheel carrier crossmember and 2 brackets to the vehicle with the 6 bolts labeled C.</ptxt>
<torque>
<subtitle>for bolt C</subtitle>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Temporarily install the bolt labeled A, and then tighten the bolt labeled A and bolts labeled B.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A176384E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0022" proc-id="RM22W0E___00004VY00000">
<ptxt>INSTALL TAILPIPE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the tailpipe to the 2 exhaust pipe supports.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket to the center exhaust pipe.</ptxt>
</s2>
<s2>
<ptxt>Connect the tailpipe to the center exhaust pipe.</ptxt>
</s2>
<s2>
<ptxt>Attach a new clamp to the tailpipe and center exhaust pipe. Then install the bolt to the clamp.</ptxt>
<figure>
<graphic graphicname="A240602E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<torque>
<torqueitem>
<t-value1>32</t-value1>
<t-value2>326</t-value2>
<t-value4>24</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gasket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Top</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>LH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Install the clamp within the angle range shown in the illustration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039WY00LX_01_0026" proc-id="RM22W0E___000064C00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039WY00LX_01_0023" proc-id="RM22W0E___00004VK00000">
<ptxt>INSPECT FOR FUEL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the engine switch on (IG) and intelligent tester main switch on.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Fuel Pump / Speed.</ptxt>
</s2>
<s2>
<ptxt>Check that there are no fuel leaks after doing maintenance anywhere on the fuel system.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039WY00LX_01_0029" proc-id="RM22W0E___00004VM00000">
<ptxt>INSPECT FOR EXHAUST GAS LEAK
</ptxt>
<content1 releasenbr="1">
<ptxt>If gas is leaking, tighten the areas necessary to stop the leak. Replace damaged parts as necessary.</ptxt>
</content1></s-1>
<s-1 id="RM0000039WY00LX_01_0024">
<ptxt>INSTALL SPARE TIRE</ptxt>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>