<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM00000187I036X" category="C" type-id="302GV" name-id="ES1695-001" from="201301" to="201308">
<dtccode>P0031</dtccode>
<dtcname>Oxygen (A/F) Sensor Heater Control Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P0032</dtccode>
<dtcname>Oxygen (A/F) Sensor Heater Control Circuit High (Bank 1 Sensor 1)</dtcname>
<dtccode>P0051</dtccode>
<dtcname>Oxygen (A/F) Sensor Heater Control Circuit Low (Bank 2 Sensor 1)</dtcname>
<dtccode>P0052</dtccode>
<dtcname>Oxygen (A/F) Sensor Heater Control Circuit High (Bank 2 Sensor 1)</dtcname>
<subpara id="RM00000187I036X_01" type-id="60" category="03" proc-id="RM22W0E___00003B600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>For more information on the air fuel ratio sensor and Diesel Particulate Filter (DPF), refer to the following procedures (See page <xref label="Seep01" href="RM000000XSN037X"/>).</ptxt>
</item>
<item>
<ptxt>These DTCs are related to the air fuel ratio sensor, although the titles say oxygen sensor.</ptxt>
</item>
</list1>
</atten4>
<ptxt>The air fuel ratio sensor outputs a voltage* that is proportional to the air-fuel ratio. The air fuel ratio sensor output voltage is used to control the A/F mixture.</ptxt>
<ptxt>The air fuel ratio sensor is located after the Diesel Particulate Filter (DPF) catalytic converter. This sensor has been developed based on the structure and technology of the air fuel ratio sensor that is used for gasoline engines. The cover for the sensor electrode has been modified to suit its application in a diesel engine. This change allows the sensor to function effectively in this DPF type diesel engine, and it also avoids problems with sensor temperature and particulate matter (PM).</ptxt>
<ptxt>In order to reduce PM, the ECM adjusts the air-fuel ratio to a value slightly richer than that which would otherwise be used (note that this mixture is still leaner than the stoichiometric air fuel ratio). The ECM controls these adjustments on signals from the air fuel ratio sensor.</ptxt>
<ptxt>When the ECM performs PM forced regeneration (cleaning) by adding fuel using the exhaust fuel addition injector assembly, the air fuel ratio sensor feedback is used to ensure that an appropriate air fuel ratio is maintained.</ptxt>
<ptxt>*: This voltage change occurs only inside the ECM. It is not possible to measure this voltage at the sensor. The GTS can be used to monitor this voltage.</ptxt>
<figure>
<graphic graphicname="A258975E01" width="7.106578999in" height="9.803535572in"/>
</figure>
<atten4>
<ptxt>The ECM provides a pulse width modulated control circuit to adjust current through the heater. The air fuel ratio sensor heater circuit uses a relay on the +B side of the circuit.</ptxt>
</atten4>
<figure>
<graphic graphicname="A210271E08" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>P0031 (Bank 1), P0051 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Drive the vehicle at 50 km/h (30 mph) or more for a total of 300 seconds or more.</ptxt>
</item>
<item>
<ptxt>Check that "Exhaust Temperature B1S1" displays 200°C (392°F) or higher using the GTS.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor heater current is 0.8 A or less when the heater operates for 4 seconds.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in air fuel ratio sensor heater circuit</ptxt>
</item>
<item>
<ptxt>air fuel ratio sensor heater</ptxt>
</item>
<item>
<ptxt>A/F relay</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0032 (Bank 1), P0052 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor heater current reaches the high limit (overcurrent is detected even though the air fuel ratio sensor heater is off)</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor heater current is 0.8 A or more when the heater does not operate for 4 seconds.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in air fuel ratio sensor heater circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor heater</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0031</ptxt>
<ptxt>P0032</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>AFS Voltage B1S1</ptxt>
</item>
<item>
<ptxt>AF Lambda B1S1</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B1S1</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0052</ptxt>
<ptxt>P0053</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>AFS Voltage B2S1</ptxt>
</item>
<item>
<ptxt>AF Lambda B2S1</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B2S1</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000187I036X_02" type-id="64" category="03" proc-id="RM22W0E___00003B700000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The inner surface of the air fuel ratio sensor element is exposed to outside air. The outer surface of the sensor element is exposed to exhaust gases. The sensor element is made of platinum coated zirconia and includes an integrated heating element. The zirconia element generates a small voltage when there is a large difference in the oxygen concentrations of the exhaust and the outside air. The platinum coating amplifies the voltage generation. When heated, the sensor becomes very efficient. If the temperature of the exhaust is low, without supplemental heating, the sensor will not generate useful voltage signals. The ECM controls the heating by using a duty-cycle to regulate the average current in the heater element. If the heater current is out of the normal range, the output signals of the sensor are inaccurate and the ECM cannot regulate the air-fuel ratio properly. When the heater current is out of the normal operating range, the ECM interprets this as a malfunction and stores a DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM00000187I036X_03" type-id="32" category="03" proc-id="RM22W0E___00003B800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A275246E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000187I036X_04" type-id="51" category="05" proc-id="RM22W0E___00003B900000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187I036X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187I036X_05_0001" proc-id="RM22W0E___00003BA00000">
<testtitle>INSPECT AIR FUEL RATIO SENSOR (HEATER RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the air fuel ratio sensor (for bank 1 or bank 2) (See page <xref label="Seep01" href="RM000004KG900HX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187I036X_05_0003" fin="false">OK</down>
<right ref="RM00000187I036X_05_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0003" proc-id="RM22W0E___00003BB00000">
<testtitle>CHECK TERMINAL VOLTAGE (AIR FUEL RATIO SENSOR HEATER VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air fuel ratio sensor connector.  </ptxt>
<figure>
<graphic graphicname="A275245E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C159-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>for Bank 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>for Bank 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Air Fuel Ratio Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000187I036X_05_0004" fin="false">OK</down>
<right ref="RM00000187I036X_05_0017" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0004" proc-id="RM22W0E___00003BC00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR FUEL RATIO SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C159-1 (HAF1) - C45-45 (HA1A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-1 (HAF2) - C45-46 (HA2A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C159-1 (HAF1) or C45-45 (HA1A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-1 (HAF2) or C45-46 (HA2A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C159-1 (HAF1) - C46-45 (HA1A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-1 (HAF2) - C46-46 (HA2A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C159-1 (HAF1) or C46-45 (HA1A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-1 (HAF2) or C46-46 (HA2A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187I036X_05_0009" fin="false">OK</down>
<right ref="RM00000187I036X_05_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0009" proc-id="RM22W0E___00003BF00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187I036X_05_0011" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0017" proc-id="RM22W0E___00003BJ00000">
<testtitle>INSPECT A/F RELAY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the A/F relay (See page <xref label="Seep01" href="RM000003BLB02YX_01_0015"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187I036X_05_0018" fin="false">OK</down>
<right ref="RM00000187I036X_05_0020" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0018" proc-id="RM22W0E___00003BK00000">
<testtitle>CHECK HARNESS AND CONNECTOR (A/F RELAY - A/F SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Remove the A/F relay from the engine room relay block and junction block assembly.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C159-2 (+B) - A/F relay terminal 3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-2 (+B) - A/F relay terminal 3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C159-2 (+B) or A/F relay terminal 3 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C158-2 (+B) or A/F relay terminal 3 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187I036X_05_0021" fin="false">OK</down>
<right ref="RM00000187I036X_05_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0021" proc-id="RM22W0E___00003BM00000">
<testtitle>INSPECT INTEGRATION RELAY (EFI MAIN 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the integration relay (EFI MAIN 2) (See page <xref label="Seep01" href="RM000003BLB02YX_01_0003"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187I036X_05_0013" fin="false">OK</down>
<right ref="RM00000187I036X_05_0022" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0013" proc-id="RM22W0E___00003BI00000">
<testtitle>CHECK HARNESS AND CONNECTOR (A/F RELAY - INTEGRATION RELAY, BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the A/F relay from the engine room relay block and junction block assembly.</ptxt>
</test1>
<test1>
<ptxt>Remove the integration relay from the engine room relay block and junction block assembly.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A/F relay terminal 5 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A/F relay terminal 1 - 1A-4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A/F relay terminal 2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A/F relay terminal 1 or 1A-4 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187I036X_05_0005" fin="false">OK</down>
<right ref="RM00000187I036X_05_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0005" proc-id="RM22W0E___00003BD00000">
<testtitle>CHECK HARNESS AND CONNECTOR (INTEGRATION RELAY - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the integration relay from the engine room relay block and junction block assembly.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.53in"/>
<colspec colname="COLSPEC3" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1A-2 - A38-44 (MREL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1A-2 or A38-44 (MREL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.53in"/>
<colspec colname="COLSPEC3" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1A-2 - A52-44 (MREL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1A-2 or A52-44 (MREL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<right ref="RM00000187I036X_05_0023" fin="false">OK</right>
<right ref="RM00000187I036X_05_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0010" proc-id="RM22W0E___00003BG00000">
<testtitle>REPLACE AIR FUEL RATIO SENSOR</testtitle>
<content6 releasenbr="1">
<ptxt>When DTC P0031 or P0032 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the air fuel ratio sensor (for bank 1) (See page <xref label="Seep01" href="RM000004KGB00HX"/>).</ptxt>
</item>
</list1>
<ptxt>When DTC P0051 or P0052 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the air fuel ratio sensor (for bank 2) (See page <xref label="Seep02" href="RM000004KGB00HX"/>).</ptxt>
</item>
</list1>
</content6>
<res>
<right ref="RM00000187I036X_05_0011" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0020" proc-id="RM22W0E___00003BL00000">
<testtitle>REPLACE A/F RELAY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the A/F relay.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187I036X_05_0011" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0022" proc-id="RM22W0E___00003BN00000">
<testtitle>REPLACE INTEGRATION RELAY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the integration relay (EFI MAIN 2).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187I036X_05_0011" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0008" proc-id="RM22W0E___00003BE00000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace harness or connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187I036X_05_0011" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0023" proc-id="RM22W0E___00003BO00000">
<testtitle>CHECK ECM POWER SOURCE CIRCUIT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the ECM power source circuit (See page <xref label="Seep01" href="RM000001DN809BX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187I036X_05_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0011" proc-id="RM22W0E___00003BH00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at 50 km/h (30 mph) or more for a total of 300 seconds or more.</ptxt>
<atten4>
<ptxt>"Exhaust Temperature B1S1" and "Exhaust Temperature B2S1" on the GTS screen should be 200°C (392°F) or higher.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the GTS to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Engine and ECT / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P0031, P0032, P0052 or P0053.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If STATUS is NORMAL, DTC judgment is complete and the system is determined to be normal.</ptxt>
</item>
<item>
<ptxt>If STATUS is INCOMPLETE or N/A, DTC judgment is incomplete. Drive the vehicle at 50 km/h (30 mph) or more for a total of 300 seconds or more again.</ptxt>
</item>
</list1>
</atten4>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000187I036X_05_0012" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187I036X_05_0012">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>