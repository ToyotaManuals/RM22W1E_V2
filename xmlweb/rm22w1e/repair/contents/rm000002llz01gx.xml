<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002LLZ01GX" category="J" type-id="30383" name-id="ACBLI-03" from="201301" to="201308">
<dtccode/>
<dtcname>Rear Air Conditioning Control Panel Circuit</dtcname>
<subpara id="RM000002LLZ01GX_01" type-id="60" category="03" proc-id="RM22W0E___0000H4L00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit consists of the No. 2 air conditioning control assembly* and the air conditioning amplifier assembly. When the No. 2 air conditioning control assembly is operated, signals are transmitted to the air conditioning amplifier assembly through the LIN communication system. </ptxt>
<ptxt>If the LIN communication system malfunctions, the air conditioning amplifier assembly does not operate even if the No. 2 air conditioning control assembly is operated.</ptxt>
<atten4>
<ptxt>*: w/ Rear Heater</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002LLZ01GX_02" type-id="32" category="03" proc-id="RM22W0E___0000H4M00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E157369E11" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002LLZ01GX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002LLZ01GX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002LLZ01GX_04_0009" proc-id="RM22W0E___0000H4Q00000">
<testtitle>INSPECT FUSE (ECU-IG NO. 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the ECU-IG NO. 2 fuse from the cowl side junction block LH.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ECU-IG NO. 2 fuse</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LLZ01GX_04_0001" fin="false">OK</down>
<right ref="RM000002LLZ01GX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLZ01GX_04_0001" proc-id="RM22W0E___0000H4N00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 2 AIR CONDITIONING CONTROL - BODY GROUND AND BATTERY)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E127597E04" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E55 No. 2 air conditioning control assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E55-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E55-6 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LLZ01GX_04_0004" fin="false">OK</down>
<right ref="RM000002LLZ01GX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLZ01GX_04_0004" proc-id="RM22W0E___0000H4O00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - NO. 2 AIR CONDITIONING CONTROL)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E157371E01" width="2.775699831in" height="5.787629434in"/>
</figure>
<test1>
<ptxt>Disconnect the E55 No. 2 air conditioning control assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E36 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E36-21 (RLIN) - E55-4 (RLIN)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E55-4 (RLIN) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E36-21 (RLIN) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LLZ01GX_04_0005" fin="false">OK</down>
<right ref="RM000002LLZ01GX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLZ01GX_04_0005" proc-id="RM22W0E___0000H4P00000">
<testtitle>CHECK NO. 2 AIR CONDITIONING CONTROL ASSEMBLY (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the No. 2 air conditioning control assembly with a new or properly functioning one (See page <xref label="Seep01" href="RM000004J7V005X"/>).</ptxt>
</test1>
<test1>
<ptxt>Operate the No. 2 air conditioning control assembly to check that it functions properly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No. 2 air conditioning control assembly function operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LLZ01GX_04_0008" fin="true">OK</down>
<right ref="RM000002LLZ01GX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLZ01GX_04_0008">
<testtitle>REPLACE NO. 2 AIR CONDITIONING CONTROL ASSEMBLY<xref label="Seep01" href="RM000004J7V005X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LLZ01GX_04_0010">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000002LLZ01GX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002LLZ01GX_04_0006">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>