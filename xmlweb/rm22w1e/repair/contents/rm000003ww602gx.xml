<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001X" variety="S001X">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001X_7C3RY_T00L1" variety="T00L1">
<name>PARKING ASSIST MONITOR SYSTEM (w/ Side Monitor System)</name>
<para id="RM000003WW602GX" category="C" type-id="803N0" name-id="PM5KY-02" from="201301" to="201308">
<dtccode>C168D</dtccode>
<dtcname>Vehicle Information Unmatched</dtcname>
<subpara id="RM000003WW602GX_01" type-id="60" category="03" proc-id="RM22W0E___0000D1400000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored if the parking assist ECU judges as a result of its self check that the vehicle information received via CAN communication and the vehicle information stored in the parking assist ECU do not match.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C168D</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle Information Unmatched</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Back camera position setting</ptxt>
</item>
<item>
<ptxt>Steering angle setting</ptxt>
</item>
<item>
<ptxt>Side camera position setting</ptxt>
</item>
<item>
<ptxt>Parking assist ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003WW602GX_02" type-id="51" category="05" proc-id="RM22W0E___0000D1500000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When "System initializing" is displayed on the parking assist ECU after the battery terminal disconnected, correct the steering angle neutral point (See page <xref label="Seep01" href="RM0000035DE03CX"/>).</ptxt>
</item>
<item>
<ptxt>Depending on the parts that are replaced or operations that are performed during vehicle inspection or maintenance, calibration of other systems as well as the parking assist monitor system (w/ Side Monitor System) may be needed (See page <xref label="Seep02" href="RM0000035D303ZX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000003WW602GX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003WW602GX_03_0001" proc-id="RM22W0E___0000D1600000">
<testtitle>PERFORM SETTINGS (BACK CAMERA POSITION SETTING, STEERING ANGLE SETTING, SIDE CAMERA POSITION SETTING)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform the following procedure depending on the memory conditions to register the vehicle information into the parking assist ECU. </ptxt>
<test2>
<ptxt>Refer to back camera position setting (See page <xref label="Seep01" href="RM0000035DD03SX"/>).</ptxt>
</test2>
<test2>
<ptxt>Refer to steering angle setting (See page <xref label="Seep02" href="RM0000035DD03SX"/>).</ptxt>
</test2>
<test2>
<ptxt>Refer to side camera position setting (See page <xref label="Seep03" href="RM0000035DD03SX"/>).</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM000003WW602GX_03_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WW602GX_03_0002" proc-id="RM22W0E___0000D1700000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000035DB03WX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000035DB03WX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC C168D is not output.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTC C168D is output, perform the procedure to allow the parking assist ECU to store the vehicle information again.</ptxt>
</item>
<item>
<ptxt>If DTC C168D is still output after performing the procedure to make the parking assist ECU store the vehicle information 3 times, replace the parking assist ECU.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003WW602GX_03_0004" fin="true">OK</down>
<right ref="RM000003WW602GX_03_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WW602GX_03_0004">
<testtitle>END (SETTINGS ARE DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000003WW602GX_03_0003">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM0000039LH004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>