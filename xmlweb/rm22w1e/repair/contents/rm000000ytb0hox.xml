<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000000YTB0HOX" category="C" type-id="305UJ" name-id="ES11H3-001" from="201301">
<dtccode>P0365</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit (Bank 1)</dtcname>
<dtccode>P0367</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit Low Input (Bank 1)</dtcname>
<dtccode>P0368</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit High Input (Bank 1)</dtcname>
<dtccode>P0390</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit (Bank 2)</dtcname>
<dtccode>P0392</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit Low Input (Bank 2)</dtcname>
<dtccode>P0393</dtccode>
<dtcname>Camshaft Position Sensor "B" Circuit High Input (Bank 2)</dtcname>
<subpara id="RM000000YTB0HOX_01" type-id="60" category="03" proc-id="RM22W0E___00000U400000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The exhaust camshaft's Variable Valve Timing (VVT) sensor (EV1, EV2 signal) consists of a magnet and MRE (Magnetoresistive Element).</ptxt>
<ptxt>The exhaust camshaft has 3 teeth on its outer circumference.</ptxt>
<ptxt>When the exhaust camshaft rotates, changes occur in the air gaps between the 3 teeth and MRE, which affects the magnetic field. As a result, the resistance of the MRE material fluctuates. The VVT sensor converts the exhaust camshaft rotation data to pulse signals, uses the pulse signals to determine the camshaft angle, and sends the camshaft angle data to the ECM.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0365</ptxt>
<ptxt>P0390</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is no VVT sensor signal despite the crankshaft position sensor input being normal at an engine speed of 600 rpm or more (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in VVT sensor (for exhaust side) circuit</ptxt>
</item>
<item>
<ptxt>VVT sensor (for exhaust side)</ptxt>
</item>
<item>
<ptxt>Exhaust camshaft</ptxt>
</item>
<item>
<ptxt>Timing chain has jumped tooth</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0367</ptxt>
<ptxt>P0392</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output voltage of the VVT sensor is below 0.3 V for 4 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<title/>
<item>
<ptxt>Open or short in VVT sensor (for exhaust side) circuit</ptxt>
</item>
<item>
<ptxt>VVT sensor (for exhaust side)</ptxt>
</item>
<item>
<ptxt>Exhaust camshaft</ptxt>
</item>
<item>
<ptxt>Timing chain has jumped tooth</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0368</ptxt>
<ptxt>P0393</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output voltage of the VVT sensor is higher than 4.7 V for 4 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<title/>
<item>
<ptxt>Open or short in VVT sensor (for exhaust side) circuit</ptxt>
</item>
<item>
<ptxt>VVT sensor (for exhaust side)</ptxt>
</item>
<item>
<ptxt>Exhaust camshaft</ptxt>
</item>
<item>
<ptxt>Timing chain has jumped tooth</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>Reference: Inspection using an oscilloscope</ptxt>
<figure>
<graphic graphicname="A228079E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The correct waveforms are as shown.</ptxt>
</item>
<item>
<ptxt>EV1 and EV2 are the VVT sensor signal, and NE is the crankshaft position sensor signal.</ptxt>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal</ptxt>
</entry>
<entry valign="middle">
<ptxt>NE+ - NE-</ptxt>
<ptxt>EV1+ - EV1-</ptxt>
<ptxt>EV2+ - EV2-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Equipment Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV. </ptxt>
<ptxt>20 msec./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cranking or idling</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000YTB0HOX_02" type-id="64" category="03" proc-id="RM22W0E___00000U500000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If no signal is transmitted by the VVT sensor despite the camshaft revolving, or the rotations of the camshaft and crankshaft are not synchronized, the ECM interprets this as a malfunction of the sensor.</ptxt>
</content5>
</subpara>
<subpara id="RM000000YTB0HOX_07" type-id="32" category="03" proc-id="RM22W0E___00000U600000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A160377E13" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000YTB0HOX_08" type-id="51" category="05" proc-id="RM22W0E___00000U700000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000YTB0HOX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YTB0HOX_09_0002" proc-id="RM22W0E___00000U800000">
<testtitle>CHECK TERMINAL VOLTAGE (SENSOR POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the VVT sensor connector.</ptxt>
<figure>
<graphic graphicname="A208648E33" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C121-3 (VC2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-3 (VC2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>for Bank 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>for Bank 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to VVT Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HOX_09_0003" fin="false">OK</down>
<right ref="RM000000YTB0HOX_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0003" proc-id="RM22W0E___00000U900000">
<testtitle>CHECK HARNESS AND CONNECTOR (VVT SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the VVT sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C121-1 (EX+) - C45-69 (EV1+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C121-2 (EX-) - C45-68 (EV1-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-1 (EX+) - C45-64 (EV2+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-2 (EX-) - C45-65 (EV2-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C121-1 (EX+) or C45-69 (EV1+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C121-2 (EX-) or C45-68 (EV1-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-1 (EX+) or C45-64 (EV2+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-2 (EX-) or C45-65 (EV2-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C121-1 (EX+) - C46-69 (EV1+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C121-2 (EX-) - C46-68 (EV1-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-1 (EX+) - C46-64 (EV2+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-2 (EX-) - C46-65 (EV2-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C121-1 (EX+) or C46-69 (EV1+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C121-2 (EX-) or C46-68 (EV1-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-1 (EX+) or C46-64 (EV2+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C122-2 (EX-) or C46-65 (EV2-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HOX_09_0004" fin="false">OK</down>
<right ref="RM000000YTB0HOX_09_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0004" proc-id="RM22W0E___00000UA00000">
<testtitle>CHECK SENSOR INSTALLATION (VVT SENSOR FOR EXHAUST CAMSHAFT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the VVT sensor installation.</ptxt>
<figure>
<graphic graphicname="BR03795E18" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>Sensor is installed correctly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HOX_09_0005" fin="false">OK</down>
<right ref="RM000000YTB0HOX_09_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0005" proc-id="RM22W0E___00000UB00000">
<testtitle>CHECK EXHAUST CAMSHAFT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the teeth of the camshaft.</ptxt>
</test1>
<spec>
<title>OK</title>
<specitem>
<ptxt>Teeth do not have any cracks or deformation.</ptxt>
</specitem>
</spec>
</content6>
<res>
<down ref="RM000000YTB0HOX_09_0006" fin="false">OK</down>
<right ref="RM000000YTB0HOX_09_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0006" proc-id="RM22W0E___00000UC00000">
<testtitle>REPLACE VVT SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the VVT sensor (See page <xref label="Seep01" href="RM0000028AM014X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HOX_09_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0012" proc-id="RM22W0E___00000UD00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14FX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0365, P0367, P0368, P0390, P0392 or P0393 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the engine does not start, replace the ECM.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000YTB0HOX_09_0014" fin="true">A</down>
<right ref="RM000000YTB0HOX_09_0013" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0009">
<testtitle>SECURELY REINSTALL SENSOR<xref label="Seep01" href="RM0000028AK014X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0010">
<testtitle>REPLACE EXHAUST CAMSHAFT<xref label="Seep01" href="RM000003B6X013X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0013">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YTB0HOX_09_0014">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>