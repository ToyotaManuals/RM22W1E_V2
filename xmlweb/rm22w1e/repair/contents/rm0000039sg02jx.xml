<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12037_S001R" variety="S001R">
<name>VARIABLE GEAR RATIO STEERING</name>
<ttl id="12037_S001R_7C3Q9_T00JC" variety="T00JC">
<name>STEERING ACTUATOR</name>
<para id="RM0000039SG02JX" category="A" type-id="30014" name-id="VG0A6-01" from="201308">
<name>INSTALLATION</name>
<subpara id="RM0000039SG02JX_01" type-id="01" category="01">
<s-1 id="RM0000039SG02JX_01_0048" proc-id="RM22W0E___0000BAP00001">
<ptxt>HANDLING PRECAUTIONS FOR STEERING ACTUATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful that the No. 2 seal lip or boot does not turn outward while carrying or installing the steering actuator assembly. If installing a new steering actuator assembly, make sure that the spiral center lock pin is securely inserted.</ptxt>
</item>
<item>
<ptxt>Do not use the steering actuator assembly if it has been dropped.</ptxt>
<figure>
<graphic graphicname="C179309E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039SG02JX_01_0002" proc-id="RM22W0E___0000BA900001">
<ptxt>INSTALL STEERING ACTUATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Make sure that the power steering link assembly is centered.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C179311E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Install the steering actuator assembly.</ptxt>
<s3>
<ptxt>If installing a new steering actuator assembly:</ptxt>
<ptxt>Install the steering actuator assembly with the white paint on the upper surface of the spiral case facing down.</ptxt>
<atten3>
<ptxt>Do not pull out the center lock pin.</ptxt>
</atten3>
</s3>
<s3>
<figure>
<graphic graphicname="C179313E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>If reinstalling the removed steering actuator assembly:</ptxt>
<list1 type="ordered">
<item>
<ptxt>Slowly turn the spiral case clockwise until it locks.</ptxt>
</item>
<item>
<ptxt>Turn the spiral case two turns counterclockwise from the lock position.</ptxt>
</item>
<item>
<ptxt>Align the slit of the sliding yoke with the alignment mark (▲).</ptxt>
</item>
<item>
<ptxt>Install the steering actuator assembly with the white paint on the upper surface of the spiral case facing down.</ptxt>
</item>
</list1>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="C172649E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Align the matchmarks on the No. 2 steering intermediate shaft and steering actuator.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not fold back the boot part of the steering hole cover or turn it excessively. If it is turned excessively, return it to its original position.</ptxt>
</item>
<item>
<ptxt>Do not turn the actuator body and the spiral case.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Install the steering actuator from the inside of the vehicle.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>360</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="C180144" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using needle nose pliers, lock the clamp to the steering column hole cover to install it.</ptxt>
<atten3>
<ptxt>Be careful when performing the operation as the clamp may not lock if the claws of the clamp are deformed.</ptxt>
</atten3>
</s2>
<s2>
<figure>
<graphic graphicname="C179338" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Move the lock in the direction of the arrow and connect the steering actuator connector.</ptxt>
<atten4>
<ptxt>When a new actuator is installed, remove the center lock pin.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SG02JX_01_0045" proc-id="RM22W0E___0000BAM00001">
<ptxt>INSTALL STEERING COLUMN ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C179337E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Align the matchmarks on the steering actuator and steering column.</ptxt>
</s2>
<s2>
<ptxt>Install the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>360</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="C172653" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Install the steering column with the 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>26</t-value1>
<t-value2>265</t-value2>
<t-value4>19</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SG02JX_01_0043" proc-id="RM22W0E___0000BAL00001">
<ptxt>INSTALL NO. 3 AIR DUCT SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the duct.</ptxt>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0006" proc-id="RM22W0E___0000BAA00001">
<ptxt>CONNECT WIRE HARNESS PROTECTOR AND WIRE HARNESS</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172657" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws to connect the wire harness protector and wire harness.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SG02JX_01_0027" proc-id="RM22W0E___0000AT300001">
<ptxt>INSTALL DRIVER SIDE KNEE AIRBAG ASSEMBLY (w/ Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B189604E01" width="2.775699831in" height="6.791605969in"/>
</figure>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the driver side knee airbag with the 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0046" proc-id="RM22W0E___0000BAN00001">
<ptxt>INSTALL LOWER INSTRUMENT PANEL SUB-ASSEMBLY (w/o Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180299" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws and connect the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Install the lower instrument panel with the 5 bolts.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0028" proc-id="RM22W0E___0000A9H00001">
<ptxt>INSTALL NO. 1 SWITCH HOLE BASE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180298" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 claws to install the No. 1 switch hole base.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0029" proc-id="RM22W0E___0000A9I00001">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Attach the 2 claws to install the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to connect the 2 control cables.</ptxt>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 16 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 9 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B182569" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to close the hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0030" proc-id="RM22W0E___0000A9J00001">
<ptxt>INSTALL COWL SIDE TRIM BOARD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181911" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 clips to install the cowl side trim board.</ptxt>
</s2>
<s2>
<ptxt>Install the cap nut.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0031" proc-id="RM22W0E___000014600001">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY (w/ Floor Under Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182553" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 3 claws to install the No. 1 instrument panel under cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0042" proc-id="RM22W0E___0000AT400001">
<ptxt>INSTALL FRONT DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181682" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 7 claws and 4 clips to install the scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0032" proc-id="RM22W0E___0000BAF00001">
<ptxt>INSTALL INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY (w/o Multi-information Display)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180016" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 9 claws to install the instrument cluster finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0047" proc-id="RM22W0E___0000BAO00001">
<ptxt>INSTALL INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY (w/ Multi-information Display)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180017" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 9 claws to install the instrument cluster finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0033" proc-id="RM22W0E___0000BAG00001">
<ptxt>INSTALL NO. 2 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the No. 2 instrument cluster finish panel garnish.</ptxt>
<figure>
<graphic graphicname="B291252E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Entry and Start System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0034" proc-id="RM22W0E___0000BAH00001">
<ptxt>INSTALL NO. 1 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E155133" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws to install the No. 1 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0035" proc-id="RM22W0E___0000BAI00001">
<ptxt>INSTALL INSTRUMENT SIDE PANEL LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E155518" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 6 claws to install the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0036" proc-id="RM22W0E___00008U800000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292995" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Connect the connectors and attach the 2 clamps.</ptxt>
</s2>
<s2>
<ptxt>Attach the 8 claws to install the lower instrument panel pad sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Install the clip and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0037" proc-id="RM22W0E___00008U900000">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292997" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 7 claws to install the No. 2 instrument panel finish panel cushion.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0019" proc-id="RM22W0E___0000BAB00001">
<ptxt>INSTALL COMBINATION SWITCH ASSEMBLY WITH SPIRAL CABLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172677" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using pliers, grip the claws of the clamp and install the combination signal switch assembly with spiral cable sub-assembly to the steering column assembly with the clamp.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C172676" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Connect the 5 connectors to the combination switch with spiral cable.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SG02JX_01_0020" proc-id="RM22W0E___0000BAC00001">
<ptxt>INSTALL TILT AND TELESCOPIC SWITCH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C159804" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the switch.</ptxt>
</s2>
<s2>
<ptxt>Connect the switch connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SG02JX_01_0021" proc-id="RM22W0E___0000BAD00001">
<ptxt>INSTALL UPPER STEERING COLUMN COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172675" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the upper steering column cover.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 clips to the instrument cluster finish panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SG02JX_01_0022" proc-id="RM22W0E___0000BAE00001">
<ptxt>INSTALL LOWER STEERING COLUMN COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172674" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the lower steering column cover.</ptxt>
<atten3>
<ptxt>Do not damage the tilt and telescopic switch.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>1.5</t-value1>
<t-value2>15</t-value2>
<t-value3>13</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SG02JX_01_0044" proc-id="RM22W0E___0000A9400001">
<ptxt>INSTALL STEERING WHEEL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the matchmarks on the steering wheel assembly and steering main shaft assembly.</ptxt>
<figure>
<graphic graphicname="C174823E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the steering wheel assembly set nut.</ptxt>
<torque>
<torqueitem>
<t-value1>50</t-value1>
<t-value2>510</t-value2>
<t-value4>37</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0024" proc-id="RM22W0E___000077F00001">
<ptxt>INSTALL STEERING PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Support the steering pad with one hand.</ptxt>
<figure>
<graphic graphicname="B181568" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the 2 connectors to the steering pad.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the horn connector.</ptxt>
</s2>
<s2>
<ptxt>Confirm that the circumference groove of the "TORX" screw fits in the screw case, and place the steering pad onto the steering wheel.</ptxt>
</s2>
<s2>
<ptxt>Using a T30 "TORX" socket wrench, tighten the 2 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>8.8</t-value1>
<t-value2>90</t-value2>
<t-value3>78</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0025" proc-id="RM22W0E___000077G00001">
<ptxt>INSTALL LOWER NO. 2 STEERING WHEEL COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
<figure>
<graphic graphicname="B189186E01" width="2.775699831in" height="4.7836529in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0026" proc-id="RM22W0E___000077H00001">
<ptxt>INSTALL LOWER NO. 3 STEERING WHEEL COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
<figure>
<graphic graphicname="B181563" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039SG02JX_01_0040" proc-id="RM22W0E___0000BAJ00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Reset the AUTO TILT AWAY function setting to the previous condition by changing the customize parameter (See page <xref label="Seep01" href="RM000000UZ00A8X"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003C32006X"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039SG02JX_01_0041" proc-id="RM22W0E___0000BAK00001">
<ptxt>INSPECT SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0KMX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SG02JX_01_0049" proc-id="RM22W0E___0000BAQ00001">
<ptxt>PERFORM VARIABLE GEAR RATIO STEERING SYSTEM CALIBRATION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Perform variable gear ratio steering system calibration (See page <xref label="Seep01" href="RM000000XOA01XX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>