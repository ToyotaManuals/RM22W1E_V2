<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S0010" variety="S0010">
<name>1UR-FE LUBRICATION</name>
<ttl id="12012_S0010_7C3KP_T00DS" variety="T00DS">
<name>ENGINE OIL COOLER</name>
<para id="RM00000314N01WX" category="A" type-id="30014" name-id="LU4MA-01" from="201308">
<name>INSTALLATION</name>
<subpara id="RM00000314N01WX_01" type-id="01" category="01">
<s-1 id="RM00000314N01WX_01_0002" proc-id="RM22W0E___00006ZF00001">
<ptxt>INSTALL OIL COOLER SPACER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to a new gasket.</ptxt>
</s2>
<s2>
<ptxt>Install the gasket to the oil cooler spacer.</ptxt>
</s2>
<s2>
<ptxt>Install the oil cooler spacer to the oil filter bracket with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000314N01WX_01_0003" proc-id="RM22W0E___00006ZG00001">
<ptxt>INSTALL OIL COOLER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to 2 new O-rings.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 O-rings to the oil cooler.</ptxt>
</s2>
<s2>
<ptxt>Install the oil cooler with the 5 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000314N01WX_01_0004" proc-id="RM22W0E___00004NZ00001">
<ptxt>INSTALL OIL FILTER BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to 2 new O-rings.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 O-rings to the timing chain cover.</ptxt>
</s2>
<s2>
<ptxt>Install the oil filter bracket with the 2 nuts and 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000314N01WX_01_0005" proc-id="RM22W0E___00006YM00001">
<ptxt>INSTALL NO. 1 OIL COOLER BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the ground wire to the cylinder block.</ptxt>
</s2>
<s2>
<ptxt>Install the oil cooler bracket with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000314N01WX_01_0038" proc-id="RM22W0E___00004L700000">
<ptxt>INSTALL NO. 2 WATER BY-PASS PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 4 hoses.</ptxt>
</s2>
<s2>
<ptxt>Install the water by-pass pipe with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000314N01WX_01_0020" proc-id="RM22W0E___00004OC00000">
<ptxt>INSTALL OIL PRESSURE SENDER GAUGE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply adhesive to 2 or 3 threads of the oil pressure sender gauge.</ptxt>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1344, Three Bond 1344 or equivalent</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not apply adhesive to the oil inlet port of the oil pressure sender gauge.</ptxt>
<figure>
<graphic graphicname="A178540E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Oil Inlet Port</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the oil pressure sender gauge.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>153</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not start the engine within 1 hour after installation.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the oil pressure sender gauge connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000314N01WX_01_0015" proc-id="RM22W0E___00004O000000">
<ptxt>INSTALL OIL FILTER ELEMENT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the inside of the oil filter cap, threads and O-ring groove.</ptxt>
</s2>
<s2>
<ptxt>Apply a light coat of engine oil to a new O-ring for the cap, and then install the O-ring to the groove of the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A268595E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to install the O-ring in the proper location, otherwise oil may leak.</ptxt>
</item>
<item>
<ptxt>Do not twist the O-ring.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Set a new oil filter element to the oil filter cap.</ptxt>
</s2>
<s2>
<ptxt>Remove any dirt or foreign matter from the installation surface of the engine.</ptxt>
</s2>
<s2>
<ptxt>Apply a small amount of engine oil to the O-ring again and temporarily install the oil filter cap.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not remove the oil filter bracket clip.</ptxt>
</item>
<item>
<ptxt>Do not pinch the O-ring for the cap.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Using SST, tighten the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A271374E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<sst>
<sstitem>
<s-number>09228-06501</s-number>
</sstitem>
</sst>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Filter Bracket Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Filter Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Filter Bracket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>No Gap</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When tightening the oil filter cap, do not remove the oil filter bracket clip.</ptxt>
</item>
<item>
<ptxt>After tightening the oil filter cap, make sure that there is no gap and that the O-ring is not protruding.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Apply a small amount of engine oil to a new drain plug O-ring, and install it to the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A272716E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Before installing the O-ring, remove any dirt or foreign matter from the installation surface of the oil filter cap.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the oil filter drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Be careful that the O-ring does not get caught between any surrounding parts.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000314N01WX_01_0041" proc-id="RM22W0E___00006YE00001">
<ptxt>CONNECT COOLER COMPRESSOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the cooler compressor with the stud bolt.</ptxt>
<figure>
<graphic graphicname="A178491E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the 3 bolts and nut.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>250</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Tighten the bolts and nut in the order shown in the illustration to install the cooler compressor.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000314N01WX_01_0039" proc-id="RM22W0E___00006ZH00001">
<ptxt>INSTALL RADIATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000003AB200JX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM00000314N01WX_01_0022" proc-id="RM22W0E___00004GW00000">
<ptxt>ADD ENGINE OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add fresh oil and install the oil filler cap.</ptxt>
<spec>
<title>Standard Oil Grade</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Oil Grade</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Viscosity (SAE)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>API grade SL "energy-conserving", SM "energy-conserving", SN "resource-conserving" or ILSAC multigrade engine oil</ptxt>
</entry>
<entry valign="middle">
<ptxt>0W-20</ptxt>
<ptxt>5W-20</ptxt>
<ptxt>5W-30</ptxt>
<ptxt>10W-30</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>API grade SL, SM or SN multigrade engine oil</ptxt>
</entry>
<entry valign="middle">
<ptxt>15W-40</ptxt>
<ptxt>20W-50</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Oil Capacity</title>
<table>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Drain and refill without oil filter change</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>7.1 liters (7.5 US qts, 6.2 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Drain and refill with oil filter change</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>7.5 liters (7.9 US qts, 6.6 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Dry fill</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>9.3 liters (9.8 US qts, 8.2 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM00000314N01WX_01_0040" proc-id="RM22W0E___00006Y400000">
<ptxt>INSPECT ENGINE OIL LEVEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Warm up the engine. Then stop the engine and wait for 5 minutes.</ptxt>
</s2>
<s2>
<ptxt>Check that the engine oil level is between the dipstick low level mark and full level mark.</ptxt>
<figure>
<graphic graphicname="A297375E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measuring Surface</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Low Level Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Full Level Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the level is low, check for leakage and add oil up to the full level mark.</ptxt>
<atten3>
<ptxt>Do not fill engine oil above the full level mark.</ptxt>
</atten3>
<atten4>
<ptxt>A certain amount of engine oil will be consumed while driving. In the following situations, oil consumption may increase, and engine oil may need to be refilled in between oil maintenance intervals.</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the engine is new, for example directly after purchasing the vehicle or after replacing the engine.</ptxt>
</item>
<item>
<ptxt>If low quality oil or oil of an inappropriate viscosity is used.</ptxt>
</item>
<item>
<ptxt>When driving at high engine speed or with a heavy load, (when towing, or), when driving while accelerating or decelerating frequently.</ptxt>
</item>
<item>
<ptxt>When leaving the idling for a long time, or when driving frequently through heavy traffic.</ptxt>
</item>
</list1>
<ptxt>When judging the amount of oil consumption, keep in mind that the oil may have become diluted, making it difficult to judge the true level accurately.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000314N01WX_01_0017" proc-id="RM22W0E___00004HF00000">
<ptxt>INSPECT FOR OIL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start the engine. Make sure that there are no oil leaks from the area that was worked on.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>