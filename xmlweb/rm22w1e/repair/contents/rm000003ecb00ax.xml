<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3Y1_T00R4" variety="T00R4">
<name>POWER BACK DOOR SYSTEM</name>
<para id="RM000003ECB00AX" category="C" type-id="802A0" name-id="DH04V-03" from="201301" to="201308">
<dtccode>B2250</dtccode>
<dtcname>Back Door Closer Operation Malfunction</dtcname>
<subpara id="RM000003ECB00AX_01" type-id="60" category="03" proc-id="RM22W0E___0000INI00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The power back door unit assembly (power back door ECU) inputs signals from the latch switch, closer position switch and back door courtesy switch, which are built into the back door lock assembly. Based on these switch signals, the latch position of the back door lock assembly is determined.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B2250</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>While back door closer is operating, malfunction is detected in position information from closer position switch within specified amount of time</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Back door lock assembly</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Power back door unit assembly (power back door ECU)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003ECB00AX_02" type-id="32" category="03" proc-id="RM22W0E___0000INJ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B312260E05" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003ECB00AX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003ECB00AX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003ECB00AX_04_0001" proc-id="RM22W0E___0000INK00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CLOSER POSITION SW)</testtitle>
<content6 releasenbr="2">
<test1>
<ptxt>Check the Data List for proper functioning of each switch (See page <xref label="Seep01" href="RM000003GK3002X"/>).</ptxt>
<table pgwide="1">
<title>Back Door</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Closer Position SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Closer position switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Closer position switch is on</ptxt>
<ptxt>OFF: Closer position switch is off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003ECB00AX_04_0004" fin="true">OK</down>
<right ref="RM000003ECB00AX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003ECB00AX_04_0002" proc-id="RM22W0E___0000INL00000">
<testtitle>INSPECT BACK DOOR LOCK ASSEMBLY (CLOSER POSITION SW)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the back door lock assembly (See page <xref label="Seep01" href="RM0000039H800TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the back door lock assembly (See page <xref label="Seep02" href="RM000001VYW00WX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003ECB00AX_04_0003" fin="false">OK</down>
<right ref="RM000003ECB00AX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003ECB00AX_04_0003" proc-id="RM22W0E___0000INM00000">
<testtitle>CHECK HARNESS AND CONNECTOR (BACK DOOR LOCK ASSEMBLY - POWER BACK DOOR UNIT ASSEMBLY [POWER BACK DOOR ECU] AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the S22 back door lock assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the L52 power back door unit assembly (power back door ECU) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>S22-6 (OPN) - L52-24 (POS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S22-4 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S22-6 (OPN) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003ECB00AX_04_0004" fin="true">OK</down>
<right ref="RM000003ECB00AX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003ECB00AX_04_0004">
<testtitle>REPLACE POWER BACK DOOR UNIT ASSEMBLY (POWER BACK DOOR ECU)<xref label="Seep01" href="RM0000039HB017X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003ECB00AX_04_0005">
<testtitle>REPLACE BACK DOOR LOCK ASSEMBLY<xref label="Seep01" href="RM0000039H800TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003ECB00AX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>