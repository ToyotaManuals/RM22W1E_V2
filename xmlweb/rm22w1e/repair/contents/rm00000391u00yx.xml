<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UN_T00NQ" variety="T00NQ">
<name>REAR NO. 2 SEAT ASSEMBLY (except Face to Face Seat Type)</name>
<para id="RM00000391U00YX" category="A" type-id="8000E" name-id="SE6SD-03" from="201308">
<name>REASSEMBLY</name>
<subpara id="RM00000391U00YX_02" type-id="11" category="10" proc-id="RM22W0E___0000G1J00001">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000391U00YX_01" type-id="01" category="01">
<s-1 id="RM00000391U00YX_01_0037" proc-id="RM22W0E___0000G1C00001">
<ptxt>INSTALL NO. 2 SEAT RECLINING RELEASE HANDLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the release handle with the bolt.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0002" proc-id="RM22W0E___0000G0H00001">
<ptxt>INSTALL REAR SEATBACK LOCK STRIKER COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 clamps to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0001" proc-id="RM22W0E___0000G0G00001">
<ptxt>INSTALL REAR NO. 2 SEATBACK COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0003" proc-id="RM22W0E___0000G0I00001">
<ptxt>INSTALL REAR SEATBACK LOCK STRIKER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181859" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the lock striker with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>138</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0004" proc-id="RM22W0E___0000G0J00001">
<ptxt>INSTALL REAR SEAT LOCK CONTROL CABLE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181858" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 2 clamps to install the lock control cable.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 cable clamps to connect the 2 cables.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0005" proc-id="RM22W0E___0000G0K00001">
<ptxt>INSTALL REAR INNER SEAT RECLINING COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181857" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0006" proc-id="RM22W0E___0000G0L00001">
<ptxt>INSTALL REAR INNER SEAT RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0007" proc-id="RM22W0E___0000G0M00001">
<ptxt>INSTALL NO. 3 SEATBACK COVER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184053E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using hog ring pliers, install the seatback cover to the seatback pad with new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0008" proc-id="RM22W0E___0000G0N00001">
<ptxt>INSTALL SEATBACK COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using hog ring pliers, install the seatback cover with pad to the seatback frame with a new hog rings.</ptxt>
<figure>
<graphic graphicname="B181851" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>LH Side:</ptxt>
<s3>
<ptxt>w/ Rear Center Seat Headrest:</ptxt>
<list1 type="ordered">
<item>
<ptxt>Attach the 8 claws to install the 4 headrest supports.</ptxt>
<figure>
<graphic graphicname="B181850" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Install the rear seat protector.</ptxt>
</item>
<item>
<ptxt>Insert the center headrest storage bag.</ptxt>
<figure>
<graphic graphicname="B184046" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Attach the 2 hooks.</ptxt>
<figure>
<graphic graphicname="B181848" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Close the fastener.</ptxt>
<figure>
<graphic graphicname="B184047" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
</list1>
</s3>
<s3>

<ptxt>w/o Rear Center Seat Headrest:</ptxt>
<list1 type="ordered">
<item>
<ptxt>Attach the 4 claws to install the 2 headrest supports.</ptxt>
<figure>
<graphic graphicname="B235505" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Install the rear seat protector.</ptxt>
</item>
<item>
<ptxt>Attach the hook.</ptxt>
<figure>
<graphic graphicname="B235503" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
</list1>
</s3>
</s2>
<s2>
<ptxt>RH Side:</ptxt>
<s3>
<ptxt>Attach the 4 claws to install the 2 headrest supports.</ptxt>
<figure>
<graphic graphicname="B181854" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Install the rear seat protector.</ptxt>
</s3>
<s3>
<ptxt>Attach the hook.</ptxt>
<figure>
<graphic graphicname="B181853" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0010" proc-id="RM22W0E___0000G0O00001">
<ptxt>INSTALL REAR SEAT HEADREST HANDLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the snap ring to the headrest handle.</ptxt>
</s2>
<s2>
<ptxt>Install the headrest handle to the headrest.</ptxt>
<figure>
<graphic graphicname="B184054" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0011" proc-id="RM22W0E___0000G0P00001">
<ptxt>INSTALL REAR SEAT HEADREST ASSEMBLY (w/ Folding Headrest)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190319E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>While releasing the lock inside the headrest support with a screwdriver, install the headrest.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0042">
<ptxt>INSTALL REAR SEAT HEADREST ASSEMBLY (w/o Folding Headrest)</ptxt>
</s-1>
<s-1 id="RM00000391U00YX_01_0012" proc-id="RM22W0E___0000G0Q00001">
<ptxt>INSTALL REAR SEAT HINGE PAWL GUIDE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181846" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the guide with the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0013" proc-id="RM22W0E___0000G0R00001">
<ptxt>INSTALL REAR SEAT HINGE PAWL GUIDE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181845" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 5 claws to install the guide.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0014" proc-id="RM22W0E___0000G0S00001">
<ptxt>INSTALL REAR LOWER SEATBACK LOCK BEZEL</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181844" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws and clip to install the seatback lock bezel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0015" proc-id="RM22W0E___0000G0T00001">
<ptxt>INSTALL REAR NO. 2 SEATBACK CUSHION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181843" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seatback cushion with the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0017" proc-id="RM22W0E___0000G0V00001">
<ptxt>INSTALL NO. 3 FOLD SEAT LOCK CONTROL CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 clamps to install the 2 cables.</ptxt>
<figure>
<graphic graphicname="B235502" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 2 cable clamps to connect the 2 cables.</ptxt>
<figure>
<graphic graphicname="B181838" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0018" proc-id="RM22W0E___0000G0W00001">
<ptxt>INSTALL NO. 2 SEAT STAY SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181839" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seat stay with the bolt.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0019" proc-id="RM22W0E___0000G0X00001">
<ptxt>INSTALL NO. 3 SEAT CUSHION COVER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Bottle Holder:</ptxt>
<figure>
<graphic graphicname="B263633E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Attach the fastening tape to install the seatback cover.</ptxt>
</s3>
<s3>
<ptxt>Using hog ring pliers, install the seat cushion cover to the seat cushion pad with new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
<s2>
<ptxt>w/ Bottle Holder:</ptxt>
<figure>
<graphic graphicname="B263634E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<s3>
<ptxt>Attach the 3 fastening tapes to install the seatback cover.</ptxt>
</s3>
<s3>
<ptxt>Using hog ring pliers, install the seat cushion cover to the seat cushion pad with new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0020">
<ptxt>INSTALL NO. 3 SEAT CUSHION PAD LH</ptxt>
</s-1>
<s-1 id="RM00000391U00YX_01_0021">
<ptxt>INSTALL SEAT CUSHION COVER WITH PAD</ptxt>
</s-1>
<s-1 id="RM00000391U00YX_01_0044" proc-id="RM22W0E___0000G1I00001">
<ptxt>INSTALL NO. 2 BOTTLE HOLDER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 bottle holder with the 2 screws.</ptxt>
<figure>
<graphic graphicname="B235484" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0043" proc-id="RM22W0E___0000G1H00001">
<ptxt>INSTALL CAP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws to install the 2 caps.</ptxt>
<figure>
<graphic graphicname="B235506" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0016" proc-id="RM22W0E___0000G0U00001">
<ptxt>INSTALL REAR SEAT CUSHION CARPET</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181840" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seat cushion carpet with new tack pins.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0039" proc-id="RM22W0E___0000G1E00001">
<ptxt>INSTALL REAR NO. 2 SEAT INNER BELT SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 belts.</ptxt>
<figure>
<graphic graphicname="B184524E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure the claws are facing away from the seat.</ptxt>
</item>
<item>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the seat.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000391U00YX_01_0040" proc-id="RM22W0E___0000G1F00001">
<ptxt>INSTALL REAR CENTER SEAT INNER BELT LH (w/ Rear Center Seat Headrest)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180632E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the belt with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the seat.</ptxt>
</atten3>
<atten4>
<ptxt>The rear No. 2 seat inner belt is fixed with the same nut as the rear seat inner belt. Therefore, connect it when installing the rear seat inner belt.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000391U00YX_01_0041" proc-id="RM22W0E___0000G1G00001">
<ptxt>INSTALL REAR CENTER SEAT INNER BELT RH (w/ Rear Center Seat Headrest)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180634E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the belt with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the seat.</ptxt>
</atten3>
<atten4>
<ptxt>The rear No. 2 seat inner belt is fixed with the same nut as the rear seat outer belt. Therefore, connect it when installing the rear seat outer belt.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000391U00YX_01_0023" proc-id="RM22W0E___0000G0Y00001">
<ptxt>INSTALL SEAT CUSHION LOCK RELEASE LEVER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the cable clamp to connect the cable.</ptxt>
<figure>
<graphic graphicname="B235501" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the claw to install the release lever.</ptxt>
<figure>
<graphic graphicname="B235509" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0024" proc-id="RM22W0E___0000G0Z00001">
<ptxt>INSTALL SEAT STAND FRAME CAP</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184049" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the stand frame cap.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0025" proc-id="RM22W0E___0000G1000001">
<ptxt>INSTALL REAR SEAT LOCK COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181831" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 clamps to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0026" proc-id="RM22W0E___0000G1100001">
<ptxt>INSTALL NO. 3 SEAT LEG COVER NO. 2</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181830" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0027" proc-id="RM22W0E___0000G1200001">
<ptxt>INSTALL NO. 3 SEAT FRONT BRACKET COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181829" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0028" proc-id="RM22W0E___0000G1300001">
<ptxt>INSTALL REAR SEAT CUSHION UNDER COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the cover with the 7 screws.</ptxt>
<figure>
<graphic graphicname="B258453" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 16 claws.</ptxt>
<figure>
<graphic graphicname="B181827" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0029" proc-id="RM22W0E___0000G1400001">
<ptxt>INSTALL 3RD SEAT CUSHION LOCK COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181826" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the 2 covers.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0030" proc-id="RM22W0E___0000G1500001">
<ptxt>INSTALL 3RD SEAT LINK SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181825" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seat link with the bolt and nut.</ptxt>
<torque>
<torqueitem>
<t-value1>8.5</t-value1>
<t-value2>87</t-value2>
<t-value3>75</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0031" proc-id="RM22W0E___0000G1600001">
<ptxt>INSTALL NO. 2 SEAT LEG COVER NO. 2</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182643" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the seat leg cover with the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0032" proc-id="RM22W0E___0000G1700001">
<ptxt>INSTALL REAR NO. 3 SEAT LEG COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seat leg cover with the 2 screws.</ptxt>
<figure>
<graphic graphicname="B181823" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the spring of the seat stay ball-joint.</ptxt>
<figure>
<graphic graphicname="B184055" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the seat stay to the seat frame.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0038" proc-id="RM22W0E___0000G1D00001">
<ptxt>INSTALL REAR SEAT REAR BRACKET COVER REAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0033" proc-id="RM22W0E___0000G1800001">
<ptxt>INSTALL RECLINING ADJUSTER COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181821E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 5 claws in the order shown in the illustration to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0034" proc-id="RM22W0E___0000G1900001">
<ptxt>INSTALL RECLINING ADJUSTER COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181820E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws in the order shown in the illustration to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0035" proc-id="RM22W0E___0000G1A00001">
<ptxt>INSTALL NO. 2 RECLINING ADJUSTER RELEASE HANDLE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181819" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the release handle with the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391U00YX_01_0036" proc-id="RM22W0E___0000G1B00001">
<ptxt>INSTALL SEAT ADJUSTER BOLT COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184050" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the cover.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>