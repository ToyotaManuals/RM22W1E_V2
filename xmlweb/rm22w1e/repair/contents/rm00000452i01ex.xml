<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM00000452I01EX" category="C" type-id="804RG" name-id="BCGHM-01" from="201308">
<dtccode>C1453</dtccode>
<dtcname>Reservoir Level Switch Disconnected</dtcname>
<dtccode>C1454</dtccode>
<dtcname>Reservoir Level Low</dtcname>
<subpara id="RM00000452I01EX_01" type-id="60" category="03" proc-id="RM22W0E___0000ALF00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The brake fluid level warning switch (brake master cylinder reservoir sub-assembly) sends the appropriate signal to the skid control ECU (master cylinder solenoid) when the brake fluid level drops.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.14in"/>
<colspec colname="COL2" colwidth="3.58in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1453</ptxt>
</entry>
<entry valign="middle">
<ptxt>With the ECU terminal IG1 voltage at 10 to 14 V, an open in the brake fluid level warning switch circuit continues for 2 seconds or more.</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Brake fluid level</ptxt>
</item>
<item>
<ptxt>Brake fluid level warning switch (Brake master cylinder reservoir sub-assembly)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Skid control ECU (Master cylinder solenoid)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1454</ptxt>
</entry>
<entry valign="middle">
<ptxt>The fluid level of the reservoir is below the LOW level for 40 seconds after the ignition switch is turned to ON, or for 7 seconds during pump motor operation.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000452I01EX_02" type-id="32" category="03" proc-id="RM22W0E___0000ALG00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C187573E11" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000452I01EX_03" type-id="51" category="05" proc-id="RM22W0E___0000ALH00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000452I01EX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000452I01EX_04_0001" proc-id="RM22W0E___0000ALI00001">
<testtitle>CHECK BRAKE FLUID LEVEL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal 40 times or more (until the pedal reaction feels light and pedal stroke becomes longer).</ptxt>
</test1>
<test1>
<ptxt>Check the amount of fluid in the brake reservoir.</ptxt>
<atten4>
<ptxt>When the ignition switch is turned to ON, brake fluid is sent to the accumulator and the fluid level decreases by approximately 5 mm (0.197 in.) from the level when the ignition switch is off (normal).</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Brake fluid level is normal.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000452I01EX_04_0002" fin="false">OK</down>
<right ref="RM00000452I01EX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000452I01EX_04_0002" proc-id="RM22W0E___0000ALJ00001">
<testtitle>INSPECT BRAKE FLUID LEVEL WARNING SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A26 brake fluid level warning switch (brake master cylinder reservoir sub-assembly) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C217215E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>A float is placed inside the reservoir. Its position can be changed by increasing/decreasing the level of brake fluid.</ptxt>
</atten4>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 (GND) - 2 (LBL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Float UP (Switch off)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.9 to 2.1 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Float DOWN (Switch on)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Brake Fluid Level Warning Switch [Brake Master Cylinder Reservoir Sub-assembly])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If there is no problem after finishing the above check, adjust the brake fluid level to the MAX level.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000452I01EX_04_0003" fin="false">A</down>
<right ref="RM00000452I01EX_04_0006" fin="true">B</right>
<right ref="RM00000452I01EX_04_0013" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000452I01EX_04_0003" proc-id="RM22W0E___0000ALK00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU - BRAKE FLUID LEVEL WARNING SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A26 brake fluid level warning switch (brake master cylinder reservoir sub-assembly) connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.88in"/>
<colspec colname="COL2" colwidth="1.85in"/>
<colspec colname="COL3" colwidth="2.35in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A24-41 (LBL) - A26-2 (LBL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A24-41 (LBL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>A26-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000452I01EX_04_0004" fin="false">OK</down>
<right ref="RM00000452I01EX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000452I01EX_04_0004" proc-id="RM22W0E___0000ALL00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and idle it for approximately 40 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00UX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="4.03in"/>
<colspec colname="COL2" colwidth="3.05in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000452I01EX_04_0008" fin="true">A</down>
<right ref="RM00000452I01EX_04_0009" fin="true">B</right>
<right ref="RM00000452I01EX_04_0012" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000452I01EX_04_0005">
<testtitle>CHECK AND REPAIR BRAKE FLUID LEAK</testtitle>
</testgrp>
<testgrp id="RM00000452I01EX_04_0006">
<testtitle>REPLACE BRAKE MASTER CYLINDER RESERVOIR SUB-ASSEMBLY<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000452I01EX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000452I01EX_04_0008">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000452I01EX_04_0009">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000452I01EX_04_0012">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000452I01EX_04_0013">
<testtitle>REPLACE BRAKE MASTER CYLINDER RESERVOIR SUB-ASSEMBLY<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>