<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T9_T00MC" variety="T00MC">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000000XUI0FIX" category="J" type-id="800Q4" name-id="TD636-02" from="201301" to="201308">
<dtccode/>
<dtcname>Rear Door LH Entry Lock and Unlock Functions do not Operate</dtcname>
<subpara id="RM000000XUI0FIX_01" type-id="60" category="03" proc-id="RM22W0E___0000EOT00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the rear door LH entry lock and unlock functions do not operate, one of the following may be malfunctioning: 1) power door lock system, 2) rear door electrical key oscillator LH, or 3) certification ECU (smart key ECU assembly).</ptxt>
<atten4>
<ptxt>For vehicles with entry function for rear doors.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XUI0FIX_02" type-id="32" category="03" proc-id="RM22W0E___0000EOU00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B296362E07" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XUI0FIX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000XUI0FIX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XUI0FIX_05_0020" proc-id="RM22W0E___0000EOV00000">
<testtitle>CHECK POWER DOOR LOCK OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When the master switch assembly door control switch is operated, check that the locked doors unlock.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Door locks operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0050" fin="false">OK</down>
<right ref="RM000000XUI0FIX_05_0034" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0050" proc-id="RM22W0E___0000EP400000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR LOCK POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Main Body / Data List.</ptxt>
</test1>
<test1>
<ptxt>Read the Data List according to the display on the intelligent tester.</ptxt>
<test2>
<ptxt>for LHD</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DR Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side rear door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side rear door unlocked</ptxt>
<ptxt>OFF: Driver side rear door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between ON and OFF as shown in the chart above.</ptxt>
</specitem>
</spec>
</test2>
<test2>
<ptxt>for RHD</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PR Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Passenger side rear door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Passenger side rear door unlocked</ptxt>
<ptxt>OFF: Passenger side rear door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between ON and OFF as shown in the chart above.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0021" fin="false">OK</down>
<right ref="RM000000XUI0FIX_05_0051" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0021" proc-id="RM22W0E___0000EOW00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (TOUCH SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List.</ptxt>
<test2>
<ptxt>for LHD</ptxt>
<table pgwide="1">
<title>Entry &amp; Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Dr-Door Touch Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side rear door touch sensor / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Touch sensor touched</ptxt>
<ptxt>OFF: Touch sensor not touched</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Dr-Door Trigger Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side rear door outside handle lock switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Entry lock switch pushed</ptxt>
<ptxt>OFF: Entry lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
<test2>
<ptxt>for RHD</ptxt>
<table pgwide="1">
<title>Entry &amp; Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Pr-Door Touch Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Passenger side rear door touch sensor / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Touch sensor touched</ptxt>
<ptxt>OFF: Touch sensor not touched</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pr-Door Trigger Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Passenger side rear door outside handle lock switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Entry lock switch pushed</ptxt>
<ptxt>OFF: Entry lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Display changes according to manual operation</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Display does not change according to manual operation</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0026" fin="false">A</down>
<right ref="RM000000XUI0FIX_05_0028" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0026" proc-id="RM22W0E___0000EOX00000">
<testtitle>CHECK WAVE ENVIRONMENT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bring the electrical key transmitter near the rear door outside handle LH, and perform a rear door LH entry lock and unlock operation check.</ptxt>
<figure>
<graphic graphicname="B185389E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>If the key is brought within 0.2 m (0.656 ft.) of the door handle, communication is not possible.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the electrical key transmitter is brought near the rear door outside handle LH, the possibility of wave interference decreases, and it can be determined if wave interference is causing the problem symptom.</ptxt>
</item>
<item>
<ptxt>If the operation check is normal, the possibility of wave interference is high. Also, added vehicle components may cause wave interference. If installed, remove them and perform the operation check.</ptxt>
</item>
</list1>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Operation check fails</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Operation check is normal</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0027" fin="false">A</down>
<right ref="RM000000XUI0FIX_05_0039" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0027" proc-id="RM22W0E___0000EOY00000">
<testtitle>PERFORM KEY DIAGNOSTIC MODE INSPECTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Diagnostic mode inspection (rear door electrical key oscillator LH).</ptxt>
<figure>
<graphic graphicname="B185387E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test2>
<test2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test2>
<test2>
<ptxt>Turn the intelligent tester on.</ptxt>
</test2>
<test2>
<ptxt>Enter the following menus:</ptxt>
<ptxt>for LHD</ptxt>
<ptxt>Select: Body / Entry &amp; Start / Key Communication Check / Overhead + Driver Side Rear.</ptxt>
<ptxt>for RHD</ptxt>
<ptxt>Select: Body / Entry &amp; Start / Key Communication Check / Overhead + Passenger Side Rear.</ptxt>
</test2>
<test2>
<ptxt>When the electrical key transmitter is in the position shown in the illustration, check that the wireless door lock buzzer sounds.</ptxt>
<atten4>
<ptxt>If the buzzer sounds, it can be determined that the rear LH seat outside transmitter is operating normally.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Wireless door lock buzzer sounds.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0040" fin="true">OK</down>
<right ref="RM000000XUI0FIX_05_0028" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0028" proc-id="RM22W0E___0000EOZ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU [SMART KEY ECU ASSEMBLY] - DOOR ELECTRICAL KEY OSCILLATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E30 ECU connector.</ptxt>
<figure>
<graphic graphicname="B187936E11" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the z28 oscillator connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E30-1 (SEL3) - z28-4 (SEL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-8 (CLG3) - z28-10 (CLG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-9 (CG3B) - z28-5 (CLGB)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-1 (SEL3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-8 (CLG3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-9 (CG3B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E30-2 (SEL4) - z28-4 (SEL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-10 (CLG4) - z28-10 (CLG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-11 (CG4B) - z28-5 (CLGB)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-2 (SEL4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-10 (CLG4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E30-11 (CG4B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0029" fin="false">OK</down>
<right ref="RM000000XUI0FIX_05_0036" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0029" proc-id="RM22W0E___0000EP000000">
<testtitle>CHECK HARNESS AND CONNECTOR (DOOR ELECTRICAL KEY OSCILLATOR - REAR DOOR OUTSIDE HANDLE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the z27 handle connector.</ptxt>
<figure>
<graphic graphicname="B164339E46" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the z28 oscillator connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z27-2 (ANT2) - z28-8 (ANT2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z27-5 (ANT1) - z28-3 (ANT1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z27-2 (ANT2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z27-5 (ANT1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0030" fin="false">OK</down>
<right ref="RM000000XUI0FIX_05_0037" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0030" proc-id="RM22W0E___0000EP100000">
<testtitle>CHECK HARNESS AND CONNECTOR (DOOR ELECTRICAL KEY OSCILLATOR - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the z28 oscillator connector.</ptxt>
<figure>
<graphic graphicname="B164336E17" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z28-1 (B) - z28-7 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0049" fin="false">OK</down>
<right ref="RM000000XUI0FIX_05_0038" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0049" proc-id="RM22W0E___0000EP300000">
<testtitle>REPLACE DOOR ELECTRICAL KEY OSCILLATOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the door electrical key oscillator (See page <xref label="Seep01" href="RM000002M8105PX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0031" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0031" proc-id="RM22W0E___0000EP200000">
<testtitle>CHECK DOOR ELECTRICAL KEY OSCILLATOR (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the entry function operates normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0044" fin="true">OK</down>
<right ref="RM000000XUI0FIX_05_0052" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0034">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM<xref label="Seep01" href="RM000002T6K05PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0036">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0037">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0038">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0039">
<testtitle>AFFECTED BY WAVE INTERFERENCE</testtitle>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0040">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0044">
<testtitle>END (DOOR ELECTRICAL KEY OSCILLATOR IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0048">
<testtitle>END (REAR DOOR OUTSIDE HANDLE IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0051">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM (Proceed to Only Rear Door LH LOCK/UNLOCK Functions do not Operate)<xref label="Seep01" href="RM000000TNU06PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0052" proc-id="RM22W0E___0000EP500000">
<testtitle>REPAIR REAR DOOR OUTSIDE HANDLE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the rear door outside handle (See page <xref label="Seep01" href="RM000002M8105PX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0036" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XUI0FIX_05_0053" proc-id="RM22W0E___0000EP600000">
<testtitle>CHECK REAR DOOR OUTSIDE HANDLE OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the entry function operates normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0FIX_05_0048" fin="true">OK</down>
<right ref="RM000000XUI0FIX_05_0040" fin="true">NG</right>
</res>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>