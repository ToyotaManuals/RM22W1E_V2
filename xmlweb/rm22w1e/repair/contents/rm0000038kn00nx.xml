<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C402_T00T5" variety="T00T5">
<name>REAR DOOR BELT MOULDING</name>
<para id="RM0000038KN00NX" category="A" type-id="30014" name-id="ET8DW-01" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM0000038KN00NX_01" type-id="11" category="10" proc-id="RM22W0E___0000JM300000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000038KN00NX_02" type-id="01" category="01">
<s-1 id="RM0000038KN00NX_02_0001" proc-id="RM22W0E___0000IG300000">
<ptxt>INSTALL REAR DOOR BELT MOULDING ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182605" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the belt moulding.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038KN00NX_02_0014" proc-id="RM22W0E___0000I1D00000">
<ptxt>INSTALL REAR DOOR GLASS SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Slide the rear door glass sub-assembly LH as shown in the illustration to install it.</ptxt>
<figure>
<graphic graphicname="B180922" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KN00NX_02_0015" proc-id="RM22W0E___0000I1E00000">
<ptxt>INSTALL REAR DOOR QUARTER WINDOW GLASS LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear door quarter window glass LH together with the rear door quarter window weatherstrip LH in the direction indicated by the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="B183926" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KN00NX_02_0016" proc-id="RM22W0E___0000I1J00000">
<ptxt>INSTALL REAR DOOR REAR LOWER WINDOW FRAME SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear door rear lower window frame sub-assembly LH with the 2 bolts and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KN00NX_02_0017" proc-id="RM22W0E___0000I1F00000">
<ptxt>INSTALL REAR DOOR GLASS RUN LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear door glass run LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KN00NX_02_0018" proc-id="RM22W0E___0000E5900000">
<ptxt>INSTALL REAR DOOR SERVICE HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply butyl tape to the door.</ptxt>
</s2>
<s2>
<ptxt>Pass the rear door lock remote control cable assembly LH and rear door inside locking cable assembly LH through a new rear door service hole cover LH.</ptxt>
<figure>
<graphic graphicname="B180918" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When installing the rear door service hole cover LH, pull the links and connectors through the rear door service hole cover LH.</ptxt>
</item>
<item>
<ptxt>There should be no wrinkles or folds after attaching the rear door service hole cover LH.</ptxt>
</item>
<item>
<ptxt>After attaching the rear door service hole cover LH, check the sealing quality.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Connect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 6 clamps.</ptxt>
</s2>
<s2>
<ptxt>Install the bolt as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B180916E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>8.4</t-value1>
<t-value2>86</t-value2>
<t-value3>74</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000038KN00NX_02_0019" proc-id="RM22W0E___0000I1L00000">
<ptxt>INSTALL REAR INNER DOOR GLASS WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear inner door glass weatherstrip LH to the door panel.</ptxt>
<figure>
<graphic graphicname="B180917E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KN00NX_02_0020" proc-id="RM22W0E___0000BPJ00000">
<ptxt>INSTALL REAR DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the rear door lock remote control cable assembly LH and rear door inside locking cable assembly LH to the rear door inside handle sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B183925" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 4 claws and 9 clips to install the rear door trim board sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180912" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KN00NX_02_0021" proc-id="RM22W0E___0000BPN00000">
<ptxt>INSTALL ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 9 claws to install the assist grip cover LH to the rear door trim board sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KN00NX_02_0022" proc-id="RM22W0E___0000BPK00000">
<ptxt>INSTALL REAR DOOR ARMREST BASE PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector. </ptxt>
</s2>
<s2>
<ptxt>Attach the 7 claws to install the armrest base panel.</ptxt>
<figure>
<graphic graphicname="B183923" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KN00NX_02_0023" proc-id="RM22W0E___0000BPL00000">
<ptxt>INSTALL REAR DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt> Attach the 4 claws to install the rear door inside handle bezel LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KN00NX_02_0013" proc-id="RM22W0E___0000JM400000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>