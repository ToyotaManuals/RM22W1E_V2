<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LY_T00F1" variety="T00F1">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM000003FEI00GX" category="C" type-id="305G7" name-id="CC41U-02" from="201308">
<dtccode>P1615</dtccode>
<dtcname>Communication Error from Distance Control ECU to ECM</dtcname>
<subpara id="RM000003FEI00GX_01" type-id="60" category="03" proc-id="RM22W0E___00007BT00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM sends the cruise control main switch signal, vehicle speed signal, etc. to the distance control ECU. The distance control ECU sends the acceleration signal, deceleration request signal, etc. to the ECM. The ECM controls cruise control according to these signals.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P1615</ptxt>
</entry>
<entry valign="middle">
<ptxt>The vehicle speed is 50 km/h (30 mph) or more with the cruise control main switch on (vehicle-to-vehicle distance control mode) and the data sent from the distance control ECU to the ECM is abnormal.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Distance control ECU</ptxt>
</item>
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003FEI00GX_02" type-id="32" category="03" proc-id="RM22W0E___00007BU00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E149011E04" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003FEI00GX_03" type-id="51" category="05" proc-id="RM22W0E___00007BV00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When the distance control ECU is replaced with a new one, initialization must be performed (See page <xref label="Seep01" href="RM000003FY200AX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003FEI00GX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003FEI00GX_04_0001" proc-id="RM22W0E___00007BW00001">
<testtitle>REPLACE DISTANCE CONTROL ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the distance control ECU with a new one (See page <xref label="Seep01" href="RM0000038IO00JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform initialization (See page <xref label="Seep02" href="RM000003FY200AX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003FEI00GX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003FEI00GX_04_0002" proc-id="RM22W0E___00007BX00001">
<testtitle>CHECK DTC (DYNAMIC RADAR CRUISE CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Powertrain / Radar Cruise / Trouble Code.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002R6B01RX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the following to make sure the DTC detection conditions are met.</ptxt>
<atten4>
<ptxt>If the detection conditions are not met, the system cannot detect the malfunction.</ptxt>
</atten4>
<test2>
<ptxt>Drive the vehicle at a speed of 50 km/h (30 mph) or more.</ptxt>
</test2>
<test2>
<ptxt>Turn the cruise control main switch on to set the vehicle-to-vehicle distance control mode to the standby state.</ptxt>
</test2>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000002R6B01RX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC P1615 is not output.</ptxt>
</specitem>
</spec>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 1UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 3UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000003FEI00GX_04_0003" fin="true">A</down>
<right ref="RM000003FEI00GX_04_0004" fin="true">B</right>
<right ref="RM000003FEI00GX_04_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003FEI00GX_04_0003">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000003FEI00GX_04_0004">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292038X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FEI00GX_04_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000003292039X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>