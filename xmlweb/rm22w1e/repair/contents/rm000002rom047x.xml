<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WO_T00PR" variety="T00PR">
<name>AIR CONDITIONING SYSTEM (for Manual Air Conditioning System)</name>
<para id="RM000002ROM047X" category="C" type-id="305D2" name-id="ACILM-01" from="201301" to="201308">
<dtccode>B1443</dtccode>
<dtcname>Air Outlet Damper Control Servo Motor Circuit</dtcname>
<subpara id="RM000002ROM047X_01" type-id="60" category="03" proc-id="RM22W0E___0000HCC00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo) sends pulse signals to inform the air conditioning amplifier assembly of the damper position. The air conditioning amplifier assembly activates the motor (normal or reverse) based on the signals to move the air outlet damper to any position, which controls the air outlet changes.</ptxt>
<atten4>
<ptxt>Confirm that no mechanical problem is present because this trouble code may be stored when either a damper link or damper is mechanically locked.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1443</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The air outlet damper position does not change for 30 seconds or more when the air conditioning amplifier assembly operates the No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo)</ptxt>
</item>
<item>
<ptxt>Air conditioning harness assembly</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002ROM047X_02" type-id="32" category="03" proc-id="RM22W0E___0000HCD00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E180810E77" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002ROM047X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002ROM047X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002ROM047X_04_0001" proc-id="RM22W0E___0000HCE00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (AIR OUTLET SERVO PULSE [D])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo) is functioning properly (See page <xref label="Seep01" href="RM000002LIV02PX"/>).</ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Air Outlet Servo Pulse(D)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo) target pulse / Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>FACE: 117 (pulse)</ptxt>
</item>
<item>
<ptxt>B/L: 93 (pulse)</ptxt>
</item>
<item>
<ptxt>FOOT: 41 (pulse)</ptxt>
</item>
<item>
<ptxt>F/D: 18 (pulse)</ptxt>
</item>
<item>
<ptxt>DEF: 6 (pulse)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition column.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting according to the DTC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting according to problem symptoms table)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002ROM047X_04_0002" fin="false">A</down>
<right ref="RM000002ROM047X_04_0017" fin="true">B</right>
<right ref="RM000002ROM047X_04_0015" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002ROM047X_04_0002" proc-id="RM22W0E___0000HCF00000">
<testtitle>REPLACE NO. 1 AIR CONDITIONING RADIATOR DAMPER SERVO SUB-ASSEMBLY (MODE DAMPER SERVO)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo).</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: See page <xref label="Seep01" href="RM000003AXS02WX"/>.</ptxt>
</item>
<item>
<ptxt>for RHD: See page <xref label="Seep02" href="RM000003AXS02VX"/>.</ptxt>
</item>
</list1>
<atten4>
<ptxt>Since the No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo) cannot be inspected while it is removed from the vehicle, replace the No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo) with a new or normally functioning one.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep03" href="RM000002LIT038X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep04" href="RM000002LIT038X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1443 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1443 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002ROM047X_04_0008" fin="true">A</down>
<right ref="RM000002ROM047X_04_0013" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002ROM047X_04_0013" proc-id="RM22W0E___0000HCH00000">
<testtitle>REPLACE AIR CONDITIONING HARNESS ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the air conditioning harness assembly with a new or normally functioning one.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: See page <xref label="Seep01" href="RM000003AXS02WX"/>.</ptxt>
</item>
<item>
<ptxt>for RHD: See page <xref label="Seep04" href="RM000003AXS02VX"/>.</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000002LIT038X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep03" href="RM000002LIT038X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1443 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1443 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002ROM047X_04_0016" fin="true">A</down>
<right ref="RM000002ROM047X_04_0015" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002ROM047X_04_0015">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ROM047X_04_0008">
<testtitle>END (NO. 1 AIR CONDITIONING RADIATOR DAMPER SERVO SUB-ASSEMBLY [MODE DAMPER SERVO] IS FAULTY)</testtitle>
</testgrp>
<testgrp id="RM000002ROM047X_04_0016">
<testtitle>END (AIR CONDITIONING HARNESS ASSEMBLY IS FAULTY)</testtitle>
</testgrp>
<testgrp id="RM000002ROM047X_04_0017">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002LIQ023X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>