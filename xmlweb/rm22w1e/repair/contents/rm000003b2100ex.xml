<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C40D_T00TG" variety="T00TG">
<name>GRAPHIC DECAL</name>
<para id="RM000003B2100EX" category="A" type-id="30014" name-id="ET9Y7-01" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000003B2100EX_01" type-id="11" category="10" proc-id="RM22W0E___0000JOV00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003B2100EX_02" type-id="01" category="01">
<s-1 id="RM000003B2100EX_02_0001" proc-id="RM22W0E___0000JOW00001">
<ptxt>REPAIR INSTRUCTION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the vehicle body surface</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body surface.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any stripe adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Installation temperature</ptxt>
<s3>
<ptxt>When the ambient temperature is below 15°C (59°F), perform the installation procedure after warming the vehicle body surface (installation surface of the door frame) and stripe to between 20 and 30°C (68 and 86°F) using a heat light. When the ambient temperature is higher than 35°C (95°F), cool the vehicle body surface (installation surface of the door frame) and stripe to between 20 and 30°C (68 and 86°F) prior to installation.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The most appropriate temperature for installing the stripe is 25°C (77°F).</ptxt>
</item>
<item>
<ptxt>When the temperature is low, the stripe turns stiff and falls off easily. When the temperature is high, the stripe loses elasticity.</ptxt>
</item>
</list1>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Before installation</ptxt>
<s3>
<ptxt>Make sure any dirt on and around the vehicle body surface where the stripe will be installed (installation surface of the door frame) is removed, and that the surface is smooth. If the surface is rough or dirt remains when pressing the stripe onto the surface, air will be trapped under the stripe and result in a poor appearance.</ptxt>
<atten4>
<ptxt>Spray water on the shop floor to settle any dust.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Key points for handling the stripe</ptxt>
<s3>
<ptxt>The stripe bends and rolls up easily. Store the stripe between flat pieces of cardboard or other similar objects and keep it dry and level.</ptxt>
<atten3>
<ptxt>Do not bend the stripe or leave it in a place with a high temperature.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Key points for the installation of the stripe (how to use a squeegee and the installation procedure for a flat surface).</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Position the stripe with a high level of accuracy to achieve a neat finish and to avoid peeling.</ptxt>
</item>
<item>
<ptxt>The stripe cannot be reused because it deforms and will not fit after removal.</ptxt>
</item>
</list1>
</atten3>
<s3>
<ptxt>To avoid air bubbles, slightly raise the part of the stripe that is going to be applied so that its adhesive surface does not touch the vehicle body while applying the stripe. Tilt the squeegee 40 to 50° (for pressing forward) or 30 to 45° (for pulling) from the vehicle body surface and press with a force of 20 to 30 N (2 to 3 kgf) while moving the squeegee at a constant slow speed of 3 to 7 cm (1.2 to 2.8 in.) per second.</ptxt>
<atten3>
<ptxt>Be sure to observe the specified pressing speed, force and angle of the squeegee to avoid wrinkles and air bubbles.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Either angle of the squeegee (for pressing forward or for pulling) is acceptable.</ptxt>
</item>
<item>
<ptxt>Be sure to apply the stripe while removing the release paper 10 to 20 mm (0.393 to 0.787 in.) from the edge of the squeegee.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="B356300E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Squeegee</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stripe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Release Paper</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Application Sheet</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sectional View</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Non-padded Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Padded Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressing</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2100EX_02_0006" proc-id="RM22W0E___0000JP100001">
<ptxt>INSTALL FENDER STRIPE LH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>When installing the fender stripe LH, heat the vehicle body and fender stripe LH using a heat light.</ptxt>
</atten4>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Body</ptxt>
</entry>
<entry valign="middle">
<ptxt>40 to 60°C (104 to 140°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fender Stripe LH</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not heat the vehicle body and fender stripe LH excessively.</ptxt>
</atten3>
<s2>
<ptxt>Clean the vehicle body surface.</ptxt>
<s3>
<ptxt>Using heat light, heat the vehicle body and fender stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the double-sided tape from the vehicle body.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any tape adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install a new fender stripe LH.</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body and a new fender stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the peeling paper from the face of the fender stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Align the perforated end of the fender stripe LH with the fender parting line and the other end of the fender stripe LH with the headlight parting line as shown in the illustration, and using a squeegee, apply the fender stripe LH.</ptxt>
<atten4>
<ptxt>For snorkel-equipped vehicles, there is no fender stripe RH.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
<figure>
<graphic graphicname="B355054E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fender Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight Parting Line</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Application Sheet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2100EX_02_0005" proc-id="RM22W0E___0000JP000001">
<ptxt>INSTALL FRONT DOOR STRIPE LH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>When installing the front door stripe LH, heat the vehicle body and front door stripe LH using a heat light.</ptxt>
</atten4>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Body</ptxt>
</entry>
<entry valign="middle">
<ptxt>40 to 60°C (104 to 140°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front Door Stripe LH</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not heat the vehicle body and front door stripe LH excessively.</ptxt>
</atten3>
<s2>
<ptxt>Clean the vehicle body surface.</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body and front door stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the double-sided tape from the vehicle body.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any tape adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install a new front door stripe LH.</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body and a new front door stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the peeling paper from the face of the front door stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Align the perforations of the front door stripe LH with the door parting lines as shown in the illustration, and using a squeegee, apply the front door stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
<figure>
<graphic graphicname="B355052E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Application Sheet</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2100EX_02_0004" proc-id="RM22W0E___0000JOZ00001">
<ptxt>INSTALL REAR DOOR OUTSIDE STRIPE LH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>When installing the rear door outside stripe LH, heat the vehicle body and rear door outside stripe LH using a heat light.</ptxt>
</atten4>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Body</ptxt>
</entry>
<entry valign="middle">
<ptxt>40 to 60°C (104 to 140°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear Door Outside Stripe LH</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not heat the vehicle body and rear door outside stripe LH excessively.</ptxt>
</atten3>
<s2>
<ptxt>Clean the vehicle body surface.</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body and rear door outside stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the double-sided tape from the vehicle body.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any tape adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install a new rear door outside stripe LH.</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body and a new rear door outside stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the peeling paper from the face of the rear door outside stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Align the perforations of the rear door outside stripe LH with the door parting lines as shown in the illustration, and using a squeegee, apply the rear door outside stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
<figure>
<graphic graphicname="B355050E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Application Sheet</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2100EX_02_0003" proc-id="RM22W0E___0000JOY00001">
<ptxt>INSTALL FRONT QUARTER STRIPE LH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>When installing the front quarter stripe LH, heat the vehicle body and front quarter stripe LH using a heat light.</ptxt>
</atten4>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Body</ptxt>
</entry>
<entry valign="middle">
<ptxt>40 to 60°C (104 to 140°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front Quarter Stripe LH</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not heat the vehicle body and front quarter stripe LH excessively.</ptxt>
</atten3>
<s2>
<ptxt>Clean the vehicle body surface.</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body and front quarter stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the double-sided tape from the vehicle body.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any tape adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install a new front quarter stripe LH.</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body and a new front quarter stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the peeling paper from the face of the front quarter stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Align the perforations of the front quarter stripe LH with the door parting line and body parting line as shown in the illustration, and using a squeegee, apply the front quarter stripe LH.</ptxt>
</s3>
<s3>
<ptxt>Remove the application sheet.</ptxt>
<figure>
<graphic graphicname="B355048E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Perforation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door Parting Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Body Parting Line</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Application Sheet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>