<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C401_T00T4" variety="T00T4">
<name>FRONT DOOR BELT MOULDING</name>
<para id="RM0000038KK02LX" category="A" type-id="30014" name-id="ET8DY-01" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM0000038KK02LX_02" type-id="11" category="10" proc-id="RM22W0E___0000JLX00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000038KK02LX_01" type-id="01" category="01">
<s-1 id="RM0000038KK02LX_01_0001" proc-id="RM22W0E___0000IEI00000">
<ptxt>INSTALL FRONT DOOR BELT MOULDING ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182604" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the belt moulding.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038KK02LX_01_0014" proc-id="RM22W0E___0000IEB00000">
<ptxt>INSTALL FRONT DOOR GLASS RUN LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door glass run LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK02LX_01_0015" proc-id="RM22W0E___0000I0Y00000">
<ptxt>INSTALL FRONT DOOR GLASS SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Insert the front door glass sub-assembly LH into the door panel along the glass run as indicated by the arrows in the illustration.</ptxt>
<figure>
<graphic graphicname="B183141" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to damage the glass.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the front door glass sub-assembly LH to the front door window regulator sub-assembly LH with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK02LX_01_0016" proc-id="RM22W0E___0000IEC00000">
<ptxt>INSTALL FRONT INNER DOOR GLASS WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front inner door glass weatherstrip LH.</ptxt>
<figure>
<graphic graphicname="B180828" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK02LX_01_0017" proc-id="RM22W0E___0000E4H00000">
<ptxt>INSTALL FRONT DOOR SERVICE HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply butyl tape to the door.</ptxt>
<figure>
<graphic graphicname="B180831" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Pass the front door lock remote control cable assembly LH and front door inside locking cable assembly LH through a new front door service hole cover LH.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When installing the front door service hole cover LH, pull the links and connectors through the front door service hole cover LH.</ptxt>
</item>
<item>
<ptxt>There should be no wrinkles or folds after attaching the front door service hole cover LH.</ptxt>
</item>
<item>
<ptxt>After attaching the front door service hole cover LH, check the sealing quality.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Connect the 2 connectors.</ptxt>
<figure>
<graphic graphicname="B186231E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 9 clamps.</ptxt>
</s2>
<s2>
<ptxt>Install the bolt as shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>8.4</t-value1>
<t-value2>86</t-value2>
<t-value3>74</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK02LX_01_0018" proc-id="RM22W0E___0000IEG00000">
<ptxt>INSTALL OUTER REAR VIEW MIRROR ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180573E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the claw and Install the mirror with the 3 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>w/ Power Mirror Control System:</ptxt>
<s3>
<ptxt>Attach the clamp.</ptxt>
</s3>
<s3>
<ptxt>Connect the connector labeled A.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK02LX_01_0019" proc-id="RM22W0E___0000BP000000">
<ptxt>INSTALL FRONT DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B183139" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the front door lock remote control cable assembly LH and front door inside locking cable assembly LH to the front door inside handle sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 claws and 13 clips to install the front door trim board sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180821" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK02LX_01_0020" proc-id="RM22W0E___0000BP400000">
<ptxt>INSTALL DOOR ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 claws to install the door assist grip cover LH to the front door trim board sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK02LX_01_0021" proc-id="RM22W0E___0000BP100000">
<ptxt>INSTALL FRONT DOOR ARMREST BASE PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector. </ptxt>
</s2>
<s2>
<ptxt>Attach the 5 claws to install the armrest base panel.</ptxt>
<figure>
<graphic graphicname="B183137" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK02LX_01_0022" proc-id="RM22W0E___0000BP200000">
<ptxt>INSTALL FRONT DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws to install the front door inside handle bezel LH.</ptxt>
<figure>
<graphic graphicname="B183136" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK02LX_01_0023" proc-id="RM22W0E___0000BP500000">
<ptxt>INSTALL FRONT LOWER DOOR FRAME BRACKET GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and claw, and install the front door lower frame bracket garnish LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK02LX_01_0013" proc-id="RM22W0E___0000JLW00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>