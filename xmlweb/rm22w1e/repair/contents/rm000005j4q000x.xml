<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002K" variety="S002K">
<name>WIPER / WASHER</name>
<ttl id="12068_S002K_7C3Y9_T00RC" variety="T00RC">
<name>WIPER AND WASHER SYSTEM (w/ Rain Sensor)</name>
<para id="RM000005J4Q000X" category="J" type-id="808YS" name-id="WW604-01" from="201301">
<dtccode/>
<dtcname>Washer Nozzle Heater Circuit</dtcname>
<subpara id="RM000005J4Q000X_01" type-id="60" category="03" proc-id="RM22W0E___0000JYD00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The No. 2 multiplex network body ECU receives ambient air temperature information from the air conditioning amplifier assembly via CAN communication.</ptxt>
<ptxt>The washer nozzle heater assembly controls the No. 2 multiplex network body ECU and operates the washer nozzle heater assembly according to the ambient temperature.</ptxt>
</content5>
</subpara>
<subpara id="RM000005J4Q000X_02" type-id="32" category="03" proc-id="RM22W0E___0000JYG00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E273238E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000005J4Q000X_03" type-id="51" category="05" proc-id="RM22W0E___0000JXU00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>Since the wiper and washer system has functions that use CAN communication, first confirm that there is no malfunction in the CAN communication system with the How to Proceed with Troubleshooting procedure (See page <xref label="Seep01" href="RM000002M5S01EX"/>).</ptxt>
</item>
<item>
<ptxt>Since the wiper and washer system has functions that use the air conditioning system, first confirm that there is no malfunction in the air conditioning system with the How to Proceed with Troubleshooting procedure (See page <xref label="Seep02" href="RM000002LIM05LX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000005J4Q000X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000005J4Q000X_04_0001" proc-id="RM22W0E___0000JXV00000">
<testtitle>CHECK NO. 2 MULTIPLEX NETWORK BODY ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, enter the following menus: Body / Body No. 4 / Active Test.</ptxt>
<figure>
<graphic graphicname="E176670E77" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Select "Front Washer Nozzle Heater", and perform the Active Test.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L50-25 (WHTR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Active Test is not performed</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L50-25 (WHTR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>Active Test is performed</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000005J4Q000X_04_0002" fin="false">OK</down>
<right ref="RM000005J4Q000X_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0002" proc-id="RM22W0E___0000JXW00000">
<testtitle>INSPECT WASHER NOZZLE HEATER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LH:</ptxt>
<test2>
<ptxt>Remove the washer nozzle heater assembly LH (See page <xref label="Seep01" href="RM000002M6D03DX"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the washer nozzle heater assembly LH (See page <xref label="Seep02" href="RM00000599Y001X"/>).</ptxt>
</test2>
</test1>
<test1>
<ptxt>for RH:</ptxt>
<test2>
<ptxt>Remove the washer nozzle heater assembly RH (See page <xref label="Seep03" href="RM000002M6D03DX"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the washer nozzle heater assembly RH (See page <xref label="Seep04" href="RM00000599Y001X"/>).</ptxt>
</test2>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for LH)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for RH)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000005J4Q000X_04_0003" fin="false">A</down>
<right ref="RM000005J4Q000X_04_0007" fin="true">B</right>
<right ref="RM000005J4Q000X_04_0012" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0003" proc-id="RM22W0E___0000JXX00000">
<testtitle>CHECK HARNESS AND CONNECTOR (WASHER NOZZLE HEATER ASSEMBLY - COWL SIDE JUNCTION BLOCK RH AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the z70 washer nozzle heater assembly LH connector.</ptxt>
<figure>
<graphic graphicname="E273239E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the z69 washer nozzle heater assembly RH connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the 3C cowl side junction block RH connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z70-2 (L+) - 3C-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z69-2 (R+) - 3C-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z70-1 (L-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z69-1 (R-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z70-2 (L+) or 3C-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z69-2 (R+) or 3C-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000005J4Q000X_04_0004" fin="false">OK</down>
<right ref="RM000005J4Q000X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0004" proc-id="RM22W0E___0000JXY00000">
<testtitle>CHECK HARNESS AND CONNECTOR (COWL SIDE JUNCTION BLOCK RH - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the 3B cowl side junction block RH connector.</ptxt>
<figure>
<graphic graphicname="E273240E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3B-5 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3B-5 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000005J4Q000X_04_0015" fin="false">OK</down>
<right ref="RM000005J4Q000X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0015" proc-id="RM22W0E___0000JYJ00000">
<testtitle>CHECK COWL SIDE JUNCTION BLOCK RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the cowl side junction block RH.</ptxt>
<figure>
<graphic graphicname="E273474E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3B-5 - 3C-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery voltage applied between terminals 3C-16 and 3B-11</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3B-5 - 3C-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery voltage not applied between terminals 3C-16 and 3B-11</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK (w/ Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK (w/o Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000005J4Q000X_04_0011" fin="true">A</down>
<right ref="RM000005J4Q000X_04_0013" fin="true">B</right>
<right ref="RM000005J4Q000X_04_0009" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0005" proc-id="RM22W0E___0000JXZ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 2 MULTIPLEX NETWORK BODY ECU - COWL SIDE JUNCTION BLOCK RH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the L50 No. 2 multiplex network body ECU connector.</ptxt>
<figure>
<graphic graphicname="E273242E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the 3B cowl side junction block RH connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L50-25 (WHTR) - 3B-11</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>L50-25 (WHTR) or 3B-11 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000005J4Q000X_04_0006" fin="false">OK</down>
<right ref="RM000005J4Q000X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0006" proc-id="RM22W0E___0000JY000000">
<testtitle>CHECK HARNESS AND CONNECTOR (COWL SIDE JUNCTION BLOCK RH - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the 3C cowl side junction block RH connector.</ptxt>
<figure>
<graphic graphicname="E273241E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3C-16 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3C-16 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000005J4Q000X_04_0014" fin="false">OK</down>
<right ref="RM000005J4Q000X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0014" proc-id="RM22W0E___0000JYI00000">
<testtitle>INSPECT COWL SIDE JUNCTION BLOCK RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the cowl side junction block RH.</ptxt>
<figure>
<graphic graphicname="E273474E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3B-5 - 3C-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery voltage applied between terminals 3C-16 and 3B-11</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3B-5 - 3C-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery voltage not applied between terminals 3C-16 and 3B-11</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK (w/ Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK (w/o Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000005J4Q000X_04_0011" fin="true">A</down>
<right ref="RM000005J4Q000X_04_0013" fin="true">B</right>
<right ref="RM000005J4Q000X_04_0010" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0007">
<testtitle>REPLACE WASHER NOZZLE HEATER ASSEMBLY LH<xref label="Seep01" href="RM000002M6D03DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0012">
<testtitle>REPLACE WASHER NOZZLE HEATER ASSEMBLY RH<xref label="Seep01" href="RM000002M6D03DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0010">
<testtitle>REPLACE COWL SIDE JUNCTION BLOCK RH</testtitle>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0011">
<testtitle>REPLACE NO. 2 MULTIPLEX NETWORK BODY ECU<xref label="Seep01" href="RM0000038MO00QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000005J4Q000X_04_0013">
<testtitle>REPLACE NO. 2 MULTIPLEX NETWORK BODY ECU<xref label="Seep01" href="RM0000038MO00PX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>