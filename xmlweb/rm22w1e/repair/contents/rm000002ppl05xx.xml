<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000002PPL05XX" category="C" type-id="305K8" name-id="ES1696-001" from="201301" to="201308">
<dtccode>P0046</dtccode>
<dtcname>Turbocharger / Supercharger Boost Control Solenoid Circuit Range / Performance</dtcname>
<dtccode>P0047</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control "A" Circuit Low</dtcname>
<dtccode>P0048</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control "A" Circuit High</dtcname>
<dtccode>P004B</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Solenoid "B" Circuit Range/Performance</dtcname>
<dtccode>P004C</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Solenoid "B" Circuit Low</dtcname>
<dtccode>P004D</dtccode>
<dtcname>Turbocharger/Supercharger Boost Control Solenoid "B" Circuit High</dtcname>
<subpara id="RM000002PPL05XX_01" type-id="60" category="03" proc-id="RM22W0E___00003Q700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>These DTCs indicate that the DC motor of the turbocharger is malfunctioning. The ECM monitors the DC motor current to detect an open or short in the DC motor circuit. If the current meets certain criteria, the ECM stores a DTC and illuminates the MIL immediately.</ptxt>
<ptxt>The DC motor is used to operate the nozzle vane of the turbocharger. The nozzle vane opens and closes to change the velocity of exhaust emissions in order to control the turbo pressure. The ECM varies the duty ratio of the DC motor in accordance with the driving conditions.</ptxt>
<ptxt>If the nozzle vane is stuck closed (DC motor stuck off), drivability may deteriorate at wide open throttle. If the nozzle vane is stuck open (DC motor stuck on), drivability may deteriorate at intermediate throttle positions or the engine power may be insufficient.</ptxt>
<table pgwide="1">
<title>P0046 (Bank 1), P004B (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either of the following conditions are met when the motor is driving (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>DC motor duty ratio is +/-100% for 5 seconds or more.</ptxt>
</item>
<item>
<ptxt>DC motor current is 2.2 A or more for 5 seconds or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in DC motor (turbocharger sub-assembly) circuit</ptxt>
</item>
<item>
<ptxt>DC motor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Turbocharger nozzle vane is stuck or movement is irregular</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0047 (Bank 1), P004C (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2 seconds after engine is started, race engine for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>The DC motor duty ratio is 80% or more and the motor current is 0.5 A or less for 3 seconds or more (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in DC motor (turbocharger sub-assembly) circuit</ptxt>
</item>
<item>
<ptxt>DC motor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Turbocharger nozzle vane is stuck or movement is irregular</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0048 (Bank 1), P004D (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2 seconds after engine is started, race engine for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>Overcurrent is detected 25 times or more (0.4 seconds or more) (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in DC motor (turbocharger sub-assembly) circuit</ptxt>
</item>
<item>
<ptxt>DC motor (turbocharger sub-assembly)</ptxt>
</item>
<item>
<ptxt>Turbocharger nozzle vane is stuck or movement is irregular</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTC P0046, P0047, P0048, P004B, P004C or P004D is stored, the following symptom may appear.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stuck closed malfunction:</ptxt>
<ptxt>- Vehicle surge when driving with full load</ptxt>
</item>
<item>
<ptxt>Stuck open malfunction:</ptxt>
<ptxt>- Lack of power</ptxt>
<ptxt>- Vehicle surge or hesitation under light or medium load</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000002PPL05XX_02" type-id="32" category="03" proc-id="RM22W0E___00003Q800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A275247E02" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002PPL05XX_03" type-id="51" category="05" proc-id="RM22W0E___00003Q900000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002PPL05XX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002PPL05XX_04_0016" proc-id="RM22W0E___00003QG00000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0046, P0047, P0048, P004B, P004C OR P004D)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0046, P0047, P0048, P004B, P004C or P004D is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0046, P0047, P0048, P004B, P004C or P004D and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If codes other than P0046, P0047, P0048, P004B, P004C or P004D are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002PPL05XX_04_0002" fin="false">A</down>
<right ref="RM000002PPL05XX_04_0022" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002PPL05XX_04_0002" proc-id="RM22W0E___00003QA00000">
<testtitle>PERFORM ACTIVE TEST USING GTS (OPERATE TURBOCHARGER SUB-ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Active Test / Test the Turbo Charger Step Motor (Test the Turbo Charger Step Motor #2) / Data List / Target VN Turbo Position (Target VN Turbo Position #2) and Actual VN Turbo Position (Actual VN Turbo Position #2).</ptxt>
</test1>
<test1>
<ptxt>While changing the Active Test value to 20, 95, 60 and 20%, check that Actual VN Turbo Position (Actual VN Turbo Position #2) smoothly changes to the set opening amount.</ptxt>
<atten3>
<ptxt>When moving the vanes toward the fully closed position, make sure that Actual VN Turbo Position (Actual VN Turbo Position #2) does not become 5% or less.</ptxt>
</atten3>
<spec>
<title>OK</title>
<specitem>
<ptxt>The Actual VN Turbo Position (Actual VN Turbo Position #2) value follows the Target VN Turbo Position (Target VN Turbo Position #2).</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002PPL05XX_04_0020" fin="true">OK</down>
<right ref="RM000002PPL05XX_04_0021" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PPL05XX_04_0021" proc-id="RM22W0E___00003QH00000">
<testtitle>INSPECT TURBOCHARGER SUB-ASSEMBLY (DC MOTOR RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the DC motor connector.</ptxt>
<figure>
<graphic graphicname="C217215E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 (M-) - 2 (M+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 to 100 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 (M2-) - 2 (M2+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 to 100 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(DC Motor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002PPL05XX_04_0003" fin="false">OK</down>
<right ref="RM000002PPL05XX_04_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PPL05XX_04_0003" proc-id="RM22W0E___00003QB00000">
<testtitle>CHECK HARNESS AND CONNECTOR (DC MOTOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the DC motor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C90-2 (M+) - C45-86 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C90-1 (M-) - C45-85 (M-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z67-2 (M2+) - C45-18 (VN2+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z67-1 (M2-) - C45-41 (VN2-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C90-2 (M+) or C45-86 (M+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C90-1 (M-) or C45-85 (M-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z67-2 (M2+) or C45-18 (VN2+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z67-1 (M2-) or C45-41 (VN2-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C90-2 (M+) - C46-86 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C90-1 (M-) - C46-85 (M-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z67-2 (M2+) - C46-18 (VN2+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z67-1 (M2-) - C46-41 (VN2-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C90-2 (M+) or C46-86 (M+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C90-1 (M-) or C46-85 (M-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z67-2 (M2+) or C46-18 (VN2+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z67-1 (M2-) or C46-41 (VN2-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the DC motor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002PPL05XX_04_0015" fin="false">OK</down>
<right ref="RM000002PPL05XX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PPL05XX_04_0015" proc-id="RM22W0E___00003QF00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000002PPL05XX_04_0014" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000002PPL05XX_04_0013" proc-id="RM22W0E___00003QD00000">
<testtitle>REPLACE TURBOCHARGER SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<ptxt>When DTC P0046, P0047 or P0048 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the turbocharger sub-assembly (for bank 1) (See page <xref label="Seep01" href="RM0000032A801WX"/>).</ptxt>
</item>
</list1>
<ptxt>When DTC P004B, P004C or P004D is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Replace the turbocharger sub-assembly  (for bank 2) (See page <xref label="Seep02" href="RM0000032A801WX"/>).</ptxt>
</item>
</list1>
</content6>
<res>
<right ref="RM000002PPL05XX_04_0014" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000002PPL05XX_04_0009" proc-id="RM22W0E___00003QC00000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002PPL05XX_04_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002PPL05XX_04_0014" proc-id="RM22W0E___00003QE00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) for 5 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>2 seconds after the engine is started, race the engine for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002PPL05XX_04_0012" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002PPL05XX_04_0012">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000002PPL05XX_04_0020">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ10BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PPL05XX_04_0022">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW05CX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>