<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SN_T00LQ" variety="T00LQ">
<name>POWER DOOR LOCK CONTROL SYSTEM</name>
<para id="RM000000TNZ0ALX" category="U" type-id="303FJ" name-id="DL001O-189" from="201301">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM000000TNZ0ALX_z0" proc-id="RM22W0E___0000E8100000">
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: </ptxt>
<ptxt>Body / (desired system) / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>D Door Courtesy SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Driver side door opened</ptxt>
<ptxt>OFF: Driver side door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>D Door Lock Pos SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Driver side door unlocked</ptxt>
<ptxt>OFF: Driver side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Key SW-Lock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door key linked lock switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Driver side door key cylinder turned to lock position</ptxt>
<ptxt>OFF: Driver side door key cylinder not turned</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>D-Door Key SW-UL</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door key linked unlock switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Driver side door key cylinder turned to unlock position</ptxt>
<ptxt>OFF: Driver side door key cylinder not turned</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P-Door Courtesy SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Front passenger side door opened</ptxt>
<ptxt>OFF: Front passenger side door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P-Door Lock Pos SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Front passenger side door unlocked</ptxt>
<ptxt>OFF: Front passenger side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Lock SW-Lock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door manual lock switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Front passenger side door control switch on (lock)</ptxt>
<ptxt>OFF: Front passenger side door control switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Unlock SW-Unlock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door manual unlock switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Front passenger side door control switch on (unlock)</ptxt>
<ptxt>OFF: Front passenger side door control switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL Door Courtesy SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear LH side door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Rear LH side door opened</ptxt>
<ptxt>OFF: Rear LH side door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL-Door Lock Pos SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear LH side door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Rear LH side door unlocked</ptxt>
<ptxt>OFF: Rear LH side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RR Door Courtesy SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear RH side door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Rear RH side door opened</ptxt>
<ptxt>OFF: Rear RH side door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RR-Door Lock Pos SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear RH side door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Rear RH side door unlocked</ptxt>
<ptxt>OFF: Rear RH side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Courtesy SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door courtesy light switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Back door open</ptxt>
<ptxt>OFF: Back door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Open Handle SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door opener handle switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Back door opener switch pushed</ptxt>
<ptxt>OFF: Back door opener switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Lock Pos SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Back door unlocked</ptxt>
<ptxt>OFF: Back door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door open status / Permit or Prohibit</ptxt>
</entry>
<entry>
<ptxt>Permit: Back door unlocked</ptxt>
<ptxt>Prohibit: Back door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>IG SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch status / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Ignition switch ON</ptxt>
<ptxt>OFF: Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ACC SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch status / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Ignition switch ACC</ptxt>
<ptxt>OFF: Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Communication Double Locking</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Connection status between double door lock ECU and main body ECU / OK or STOP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Connection</ptxt>
<ptxt>STOP: No connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Communication Master SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Connection status between master switch and main body ECU / OK or STOP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Connection</ptxt>
<ptxt>STOP: No connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Master Switch</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Door Lock Switch Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door manual lock switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Door control switch ON (LOCK)</ptxt>
<ptxt>OFF: Door control switch OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Unlock Switch Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door manual unlock switch signal / ON or OFF</ptxt>
</entry>
<entry>
<ptxt>ON: Door control switch ON (UNLOCK)</ptxt>
<ptxt>OFF: Door control switch OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Back Door (w/ Power Back Door System)</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Door Lock Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door lock switch signal / UNLOCK or LOCK</ptxt>
</entry>
<entry>
<ptxt>UNLOCK: Back door unlocked</ptxt>
<ptxt>LOCK: Back door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: </ptxt>
<ptxt>Body / Main Body / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, perform the Active Test.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Door Lock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate door lock motor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock or Unlock</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>D-Door Unlock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate door lock motor</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Trunk and Back-Door Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate back door opener</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Back Door (w/ Power Back Door System)</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Latch Release</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Operate back door lock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>