<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000A" variety="S000A">
<name>1GR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000A_7C3EU_T007X" variety="T007X">
<name>CYLINDER BLOCK</name>
<para id="RM000002YD1019X" category="A" type-id="30019" name-id="EM6HU-04" from="201301">
<name>REPLACEMENT</name>
<subpara id="RM000002YD1019X_01" type-id="01" category="01">
<s-1 id="RM000002YD1019X_01_0002" proc-id="RM22W0E___000044Z00000">
<ptxt>REPLACE STRAIGHT PIN</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>It is not necessary to remove a straight pin unless it is being replaced.</ptxt>
</atten3>
<figure>
<graphic graphicname="A224781E03" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upper Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lower Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>Remove the straight pins.</ptxt>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer, tap in new straight pins to the cylinder block.</ptxt>
<spec>
<title>Standard Protrusion Height</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Pin A</ptxt>
</entry>
<entry align="center">
<ptxt>22.5 to 23.5 mm (0.886 to 0.925 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Pin B</ptxt>
</entry>
<entry align="center">
<ptxt>10.5 to 11.5 mm (0.413 to 0.453 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Pin C</ptxt>
</entry>
<entry align="center">
<ptxt>8.5 to 9.5 mm (0.335 to 0.374 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Pin D</ptxt>
</entry>
<entry align="center">
<ptxt>5.5 to 6.5 mm (0.217 to 0.256 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YD1019X_01_0003" proc-id="RM22W0E___000045000000">
<ptxt>REPLACE CYLINDER BLOCK TIGHT PLUG</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>If coolant leaks from a tight plug or a plug is corroded, replace it.</ptxt>
</atten3>
<figure>
<graphic graphicname="A221129E03" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard Depth</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>LH Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>RH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>Remove the tight plugs.</ptxt>
</s2>
<s2>
<ptxt>Apply adhesive around new tight plugs.</ptxt>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1324, Three Bond 1324 or equivalent</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Using SST and a hammer, tap in the tight plugs to the standard depth.</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00350</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
</sst>
<spec>
<title>Standard depth</title>
<specitem>
<ptxt>0.2 to 1.2 mm (0.00787 to 0.0472 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>