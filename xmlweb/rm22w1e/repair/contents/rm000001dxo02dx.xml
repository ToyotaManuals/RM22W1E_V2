<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000001DXO02DX" category="C" type-id="804QZ" name-id="BCG1X-01" from="201301" to="201308">
<dtccode>C1256</dtccode>
<dtcname>Accumulator Low Pressure</dtcname>
<subpara id="RM000001DXO02DX_01" type-id="60" category="03" proc-id="RM22W0E___0000AKH00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC C1254 (See page <xref label="Seep01" href="RM000001EA802DX_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1256</ptxt>
</entry>
<entry valign="middle">
<ptxt>The fluid pressure inside the accumulator is below the standard value.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Brake booster with accumulator pump assembly</ptxt>
</item>
<item>
<ptxt>Master cylinder solenoid (Accumulator pressure sensor)</ptxt>
</item>
<item>
<ptxt>Hydraulic brake booster assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001DXO02DX_02" type-id="51" category="05" proc-id="RM22W0E___0000AKI00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>When DTC C1251, C1253 or C1254 is output together with DTC C1256 inspect and repair the trouble area indicated by DTC C1251, C1253 or C1254 first.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001DXO02DX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001DXO02DX_03_0001" proc-id="RM22W0E___0000AKJ00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00MX"/>). </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal 20 times or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Wait for 99 seconds. </ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00MX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Excessive brake pedal operation results in abnormal accumulator pressure consumption. This is normal.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000001DXO02DX_03_0002" fin="false">A</down>
<right ref="RM000001DXO02DX_03_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0002" proc-id="RM22W0E___0000AKK00000">
<testtitle>INSPECT HYDRAULIC BRAKE BOOSTER ASSEMBLY FUNCTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect brake booster pump power supply system function.</ptxt>
<test2>
<ptxt>Turn the ignition switch off.</ptxt>
</test2>
<test2>
<ptxt>Depress the brake pedal 20 times or more to release the pressure in the accumulator.</ptxt>
<atten4>
<ptxt>When the pressure is released, the brake pedal stroke becomes longer.</ptxt>
</atten4>
</test2>
<test2>
<ptxt>Check that the brake fluid level is at the MAX level.</ptxt>
</test2>
<test2>
<ptxt>Chock the 4 wheels and release the parking brake.</ptxt>
</test2>
<test2>
<ptxt>Turn the ignition switch to ON and measure the brake booster pump operating time (the time from when the brake booster pump starts operating until it stops).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>20 to 80 seconds</ptxt>
</specitem>
</spec>
</test2>
<test2>
<ptxt>Start the engine after the brake boost pump stops.</ptxt>
</test2>
<test2>
<ptxt>Check that the ABS warning light and slip indicator light are not illuminated.</ptxt>
</test2>
<test2>
<ptxt>Turn off the engine, and then turn the ignition switch to ON.</ptxt>
</test2>
<test2>
<ptxt>Check that the brake boost pump operates and then stops when the brake pedal is depressed 4 or 5 times.</ptxt>
</test2>
<test2>
<ptxt>Depress the brake pedal 4 or 5 times and measure the brake booster pump operating time (the time from when the brake booster pump starts operating until it stops).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>2 to 11 seconds</ptxt>
</specitem>
</spec>
</test2>
<test2>
<ptxt>Check that the brake warning light illuminates and the buzzer sounds when the brake pedal is fully depressed continuously 15 to 20 times.</ptxt>
<atten3>
<ptxt>Wait 120 seconds or more after turning the ignition switch to ON before performing this inspection.</ptxt>
</atten3>
</test2>
</test1>
<test1>
<ptxt>Check brake booster operation.</ptxt>
<test2>
<ptxt>Turn the ignition switch off.</ptxt>
</test2>
<test2>
<ptxt>Depress the brake pedal 20 times or more to release the pressure in the accumulator.</ptxt>
<atten4>
<ptxt>When the pressure is released, the brake pedal stroke becomes longer.</ptxt>
</atten4>
</test2>
<test2>
<ptxt>Depress the brake pedal, start the engine, and check the change in the brake pedal height.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The brake pedal moves slightly inward.</ptxt>
</specitem>
</spec>
</test2>
</test1>
<test1>
<ptxt>Inspect brake master cylinder fluid pressure change (for LHD: See page <xref label="Seep01" href="RM00000171W029X_01_0001"/>, for RHD: See page <xref label="Seep02" href="RM00000171W02AX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001DXO02DX_03_0003" fin="false">OK</down>
<right ref="RM000001DXO02DX_03_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0003" proc-id="RM22W0E___0000AKL00000">
<testtitle>READ VALUE USING GTS (ACCUMULATOR SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3. </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
</test1>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Accumulator Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accumulator pressure sensor reading/ Min.: 0.00 V, Max.: 5.00 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.58 to 5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>If the value is constant regardless of the pump operation, an accumulator pressure sensor malfunction is suspected.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<test1>
<ptxt>Check that the accumulator output value is normal.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Accumulator pressure sensor value is normal.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXO02DX_03_0004" fin="false">A</down>
<right ref="RM000001DXO02DX_03_0005" fin="true">B</right>
<right ref="RM000001DXO02DX_03_0009" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0004" proc-id="RM22W0E___0000AKM00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00MX"/>). </ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00MX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.47in"/>
<colspec colname="COL2" colwidth="1.66in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXO02DX_03_0006" fin="true">A</down>
<right ref="RM000001DXO02DX_03_0005" fin="true">B</right>
<right ref="RM000001DXO02DX_03_0009" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0010" proc-id="RM22W0E___0000JZ900000">
<testtitle>INSPECT BRAKE BOOSTER PUMP ASSEMBLY</testtitle>
<content6 releasenbr="2">
<test1>
<ptxt>Remove the hydraulic brake booster assembly (for LHD: See page <xref label="Seep01" href="RM00000171T028X"/>, for RHD: See page <xref label="Seep02" href="RM00000171T029X"/>).  </ptxt>
</test1>
<test1>
<ptxt>Using a screwdriver, remove the 2 screws and pull out the wire harness from the master cylinder solenoid.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C173363E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.88in"/>
<colspec colname="COL2" colwidth="1.26in"/>
<colspec colname="COL3" colwidth="0.99in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Red wire terminal - Black wire terminal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 2 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pump motor wire harness</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Red wire</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Black wire</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Install the pump motor wire harness to the master cylinder solenoid with the 2 screws (for LHD: See page <xref label="Seep03" href="RM00000171V01UX_01_0020"/>, for RHD: See page <xref label="Seep04" href="RM00000171V01VX_01_0020"/>).</ptxt>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001DXO02DX_03_0007" fin="true">A</down>
<right ref="RM000001DXO02DX_03_0013" fin="true">B</right>
<right ref="RM000001DXO02DX_03_0011" fin="true">C</right>
<right ref="RM000001DXO02DX_03_0012" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0005">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0006">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0007">
<testtitle>REPLACE HYDRAULIC BRAKE BOOSTER ASSEMBLY<xref label="Seep01" href="RM00000171T028X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0009">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0011">
<testtitle>REPLACE BRAKE BOOSTER WITH ACCUMULATOR PUMP ASSEMBLY<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0012">
<testtitle>REPLACE BRAKE BOOSTER WITH ACCUMULATOR PUMP ASSEMBLY<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXO02DX_03_0013">
<testtitle>REPLACE HYDRAULIC BRAKE BOOSTER ASSEMBLY<xref label="Seep01" href="RM00000171T029X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>