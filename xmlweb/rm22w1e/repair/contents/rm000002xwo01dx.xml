<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T0_T00M3" variety="T00M3">
<name>ENTRY LOCK AND UNLOCK SWITCH (for Front)</name>
<para id="RM000002XWO01DX" category="A" type-id="30014" name-id="TD8F1-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000002XWO01DX_05" type-id="11" category="10" proc-id="RM22W0E___0000EE000000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the LH side and RH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000002XWO01DX_04" type-id="01" category="01">
<s-1 id="RM000002XWO01DX_04_0018" proc-id="RM22W0E___0000EDR00000">
<ptxt>INSTALL FRONT DOOR HANDLE ASSEMBLY OUTSIDE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the handle by pushing it in the direction of the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="B182925" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a T30 ''TORX'' socket, install the screw.</ptxt>
<figure>
<graphic graphicname="B180849" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002XWO01DX_04_0027" proc-id="RM22W0E___0000E4K00000">
<ptxt>INSTALL FRONT DOOR OUTSIDE HANDLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the front door outside handle cover LH to the front door lock cylinder.</ptxt>
<figure>
<graphic graphicname="B183142" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a T30 ''TORX'' socket, install the front door outside cover LH (with the door lock cylinder) with the screw.</ptxt>
<torque>
<torqueitem>
<t-value1>4.0</t-value1>
<t-value2>41</t-value2>
<t-value3>35</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the hole plug.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002XWO01DX_04_0028" proc-id="RM22W0E___0000E4H00000">
<ptxt>INSTALL FRONT DOOR SERVICE HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply butyl tape to the door.</ptxt>
<figure>
<graphic graphicname="B180831" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Pass the front door lock remote control cable assembly LH and front door inside locking cable assembly LH through a new front door service hole cover LH.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When installing the front door service hole cover LH, pull the links and connectors through the front door service hole cover LH.</ptxt>
</item>
<item>
<ptxt>There should be no wrinkles or folds after attaching the front door service hole cover LH.</ptxt>
</item>
<item>
<ptxt>After attaching the front door service hole cover LH, check the sealing quality.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Connect the 2 connectors.</ptxt>
<figure>
<graphic graphicname="B186231E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 9 clamps.</ptxt>
</s2>
<s2>
<ptxt>Install the bolt as shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>8.4</t-value1>
<t-value2>86</t-value2>
<t-value3>74</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002XWO01DX_04_0029" proc-id="RM22W0E___0000BP000000">
<ptxt>INSTALL FRONT DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B183139" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the front door lock remote control cable assembly LH and front door inside locking cable assembly LH to the front door inside handle sub-assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 claws and 13 clips to install the front door trim board sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180821" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002XWO01DX_04_0030" proc-id="RM22W0E___0000E4I00000">
<ptxt>INSTALL COURTESY LIGHT ASSEMBLY (w/ Courtesy Light)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="E155940" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the claw of the courtesy light to the front door trim.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002XWO01DX_04_0031" proc-id="RM22W0E___0000BP400000">
<ptxt>INSTALL DOOR ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 claws to install the door assist grip cover LH to the front door trim board sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002XWO01DX_04_0032" proc-id="RM22W0E___0000E4J00000">
<ptxt>INSTALL FRONT DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door inside handle bezel LH to the trim board with the 7 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002XWO01DX_04_0033" proc-id="RM22W0E___0000BP100000">
<ptxt>INSTALL FRONT DOOR ARMREST BASE PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector. </ptxt>
</s2>
<s2>
<ptxt>Attach the 5 claws to install the armrest base panel.</ptxt>
<figure>
<graphic graphicname="B183137" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002XWO01DX_04_0034" proc-id="RM22W0E___0000BP500000">
<ptxt>INSTALL FRONT LOWER DOOR FRAME BRACKET GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and claw, and install the front door lower frame bracket garnish LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002XWO01DX_04_0035" proc-id="RM22W0E___0000EDY00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002XWO01DX_04_0036" proc-id="RM22W0E___0000EDZ00000">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0IZX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>