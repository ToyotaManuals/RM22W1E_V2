<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7C3A6_T0039" variety="T0039">
<name>1VD-FTV LUBRICATION</name>
<para id="RM0000033FQ004X" category="F" type-id="30028" name-id="SS3W3-02" from="201301">
<name>TORQUE SPECIFICATIONS</name>
<subpara id="RM0000033FQ004X_z0" proc-id="RM22W0E___000007Z00000">
<content5 releasenbr="1">
<table pgwide="1">
<title>Oil and Oil Filter</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry>
<ptxt>Part Tightened</ptxt>
</entry>
<entry>
<ptxt>N*m</ptxt>
</entry>
<entry>
<ptxt>kgf*cm</ptxt>
</entry>
<entry>
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Oil pan drain plug x No. 2 oil pan</ptxt>
</entry>
<entry>
<ptxt>38</ptxt>
</entry>
<entry>
<ptxt>387</ptxt>
</entry>
<entry>
<ptxt>28</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Oil filter drain plug x Oil filter cap</ptxt>
</entry>
<entry>
<ptxt>13</ptxt>
</entry>
<entry>
<ptxt>127</ptxt>
</entry>
<entry>
<ptxt>9</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Oil filter cap x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>25</ptxt>
</entry>
<entry>
<ptxt>255</ptxt>
</entry>
<entry>
<ptxt>18</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 2 engine under cover seal of No. 2 engine under cover x No. 2 engine under cover</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Oil Pressure Sensor</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry>
<ptxt>Part Tightened</ptxt>
</entry>
<entry>
<ptxt>N*m</ptxt>
</entry>
<entry>
<ptxt>kgf*cm</ptxt>
</entry>
<entry>
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Oil pressure sender gauge x Oil filter bracket</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
<entry>
<ptxt>153</ptxt>
</entry>
<entry>
<ptxt>11</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Oil Level Sensor</title>
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COLSPEC0" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.4in"/>
<thead>
<row>
<entry namest="COL1" nameend="COLSPEC0">
<ptxt>Part Tightened</ptxt>
</entry>
<entry>
<ptxt>N*m</ptxt>
</entry>
<entry>
<ptxt>kgf*cm</ptxt>
</entry>
<entry>
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COLSPEC0">
<ptxt>Engine oil level sensor x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Oil cooler tube x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>for Automatic Transmission</ptxt>
</entry>
<entry>
<ptxt>14</ptxt>
</entry>
<entry>
<ptxt>143</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Oil Pump</title>
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Part Tightened</ptxt>
</entry>
<entry>
<ptxt>N*m</ptxt>
</entry>
<entry>
<ptxt>kgf*cm</ptxt>
</entry>
<entry>
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Oil relief valve x Oil relief valve body</ptxt>
</entry>
<entry>
<ptxt>50</ptxt>
</entry>
<entry>
<ptxt>510</ptxt>
</entry>
<entry>
<ptxt>37</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Oil pump x Cylinder block</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Rear engine oil seal retainer x Cylinder block</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Oil regulator x Oil pump</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 1 oil pan x Cylinder block</ptxt>
</entry>
<entry>
<ptxt>25</ptxt>
</entry>
<entry>
<ptxt>250</ptxt>
</entry>
<entry>
<ptxt>18</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Oil strainer x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 2 oil pan x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Engine oil level sensor x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Oil filter bracket x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Timing gear cover spacer x Timing gear cover</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
<row>
<entry morerows="1">
<ptxt>V-ribbed belt tensioner x Timing chain cover</ptxt>
</entry>
<entry>
<ptxt>Bolt length: 116 mm (4.57 in.) and 95 mm (3.74 in.)</ptxt>
</entry>
<entry>
<ptxt>43</ptxt>
</entry>
<entry>
<ptxt>438</ptxt>
</entry>
<entry>
<ptxt>32</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Bolt length: 40 mm (1.58 in.)</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 1 idler pulley x V-ribbed belt tensioner</ptxt>
</entry>
<entry>
<ptxt>43</ptxt>
</entry>
<entry>
<ptxt>438</ptxt>
</entry>
<entry>
<ptxt>32</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>V-ribbed belt tensioner bracket x V-ribbed belt tensioner</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Stiffener insulator RH x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 2 inlet turbo oil pipe x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>29</ptxt>
</entry>
<entry>
<ptxt>296</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>No. 1 engine oil level dipstick guide x No. 1 oil pan and cylinder head</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Scavenging Pump</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry>
<ptxt>Part Tightened</ptxt>
</entry>
<entry>
<ptxt>N*m</ptxt>
</entry>
<entry>
<ptxt>kgf*cm</ptxt>
</entry>
<entry>
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Scavenging pump x Cylinder block</ptxt>
</entry>
<entry>
<ptxt>10</ptxt>
</entry>
<entry>
<ptxt>102</ptxt>
</entry>
<entry>
<ptxt>7</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Engine Oil Cooler</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry>
<ptxt>Part Tightened</ptxt>
</entry>
<entry>
<ptxt>N*m</ptxt>
</entry>
<entry>
<ptxt>kgf*cm</ptxt>
</entry>
<entry>
<ptxt>ft.*lbf</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Oil cooler x No. 1 oil pan</ptxt>
</entry>
<entry>
<ptxt>21</ptxt>
</entry>
<entry>
<ptxt>214</ptxt>
</entry>
<entry>
<ptxt>15</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>