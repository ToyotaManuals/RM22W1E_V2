<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12048_S001Z" variety="S001Z">
<name>1GR-FE BATTERY / CHARGING</name>
<ttl id="12048_S001Z_7C3S2_T00L5" variety="T00L5">
<name>GENERATOR (for 100 A Type)</name>
<para id="RM000001ONP04JX" category="A" type-id="80001" name-id="BH1LT-01" from="201308">
<name>REMOVAL</name>
<subpara id="RM000001ONP04JX_01" type-id="01" category="01">
<s-1 id="RM000001ONP04JX_01_0063" proc-id="RM22W0E___0000D2500001">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001ONP04JX_01_0027" proc-id="RM22W0E___0000D2400001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001ONP04JX_01_0030" proc-id="RM22W0E___00000Z800001">
<ptxt>REMOVE V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Raise the front of the V-bank cover to detach the 2 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<figure>
<graphic graphicname="A267633E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000001ONP04JX_01_0059" proc-id="RM22W0E___000011Z00002">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001ONP04JX_01_0060" proc-id="RM22W0E___000011Y00002">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001ONP04JX_01_0061" proc-id="RM22W0E___000011X00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts and No. 1 engine under cover.</ptxt>
<figure>
<graphic graphicname="A178461" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001ONP04JX_01_0045" proc-id="RM22W0E___000042I00000">
<ptxt>REMOVE FAN AND GENERATOR V BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>While turning the belt tensioner counterclockwise, align the service hole for the belt tensioner and the belt tensioner fixing position, and then insert a bar with a diameter of 6 mm (0.236 in.) into the service hole to fix the belt tensioner in place.</ptxt>
<figure>
<graphic graphicname="A217571" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>The pulley bolt for the belt tensioner has a left-hand thread.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the V belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001ONP04JX_01_0062" proc-id="RM22W0E___00004A100000">
<ptxt>REMOVE FRONT FENDER APRON TRIM PACKING B
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ KDSS:</ptxt>
<ptxt>Remove the 3 clips and front fender apron trim packing B.</ptxt>
<figure>
<graphic graphicname="A177004" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/o KDSS:</ptxt>
<ptxt>Remove the 4 clips and front fender apron trim packing B.</ptxt>
<figure>
<graphic graphicname="A176301" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001ONP04JX_01_0004" proc-id="RM22W0E___0000D2300001">
<ptxt>REMOVE GENERATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the generator connector.</ptxt>
<figure>
<graphic graphicname="A267518" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Open the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and disconnect the generator wire from terminal B.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the wire harness clamp bracket.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the generator bracket.</ptxt>
<figure>
<graphic graphicname="A267519" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and generator.</ptxt>
<figure>
<graphic graphicname="A222883" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and generator bracket.</ptxt>
<figure>
<graphic graphicname="A222885" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>