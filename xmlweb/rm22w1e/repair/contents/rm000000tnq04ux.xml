<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S0024" variety="S0024">
<name>DOOR LOCK</name>
<ttl id="12051_S0024_7C3SN_T00LQ" variety="T00LQ">
<name>POWER DOOR LOCK CONTROL SYSTEM</name>
<para id="RM000000TNQ04UX" category="D" type-id="3001B" name-id="DL01K-99" from="201308">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM000000TNQ04UX_z0" proc-id="RM22W0E___0000E7Q00001">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use these procedures to troubleshoot the power door lock control system.</ptxt>
</item>
<item>
<ptxt>*: Use the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<spec>
<title>Standard Voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding.</ptxt>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK COMMUNICATION FUNCTION OF CAN COMMUNICATION SYSTEM*</testtitle>
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>CAN DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to CAN COMMUNICATION SYSTEM (See page <xref label="Seep10" href="RM000003A8W005X"/>)</action-ci-right>
<result>C</result>
<action-ci-right>Go to CAN COMMUNICATION SYSTEM (See page <xref label="Seep12" href="RM000002L7X02IX"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK COMMUNICATION FUNCTION OF LIN COMMUNICATION SYSTEM*</testtitle>
<test1>
<ptxt>Use the intelligent tester to check if the LIN communication system is functioning normally.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>LIN DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>LIN DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to LIN COMMUNICATION SYSTEM (See page <xref label="Seep11" href="RM000002S8802GX"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to problem symptoms table (See page <xref label="Seep09" href="RM000000TNU074X"/>).</ptxt>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Fault is not listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Fault is listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<results>
<result>B</result>
<action-ci-right>Go to step 7</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>OVERALL ANALYSIS AND TROUBLESHOOTING*</testtitle>
<test1>
<ptxt>Operation Check (See page <xref label="Seep05" href="RM000002T6K05PX"/>).</ptxt>
</test1>
<test1>
<ptxt>Terminals of ECU (See page <xref label="Seep06" href="RM000001JW704YX"/>).</ptxt>
</test1>
<test1>
<ptxt>Data List / Active Test  (See page <xref label="Seep07" href="RM000000TNZ0ALX"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>ADJUST, REPAIR OR REPLACE</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>