<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SE_T00LH" variety="T00LH">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000002S8A035X" category="C" type-id="3019M" name-id="NW2B9-03" from="201301" to="201308">
<dtccode>B1273</dtccode>
<dtcname>Sliding Roof ECU Communication Stop</dtcname>
<subpara id="RM000002S8A035X_01" type-id="60" category="03" proc-id="RM22W0E___0000E0A00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when LIN communication between the sliding roof ECU and main body ECU (cowl side junction block LH) stops for 10 seconds or more.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.64in"/>
<colspec colname="COL2" colwidth="3.10in"/>
<colspec colname="COL3" colwidth="2.34in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1273</ptxt>
</entry>
<entry valign="middle">
<ptxt>No communication between the sliding roof ECU and main body ECU (cowl side junction block LH) for 10 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Sliding roof ECU</ptxt>
</item>
<item>
<ptxt>Main body ECU (cowl side junction block LH)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002S8A035X_02" type-id="32" category="03" proc-id="RM22W0E___0000E0B00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B173719E14" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002S8A035X_03" type-id="51" category="05" proc-id="RM22W0E___0000E0C00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot:</ptxt>
<ptxt>Connect the intelligent tester to the vehicle, and turn a courtesy switch on and off at 1.5 second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</atten3>
<atten4>
<ptxt>DTC B2325 is stored when the communication between the sliding roof ECU and main body ECU (cowl side junction block LH) stops.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002S8A035X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002S8A035X_04_0001" proc-id="RM22W0E___0000E0D00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8A035X_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8A035X_04_0002" proc-id="RM22W0E___0000E0E00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1273 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1273 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8A035X_04_0012" fin="false">A</down>
<right ref="RM000002S8A035X_04_0007" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8A035X_04_0012" proc-id="RM22W0E___0000E0I00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - SLIDING ROOF ECU)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E156401E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the E3 and R5 ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E3-10 (LIN2) - R5-4 (MPX1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E3-10 (LIN2) or R5-4 (MPX1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R5-4 (MPX1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8A035X_04_0004" fin="false">OK</down>
<right ref="RM000002S8A035X_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8A035X_04_0004" proc-id="RM22W0E___0000E0F00000">
<testtitle>CHECK SLIDING ROOF ECU (BATTERY VOLTAGE AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the R5 ECU connector.</ptxt>
<figure>
<graphic graphicname="B163105E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COLSPEC3" colwidth="1.37in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R5-2 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R5-1 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8A035X_04_0005" fin="false">OK</down>
<right ref="RM000002S8A035X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8A035X_04_0005" proc-id="RM22W0E___0000E0G00000">
<testtitle>REPLACE SLIDING ROOF ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the sliding roof ECU with a new or normally functioning one (See page <xref label="Seep01" href="RM000002LPB02PX"/>).</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep02" href="RM000002S8D030X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8A035X_04_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8A035X_04_0006" proc-id="RM22W0E___0000E0H00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep01" href="RM000002S8D030X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" colwidth="1.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1273 is not output </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1273 is output </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8A035X_04_0010" fin="true">A</down>
<right ref="RM000002S8A035X_04_0011" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8A035X_04_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000UZ30DCX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8A035X_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8A035X_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8A035X_04_0011">
<testtitle>REPLACE MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)</testtitle>
</testgrp>
<testgrp id="RM000002S8A035X_04_0010">
<testtitle>END (SLIDING ROOF ECU IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>