<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3ML_T00FO" variety="T00FO">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV)</name>
<para id="RM0000012TD07QX" category="C" type-id="302FJ" name-id="AT9N7-01" from="201301" to="201308">
<dtccode>P0722</dtccode>
<dtcname>Output Speed Sensor Circuit No Signal</dtcname>
<subpara id="RM0000012TD07QX_01" type-id="60" category="03" proc-id="RM22W0E___000081900000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The speed sensor SP2 detects the rotation speed of the transmission output shaft and sends signals to the ECM. The ECM determines the vehicle speed based on these signals. An AC voltage is generated in the speed sensor SP2 coil as the parking gear mounted on the rear planetary gear assembly rotates, and this voltage is sent to the ECM. The parking gear on the rear planetary gear is used as the timing rotor for this sensor.</ptxt>
<ptxt>The gear shift point and lock-up timing are controlled by the ECM based on the signals from this vehicle speed sensor and the throttle position sensor signal.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0722</ptxt>
</entry>
<entry valign="middle">
<ptxt>All the conditions below are detected 500 times or more continuously (2 trip detection logic).</ptxt>
<ptxt>(a) No signal from the speed sensor SP2 is input to the ECM while 4 pulses of the No. 1 vehicle speed sensor signal are sent.</ptxt>
<ptxt>(b) The vehicle speed is 9 km/h (6 mph) or more for at least 5 sec.</ptxt>
<ptxt>(c) The park/neutral position switch is off.</ptxt>
<ptxt>(d) The transfer position is not in neutral.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in speed sensor SP2 circuit</ptxt>
</item>
<item>
<ptxt>Speed sensor SP2</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake or gear, etc.)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Reference: Inspect using an oscilloscope.</ptxt>
<ptxt>Check the waveform of the ECM connector.</ptxt>
<spec>
<title>Standard</title>
<table>
<title>for LHD</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COL3" colwidth="1.03in"/>
<colspec colname="COL4" colwidth="1.04in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>C45-68 (SP2+) - C45-67 (SP2-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2 V/DIV., 2 msec./DIV</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle speed 20 km/h (12 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Refer to illustration</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COL3" colwidth="1.03in"/>
<colspec colname="COL4" colwidth="1.04in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>C46-68 (SP2+) - C46-67 (SP2-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2 V/DIV., 2 msec./DIV</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Vehicle speed 20 km/h (12 mph)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Refer to illustration</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="C175013E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000012TD07QX_02" type-id="64" category="03" proc-id="RM22W0E___000081A00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The output speed sensor SP2 monitors the output shaft speed. The ECM controls the gear shift point and the lock up timing based on the signals from the output speed sensor SP2 and throttle position sensor.</ptxt>
<ptxt>If the ECM detects no signal from the output speed sensor SP2 even while the vehicle is moving, it will conclude that there is a malfunction of the output speed sensor SP2. The ECM will illuminate the MIL and store a DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM0000012TD07QX_03" type-id="32" category="03" proc-id="RM22W0E___000081B00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C155187E24" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000012TD07QX_04" type-id="51" category="05" proc-id="RM22W0E___000081C00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
<table pgwide="1">
<title>ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>SPD (SP2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output shaft speed/</ptxt>
<ptxt>Min.: 0 km/h (0 mph)</ptxt>
<ptxt>Max.: 255 km/h (158 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle stopped: 0 km/h (0 mph) (output shaft speed equal to vehicle speed)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>SPD (SP2) is always 0 while driving:</ptxt>
<ptxt>Open or short in the sensor or circuit.</ptxt>
</item>
<item>
<ptxt>The SPD (SP2) value displayed on the tester is much lower than the actual vehicle speed:</ptxt>
<ptxt>Sensor trouble, improper installation, or intermittent connection trouble of the circuit.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM0000012TD07QX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012TD07QX_05_0001" proc-id="RM22W0E___000081D00000">
<testtitle>INSPECT SPEED SENSOR SP2 INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the speed sensor SP2 installation.</ptxt>
<figure>
<graphic graphicname="C159067E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>The installation bolt is tightened properly and there is no clearance between the sensor and transmission case.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012TD07QX_05_0002" fin="false">OK</down>
<right ref="RM0000012TD07QX_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012TD07QX_05_0002" proc-id="RM22W0E___000081E00000">
<testtitle>INSPECT SPEED SENSOR SP2</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the speed sensor connector.</ptxt>
<figure>
<graphic graphicname="C159420E02" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>560 to 680 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012TD07QX_05_0008" fin="false">OK</down>
<right ref="RM0000012TD07QX_05_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012TD07QX_05_0008" proc-id="RM22W0E___000081F00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SPEED SENSOR SP2 - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="C177486E16" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C45-68 (SP2+) - C45-67 (SP2-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>560 to 680 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-68 (SP2+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-67 (SP2-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C46-68 (SP2+) - C46-67 (SP2-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>560 to 680 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-68 (SP2+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-67 (SP2-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012TD07QX_05_0007" fin="true">OK</down>
<right ref="RM0000012TD07QX_05_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012TD07QX_05_0004">
<testtitle>SECURELY INSTALL OR REPLACE SPEED SENSOR SP2<xref label="Seep01" href="RM000002BL403LX_01_0002"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012TD07QX_05_0005">
<testtitle>REPLACE SPEED SENSOR SP2<xref label="Seep01" href="RM000002BL403LX_01_0002"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012TD07QX_05_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000012TD07QX_05_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202SX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>