<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C40B_T00TE" variety="T00TE">
<name>BLACK OUT TAPE (for Front Door)</name>
<para id="RM0000039Z400MX" category="A" type-id="80001" name-id="ET8DQ-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM0000039Z400MX_01" type-id="11" category="10" proc-id="RM22W0E___0000JO400000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides. </ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>When removing the black out tape and window frame moulding, heat the vehicle body, black out tape and window frame moulding using a heat light.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>Standard Heating Temperature</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Body</ptxt>
</entry>
<entry valign="middle">
<ptxt>40 to 60°C (104 to 140°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Black Out Tape</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Window Frame Moulding</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not heat the vehicle body, black out tape and window frame moulding excessively.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM0000039Z400MX_02" type-id="01" category="01">
<s-1 id="RM0000039Z400MX_02_0032" proc-id="RM22W0E___0000JOA00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039Z400MX_02_0004" proc-id="RM22W0E___0000JO800000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039Z400MX_02_0021" proc-id="RM22W0E___0000BPE00000">
<ptxt>REMOVE FRONT LOWER DOOR FRAME BRACKET GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clip and claw, and remove the front door lower frame bracket garnish LH.</ptxt>
<figure>
<graphic graphicname="B180820" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0022" proc-id="RM22W0E___0000BPA00000">
<ptxt>REMOVE FRONT DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the front door inside handle bezel LH.</ptxt>
<figure>
<graphic graphicname="B180816E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0023" proc-id="RM22W0E___0000BPB00000">
<ptxt>REMOVE FRONT DOOR ARMREST BASE PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180817" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 5 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the armrest base panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0024" proc-id="RM22W0E___0000BPF00000">
<ptxt>REMOVE DOOR ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a moulding remover, detach the 8 claws and remove the door assist grip cover LH.</ptxt>
<figure>
<graphic graphicname="B180819" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0025" proc-id="RM22W0E___0000BPC00000">
<ptxt>REMOVE FRONT DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B310800E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 13 clips.</ptxt>
</s2>
<s2>
<ptxt>Remove the front inner door glass weatherstrip LH together with the front door trim board sub-assembly LH by pulling them upward in the order shown in the illustration.</ptxt>
<atten4>
<ptxt>Make sure that the pin labeled A in the illustration is detached from the door panel.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 cables from the front door inside handle sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B180822" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0026" proc-id="RM22W0E___0000IDK00000">
<ptxt>REMOVE OUTER REAR VIEW MIRROR ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180573E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>w/ Power Mirror Control System:</ptxt>
<s3>
<ptxt>Disconnect the connector labeled A.</ptxt>
</s3>
<s3>
<ptxt>Detach the clamp.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Remove the 3 nuts and claw.</ptxt>
</s2>
<s2>
<ptxt>Remove the outer rear view mirror.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0027" proc-id="RM22W0E___0000E4X00000">
<ptxt>REMOVE FRONT DOOR SERVICE HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, detach the 9 clamps. </ptxt>
<figure>
<graphic graphicname="B182874" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>Remove the front door service hole cover LH.</ptxt>
<atten4>
<ptxt>Remove the remaining tape on the door.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0028" proc-id="RM22W0E___0000IDC00000">
<ptxt>REMOVE FRONT INNER DOOR GLASS WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 clips and remove the front inner door glass weatherstrip LH from the front door trim board sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B310801" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Squeeze the metal clip to remove it.</ptxt>
</item>
<item>
<ptxt>The metal clip may be deformed if it is forcibly pulled.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0029" proc-id="RM22W0E___0000I1900000">
<ptxt>REMOVE FRONT DOOR GLASS SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Driver Side:</ptxt>
<ptxt>Temporarily install the multiplex network master switch.</ptxt>
</s2>
<s2>
<ptxt>Passenger Side:</ptxt>
<ptxt>Temporarily install the power window regulator switch assembly.</ptxt>
</s2>
<s2>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</s2>
<s2>
<ptxt>Move the front door glass sub-assembly until the bolts appear in the service holes.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B180832" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful when removing the bolts as the glass may fall and become damaged.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the front door glass sub-assembly in the direction indicated by the arrows in the illustration.</ptxt>
<figure>
<graphic graphicname="B180833" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Remove the front door glass sub-assembly upward.</ptxt>
</atten4>
<atten3>
<ptxt>Be careful not to damage the glass.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Driver Side:</ptxt>
<ptxt>Remove the multiplex network master switch.</ptxt>
</s2>
<s2>
<ptxt>Passenger Side:</ptxt>
<ptxt>Remove the power window regulator switch assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0030" proc-id="RM22W0E___0000IDD00000">
<ptxt>REMOVE FRONT DOOR GLASS RUN LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front door glass run LH.</ptxt>
<figure>
<graphic graphicname="B180838" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0019" proc-id="RM22W0E___0000IDU00000">
<ptxt>REMOVE FRONT DOOR BELT MOULDING ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182606E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the belt moulding.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B182604" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Detach the claw and remove the belt moulding.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0020" proc-id="RM22W0E___0000IDV00000">
<ptxt>REMOVE FRONT DOOR REAR WINDOW FRAME MOULDING LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182608E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the window frame moulding.</ptxt>
</s2>
<s2>
<ptxt>Detach the clip and remove the double-sided tape to remove the window frame moulding. </ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0031" proc-id="RM22W0E___0000IDN00000">
<ptxt>REMOVE LOWER DOOR FRAME GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw and remove the lower door frame garnish LH.</ptxt>
<figure>
<graphic graphicname="B180839" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039Z400MX_02_0018" proc-id="RM22W0E___0000JO900000">
<ptxt>REMOVE FRONT DOOR WEATHERSTRIP LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184537" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 6 clips, and remove the upper part of the front door weatherstrip so that the black out tape can be removed.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039Z400MX_02_0001" proc-id="RM22W0E___0000JO500000">
<ptxt>REMOVE NO. 1 BLACK OUT TAPE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182610" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Pull back an edge of the black out tape and pull it parallel to the vehicle body to remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039Z400MX_02_0002" proc-id="RM22W0E___0000JO600000">
<ptxt>REMOVE NO. 3 BLACK OUT TAPE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182612" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Pull back an edge of the black out tape and pull it parallel to the vehicle body to remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039Z400MX_02_0003" proc-id="RM22W0E___0000JO700000">
<ptxt>REMOVE NO. 4 BLACK OUT TAPE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182614" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Pull back an edge of the black out tape and pull it parallel to the vehicle body to remove it.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>