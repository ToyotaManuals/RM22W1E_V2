<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000U" variety="S000U">
<name>1GR-FE COOLING</name>
<ttl id="12011_S000U_7C3JS_T00CV" variety="T00CV">
<name>THERMOSTAT</name>
<para id="RM000002BG802WX" category="A" type-id="80001" name-id="CO6C3-01" from="201301">
<name>REMOVAL</name>
<subpara id="RM000002BG802WX_02" type-id="01" category="01">
<s-1 id="RM000002BG802WX_02_0024" proc-id="RM22W0E___000011Z00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and screw.</ptxt>
<figure>
<graphic graphicname="B310791" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802WX_02_0025" proc-id="RM22W0E___000011Y00000">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
<figure>
<graphic graphicname="B310792" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the clip indicated by the arrow in the illustration to remove the front fender splash shield sub-assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802WX_02_0018" proc-id="RM22W0E___000011X00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts and No. 1 engine under cover.</ptxt>
<figure>
<graphic graphicname="A178461" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802WX_02_0019" proc-id="RM22W0E___000011W00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<figure>
<graphic graphicname="A274035E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Water Drain Cock Plug</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>for Type A:</ptxt>
<ptxt>Loosen the radiator drain cock plug and 2 cylinder block water drain cock plugs.</ptxt>
</s2>
<s2>
<ptxt>for Type B:</ptxt>
<ptxt>Loosen the radiator drain cock plug and cylinder block water drain cock plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the radiator cap. Then drain the coolant.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>for Type A:</ptxt>
<ptxt>Tighten the 2 cylinder block water drain cock plugs.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>for Type B:</ptxt>
<ptxt>Tighten the cylinder block water drain cock plug.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802WX_02_0020" proc-id="RM22W0E___00000Z800000">
<ptxt>REMOVE V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Raise the front of the V-bank cover to detach the 2 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<figure>
<graphic graphicname="A267633E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002BG802WX_02_0015" proc-id="RM22W0E___00006UL00000">
<ptxt>DISCONNECT NO. 2 RADIATOR HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223077" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the No. 2 radiator hose.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BG802WX_02_0004" proc-id="RM22W0E___00006UK00000">
<ptxt>REMOVE WATER INLET WITH THERMOSTAT</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>If the thermostat is not installed, cooling efficiency decreases. Even if the engine tends to overheat, do not remove the thermostat.</ptxt>
</atten4>
<s2>
<ptxt>Remove the 3 nuts, the water inlet with thermostat, and the gasket.</ptxt>
<figure>
<graphic graphicname="A223010" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>