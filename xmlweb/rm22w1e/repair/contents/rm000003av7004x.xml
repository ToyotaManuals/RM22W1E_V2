<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZA_T00SD" variety="T00SD">
<name>TURN SIGNAL FLASHER ASSEMBLY (for RHD)</name>
<para id="RM000003AV7004X" category="G" type-id="8000T" name-id="LE06V-01" from="201301">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM000003AV7004X_01" type-id="01" category="01">
<s-1 id="RM000003AV7004X_01_0001" proc-id="RM22W0E___0000J2300000">
<ptxt>INSPECT TURN SIGNAL FLASHER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E157045E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Measure the voltage according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<colspec colname="COL4" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (IG) - 7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>2 (LR) - 7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 (LL) - 7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4 (B) - 7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>5 (EL) - 7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer switch (Turn L) off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>9 V or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer switch (Turn L) on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>6 (ER) - 7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer switch (Turn R) off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>9 V or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer switch (Turn R) on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>8 (HAZ) - 7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>9 V or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<colspec colname="COL4" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>7 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, there may be a malfunction in the turn signal flasher.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>