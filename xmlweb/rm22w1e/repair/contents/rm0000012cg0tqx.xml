<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R3_T00K6" variety="T00K6">
<name>AUDIO AND VISUAL SYSTEM (w/ Navigation System)</name>
<para id="RM0000012CG0TQX" category="J" type-id="3016Z" name-id="AVF61-01" from="201308">
<dtccode/>
<dtcname>Illumination Circuit</dtcname>
<subpara id="RM0000012CG0TQX_01" type-id="60" category="03" proc-id="RM22W0E___0000BWM00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Power is supplied to the multi-media module receiver assembly and steering pad switch assembly illumination when the light control switch is in the tail or head position.</ptxt>
</content5>
</subpara>
<subpara id="RM0000012CG0TQX_02" type-id="32" category="03" proc-id="RM22W0E___0000BWN00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E248927E02" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000012CG0TQX_03" type-id="51" category="05" proc-id="RM22W0E___0000BWO00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>The vehicle is equipped with a Supplemental Restraint System (SRS) which includes components such as airbags. Before servicing (including removal or installation of parts), be sure to read the precaution for Supplemental Restraint System (See page <xref label="Seep01" href="RM000000KT10L4X"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuse for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM0000012CG0TQX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012CG0TQX_06_0001" proc-id="RM22W0E___0000BWP00001">
<testtitle>CHECK ILLUMINATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the illumination for the multi-media module receiver assembly, steering pad switch assembly or others (hazard switch etc.) comes on when the light control switch is turned to the head or tail position.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>Illumination comes on for all components except steering pad switch assembly.</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Illumination comes on for all components except multi-media module receiver assembly.</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>No illumination comes on (multi-media module receiver assembly, steering pad switch, hazard switch, heater control switch, etc.).</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000012CG0TQX_06_0003" fin="false">A</down>
<right ref="RM0000012CG0TQX_06_0011" fin="false">B</right>
<right ref="RM0000012CG0TQX_06_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0003" proc-id="RM22W0E___0000BWR00001">
<testtitle>CHECK HARNESS AND CONNECTOR (ILLUMINATION SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the spiral cable sub-assembly connector.</ptxt>
<figure>
<graphic graphicname="E172054E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E12-12 (IL+2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Light control switch in the tail or head position</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V </ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Spiral Cable Sub-assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000012CG0TQX_06_0002" fin="false">OK</down>
<right ref="RM0000012CG0TQX_06_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0002" proc-id="RM22W0E___0000BWQ00001">
<testtitle>INSPECT STEERING PAD SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the steering pad switch assembly (See page <xref label="Seep01" href="RM0000039SA01DX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the steering pad switch assembly (See page <xref label="Seep02" href="RM000003C4V014X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012CG0TQX_06_0009" fin="false">OK</down>
<right ref="RM0000012CG0TQX_06_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0009" proc-id="RM22W0E___0000BWS00001">
<testtitle>INSPECT SPIRAL CABLE SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the spiral cable sub-assembly (See page <xref label="Seep01" href="RM000002O8X01XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the spiral cable sub-assembly (See page <xref label="Seep02" href="RM000002O8V02TX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012CG0TQX_06_0018" fin="false">OK</down>
<right ref="RM0000012CG0TQX_06_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0018" proc-id="RM22W0E___0000BWU00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA NODULE RECEIVER ASSEMBLY - SPIRAL CABLE SUB-ASSEMBLY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F77 multi-media module receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E12 spiral cable sub-assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F77-14 (ILL+) - E12-12 (IL+2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-32 (SWG) - E12-4 (EAU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-14 (ILL+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-32 (SWG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012CG0TQX_06_0004" fin="true">OK</down>
<right ref="RM0000012CG0TQX_06_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0011" proc-id="RM22W0E___0000BWT00001">
<testtitle>CHECK HARNESS AND CONNECTOR (ILLUMINATION SIGNAL CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the multi-media module receiver assembly connector.</ptxt>
<figure>
<graphic graphicname="C250794E12" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F77-14 (ILL+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Light control switch in the tail or head position</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Multi-media Module Receiver Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000012CG0TQX_06_0004" fin="true">OK</down>
<right ref="RM0000012CG0TQX_06_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0007">
<testtitle>GO TO LIGHTING SYSTEM<xref label="Seep01" href="RM000002WKL01IX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0015">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0017">
<testtitle>REPLACE STEERING PAD SWITCH ASSEMBLY<xref label="Seep01" href="RM0000039SA01DX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0008">
<testtitle>REPLACE SPIRAL CABLE SUB-ASSEMBLY<xref label="Seep01" href="RM000002O8X01XX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0019">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0004">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000011BR0O2X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012CG0TQX_06_0020">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>