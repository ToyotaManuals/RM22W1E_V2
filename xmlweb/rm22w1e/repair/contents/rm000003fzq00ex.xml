<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LY_T00F1" variety="T00F1">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM000003FZQ00EX" category="J" type-id="802GX" name-id="CC3CV-02" from="201301" to="201308">
<dtccode/>
<dtcname>Washer Signal Circuit</dtcname>
<subpara id="RM000003FZQ00EX_01" type-id="60" category="03" proc-id="RM22W0E___00007D300000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The distance control ECU detects washer operation. Cruise control will be canceled by the distance control ECU if the windshield wipers operate in HI or LO mode.</ptxt>
</content5>
</subpara>
<subpara id="RM000003FZQ00EX_02" type-id="32" category="03" proc-id="RM22W0E___00007D400000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E158906E03" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003FZQ00EX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003FZQ00EX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003FZQ00EX_04_0001" proc-id="RM22W0E___00007D500000">
<testtitle>CHECK WASHER MOTOR (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the front washer operates (See page <xref label="Seep01" href="RM000002OXN01GX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Washer function is normal.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003FZQ00EX_04_0002" fin="false">OK</down>
<right ref="RM000003FZQ00EX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003FZQ00EX_04_0002" proc-id="RM22W0E___00007D600000">
<testtitle>CHECK HARNESS AND CONNECTOR (DISTANCE CONTROL ECU - WINDSHIELD WIPER SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E108 distance control ECU connector.</ptxt>
<figure>
<graphic graphicname="E158907E03" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the E15 windshield wiper switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E108-3 (WASH) - E15-3 (WF)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E15-3 (WF) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E108-3 (WASH) - E15-1 (WF)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E15-1 (WF) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003FZQ00EX_04_0003" fin="true">OK</down>
<right ref="RM000003FZQ00EX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003FZQ00EX_04_0003">
<testtitle>PROCEED TO NEXT CIRCUIT INSPECTION SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000PLS0DBX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FZQ00EX_04_0004">
<testtitle>GO TO WIPER AND WASHER SYSTEM<xref label="Seep01" href="RM000002M5S01EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003FZQ00EX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>