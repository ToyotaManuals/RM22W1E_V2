<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002K" variety="S002K">
<name>WIPER / WASHER</name>
<ttl id="12068_S002K_7C3YA_T00RD" variety="T00RD">
<name>WIPER AND WASHER SYSTEM (w/o Rain Sensor)</name>
<para id="RM00000397H011X" category="D" type-id="303F2" name-id="WW65T-01" from="201308">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM00000397H011X_z0" proc-id="RM22W0E___0000ISP00001">
<content5 releasenbr="1">
<step1>
<ptxt>WASHER LINKED OPERATION</ptxt>
<step2>
<ptxt>This system operates the front wipers at low speed immediately after a jet of washer fluid when the front washer switch is turned on for 0.3 seconds or more. The system operates the front wipers at low speed for approximately 2.2 seconds and then stops operation when the washer switch is turned on for 1.5 seconds or more.</ptxt>
</step2>
</step1>
<step1>
<ptxt>INTERMITTENT OPERATION</ptxt>
<step2>
<ptxt>The system operates the front wipers once in approximately 1.6 to 10.7 seconds when the front wiper switch is turned to the INT position. The intermittent time can be adjusted from 1.6 to 10.7 seconds by using the intermittent time adjust dial.</ptxt>
</step2>
<step2>
<ptxt>If the front wiper switch is turned to the INT position, current flows from the already charged capacitor C1 through terminals INT1 and INT2 of the front wiper switch to Tr1 (transistor). When Tr1 turns on, current flows from terminal +S of the front wiper switch to terminal +1 of the front wiper switch, to terminal +1 of the wiper motor, to the wiper motor and finally to ground, causing the wiper motor to operate. At the same time, current flows from capacitor C1 to terminal INT1 of the front wiper switch and then INT2. When the current flow from capacitor C1 ends, Tr1 turns off to stop the relay contact point and halt the wiper motor.</ptxt>
<figure>
<graphic graphicname="E252232E01" width="7.106578999in" height="6.791605969in"/>
</figure>
<ptxt>When the relay contact point turns off, capacitor C1 begins to charge again and Tr1 remains off until charging has been completed. This period corresponds to the intermittent time. When capacitor C1 is fully charged, Tr1 turns on and then the relay contact point turns on, causing the motor to operate again. This cycle is the intermittent operation.</ptxt>
<ptxt>The intermittent time can be adjusted by using the intermittent time adjust dial (variable resistor) to change the charge time of capacitor C1.</ptxt>
</step2>
</step1>
<step1>
<ptxt>REAR WIPER INTERMITTENT OPERATION (w/ Rear Wiper)</ptxt>
<step2>
<ptxt>When the rear wiper switch is turned to the INT position, current flows from the capacitor of the intermittent operation control circuit to turn on the transistor. Current flows from terminal +B of the rear wiper relay, to the relay coil, to the transistor, to terminal C1 of the rear wiper relay, to terminal C1R of the rear wiper control switch and finally to ground, causing the relay contact point to turn on.</ptxt>
<figure>
<graphic graphicname="E252231E02" width="7.106578999in" height="5.787629434in"/>
</figure>
<figure>
<graphic graphicname="E286271E03" width="7.106578999in" height="5.787629434in"/>
</figure>
<ptxt>When the relay contact point turns on, current flows from terminal +B of the rear wiper relay to the relay contact point, to terminal LM of the rear wiper relay, to terminal +1 of the rear wiper motor, to the rear wiper motor and finally to ground, causing the rear wiper motor to operate. The transistor turns off immediately after the rear wiper motor operation as the current flow from the capacitor ends, causing the relay contact point to turn off.</ptxt>
<ptxt>Even when the relay contact point turns off, current flows from terminal +B of the rear wiper motor, to the relay contact point in the rear wiper motor, to terminal S of the rear wiper motor, to terminal SM of the rear wiper relay, to the contact point of the rear wiper relay, to terminal LM of the rear wiper relay, to terminal +1 of the rear wiper motor and finally to ground until the rear wiper motor stops at the automatic stop position, causing the rear wiper motor to operate. Then the rear wiper motor stops at the automatic stop position as the relay contact point in the rear wiper motor turns off.</ptxt>
<ptxt>The capacitor in the intermittent operation control circuit is charged in approximately 12 seconds after the current flow ends. After the charge is completed, current starts flowing again to turn on the transistor, causing the relay contact point to turn on.</ptxt>
<ptxt>This cycle of current flow and charging described above is the intermittent operation.</ptxt>
</step2>
</step1>
<step1>
<ptxt>HEADLIGHT CLEANER SYSTEM (w/ Headlight Cleaner System)</ptxt>
<step2>
<ptxt>The headlight cleaner system operates when the headlights are on and the headlight cleaner switch is pushed on. It also operates when the headlights are on and the front wiper washer system is operating.</ptxt>
</step2>
<step2>
<ptxt>The headlight cleaner system also operates when the front washer switch is first operated with the headlights on after the ignition switch is turned to ON*.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Headlight Auto Leveling</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>WASHER NOZZLE HEATER SYSTEM (w/ Washer Nozzle Heater System)</ptxt>
<step2>
<ptxt>The washer nozzle heater system operates when the ambient temperature drops to 5°C (41°F) or less while the ignition switch is ON.</ptxt>
</step2>
<step2>
<ptxt>The washer nozzle heater system stops operating when the ignition switch is turned off, or when the ambient temperature reaches 6°C (42°F) or higher while the ignition switch is ON.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>