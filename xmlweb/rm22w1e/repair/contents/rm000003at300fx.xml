<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3OC_T00HF" variety="T00HF">
<name>ACTIVE HEIGHT CONTROL SUSPENSION</name>
<para id="RM000003AT300FX" category="C" type-id="802W1" name-id="SC0QW-07" from="201301" to="201308">
<dtccode>C1787</dtccode>
<dtcname>Forward/Rearward/Sideways G Sensor Communication Error</dtcname>
<subpara id="RM000003AT300FX_01" type-id="60" category="03" proc-id="RM22W0E___00009PL00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The suspension control ECU receives signals from the yaw rate sensor via CAN communication.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.25in"/>
<colspec colname="COL2" colwidth="3.03in"/>
<colspec colname="COL3" colwidth="2.80in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1787</ptxt>
</entry>
<entry valign="middle">
<ptxt>While the engine switch is on (IG), the yaw rate sensor malfunction signal is detected for 1 second or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Vehicle stability control system</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003AT300FX_02" type-id="51" category="05" proc-id="RM22W0E___00009PM00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before performing troubleshooting, inspect the connectors of related circuits.</ptxt>
</item>
<item>
<ptxt>If the suspension control ECU or height control sensor is replaced, the vehicle height offset calibration must be performed (See page <xref label="Seep01" href="RM000003AG300EX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000003AT300FX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003AT300FX_03_0001" proc-id="RM22W0E___00009PN00000">
<testtitle>CHECK DTC (VEHICLE STABILITY CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if a yaw rate sensor malfunction DTC is output under the vehicle stability control system (See page <xref label="Seep01" href="RM0000046KV00MX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003AT300FX_03_0002" fin="false">A</down>
<right ref="RM000003AT300FX_03_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003AT300FX_03_0002" proc-id="RM22W0E___00009PO00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001CU200KX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003AT300FX_03_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003AT300FX_03_0003" proc-id="RM22W0E___00009PP00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC C1787 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1787 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003AT300FX_03_0005" fin="true">A</down>
<right ref="RM000003AT300FX_03_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003AT300FX_03_0004">
<testtitle>GO TO VEHICLE STABILITY CONTROL SYSTEM (DIAGNOSTIC TROUBLE CODE CHART)<xref label="Seep01" href="RM0000045Z600TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003AT300FX_03_0005">
<testtitle>REPLACE SUSPENSION CONTROL ECU<xref label="Seep01" href="RM000003A0D00DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003AT300FX_03_0006">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>