<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12042_S001W" variety="S001W">
<name>G-BOOK</name>
<ttl id="12042_S001W_7C3RH_T00KK" variety="T00KK">
<name>G-BOOK SYSTEM</name>
<para id="RM000003YUE025X" category="D" type-id="3001B" name-id="GB04J-42" from="201301" to="201308">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM000003YUE025X_z0" proc-id="RM22W0E___0000COZ00000">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the following procedure to troubleshoot the G-BOOK system.</ptxt>
</item>
<item>
<ptxt>*: Use the GTS.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK PROBLEM SYMPTOM</testtitle>
<test1>
<ptxt>The G-BOOK system provides a variety of functions. Therefore, it is important to identify whether a malfunction is related to the communication system, including radio wave conditions, or any other systems. Carefully confirm the conditions that caused the malfunction in order to determine if a temporary problem with the radio wave signal condition caused the malfunction.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Displayed Message and Problem Symptom</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Cause</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A message is displayed temporarily indicating to try again after moving the vehicle because the center cannot be accessed, as the screen cannot be displayed due to a busy line, etc.</ptxt>
</entry>
<entry valign="middle">
<ptxt>This is caused when there is a problem with the radio wave signal conditions or when the base station is busy. This is not a malfunction (inform the customer that this is not a malfunction and ask them to use the system as is).</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A message is displayed temporarily indicating to try again after waiting for a while because the information cannot be retrieved due to a busy line, etc.</ptxt>
</entry>
<entry valign="middle">
<ptxt>This is caused when there is a problem with the radio wave signal conditions or when the base station is busy. This is not a malfunction (inform the customer that this is not a malfunction and ask them to use the system as is).</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A message is displayed temporarily indicating to try again after waiting for a while because the lines are busy.</ptxt>
</entry>
<entry valign="middle">
<ptxt>This is caused when there is a problem with the radio wave signal conditions or when the base station is busy. This is not a malfunction (inform the customer that this is not a malfunctions and ask them to use the system as is).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<test1>
<ptxt>Measure the battery voltage.</ptxt>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding to the next step.</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK CAN COMMUNICATION SYSTEM*</testtitle>
<test1>
<ptxt>Use the GTS to check if the CAN communication system is functioning normally (See page <xref label="Seep01" href="RM000001RSW03JX"/>). </ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>CAN communication system DTC is not output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system DTC is output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO CAN COMMUNICATION SYSTEM (See page <xref label="Seep02" href="RM000001RSO08CX"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to Problem Symptoms Table (See page <xref label="Seep03" href="RM000003YUH029X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Fault is not listed in Problem Symptoms Table.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fault is listed in Problem Symptoms Table.</ptxt>
<ptxt>CAN communication system DTC is output.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the symptom does not recur and no DTCs are output, attempt to reproduce the symptoms (See page <xref label="Seep04" href="RM000002V5U015X"/>).</ptxt>
</atten4>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to step 7</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PERFORM TROUBLESHOOTING BASED ON MALFUNCTION SYMPTOM</testtitle>
<test1>
<ptxt>Refer to Terminals of ECU (See page <xref label="Seep05" href="RM000003YUG024X"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK CIRCUIT</testtitle>
<test1>
<ptxt>Adjust, repair or replace as necessary.</ptxt>
<atten3>
<ptxt>If the multi-media module receiver assembly is replaced, perform the vehicle contract setting (See page <xref label="Seep06" href="RM000003Z0P021X"/>).</ptxt>
</atten3>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PERFORM CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>