<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000001DXS03XX" category="C" type-id="803OI" name-id="BCDWS-04" from="201308">
<dtccode>C1337</dtccode>
<dtcname>Diameter of the Tire is not Uniform</dtcname>
<subpara id="RM000001DXS03XX_01" type-id="60" category="03" proc-id="RM22W0E___0000AQ300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU (master cylinder solenoid) measures the speed of each wheel by receiving signals from the speed sensor. These signals are used for recognizing that all 4 wheels are operating properly. Therefore, all wheel signals must be equal.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1337</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The diameter of the 2 front wheels and 2 rear wheels is different.</ptxt>
</item>
<item>
<ptxt>There is a wheel speed sensor fault.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Speed sensor</ptxt>
</item>
<item>
<ptxt>Speed sensor rotor</ptxt>
</item>
<item>
<ptxt>Speed sensor circuit</ptxt>
</item>
<item>
<ptxt>Tire size</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001DXS03XX_02" type-id="51" category="05" proc-id="RM22W0E___0000AQ400001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration (See page <xref label="Seep01" href="RM000001DWZ01OX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>When DTCs C1401 to C1416 are output with DTC C1337, inspect and repair the trouble areas indicated by DTCs C1401 to C1416 first (See page <xref label="Seep02" href="RM000001DWQ01VX"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001DXS03XX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001DXS03XX_04_0001" proc-id="RM22W0E___0000AQ500001">
<testtitle>CHECK TIRE SIZE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check tire size and condition of all 4 wheels.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Condition of all 4 wheels is normal and tire size is same.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001DXS03XX_04_0005" fin="false">OK</down>
<right ref="RM000001DXS03XX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXS03XX_04_0005" proc-id="RM22W0E___0000AQ600001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00VX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of approximately 32 km/h (20 mph) or more for 60 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTCs are output (See page <xref label="Seep02" href="RM0000046KV00VX"/>).</ptxt>
<atten4>
<ptxt>Reinstall the sensors, connectors, etc. and restore the previous vehicle conditions before rechecking for DTCs.</ptxt>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXS03XX_04_0011" fin="true">A</down>
<right ref="RM000001DXS03XX_04_0006" fin="true">B</right>
<right ref="RM000001DXS03XX_04_0015" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXS03XX_04_0007">
<testtitle>REPLACE TIRES SO THAT ALL 4 TIRES ARE SAME SIZE</testtitle>
</testgrp>
<testgrp id="RM000001DXS03XX_04_0011">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXS03XX_04_0006">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXS03XX_04_0015">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>