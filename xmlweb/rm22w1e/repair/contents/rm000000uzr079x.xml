<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12065_S002H" variety="S002H">
<name>SLIDING ROOF / CONVERTIBLE</name>
<ttl id="12065_S002H_7C3XI_T00QL" variety="T00QL">
<name>SLIDING ROOF SYSTEM</name>
<para id="RM000000UZR079X" category="U" type-id="303FJ" name-id="RF07B-18" from="201301">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM000000UZR079X_z0" proc-id="RM22W0E___0000I8S00000">
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / Sliding Roof / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Sliding Roof</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Open Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof switch open signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: OPEN switch is pressed</ptxt>
<ptxt>OFF: OPEN switch is not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Close Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof switch close signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: CLOSE switch is pressed</ptxt>
<ptxt>OFF: CLOSE switch is not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Up Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof switch tilt up signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: UP switch is pressed</ptxt>
<ptxt>OFF: UP switch is not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Down Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof switch tilt down signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: DOWN switch is pressed</ptxt>
<ptxt>OFF: DOWN switch is not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hall IC1 Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof operation signal/Normal or Lock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal: Sliding roof motor is operating</ptxt>
<ptxt>Lock: Sliding roof motor is not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hall IC1 Pulse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof operation signal/Lo or Hi</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lo: Sliding roof motor is not operating</ptxt>
<ptxt>Hi: Sliding roof motor is operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hall IC2 Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof operation signal/Normal or Lock</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal: Sliding roof motor is operating</ptxt>
<ptxt>Lock: Sliding roof motor is not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hall IC2 Pulse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof operation signal/Lo or Hi</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lo: Sliding roof motor is not operating</ptxt>
<ptxt>Hi: Sliding roof motor is operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Courtesy</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver door courtesy light switch signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver door is open</ptxt>
<ptxt>OFF: Driver door is closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition (MPX)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch signal (MPX signal)/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Engine switch on (IG)</ptxt>
<ptxt>OFF: Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition (Direct Signal)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Engine switch on (IG)</ptxt>
<ptxt>OFF: Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Handle Lock Button</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Entry lock switch/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Entry lock switch is pressed</ptxt>
<ptxt>OFF: Entry lock switch is not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Key Close Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key-linked sliding roof close signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver door key cylinder is turned to lock position using key</ptxt>
<ptxt>OFF: Driver door key cylinder is not turned</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Key Open Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key-linked sliding roof open signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver door key cylinder is turned to unlock position using key</ptxt>
<ptxt>OFF: Driver door key cylinder is not turned</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless Open Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wireless sliding roof open signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Transmitter unlock switch is pressed</ptxt>
<ptxt>OFF: Transmitter unlock switch is not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless Close Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wireless sliding roof close signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Transmitter lock switch is pressed</ptxt>
<ptxt>OFF: Transmitter lock switch is not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Key Off Permission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key-off operation permit signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver door is not closed within 45 sec. after engine switch is turned OFF</ptxt>
<ptxt>OFF: Any status except "ON" status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Key Related (Open)*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key-linked sliding roof open operation status/Avail or Not Avail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Avail: Key-linked sliding roof open operation is available</ptxt>
<ptxt>Not Avail: Key-linked sliding roof open operation is not available</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Key Related (Close)*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key-linked sliding roof close operation status/Avail or Not Avail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Avail: Key-linked sliding roof close operation is available</ptxt>
<ptxt>Not Avail: Key-linked sliding roof close operation is not available</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless Key Rel (Open)*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Transmitter-linked sliding roof open operation status/Avail or Not Avail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Avail: Transmitter-linked sliding roof open operation is available</ptxt>
<ptxt>Not Avail: Transmitter-linked sliding roof open operation is not available</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless Key Rel (Close)*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Transmitter-linked sliding roof close operation status/Avail or Not Avail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Avail: Transmitter-linked sliding roof close operation is available</ptxt>
<ptxt>Not Avail: Transmitter-linked sliding roof close operation is not available</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Handle Button Close*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Entry lock switch-linked sliding roof close operation status/Avail or Not Avail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Avail: Entry lock switch-linked sliding roof close operation is available</ptxt>
<ptxt>Not Avail: Entry lock switch-linked sliding roof close operation is not available</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door Key Related Operation*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Direction for key-linked operation/Tilt or Slide</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tilt: Key-linked sliding roof operation operates tilt up or down</ptxt>
<ptxt>Slide: Key-linked sliding roof operation operates slide open or close</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless Key Rel Operation*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Direction for Transmitter-linked operation/Tilt or Slide</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tilt: Transmitter-linked sliding roof operation operates tilt up</ptxt>
<ptxt>Slide: Key-linked sliding roof operation operates slide open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Down Switch Failure(Current)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Down switch failure signal (Current)/Fail or Not Fail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fail: Sliding roof tilt down signal failure (Current)</ptxt>
<ptxt>Not Fail: Sliding roof tilt down signal not fail (Current)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Up Switch Failure(Current)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Up switch failure signal (Current)/Fail or Not Fail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fail: Sliding roof tilt up signal failure (Current)</ptxt>
<ptxt>Not Fail: Sliding roof tilt up signal not fail (Current)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Close Switch Failure(Current)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Close switch failure signal (Current)/Fail or Not Fail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fail: Sliding roof close signal failure (Current)</ptxt>
<ptxt>Not Fail: Sliding roof close signal not fail (Current)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Open Switch Failure(Current)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Open switch failure signal (Current)/Fail or Not Fail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fail: Sliding roof open signal failure (Current)</ptxt>
<ptxt>Not Fail: Sliding roof open signal not fail (Current)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Down Switch Failure(Past)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Down switch failure signal (Past)/Fail or Not Fail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fail: Sliding roof tilt down signal failure (Past)</ptxt>
<ptxt>Not Fail: Sliding roof tilt down signal not fail (Past)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Up Switch Failure(Past)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Up switch failure signal (Past)/Fail or Not Fail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fail: Sliding roof tilt up signal failure (Past)</ptxt>
<ptxt>Not Fail: Sliding roof tilt up signal not fail (Past)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Close Switch Failure(Past)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Close switch failure signal (Past)/Fail or Not Fail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fail: Sliding roof close signal failure (Past)</ptxt>
<ptxt>Not Fail: Sliding roof close signal not fail (Past)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Open Switch Failure(Past)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Open switch failure signal (Past)/Fail or Not Fail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fail: Sliding roof open signal failure (Past)</ptxt>
<ptxt>Not Fail: Sliding roof open signal not fail (Past)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Number of Trouble Codes</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Number of trouble codes / Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle">
<ptxt>Number of DTCs will be displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: w/ Remote Function</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs and actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / Sliding Roof / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, perform the Active Test.</ptxt>
<table pgwide="1">
<title>Sliding Roof</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Slide Roof</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Operate sliding roof SLIDE CLOSE/TILT UP</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clos/Up: Sliding roof SLIDE CLOSE or TILT UP operation occurs</ptxt>
<ptxt>OFF: Sliding roof is not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Be careful to avoid injuries as this test causes vehicle parts to move.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Slide Roof</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Operate sliding roof SLIDE OPEN/TILT DOWN</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open/Dwn: Sliding roof SLIDE OPEN or TILT DOWN operation occurs</ptxt>
<ptxt>OFF: Sliding roof is not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Be careful to avoid injuries as this test causes vehicle parts to move.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>