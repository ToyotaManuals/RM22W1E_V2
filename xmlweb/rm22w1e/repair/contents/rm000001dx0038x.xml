<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000001DX0038X" category="U" type-id="303FP" name-id="BC19D-74" from="201301" to="201308">
<name>FAIL-SAFE CHART</name>
<subpara id="RM000001DX0038X_z0" proc-id="RM22W0E___0000AD500000">
<content5 releasenbr="1">
<step1>
<ptxt>FAIL-SAFE OPERATION</ptxt>
<list1 type="unordered">
<item>
<ptxt>If there is a problem with sensor signals or hydraulic brake booster systems, the skid control ECU (master cylinder solenoid) prohibits power from being supplied to the hydraulic brake booster and informs the ECM of VSC system failure.</ptxt>
<ptxt>The hydraulic brake booster turns off each solenoid and the ECM stops its VSC (traction control signal), which is based on signals from the skid control ECU (master cylinder solenoid). As a result, the vehicle is controlled as if the ABS, TRC and VSC systems were not installed in the vehicle.</ptxt>
<ptxt>ABS control is prohibited, but EBD control continues to the extent that it is possible. If EBD control is impossible, the brake warning light comes on to warn the driver (See page <xref label="Seep01" href="RM000001DWV038X"/>).</ptxt>
</item>
<item>
<ptxt>If a system component has any malfunction before control begins, operation is prohibited. If a system component has any malfunction during control, the system will gradually stop operating so as not to trigger a sudden change in vehicle conditions.</ptxt>
<ptxt>If it is impossible to control the systems, the indicator lights come on or blink to inform the driver of termination of control of the systems (See page <xref label="Seep02" href="RM000001DWV038X"/>).</ptxt>
</item>
</list1>
<atten4>
<ptxt>If the hydraulic brake booster encounters a malfunction, brake performance is gradually lost and ABS, BA, A-TRC, VSC, hill-start assist control, Crawl Control and multi-terrain select controls are prohibited.</ptxt>
</atten4>
<step2>
<ptxt>ABS, EBD and BA system:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.83in"/>
<colspec colname="COL2" colwidth="4.25in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Area of Malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Method</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ABS</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS, BA, A-TRC, VSC, hill-start assist, CRAWL and multi-terrain select control are prohibited.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>BA system</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS, BA, TRC, VSC, hill-start assist, CRAWL and multi-terrain select control are prohibited.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>EBD system</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS, BA, EBD, A-TRC, VSC, hill-start assist, CRAWL and multi-terrain select control are prohibited.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>A-TRC, VSC, hill-start assist control and Crawl Control system:</ptxt>
<atten4>
<ptxt>The A-TRC, VSC, hill-start assist control and Crawl Control systems each prohibit different controls depending on the malfunctioning part.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.83in"/>
<colspec colname="COL2" colwidth="4.25in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Area of Malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Method</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Engine control system</ptxt>
<ptxt>(A-TRC and VSC systems)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Before control starts: Disables control</ptxt>
<ptxt>During control: Uses only brakes to perform control</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Brake control system</ptxt>
<ptxt>(VSC system)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Before control starts: Disables control</ptxt>
<ptxt>During control: Disables control</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Brake control system</ptxt>
<ptxt>(A-TRC, hill-start assist control and Crawl Control systems)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Before control starts: Disables control</ptxt>
<ptxt>During control: Disables control (by gradually ending control)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>SYSTEM TEMPERATURE PROTECTION OPERATION</ptxt>
<ptxt>If the A-TRC, hill-start assist control or Crawl Control system has been used constantly for a long time, the temperature of the hydraulic brake booster may increase excessively. If the temperature increases excessively, the buzzer sounds intermittently. If the temperature increases even more, the buzzer sounds for 3 seconds and the TRC OFF indicator light comes on.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Indicator Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A-TRC or hill-start assist control</ptxt>
</entry>
<entry valign="middle">
<ptxt>The TRC OFF indicator light comes on.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Crawl Control</ptxt>
</entry>
<entry valign="middle">
<ptxt>The TRC OFF indicator light comes on.</ptxt>
<ptxt>The CRAWL indicator light blinks, and then turns off.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the systems automatically return to normal operating conditions, the TRC OFF indicator light goes off. As this is not a malfunction, no DTC is stored.</ptxt>
</atten4>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>