<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001M" variety="S001M">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001M_7C3PO_T00IR" variety="T00IR">
<name>HYDRAULIC BRAKE BOOSTER (for LHD)</name>
<para id="RM00000171W029X" category="G" type-id="8000T" name-id="BR5TF-02" from="201301" to="201308">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM00000171W029X_01" type-id="01" category="01">
<s-1 id="RM00000171W029X_01_0001" proc-id="RM22W0E___0000AVE00000">
<ptxt>INSPECT BRAKE MASTER CYLINDER FLUID PRESSURE CHANGE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the battery voltage.</ptxt>
<figure>
<graphic graphicname="C172210E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Turn the ignition switch off and depress the brake pedal more than 20 times.</ptxt>
<atten4>
<ptxt>When pressure in the power supply system is released the reaction force becomes light and the stroke becomes longer.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the LSPV gauge (SST) and brake pedal effort gauge, and bleed air from the LSPV gauge (SST).</ptxt>
<sst>
<sstitem>
<s-number>09709-29018</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>When the booster does not operate:</ptxt>
<ptxt>Depress the brake pedal and check the fluid pressure.</ptxt>
<table>
<title>Standard Fluid Pressure with Pedal Depressing Force of 245 N (25 kgf, 55 lbf):</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Front Brake Pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear Brake Pressure</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2350 kPa (24.0 kgf/cm<sup>2</sup>, 341 psi) or more</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 kPa (0 kgf/cm<sup>2</sup>, 0 psi)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Standard Fluid Pressure with Pedal Depressing Force of 343 N (35 kgf, 77 lbf):</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Front Brake Pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear Brake Pressure</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3550 kPa (36.2 kgf/cm<sup>2</sup>, 515 psi) or more</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 kPa (0 kgf/cm<sup>2</sup>, 0 psi)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>When the booster operates:</ptxt>
<s3>
<ptxt>Turn the ignition switch on and wait until the pump motor has stopped.</ptxt>
</s3>
<s3>
<ptxt>Depress the brake pedal and check the fluid pressure.</ptxt>
<table>
<title>Standard Fluid Pressure with Pedal Depressing Force of 49 N (5 kgf, 11 lbf):</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Front Brake Pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear Brake Pressure</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1620 to 2820 kPa</ptxt>
<ptxt>(16.6 to 28.7 kgf/cm<sup>2</sup>, 235 to 408 psi)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1680 to 2880 kPa</ptxt>
<ptxt>(17.2 to 29.3 kgf/cm<sup>2</sup>, 244 to 417 psi)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Standard Fluid Pressure with Pedal Depressing Force of 98 N (10 kgf, 22 lbf):</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Front Brake Pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear Brake Pressure</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3840 to 5040 kPa</ptxt>
<ptxt>(39.2 to 51.3 kgf/cm<sup>2</sup>, 557 to 730 psi)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4000 to 5200 kPa</ptxt>
<ptxt>(40.8 to 53.0 kgf/cm<sup>2</sup>, 580 to 754 psi)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Standard Fluid Pressure with Pedal Depressing Force of 147 N (15 kgf, 33 lbf):</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Front Brake Pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear Brake Pressure</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>6200 to 7400 kPa</ptxt>
<ptxt>(63.3 to 75.4 kgf/cm<sup>2</sup>, 899 to 1073 psi)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6470 to 7670 kPa</ptxt>
<ptxt>(66.0 to 78.2 kgf/cm<sup>2</sup>, 939 to 1112 psi)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Standard Fluid Pressure with Pedal Depressing Force of 196 N (20 kgf, 44 lbf):</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Front Brake Pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear Brake Pressure</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>8500 to 9700 kPa</ptxt>
<ptxt>(86.7 to 98.9 kgf/cm<sup>2</sup>, 1233 to 1406 psi)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8890 to 10090 kPa</ptxt>
<ptxt>(90.7 to 102.8 kgf/cm<sup>2</sup>, 1290 to 1463 psi)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171W029X_01_0004" proc-id="RM22W0E___0000AVF00000">
<ptxt>INSPECT BRAKE MASTER CYLINDER OPERATION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the battery voltage.</ptxt>
<spec>
<title>Standard Voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Turn the ignition switch off and depress the brake pedal more than 20 times.</ptxt>
<atten4>
<ptxt>When pressure in the power supply system is released, the reaction force decreases and the stroke becomes longer.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Check that the brake pedal reaction force decreases.</ptxt>
<ptxt>If the pedal reaction force does not decrease, check and replace the brake line and brake master cylinder.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch on and check the pump motor operation noise. If the pump motor does not operate, check and replace the wire harness and brake booster pump assembly (See page <xref label="Seep01" href="RM00000171T028X"/>).</ptxt>
</s2>
<s2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch on.</ptxt>
</s2>
<s2>
<ptxt>Turn the GTS on.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Active Test.</ptxt>
</s2>
<s2>
<ptxt>Jack up and support the vehicle.</ptxt>
</s2>
<s2>
<ptxt>Release the parking brake lever.</ptxt>
</s2>
<s2>
<ptxt>Move the shift lever to N and check that the rear wheels can be rotated by hand.</ptxt>
</s2>
<s2>
<ptxt>Inspect the front VSC solenoid (SMCF) operation.</ptxt>
<s3>
<ptxt>Select "VSC/TRC Solenoid (SRCF)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Turn "VSC/TRC Solenoid (SRCF)" on with the GTS, depress the brake pedal with a stable force and check that the pedal cannot be depressed.</ptxt>
<ptxt>If the pedal can be depressed, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Release the brake pedal.</ptxt>
</s3>
<s3>
<ptxt>When the solenoid is off, depress the brake pedal again and check that the brake pedal can be depressed.</ptxt>
<ptxt>If the pedal cannot be depressed, replace the brake master cylinder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the front VSC solenoid (SREA) operation.</ptxt>
<s3>
<ptxt>Select "VSC/TRC Solenoid (SRCR)" and "VSC/TRC Solenoid (SRCF)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Turn "VSC/TRC Solenoid (SRCR)" and "VSC/TRC Solenoid (SRCF)" on simultaneously with the GTS, then depress the brake pedal with a stable force.</ptxt>
</s3>
<s3>
<ptxt>When the solenoids are on, check that the front wheels cannot be rotated by hand.</ptxt>
<ptxt>If the front wheels can be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheels quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheels as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoids are off, turn "VSC/TRC Solenoid (SRCF)" on again, and then depress the brake pedal. Then check the front wheels by rotating them by hand.</ptxt>
<ptxt>If the front wheels cannot be rotated, replace the brake master cylinder.</ptxt>
</s3>
<s3>
<ptxt>When "VSC/TRC Solenoid (SRCF)" is off, depress the brake pedal again and check that the brake pedal can be depressed.</ptxt>
<ptxt>If the pedal cannot be depressed, replace the brake master cylinder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the front ABS solenoid (SFRH) operation.</ptxt>
<s3>
<ptxt>Select "ABS Solenoid (SFRH)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Turn "ABS Solenoid (SFRH)" on with the GTS, then depress the brake pedal with a stable force.</ptxt>
</s3>
<s3>
<ptxt>When the solenoid is on, check the right front wheel by rotating it by hand.</ptxt>
<ptxt>If the right front wheel cannot be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheel quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheel as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoid is off, depress the brake pedal again and check that the right front wheel cannot be rotated by hand.</ptxt>
<ptxt>If the right front wheel can be rotated, replace the brake master cylinder.</ptxt>
<atten4>
<ptxt>When rotating the wheel quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheel as slowly as possible.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Inspect the front ABS solenoid (SFLH) operation.</ptxt>
<s3>
<ptxt>Select "ABS Solenoid (SFLH)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Turn "ABS Solenoid (SFLH)" on with the GTS, then depress the brake pedal with a stable force.</ptxt>
</s3>
<s3>
<ptxt>When the solenoid is on, check the left front wheel by rotating it by hand.</ptxt>
<ptxt>If the left front wheel cannot be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheel quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheel as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoid is off, depress the brake pedal again and check that the left front wheel cannot be rotated by hand.</ptxt>
<ptxt>If the left front wheel can be rotated, replace the brake master cylinder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the front ABS solenoid (SFRR) operation.</ptxt>
<s3>
<ptxt>Select "ABS Solenoid (SFRR)" and "ABS Solenoid (SFRH)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Depress the brake pedal with a stable force, then turn "ABS Solenoid (SFRR)" and "ABS Solenoid (SFRH)" on simultaneously with the GTS.</ptxt>
</s3>
<s3>
<ptxt>When the solenoids are on, check the right front wheel by rotating it by hand.</ptxt>
<ptxt>If the right front wheel cannot be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheel quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheel as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoids are off, depress the brake pedal again and check that the right front wheel cannot be rotated by hand.</ptxt>
<ptxt>If the right front wheel can be rotated, replace the brake master cylinder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the front ABS solenoid (SFLR) operation.</ptxt>
<s3>
<ptxt>Select "ABS Solenoid (SFLR)" and "ABS Solenoid (SFLH)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Depress the brake pedal with a stable force, then turn "ABS Solenoid (SFLR)" and "ABS Solenoid (SFLH)" on simultaneously with the GTS.</ptxt>
</s3>
<s3>
<ptxt>When the solenoids are on, check the left front wheel by rotating it by hand.</ptxt>
<ptxt>If the left front wheel cannot be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheel quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheel as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoids are off, depress the brake pedal again and check that the left front wheel cannot be rotated by hand.</ptxt>
<ptxt>If the left front wheel can be rotated, replace the brake master cylinder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the rear VSC solenoid (SREC) operation.</ptxt>
<s3>
<ptxt>Select "VSC/TRC Solenoid (SRMF)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Depress the brake pedal with a stable force, then turn "VSC/TRC Solenoid (SRMF)" on with the GTS.</ptxt>
</s3>
<s3>
<ptxt>Release the brake pedal when the solenoid is on, and check that the rear wheels cannot be rotated by hand.</ptxt>
<ptxt>If the rear wheels can be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheels quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheels as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoid is off, check the rear wheels by rotating them by hand.</ptxt>
<ptxt>If the rear wheels cannot be rotated, replace the brake master cylinder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the rear VSC solenoid (STR) operation.</ptxt>
<s3>
<ptxt>Select "VSC/TRC Solenoid (SRMR)" and "VSC/TRC Solenoid (SRMF)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Turn "VSC/TRC Solenoid (SRMR)" and "VSC/TRC Solenoid (SRMF)" on simultaneously with the GTS.</ptxt>
</s3>
<s3>
<ptxt>When the solenoids are on, check that the rear wheels cannot be rotated by hand.</ptxt>
<ptxt>If the rear wheels can be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheels quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheels as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoids are off, turn "VSC/TRC Solenoid (SRMF)" on again, and check the rear wheels by rotating them by hand.</ptxt>
<ptxt>If the rear wheels cannot be rotated, replace the brake master cylinder.</ptxt>
</s3>
<s3>
<ptxt>When "VSC/TRC Solenoid (SRMF)" is off, depress the brake pedal again and check that the rear wheels cannot be rotated by hand.</ptxt>
<ptxt>If the rear wheels can be rotated, replace the brake master cylinder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the rear ABS solenoid (SRRH) operation.</ptxt>
<s3>
<ptxt>Select "ABS Solenoid (SRRH)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Turn "ABS Solenoid (SRRH)" on with the GTS, then depress the brake pedal with a stable force.</ptxt>
</s3>
<s3>
<ptxt>When the solenoid is on, check the right rear wheel by rotating it by hand.</ptxt>
<ptxt>If the right rear wheel cannot be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheel quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheel as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoid is off, depress the brake pedal again and check that the right rear wheel cannot be rotated by hand.</ptxt>
<ptxt>If the right rear wheel can be rotated, replace the brake master cylinder.</ptxt>
<atten4>
<ptxt>When rotating the wheel quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheel as slowly as possible.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Inspect the rear ABS solenoid (SRLH) operation.</ptxt>
<s3>
<ptxt>Select "ABS Solenoid (SRLH)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Turn "ABS solenoid (SRLH)" on with the GTS, then depress the brake pedal with a stable force.</ptxt>
</s3>
<s3>
<ptxt>When the solenoid is on, check the left rear wheel by rotating it by hand.</ptxt>
<ptxt>If the left rear wheel cannot be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheel quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheel as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoid is off, depress the brake pedal again and check that the left rear wheel cannot be rotated by hand.</ptxt>
<ptxt>If the left rear wheel can be rotated, replace the brake master cylinder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the rear ABS solenoid (SRRR) operation.</ptxt>
<s3>
<ptxt>Select "ABS Solenoid (SRRR)" and "ABS Solenoid (SRRH)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Depress the brake pedal with a stable force, then turn "ABS Solenoid (SRRR)" and "ABS Solenoid (SRRH)" on simultaneously with the GTS.</ptxt>
</s3>
<s3>
<ptxt>When the solenoids are on, check the right rear wheel by rotating it by hand.</ptxt>
<ptxt>If the right rear wheel cannot be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheel too quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheel as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoids are off, depress the brake pedal again and check that the right rear wheel cannot be rotated by hand.</ptxt>
<ptxt>If the right rear wheel can be rotated, replace the brake master cylinder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the rear ABS solenoid (SRLR) operation.</ptxt>
<s3>
<ptxt>Select "ABS Solenoid (SRLR)" and "ABS Solenoid (SRLH)" on the GTS.</ptxt>
</s3>
<s3>
<ptxt>Depress the brake pedal with a stable force, then turn "ABS Solenoid (SRLR)" and "ABS Solenoid (SRLH)" on simultaneously with the GTS.</ptxt>
</s3>
<s3>
<ptxt>When the solenoids are on, check the left rear wheel by rotating it by hand.</ptxt>
<ptxt>If the left rear wheel cannot be rotated, replace the brake master cylinder.</ptxt>
<atten3>
<ptxt>When operating the solenoid continuously, set the interval to more than 20 seconds.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To protect the solenoids, the GTS turns off automatically 2 seconds after every solenoid has been turned on.</ptxt>
</item>
<item>
<ptxt>When rotating the wheel too quickly, the fail-safe function is activated and judgment cannot be made properly. Rotate the wheel as slowly as possible.</ptxt>
</item>
</list1>
</atten4>
</s3>
<s3>
<ptxt>When the solenoids are off, depress the brake pedal again and check that the left rear wheel cannot rotated by hand.</ptxt>
<ptxt>If the left rear wheel can be rotated, replace the brake master cylinder.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Lower the vehicle.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the GTS.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171W029X_01_0005" proc-id="RM22W0E___0000ASU00000">
<ptxt>CHECK FLUID LEVEL IN RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the ignition switch off and fully depress the brake pedal 40 times or more to release the pressure in the accumulator.</ptxt>
<atten4>
<ptxt>When the pressure in the accumulator is released, the pedal stroke will lengthen.</ptxt>
</atten4>
<figure>
<graphic graphicname="C208936" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Adjust the fluid level so that it is at the MAX line.</ptxt>
<atten4>
<ptxt>When the ignition switch is turned on, brake fluid is sent to the accumulator and the fluid level decreases by approximately 10 mm (0.394 in.) from the MAX line.</ptxt>
</atten4>
<spec>
<title>Brake Fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT3</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>