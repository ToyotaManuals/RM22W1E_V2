<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3ZC_T00SF" variety="T00SF">
<name>LIGHTING SYSTEM</name>
<para id="RM0000011T00HVX" category="C" type-id="8031J" name-id="LE63S-02" from="201308">
<dtccode>B2450</dtccode>
<dtcname>Initialization has not been Performed</dtcname>
<subpara id="RM0000011T00HVX_01" type-id="60" category="03" proc-id="RM22W0E___0000J9F00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Initialization of the headlight leveling ECU*1 or headlight swivel ECU*2 has not been performed.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Static Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
</list1>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B2450</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Initialization of the headlight leveling ECU has not been performed. (w/ Static Headlight Auto Leveling)</ptxt>
</item>
<item>
<ptxt>Initialization of the headlight swivel ECU assembly has not been performed. (w/ Dynamic Headlight Auto Leveling)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Headlight leveling ECU assembly (w/ Static Headlight Auto Leveling)</ptxt>
</item>
<item>
<ptxt>Headlight swivel ECU assembly (w/ Dynamic Headlight Auto Leveling)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000011T00HVX_03" type-id="51" category="05" proc-id="RM22W0E___0000J9G00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the headlight leveling ECU*1 or headlight swivel ECU*2, initialization of the ECU is necessary (except w/ Dynamic Headlight Auto Leveling) (See page <xref label="Seep01" href="RM000002CE4030X"/>).</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Static Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM0000011T00HVX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000011T00HVX_05_0001" proc-id="RM22W0E___0000J9H00001">
<testtitle>PERFORM INITIALIZATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform initialization of the headlight leveling ECU*1 or headlight swivel ECU*2 (See page <xref label="Seep01" href="RM000002CE4030X"/>).</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Static Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
</list1>
</test1>
</content6>
<res>
<down ref="RM0000011T00HVX_05_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000011T00HVX_05_0003" proc-id="RM22W0E___0000J9I00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002WP401YX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2450 is not output.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/ Static Headlight Auto Leveling)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/ Dynamic Headlight Auto Leveling) (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/ Dynamic Headlight Auto Leveling) (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the headlight leveling ECU*1 or headlight swivel ECU*2 stores DTC B2450 even though the ECU has been initialized, there may be a malfunction in the headlight leveling ECU*1 or headlight swivel ECU*2.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Static Headlight Auto Leveling</ptxt>
</item>
<item>
<ptxt>*2: w/ Dynamic Headlight Auto Leveling</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000011T00HVX_05_0002" fin="true">A</down>
<right ref="RM0000011T00HVX_05_0006" fin="true">B</right>
<right ref="RM0000011T00HVX_05_0004" fin="true">C</right>
<right ref="RM0000011T00HVX_05_0007" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM0000011T00HVX_05_0002">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM0000011T00HVX_05_0006">
<testtitle>REPLACE HEADLIGHT LEVELING ECU ASSEMBLY<xref label="Seep01" href="RM000003AV900DX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011T00HVX_05_0004">
<testtitle>REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY<xref label="Seep01" href="RM000003A5S007X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011T00HVX_05_0007">
<testtitle>REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY<xref label="Seep01" href="RM000003AV900DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>