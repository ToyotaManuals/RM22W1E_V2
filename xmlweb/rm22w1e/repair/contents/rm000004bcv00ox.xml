<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SC_T00LF" variety="T00LF">
<name>CAN COMMUNICATION SYSTEM (for LHD)</name>
<para id="RM000004BCV00OX" category="C" type-id="80312" name-id="NW5OD-01" from="201308">
<dtccode>U1117</dtccode>
<dtcname>Lost Communication with Accessory Gateway</dtcname>
<dtccode>B2779</dtccode>
<dtcname>Engine Starter Communication Malfunction</dtcname>
<subpara id="RM000004BCV00OX_01" type-id="60" category="03" proc-id="RM22W0E___0000DJ300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U1117</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is no communication from the accessory bus buffer</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Accessory bus buffer CAN branch wire or connector</ptxt>
</item>
<item>
<ptxt>Accessory bus buffer</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B2779</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is no communication from the accessory bus buffer</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Accessory bus buffer CAN branch wire or connector</ptxt>
</item>
<item>
<ptxt>Accessory bus buffer</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000004BCV00OX_02" type-id="32" category="03" proc-id="RM22W0E___0000DJ400001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C140665E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000004BCV00OX_03" type-id="51" category="05" proc-id="RM22W0E___0000DJ500001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Operating the ignition switch, any switches or any doors triggers related ECU and sensor communication with the CAN, which causes resistance variation.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000004BCV00OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004BCV00OX_04_0007" proc-id="RM22W0E___0000DJ800001">
<testtitle>PRECAUTION</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content6>
<res>
<down ref="RM000004BCV00OX_04_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004BCV00OX_04_0001" proc-id="RM22W0E___0000DJ600001">
<testtitle>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal before measuring the resistances of the main wire and the branch wire.</ptxt>
<atten2>
<ptxt>For vehicles with an SRS system:</ptxt>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000004BCV00OX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004BCV00OX_04_0002" proc-id="RM22W0E___0000DJ700001">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (ACCESSORY BUS BUFFER CAN BRANCH WIRE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E6 accessory bus buffer connector.</ptxt>
</test1>
<figure>
<graphic graphicname="E207355E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E6-1 (CAN+) - E6-2 (CAN-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>54 to 69 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Accessory Bus Buffer)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004BCV00OX_04_0005" fin="true">OK</down>
<right ref="RM000004BCV00OX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000004BCV00OX_04_0004">
<testtitle>REPAIR OR REPLACE ACCESSORY BUS BUFFER CAN BRANCH WIRE OR CONNECTOR (CAN+, CAN-)</testtitle>
</testgrp>
<testgrp id="RM000004BCV00OX_04_0005">
<testtitle>REPLACE ACCESSORY BUS BUFFER</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>