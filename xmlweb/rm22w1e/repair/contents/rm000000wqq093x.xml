<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000F" variety="S000F">
<name>1GR-FE FUEL</name>
<ttl id="12008_S000F_7C3GA_T009D" variety="T009D">
<name>FUEL INJECTOR</name>
<para id="RM000000WQQ093X" category="G" type-id="3001K" name-id="FU954-01" from="201301">
<name>INSPECTION</name>
<subpara id="RM000000WQQ093X_01" type-id="01" category="01">
<s-1 id="RM000000WQQ093X_01_0002" proc-id="RM22W0E___00005QX00000">
<ptxt>INSPECT FUEL INJECTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11.6 to 12.4 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the injector assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000WQQ093X_01_0003" proc-id="RM22W0E___00005QY00000">
<ptxt>INSPECT INJECTION VOLUME AND LEAKAGE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the injector injection.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>This test involves high-pressure fuel and electricity.</ptxt>
</item>
<item>
<ptxt>Take every precaution regarding safe handling of both the fuel and the electrical parts.</ptxt>
</item>
<item>
<ptxt>Perform this test in a safe area and avoid any sparks or flame.</ptxt>
</item>
<item>
<ptxt>Do not smoke.</ptxt>
</item>
</list1>
</atten2>
<s3>
<ptxt>Assemble SST as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A274079E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<sst>
<sstitem>
<s-number>09268-31014</s-number>
<s-subnumber>09268-41091</s-subnumber>
<s-subnumber>09268-41120</s-subnumber>
<s-subnumber>09268-41141</s-subnumber>
<s-subnumber>09268-41410</s-subnumber>
<s-subnumber>09268-41500</s-subnumber>
<s-subnumber>09268-41700</s-subnumber>
<s-subnumber>95336-08070</s-subnumber>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Pressure Regulator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>SST (Union)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>SST (Hose Band)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>SST (Fuel Tube Connector)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>SST (Hose)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry>
<ptxt>SST (Adapter)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry>
<ptxt>SST (Clamp)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry>
<ptxt>Fuel Injector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry>
<ptxt>Fuel Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry>
<ptxt>SST (T-Joint)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Discharge the fuel system pressure (See page <xref label="Seep01" href="RM0000028RU03VX"/>).</ptxt>
</s3>
<s3>
<ptxt>Disconnect the fuel inlet hose (fuel tube connector) from the fuel pipe.</ptxt>
</s3>
<s3>
<ptxt>Remove the bolt and disconnect the fuel pressure regulator from the fuel delivery pipe.</ptxt>
<atten3>
<ptxt>Do not disconnect the No. 2 fuel pipe from the fuel pressure regulator.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Connect SST to the fuel pipe.</ptxt>
<figure>
<graphic graphicname="A274080E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09268-31014</s-number>
<s-subnumber>09268-41500</s-subnumber>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>SST (Fuel Tube Connector)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Fuel Pipe</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten2>
<ptxt>Always read the precautions before connecting the fuel tube connector.</ptxt>
</atten2>
</s3>
<s3>
<ptxt>Connect SST (hose) to the fuel inlet of the pressure regulator with another SST (union) and the 2 bolts.</ptxt>
<figure>
<graphic graphicname="A274081E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09268-31014</s-number>
<s-subnumber>09268-41091</s-subnumber>
<s-subnumber>95336-08070</s-subnumber>
</sstitem>
</sst>
<torque>
<torqueitem>
<t-value1>9.0</t-value1>
<t-value2>92</t-value2>
<t-value3>80</t-value3>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Pressure Regulator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>SST (Hose)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>No. 2 Fuel Pipe Sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>SST (Union)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Connect the fuel return hose to the fuel outlet of the pressure regulator.</ptxt>
</s3>
<s3>
<ptxt>Install a new O-ring to the injector.</ptxt>
<figure>
<graphic graphicname="A274082E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Assemble SST as shown in the illustration.</ptxt>
<sst>
<sstitem>
<s-number>09268-31014</s-number>
<s-subnumber>09268-41141</s-subnumber>
<s-subnumber>09268-41410</s-subnumber>
<s-subnumber>09268-41700</s-subnumber>
<s-subnumber>95336-08070</s-subnumber>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>SST (Hose)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>SST (Hose Band)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>SST (Adapter)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Vinyl Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry>
<ptxt>SST (Clamp)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Pass SST (tie band) through the loop on the handle of SST (clamp) to secure SST (clamp) to SST (adapter).</ptxt>
<figure>
<graphic graphicname="A274083E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09268-31014</s-number>
<s-subnumber>09268-41800</s-subnumber>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Lock</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>SST (Tie Band)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>As SST (tie band) does not completely prevent SST (clamp) from becoming loose, do not subject the parts to any impacts while using them.</ptxt>
</item>
<item>
<ptxt>Before using SST (tie band), make sure that there is no deterioration, damage or cracks. If there are any abnormalities, replace SST.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>When removing SST (tie band), disengage the lock.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Check that SST (clamp) and SST (adapter) cannot be easily separated.</ptxt>
</s3>
<s3>
<ptxt>Install a vinyl tube to the injector.</ptxt>
</s3>
<s3>
<ptxt>Put the injector into a graduated cylinder.</ptxt>
<atten2>
<ptxt>Install a suitable vinyl tube to the injector to contain any gasoline spray.</ptxt>
</atten2>
</s3>
<s3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the ignition switch to ON.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Turn the GTS on.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Activate the Fuel Pump Speed Control.</ptxt>
</s3>
<s3>
<ptxt>Connect SST (EFI inspection wire H) to the injector and the battery for 15 seconds and measure the injection volume with a graduated cylinder. Test each injector 2 or 3 times.</ptxt>
<sst>
<sstitem>
<s-number>09842-30080</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A274084E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard injection volume</title>
<specitem>
<ptxt>71 to 86 cc (4.3 to 5.2 cu. in.) per 15 seconds</ptxt>
</specitem>
</spec>
<spec>
<title>Difference between each injector</title>
<specitem>
<ptxt>15 cc (0.9 cu. in.) or less</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>SST (EFI Inspection Wire H)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Connect</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that SST (EFI inspection wire H) is securely connected.</ptxt>
</item>
<item>
<ptxt>Always turn the voltage on and off on the battery side, not the fuel pump side.</ptxt>
</item>
</list1>
</atten3>
<ptxt>If the result is not as specified, replace the injector assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check for fuel leakage.</ptxt>
<s3>
<ptxt>In the condition above, disconnect the tester probes of SST (wire) from the battery and check for fuel leakage from the injector.</ptxt>
<spec>
<title>Standard fuel leakage</title>
<specitem>
<ptxt>1 drop or less every 12 minutes</ptxt>
</specitem>
</spec>
<ptxt>If the fuel leakage is not as specified, replace the injector assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>