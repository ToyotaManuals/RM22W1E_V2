<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM000003WPV0A0X" category="C" type-id="805L2" name-id="NS8WP-03" from="201308">
<dtccode>U0073</dtccode>
<dtcname>Sending Malfunction (Navigation to APGS)</dtcname>
<dtccode>U0100</dtccode>
<dtcname>Engine ECU Communication</dtcname>
<dtccode>U0126</dtccode>
<dtcname>Steering Sensor Communication</dtcname>
<dtccode>U0140</dtccode>
<dtcname>Lost Communication with Body Control Module</dtcname>
<dtccode>U0155</dtccode>
<dtcname>Meter ECU Communication</dtcname>
<dtccode>U1110</dtccode>
<dtcname>Clearance Sonar ECU Communication</dtcname>
<subpara id="RM000003WPV0A0X_01" type-id="60" category="03" proc-id="RM22W0E___0000CO000001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>These DTCs are stored when a malfunction occurs in the CAN communication circuit.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>U0073</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>CAN bus connection error</ptxt>
</entry>
<entry morerows="5" valign="middle" align="left">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U0100</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>CAN reception error</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U0126</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>CAN reception error</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U0140</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>CAN reception error</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U0155</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>CAN reception error</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U1110</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>CAN reception error</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003WPV0A0X_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003WPV0A0X_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003WPV0A0X_03_0004" proc-id="RM22W0E___0000CO200001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000011BU0PQX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003WPV0A0X_03_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WPV0A0X_03_0001" proc-id="RM22W0E___0000CO100001">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTCs are output again.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: <xref label="Seep01" href="RM000001RSO09SX"/>
</ptxt>
</item>
<item>
<ptxt>for RHD: <xref label="Seep02" href="RM000001RSO09TX"/>
</ptxt>
</item>
</list1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs are output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs are output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003WPV0A0X_03_0002" fin="true">A</down>
<right ref="RM000003WPV0A0X_03_0003" fin="true">B</right>
<right ref="RM000003WPV0A0X_03_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003WPV0A0X_03_0002">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WPV0A0X_03_0003">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003WPV0A0X_03_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>