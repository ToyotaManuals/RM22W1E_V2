<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WO_T00PR" variety="T00PR">
<name>AIR CONDITIONING SYSTEM (for Manual Air Conditioning System)</name>
<para id="RM000002LIN055X" category="S" type-id="3001E" name-id="AC0339-147" from="201308">
<name>DIAGNOSTIC TROUBLE CODE CHART</name>
<subpara id="RM000002LIN055X_z0" proc-id="RM22W0E___0000HA400001">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a trouble code is output during the DTC check, inspect the trouble areas listed for that code. For details of the code, refer to the "See page" below.</ptxt>
</item>
<item>
<ptxt>*: The air conditioning amplifier assembly stores the DTC of the respective malfunction that has occurred for the period of the time indicated in the brackets.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Air Conditioning Amplifier</title>
<tgroup cols="5" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="1.42in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="1.42in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.42in" colsep="0"/>
<colspec colnum="4" colname="4" colwidth="1.42in" colsep="0"/>
<colspec colnum="5" colname="5" colwidth="1.4in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>Memory*</ptxt>
</entry>
<entry colname="4" colsep="1" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1412</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Ambient Temperature Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(8.5 minutes or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>If ambient temperature is approximately -52.9 (- 63.22) or less, a malfunction code may be output even though the system is normal.</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002LJ405HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1413</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Evaporator Temperature Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(8.5 minutes or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002LJ504YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1422</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Compressor Lock Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000026BV08TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1423</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Pressure Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003E0P04CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1442</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Air Inlet Damper Control Servo Motor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(30 minutes or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002LLJ04KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1443</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Air Outlet Damper Control Servo Motor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(30 minutes or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002ROM04VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1445</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Cool Air Bypass Damper Control</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(30 minutes or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002ROM04WX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1446</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Air Mix Damper Control Servo Motor Circuit (Driver Side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(30 minutes or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002LLI04JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1497</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Communication Malfunction (Bus Ic)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001GFM03QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>U0100</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with ECM / PCM "A"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002LLO01EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>U0142</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with Body Control Module "B"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002LLO01EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>U0155</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with Instrument Panel Cluster (IPC) Control Module</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002LLO01EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>