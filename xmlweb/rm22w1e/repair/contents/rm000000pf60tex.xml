<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000PF60TEX" category="C" type-id="300XW" name-id="ES11I4-002" from="201301" to="201308">
<dtccode>P0120</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "A" Circuit Malfunction</dtcname>
<dtccode>P0122</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "A" Circuit Low Input</dtcname>
<dtccode>P0123</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "A" Circuit High Input</dtcname>
<dtccode>P0220</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "B" Circuit</dtcname>
<dtccode>P0222</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "B" Circuit Low Input</dtcname>
<dtccode>P0223</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "B" Circuit High Input</dtcname>
<dtccode>P2135</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "A" / "B" Voltage Correlation</dtcname>
<subpara id="RM000000PF60TEX_11" type-id="11" category="10">
<name>CAUTION / NOTICE / HINT</name>
</subpara>
<subpara id="RM000000PF60TEX_01" type-id="60" category="03" proc-id="RM22W0E___000021A00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The TP sensor is mounted on the throttle body, and detects the opening angle of the throttle valve. This sensor is a non-contact type. It uses Hall-effect elements in order to yield accurate signals even in extreme driving conditions, such as at high speeds as well as very low speeds.</ptxt>
<ptxt>The TP sensor has 2 sensor circuits, each of which transmits a signal, VTA1 and VTA2. VTA1 is used to detect the throttle valve angle and VTA2 is used to detect malfunctions in VTA1. The sensor signal voltages vary between 0 V and 5 V in proportion to the throttle valve opening angle, and are transmitted to the VTA terminals of the ECM.</ptxt>
<ptxt>As the valve closes, the sensor output voltage decreases and as the valve opens, the sensor output voltage increases. The ECM calculates the throttle valve opening angle according to these signals and controls the throttle actuator in response to driver inputs. These signals are also used in calculations such as air-fuel ratio correction, power increase correction and fuel cut control.</ptxt>
<figure>
<graphic graphicname="A208679E46" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0120</ptxt>
</entry>
<entry valign="middle">
<ptxt>The output voltage of VTA1 quickly fluctuates beyond the lower and upper malfunction thresholds for 2 seconds when the accelerator pedal is depressed (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Throttle Position (TP) sensor (built into throttle body)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0122</ptxt>
</entry>
<entry valign="middle">
<ptxt>The output voltage of VTA1 is 0.2 V or less for 2 seconds when the accelerator pedal is depressed (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>TP sensor (built into throttle body)</ptxt>
</item>
<item>
<ptxt>Short in VTA1 circuit</ptxt>
</item>
<item>
<ptxt>Open in VC circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0123</ptxt>
</entry>
<entry valign="middle">
<ptxt>The output voltage of VTA1 is 4.535 V or higher for 2 seconds when the accelerator pedal is depressed (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>TP sensor (built into throttle body)</ptxt>
</item>
<item>
<ptxt>Open in VTA1 circuit</ptxt>
</item>
<item>
<ptxt>Open in E2 circuit</ptxt>
</item>
<item>
<ptxt>Short between VC and VTA1 circuits</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0220</ptxt>
</entry>
<entry>
<ptxt>The output voltage of VTA2 quickly fluctuates beyond the lower and upper malfunction thresholds for 2 seconds when the accelerator pedal is depressed (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>TP sensor (built into throttle body)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0222</ptxt>
</entry>
<entry valign="middle">
<ptxt>The output voltage of VTA2 is 1.75 V or less for 2 seconds when the accelerator pedal is depressed (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>TP sensor (built into throttle body)</ptxt>
</item>
<item>
<ptxt>Short in VTA2 circuit</ptxt>
</item>
<item>
<ptxt>Open in VC circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0223</ptxt>
</entry>
<entry valign="middle">
<ptxt>The output voltage of VTA2 is 4.8 V or higher, and VTA1 is between 0.2 V and 2.02 V for 2 seconds when the accelerator pedal is depressed (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>TP sensor (built into throttle body)</ptxt>
</item>
<item>
<ptxt>Open in VTA2 circuit</ptxt>
</item>
<item>
<ptxt>Open in E2 circuit</ptxt>
</item>
<item>
<ptxt>Short between VC and VTA2 circuits</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2135</ptxt>
</entry>
<entry>
<ptxt>Either condition (a) or (b) is met (1 trip detection logic):</ptxt>
<ptxt>(a) The difference between the output voltages of VTA1 and VTA2 is 0.02 V or less for 0.5 seconds or more.</ptxt>
<ptxt>(b) The output voltage of VTA1 is 0.2 V or less and VTA2 is 1.75 V or less for 0.4 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short between VTA1 and VTA2 circuits</ptxt>
</item>
<item>
<ptxt>TP sensor (built into throttle body)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When any of these DTCs is output, check the throttle valve opening angle by entering the following menus: Powertrain / Engine and ECT / Data List / ETCS / Throttle Position No.1 and Throttle Position No.2.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Throttle Position No.1 is the VTA1 signal, and Throttle Position No.2 is the VTA2 signal.</ptxt>
<spec>
<title>Reference (Normal Condition)</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>Display Item</ptxt>
</entry>
<entry>
<ptxt>Accelerator Pedal Fully Released</ptxt>
</entry>
<entry>
<ptxt>Accelerator Pedal Fully Depressed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Throttle Position No.1</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.5 to 1.1 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.2 to 4.8 V</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Throttle Position No.2</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.1 to 3.1 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.6 to 5.0 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PF60TEX_02" type-id="64" category="03" proc-id="RM22W0E___000021B00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM uses the Throttle Position (TP) sensor to monitor the throttle valve opening angle. There are several checks that the ECM performs to confirm the proper operation of the TP sensor.</ptxt>
<list1 type="unordered">
<item>
<ptxt>A specific voltage difference is expected between the sensor terminals, VTA1 and VTA2, for each throttle valve opening angle. If the difference between VTA1 and VTA2 is incorrect, the ECM interprets this as a malfunction in the sensor, and stores DTC(s).</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>VTA1 and VTA2 each have a specific voltage range. If VTA1 or VTA2 is outside the normal operating range, the ECM interprets this as a malfunction in the sensor, and stores DTC(s).</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>VTA1 and VTA2 should never be close to the same voltage level. If VTA1 is within 0.02 V of VTA2, the ECM determines that there is a short circuit in the sensor, and stores DTC(s).</ptxt>
</item>
</list1>
<ptxt>If the malfunction is not repaired successfully, a DTC is stored 10 seconds after the engine is next started.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PF60TEX_07" type-id="62" category="03" proc-id="RM22W0E___000021C00000">
<name>FAIL-SAFE</name>
<content5 releasenbr="1">
<ptxt>When any of these DTCs, as well as other DTCs relating to ETCS (Electronic Throttle Control System) malfunctions are stored, the ECM enters fail-safe mode. During fail-safe mode, the ECM cuts the current to the throttle actuator, and the throttle valve is returned to a 7° throttle angle by the return spring. The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel cut) and ignition timing, in accordance with the accelerator pedal opening angle, to allow the vehicle to continue at a minimal speed. If the accelerator pedal is depressed firmly and gently, the vehicle can be driven slowly.</ptxt>
<ptxt>Fail-safe mode continues until a pass condition is detected, and the engine switch is then turned off.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PF60TEX_08" type-id="32" category="03" proc-id="RM22W0E___000021D00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A162761E07" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000PF60TEX_09" type-id="51" category="05" proc-id="RM22W0E___000021E00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>These DTCs relate to the Throttle Position (TP) sensor.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PF60TEX_10" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PF60TEX_10_0001" proc-id="RM22W0E___000021F00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (THROTTLE POSITION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / ETCS / Throttle Position No.1 and Throttle Position No.2.</ptxt>
</test1>
<test1>
<ptxt>Check the values displayed on the tester.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="6" align="center">
<colspec colname="COL1" colwidth="1.18in"/>
<colspec colname="COL2" colwidth="1.18in"/>
<colspec colname="COL3" colwidth="1.18in"/>
<colspec colname="COL4" colwidth="1.15in"/>
<colspec colname="COL5" colwidth="1.21in"/>
<colspec colname="COL6" colwidth="1.18in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>TP#1 (VTA1)</ptxt>
<ptxt>When Accelerator Pedal Released</ptxt>
</entry>
<entry valign="middle">
<ptxt>TP#2 (VTA2)</ptxt>
<ptxt>When Accelerator Pedal Released</ptxt>
</entry>
<entry valign="middle">
<ptxt>TP#1 (VTA1)</ptxt>
<ptxt>When Accelerator Pedal Depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>TP#2 (VTA2)</ptxt>
<ptxt>When Accelerator Pedal Depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>VC circuit open</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>E2 circuit open</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>0 to 0.2 V, </ptxt>
<ptxt>or 4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.1 to 3.1 V</ptxt>
<ptxt>(Fail-safe)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V, </ptxt>
<ptxt>or 4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.1 to 3.1 V</ptxt>
<ptxt>(Fail-safe)</ptxt>
</entry>
<entry valign="middle">
<ptxt>VTA1 circuit open or ground short</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>0.6 to 1.4 V</ptxt>
<ptxt>(Fail-safe)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V, </ptxt>
<ptxt>or 4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.6 to 1.4 V</ptxt>
<ptxt>(Fail-safe)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 0.2 V, </ptxt>
<ptxt>or 4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>VTA2 circuit open or ground short</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>0.5 to 1.1 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.1 to 3.1 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.2 to 4.8 V</ptxt>
<ptxt>(Not fail-safe)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.6 to 5.0 V</ptxt>
<ptxt>(Not fail-safe)</ptxt>
</entry>
<entry valign="middle">
<ptxt>TP sensor circuit normal</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<atten4>
<ptxt>TP#1 stands for Throttle Position No. 1, and TP#2 stands for Throttle Position No. 2.</ptxt>
</atten4>
</content6>
<res>
<down ref="RM000000PF60TEX_10_0002" fin="false">A</down>
<right ref="RM000000PF60TEX_10_0005" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000PF60TEX_10_0002" proc-id="RM22W0E___000021G00000">
<testtitle>CHECK HARNESS AND CONNECTOR (THROTTLE POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C3 throttle body connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C45 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.89in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C3-5 (VC) - C45-80 (VCTA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-6 (VTA) - C45-78 (VTA1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-4 (VTA2) - C45-77 (VTA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-3 (E2) - C45-79 (ETA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-5 (VC) or C45-80 (VCTA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-6 (VTA) or C45-78 (VTA1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-4 (VTA2) or C45-77 (VTA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PF60TEX_10_0003" fin="false">OK</down>
<right ref="RM000000PF60TEX_10_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PF60TEX_10_0003" proc-id="RM22W0E___000021H00000">
<testtitle>INSPECT ECM (VC VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A107895E87" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the C3 throttle body connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>C3-5 (VC) - C3-3 (E2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PF60TEX_10_0011" fin="false">OK</down>
<right ref="RM000000PF60TEX_10_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PF60TEX_10_0011" proc-id="RM22W0E___000021J00000">
<testtitle>REPLACE THROTTLE BODY ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the throttle body assembly (See page <xref label="Seep01" href="RM000002PQN035X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PF60TEX_10_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PF60TEX_10_0005" proc-id="RM22W0E___000021I00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (THROTTLE POSITION SENSOR DTCS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK14HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Allow the engine to idle for 15 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0120, P0122, P0123, P0220, P0222, P0223 and/or P2135 are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PF60TEX_10_0010" fin="true">A</down>
<right ref="RM000000PF60TEX_10_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PF60TEX_10_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000PF60TEX_10_0008">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PF60TEX_10_0009">
<testtitle>SYSTEM OK</testtitle>
</testgrp>
<testgrp id="RM000000PF60TEX_10_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>