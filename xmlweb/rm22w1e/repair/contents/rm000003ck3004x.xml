<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S002F" variety="S002F">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S002F_7C3WX_T00Q0" variety="T00Q0">
<name>ROOF HEADLINING (w/ Sliding Roof)</name>
<para id="RM000003CK3004X" category="A" type-id="8000E" name-id="IT0KV-04" from="201308">
<name>REASSEMBLY</name>
<subpara id="RM000003CK3004X_01" type-id="01" category="01">
<s-1 id="RM000003CK3004X_01_0007" proc-id="RM22W0E___0000BS100001">
<ptxt>INSTALL ANTENNA CORD SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<s3>
<ptxt>Apply new double-sided tape as shown in the illustration.</ptxt>
<atten4>
<ptxt>Attach double-sided tape to the hatched areas shown in the illustration below.</ptxt>
</atten4>
<figure>
<graphic graphicname="B188592E01" width="7.106578999in" height="9.803535572in"/>
</figure>
</s3>
<s3>
<ptxt>Align the red tape wound around the antenna cord with the V markings on the roof headlining and the notch at the rear of the headlining, and attach the antenna cord to the double-sided tape.</ptxt>
</s3>
<s3>
<ptxt>Attach the 2 clamps and fit the antenna into the notch.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<s3>
<ptxt>Apply new double-sided tape as shown in the illustration.</ptxt>
<atten4>
<ptxt>Attach double-sided tape to the hatched areas shown in the illustration below.</ptxt>
<figure>
<graphic graphicname="B184459E02" width="7.106578999in" height="9.803535572in"/>
</figure>
</atten4>
</s3>
<s3>
<ptxt>Align the red tape wound around the antenna cord with the V markings on the roof headlining and the notch at the rear of the headlining, and attach the antenna cord to the double-sided tape.</ptxt>
</s3>
<s3>
<ptxt>Attach the 4 clamps and fit the antenna into the notch.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000003CK3004X_01_0002" proc-id="RM22W0E___0000HRP00001">
<ptxt>INSTALL NO. 1 ROOF WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place double-sided tape along the center of the harness marking and washer marking.</ptxt>
<atten4>
<ptxt>Make sure that the double-sided tape is not peeling off or misaligned.</ptxt>
</atten4>
<figure>
<graphic graphicname="B182439E02" width="7.106578999in" height="5.787629434in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the peeling paper from the double-sided tape while not touching the adhesive side.</ptxt>
</s2>
<s2>
<ptxt>Align the triangle mark of the roof wire protector with the V mark of the headlining as shown in the illustration, and then attach the roof wire protector.</ptxt>
</s2>
<s2>
<ptxt>Attach each clamp as shown in the illustration.</ptxt>
<atten4>
<ptxt>The positions of the clamps at the front of the roof headlining are shown in illustration D.</ptxt>
</atten4>
<figure>
<graphic graphicname="B188448E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Align the roof wire and washer hose with the V marks of the headlining as shown in the illustration, and attach them to the headlining.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Attach the washer hose from the front to the point labeled A, and attach the washer hose from the rear to the point labeled B as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>Attach the extra length of the washer hose to the part labeled C in the illustration.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Turn the visor connectors approximately 90° clockwise to install them to the roof headlining.</ptxt>
<figure>
<graphic graphicname="B188449E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</s2>
<s2>
<ptxt>Place tape at the wire harness marking and washer marking locations as shown in the illustration to fix the roof wire and washer hose in place.</ptxt>
<atten4>
<ptxt>Apply sufficient pressure when placing tape.</ptxt>
</atten4>
<figure>
<graphic graphicname="B182443E02" width="7.106578999in" height="7.795582503in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003CK3004X_01_0003" proc-id="RM22W0E___0000HRQ00001">
<ptxt>INSTALL NO. 4 ROOF SILENCER PAD</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182434E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Align the silencer marking on the roof headlining with the roof silencer pad and install the pad using double-sided tape as shown in the illustration.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003CK3004X_01_0004" proc-id="RM22W0E___0000HRR00001">
<ptxt>INSTALL NO. 1 ROOF SILENCER PAD</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182433E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Align the silencer marking on the roof headlining with the roof silencer pad and install the pad using double-sided tape as shown in the illustration.  </ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003CK3004X_01_0005" proc-id="RM22W0E___0000HRS00001">
<ptxt>INSTALL NO. 2 AIR OUTLET GRILLE ASSEMBLY (w/ Rear Air Conditioning System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182435" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedures to install the air outlet grille on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 6 claws to install the air outlet grille.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003CK3004X_01_0006" proc-id="RM22W0E___0000HRT00001">
<ptxt>INSTALL NO. 1 AIR OUTLET GRILLE ASSEMBLY (w/ Rear Air Conditioning System)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the No. 2 air outlet grille.</ptxt>
</atten4>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>