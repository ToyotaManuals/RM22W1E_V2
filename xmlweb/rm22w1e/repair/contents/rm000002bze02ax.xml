<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S000Z" variety="S000Z">
<name>1GR-FE LUBRICATION</name>
<ttl id="12012_S000Z_7C3KH_T00DK" variety="T00DK">
<name>OIL PUMP</name>
<para id="RM000002BZE02AX" category="A" type-id="80001" name-id="LU3VO-03" from="201308">
<name>REMOVAL</name>
<subpara id="RM000002BZE02AX_01" type-id="01" category="01">
<s-1 id="RM000002BZE02AX_01_0115" proc-id="RM22W0E___00006XR00001">
<ptxt>REMOVE ENGINE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000002B5L01UX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0158" proc-id="RM22W0E___000049Q00001">
<ptxt>REMOVE AIR TUBE (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 3 air hose.</ptxt>
<figure>
<graphic graphicname="A270147" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for Bank 1 Side:</ptxt>
<ptxt>Remove the bolt and disconnect the air tube assembly from the emission control valve set.</ptxt>
<figure>
<graphic graphicname="A270144" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>for Bank 2 Side:</ptxt>
<ptxt>Remove the 2 bolts and disconnect the air tube assembly from the No. 2 emission control valve set.</ptxt>
<figure>
<graphic graphicname="A270145" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002BZE02AX_01_0155" proc-id="RM22W0E___000011H00000">
<ptxt>REMOVE NO. 1 EMISSION CONTROL VALVE SET (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A267738" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the No. 1 emission control valve set connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 1 air hose from the No. 1 emission control valve set.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223570" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 nuts and No. 1 emission control valve set.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BZE02AX_01_0156" proc-id="RM22W0E___000011I00000">
<ptxt>REMOVE NO. 2 EMISSION CONTROL VALVE SET (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223562" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the No. 2 emission control valve set connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 3 air hose.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223564" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 nuts and No. 2 emission control valve set.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BZE02AX_01_0142" proc-id="RM22W0E___000011E00000">
<ptxt>REMOVE IGNITION COIL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 6 ignition coil connectors.</ptxt>
<figure>
<graphic graphicname="A272605E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>Bank 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>Bank 2</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 6 bolts and 6 ignition coils.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BZE02AX_01_0098" proc-id="RM22W0E___00004BN00001">
<ptxt>REMOVE ENGINE OIL LEVEL DIPSTICK GUIDE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the engine oil level dipstick.</ptxt>
<figure>
<graphic graphicname="A270151" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and engine oil level dipstick guide.</ptxt>
</s2>
<s2>
<ptxt>Remove the O-ring from the engine oil level dipstick guide.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0159" proc-id="RM22W0E___00004CF00001">
<ptxt>REMOVE WATER INLET HOUSING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the throttle body connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 5 water by-pass hoses.</ptxt>
<figure>
<graphic graphicname="A273503" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the oil cooler hose.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 2 oil cooler hose.</ptxt>
</s2>
<s2>
<ptxt>Remove the 5 bolts and water inlet.</ptxt>
<figure>
<graphic graphicname="A223012" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the water outlet pipe.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the water pump.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BZE02AX_01_0137" proc-id="RM22W0E___00004BO00001">
<ptxt>REMOVE WATER BY-PASS PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 hoses.</ptxt>
<figure>
<graphic graphicname="A223013" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and water by-pass pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0101" proc-id="RM22W0E___00004BP00001">
<ptxt>REMOVE NO. 1 IDLER PULLEY SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and No. 1 idler pulley.</ptxt>
<figure>
<graphic graphicname="A158544" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0153" proc-id="RM22W0E___00004BQ00001">
<ptxt>REMOVE NO. 2 IDLER PULLEY SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223006" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and 2 No. 2 idler pulleys.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BZE02AX_01_0099" proc-id="RM22W0E___00004BM00001">
<ptxt>REMOVE V-RIBBED BELT TENSIONER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and V-ribbed belt tensioner.</ptxt>
<figure>
<graphic graphicname="A222180" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0157" proc-id="RM22W0E___00004CC00000">
<ptxt>REMOVE OIL FILTER ELEMENT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect a hose with an inside diameter of 15 mm (0.591 in.) to the pipe.</ptxt>
<figure>
<graphic graphicname="A094910E09" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hose</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the oil filter drain plug.</ptxt>
<figure>
<graphic graphicname="A222100" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the pipe to the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A272717E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hose</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>If the O-ring is removed with the drain plug, install the O-ring together with the pipe.</ptxt>
</atten3>
<atten4>
<ptxt>Use a container to catch the draining oil.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Check that oil is drained from the oil filter. Then, disconnect the pipe and remove the O-ring as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A222102" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, remove the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A222103E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09228-06501</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Filter Bracket Clip</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not remove the oil filter bracket clip.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the oil filter element and O-ring from the oil filter cap.</ptxt>
<figure>
<graphic graphicname="A153538E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be sure to remove the O-ring (for the cap) by hand, without using any tools, to prevent damage to the groove for the O-ring in the cap.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002BZE02AX_01_0121" proc-id="RM22W0E___000046A00001">
<ptxt>REMOVE OIL FILTER BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts, bolt, oil filter bracket and gasket.</ptxt>
<figure>
<graphic graphicname="A268548" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0102" proc-id="RM22W0E___000046800001">
<ptxt>REMOVE CRANKSHAFT PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, hold the crankshaft pulley and loosen the pulley bolt. Continue to loosen the bolt until only 2 or 3 threads are screwed into the crankshaft.</ptxt>
<figure>
<graphic graphicname="A072947E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09213-54015</s-number>
<s-subnumber>91651-60855</s-subnumber>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using the pulley set bolt and SST, remove the crankshaft pulley and pulley bolt.</ptxt>
<figure>
<graphic graphicname="A072948E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-50013</s-number>
<s-subnumber>09951-05010</s-subnumber>
<s-subnumber>09952-05010</s-subnumber>
<s-subnumber>09953-05020</s-subnumber>
<s-subnumber>09954-05031</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0138" proc-id="RM22W0E___00004CA00001">
<ptxt>REMOVE NO. 1 OIL PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 oil pipe unions, oil control valve filter LH, 3 gaskets and No. 1 oil pipe.</ptxt>
<figure>
<graphic graphicname="A223691" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not touch the mesh when removing the oil control valve filter.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0139" proc-id="RM22W0E___00004CB00001">
<ptxt>REMOVE NO. 2 OIL PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 oil pipe unions, oil control valve filter RH, 3 gaskets and No. 2 oil pipe.</ptxt>
<figure>
<graphic graphicname="A223692" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not touch the mesh when removing the oil control valve filter.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0154" proc-id="RM22W0E___00004ED00001">
<ptxt>DISCONNECT FUEL PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and disconnect the fuel pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0097" proc-id="RM22W0E___00004E300001">
<ptxt>REMOVE CYLINDER HEAD COVER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 12 bolts, seal washer, cylinder head cover and gasket.</ptxt>
<figure>
<graphic graphicname="A223666" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure the removed parts are returned to the same places they were removed from.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the 3 gaskets.</ptxt>
<figure>
<graphic graphicname="A223665" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0096" proc-id="RM22W0E___00004E200001">
<ptxt>REMOVE CYLINDER HEAD COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 12 bolts, seal washer, cylinder head cover and gasket.</ptxt>
<figure>
<graphic graphicname="A223669" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure the removed parts are returned to the same places they were removed from.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the 3 gaskets.</ptxt>
<figure>
<graphic graphicname="A223668" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0125" proc-id="RM22W0E___00004C500001">
<ptxt>REMOVE NO. 2 OIL PAN SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="A174340" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure the removed parts are returned to the same places they were removed from.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Insert the blade of an oil pan seal cutter between the oil pans. Cut through the applied sealer and remove the No. 2 oil pan.</ptxt>
<figure>
<graphic graphicname="A174341E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Pan Seal Cutter</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to damage the contact surfaces of the oil pans.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0126" proc-id="RM22W0E___00004C600001">
<ptxt>REMOVE OIL STRAINER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and oil strainer.</ptxt>
<figure>
<graphic graphicname="A239204" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0127" proc-id="RM22W0E___00004C700001">
<ptxt>REMOVE OIL PAN SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 17 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="A222173" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure the removed parts are returned to the same places they were removed from.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the oil pan by prying between the oil pan and cylinder block as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A221168E05" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>LH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>RH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pry</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to damage the contact surfaces of the cylinder block and oil pan.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the 3 O-rings from the timing chain cover.</ptxt>
<figure>
<graphic graphicname="A222122" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0128" proc-id="RM22W0E___00004C800001">
<ptxt>REMOVE TIMING CHAIN COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 26 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="A221169" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the timing chain cover by prying between the timing chain cover and cylinder head or cylinder block with a screwdriver.</ptxt>
<figure>
<graphic graphicname="A221170" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>Be careful not to damage the contact surfaces of the timing chain cover, cylinder block and cylinder head.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the oil pump gasket from the cylinder block.</ptxt>
<figure>
<graphic graphicname="A221404" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BZE02AX_01_0129" proc-id="RM22W0E___00004C900001">
<ptxt>REMOVE FRONT CRANKSHAFT OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver and wooden block, pry out the oil seal.</ptxt>
<atten3>
<ptxt>Do not damage the surface of the oil seal press fit hole.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>