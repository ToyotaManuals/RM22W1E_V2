<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001G" variety="S001G">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001G_7C3OM_T00HP" variety="T00HP">
<name>SUSPENSION CONTROL CYLINDER</name>
<para id="RM000003B2000AX" category="A" type-id="80001" name-id="SC0SG-02" from="201301">
<name>REMOVAL</name>
<subpara id="RM000003B2000AX_01" type-id="01" category="01">
<s-1 id="RM000003B2000AX_01_0002" proc-id="RM22W0E___00009XF00000">
<ptxt>REMOVE TAILPIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>Remove the tailpipe (See page <xref label="Seep01" href="RM000002HA700MX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1UR-FE:</ptxt>
<ptxt>Remove the tailpipe (See page <xref label="Seep02" href="RM00000456J00JX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 3UR-FE:</ptxt>
<ptxt>Remove the tailpipe (See page <xref label="Seep03" href="RM000002HA002FX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1VD-FTV:</ptxt>
<ptxt>Remove the tailpipe (See page <xref label="Seep04" href="RM000003B0N00FX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2000AX_01_0003" proc-id="RM22W0E___00009XG00000">
<ptxt>REMOVE CENTER EXHAUST PIPE ASSEMBLY</ptxt>
<content1 releasenbr="4">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>Remove the center exhaust pipe (See page <xref label="Seep01" href="RM000002HA700MX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1UR-FE:</ptxt>
<ptxt>Remove the center exhaust pipe (See page <xref label="Seep02" href="RM00000456J00JX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 3UR-FE:</ptxt>
<ptxt>Remove the center exhaust pipe (See page <xref label="Seep03" href="RM000002HA002FX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1VD-FTV:</ptxt>
<ptxt>Remove the center exhaust pipe (See page <xref label="Seep04" href="RM000003B0N00FX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2000AX_01_0004" proc-id="RM22W0E___00009SG00000">
<ptxt>REMOVE HEIGHT CONTROL UNIT INSULATOR
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172582" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 3 bolts and insulator from the control unit.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0021" proc-id="RM22W0E___00009RM00000">
<ptxt>DISCHARGE SUSPENSION FLUID PRESSURE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C172938" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Connect a hose to the bleeder plug for the height control accumulator and loosen the bleeder plug.</ptxt>
</s2>
<s2>
<ptxt>Discharge the suspension fluid pressure.</ptxt>
</s2>
<s2>
<ptxt>After the fluid pressure has dropped and oil has drained out, tighten the bleeder plug and remove the hose.</ptxt>
<torque>
<torqueitem>
<t-value1>6.9</t-value1>
<t-value2>70</t-value2>
<t-value3>61</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0006" proc-id="RM22W0E___00009SE00000">
<ptxt>REMOVE NO. 7 HEIGHT CONTROL TUBE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172583" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a union nut wrench, remove the control tube from the control valve and pump accumulator. </ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0007" proc-id="RM22W0E___00009SF00000">
<ptxt>REMOVE SUSPENSION CONTROL PUMP ACCUMULATOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172584" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 3 nuts and pump accumulator.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0008" proc-id="RM22W0E___00009VM00000">
<ptxt>REMOVE CLAMP
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clamp from the 3 height control tubes.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0009" proc-id="RM22W0E___00009VN00000">
<ptxt>REMOVE NO. 6 HEIGHT CONTROL TUBE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174920E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Using a union nut wrench, remove the No. 6 height control tube from the control valve and center cylinder.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0010" proc-id="RM22W0E___00009VO00000">
<ptxt>REMOVE NO. 6 HEIGHT CONTROL TUBE LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a union nut wrench, remove the No. 6 height control tube LH from the control valve and center cylinder.</ptxt>
<figure>
<graphic graphicname="C174928E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0011" proc-id="RM22W0E___00009VP00000">
<ptxt>REMOVE NO. 5 HEIGHT CONTROL TUBE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174919E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Using a union nut wrench, remove the No. 5 height control tube LH from the control valve and center cylinder.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0012" proc-id="RM22W0E___00009VQ00000">
<ptxt>REMOVE NO. 5 HEIGHT CONTROL TUBE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a union nut wrench, remove the No. 5 height control tube from the control valve and center cylinder.</ptxt>
<figure>
<graphic graphicname="C174918E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0013" proc-id="RM22W0E___00009VR00000">
<ptxt>DISCONNECT REAR NO. 4 HEIGHT CONTROL TUBE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C174917E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the union bolt and gasket, then disconnect the rear No. 4 height control tube from the control valve.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0014" proc-id="RM22W0E___00009VS00000">
<ptxt>REMOVE NO. 1 HEIGHT CONTROL VALVE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172613" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 3 nuts and control valve.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolt holders and 3 bolt cushions from the control valve.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B2000AX_01_0017" proc-id="RM22W0E___00009XJ00000">
<ptxt>DISCONNECT NO. 3 HEIGHT CONTROL TUBE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174886" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the union bolt and gasket, then disconnect the No. 3 height control tube from the control cylinder.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2000AX_01_0018" proc-id="RM22W0E___00009XK00000">
<ptxt>DISCONNECT NO. 4 HEIGHT CONTROL TUBE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174887" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the union bolt and gasket, then disconnect the No. 4 height control tube from the control cylinder.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2000AX_01_0019" proc-id="RM22W0E___00009XL00000">
<ptxt>DISCONNECT NO. 4 HEIGHT CONTROL TUBE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174888" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the union bolt and gasket, then disconnect the No. 4 height control tube from the control cylinder.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2000AX_01_0020" proc-id="RM22W0E___00009XM00000">
<ptxt>DISCONNECT NO. 3 HEIGHT CONTROL TUBE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C174889" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the union bolt and gasket, then disconnect the No. 3 height control tube from the control cylinder.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2000AX_01_0015" proc-id="RM22W0E___00009XH00000">
<ptxt>REMOVE HEIGHT CONTROL BRACKET WITH CYLINDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172670" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and bracket with cylinder.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2000AX_01_0016" proc-id="RM22W0E___00009XI00000">
<ptxt>REMOVE CENTER SUSPENSION CONTROL CYLINDER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172671" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt> Remove the 2 nuts and cylinder from the bracket.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>