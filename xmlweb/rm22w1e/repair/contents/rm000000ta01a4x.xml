<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3DW_T006Z" variety="T006Z">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM000000TA01A4X" category="C" type-id="302IA" name-id="ES169L-001" from="201301" to="201308">
<dtccode>P0617</dtccode>
<dtcname>Starter Relay Circuit High</dtcname>
<subpara id="RM000000TA01A4X_08" type-id="60" category="03" proc-id="RM22W0E___00002NH00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>While the engine is being cranked, positive battery voltage is applied to terminal STA of the ECM.</ptxt>
<ptxt>If the ECM detects the starter control (STA) signal while the vehicle is being driven, it determines that there is a malfunction in the STA circuit. The ECM then illuminates the MIL and stores the DTC.</ptxt>
<ptxt>This monitor runs when the vehicle has been driven at 20 km/h (12.4 mph) or more for more than 20 seconds.</ptxt>
<table pgwide="1">
<title>P0617</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Drive vehicle for 25 seconds or more at a speed of 20 km/h (12.4 mph) or more and an engine speed of 1000 rpm or more</ptxt>
</entry>
<entry>
<ptxt>Conditions (a), (b) and (c) are met for 20 seconds</ptxt>
<ptxt>(a) Vehicle speed is more than 20 km/h (12.4 mph).</ptxt>
<ptxt>(b) Engine speed is more than 1000 rpm.</ptxt>
<ptxt>(c) STA signal is on.</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Park/neutral position switch (A/T)</ptxt>
</item>
<item>
<ptxt>Starter relay circuit</ptxt>
</item>
<item>
<ptxt>Clutch start switch (M/T)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>DTC No.</ptxt>
</entry>
<entry>
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P0617</ptxt>
</entry>
<entry>
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000TA01A4X_05" type-id="32" category="03" proc-id="RM22W0E___00002MY00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A263237E03" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="A301636E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TA01A4X_06" type-id="51" category="05" proc-id="RM22W0E___00002MZ00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07YX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06NX"/>).</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The following troubleshooting process is based on the premise that the engine can crank normally. If the engine does not crank, proceed to the Problem Symptoms Table (See page <xref label="Seep03" href="RM0000012WF07OX"/>).</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TA01A4X_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TA01A4X_07_0022" proc-id="RM22W0E___00002N700000">
<testtitle>CHECK IF VEHICLE IS EQUIPPED WITH ENTRY AND START SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the vehicle is equipped with the entry and start system.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/o Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0001" fin="false">A</down>
<right ref="RM000000TA01A4X_07_0023" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0001" proc-id="RM22W0E___00002N000000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / All Data / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Check the value displayed on the tester when the ignition switch is turned to ON and the engine is started.</ptxt>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Ignition Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine started</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG (A/T)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (M/T)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0002" fin="false">A</down>
<right ref="RM000000TA01A4X_07_0020" fin="false">B</right>
<right ref="RM000000TA01A4X_07_0012" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0002" proc-id="RM22W0E___00002N100000">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the park/neutral position switch (See page <xref label="Seep01" href="RM000002BKX03LX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0013" fin="false">A</down>
<right ref="RM000000TA01A4X_07_0011" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0013" proc-id="RM22W0E___00002N300000">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the park/neutral position switch (See page <xref label="Seep01" href="RM0000010NC075X"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA01A4X_07_0003" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0020" proc-id="RM22W0E___00002N500000">
<testtitle>INSPECT CLUTCH START SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the clutch start switch (for LHD) (See page <xref label="Seep01" href="RM0000032S3008X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the clutch start switch (for RHD) (See page <xref label="Seep02" href="RM000003D8H002X"/>).</ptxt>
</test1>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0021" fin="false">A</down>
<right ref="RM000000TA01A4X_07_0011" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0021" proc-id="RM22W0E___00002N600000">
<testtitle>REPLACE CLUTCH START SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the clutch start switch (for LHD) (See page <xref label="Seep01" href="RM0000032S5004X"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the clutch start switch (for RHD) (See page <xref label="Seep02" href="RM000003D8J002X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0003" proc-id="RM22W0E___00002N200000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / All Data / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Check the value displayed on the tester when the ignition switch is turned to ON and the engine is started.</ptxt>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Ignition Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine started</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0011" fin="false">A</down>
<right ref="RM000000TA01A4X_07_0016" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0011">
<testtitle>CHECK HARNESS AND CONNECTOR (PNP SWITCH OR CLUTCH  START SWITCH - STA TERMINAL OF ECM)</testtitle>
<res>
<down ref="RM000000TA01A4X_07_0017" fin="false">OK</down>
<right ref="RM000000TA01A4X_07_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0017" proc-id="RM22W0E___00002N400000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at 20 km/h (12.4 mph) or more for 20 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display (DTC Output)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0617</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0019" fin="true">A</down>
<right ref="RM000000TA01A4X_07_0016" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0023" proc-id="RM22W0E___00002N800000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / All Data / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Check the value displayed on the tester when the ignition switch is turned to ON and the engine is started.</ptxt>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Ignition Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine started</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG (A/T)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (M/T)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0024" fin="false">A</down>
<right ref="RM000000TA01A4X_07_0026" fin="false">B</right>
<right ref="RM000000TA01A4X_07_0012" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0024" proc-id="RM22W0E___00002N900000">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the park/neutral position switch (See page <xref label="Seep01" href="RM000002BKX03LX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0025" fin="false">A</down>
<right ref="RM000000TA01A4X_07_0028" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0025" proc-id="RM22W0E___00002NA00000">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the park/neutral position switch (See page <xref label="Seep01" href="RM0000010NC075X"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA01A4X_07_0030" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0026" proc-id="RM22W0E___00002NB00000">
<testtitle>INSPECT CLUTCH START SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the clutch start switch (for LHD) (See page <xref label="Seep01" href="RM0000032S3008X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the clutch start switch (for RHD) (See page <xref label="Seep02" href="RM000003D8H002X"/>).</ptxt>
</test1>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0027" fin="false">A</down>
<right ref="RM000000TA01A4X_07_0028" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0027" proc-id="RM22W0E___00002NC00000">
<testtitle>REPLACE CLUTCH START SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the clutch start switch (for LHD) (See page <xref label="Seep01" href="RM0000032S5004X"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the clutch start switch (for RHD) (See page <xref label="Seep02" href="RM000003D8J002X"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA01A4X_07_0030" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0028" proc-id="RM22W0E___00002ND00000">
<testtitle>INSPECT IGNITION OR STARTER SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the Ignition or Starter Switch Assembly (See page <xref label="Seep01" href="RM000000YK004HX"/>).</ptxt>
</test1>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0029" fin="false">A</down>
<right ref="RM000000TA01A4X_07_0031" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0029" proc-id="RM22W0E___00002NE00000">
<testtitle>REPLACE IGNITION OR STARTER SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the Ignition or Starter Switch Assembly (See page <xref label="Seep01" href="RM000000YK102WX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0030" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0030" proc-id="RM22W0E___00002NF00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / All Data / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Check the value displayed on the tester when the ignition switch is turned to ON and the engine is started.</ptxt>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Ignition Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine started</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0031" fin="false">A</down>
<right ref="RM000000TA01A4X_07_0016" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0031">
<testtitle>CHECK HARNESS AND CONNECTOR (PNP SWITCH OR CLUTCH  START SWITCH - STA TERMINAL OF ECM)</testtitle>
<res>
<down ref="RM000000TA01A4X_07_0032" fin="false">OK</down>
<right ref="RM000000TA01A4X_07_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0032" proc-id="RM22W0E___00002NG00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at 20 km/h (12.4 mph) or more for 20 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display (DTC Output)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0617</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA01A4X_07_0033" fin="true">A</down>
<right ref="RM000000TA01A4X_07_0016" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0012">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ10AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0018">
<testtitle>REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0016">
<testtitle>SYSTEM OK</testtitle>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0019">
<testtitle>GO TO CRANKING HOLDING FUNCTION CIRCUIT<xref label="Seep01" href="RM000000WZ30A7X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TA01A4X_07_0033">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202SX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>