<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CJ_T005M" variety="T005M">
<name>SFI SYSTEM</name>
<para id="RM000000XOK0DOX" category="C" type-id="302LP" name-id="ES11HU-001" from="201301">
<dtccode>P2237</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit / Open (Bank 1 Sensor 1)</dtcname>
<dtccode>P2238</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P2239</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit High (Bank 1 Sensor 1)</dtcname>
<dtccode>P2240</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit / Open (Bank 2 Sensor 1)</dtcname>
<dtccode>P2241</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit Low (Bank 2 Sensor 1)</dtcname>
<dtccode>P2242</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit High (Bank 2 Sensor 1)</dtcname>
<dtccode>P2252</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P2253</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit High (Bank 1 Sensor 1)</dtcname>
<dtccode>P2255</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit Low (Bank 2 Sensor 1)</dtcname>
<dtccode>P2256</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit High (Bank 2 Sensor 1)</dtcname>
<subpara id="RM000000XOK0DOX_01" type-id="60" category="03" proc-id="RM22W0E___00000LV00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2195 (See page <xref label="Seep01" href="RM000000WC40S6X_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2237</ptxt>
<ptxt>P2240</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open in the circuit between terminals AF+ and AF- of the air fuel ratio sensor while the engine is running (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor (sensor 1) circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (sensor 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2238</ptxt>
<ptxt>P2241</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Case 1:</ptxt>
</item>
</list1>
<ptxt>Condition (a) or (b) is met for 5.0 seconds or more (2 trip detection logic):</ptxt>
<ptxt>(a) AF+ voltage is 0.5 V or less.</ptxt>
<ptxt>(b) (AF+) - (AF-) is 0.1 V or less.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Case 2:</ptxt>
</item>
</list1>
<ptxt>Air fuel ratio sensor admittance is below 0.0074 1/Ω (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor (sensor 1) circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (sensor 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2239</ptxt>
<ptxt>P2242</ptxt>
</entry>
<entry valign="middle">
<ptxt>AF+ voltage is higher than 4.5 V for 5.0 seconds or more (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor (sensor 1) circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (sensor 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2252</ptxt>
<ptxt>P2255</ptxt>
</entry>
<entry valign="middle">
<ptxt>AF- voltage is 0.5 V or less for 5.0 seconds or more (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor (sensor 1) circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (sensor 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2253</ptxt>
<ptxt>P2256</ptxt>
</entry>
<entry valign="middle">
<ptxt>AF- voltage is higher than 4.5 V for 5.0 seconds or more (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor (sensor 1) circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (sensor 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTCs P2237, P2238, P2239, P2252 and P2253 indicate malfunctions related to the bank 1 air fuel ratio sensor circuit.</ptxt>
</item>
<item>
<ptxt>DTCs P2240, P2241, P2242, P2255 and P2256 indicate malfunctions related to the bank 2 air fuel ratio sensor circuit.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes cylinder No. 1.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that includes cylinder No. 2.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XOK0DOX_02" type-id="64" category="03" proc-id="RM22W0E___00000LW00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The air fuel ratio sensor varies its output voltage in proportion to the air-fuel ratio. If the air fuel ratio sensor impedance (alternating current resistance) or output voltage deviates greatly from the standard range, the ECM determines that there is an open or short in the air fuel ratio sensor circuit.</ptxt>
</content5>
</subpara>
<subpara id="RM000000XOK0DOX_11" type-id="73" category="03" proc-id="RM22W0E___00000M300000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<list1 type="ordered">
<item>
<ptxt>Start the engine and wait for 5 minutes or more.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000XOK0DOX_06" type-id="32" category="03" proc-id="RM22W0E___00000LX00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2195 (See page <xref label="Seep01" href="RM000000WC40S6X_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000XOK0DOX_07" type-id="51" category="05" proc-id="RM22W0E___00000LY00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Sensor 1 refers to the sensor closest to the engine assembly.</ptxt>
</item>
<item>
<ptxt>Sensor 2 refers to the sensor farthest away from the engine assembly.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XOK0DOX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XOK0DOX_08_0010" proc-id="RM22W0E___00000M000000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR FUEL RATIO SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C22-1 (HA1A) - C45-22 (HA1A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C22-3 (A1A+) - C45-126 (A1A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C22-4 (A1A-) - C45-125 (A1A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-1 (HA2A) - C45-20 (HA2A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-3 (A2A+) - C45-103 (A2A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-4 (A2A-) - C45-102 (A2A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C22-1 (HA1A) or C45-22 (HA1A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C22-3 (A1A+) or C45-126 (A1A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C22-4 (A1A-) or C45-125 (A1A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-1 (HA2A) or C45-20 (HA2A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-3 (A2A+) or C45-103 (A2A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-4 (A2A-) or C45-102 (A2A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C22-1 (HA1A) - C46-22 (HA1A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C22-3 (A1A+) - C46-126 (A1A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C22-4 (A1A-) - C46-125 (A1A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-1 (HA2A) - C46-20 (HA2A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-3 (A2A+) - C46-103 (A2A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-4 (A2A-) - C46-102 (A2A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C22-1 (HA1A) or C46-22 (HA1A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C22-3 (A1A+) or C46-126 (A1A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C22-4 (A1A-) or C46-125 (A1A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-1 (HA2A) or C46-20 (HA2A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-3 (A2A+) or C46-103 (A2A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C23-4 (A2A-) or C46-102 (A2A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XOK0DOX_08_0004" fin="false">OK</down>
<right ref="RM000000XOK0DOX_08_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XOK0DOX_08_0004" proc-id="RM22W0E___00000LZ00000">
<testtitle>REPLACE AIR FUEL RATIO SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air fuel ratio sensor (See page <xref label="Seep01" href="RM000002W9Q00XX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XOK0DOX_08_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XOK0DOX_08_0014" proc-id="RM22W0E___00000M200000">
<testtitle>PERFORM CONFIRMATION DRIVING PATTERN</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Drive the vehicle according to Confirmation Driving Pattern.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XOK0DOX_08_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XOK0DOX_08_0011" proc-id="RM22W0E___00000M100000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes / Pending.</ptxt>
</test1>
<test1>
<ptxt>Read the pending DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2237, P2238, P2239, P2240, P2241, P2242, P2252, P2253, P2255 or P2256 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XOK0DOX_08_0013" fin="true">A</down>
<right ref="RM000000XOK0DOX_08_0007" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XOK0DOX_08_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XOK0DOX_08_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XOK0DOX_08_0013">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>