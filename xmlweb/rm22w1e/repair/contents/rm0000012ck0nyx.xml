<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001V" variety="S001V">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001V_7C3RG_T00KJ" variety="T00KJ">
<name>NAVIGATION SYSTEM</name>
<para id="RM0000012CK0NYX" category="J" type-id="303DM" name-id="AVBW1-22" from="201308">
<dtccode/>
<dtcname>Steering Pad Switch Circuit</dtcname>
<subpara id="RM0000012CK0NYX_01" type-id="60" category="03" proc-id="RM22W0E___0000CIO00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit sends an operation signal from the steering pad switch assembly to the multi-media module receiver assembly.</ptxt>
<ptxt>If there is an open in the circuit, the audio system cannot be operated using the steering pad switch assembly.</ptxt>
<ptxt>If there is a short in the circuit, the same condition as when a switch is continuously depressed occurs.</ptxt>
<ptxt>Therefore, the multi-media module receiver assembly cannot be operated using the steering pad switch assembly, and also the multi-media module receiver assembly itself cannot function.</ptxt>
</content5>
</subpara>
<subpara id="RM0000012CK0NYX_02" type-id="32" category="03" proc-id="RM22W0E___0000CIP00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E248834E02" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000012CK0NYX_03" type-id="51" category="05" proc-id="RM22W0E___0000CIQ00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>The vehicle is equipped with a Supplemental Restraint System (SRS) which includes components such as airbags. Before servicing (including removal or installation of parts), be sure to read the precaution for Supplemental Restraint System (See page <xref label="Seep01" href="RM000000KT10L4X"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000012CK0NYX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012CK0NYX_04_0003" proc-id="RM22W0E___0000CIS00001">
<testtitle>INSPECT STEERING PAD SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the steering pad switch assembly (See page <xref label="Seep01" href="RM0000039SA01DX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the steering pad switch assembly (See page <xref label="Seep02" href="RM000003C4V014X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012CK0NYX_04_0004" fin="false">OK</down>
<right ref="RM0000012CK0NYX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012CK0NYX_04_0004" proc-id="RM22W0E___0000CIT00001">
<testtitle>INSPECT SPIRAL CABLE SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the spiral cable sub-assembly (See page <xref label="Seep01" href="RM000002O8X01XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the spiral cable sub-assembly (See page <xref label="Seep02" href="RM000002O8V02TX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012CK0NYX_04_0001" fin="false">OK</down>
<right ref="RM0000012CK0NYX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012CK0NYX_04_0001" proc-id="RM22W0E___0000CIR00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA NODULE RECEIVER ASSEMBLY - SPIRAL CABLE SUB-ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F77 multi-media module receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E12 spiral cable sub-assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F77-30 (SW1) - E12-6 (AU1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>F77-31 (SW2) - E12-5 (AU2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>F77-32 (SWG) - E12-4 (EAU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>F77-30 (SW1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>F77-31 (SW2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F77-32 (SWG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012CK0NYX_04_0002" fin="true">OK</down>
<right ref="RM0000012CK0NYX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012CK0NYX_04_0006">
<testtitle>REPLACE STEERING PAD SWITCH ASSEMBLY<xref label="Seep01" href="RM0000039SA01DX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012CK0NYX_04_0005">
<testtitle>REPLACE SPIRAL CABLE SUB-ASSEMBLY<xref label="Seep01" href="RM000002O8X01XX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012CK0NYX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000012CK0NYX_04_0002">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000011BR0NBX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>