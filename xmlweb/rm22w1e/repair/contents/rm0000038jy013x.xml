<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C3ZV_T00SY" variety="T00SY">
<name>FRONT BUMPER (for Standard)</name>
<para id="RM0000038JY013X" category="A" type-id="80002" name-id="ET8EC-01" from="201301" to="201308">
<name>DISASSEMBLY</name>
<subpara id="RM0000038JY013X_01" type-id="01" category="01">
<s-1 id="RM0000038JY013X_01_0023" proc-id="RM22W0E___0000IUL00000">
<ptxt>REMOVE HEADLIGHT CLEANER HOSE (w/ Headlight Cleaner System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 clamps and headlight cleaner hose.</ptxt>
<figure>
<graphic graphicname="B302166" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the headlight cleaner hose.</ptxt>
<figure>
<graphic graphicname="B302165E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000038JY013X_01_0021" proc-id="RM22W0E___0000IUJ00000">
<ptxt>REMOVE TYPE 1 HEADLIGHT WASHER NOZZLE SUB-ASSEMBLY LH (w/ Headlight Cleaner System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Extend the type 1 headlight washer nozzle sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B302163" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the type 1 headlight washer nozzle sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B302164" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038JY013X_01_0024" proc-id="RM22W0E___0000JIN00000">
<ptxt>REMOVE TYPE 1 HEADLIGHT WASHER NOZZLE SUB-ASSEMBLY RH (w/ Headlight Cleaner System)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0022" proc-id="RM22W0E___0000IUI00000">
<ptxt>REMOVE HEADLIGHT WASHER ACTUATOR SUB-ASSEMBLY LH (w/ Headlight Cleaner System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt.</ptxt>
<figure>
<graphic graphicname="B302167" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the headlight washer actuator sub-assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038JY013X_01_0025" proc-id="RM22W0E___0000JIO00000">
<ptxt>REMOVE HEADLIGHT WASHER ACTUATOR SUB-ASSEMBLY RH (w/ Headlight Cleaner System)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0001" proc-id="RM22W0E___0000CTP00000">
<ptxt>REMOVE NO. 4 ENGINE ROOM WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o TOYOTA Parking Assist-sensor System, w/ Fog Light:</ptxt>
<ptxt>Remove the No. 4 engine room wire.</ptxt>
<s3>
<ptxt>Disconnect the 2 connectors.</ptxt>
</s3>
<s3>
<ptxt>Detach the 11 clamps and remove the No. 4 engine room wire.</ptxt>
<figure>
<graphic graphicname="B302195" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/ Fog Light:</ptxt>
<ptxt>Remove the No. 4 engine room wire.</ptxt>
<s3>
<ptxt>Disconnect the 4 connectors.</ptxt>
</s3>
<s3>
<ptxt>Detach the 12 clamps and remove the  No. 4 engine room wire.</ptxt>
<figure>
<graphic graphicname="B302156" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/o Fog Light:</ptxt>
<ptxt>Remove the No. 4 engine room wire.</ptxt>
<s3>
<ptxt>Disconnect the 2 connectors.</ptxt>
</s3>
<s3>
<ptxt>Detach the 12 clamps and remove the  No. 4 engine room wire.</ptxt>
<figure>
<graphic graphicname="B302210" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0030" proc-id="RM22W0E___0000CTQ00000">
<ptxt>REMOVE ULTRASONIC SENSOR CLIP (w/ TOYOTA Parking Assist-sensor System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 claws and remove the ultrasonic sensor clip.</ptxt>
<figure>
<graphic graphicname="E244157" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038JY013X_01_0018" proc-id="RM22W0E___0000CTN00000">
<ptxt>REMOVE NO. 1 ULTRASONIC SENSOR (w/ TOYOTA Parking Assist-sensor System)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the No. 1 ultrasonic sensor on the other side.</ptxt>
</atten4>
<s2>
<ptxt>While pushing down on the lever with a finger to release the one claw, detach the claw on the other side to remove the No. 1 ultrasonic sensor.</ptxt>
<figure>
<graphic graphicname="E244158" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>The type of claw on the top and bottom of the sensor are different. Observe the claws carefully before removing the No. 1 ultrasonic sensor.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000038JY013X_01_0019" proc-id="RM22W0E___0000CTO00000">
<ptxt>REMOVE NO. 2 ULTRASONIC SENSOR RETAINER (w/ TOYOTA Parking Assist-sensor System)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to remove the No. 2 ultrasonic sensor retainer on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 2 claws and remove the  No. 2 ultrasonic sensor retainer from the front bumper cover.</ptxt>
<figure>
<graphic graphicname="E244159" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038JY013X_01_0028" proc-id="RM22W0E___0000J0000000">
<ptxt>REMOVE FRONT BUMPER INNER BRACKET (w/ Fog Light)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 claws and remove the front bumper inner bracket.</ptxt>
<figure>
<graphic graphicname="B302157" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0020" proc-id="RM22W0E___0000IZZ00000">
<ptxt>REMOVE FOG LIGHT ASSEMBLY LH (w/ Fog Light)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws.</ptxt>
<figure>
<graphic graphicname="E244776" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Screw</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Connector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 2 guides and remove the fog light assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038JY013X_01_0005" proc-id="RM22W0E___0000JIG00000">
<ptxt>REMOVE FOG LIGHT ASSEMBLY RH (w/ Fog Light)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0026" proc-id="RM22W0E___0000JIP00000">
<ptxt>REMOVE FRONT BUMPER HOLE COVER LH (w/ Fog Light)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 retainers and the 2 screws.</ptxt>
<figure>
<graphic graphicname="B302159" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 5 claws and remove the front bumper hole cover LH.</ptxt>
<figure>
<graphic graphicname="B310833" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0027" proc-id="RM22W0E___0000JIQ00000">
<ptxt>REMOVE FRONT BUMPER HOLE COVER RH (w/ Fog Light)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0016" proc-id="RM22W0E___0000JIL00000">
<ptxt>REMOVE FRONT BUMPER HOLE COVER LH (w/o Fog Light)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 retainers and the screw.</ptxt>
<figure>
<graphic graphicname="B302160" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 5 claws and remove the front bumper hole cover LH.</ptxt>
<figure>
<graphic graphicname="B310834" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0017" proc-id="RM22W0E___0000JIM00000">
<ptxt>REMOVE FRONT BUMPER HOLE COVER RH (w/o Fog Light)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0006" proc-id="RM22W0E___0000JIH00000">
<ptxt>REMOVE LOWER NO. 1 RADIATOR GRILLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 retainers.</ptxt>
</s2>
<s2>
<ptxt>Detach the 16 claws and remove the lower No. 1 radiator grille.</ptxt>
<figure>
<graphic graphicname="B302161E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0014" proc-id="RM22W0E___0000JIJ00000">
<ptxt>REMOVE FRONT BUMPER EXTENSION MOUNTING BRACKET (w/ Bracket)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B302211" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the front bumper extension mounting bracket.</ptxt>
</s2>

</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0015" proc-id="RM22W0E___0000JIK00000">
<ptxt>REMOVE FRONT BUMPER GUARD (w/ Garnish)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 nuts.</ptxt>
<figure>
<graphic graphicname="B302212" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 6 clips and remove the front bumper guard.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JY013X_01_0013" proc-id="RM22W0E___0000JII00000">
<ptxt>REMOVE FRONT NO. 2 BUMPER PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 front No. 2 bumper protectors.</ptxt>
<figure>
<graphic graphicname="B302162" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>