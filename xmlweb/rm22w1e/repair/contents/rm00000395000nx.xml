<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UV_T00NY" variety="T00NY">
<name>FRONT SEATBACK HEATER</name>
<para id="RM00000395000NX" category="A" type-id="30014" name-id="SE9DQ-01" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM00000395000NX_01" type-id="11" category="10" proc-id="RM22W0E___0000G7200000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000395000NX_02" type-id="01" category="01">
<s-1 id="RM00000395000NX_02_0011" proc-id="RM22W0E___0000G7500000">
<ptxt>INSTALL FRONT SEATBACK HEATER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the seatback heater with the name stamp side facing the front separate type seatback cover.</ptxt>
</s2>
<s2>
<ptxt>w/o Climate Control Seat System:</ptxt>
<ptxt>Install the front seatback heater assembly LH to the front separate type seatback cover with new tack pins.</ptxt>
<figure>
<graphic graphicname="B180729E02" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Separate Type Seatback Cover</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Seatback Heater Assembly LH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Name Stamp</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/ Climate Control Seat System:</ptxt>
<ptxt>Install the front seatback heater assembly LH to the front separate type seatback cover with new tack pins.</ptxt>
<figure>
<graphic graphicname="B298408E01" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Separate Type Seatback Cover</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Seatback Heater Assembly LH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Name Stamp</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000395000NX_02_0012" proc-id="RM22W0E___0000FUV00000">
<ptxt>INSTALL FRONT SEPARATE TYPE SEATBACK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the fastening tape to install the front separate type seatback cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B180730E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Using hog ring pliers, install new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Pass the seatback cover bracket through the hole of the front separate type seatback pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Cover Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B180731E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395000NX_02_0022" proc-id="RM22W0E___0000FVA00000">
<ptxt>INSTALL FRONT SEPARATE TYPE SEATBACK COVER (w/ Climate Control Seat System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using hog ring pliers, install the front separate type seatback cover to the front separate type seatback pad with new hog rings.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
<figure>
<graphic graphicname="B298259E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Pass the seatback cover bracket through the hole of the front separate type seatback pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Cover Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B180731E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000395000NX_02_0002" proc-id="RM22W0E___0000G7300000">
<ptxt>INSTALL SEATBACK COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B299577E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<s2>
<ptxt>Attach the hooks to install the seatback cover with pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Attach the seatback cover bracket with the nut.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Cover Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B180716E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
<atten2>
<ptxt>After the seatback cover with pad is installed, make sure the seatback cover bracket is not twisted.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Using hog ring pliers, install new hog rings.</ptxt>
<atten3>
<ptxt>Be careful not to damage the cover.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>w/o Climate Control Seat System:</ptxt>
<ptxt>Connect the connector and attach the 2 wire harness clamps.</ptxt>
<figure>
<graphic graphicname="B180706" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Climate Control Seat System:</ptxt>
<ptxt>Connect the 2 connectors and attach the 4 wire harness clamps.</ptxt>
<figure>
<graphic graphicname="B299194" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000395000NX_02_0013" proc-id="RM22W0E___0000FM300000">
<ptxt>INSTALL FRONT SEAT HEADREST SUPPORT
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B298589" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 4 claws to install the 2 front seat headrest supports.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395000NX_02_0021" proc-id="RM22W0E___0000FV900000">
<ptxt>INSTALL SEAT CLIMATE CONTROL CONTROLLERLH (w/ Climate Control Seat System)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B299415" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 4 hooks to install the seat climate control controller LH.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 claws to connect the seat climate control blower LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395000NX_02_0014" proc-id="RM22W0E___0000FM400000">
<ptxt>INSTALL FRONT SEATBACK BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182646" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Move the front seatback board sub-assembly in the direction of the arrow to attach the 2 hooks and install it.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395000NX_02_0015" proc-id="RM22W0E___0000FM500000">
<ptxt>INSTALL FRONT INNER SEAT CUSHION SHIELD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180703" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws and clip to install the front inner seat cushion shield LH.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395000NX_02_0019" proc-id="RM22W0E___0000FM000000">
<ptxt>INSTALL FRONT SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185875E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install the front seat inner belt assembly with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not allow the anchor part of the front seat inner belt assembly to overlap the protruding parts of the front seat adjuster.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the connectors and attach the clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395000NX_02_0016" proc-id="RM22W0E___0000FM600000">
<ptxt>INSTALL FRONT SEAT CUSHION SHIELD LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Driver Side:</ptxt>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 claws to install the front seat cushion shield LH together with the front inner No. 1 cushion shield LH.</ptxt>
<figure>
<graphic graphicname="B180700" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 rubber bands.</ptxt>
<figure>
<graphic graphicname="B180699" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395000NX_02_0017" proc-id="RM22W0E___0000FM700000">
<ptxt>INSTALL RECLINING POWER SEAT SWITCH KNOB
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180734" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the reclining power seat switch knob.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395000NX_02_0018" proc-id="RM22W0E___0000FM800000">
<ptxt>INSTALL SLIDE AND VERTICAL POWER SEAT SWITCH KNOB
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180735" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws to install the slide and vertical power seat switch knob.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000395000NX_02_0010" proc-id="RM22W0E___0000G7400000">
<ptxt>INSTALL FRONT SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000390Q00WX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>