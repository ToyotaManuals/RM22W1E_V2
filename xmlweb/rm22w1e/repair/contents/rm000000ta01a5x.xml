<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000000TA01A5X" category="C" type-id="302IA" name-id="ES169F-001" from="201301" to="201308">
<dtccode>P0617</dtccode>
<dtcname>Starter Relay Circuit High</dtcname>
<subpara id="RM000000TA01A5X_08" type-id="60" category="03" proc-id="RM22W0E___00003E100000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>While the engine is being cranked, positive battery voltage is applied to terminal STA of the ECM.</ptxt>
<ptxt>If the ECM detects the starter control (STA) signal while the vehicle is being driven, it determines that there is a malfunction in the STA circuit. The ECM then illuminates the MIL and stores the DTC.</ptxt>
<ptxt>This monitor runs when the vehicle has been driven at 20 km/h (12.5 mph) or more for more than 20 seconds.</ptxt>
<table pgwide="1">
<title>P0617</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Drive the vehicle for 25 seconds or more at a speed of 20 km/h (12.5 mph) or more and an engine speed of 1000 rpm or more</ptxt>
</entry>
<entry>
<ptxt>Conditions (a), (b) and (c) are met for 20 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) Vehicle speed is more than 20 km/h (12.5 mph).</ptxt>
<ptxt>(b) Engine speed is more than 1000 rpm.</ptxt>
<ptxt>(c) STA signal is on.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Starter relay (ST) circuit</ptxt>
</item>
<item>
<ptxt>Park/neutral position switch assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P0617</ptxt>
</entry>
<entry>
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000TA01A5X_05" type-id="32" category="03" proc-id="RM22W0E___00003DZ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A180644E34" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TA01A5X_06" type-id="51" category="05" proc-id="RM22W0E___00003E000000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM0000012XK07ZX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM000000TIN06OX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The following troubleshooting process is based on the premise that the engine can crank normally. If the engine does not crank, proceed to Problem Symptoms Table (See page <xref label="Seep05" href="RM0000012WF07PX"/>).</ptxt>
</item>
<item>
<ptxt>If this DTC is output, inspect the starter.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TA01A5X_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TA01A5X_09_0028" proc-id="RM22W0E___00003E200000">
<testtitle>READ VALUE USING GTS (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the GTS when the engine switch is turned on (IG) and when the engine is started.</ptxt>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch turned on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Close</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine started</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TA01A5X_09_0046" fin="false">NG</down>
<right ref="RM000000TA01A5X_09_0035" fin="false">OK</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A5X_09_0046" proc-id="RM22W0E___00003E700000">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the park/neutral position switch assembly (See page <xref label="Seep01" href="RM000002BKX03MX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA01A5X_09_0043" fin="false">OK</down>
<right ref="RM000000TA01A5X_09_0047" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A5X_09_0043" proc-id="RM22W0E___00003E600000">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION&#13;
SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the park/neutral position switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C45-49 (NSW) - A38-48 (STA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C45-49 (NSW) or A38-48 (STA) - Body ground and other terminals</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C46-49 (NSW) - A52-48 (STA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C46-49 (NSW) or A52-48 (STA) - Body ground and other terminals</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA01A5X_09_0032" fin="false">OK</down>
<right ref="RM000000TA01A5X_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A5X_09_0032" proc-id="RM22W0E___00003E300000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202SX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA01A5X_09_0035" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A5X_09_0047" proc-id="RM22W0E___00003E800000">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the park/neutral position switch assembly (See page <xref label="Seep01" href="RM0000010NC076X"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA01A5X_09_0035" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TA01A5X_09_0034" proc-id="RM22W0E___00003E400000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA01A5X_09_0035" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TA01A5X_09_0035" proc-id="RM22W0E___00003E500000">
<testtitle>CONFIRM WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for 25 seconds or more at a speed of 20 km/h (12.5 mph) or more and an engine speed of 1000 rpm or more.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA01A5X_09_0036" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TA01A5X_09_0036">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>