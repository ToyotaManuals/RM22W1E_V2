<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3SX_T00M0" variety="T00M0">
<name>ELECTRICAL KEY OSCILLATOR (for Rear Floor)</name>
<para id="RM000002QFV032X" category="A" type-id="80001" name-id="TD8S0-01" from="201308">
<name>REMOVAL</name>
<subpara id="RM000002QFV032X_02" type-id="01" category="01">
<s-1 id="RM000002QFV032X_02_0062" proc-id="RM22W0E___0000K7300000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="2">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002QFV032X_02_0048" proc-id="RM22W0E___0000ECW00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002QFV032X_02_0056" proc-id="RM22W0E___0000ED100001">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY LH (except Face to Face Seat Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 2 seat assembly LH (See page <xref label="Seep01" href="RM00000391S00ZX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002QFV032X_02_0057" proc-id="RM22W0E___0000ED200001">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY RH (except Face to Face Seat Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 2 seat assembly RH (See page <xref label="Seep01" href="RM00000391S00ZX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002QFV032X_02_0058" proc-id="RM22W0E___0000ED300001">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY LH (for Face to Face Seat Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 2 seat assembly LH (See page <xref label="Seep01" href="RM00000311C003X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002QFV032X_02_0059" proc-id="RM22W0E___0000ED400001">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY RH (for Face to Face Seat Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 2 seat assembly RH (See page <xref label="Seep01" href="RM00000311C003X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002QFV032X_02_0052" proc-id="RM22W0E___0000ECX00001">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat assembly LH (See page <xref label="Seep01" href="RM00000391400RX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002QFV032X_02_0053" proc-id="RM22W0E___0000ECY00001">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat assembly RH (See page <xref label="Seep01" href="RM00000391400RX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002QFV032X_02_0060" proc-id="RM22W0E___0000ED500001">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front quarter trim panel assembly LH (See page <xref label="Seep01" href="RM0000038MO00UX_01_0020"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002QFV032X_02_0061" proc-id="RM22W0E___0000ED600001">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front quarter trim panel assembly LH (See page <xref label="Seep01" href="RM0000038MO00UX_01_0022"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002QFV032X_02_0054" proc-id="RM22W0E___000068G00001">
<ptxt>REMOVE REAR NO. 1 SEAT PROTECTOR
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184032E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 10 claws and remove the 2 seat protectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002QFV032X_02_0055" proc-id="RM22W0E___000068H00001">
<ptxt>REMOVE REAR NO. 2 SEAT PROTECTOR
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184031E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 10 claws and remove the 2 seat protectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002QFV032X_02_0041" proc-id="RM22W0E___0000ECV00001">
<ptxt>REMOVE INDOOR ELECTRICAL KEY OSCILLATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fold back the front floor carpet assembly.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B180449" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the electrical key oscillator.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>