<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SD_T00LG" variety="T00LG">
<name>CAN COMMUNICATION SYSTEM (for RHD)</name>
<para id="RM000002L8204EX" category="J" type-id="801MT" name-id="NW48X-01" from="201301" to="201308">
<dtccode/>
<dtcname>Open in CAN Main Wire</dtcname>
<subpara id="RM000002L8204EX_01" type-id="60" category="03" proc-id="RM22W0E___0000DSC00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>There may be an open circuit in the CAN main wire and/or DLC3 CAN branch wire when the resistance between terminals 6 (CANH) and 14 (CANL) of the DLC3 is 69 Ω or higher.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>The resistance between terminals 6 (CANH) and 14 (CANL) of the DLC3 is 69 Ω or higher.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>CAN main wire and connector</ptxt>
</item>
<item>
<ptxt>No. 1 junction connector</ptxt>
</item>
<item>
<ptxt>No. 2 junction connector</ptxt>
</item>
<item>
<ptxt>No. 3 junction connector</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Combination meter assembly</ptxt>
</item>
<item>
<ptxt>DLC3 CAN branch wire or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002L8204EX_02" type-id="32" category="03" proc-id="RM22W0E___0000DSD00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C258821E02" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002L8204EX_03" type-id="51" category="05" proc-id="RM22W0E___0000DSE00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Operating the ignition switch, any switches or any doors triggers related ECU and sensor communication with the CAN, which causes resistance variation.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002L8204EX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002L8204EX_04_0034" proc-id="RM22W0E___0000DST00000">
<testtitle>PRECAUTION</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content6>
<res>
<down ref="RM000002L8204EX_04_0024" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0024" proc-id="RM22W0E___0000DSR00000">
<testtitle>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal before measuring the resistances of the CAN main wire and the CAN branch wire.</ptxt>
<atten2>
<ptxt>For vehicles with an SRS system:</ptxt>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0001" proc-id="RM22W0E___0000DSF00000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (NO. 3 JUNCTION CONNECTOR - DLC3)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C253362E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E22-6 (CANH) - E22-14 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>When the measured value is 133 Ω or higher and a CAN communication system diagnostic trouble code is output, there may be a fault besides disconnection of the DLC3 branch wire. For that reason, troubleshooting should be performed again from "How to Proceed with Troubleshooting" (See page <xref label="Seep01" href="RM000001RSO08DX"/>) after repairing the trouble area.</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0002" fin="false">OK</down>
<right ref="RM000002L8204EX_04_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0002" proc-id="RM22W0E___0000DSG00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 2 JUNCTION CONNECTOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E69 No. 2 junction connector. </ptxt>
<figure>
<graphic graphicname="E207339E13" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E69-4 (CANH) - E69-15 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 2 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0003" fin="false">OK</down>
<right ref="RM000002L8204EX_04_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0003" proc-id="RM22W0E___0000DSH00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 2 JUNCTION CONNECTOR - COMBINATION METER ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E207339E12" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E69-1 (CANH) - E69-12 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 2 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0016" fin="true">OK</down>
<right ref="RM000002L8204EX_04_0027" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0004" proc-id="RM22W0E___0000DSI00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the E69 No. 2 junction connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0005" proc-id="RM22W0E___0000DSJ00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 1 JUNCTION CONNECTOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E72 No. 1 junction connector.</ptxt>
<figure>
<graphic graphicname="E207339E14" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E72-2 (CANH) - E72-13 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 1 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0006" fin="false">OK</down>
<right ref="RM000002L8204EX_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0006" proc-id="RM22W0E___0000DSK00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 1 JUNCTION CONNECTOR - NO. 2 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E207339E15" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E72-1 (CANH) - E72-12 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 1 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0019" fin="true">OK</down>
<right ref="RM000002L8204EX_04_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0007" proc-id="RM22W0E___0000DSL00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the E72 No. 1 junction connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0008" proc-id="RM22W0E___0000DSM00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (ECM - NO. 1 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A52 ECM connector.</ptxt>
<figure>
<graphic graphicname="E207340E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>except 1VD-FTV</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A52-10 (CANH) - A52-11 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 1VD-FTV</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A52-41 (CANH) - A52-49 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>except 1VD-FTV</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1VD-FTV</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 1 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for 1UR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0029" fin="true">A</down>
<right ref="RM000002L8204EX_04_0031" fin="true">B</right>
<right ref="RM000002L8204EX_04_0033" fin="true">C</right>
<right ref="RM000002L8204EX_04_0028" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0027" proc-id="RM22W0E___0000DSS00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the E69 No. 2 junction connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0010" proc-id="RM22W0E___0000DSN00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 3 JUNCTION CONNECTOR - COMBINATION METER ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E75 No. 3 junction connector.</ptxt>
<figure>
<graphic graphicname="E207339E16" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E75-3 (CANH) - E75-14 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 3 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0011" fin="false">OK</down>
<right ref="RM000002L8204EX_04_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0011" proc-id="RM22W0E___0000DSO00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 3 JUNCTION CONNECTOR - NO. 2 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E207339E17" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E75-1 (CANH) - E75-12 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 3 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0021" fin="true">OK</down>
<right ref="RM000002L8204EX_04_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0012" proc-id="RM22W0E___0000DSP00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the E75 No. 3 junction connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0013" proc-id="RM22W0E___0000DSQ00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (COMBINATION METER ASSEMBLY - NO. 3 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E7 combination meter assembly connector.</ptxt>
<figure>
<graphic graphicname="E207341E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E7-25 (CANH) - E7-26 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Combination Meter Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (w/ Multi-information Display)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (w/o Multi-information Display)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8204EX_04_0014" fin="true">A</down>
<right ref="RM000002L8204EX_04_0032" fin="true">B</right>
<right ref="RM000002L8204EX_04_0022" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002L8204EX_04_0014">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM0000038ID00IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0015">
<testtitle>REPAIR OR REPLACE CAN BRANCH WIRE CONNECTED TO DLC3 (CANH, CANL)</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0016">
<testtitle>REPLACE NO. 2 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0017">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE OR CONNECTOR (NO. 1 JUNCTION CONNECTOR - NO. 2 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0019">
<testtitle>REPLACE NO. 1 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0028">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE CONNECTED TO ECM (ECM - NO. 1 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0029">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0020">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE OR CONNECTOR (NO. 3 JUNCTION CONNECTOR - NO. 2 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0021">
<testtitle>REPLACE NO. 3 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0022">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE CONNECTED TO COMBINATION METER ASSEMBLY (COMBINATION METER ASSEMBLY - NO. 3 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0031">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0032">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM0000039M2009X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002L8204EX_04_0033">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202SX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>