<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000003NJL0AXX" category="C" type-id="8046R" name-id="BCE7K-01" from="201301" to="201308">
<dtccode>C1439</dtccode>
<dtcname>Steering Angle Sensor Initialization Incomplete</dtcname>
<dtccode>C1445</dtccode>
<dtcname>Vehicle Driven with Steering Angle Sensor not Initialized</dtcname>
<subpara id="RM000003NJL0AXX_01" type-id="60" category="03" proc-id="RM22W0E___0000AJL00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU (master cylinder solenoid) acquires steering sensor zero point every time the ignition switch is turned to ON and the vehicle is driven at 35 km/h (22 mph) or more for approximately 5 seconds. The ECU also stores the previous zero point.</ptxt>
<ptxt>The steering sensor zero point malfunction warning is canceled by turning the ignition switch off.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.25in"/>
<colspec colname="COL2" colwidth="3.13in"/>
<colspec colname="COL3" colwidth="2.71in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1439</ptxt>
</entry>
<entry valign="middle">
<ptxt>After the steering sensor zero point calibration is cleared due to, for example, fluctuations in the power source, the steering zero point calibration cannot be obtained for 60 seconds of continuous driving.</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Steering sensor</ptxt>
</item>
<item>
<ptxt>Yaw rate sensor assembly</ptxt>
</item>
<item>
<ptxt>Skid control ECU (Master cylinder solenoid)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1445</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering sensor zero point calibration cannot be obtained when the vehicle is driven without a record of steering sensor zero point calibration at a speed of 35 km/h (22 mph) or more for 30 seconds or more without the brake pedal being depressed.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003NJL0AXX_02" type-id="51" category="05" proc-id="RM22W0E___0000AJM00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before proceeding with the inspection, explain to the customer that the output DTC is related to temporary loss of the steering sensor zero point calibration, such as when the power voltage drops when the battery is removed, and confirm that this condition occurred.</ptxt>
</item>
<item>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>When the speed sensor or the yaw rate sensor assembly has trouble, DTCs for the steering sensor may be stored even when the steering sensor is normal. When DTCs for the speed sensor or yaw rate sensor assembly are output together with other DTCs for the steering sensor, inspect and repair the speed sensor and yaw rate sensor assembly first, and then inspect and repair the steering sensor.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000003NJL0AXX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003NJL0AXX_05_0002" proc-id="RM22W0E___0000AJO00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00MX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON again and check that no CAN communication system DTC is output.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 35 km/h (22 mph) and turn the steering wheel to the right and left, and then check that no speed sensor and yaw rate sensor assembly DTCs are output (See page <xref label="Seep02" href="RM0000046KV00MX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.29in"/>
<colspec colname="COL2" colwidth="4.67in"/>
<colspec colname="COL3" colwidth="1.12in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="3" valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>C1439 and/or C1445</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Relating to CAN communication system (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Relating to CAN communication system (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Relating to speed sensor or yaw rate sensor assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If there is a malfunction in the speed sensor or the yaw rate sensor assembly, an abnormal value may be output although the steering sensor is normal.</ptxt>
</item>
<item>
<ptxt>If the speed sensor and the yaw rate sensor assembly DTCs are output simultaneously, repair the sensors and inspect the steering sensor.</ptxt>
</item>
<item>
<ptxt>If DTC C1439 and/or C1445 is not output again, the DTC may have been stored due to a temporary loss of the steering sensor zero point calibration, such as when the power voltage drops.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003NJL0AXX_05_0003" fin="false">A</down>
<right ref="RM000003NJL0AXX_05_0010" fin="true">B</right>
<right ref="RM000003NJL0AXX_05_0028" fin="true">C</right>
<right ref="RM000003NJL0AXX_05_0011" fin="true">D</right>
<right ref="RM000003NJL0AXX_05_0009" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000003NJL0AXX_05_0003" proc-id="RM22W0E___0000AJP00000">
<testtitle>PERFORM STEERING SENSOR ZERO POINT CALIBRATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Drive the vehicle straight ahead at 35 km/h (22 mph) or more for at least 5 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check that the centered position of the steering wheel is correctly set while driving straight ahead.</ptxt>
<atten4>
<ptxt>If the front wheel alignment and steering position are adjusted as a result of an abnormal centered position of the steering wheel, acquire the yaw rate and acceleration sensor zero point again after the adjustments are completed.</ptxt>
</atten4>
</test1>
<spec>
<title>OK</title>
<specitem>
<ptxt>The centered position of the steering wheel is correctly set.</ptxt>
</specitem>
</spec>
</content6>
<res>
<down ref="RM000003NJL0AXX_05_0001" fin="false">OK</down>
<right ref="RM000003NJL0AXX_05_0026" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003NJL0AXX_05_0001" proc-id="RM22W0E___0000AJN00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00MX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 35 km/h (22 mph) or more and turn the steering wheel to the right and left.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00MX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.81in"/>
<colspec colname="COL2" colwidth="1.27in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTCs C1439 and C1445 are not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Yaw rate sensor assembly and/or steering sensor DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the same DTC C1439 and/or C1445 is not output again, the DTC may have been stored due to a temporary loss of the steering angle zero point calibration, such as when the power voltage drops.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003NJL0AXX_05_0013" fin="true">A</down>
<right ref="RM000003NJL0AXX_05_0011" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003NJL0AXX_05_0010">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003NJL0AXX_05_0011">
<testtitle>REPAIR CIRCUITS INDICATED BY OUTPUT DTCS<xref label="Seep01" href="RM0000045Z600TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003NJL0AXX_05_0009">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003NJL0AXX_05_0013">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000003NJL0AXX_05_0026">
<testtitle>ADJUST FRONT WHEEL ALIGNMENT OR STEERING POSITION<xref label="Seep01" href="RM000001V43020X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003NJL0AXX_05_0028">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO08DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>