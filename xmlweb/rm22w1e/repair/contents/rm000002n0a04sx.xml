<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SD_T00LG" variety="T00LG">
<name>CAN COMMUNICATION SYSTEM (for RHD)</name>
<para id="RM000002N0A04SX" category="C" type-id="8026L" name-id="NW496-02" from="201308">
<dtccode>U1100</dtccode>
<dtcname>Lost Communication with Seat Belt Control ECU</dtcname>
<subpara id="RM000002N0A04SX_01" type-id="60" category="03" proc-id="RM22W0E___0000DSU00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U1100</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is no communication from the seat belt control ECU.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Power source circuit of seat belt control ECU</ptxt>
</item>
<item>
<ptxt>Seat belt control ECU CAN branch wire or connector</ptxt>
</item>
<item>
<ptxt>Seat belt control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>For vehicles with an pre-crash safety system.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002N0A04SX_02" type-id="32" category="03" proc-id="RM22W0E___0000DSV00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C150433E11" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002N0A04SX_03" type-id="51" category="05" proc-id="RM22W0E___0000DSW00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Operating the ignition switch, any switches or any doors triggers related ECU and sensor communication with the CAN, which causes resistance variation.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002N0A04SX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002N0A04SX_04_0009" proc-id="RM22W0E___0000DT000001">
<testtitle>PRECAUTION</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content6>
<res>
<down ref="RM000002N0A04SX_04_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002N0A04SX_04_0008" proc-id="RM22W0E___0000DSZ00001">
<testtitle>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal before measuring the resistances of the CAN main wire and the CAN branch wire.</ptxt>
<atten2>
<ptxt>For vehicles with an SRS system:</ptxt>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000002N0A04SX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002N0A04SX_04_0002" proc-id="RM22W0E___0000DSX00001">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (SEAT BELT CONTROL ECU CAN BRANCH WIRE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E42 seat belt control ECU connector.</ptxt>
<figure>
<graphic graphicname="C251357E02" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E42-1 (CANH) - E42-2 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>54 to 69 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Seat Belt Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002N0A04SX_04_0003" fin="false">OK</down>
<right ref="RM000002N0A04SX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002N0A04SX_04_0003" proc-id="RM22W0E___0000DSY00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SEAT BELT CONTROL ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
<figure>
<graphic graphicname="C251358E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Disconnect the E41 seat belt control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E41-7 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E42-8 (IG1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E41-8 (PGND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Seat Belt Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000002N0A04SX_04_0007" fin="true">OK</down>
<right ref="RM000002N0A04SX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002N0A04SX_04_0005">
<testtitle>REPAIR OR REPLACE SEAT BELT CONTROL ECU CAN BRANCH WIRE OR CONNECTOR (CANH, CANL)</testtitle>
</testgrp>
<testgrp id="RM000002N0A04SX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002N0A04SX_04_0007">
<testtitle>REPLACE SEAT BELT CONTROL ECU<xref label="Seep01" href="RM000003A6M005X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>