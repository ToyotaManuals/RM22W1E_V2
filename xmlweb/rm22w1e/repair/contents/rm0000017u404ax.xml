<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7C3CR_T005U" variety="T005U">
<name>CAMSHAFT OIL CONTROL VALVE</name>
<para id="RM0000017U404AX" category="G" type-id="8000T" name-id="ES1QR-32" from="201301">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM0000017U404AX_01" type-id="01" category="01">
<s-1 id="RM0000017U404AX_01_0001" proc-id="RM22W0E___000010X00000">
<ptxt>INSPECT CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>Turn the GTS on.</ptxt>
</s2>
<s2>
<ptxt>Inspect the oil control valve (for intake camshaft).</ptxt>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine / Active Test / Control the VVT System (Bank 1).</ptxt>
</s3>
<s3>
<ptxt>Operate the oil control valve using the GTS, and then check the engine speed.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal engine speed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rough idle or engine stalls soon after oil control valve switched from OFF to ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the operation is not as specified, check the oil control valve, wire harness and ECM.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine / Active Test / Control the VVT System (Bank 2).</ptxt>
</s3>
<s3>
<ptxt>Operate the oil control valve using the GTS, and then check the engine speed.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal engine speed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rough idle or engine stalls soon after oil control valve switched from OFF to ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the operation is not as specified, check the oil control valve, wire harness and ECM.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the oil control valve (for exhaust camshaft).</ptxt>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine / Active Test / Control the VVT Exhaust Linear (Bank 1).</ptxt>
</s3>
<s3>
<ptxt>Operate the oil control valve using the GTS, and then check the engine speed.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>-100%</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal engine speed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>100%</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rough idle or engine stalls soon after oil control valve switched to 100%</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the operation is not as specified, check the oil control valve, wire harness and ECM.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine / Active Test / Control the VVT Exhaust Linear (Bank 2).</ptxt>
</s3>
<s3>
<ptxt>Operate the oil control valve using the GTS, and then check the engine speed.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>-100%</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal engine speed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>100%</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rough idle or engine stalls soon after oil control valve switched to 100%</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the operation is not as specified, check the oil control valve, wire harness and ECM.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>