<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12054_S0027" variety="S0027">
<name>METER / GAUGE / DISPLAY</name>
<ttl id="12054_S0027_7C3TR_T00MU" variety="T00MU">
<name>COMBINATION METER (w/ Multi-information Display)</name>
<para id="RM0000038ID00IX" category="A" type-id="80001" name-id="ME4PA-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM0000038ID00IX_01" type-id="01" category="01">
<s-1 id="RM0000038ID00IX_01_0009" proc-id="RM22W0E___0000F2K00000">
<ptxt>DISABLE AUTO TILT AWAY FUNCTION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disable the function by changing the customize parameter (See page <xref label="Seep01" href="RM000000UZ00A8X"/>).</ptxt>
<atten3>
<ptxt>Record the current customize parameter setting (whether the Autoaway/Return function is enabled or disabled) in order to restore the current setting after finishing the operation.</ptxt>
</atten3>
<atten4>
<ptxt>Performing the above operation causes the Autoaway/Return function to be disabled when the engine switch is turned off.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Turn the engine switch on (IG). Operate the tilt and telescopic switch to fully extend and lower the steering column assembly.</ptxt>
</s2>
<s2>
<ptxt>Turn the engine switch off.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038ID00IX_01_0010" proc-id="RM22W0E___0000F2L00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="2">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000038ID00IX_01_0001" proc-id="RM22W0E___0000F2H00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000038ID00IX_01_0003" proc-id="RM22W0E___0000F2200000">
<ptxt>REMOVE NO. 1 INSTRUMENT CLUSTER FINISH PANEL GARNISH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E154744E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 3 claws and remove the panel garnish.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038ID00IX_01_0004" proc-id="RM22W0E___0000CXB00000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E155426E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the panel cushion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038ID00IX_01_0005" proc-id="RM22W0E___0000CXC00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip.</ptxt>
<figure>
<graphic graphicname="E154742" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 8 claws.</ptxt>
</s2>
<s2>
<ptxt>Remove the panel pad and disconnect the connectors and 2 clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038ID00IX_01_0006" proc-id="RM22W0E___0000CXD00000">
<ptxt>REMOVE NO. 2 INSTRUMENT CLUSTER FINISH PANEL GARNISH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E154741E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws and remove the panel garnish.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038ID00IX_01_0007" proc-id="RM22W0E___0000F2I00000">
<ptxt>REMOVE INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 claws.</ptxt>
<figure>
<graphic graphicname="E155398" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 9 claws.</ptxt>
<figure>
<graphic graphicname="E154732" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the finish panel and disconnect the connector.</ptxt>
<figure>
<graphic graphicname="E155437" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038ID00IX_01_0008" proc-id="RM22W0E___0000F2J00000">
<ptxt>REMOVE COMBINATION METER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 screws.</ptxt>
<figure>
<graphic graphicname="E154733" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors, and remove the combination meter by pulling it up in the direction of the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="E154734" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>