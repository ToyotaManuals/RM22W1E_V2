<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3TW_T00MZ" variety="T00MZ">
<name>AIRBAG SYSTEM</name>
<para id="RM00000432J05BX" category="C" type-id="804O2" name-id="RS8B6-72" from="201301" to="201308">
<dtccode>B161A/8A</dtccode>
<dtcname>Lost Communication with Front Satellite Sensor Bus</dtcname>
<subpara id="RM00000432J05BX_01" type-id="60" category="03" proc-id="RM22W0E___0000FG700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The front collision sensor circuit (front airbag sensor RH circuit and front airbag sensor LH circuit) is composed of the center airbag sensor assembly, front airbag sensor RH and front airbag sensor LH.</ptxt>
<ptxt>The front airbag sensor RH or front airbag sensor LH detects impacts to the vehicle and sends signals to the center airbag sensor assembly to determine if the airbag should be deployed.</ptxt>
<ptxt>DTC B161A/8A is stored when a malfunction is detected in the front collision sensor circuit (front airbag sensor RH circuit and front airbag sensor LH circuit).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B161A/8A</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor assembly detects a line short circuit signal, open circuit signal, short circuit to ground signal or short circuit to B+ signal in the front collision sensor circuit (front airbag sensor RH circuit and front airbag sensor LH circuit).</ptxt>
</item>
<item>
<ptxt>A front airbag sensor RH malfunction.</ptxt>
</item>
<item>
<ptxt>A front airbag sensor LH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor assembly malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>Engine room main wire</ptxt>
</item>
<item>
<ptxt>Front airbag sensor RH</ptxt>
</item>
<item>
<ptxt>Front airbag sensor LH</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000432J05BX_02" type-id="32" category="03" proc-id="RM22W0E___0000FG800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C215016E03" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000432J05BX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM00000432J05BX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000432J05BX_04_0001" proc-id="RM22W0E___0000FG900000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<atten4>
<ptxt>If a communication error occurs, DTCs for both the LH and RH sides will be stored simultaneously. To identify the malfunctioning area, turn the ignition switch off and then to ON again.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC B1613/83 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC B1618/84 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTCs B1613/83 and B1618/84 are not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTCs indicating communication errors will be changed to DTCs indicating errors in initialization by turning the  ignition switch off and then to ON again.</ptxt>
</item>
<item>
<ptxt>Codes other than DTCs B1613/83 and B1618/84 may be output at this time, but they are not related to this check.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000432J05BX_04_0002" fin="false">C</down>
<right ref="RM00000432J05BX_04_0003" fin="true">A</right>
<right ref="RM00000432J05BX_04_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000432J05BX_04_0002" proc-id="RM22W0E___0000FGA00000">
<testtitle>CHECK CENTER AIRBAG SENSOR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs stored in memory (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0JBX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B161A/8A is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B161A/8A may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000432J05BX_04_0006" fin="true">OK</down>
<right ref="RM00000432J05BX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000432J05BX_04_0003">
<testtitle>GO TO DTC B1613/83<xref label="Seep01" href="RM00000387602PX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000432J05BX_04_0004">
<testtitle>GO TO DTC B1618/84<xref label="Seep01" href="RM0000038AX02VX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000432J05BX_04_0005">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000002Y2N00UX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000432J05BX_04_0006">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0IZX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>