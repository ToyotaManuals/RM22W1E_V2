<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12018_S001B" variety="S001B">
<name>CLUTCH</name>
<ttl id="12018_S001B_7C3MZ_T00G2" variety="T00G2">
<name>CLUTCH MASTER CYLINDER (for LHD)</name>
<para id="RM0000032QW004X" category="A" type-id="30014" name-id="CL2BX-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000032QW004X_01" type-id="01" category="01">
<s-1 id="RM0000032QW004X_01_0001" proc-id="RM22W0E___00008VN00000">
<ptxt>INSTALL CLUTCH MASTER CYLINDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the clutch master cylinder with the 2 bolts.</ptxt>
<figure>
<graphic graphicname="C173224" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>120</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Apply MP grease to the contact surface of the clevis pin and clevis bush.</ptxt>
<figure>
<graphic graphicname="C175184E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the wave washer to the clevis pin.</ptxt>
</s2>
<s2>
<ptxt>Connect the clevis of the master cylinder to the clutch pedal with the clevis pin.</ptxt>
</s2>
<s2>
<ptxt>Install a new clip to the clevis pin.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032QW004X_01_0012" proc-id="RM22W0E___00008VQ00000">
<ptxt>INSTALL CLUTCH START SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C173223" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the clutch start switch to the clutch pedal bracket, and then temporarily install the 2 nuts.</ptxt>
<atten4>
<ptxt>The 2 nuts will be tightened to a torque specification in the "Adjust Clutch Start Switch Assembly" procedure.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032QW004X_01_0002" proc-id="RM22W0E___00008VO00000">
<ptxt>INSTALL CLUTCH MASTER CYLINDER TO WAY TUBE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C249774" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a union nut wrench, install the tube to the clutch master cylinder and clutch pedal support way.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032QW004X_01_0003" proc-id="RM22W0E___00008VP00000">
<ptxt>INSTALL CLUTCH RESERVOIR HOSE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C173228" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the clutch reservoir hose to the clutch master cylinder and clutch pedal support way with 2 new clips.</ptxt>
<atten3>
<ptxt>Connect the clutch reservoir hose so that it will not be twisted.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032QW004X_01_0029" proc-id="RM22W0E___00008TV00000">
<ptxt>INSTALL CLUTCH PEDAL SUPPORT ASSEMBLY WITH CLUTCH MASTER CYLINDER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172960" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install the clutch pedal support with clutch master cylinder with the bolt and 2 nuts.</ptxt>
<torque>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
<subtitle>for nut</subtitle>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>145</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the clutch start switch connector.</ptxt>
</s2>
<s2>
<ptxt>w/ Cruise Control System:</ptxt>
<ptxt>Connect the clutch switch connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0030" proc-id="RM22W0E___00008TW00000">
<ptxt>CONNECT CLUTCH MASTER CYLINDER TO FLEXIBLE HOSE TUBE AND CLUTCH RESERVOIR TUBE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C249773" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a union nut wrench, connect the flexible hose tube.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the clutch reservoir tube with a new clip.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0031" proc-id="RM22W0E___00008TY00000">
<ptxt>INSTALL MAIN BODY ECU (COWL SIDE JUNCTION BLOCK LH)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C173730" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the 9 ECU connectors.</ptxt>
</s2>
<s2>
<ptxt>Install the ECU with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="C173731" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0009" proc-id="RM22W0E___000034P00000">
<ptxt>FILL RESERVOIR WITH BRAKE FLUID
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fill the reservoir with brake fluid.</ptxt>
<spec>
<title>Brake Fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT 3</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0011" proc-id="RM22W0E___000034Q00000">
<ptxt>BLEED CLUTCH LINE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172957" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the release cylinder bleeder plug cap.</ptxt>
</s2>
<s2>
<ptxt>Connect a vinyl tube to the bleeder plug.</ptxt>
</s2>
<s2>
<ptxt>Depress the clutch pedal several times, and then loosen the bleeder plug while the pedal is depressed.</ptxt>
</s2>
<s2>
<ptxt>When fluid no longer comes out, tighten the bleeder plug, and then release the clutch pedal.</ptxt>
</s2>
<s2>
<ptxt>Repeat the previous 2 steps until all the air in the fluid is completely bled.</ptxt>
</s2>
<s2>
<ptxt>Tighten the bleeder plug. </ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the bleeder plug cap.</ptxt>
</s2>
<s2>
<ptxt>Check that all the air has been bled from the clutch line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0008" proc-id="RM22W0E___00008TL00000">
<ptxt>INSPECT AND ADJUST CLUTCH PEDAL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the clutch pedal height is correct.</ptxt>
<figure>
<graphic graphicname="C173250E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Clutch Pedal Height from Tibia Pad</title>
<specitem>
<ptxt>162.7 to 172.7 mm (6.41 to 6.79 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Adjust the clutch pedal height.</ptxt>
<s3>
<ptxt>Loosen the lock nut and turn the stopper bolt or clutch switch until the height is correct. Tighten the lock nut.</ptxt>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>160</t-value2>
<t-value4>12</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
<s2>
<ptxt>Check that the clutch pedal free play and push rod play are correct.</ptxt>
<atten4>
<ptxt>Pay close attention to the change in resistance to distinguish between pedal free play and push rod play while performing the inspection.</ptxt>
</atten4>
<figure>
<graphic graphicname="D031462E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Depress the clutch pedal until the clutch resistance begins to be felt.</ptxt>
<spec>
<title>Standard Pedal Free Play</title>
<specitem>
<ptxt>5.0 to 15.0 mm (0.197 to 0.591 in.)</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Gently depress the clutch pedal until the resistance begins to increase a little.</ptxt>
<spec>
<title>Standard Push Rod Play at Pedal Top</title>
<specitem>
<ptxt>1.0 to 5.0 mm (0.0394 to 0.197 in.)</ptxt>
</specitem>
</spec>
</s3>
</s2>
<s2>
<ptxt>Adjust the clutch pedal free play and push rod play.</ptxt>
<atten4>
<ptxt>The push rod play can be adjusted by changing the length of the push rod. Pedal free play changes together with push rod play.</ptxt>
</atten4>
<s3>
<ptxt>Loosen the lock nut and turn the clutch push rod until the clutch pedal free play and push rod play are correct.</ptxt>
<atten3>
<ptxt>If pedal free play and push rod play are not within the standard range even after adjustment, inspect the related parts.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Tighten the lock nut.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>120</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>After adjusting the clutch pedal free play and push rod play, check the clutch pedal height.</ptxt>
<spec>
<title>Standard Clutch Pedal Height from Tibia Pad</title>
<specitem>
<ptxt>162.7 to 172.7 mm (6.41 to 6.79 in.)</ptxt>
</specitem>
</spec>
</s3>
</s2>
<s2>
<ptxt>Check the clutch release point.</ptxt>
<figure>
<graphic graphicname="CL00512E29" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Pull the parking brake lever and chock the wheels.</ptxt>
</s3>
<s3>
<ptxt>Start the engine and allow it to idle.</ptxt>
</s3>
<s3>
<ptxt>Without depressing the clutch pedal, slowly move the shift lever to R until the gears engage.</ptxt>
</s3>
<s3>
<ptxt>Gradually depress the clutch pedal and measure the stroke distance from the point that the gear noise stops (release point) up to the full stroke end position.</ptxt>
<spec>
<title>Standard Distance from Pedal Stroke End Position to Release Point</title>
<specitem>
<ptxt>20 mm (0.787 in.) or more</ptxt>
</specitem>
</spec>
<ptxt>If the distance is not as specified, perform the following operations:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Check the clutch pedal height.</ptxt>
</item>
<item>
<ptxt>Check the push rod play and pedal free play.</ptxt>
</item>
<item>
<ptxt>Bleed the clutch line.</ptxt>
</item>
<item>
<ptxt>Check the clutch cover and disc.</ptxt>
</item>
</list1>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0013" proc-id="RM22W0E___00008VR00000">
<ptxt>ADJUST CLUTCH START SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>While depressing the clutch pedal, tighten the 2 nuts so that the clearance between the clutch pedal and clutch start switch is within the specification.</ptxt>
<figure>
<graphic graphicname="C175188" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard clearance between clutch pedal and clutch start switch</title>
<specitem>
<ptxt>4.1 to 6.1 mm (0.162 to 0.240 in.)</ptxt>
</specitem>
</spec>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032QW004X_01_0010" proc-id="RM22W0E___000034R00000">
<ptxt>CHECK FLUID LEVEL IN RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the fluid level.</ptxt>
<ptxt>If the brake fluid level is low, check for leaks and inspect the disc brake pad. If necessary, refill the reservoir with brake fluid after repair or replacement.</ptxt>
<spec>
<title>Brake fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT 3</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0007">
<ptxt>INSPECT FOR CLUTCH FLUID LEAK</ptxt>
</s-1>
<s-1 id="RM0000032QW004X_01_0032" proc-id="RM22W0E___0000BAN00000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL SUB-ASSEMBLY (w/o Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180299" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 claws and connect the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Install the lower instrument panel with the 5 bolts.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0033" proc-id="RM22W0E___0000AT300000">
<ptxt>INSTALL DRIVER SIDE KNEE AIRBAG ASSEMBLY (w/ Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B189604E01" width="2.775699831in" height="6.791605969in"/>
</figure>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the driver side knee airbag with the 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0034" proc-id="RM22W0E___0000A9H00000">
<ptxt>INSTALL NO. 1 SWITCH HOLE BASE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180298" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 claws to install the No. 1 switch hole base.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0035" proc-id="RM22W0E___0000A9I00000">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Attach the 2 claws to install the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to connect the 2 control cables.</ptxt>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 16 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 9 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B182569" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to close the hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0036" proc-id="RM22W0E___0000A9J00000">
<ptxt>INSTALL COWL SIDE TRIM BOARD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181911" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 clips to install the cowl side trim board.</ptxt>
</s2>
<s2>
<ptxt>Install the cap nut.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0037" proc-id="RM22W0E___000014600000">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY (w/ Floor Under Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182553" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 3 claws to install the No. 1 instrument panel under cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0038" proc-id="RM22W0E___0000BNS00000">
<ptxt>INSTALL FRONT DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0039" proc-id="RM22W0E___0000BAG00000">
<ptxt>INSTALL NO. 2 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the No. 2 instrument cluster finish panel garnish.</ptxt>
<figure>
<graphic graphicname="B291252E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Entry and Start System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0040" proc-id="RM22W0E___0000BAH00000">
<ptxt>INSTALL NO. 1 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E155133" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws to install the No. 1 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0041" proc-id="RM22W0E___0000BAI00000">
<ptxt>INSTALL INSTRUMENT SIDE PANEL LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E155518" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 6 claws to install the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0042" proc-id="RM22W0E___00008U800000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292995" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Connect the connectors and attach the 2 clamps.</ptxt>
</s2>
<s2>
<ptxt>Attach the 8 claws to install the lower instrument panel pad sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Install the clip and screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032QW004X_01_0043" proc-id="RM22W0E___00008U900000">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292997" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 7 claws to install the No. 2 instrument panel finish panel cushion.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>