<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SE_T00LH" variety="T00LH">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000002U4V03AX" category="J" type-id="8038X" name-id="NW3DR-03" from="201308">
<dtccode/>
<dtcname>Rear Heater Control Panel LIN Communication Malfunction</dtcname>
<subpara id="RM000002U4V03AX_01" type-id="60" category="03" proc-id="RM22W0E___0000E4100001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The LIN communication of the components related to the rear air conditioning occurs between the air conditioning amplifier (A/C ECU), rear air conditioning control panel and front air conditioning control panel.</ptxt>
</content5>
</subpara>
<subpara id="RM000002U4V03AX_02" type-id="32" category="03" proc-id="RM22W0E___0000E4200001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E237270E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="E237271E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002U4V03AX_03" type-id="51" category="05" proc-id="RM22W0E___0000E4300001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot:</ptxt>
<ptxt>Connect the intelligent tester to the vehicle, and turn a courtesy switch on and off at 1.5 second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002U4V03AX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002U4V03AX_04_0015" proc-id="RM22W0E___0000E4900001">
<testtitle>CHECK AIR CONDITIONING TYPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check air conditioning type.</ptxt>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>for 4-ZONE Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>except 4-ZONE Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000002U4V03AX_04_0001" fin="false">A</down>
<right ref="RM000002U4V03AX_04_0016" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0001" proc-id="RM22W0E___0000E4400001">
<testtitle>CHECK HARNESS AND CONNECTOR (A/C ECU - REAR AIR CONDITIONING CONTROL PANEL AND FRONT AIR CONDITIONING CONTROL PANEL*)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E156406E01" width="2.775699831in" height="5.787629434in"/>
</figure>
<atten4>
<ptxt>*: w/o Navigation System</ptxt>
</atten4>
<test1>
<ptxt>Disconnect the E36 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the F10* and E55 panel connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E36-21 (RLIN) - E55-4 (RLIN)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E36-20 (LIN1) - F10-5 (LIN1)*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E36-21 (RLIN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E36-20 (LIN1) - Body ground*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E36-21 (RLIN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E36-20 (LIN1) - Body ground*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002U4V03AX_04_0002" fin="false">OK</down>
<right ref="RM000002U4V03AX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0002" proc-id="RM22W0E___0000E4500001">
<testtitle>CHECK AIR CONDITIONING CONTROL PANEL (BATTERY VOLTAGE AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E55 panel connector.</ptxt>
<figure>
<graphic graphicname="E156408E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E55-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E55-6 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002U4V03AX_04_0003" fin="false">OK</down>
<right ref="RM000002U4V03AX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0003" proc-id="RM22W0E___0000E4600001">
<testtitle>CHECK REAR AIR CONDITIONING CONTROL PANEL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the rear air conditioning control panel with a new or normally functioning one (See page <xref label="Seep01" href="RM000003A65003X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check that the rear air conditioning function is normal.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Rear air conditioning function is normal.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/ Navigation System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/o Navigation System) (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/o Navigation System) (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002U4V03AX_04_0007" fin="true">A</down>
<right ref="RM000002U4V03AX_04_0008" fin="false">B</right>
<right ref="RM000002U4V03AX_04_0013" fin="true">C</right>
<right ref="RM000002U4V03AX_04_0006" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0007">
<testtitle>END (REAR AIR CONDITIONING CONTROL PANEL IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0008" proc-id="RM22W0E___0000E4700001">
<testtitle>CHECK FRONT AIR CONDITIONING CONTROL PANEL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F10 panel connector.</ptxt>
<figure>
<graphic graphicname="E156422E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F10-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F10-7 (IG+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002U4V03AX_04_0009" fin="false">OK</down>
<right ref="RM000002U4V03AX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0009" proc-id="RM22W0E___0000E4800001">
<testtitle>CHECK FRONT AIR CONDITIONING CONTROL PANEL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the front air conditioning control panel with a new or normally functioning one (See page <xref label="Seep01" href="RM000003B4601NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the that front air conditioning function is normal.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Front air conditioning function is normal.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002U4V03AX_04_0011" fin="true">A</down>
<right ref="RM000002U4V03AX_04_0014" fin="true">B</right>
<right ref="RM000002U4V03AX_04_0020" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0011">
<testtitle>END (FRONT AIR CONDITIONING CONTROL PANEL IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0016" proc-id="RM22W0E___0000E4A00001">
<testtitle>CHECK HARNESS AND CONNECTOR (A/C ECU - FRONT AIR CONDITIONING CONTROL PANEL)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E237401E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the E81 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the F10 panel connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E81-37 (LIN1) - F10-5 (LIN1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E81-37 (LIN1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E81-37 (LIN1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002U4V03AX_04_0017" fin="false">OK</down>
<right ref="RM000002U4V03AX_04_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0017" proc-id="RM22W0E___0000E4B00001">
<testtitle>CHECK FRONT AIR CONDITIONING CONTROL ASSEMBLY (BATTERY VOLTAGE AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the F10 panel connector.</ptxt>
<figure>
<graphic graphicname="E156422E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the tables below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F10-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F10-7 (IG+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002U4V03AX_04_0018" fin="false">OK</down>
<right ref="RM000002U4V03AX_04_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0018" proc-id="RM22W0E___0000E4C00001">
<testtitle>CHECK FRONT AIR CONDITIONING CONTROL ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the front air conditioning control panel with a new or normally functioning one (See page <xref label="Seep01" href="RM000003B4601NX"/>).</ptxt>
<ptxt>Check the that front air conditioning function is normal.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Front air conditioning function is normal.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002U4V03AX_04_0019" fin="true">A</down>
<right ref="RM000002U4V03AX_04_0023" fin="true">B</right>
<right ref="RM000002U4V03AX_04_0024" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0019">
<testtitle>END (FRONT AIR CONDITIONING CONTROL PANEL IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0013">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER<xref label="Seep01" href="RM0000039R501QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0006">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER<xref label="Seep01" href="RM0000039R501QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0010">
<testtitle>REPLACE FRONT AIR CONDITIONING PANEL<xref label="Seep01" href="RM000003B4601NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0014">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER<xref label="Seep01" href="RM0000039R501QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0020">
<testtitle>REPLACE FRONT AIR CONDITIONING CONTROL ASSEMBLY<xref label="Seep01" href="RM000003B4601MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0021">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0022">
<testtitle>REPLACE FRONT AIR CONDITIONING CONTROL ASSEMBLY<xref label="Seep01" href="RM000003B4601NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0023">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U4V03AX_04_0024">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501QX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>