<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MR_T00FU" variety="T00FU">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 3UR-FE)</name>
<para id="RM0000030G909GX" category="S" type-id="3001E" name-id="AT023O-266" from="201308">
<name>DIAGNOSTIC TROUBLE CODE CHART</name>
<subpara id="RM0000030G909GX_z0" proc-id="RM22W0E___00008BB00001">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a DTC is displayed during the DTC check, check the parts listed in the table below and proceed to the "See page" given.</ptxt>
</item>
<item>
<ptxt>*1: "Comes on" means the Malfunction Indicator Lamp (MIL) illuminates.</ptxt>
</item>
<item>
<ptxt>*2: "DTC stored" means the ECM memorizes the trouble code if the ECM detects the DTC detection condition.</ptxt>
</item>
<item>
<ptxt>*3: The ATF temperature warning light blinks or a message is displayed in the combination meter.</ptxt>
</item>
<item>
<ptxt>These DTCs may be output when the clutch, brake, gear components, etc., inside the automatic transmission are damaged.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Automatic Transmission System</title>
<tgroup cols="5" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="1.42in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="1.42in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.42in" colsep="0"/>
<colspec colnum="4" colname="4" colwidth="1.42in" colsep="0"/>
<colspec colnum="5" colname="5" colwidth="1.4in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>MIL*1</ptxt>
</entry>
<entry colname="4" colsep="1" align="center">
<ptxt>Memory*2</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0705</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Range Sensor Circuit Malfunction (PRNDL Input)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8412HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0712</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Fluid Temperature Sensor "A" Circuit Low Input</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W850Q7X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0713</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Fluid Temperature Sensor "A" Circuit High Input</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W850Q7X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0717</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Turbine Speed Sensor Circuit No Signal</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W7Y0B3X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0722</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Output Speed Sensor Circuit No Signal</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM0000012TD08JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0729</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Gear 6 Incorrect Ratio</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8I06QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0748</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Pressure Control Solenoid "A" Electrical (Shift Solenoid Valve SL1)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8F0HZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0751</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "A" Performance (Shift Solenoid Valve S1)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8J0DAX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0761</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "C" Performance (Shift Solenoid Valve S3)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8106QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0766</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "D" Performance (Shift Solenoid Valve S4)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W820BGX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0776</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Pressure Control Solenoid "B" Performance (Shift Solenoid Valve SL2)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W820BGX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0778</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Pressure Control Solenoid "B" Electrical (Shift Solenoid Valve SL2)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8G0H2X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0781</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>1-2 Shift (1-2 Shift Valve)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000XP10AYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0973</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "A" Control Circuit Low (Shift Solenoid Valve S1)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W860DYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0974</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "A" Control Circuit High (Shift Solenoid Valve S1)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W860DYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0976</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "B" Control Circuit Low (Shift Solenoid Valve S2)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W870DPX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0977</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "B" Control Circuit High (Shift Solenoid Valve S2)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W870DPX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0979</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "C" Control Circuit Low (Shift Solenoid Valve S3)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8B06LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0980</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "C" Control Circuit High (Shift Solenoid Valve S3)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8B06LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0982</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "D" Control Circuit Low (Shift Solenoid Valve S4)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8C08BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0983</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "D" Control Circuit High (Shift Solenoid Valve S4)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8C08BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0985</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "E" Control Circuit Low (Shift Solenoid Valve SR)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8H0BUX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P0986</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Shift Solenoid "E" Control Circuit High (Shift Solenoid Valve SR)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8H0BUX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2714</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Pressure Control Solenoid "D" Performance (Shift Solenoid Valve SLT)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W830LWX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2716</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Pressure Control Solenoid "D" Electrical (Shift Solenoid Valve SLT)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W8A0KXX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2742</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Fluid Temperature Sensor "B" Circuit Low Input</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-*3</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM0000012XR08SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2743</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Transmission Fluid Temperature Sensor "B" Circuit High Input</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-*3</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM0000012XR08SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>P2759</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Torque Converter Clutch Pressure Control Solenoid Control Circuit Electrical (Shift Solenoid Valve SLU)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000W890IIX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>