<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UI_T00NL" variety="T00NL">
<name>FRONT POWER SEAT CONTROL SYSTEM (w/ Seat Position Memory System)</name>
<para id="RM000002RBB03DX" category="J" type-id="304PA" name-id="SE1L1-12" from="201301">
<dtccode/>
<dtcname>Wireless-linked Return Function does not Operate</dtcname>
<subpara id="RM000002RBB03DX_01" type-id="60" category="03" proc-id="RM22W0E___0000FSF00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When a door is unlocked, the certification ECU (smart key ECU) sends a door unlock signal and key ID signal to the position control ECU and switch. When the position control ECU and switch receives the door unlock signal, it stores the key ID. When the driver door is opened, the memory call and memory command information of the stored key ID is sent to each driving position related control ECU. Each control ECU receives the memory commands and performs the memory call operation according to predetermined control timing values.</ptxt>
</content5>
</subpara>
<subpara id="RM000002RBB03DX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002RBB03DX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002RBB03DX_03_0001" proc-id="RM22W0E___0000FSG00000">
<testtitle>CHECK ENTRY UNLOCK FUNCTION AND WIRELESS UNLOCK FUNCTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the driver side door unlocks when holding the driver side door outside handle, or when pressing the transmitter unlock switch.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Driver side door unlocks normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RBB03DX_03_0002" fin="false">OK</down>
<right ref="RM000002RBB03DX_03_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RBB03DX_03_0002" proc-id="RM22W0E___0000FSH00000">
<testtitle>CHECK POWER SEAT CONTROL FUNCTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Manually operate the driver seat power seat switches, and check if the seat operates normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Seat operates normally through manual operation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RBB03DX_03_0003" fin="false">OK</down>
<right ref="RM000002RBB03DX_03_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RBB03DX_03_0003" proc-id="RM22W0E___0000FSI00000">
<testtitle>CHECK SEAT MEMORY SWITCH FUNCTION (SEAT POSITION MEMORY FUNCTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform a memory operation properly. Check that the buzzer sounds to indicate the completion of the memory operation.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The seat position will not be recorded if the seat memory SET switch and 2 or more of the seat memory switches (for example, seat memory switch 1 and seat memory switch 2) are pressed simultaneously.</ptxt>
</item>
<item>
<ptxt>If a memorizing operation has failed, release all switches. The seat memory function does not operate unless the switches are released.</ptxt>
</item>
</list1>
</atten3>
<spec>
<title>OK</title>
<specitem>
<ptxt>Seat memory switch function operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RBB03DX_03_0004" fin="false">OK</down>
<right ref="RM000002RBB03DX_03_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RBB03DX_03_0004" proc-id="RM22W0E___0000FSJ00000">
<testtitle>CHECK SEAT MEMORY SWITCH FUNCTION (SEAT RESTORING OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Press the seat memory switch 1, seat memory switch 2 or seat memory switch 3 and check that the seat moves to a recorded position.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Seat moves to recorded position.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RBB03DX_03_0010" fin="false">OK</down>
<right ref="RM000002RBB03DX_03_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RBB03DX_03_0010" proc-id="RM22W0E___0000FSL00000">
<testtitle>CHECK MEMORY CALL FUNCTION (REGISTER RECOGNITION CODE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>While pressing the seat memory switch 1, seat memory switch 2 or seat memory switch 3, press the transmitter lock or unlock switch. Then check that the buzzer sounds normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Buzzer sounds normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RBB03DX_03_0005" fin="false">OK</down>
<right ref="RM000002RBB03DX_03_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RBB03DX_03_0005" proc-id="RM22W0E___0000FSK00000">
<testtitle>CHECK MEMORY CALL FUNCTION (SEAT MEMORY CALL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>With a recognition code stored in memory, open the driver door by performing an entry unlock operation or by pressing the transmitter unlock switch. Check that the driver side power seat moves to the memorized position.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Driver side power seat moves to the memorized position.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RBB03DX_03_0006" fin="true">OK</down>
<right ref="RM000002RBB03DX_03_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RBB03DX_03_0006">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000002RBB03DX_03_0007">
<testtitle>GO TO ENTRY AND START SYSTEM OR WIRELESS DOOR LOCK CONTROL SYSTEM</testtitle>
</testgrp>
<testgrp id="RM000002RBB03DX_03_0008">
<testtitle>GO TO PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000039RM039X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002RBB03DX_03_0009">
<testtitle>REPLACE POSITION CONTROL ECU AND SWITCH<xref label="Seep01" href="RM00000390T01LX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>