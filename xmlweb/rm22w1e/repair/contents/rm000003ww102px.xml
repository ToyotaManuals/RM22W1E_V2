<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001X" variety="S001X">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001X_7C3RY_T00L1" variety="T00L1">
<name>PARKING ASSIST MONITOR SYSTEM (w/ Side Monitor System)</name>
<para id="RM000003WW102PX" category="C" type-id="8038Y" name-id="PM2AM-50" from="201308">
<dtccode>C1611</dtccode>
<dtcname>ECU Malfunction</dtcname>
<subpara id="RM000003WW102PX_01" type-id="60" category="03" proc-id="RM22W0E___0000D0300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored if the parking assist ECU judges that there is an internal malfunction as a result of its self check.</ptxt>
<atten4>
<ptxt>The parking assist ECU stores different types of information during initialization. If the parking assist ECU cannot read the stored information when it is activated, the parking assist ECU judges that there is an internal malfunction and stores this DTC.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1611</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>EEPROM Status (Initial read)</ptxt>
</item>
<item>
<ptxt>AISIC Output Status</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Parking assist ECU initialization (not initialized parking assist ECU)</ptxt>
</item>
<item>
<ptxt>Back camera position setting (initialized parking assist ECU)</ptxt>
</item>
<item>
<ptxt>Side camera position setting (initialized parking assist ECU)</ptxt>
</item>
<item>
<ptxt>Steering Angle setting (initialized parking assist ECU)</ptxt>
</item>
<item>
<ptxt>Parking assist ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003WW102PX_02" type-id="51" category="05" proc-id="RM22W0E___0000D0400001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When "System initializing" is displayed on the parking assist ECU after the battery terminal disconnected, correct the steering angle neutral point (See page <xref label="Seep01" href="RM0000035DE03CX"/>).</ptxt>
</item>
<item>
<ptxt>Depending on the parts that are replaced or operations that are performed during vehicle inspection or maintenance, calibration of other systems as well as the parking assist monitor system (w/ Side Monitor System) may be needed (See page <xref label="Seep02" href="RM0000035DD03SX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000003WW102PX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003WW102PX_03_0001" proc-id="RM22W0E___0000D0500001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (PARKING ASSIST ECU STATUS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the parking assist ECU status is functioning properly (See page <xref label="Seep01" href="RM000003WW0039X"/>). </ptxt>
<table pgwide="1">
<title>Parking Assist Monitor System</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>EEPROM Status (Initial read)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Parking assist ECU information / OK or NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Parking assist ECU is normal</ptxt>
<ptxt>NG: Parking assist ECU is abnormal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AISIC Output Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output condition of camera display signal / OK or NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Output condition of camera display signal is normal</ptxt>
<ptxt>NG: Output condition of camera display signal is abnormal</ptxt>
</entry>
<entry valign="middle">
<ptxt>If displayed "NG", There is a malfunctions in parking assist ECU.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>OK is displayed for both items.</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>NG is displayed for EEPROM Status (Initial read).</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>NG is displayed for AISIC Output Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003WW102PX_03_0002" fin="false">A</down>
<right ref="RM000003WW102PX_03_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003WW102PX_03_0002" proc-id="RM22W0E___0000D0600001">
<testtitle>SETTINGS (PARKING ASSIST ECU INITIALIZATION)</testtitle>
<content6 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>The parking assist ECU stores different types of information during initialization. If the parking assist ECU cannot read the stored information when it is activated, the parking assist ECU judges that there is an internal malfunction and stores this DTC.</ptxt>
</item>
<item>
<ptxt>If the parking assist ECU cannot read the stored information when it is activated, allowing the ECU to store the information again may return the system to normal.</ptxt>
</item>
</list1>
</atten4>
<test1>
<ptxt>Use the transition of diagnosis screens (note which transition pattern occurs) to confirm if the parking assist ECU was able to properly store the information from initialization (See page <xref label="Seep01" href="RM000003WVZ03QX"/>).</ptxt>
<atten4>
<ptxt>The procedure to make the parking assist ECU store the vehicle information differs when the parking assist ECU has stored the information (initialized) and when the information has not been stored (not initialized).</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Perform the following procedure depending on the memory conditions to register the vehicle information into the parking assist ECU.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="2.75in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Procedure</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Not initialized</ptxt>
</entry>
<entry valign="middle">
<ptxt>Parking assist ECU initialization (See page <xref label="Seep02" href="RM0000035DD03SX"/>)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Initialized</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Back camera position setting (See page <xref label="Seep04" href="RM0000035DD03SX"/>)</ptxt>
</item>
<item>
<ptxt>Steering angle setting (See page <xref label="Seep05" href="RM0000035DD03SX"/>)</ptxt>
</item>
<item>
<ptxt>Side camera position setting (See page<xref label="Seep03" href="RM000003XM401EX"/> )</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003WW102PX_03_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003WW102PX_03_0003" proc-id="RM22W0E___0000D0700001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000035DB03WX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000035DB03WX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC C1611 is not output.</ptxt>
</specitem>
</spec>
</test1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTC C1611 is output, perform the procedure to allow the parking assist ECU to store the vehicle information again.</ptxt>
</item>
<item>
<ptxt>If DTC C1611 is still output after performing the procedure to make the parking assist ECU store the vehicle information 3 times, replace the parking assist ECU.</ptxt>
</item>
<item>
<ptxt>If DTC C1611 is output frequently, replace the parking assist ECU even though the DTC is not output when rechecking for DTCs.</ptxt>
</item>
</list1>
</atten4>
</content6>
<res>
<down ref="RM000003WW102PX_03_0005" fin="true">OK</down>
<right ref="RM000003WW102PX_03_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003WW102PX_03_0005">
<testtitle>END (SETTINGS ARE DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000003WW102PX_03_0004">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM0000039LH004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>