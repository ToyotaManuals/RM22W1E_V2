<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12037_S001R" variety="S001R">
<name>VARIABLE GEAR RATIO STEERING</name>
<ttl id="12037_S001R_7C3Q8_T00JB" variety="T00JB">
<name>VARIABLE GEAR RATIO STEERING SYSTEM</name>
<para id="RM000000YU801MX" category="C" type-id="800UW" name-id="VG007-04" from="201301" to="201308">
<dtccode>C15A5/65</dtccode>
<dtcname>Actuator Malfunction</dtcname>
<subpara id="RM000000YU801MX_01" type-id="60" category="03" proc-id="RM22W0E___0000B9C00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the steering control ECU detects a malfunction in the lock mechanism, it will turn the master warning light on, store DTC C15A5/65, and stop VGRS operation.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.22in"/>
<colspec colname="COL2" colwidth="3.50in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C15A5/65</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The steering control ECU detects that the LG terminal voltage is abnormal.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Steering actuator</ptxt>
</item>
<item>
<ptxt>Steering control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000YU801MX_02" type-id="32" category="03" proc-id="RM22W0E___0000B9D00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C163901E05" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000YU801MX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000YU801MX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YU801MX_04_0001" proc-id="RM22W0E___0000B9E00000">
<testtitle>CHECK HARNESS AND CONNECTOR (STEERING CONTROL ECU - STEERING ACTUATOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C175600E02" width="7.106578999in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the E79 steering control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E80 steering actuator connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.63in"/>
<colspec colname="COLSPEC0" colwidth="1.25in"/>
<colspec colname="COL2" colwidth="1.25in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E79-3 (LV) - E80-7 (LV+)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E79-12 (LG) - E80-16 (LG+)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E79-3 (LV) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E79-12 (LG) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.63in"/>
<colspec colname="COLSPEC1" colwidth="1.22in"/>
<colspec colname="COL2" colwidth="1.28in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E79-3 (LV) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E79-12 (LG) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000YU801MX_04_0002" fin="false">OK</down>
<right ref="RM000000YU801MX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YU801MX_04_0002" proc-id="RM22W0E___0000B9F00000">
<testtitle>CHECK STEERING ACTUATOR (STEERING CONTROL ECU - STEERING ACTUATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E79 steering control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the E80 steering actuator connector.</ptxt>
</test1>
<figure>
<graphic graphicname="C166973E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.71in"/>
<colspec colname="COLSPEC2" colwidth="1.18in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E79-3 (LV) - E79-12 (LG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 to 100 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E79-3 (LV) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E79-12 (LG) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<title>Result</title>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YU801MX_04_0005" fin="true">A</down>
<right ref="RM000000YU801MX_04_0003" fin="true">B</right>
<right ref="RM000000YU801MX_04_0006" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000YU801MX_04_0003">
<testtitle>REPLACE STEERING CONTROL ECU<xref label="Seep01" href="RM000003DZD00EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YU801MX_04_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000YU801MX_04_0005">
<testtitle>REPLACE STEERING ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000039SI020X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YU801MX_04_0006">
<testtitle>REPLACE STEERING CONTROL ECU<xref label="Seep01" href="RM000003EFD004X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>