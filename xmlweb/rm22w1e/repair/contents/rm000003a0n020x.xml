<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12067_S002J" variety="S002J">
<name>MIRROR (EXT)</name>
<ttl id="12067_S002J_7C3Y2_T00R5" variety="T00R5">
<name>OUTER REAR VIEW MIRROR</name>
<para id="RM000003A0N020X" category="A" type-id="8000E" name-id="MX3CX-01" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM000003A0N020X_02" type-id="11" category="10" proc-id="RM22W0E___0000IO600000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003A0N020X_01" type-id="01" category="01">
<s-1 id="RM000003A0N020X_01_0004" proc-id="RM22W0E___0000IO500000">
<ptxt>INSTALL SIDE TELEVISION CAMERA ASSEMBLY (w/ Side Monitor System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the side television camera with the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Install the body.</ptxt>
<s3>
<ptxt>Attach the 2 guides and body.</ptxt>
<atten3>
<ptxt>Be careful not to break the guide.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Install the body cover.</ptxt>
<s3>
<ptxt>Install the body with the 5 screws.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B278341" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the connector to the wire clip.</ptxt>
</s2>
<s2>
<ptxt>Install the base</ptxt>
<s3>
<ptxt>Attach the guide.</ptxt>
</s3>
<s3>
<ptxt>Using "TORX" socket wrench T25, install the base with 3 new screws.</ptxt>
<atten3>
<ptxt>When installing the base, check that the wire harness is not caught between the base and housing. Failure to do so may cause a short circuit.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Install the lower mirror cover.</ptxt>
<s3>
<ptxt>Attach the 4 claws and install a new lower mirror cover.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install the gasket.</ptxt>
<s3>
<ptxt>Attach the 9 guides and install the gasket.</ptxt>
</s3>
<s3>
<ptxt>Align the marking on the wire harness with the marking on the end of the wire harness insertion hole of the gasket as shown in the illustration, and wrap them with new vinyl tape starting from the gasket marking.</ptxt>
<figure>
<graphic graphicname="B278323E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>New Vinyl Tape</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Marking</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Area</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>a</ptxt>
</entry>
<entry>
<ptxt>50.0 mm (1.97 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>If the vinyl tape is wrinkled, water may run down the wire harness and enter the vehicle. Make sure to wind the vinyl tape properly.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Wrap new vinyl tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B302367E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>New Vinyl Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Area</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>a</ptxt>
</entry>
<entry>
<ptxt>30.0 mm (1.18 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>b</ptxt>
</entry>
<entry>
<ptxt>50.0 mm (1.97 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A0N020X_01_0007" proc-id="RM22W0E___0000CWP00000">
<ptxt>INSTALL SIDE TURN SIGNAL LIGHT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Install the light with the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003A0N020X_01_0001" proc-id="RM22W0E___0000CWM00000">
<ptxt>INSTALL OUTER MIRROR COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the outer mirror cover.</ptxt>
<s3>
<figure>
<graphic graphicname="B302369E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Insert the rib on the outer edge of the cover into the groove of the mirror body.</ptxt>
<atten3>
<ptxt>Do not scratch the turn light.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Rib</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<figure>
<graphic graphicname="B302371" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>While making sure that the rib of the cover fits properly into the groove of the mirror body, squeeze the inner end of the cover and the mirror body together to attach the inner claws.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B302372" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Push on the cover at the locations of the 8 claws to confirm that the claws are attached properly.</ptxt>
</s2>
<s2>
<ptxt>Check that there is no gap between the cover and mirror body.</ptxt>
<atten4>
<ptxt>If there is a gap between the cover and body, noise will occur during driving.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A0N020X_01_0005" proc-id="RM22W0E___0000CWL00000">
<ptxt>INSTALL OUTER MIRROR LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B189668" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>w/ EC Mirror:</ptxt>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B189669" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>w/ Mirror Heater:</ptxt>
<ptxt>Connect the 2 connectors.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B183885" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Attach the 4 claws to install the outer mirror LH.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>