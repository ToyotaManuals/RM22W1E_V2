<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LY_T00F1" variety="T00F1">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM000000ST109RX" category="U" type-id="303FP" name-id="CC41N-01" from="201301">
<name>FAIL-SAFE CHART</name>
<subpara id="RM000000ST109RX_z0" proc-id="RM22W0E___00007B100000">
<content5 releasenbr="1">
<step1>
<ptxt>Constant speed control mode and vehicle-to-vehicle distance control mode:</ptxt>
<figure>
<graphic graphicname="E247331E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master Warning Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Constant Speed Control Mode)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Vehicle-to-vehicle Distance Control Mode)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the following conditions are detected while the dynamic radar cruise control system is in operation, the system clears the stored vehicle speed in the ECM and cancels the dynamic radar cruise control operation.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Auto Cancel Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-safe Deactivation Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Cruise control indicator light turns off</ptxt>
</item>
<item>
<ptxt>Master warning light comes on</ptxt>
</item>
<item>
<ptxt>"CHECK CRUISE CONTROL SYSTEM" is displayed</ptxt>
</item>
<item>
<ptxt>"Pong" warning sound is heard</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>There is an open or short in the stop light switch circuit</ptxt>
</item>
<item>
<ptxt>There is a problem with the vehicle speed signal</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Turn the cruise control main switch on again</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>Constant speed control mode and vehicle-to-vehicle distance control mode:</ptxt>
<ptxt>If the following conditions are detected while the dynamic radar cruise control system is in operation, the system clears the stored vehicle speed in the ECM and prohibits the dynamic radar cruise control operation.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Auto Cancel Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-safe Deactivation Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Cruise control indicator light turns off</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is a problem with the throttle position sensor and the motor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turn the cruise control main switch on again</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>Constant speed control mode and vehicle-to-vehicle distance control mode:</ptxt>
<figure>
<graphic graphicname="E247331E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master Warning Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Constant Speed Control Mode)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Vehicle-to-vehicle Distance Control Mode)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the following conditions are detected while the dynamic radar cruise control system is in operation, the system clears the stored vehicle speed in the ECM and cancels the dynamic radar cruise control operation.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Auto Cancel Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-safe Deactivation Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Cruise control indicator light turns off</ptxt>
</item>
<item>
<ptxt>Master warning light comes on</ptxt>
</item>
<item>
<ptxt>"CHECK CRUISE CONTROL SYSTEM" is displayed</ptxt>
</item>
<item>
<ptxt>"Pong" warning sound is heard</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>There is a problem with the stop light switch input circuit</ptxt>
</item>
<item>
<ptxt>There is a problem with the cancel circuit</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Turn the engine switch off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>Vehicle-to-vehicle distance control mode:</ptxt>
<figure>
<graphic graphicname="E247331E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master Warning Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Constant Speed Control Mode)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Vehicle-to-vehicle Distance Control Mode)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the following conditions are detected while the dynamic radar cruise control system is in operation, the system clears the stored vehicle speed in the ECM and cancels the dynamic radar cruise control operation.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Auto Cancel Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-safe Deactivation Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Cruise control indicator light (vehicle-to-vehicle distance control mode) turns off</ptxt>
</item>
<item>
<ptxt>Master warning light comes on</ptxt>
</item>
<item>
<ptxt>"CHECK CRUISE CONTROL SYSTEM" is displayed</ptxt>
</item>
<item>
<ptxt> "Pong" warning sound is heard</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>There is a problem with the millimeter wave radar sensor</ptxt>
</item>
<item>
<ptxt>The beam axis of the millimeter wave radar sensor deviates</ptxt>
</item>
<item>
<ptxt>There is a problem with the radar cruise control system</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Turn the engine switch on (IG)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt> Vehicle-to-vehicle distance control mode:</ptxt>
<figure>
<graphic graphicname="E247332E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master Warning Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Vehicle-to-vehicle Distance Control Mode)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the following conditions are detected while the dynamic radar cruise control system is in operation, the system maintains the stored vehicle speed in the ECM and cancels the dynamic radar cruise control operation.</ptxt>
</step1>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Auto Cancel Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-safe Deactivation Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Cruise control indicator light (vehicle-to-vehicle distance control mode) turns off</ptxt>
</item>
<item>
<ptxt>Master warning light comes on</ptxt>
</item>
<item>
<ptxt>"CLEAN RADAR SENSOR" is displayed</ptxt>
</item>
<item>
<ptxt>"Pong" warning sound is heard</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>There is dirt on the millimeter wave radar sensor.</ptxt>
<atten4>
<ptxt>When there is snow or water droplets on the front grill, the fail-safe function may operate.</ptxt>
</atten4>
</entry>
<entry valign="middle">
<ptxt>Recover from the condition on left (RESUME operation is possible after recovery)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<step1>
<ptxt>Vehicle-to-vehicle distance control mode:</ptxt>
<figure>
<graphic graphicname="E247333E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master Warning Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise Control Indicator Light (Vehicle-to-vehicle Distance Control Mode)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the following conditions are detected while the dynamic radar cruise control system is in operation, the system maintains the stored vehicle speed in the ECM and cancels the dynamic radar cruise control operation.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Auto Cancel Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-safe Deactivation Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Cruise control indicator light (vehicle-to-vehicle distance control mode) turns off</ptxt>
</item>
<item>
<ptxt>Master warning light comes on</ptxt>
</item>
<item>
<ptxt>"CRUISE CONTROL NOT AVAILABLE" is displayed</ptxt>
</item>
<item>
<ptxt>"Pong" warning sound is heard</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>The wiper switch is in HI position</ptxt>
</item>
<item>
<ptxt>The ECT 2nd Start switch is on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Recover from the condition on left (RESUME operation is possible after recovery)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>Constant speed control mode:</ptxt>
<ptxt>If the actual vehicle speed is 16 km/h (10 mph) less than the set vehicle speed while the dynamic radar cruise control system is in operation, the system clears the stored vehicle speed in the ECM and cancels the dynamic radar cruise control operation, as the load on the vehicle may be excessive or the set vehicle speed may be set too high.</ptxt>
</step1>
<step1>
<ptxt>Constant speed control mode and vehicle-to-vehicle distance control mode:</ptxt>
<ptxt>If the actual vehicle speed drops below the low speed limit (approximately 50 km/h [30 mph]) while the dynamic radar cruise control system is in operation, the system maintains the stored vehicle speed in the ECM when in vehicle-to-vehicle distance control mode and clears the stored vehicle speed in the ECM when in constant speed control mode and cancels the dynamic radar cruise control operation.</ptxt>
</step1>
<step1>
<ptxt>Constant speed control mode and vehicle-to-vehicle distance control mode:</ptxt>
<ptxt>If the VSC system operates or a cancel request is received from the 4WD system while the dynamic radar cruise control system is in operation, the system maintains the stored vehicle speed in the ECM and cancels the dynamic radar cruise control operation.</ptxt>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>