<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T8_T00MB" variety="T00MB">
<name>THEFT DETERRENT SYSTEM (w/ Entry and Start System)</name>
<para id="RM000000W0L042X" category="D" type-id="303F2" name-id="TD004-48" from="201301">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000000W0L042X_z0" proc-id="RM22W0E___0000EL900000">
<content5 releasenbr="1">
<step1>
<ptxt>THEFT DETERRENT SYSTEM DESCRIPTION</ptxt>
<ptxt>The theft deterrent system is designed to deter vehicle break-ins and theft. If an attempted break-in or theft is detected, the vehicle horns and security horn*1 or theft warning siren*2 will sound, the room light will illuminate, and the headlights*3, taillights*3 and hazard warning lights will flash. The system detects an attempted break-in or theft when any of the following occurs: 1) the vehicle is forcibly entered; 2) the engine hood is forcibly opened; 3) the doors are unlocked without the use of a key; or 4) the cable from the negative (-) battery terminal is disconnected and reconnected.</ptxt>
<ptxt>If the system enters the alarm sounding state, any unlocked doors will automatically lock.</ptxt>
<ptxt>The system has 4 states: disarmed state, arming preparation state, armed and alarm sounding state.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: w/ Security Horn</ptxt>
</item>
<item>
<ptxt>*2: w/ Theft Warning Siren</ptxt>
</item>
<item>
<ptxt>*3: For vehicles with a theft warning siren, this part is not related to the theft deterrent system.</ptxt>
</item>
</list1>
</atten4>
</step1>
<step1>
<ptxt>FUNCTION OF MAIN COMPONENT</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Function</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Security indicator</ptxt>
</entry>
<entry valign="middle">
<ptxt>Informs driver of the theft deterrent system status.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Security horn*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sounds when an attempted break-in or theft is detected.</ptxt>
<ptxt>Located in the engine compartment.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Theft warning siren*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sounds when an attempted break-in or theft is detected.</ptxt>
<ptxt>Located on the quarter inner LH.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Taillight*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flashes when an attempted break-in or theft is detected.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Map light and rear map light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates when an attempted break-in or theft is detected.</ptxt>
<ptxt>The map light has intrusion sensor cancel switch.</ptxt>
<ptxt>For vehicles equipped with a tilt sensor, the intrusion sensor cancel switch also functions as tilt sensor OFF switch.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Headlight*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Begins flashing when an attempted break-in or theft is detected.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hazard warning light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Begins flashing when an attempted break-in or theft is detected.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle horns</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sounds when an attempted break-in or theft is detected.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door courtesy light switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the door open status (open or closed).</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Door lock position switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the door lock status (locked or unlocked).</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine hood courtesy switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the engine hood open status (open or closed).</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back door courtesy switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the back door open status (open or closed).</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Main body ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the engine switch condition (off, on (ACC), or on (IG)).</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Intrusion sensor*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>If an intrusion is detected, the alarm is activated.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Yaw rate sensor*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>After the theft deterrent system is set, detects vehicle tilt. If vehicle tilt changes, the certification ECU (smart key ECU assembly) determines there was theft.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Glass breakage sensor*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sounds alarm when the left or right quarter window glass or back door glass is broken, or when there is short in the wire harness.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: w/ Security Horn</ptxt>
<ptxt>*2: w/ Theft Warning Siren</ptxt>
<ptxt>*3: For vehicles with a theft warning siren, this part is not related to the theft deterrent system.</ptxt>
<ptxt>*4: w/ Intrusion Sensor</ptxt>
<ptxt>*5: w/ Glass Breakage Sensor</ptxt>
</atten4>
</step1>
<step1>
<ptxt>SYSTEM FUNCTION</ptxt>
<step2>
<ptxt>Disarmed state:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The alarm function is not operating.</ptxt>
</item>
<item>
<ptxt>The theft deterrent system is not operating.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Arming preparation state:</ptxt>
<list1 type="nonmark">
<item>
<ptxt>The theft deterrent system is not operating.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Armed state:</ptxt>
<list1 type="nonmark">
<item>
<ptxt>The theft deterrent system is operating.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Alarm sounding state:</ptxt>
<list1 type="nonmark">
<item>
<ptxt>The alarm function is operating (30 +/-5 seconds).</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>OPERATION OF INTRUSION SENSOR</ptxt>
<atten3>
<ptxt>Ultrasonic waves are used to detect intrusion. Ultrasonic waves do not penetrate steel or glass. When any of the door glasses or sliding roof is left open, ultrasonic waves may escape. This may result in the detection of people in the surroundings, leaves or insects entering the vehicle, strong wind or other ultrasonic wave sources in the environment when the vehicle is parked with the door glasses or sliding roof open. When parking the vehicle with the door glasses or the sliding roof open for ventilation, turn off the intrusion sensor using the intrusion sensor cancel switch.</ptxt>
</atten3>
<step2>
<ptxt>An intrusion sensor is provided in the map light assembly located in the front of the vehicle interior.</ptxt>
</step2>
<step2>
<ptxt>The certification ECU (smart key ECU assembly) activates the intrusion sensor when the system changes from the disarmed state to the armed preparation state. If an intrusion detection signal is input from the intrusion sensor during the armed state, the certification ECU (smart key ECU assembly) changes to the alarm state in order to output an alarm.</ptxt>
</step2>
<step2>
<ptxt>When the system is in the disarmed state, the function of the intrusion sensor can be disabled by pressing the intrusion sensor cancel switch.</ptxt>
</step2>
<step2>
<ptxt>The intrusion sensor may erroneously detect intrusion in the following states:</ptxt>
<step3>
<ptxt>Persons or pets are in the vehicle.</ptxt>
</step3>
<step3>
<ptxt>The vehicle is parked in a place where extreme vibrations or noises occur, such as in a parking garage.</ptxt>
</step3>
<step3>
<ptxt>Ice or snow is removed from the vehicle, causing the vehicle to receive repeated impacts or vibrations.</ptxt>
</step3>
<step3>
<ptxt>The wind or something similar causes an externally mounted reflectively coated windshield cover to move.</ptxt>
</step3>
<step3>
<ptxt>The intrusion sensor may react to a hanger or other objects hanging in the cabin when they drop or swing.</ptxt>
</step3>
<step3>
<ptxt>The system changes to the armed state even when any of the windows or sunroof is open. The intrusion sensor may react to objects moving outside the vehicle when any of the windows or sunroof is open.</ptxt>
</step3>
<step3>
<ptxt>The vehicle is inside an automatic or high-pressure car wash.</ptxt>
</step3>
<step3>
<ptxt>The vehicle experiences impacts, such as hail, lightning strikes and other kinds of repeated impacts or vibrations.</ptxt>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>