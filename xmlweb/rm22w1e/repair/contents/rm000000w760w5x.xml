<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3MT_T00FW" variety="T00FW">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1UR-FE)</name>
<para id="RM000000W760W5X" category="D" type-id="303F3" name-id="AT70Z-04" from="201301" to="201308">
<name>DIAGNOSIS SYSTEM</name>
<subpara id="RM000000W760W5X_z0" proc-id="RM22W0E___00008LN00000">
<content5 releasenbr="1">
<step1>
<ptxt>EURO-OBD</ptxt>
<step2>
<ptxt>When troubleshooting Europe On-Board Diagnostic (Euro-OBD) vehicles, the vehicle must be connected to an OBD scan tool (complying with ISO 15765-4). Various data output from the vehicle ECM can then be read.</ptxt>
</step2>
<step2>
<ptxt>Euro-OBD regulations require that the vehicle on-board computer illuminate the Malfunction Indicator Lamp (MIL) on the instrument panel when the computer detects a malfunction in any of the following:</ptxt>
<figure>
<graphic graphicname="C201372" width="2.775699831in" height="1.771723296in"/>
</figure>
<list1 type="unordered">
<item>
<ptxt>The emission control system/components</ptxt>
</item>
<item>
<ptxt>The powertrain control components which affect vehicle emissions</ptxt>
</item>
<item>
<ptxt>The computer</ptxt>
</item>
</list1>
<ptxt>In addition, the applicable Diagnostic Trouble Codes (DTCs) prescribed by ISO 15765-4 are stored in the ECM memory.</ptxt>
<ptxt>If the malfunction does not recur in 3 consecutive trips, the MIL goes off automatically but the DTCs remain stored in the ECM memory.</ptxt>
</step2>
<step2>
<ptxt>To check DTCs, connect the GTS or OBD scan tool to the Data Link Connector 3 (DLC3) of the vehicle. The scan tool displays DTCs, freeze frame data and a variety of engine data.</ptxt>
<ptxt>The DTCs and freeze frame data can be cleared with the scan tool (See page <xref label="Seep01" href="RM000000W770VSX"/>).</ptxt>
</step2>
</step1>
<step1>
<ptxt>M-OBD (EXCEPT EUROPEAN SPECIFICATION)</ptxt>
<step2>
<ptxt>When troubleshooting Multiplex On-Board Diagnostic (M-OBD) vehicles, the vehicle must be connected to the GTS. Various data output from the ECM can then be read.</ptxt>
</step2>
<step2>
<ptxt>OBD regulations require that the vehicle on-board computer illuminate the MIL on the instrument panel when the computer detects a malfunction in any of the following:</ptxt>
<figure>
<graphic graphicname="C201372" width="2.775699831in" height="1.771723296in"/>
</figure>
<list1 type="unordered">
<item>
<ptxt>The emission control system/components</ptxt>
</item>
<item>
<ptxt>The powertrain control components which affect vehicle emissions</ptxt>
</item>
<item>
<ptxt>The computer</ptxt>
</item>
</list1>
<ptxt>In addition, the applicable Diagnostic Trouble Codes (DTCs) prescribed by ISO 15765-4 are stored in the ECM memory.</ptxt>
<ptxt>If the malfunction does not recur in 3 consecutive trips, the MIL goes off automatically but the DTCs remain stored in the ECM memory.</ptxt>
</step2>
</step1>
<step1>
<ptxt>NORMAL MODE AND CHECK MODE</ptxt>
<step2>
<ptxt>The diagnosis system operates in normal mode during normal vehicle use. In normal mode, 2 trip detection logic is used to ensure accurate detection of malfunctions. Check mode is also available to technicians as an option. In check mode, 1 trip detection logic is used for simulating malfunction symptoms and increasing the system's ability to detect malfunctions, including intermittent malfunctions.</ptxt>
</step2>
</step1>
<step1>
<ptxt>2 TRIP DETECTION LOGIC</ptxt>
<step2>
<ptxt>When a malfunction is first detected, the malfunction is temporarily stored in the ECM memory (1st trip). If the same malfunction is detected during the next drive cycle, the MIL is illuminated (2nd trip).</ptxt>
</step2>
</step1>
<step1>
<ptxt>FREEZE FRAME DATA</ptxt>
<step2>
<ptxt>Freeze frame data records the engine conditions (fuel system, calculated load, engine coolant temperature, fuel trim, engine speed, vehicle speed, etc.) when a malfunction is detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</step2>
<step2>
<ptxt>The GTS records freeze frame data at 5 different times: 1) 3 times before the DTC is stored, 2) once when the DTC is stored, and 3) once after the DTC is stored. These data can be used to simulate the vehicle's condition from around the time when the malfunction occurred. The data may help find the cause of the malfunction, or judge if the DTC is being caused by a temporary malfunction or not.</ptxt>
<figure>
<graphic graphicname="C140837E13" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>CHECK DATA LINK CONNECTOR 3 (DLC3) (See page <xref label="Seep02" href="RM000000UZ20DEX"/>)</ptxt>
</step1>
<step1>
<ptxt>CHECK MIL</ptxt>
<step2>
<ptxt>Check that the MIL illuminates when turning the engine switch on (IG).</ptxt>
<ptxt>If the MIL does not illuminate, there is a problem in the MIL circuit (See page <xref label="Seep03" href="RM000000WZ110UX"/>).</ptxt>
</step2>
<step2>
<ptxt>Check that when the engine is started, the MIL turns off.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>