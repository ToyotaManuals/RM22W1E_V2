<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001S" variety="S001S">
<name>STEERING COLUMN</name>
<ttl id="12038_S001S_7C3QF_T00JI" variety="T00JI">
<name>STEERING COLUMN ASSEMBLY (for Power Tilt and Power Telescopic Steering Column)</name>
<para id="RM000003B2400QX" category="G" type-id="8000T" name-id="SR2SN-03" from="201308">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM000003B2400QX_01" type-id="01" category="01">
<s-1 id="RM000003B2400QX_01_0007" proc-id="RM22W0E___0000BGT00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003B2400QX_01_0001" proc-id="RM22W0E___0000BGN00001">
<ptxt>REMOVE LOWER STEERING COLUMN COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="C172704" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Turn the steering wheel to the right and left as necessary to remove the 2 screws.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Detach the 2 claws to remove the lower steering column cover.</ptxt>
<figure>
<graphic graphicname="C172708" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2400QX_01_0002" proc-id="RM22W0E___0000BGO00001">
<ptxt>REMOVE UPPER STEERING COLUMN COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 clips.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw to remove the upper steering column cover.</ptxt>
<figure>
<graphic graphicname="C172706" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2400QX_01_0008" proc-id="RM22W0E___0000BGU00001">
<ptxt>REMOVE DRIVER SIDE KNEE AIRBAG ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the driver side knee airbag assembly (See page <xref label="Seep01" href="RM000002O9101CX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2400QX_01_0003" proc-id="RM22W0E___0000BGP00001">
<ptxt>INSPECT TILT MOTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the tilt motor.</ptxt>
<s3>
<ptxt>Disconnect connector A from the multiplex tilt and telescopic ECU.</ptxt>
</s3>
<s3>
<ptxt>Apply battery voltage to the tilt motor connector, and check the steering wheel tilt operation.</ptxt>
<figure>
<graphic graphicname="C172660E05" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Multiplex Tilt and Telescopic ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>OK</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.35in"/>
<colspec colname="COL2" colwidth="1.78in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 10 (TIM+)</ptxt>
<ptxt>Battery negative (-) → Terminal 1(TIM-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering wheel tilts up</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Apply battery voltage to the tilt motor connector, and check the steering wheel tilt operation.</ptxt>
<figure>
<graphic graphicname="C172660E05" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Multiplex Tilt and Telescopic ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>OK</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.35in"/>
<colspec colname="COL2" colwidth="1.78in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 1 (TIM-)</ptxt>
<ptxt>Battery negative (-) → Terminal 10 (TIM+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering wheel tilts down</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the steering wheel does not tilt down, replace the steering column assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2400QX_01_0004" proc-id="RM22W0E___0000BGQ00001">
<ptxt>INSPECT TELESCOPIC MOTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the telescopic motor.</ptxt>
<s3>
<ptxt>Disconnect connector B from the multiplex tilt and telescopic ECU.</ptxt>
</s3>
<s3>
<ptxt>Apply battery voltage to the telescopic motor connector, and check the steering column operation.</ptxt>
<figure>
<graphic graphicname="C172661E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Multiplex Tilt and Telescopic ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>OK</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.35in"/>
<colspec colname="COL2" colwidth="1.78in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 2 (TEM+)</ptxt>
<ptxt>Battery negative (-) → Terminal 6 (TEM-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering column contracts</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Apply battery voltage to the telescopic motor connector, and check the steering column operation.</ptxt>
<figure>
<graphic graphicname="C172661E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Multiplex Tilt and Telescopic ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>OK</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.35in"/>
<colspec colname="COL2" colwidth="1.78in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → Terminal 6 (TEM-)</ptxt>
<ptxt>Battery negative (-) → Terminal 2 (TEM+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering column extends</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the steering column operation does not match the specified condition, replace the steering column.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2400QX_01_0009" proc-id="RM22W0E___0000BGV00001">
<ptxt>INSTALL DRIVER SIDE KNEE AIRBAG ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install driver side knee airbag assembly (See page <xref label="Seep01" href="RM000002O8Z01DX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2400QX_01_0006" proc-id="RM22W0E___0000BGS00001">
<ptxt>INSTALL LOWER STEERING COLUMN COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the lower steering column cover.</ptxt>
<figure>
<graphic graphicname="C172708" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2400QX_01_0005" proc-id="RM22W0E___0000BGR00001">
<ptxt>INSTALL UPPER STEERING COLUMN COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the upper steering column cover.</ptxt>
<figure>
<graphic graphicname="C172706" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 4 clips to install the upper steering cover onto the instrument panel cluster finish panel.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
<figure>
<graphic graphicname="C172704" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>2.0</t-value1>
<t-value2>20</t-value2>
<t-value3>18</t-value3>
</torqueitem>
</torque>
<atten4>
<ptxt>Turn the steering wheel to the right and left as necessary to install the 2 screws.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2400QX_01_0010" proc-id="RM22W0E___0000BGW00001">
<ptxt>INSPECT SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0KMX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B2400QX_01_0011" proc-id="RM22W0E___0000BGX00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>