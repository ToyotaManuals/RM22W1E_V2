<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WO_T00PR" variety="T00PR">
<name>AIR CONDITIONING SYSTEM (for Manual Air Conditioning System)</name>
<para id="RM000001GFM036X" category="C" type-id="804EH" name-id="ACEMT-02" from="201301" to="201308">
<dtccode>B1497</dtccode>
<dtcname>Communication Malfunction (Bus Ic)</dtcname>
<subpara id="RM000001GFM036X_01" type-id="60" category="03" proc-id="RM22W0E___0000HD700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The air conditioning harness assembly connects the air conditioning amplifier assembly and servo motors. The air conditioning amplifier assembly supplies power and sends operation instructions to each servo motor through the air conditioning harness assembly. Each servo motor sends damper position information to the air conditioning amplifier assembly.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1497</ptxt>
</entry>
<entry valign="middle">
<ptxt>An error or open in the communication line.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air conditioning harness assembly</ptxt>
</item>
<item>
<ptxt>No. 1 air conditioning radiator damper servo sub-assembly (air mix damper servo)</ptxt>
</item>
<item>
<ptxt>No. 1 blower damper servo sub-assembly</ptxt>
</item>
<item>
<ptxt>No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo)</ptxt>
</item>
<item>
<ptxt>No. 1 air conditioning radiator damper servo sub-assembly (cooler bypass damper servo)</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001GFM036X_02" type-id="32" category="03" proc-id="RM22W0E___0000HD800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E234746E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001GFM036X_04" type-id="51" category="05" proc-id="RM22W0E___0000HD900000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001GFM036X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001GFM036X_05_0026" proc-id="RM22W0E___0000HDI00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep03" href="RM000002LIT038X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for the DTC (See page <xref label="Seep04" href="RM000002LIT038X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFM036X_05_0027" fin="true">A</down>
<right ref="RM000001GFM036X_05_0013" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001GFM036X_05_0013" proc-id="RM22W0E___0000HDG00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E81 air conditioning amplifier assembly connector.</ptxt>
<figure>
<graphic graphicname="E184007E24" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.40in"/>
<colspec colname="COL3" colwidth="1.35in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E81-21 (+B1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001GFM036X_05_0006" fin="false">OK</down>
<right ref="RM000001GFM036X_05_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFM036X_05_0006" proc-id="RM22W0E___0000HDA00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Select the Active Test, use the intelligent tester to generate a control command, and then check that items operate (See page <xref label="Seep01" href="RM000002LIV02PX"/>).</ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Air Mix Servo Targ Pulse(D)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No. 1 air conditioning radiator damper servo sub-assembly (air mix damper servo)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air Inlet Damper Targ Pulse</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No. 1 blower damper servo sub-assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air Outlet Servo Pulse(D)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Cool Air Bypass Pulse</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No. 1 air conditioning radiator damper servo sub-assembly (cooler bypass damper servo)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Arm of the damper servo motor selected in the Active Test moves smoothly.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>According to the test result, proceed to the next step.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>One of the damper servo motors is malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>All of the damper servo motors are malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFM036X_05_0007" fin="false">A</down>
<right ref="RM000001GFM036X_05_0012" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001GFM036X_05_0007" proc-id="RM22W0E___0000HDB00000">
<testtitle>SYSTEM CHECK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>According to the test result, proceed to the next step.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only the No. 1 air conditioning radiator damper servo sub-assembly (air mix damper servo) is malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Only the No. 1 blower damper servo sub-assembly is malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Only the No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo) is malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Only the No. 1 air conditioning radiator damper servo sub-assembly (cooler bypass damper servo) is malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFM036X_05_0008" fin="false">A</down>
<right ref="RM000001GFM036X_05_0010" fin="false">B</right>
<right ref="RM000001GFM036X_05_0011" fin="false">C</right>
<right ref="RM000001GFM036X_05_0017" fin="false">D</right>
</res>
</testgrp>
<testgrp id="RM000001GFM036X_05_0008" proc-id="RM22W0E___0000HDC00000">
<testtitle>REPLACE NO. 1 AIR CONDITIONING RADIATOR DAMPER SERVO SUB-ASSEMBLY (AIR MIX DAMPER SERVO)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the No. 1 air conditioning radiator damper servo sub-assembly (air mix damper servo).</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: See page <xref label="Seep01" href="RM000003AXS02WX"/>.</ptxt>
</item>
<item>
<ptxt>for RHD: See page <xref label="Seep02" href="RM000003AXS02VX"/>.</ptxt>
</item>
</list1>
<atten4>
<ptxt>Since the No. 1 air conditioning radiator damper servo sub-assembly (air mix damper servo) cannot be inspected while it is removed from the vehicle, replace the No. 1 air conditioning radiator damper servo sub-assembly (air mix damper servo) with a new or normally functioning one and check that the condition returns to normal.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep03" href="RM000002LIT038X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for the DTC (See page <xref label="Seep04" href="RM000002LIT038X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFM036X_05_0014" fin="true">A</down>
<right ref="RM000001GFM036X_05_0012" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001GFM036X_05_0010" proc-id="RM22W0E___0000HDD00000">
<testtitle>REPLACE NO. 1 BLOWER DAMPER SERVO SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the No. 1 blower damper servo sub-assembly (See page <xref label="Seep01" href="RM000003B3T01LX"/>).</ptxt>
<atten4>
<ptxt>Since the No. 1 blower damper servo sub-assembly cannot be inspected while it is removed from the vehicle, replace the No. 1 blower damper servo sub-assembly with a new or normally functioning one and check that the condition returns to normal.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep02" href="RM000002LIT038X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for the DTC (See page <xref label="Seep03" href="RM000002LIT038X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFM036X_05_0028" fin="true">A</down>
<right ref="RM000001GFM036X_05_0012" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001GFM036X_05_0011" proc-id="RM22W0E___0000HDE00000">
<testtitle>REPLACE NO. 1 AIR CONDITIONING RADIATOR DAMPER SERVO SUB-ASSEMBLY (MODE DAMPER SERVO)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo).</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: See page <xref label="Seep01" href="RM000003AXS02WX"/>.</ptxt>
</item>
<item>
<ptxt>for RHD: See page <xref label="Seep02" href="RM000003AXS02VX"/>.</ptxt>
</item>
</list1>
<atten4>
<ptxt>Since the No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo) cannot be inspected while it is removed from the vehicle, replace the No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo) with a new or normally functioning one and check that the condition returns to normal.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep03" href="RM000002LIT038X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for the DTC (See page <xref label="Seep04" href="RM000002LIT038X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFM036X_05_0029" fin="true">A</down>
<right ref="RM000001GFM036X_05_0012" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001GFM036X_05_0017" proc-id="RM22W0E___0000HDH00000">
<testtitle>REPLACE NO. 1 AIR CONDITIONING RADIATOR DAMPER SERVO SUB-ASSEMBLY (COOLER BYPASS DAMPER SERVO)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the No. 1 air conditioning radiator damper servo sub-assembly (cooler bypass damper servo).</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: See page <xref label="Seep01" href="RM000003AXS02WX"/>.</ptxt>
</item>
<item>
<ptxt>for RHD: See page <xref label="Seep02" href="RM000003AXS02VX"/>.</ptxt>
</item>
</list1>
<atten4>
<ptxt>Since the No. 1 air conditioning radiator damper servo sub-assembly (cooler bypass damper servo) cannot be inspected while it is removed from the vehicle, replace the No. 1 air conditioning radiator damper servo sub-assembly (cooler bypass damper servo) with a new or normally functioning one and check that the condition returns to normal.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep03" href="RM000002LIT038X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for the DTC (See page <xref label="Seep04" href="RM000002LIT038X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFM036X_05_0030" fin="true">A</down>
<right ref="RM000001GFM036X_05_0012" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001GFM036X_05_0012" proc-id="RM22W0E___0000HDF00000">
<testtitle>REPLACE AIR CONDITIONING HARNESS ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the air conditioning harness assembly.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for LHD: See page <xref label="Seep01" href="RM000003AXS02WX"/>.</ptxt>
</item>
<item>
<ptxt>for RHD: See page <xref label="Seep02" href="RM000003AXS02VX"/>.</ptxt>
</item>
</list1>
<atten4>
<ptxt>Since the air conditioning harness assembly cannot be inspected while it is removed from the vehicle, replace the air conditioning harness assembly with a new or normally functioning one and check that the condition returns to normal.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep03" href="RM000002LIT038X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for the DTC (See page <xref label="Seep04" href="RM000002LIT038X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1497 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFM036X_05_0031" fin="true">A</down>
<right ref="RM000001GFM036X_05_0015" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000001GFM036X_05_0014">
<testtitle>END (NO. 1 AIR CONDITIONING RADIATOR DAMPER SERVO SUB-ASSEMBLY [AIR MIX DAMPER SERVO] IS FAULTY)</testtitle>
</testgrp>
<testgrp id="RM000001GFM036X_05_0015">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001GFM036X_05_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001GFM036X_05_0027">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001GFM036X_05_0028">
<testtitle>END (NO. 1 BLOWER DAMPER SERVO SUB-ASSEMBLY IS FAULTY)</testtitle>
</testgrp>
<testgrp id="RM000001GFM036X_05_0029">
<testtitle>END (NO. 1 AIR CONDITIONING RADIATOR DAMPER SERVO SUB-ASSEMBLY [MODE DAMPER SERVO] IS FAULTY)</testtitle>
</testgrp>
<testgrp id="RM000001GFM036X_05_0030">
<testtitle>END (NO. 1 AIR CONDITIONING RADIATOR DAMPER SERVO SUB-ASSEMBLY [COOLER BYPASS DAMPER SERVO] IS FAULTY)</testtitle>
</testgrp>
<testgrp id="RM000001GFM036X_05_0031">
<testtitle>END (AIR CONDITIONING HARNESS ASSEMBLY IS FAULTY)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>