<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001U" variety="S001U">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001U_7C3R4_T00K7" variety="T00K7">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM000003XYU061X" category="C" type-id="804GV" name-id="AV5MX-51" from="201308">
<dtccode>CB-45</dtccode>
<dtcname>USB Communication Error</dtcname>
<dtccode>CB-46</dtccode>
<dtcname>iPod Control Error</dtcname>
<dtccode>CB-47</dtccode>
<dtcname>iPod Communication Error</dtcname>
<subpara id="RM000003XYU061X_01" type-id="60" category="03" proc-id="RM22W0E___0000CD800001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>CB-45</ptxt>
</entry>
<entry valign="middle">
<ptxt>Communication error with USB device</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Multi-media interface ECU</ptxt>
</item>
<item>
<ptxt>USB device</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>CB-46</ptxt>
</entry>
<entry valign="middle">
<ptxt>"iPod" control software malfunction</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Multi-media interface ECU</ptxt>
</item>
<item>
<ptxt>"iPod"</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>CB-47</ptxt>
</entry>
<entry valign="middle">
<ptxt>Communication error with "iPod"</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003XYU061X_02" type-id="51" category="05" proc-id="RM22W0E___0000CD900001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>After the inspection is completed, clear the DTCs.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000003XYU061X_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003XYU061X_03_0001" proc-id="RM22W0E___0000CDA00001">
<testtitle>REPLACE USB DEVICE OR "iPod"</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the USB device or "iPod" from the No. 1 stereo jack adapter assembly.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
<atten4>
<ptxt>When one of these DTCs has been stored, it is necessary to turn off the engine switch to make it possible for the vehicle to recognize a new device when it is connected.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Turn the engine switch on (ACC).</ptxt>
</test1>
<test1>
<ptxt>Connect a known good USB device or "iPod" to the No. 1 stereo jack adapter assembly.</ptxt>
</test1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the malfunction occurred when a USB device was in use, use another USB device for the inspection. If the malfunction occurred when an "iPod" was in use, use another "iPod" for the inspection. </ptxt>
</item>
</list1>
</atten4>
</content6>
<res>
<down ref="RM000003XYU061X_03_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003XYU061X_03_0002" proc-id="RM22W0E___0000CDB00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000014CZ0E0X"/>). </ptxt>
</test1>
</content6>
<res>
<down ref="RM000003XYU061X_03_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003XYU061X_03_0003" proc-id="RM22W0E___0000CDC00001">
<testtitle>RECHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for DTCs and check if the same DTC occurs again.</ptxt>
<atten4>
<ptxt>If DTCs are detected frequently, replace the multi-media interface ECU.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction disappears. </ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XYU061X_03_0004" fin="true">OK</down>
<right ref="RM000003XYU061X_03_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XYU061X_03_0004">
<testtitle>USB DEVICE OR "iPod" WAS DEFECTIVE</testtitle>
</testgrp>
<testgrp id="RM000003XYU061X_03_0005">
<testtitle>REPLACE MULTI-MEDIA INTERFACE ECU<xref label="Seep01" href="RM000003AWI017X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>