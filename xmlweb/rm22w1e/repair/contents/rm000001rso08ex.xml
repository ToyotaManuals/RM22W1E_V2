<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S0023" variety="S0023">
<name>NETWORKING</name>
<ttl id="12050_S0023_7C3SE_T00LH" variety="T00LH">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000001RSO08EX" category="D" type-id="3001B" name-id="MP0GO-59" from="201301" to="201308">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM000001RSO08EX_z0" proc-id="RM22W0E___0000DZZ00000">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use these procedures to troubleshoot the LIN communication system.</ptxt>
</item>
<item>
<ptxt>*: Use the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<spec>
<title>Standard Voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding.</ptxt>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT COMMUNICATION FUNCTION OF CAN COMMUNICATION SYSTEM*</testtitle>
<test1>
<ptxt>Using the intelligent tester, inspect the CAN communication function to ensure there are no malfunctions in the communication system (See page <xref label="Seep01" href="RM000002S8D030X"/>). </ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="2.75in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No DTCs output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to CAN COMMUNICATION SYSTEM (See page <xref label="Seep06" href="RM000003A8W004X"/>)</action-ci-right>
<result>C</result>
<action-ci-right>Go to CAN COMMUNICATION SYSTEM (See page <xref label="Seep07" href="RM000002L7X028X"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT COMMUNICATION FUNCTION OF LIN COMMUNICATION SYSTEM*</testtitle>
<test1>
<ptxt>Using the intelligent tester, inspect the LIN communication function to ensure there are no malfunctions in the communication system.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="2.78in"/>
<colspec colname="COL2" colwidth="1.35in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No DTCs output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to DIAGNOSTIC TROUBLE CODE CHART (See page <xref label="Seep02" href="RM000002S88023X"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to the Problem Symptoms Table (See page <xref label="Seep03" href="RM000003D2L009X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.81in"/>
<colspec colname="COL2" colwidth="1.32in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Fault is not listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Fault is listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to step 7</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>OVERALL ANALYSIS AND TROUBLESHOOTING*</testtitle>
<test1>
<ptxt>Data List / Active Test (See page <xref label="Seep04" href="RM000002S8E031X"/>)</ptxt>
</test1>
<test1>
<ptxt>Terminals of ECU (See page <xref label="Seep05" href="RM000002S8902XX"/>)</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>ADJUST, REPAIR OR REPLACE</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>