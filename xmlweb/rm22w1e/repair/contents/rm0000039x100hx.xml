<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002L" variety="S002L">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002L_7C3Z9_T00SC" variety="T00SC">
<name>TURN SIGNAL FLASHER ASSEMBLY (for LHD)</name>
<para id="RM0000039X100HX" category="G" type-id="8000T" name-id="LE01P-01" from="201301">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM0000039X100HX_01" type-id="01" category="01">
<s-1 id="RM0000039X100HX_01_0001" proc-id="RM22W0E___0000J1X00000">
<ptxt>INSPECT TURN SIGNAL FLASHER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E157045E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E19-7 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E19-1 (IG) - E19-7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E19-2 (LR) - E19-7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E19-3 (LL) - E19-7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E19-4 (B) - E19-7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E19-5 (EL) - E19-7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer switch (TURN L) off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>9 V or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer switch (TURN L) on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E19-6 (ER) - E19-7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer switch (TURN R) off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>9 V or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer switch (TURN R) on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>E19-8 (HAZ) - E19-7 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>9 V or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Hazard warning switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, there may be a malfunction in the turn signal flasher.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>