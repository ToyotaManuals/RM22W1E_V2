<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UP_T00NS" variety="T00NS">
<name>REAR NO. 1 SEAT ASSEMBLY (for 60/40 Split Seat Type 60 Side)</name>
<para id="RM00000390Z00NX" category="A" type-id="80001" name-id="SE9DY-01" from="201301" to="201308">
<name>REMOVAL</name>
<subpara id="RM00000390Z00NX_02" type-id="11" category="10" proc-id="RM22W0E___0000G2I00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000390Z00NX_01" type-id="01" category="01">
<s-1 id="RM00000390Z00NX_01_0036" proc-id="RM22W0E___0000G2H00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0009" proc-id="RM22W0E___0000G2C00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0001" proc-id="RM22W0E___0000BOL00000">
<ptxt>REMOVE REAR NO. 3 SEAT CUSHION HINGE COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B258451" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 6 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0002" proc-id="RM22W0E___0000BOM00000">
<ptxt>REMOVE REAR NO. 5 SEAT CUSHION HINGE COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B261018" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 7 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0003" proc-id="RM22W0E___0000BON00000">
<ptxt>REMOVE REAR NO. 2 SEAT CUSHION HINGE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Operate the reclining adjuster release handle to move the seat into the position shown in the illustration. </ptxt>
<figure>
<graphic graphicname="B182644" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and 2 clips, and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<figure>
<graphic graphicname="B181218" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0004" proc-id="RM22W0E___0000BOO00000">
<ptxt>REMOVE REAR NO. 4 SEAT CUSHION HINGE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and 2 clips, and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<figure>
<graphic graphicname="B181217" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0005" proc-id="RM22W0E___0000BOP00000">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Seat Side Airbag:</ptxt>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B184034" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 bolts and seat assembly.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
<figure>
<graphic graphicname="B181219" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0033" proc-id="RM22W0E___0000G2F00000">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat assembly RH (See page <xref label="Seep01" href="RM00000391400NX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0006" proc-id="RM22W0E___0000G2900000">
<ptxt>REMOVE REAR NO. 1 SEAT PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 10 claws and remove the 2 seat protectors.</ptxt>
<figure>
<graphic graphicname="B184032E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0007" proc-id="RM22W0E___0000G2A00000">
<ptxt>REMOVE REAR NO. 2 SEAT PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 10 claws and remove the 2 seat protectors.</ptxt>
<figure>
<graphic graphicname="B184031E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0021" proc-id="RM22W0E___000068I00000">
<ptxt>REMOVE REAR STEP COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181673" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to remove the step cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 2 claws and remove the step cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390Z00NX_01_0022" proc-id="RM22W0E___000068J00000">
<ptxt>REMOVE REAR DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181674" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws and 4 clips, and remove the scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390Z00NX_01_0023" proc-id="RM22W0E___000068K00000">
<ptxt>REMOVE REAR DOOR SCUFF PLATE RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM00000390Z00NX_01_0034" proc-id="RM22W0E___0000G2G00000">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Face to Face Seat Type:</ptxt>
<ptxt>Remove the rear No. 2 seat assembly (See page <xref label="Seep01" href="RM00000311C003X"/>).</ptxt>
</s2>
<s2>
<ptxt>except Face to Face Seat Type:</ptxt>
<ptxt>Remove the rear No. 2 seat assembly (See page <xref label="Seep02" href="RM00000391S00VX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0024" proc-id="RM22W0E___000068L00000">
<ptxt>REMOVE REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 6 clips and remove the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000390Z00NX_01_0026" proc-id="RM22W0E___000068M00000">
<ptxt>REMOVE REAR SEAT COVER CAP (w/ Rear No. 2 Seat, except Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190187E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to remove the rear seat cover cap on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 3 claws and remove the rear seat cover cap.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000390Z00NX_01_0027" proc-id="RM22W0E___0000G2D00000">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Remove the front quarter trim panel assembly LH (See page <xref label="Seep01" href="RM0000038MO00QX_01_0020"/>).</ptxt>
</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>Remove the front quarter trim panel assembly LH (See page <xref label="Seep02" href="RM0000038MO00PX_01_0020"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0029" proc-id="RM22W0E___0000G2E00000">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Remove the front quarter trim panel assembly RH (See page <xref label="Seep01" href="RM0000038MO00QX_01_0022"/>).</ptxt>
</s2>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<ptxt>Remove the front quarter trim panel assembly RH (See page <xref label="Seep02" href="RM0000038MO00PX_01_0022"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000390Z00NX_01_0032" proc-id="RM22W0E___00006E700000">
<ptxt>REMOVE AIR DUCT PLUG (w/ Rear Air Conditioning System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the plug.</ptxt>
<atten4>
<ptxt>Use the same procedures for both sides.</ptxt>
</atten4>
<figure>
<graphic graphicname="B183333" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000390Z00NX_01_0030" proc-id="RM22W0E___000068Q00000">
<ptxt>REMOVE REAR AIR DUCT GUIDE (w/ Rear Air Conditioning System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
<figure>
<graphic graphicname="B183334" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and remove the guide.</ptxt>
<atten4>
<ptxt>Use the same procedures for both sides.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000390Z00NX_01_0035" proc-id="RM22W0E___0000FW100000">
<ptxt>REMOVE FRONT FLOOR CARPET ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E157065" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Fold back the floor carpet.</ptxt>
<atten4>
<ptxt>Fold back the carpet until it is possible to remove the rear No. 6 air duct.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000390Z00NX_01_0031" proc-id="RM22W0E___0000FVY00000">
<ptxt>REMOVE REAR NO. 6 AIR DUCT (w/ Rear Air Conditioning System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>Remove the 5 screws and duct.</ptxt>
<figure>
<graphic graphicname="E155886" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000390Z00NX_01_0008" proc-id="RM22W0E___0000G2B00000">
<ptxt>REMOVE REAR SEAT CUSHION LOCK STRIKER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184033E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 10 bolts and 4 lock strikers.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>