<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S001A" variety="S001A">
<name>AB60F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S001A_7C3ML_T00FO" variety="T00FO">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1VD-FTV)</name>
<para id="RM000000W7Z0OSX" category="C" type-id="302FL" name-id="AT5SZ-05" from="201308">
<dtccode>P0711</dtccode>
<dtcname>Transmission Fluid Temperature Sensor "A" Performance</dtcname>
<subpara id="RM000000W7Z0OSX_01" type-id="60" category="03" proc-id="RM22W0E___000081G00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0712 (See page <xref label="Seep01" href="RM000000W850Q8X_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0711</ptxt>
</entry>
<entry valign="middle">
<ptxt>Both (a) and (b) are detected (2 trip detection logic):</ptxt>
<ptxt>(a) The intake air and engine coolant temperatures are higher than -10°C (14°F) at engine start.</ptxt>
<ptxt>(b) After normal driving for more than 22 min. for 9 km (6 miles) or more, the No. 1 ATF temperature is below 20°C (68°F).</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 transmission wire (No. 1 ATF temperature sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W7Z0OSX_02" type-id="64" category="03" proc-id="RM22W0E___000081H00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates that there is a problem with the signal from the Automatic Transmission Fluid (ATF) temperature sensor and that the sensor itself is defective. The ATF temperature sensor converts the ATF temperature to an electrical resistance value. Based on the resistance, the ECM determines the ATF temperature and detects an open or short in the ATF temperature circuit or a fault in the ATF temperature sensor.</ptxt>
<ptxt>After running the vehicle for a certain period, the ATF temperature should increase. If the ATF temperature is below 20°C (68°F) after running the vehicle for a certain period, the ECM interprets this as a fault, and turns on the MIL.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W7Z0OSX_07" type-id="32" category="03" proc-id="RM22W0E___000081I00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0712 (See page <xref label="Seep01" href="RM000000W850Q8X_01"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000W7Z0OSX_08" type-id="51" category="05" proc-id="RM22W0E___000081J00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
<table pgwide="1">
<title>ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A/T Oil Temperature 1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 ATF temperature sensor value/</ptxt>
<ptxt>Min.: -40°C (-40°F)</ptxt>
<ptxt>Max.: 215°C (419°F)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>After stall test: Approximately 80°C (176°F)</ptxt>
</item>
<item>
<ptxt>With engine cold: Equal to ambient temperature</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>If the value is -40°C (-40°F) or 215°C (419°F), the No. 1 ATF temperature sensor circuit is open or shorted.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When DTC P0712 is stored and the intelligent tester output is 150°C (302°F) or higher, there is a short circuit.</ptxt>
<ptxt>When DTC P0713 is stored and the intelligent tester output is -40°C (-40°F), there is an open circuit.</ptxt>
<ptxt>Check the temperature displayed on the tester in order to check if a malfunction exists.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Temperature Displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Malfunction</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Open circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>150°C (302°F) or higher</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Short circuit</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If a circuit related to the ATF temperature sensor becomes open, P0713 is immediately stored (in 0.5 seconds). When P0713 is stored, P0711 cannot be stored. It is not necessary to inspect the circuit when P0711 is stored.</ptxt>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000000W7Z0OSX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W7Z0OSX_09_0001" proc-id="RM22W0E___000081K00001">
<testtitle>CHECK DTC OUTPUT (IN ADDITION TO DTC P0711)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the tester.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only P0711 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0711 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any other codes besides P0711 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W7Z0OSX_09_0002" fin="false">A</down>
<right ref="RM000000W7Z0OSX_09_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000W7Z0OSX_09_0002" proc-id="RM22W0E___000081L00001">
<testtitle>CHECK TRANSMISSION FLUID LEVEL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the transmission fluid level (See page <xref label="Seep01" href="RM000002BL004DX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Automatic transmission fluid level is correct.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W7Z0OSX_09_0003" fin="true">OK</down>
<right ref="RM000000W7Z0OSX_09_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W7Z0OSX_09_0003">
<testtitle>REPAIR OR REPLACE NO. 1 ATF TEMPERATURE SENSOR (NO. 1 TRANSMISSION WIRE)<xref label="Seep01" href="RM0000013C104UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W7Z0OSX_09_0004">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000030G909HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W7Z0OSX_09_0005">
<testtitle>ADD FLUID<xref label="Seep01" href="RM000002BL004DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>