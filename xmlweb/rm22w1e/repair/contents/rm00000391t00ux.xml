<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UN_T00NQ" variety="T00NQ">
<name>REAR NO. 2 SEAT ASSEMBLY (except Face to Face Seat Type)</name>
<para id="RM00000391T00UX" category="A" type-id="80002" name-id="SE6SC-02" from="201301" to="201308">
<name>DISASSEMBLY</name>
<subpara id="RM00000391T00UX_02" type-id="11" category="10" proc-id="RM22W0E___0000G0F00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000391T00UX_01" type-id="01" category="01">
<s-1 id="RM00000391T00UX_01_0001" proc-id="RM22W0E___0000FZC00000">
<ptxt>REMOVE SEAT ADJUSTER BOLT COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181818E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0002" proc-id="RM22W0E___0000FZD00000">
<ptxt>REMOVE NO. 2 RECLINING ADJUSTER RELEASE HANDLE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181819" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw and release handle.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0003" proc-id="RM22W0E___0000FZE00000">
<ptxt>REMOVE RECLINING ADJUSTER COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181820E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws in the order shown in the illustration, and then remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0004" proc-id="RM22W0E___0000FZF00000">
<ptxt>REMOVE RECLINING ADJUSTER COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181821E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 5 claws in the order shown in the illustration, and then remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0038" proc-id="RM22W0E___0000G0900000">
<ptxt>REMOVE REAR SEAT REAR BRACKET COVER REAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0005" proc-id="RM22W0E___0000FZG00000">
<ptxt>REMOVE REAR NO. 3 SEAT LEG COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, remove the spring of the seat stay ball-joint.</ptxt>
<figure>
<graphic graphicname="B181822" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the seat stay.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 screws and cover.</ptxt>
<figure>
<graphic graphicname="B181823" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0006" proc-id="RM22W0E___0000FZH00000">
<ptxt>REMOVE NO. 2 SEAT LEG COVER NO. 2</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw and seat leg cover.</ptxt>
<figure>
<graphic graphicname="B182643" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0007" proc-id="RM22W0E___0000FZI00000">
<ptxt>REMOVE 3RD SEAT LINK SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181825" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt, nut and seat link.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0008" proc-id="RM22W0E___0000FZJ00000">
<ptxt>REMOVE 3RD SEAT CUSHION LOCK COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181826" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the 2 covers.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0009" proc-id="RM22W0E___0000FZK00000">
<ptxt>REMOVE REAR SEAT CUSHION UNDER COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 16 claws.</ptxt>
<figure>
<graphic graphicname="B181827" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 7 screws and cover.</ptxt>
<figure>
<graphic graphicname="B258453" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0010" proc-id="RM22W0E___0000FZL00000">
<ptxt>REMOVE NO. 3 SEAT FRONT BRACKET COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181829" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0011" proc-id="RM22W0E___0000FZM00000">
<ptxt>REMOVE NO. 3 SEAT LEG COVER NO. 2</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181830" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0012" proc-id="RM22W0E___0000FZN00000">
<ptxt>REMOVE REAR SEAT LOCK COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181831" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clamps and remove the 2 covers.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0013" proc-id="RM22W0E___0000FZO00000">
<ptxt>REMOVE SEAT STAND FRAME CAP</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181832E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the stand frame cap.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0014" proc-id="RM22W0E___0000FZP00000">
<ptxt>REMOVE SEAT CUSHION LOCK RELEASE LEVER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
<figure>
<graphic graphicname="B235509" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and remove the release lever.</ptxt>
</s2>
<s2>
<ptxt>Detach the cable clamp and disconnect the cable.</ptxt>
<figure>
<graphic graphicname="B235501" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0039" proc-id="RM22W0E___0000G0A00000">
<ptxt>REMOVE REAR CENTER SEAT INNER BELT LH (w/ Rear Center Seat Headrest)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180631E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the nut and belt.</ptxt>
<atten4>
<ptxt>The rear No. 2 seat inner belt is fixed with the same nut as the rear seat inner belt. Therefore, it becomes disconnected when the nut is removed.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000391T00UX_01_0041" proc-id="RM22W0E___0000G0C00000">
<ptxt>REMOVE REAR CENTER SEAT INNER BELT RH (w/ Rear Center Seat Headrest)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B185871" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000391T00UX_01_0040" proc-id="RM22W0E___0000G0B00000">
<ptxt>REMOVE REAR NO. 2 SEAT INNER BELT SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180633E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the nut and belt.</ptxt>
<atten4>
<ptxt>The rear No. 2 seat inner belt is fixed with the same nut as the rear seat outer belt. Therefore, it becomes disconnected when the nut is removed.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000391T00UX_01_0021" proc-id="RM22W0E___0000FZT00000">
<ptxt>REMOVE REAR SEAT CUSHION CARPET</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181840" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Cut off the tack pins which fasten the seat cushion carpet to the seat cushion pad, and then remove the seat cushion carpet from the seat cushion pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0043" proc-id="RM22W0E___0000G0D00000">
<ptxt>REMOVE CAP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 claws and remove the 2 caps.</ptxt>
<figure>
<graphic graphicname="B235506" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0044" proc-id="RM22W0E___0000G0E00000">
<ptxt>REMOVE NO. 2 BOTTLE HOLDER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and No. 2 bottle holder.</ptxt>
<figure>
<graphic graphicname="B235484" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0016">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD</ptxt>
</s-1>
<s-1 id="RM00000391T00UX_01_0017">
<ptxt>REMOVE NO. 3 SEAT CUSHION PAD LH</ptxt>
</s-1>
<s-1 id="RM00000391T00UX_01_0018" proc-id="RM22W0E___0000FZQ00000">
<ptxt>REMOVE NO. 3 SEAT CUSHION COVER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Bottle Holder:</ptxt>
<s3>
<ptxt>Remove the hog rings.</ptxt>
<figure>
<graphic graphicname="B262671E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Detach the fastening tape and remove the seat cushion cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
<s2>
<ptxt>w/ Bottle Holder:</ptxt>
<s3>
<ptxt>Remove the hog rings.</ptxt>
<figure>
<graphic graphicname="B262672E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s3>
<s3>
<ptxt>Detach the 3 fastening tapes and remove the seat cushion cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0019" proc-id="RM22W0E___0000FZR00000">
<ptxt>REMOVE NO. 2 SEAT STAY SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181839" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and seat stay.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0020" proc-id="RM22W0E___0000FZS00000">
<ptxt>REMOVE NO. 3 FOLD SEAT LOCK CONTROL CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 cable clamps and disconnect the 2 cables.</ptxt>
<figure>
<graphic graphicname="B181838" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 clamps and remove the cable.</ptxt>
<figure>
<graphic graphicname="B235502" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0022" proc-id="RM22W0E___0000FZU00000">
<ptxt>REMOVE REAR NO. 2 SEATBACK CUSHION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181843" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw and seatback cushion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0023" proc-id="RM22W0E___0000FZV00000">
<ptxt>REMOVE REAR LOWER SEATBACK LOCK BEZEL</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181844" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 3 claws and clip, and then remove the seatback lock bezel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0024" proc-id="RM22W0E___0000FZW00000">
<ptxt>REMOVE REAR SEAT HINGE PAWL GUIDE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181845" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 5 claws and remove the guide.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0025" proc-id="RM22W0E___0000FZX00000">
<ptxt>REMOVE REAR SEAT HINGE PAWL GUIDE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181846" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw and guide.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0026" proc-id="RM22W0E___0000FZY00000">
<ptxt>REMOVE REAR SEAT HEADREST ASSEMBLY (w/ Folding Headrest)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190318E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>While releasing the lock inside the headrest support with a screwdriver, remove the headrest.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0042">
<ptxt>REMOVE REAR SEAT HEADREST ASSEMBLY (w/o Folding Headrest)</ptxt>
</s-1>
<s-1 id="RM00000391T00UX_01_0027" proc-id="RM22W0E___0000FZZ00000">
<ptxt>REMOVE REAR SEAT HEADREST HANDLE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181842" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the snap ring and headrest handle.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0029" proc-id="RM22W0E___0000G0000000">
<ptxt>REMOVE SEATBACK COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>LH Side:</ptxt>
<s3>
<ptxt>w/ Rear Center Headrest:</ptxt>
<list1 type="ordered">
<item>
<ptxt>Open the fastener.</ptxt>
<figure>
<graphic graphicname="B181847" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Detach the 2 hooks.</ptxt>
<figure>
<graphic graphicname="B181848" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Pull out the center headrest storage bag.</ptxt>
<figure>
<graphic graphicname="B181849" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Remove the rear seat protector.</ptxt>
</item>
<item>
<ptxt>Detach the 8 claws and remove the 4 headrest supports.</ptxt>
<figure>
<graphic graphicname="B181850" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
</list1>
</s3>
<s3>
<ptxt>w/o Rear Center Headrest:</ptxt>
<list1 type="ordered">
<item>
<ptxt>Detach the hook.</ptxt>
<figure>
<graphic graphicname="B235503" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Remove the rear seat protector.</ptxt>
</item>
<item>
<ptxt>Detach the 4 claws and remove the 2 headrest supports.</ptxt>
<figure>
<graphic graphicname="B235505" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
</list1>
</s3>
</s2>
<s2>
<ptxt>RH Side:</ptxt>
<s3>
<ptxt>Detach the hook.</ptxt>
<figure>
<graphic graphicname="B181853" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the rear seat protector.</ptxt>
</s3>
<s3>
<ptxt>Detach the 4 claws and remove the 2 headrest supports.</ptxt>
<figure>
<graphic graphicname="B181854" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Remove the hog ring and seatback cover with pad.</ptxt>
<figure>
<graphic graphicname="B181851" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0030" proc-id="RM22W0E___0000G0100000">
<ptxt>REMOVE NO. 3 SEATBACK COVER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181852" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the hog rings and seatback cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0031" proc-id="RM22W0E___0000G0200000">
<ptxt>REMOVE REAR INNER SEAT RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181857" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0032" proc-id="RM22W0E___0000G0300000">
<ptxt>REMOVE REAR INNER SEAT RECLINING COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0033" proc-id="RM22W0E___0000G0400000">
<ptxt>REMOVE REAR SEAT LOCK CONTROL CABLE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181858" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 2 cable clamps and disconnect the 2 cables.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clamps and remove the lock control cable.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0034" proc-id="RM22W0E___0000G0500000">
<ptxt>REMOVE REAR SEATBACK LOCK STRIKER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181859" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 nuts and lock striker.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0036" proc-id="RM22W0E___0000G0700000">
<ptxt>REMOVE REAR NO. 2 SEATBACK COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0035" proc-id="RM22W0E___0000G0600000">
<ptxt>REMOVE REAR SEATBACK LOCK STRIKER COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 clamps and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000391T00UX_01_0037" proc-id="RM22W0E___0000G0800000">
<ptxt>REMOVE NO. 2 SEAT RECLINING RELEASE HANDLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and release handle.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>