<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12013_S0017" variety="S0017">
<name>1VD-FTV STARTING</name>
<ttl id="12013_S0017_7C3LP_T00ES" variety="T00ES">
<name>GLOW PLUG</name>
<para id="RM0000013ZT02EX" category="A" type-id="30014" name-id="ST623-02" from="201308">
<name>INSTALLATION</name>
<subpara id="RM0000013ZT02EX_02" type-id="11" category="10" proc-id="RM22W0E___000075U00001">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing an injector (including interchanging injectors between cylinders) or common rail, replace the corresponding injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>w/DPF</ptxt>
<ptxt>When fuel lines are disconnected, air may enter the fuel lines, leading to engine starting trouble. Therefore, perform forced regeneration and bleed the air from the fuel lines (See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>).</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM0000013ZT02EX_01" type-id="01" category="01">
<s-1 id="RM0000013ZT02EX_01_0001" proc-id="RM22W0E___000059H00001">
<ptxt>INSTALL GLOW PLUG ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the glow plug and glow plug hole.</ptxt>
</s2>
<s2>
<ptxt>Apply adhesive to 3 or more thread roots in the area shown in the illustration.</ptxt>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1324, Three Bond 1324 or equivalent</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="A263013E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>12 Thread Roots</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a 10 mm deep socket wrench, install the 8 glow plugs.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>125</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013ZT02EX_01_0002" proc-id="RM22W0E___000059I00001">
<ptxt>INSTALL NO. 1 GLOW PLUG CONNECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 glow plug connectors by uniformly tightening the 8 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>2.2</t-value1>
<t-value2>22</t-value2>
<t-value3>19</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the 8 screw grommets.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 wire harnesses with the 2 nuts and 2 screw grommets.</ptxt>
<torque>
<torqueitem>
<t-value1>4.0</t-value1>
<t-value2>41</t-value2>
<t-value3>35</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013ZT02EX_01_0046" proc-id="RM22W0E___000075S00001">
<ptxt>INSTALL INJECTION PIPES</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000031FI00ZX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000013ZT02EX_01_0032" proc-id="RM22W0E___000075R00001">
<ptxt>INSTALL INTERCOOLER ASSEMBLY (w/ Intercooler)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000031FQ003X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000013ZT02EX_01_0047" proc-id="RM22W0E___000075T00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>