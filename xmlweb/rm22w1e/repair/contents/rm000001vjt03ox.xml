<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12065_S002H" variety="S002H">
<name>SLIDING ROOF / CONVERTIBLE</name>
<ttl id="12065_S002H_7C3XI_T00QL" variety="T00QL">
<name>SLIDING ROOF SYSTEM</name>
<para id="RM000001VJT03OX" category="J" type-id="304XH" name-id="RF0G0-01" from="201301" to="201308">
<dtccode/>
<dtcname>Sliding Roof ECU Power Source Circuit</dtcname>
<subpara id="RM000001VJT03OX_04" type-id="60" category="03" proc-id="RM22W0E___0000I8Z00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the sliding function and tilt function do not operate, there may be a malfunction in the sliding roof ECU power source circuit.</ptxt>
</content5>
</subpara>
<subpara id="RM000001VJT03OX_02" type-id="32" category="03" proc-id="RM22W0E___0000I8Y00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B156647E02" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001VJT03OX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001VJT03OX_01" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001VJT03OX_01_0017" proc-id="RM22W0E___0000I8X00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (IGNITION SWITCH SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the ignition switch signal is functioning properly (See page <xref label="Seep01" href="RM000000UZR079X"/>).</ptxt>
</test1>
<table pgwide="1">
<title>Sliding Roof</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition (MPX)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch signal (MPX signal)/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Engine switch on (IG)</ptxt>
<ptxt>OFF: Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition (Direct Signal)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Engine switch on (IG)</ptxt>
<ptxt>OFF: Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The intelligent tester displays as shown in the table according to the operation of each switch.</ptxt>
</specitem>
</spec>
</content6>
<res>
<down ref="RM000001VJT03OX_01_0012" fin="true">OK</down>
<right ref="RM000001VJT03OX_01_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001VJT03OX_01_0002" proc-id="RM22W0E___0000I8V00000">
<testtitle>INSPECT FUSE (S/ROOF, ECU-IG NO. 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the S/ROOF and ECU-IG NO. 2 fuses from the main body ECU.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>S/ROOF fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ECU-IG NO. 2 fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001VJT03OX_01_0003" fin="false">OK</down>
<right ref="RM000001VJT03OX_01_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001VJT03OX_01_0003" proc-id="RM22W0E___0000I8W00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SLIDING ROOF DRIVE GEAR - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the R5 drive gear connector.</ptxt>
<figure>
<graphic graphicname="B130117E08" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R5-2 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>Standard Voltage</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R5-1 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R5-5 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R5-5 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001VJT03OX_01_0012" fin="true">OK</down>
<right ref="RM000001VJT03OX_01_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001VJT03OX_01_0009">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000001VJT03OX_01_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001VJT03OX_01_0012">
<testtitle>REPLACE SLIDING ROOF DRIVE GEAR SUB-ASSEMBLY (SLIDING ROOF ECU)<xref label="Seep01" href="RM000002LPC029X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>