<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000002VGL00OX" category="J" type-id="80113" name-id="ESZJP-01" from="201301" to="201308">
<dtccode/>
<dtcname>Active Control Engine Mount System</dtcname>
<subpara id="RM000002VGL00OX_01" type-id="60" category="03" proc-id="RM22W0E___00003TC00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The Active Control Engine Mount (ACM) system decreases engine vibration at low engine speed using the VSV for engine mount. The VSV is controlled by a pulse signal transmitted to the VSV from the ECM. The frequency of this pulse signal is matched to the engine speed to decrease engine vibration.</ptxt>
</content5>
</subpara>
<subpara id="RM000002VGL00OX_02" type-id="32" category="03" proc-id="RM22W0E___00003TD00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A182519E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002VGL00OX_03" type-id="51" category="05" proc-id="RM22W0E___00003TE00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000002VGL00OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002VGL00OX_04_0001" proc-id="RM22W0E___00003TF00000">
<testtitle>PERFORM ACTIVE TEST USING GTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Warm up the engine.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Move the shift lever to N or P.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the ACM Inhibit.</ptxt>
</test1>
<test1>
<ptxt>Hold the top part of the steering wheel, and turn the Active Test from OFF to ON. Check the engine vibration.</ptxt>
<spec>
<title>Result</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Engine Vibration</ptxt>
</entry>
<entry>
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Increases</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Does not increase</ptxt>
</entry>
<entry>
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002VGL00OX_04_0002" fin="false">B</down>
<right ref="RM000002VGL00OX_04_0017" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000002VGL00OX_04_0002" proc-id="RM22W0E___00003TG00000">
<testtitle>CHECK VACUUM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the vacuum hose cap is missing, if the hose is damaged, and if the air and vacuum hoses have looseness, disconnection or blockage.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Vacuum hose cap is not missing and hoses are normal.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002VGL00OX_04_0004" fin="false">OK</down>
<right ref="RM000002VGL00OX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VGL00OX_04_0004" proc-id="RM22W0E___00003TH00000">
<testtitle>INSPECT VSV FOR ENGINE MOUNT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the VSV for engine mount (See page <xref label="Seep01" href="RM000003B0B005X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002VGL00OX_04_0005" fin="false">OK</down>
<right ref="RM000002VGL00OX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VGL00OX_04_0005" proc-id="RM22W0E___00003TI00000">
<testtitle>CHECK HARNESS AND CONNECTOR (VSV FOR ENGINE MOUNT - ECM, EFI NO. 2 FUSE - VSV FOR ENGINE MOUNT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the VSV for engine mount connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" align="center" colwidth="2.37in"/>
<colspec colname="COLSPEC0" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C81-2 - A38-52 (ACM)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C81-2 or A38-52 (ACM) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" align="center" colwidth="2.37in"/>
<colspec colname="COLSPEC0" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C81-2 - A52-52 (ACM)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C81-2 or A52-52 (ACM) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Remove the EFI NO. 2 fuse from the engine room relay block.</ptxt>
<figure>
<graphic graphicname="A182520E01" width="2.775699831in" height="5.787629434in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" align="center" colwidth="1.36in"/>
<colspec colname="COLSPEC2" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>EFI NO. 2 fuse (2) - C81-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002VGL00OX_04_0006" fin="false">OK</down>
<right ref="RM000002VGL00OX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VGL00OX_04_0006" proc-id="RM22W0E___00003TJ00000">
<testtitle>INSPECT ENGINE MOUNTING INSULATOR FRONT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the engine mounting insulator front (See page <xref label="Seep01" href="RM000003B0F005X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002VGL00OX_04_0013" fin="true">OK</down>
<right ref="RM000002VGL00OX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VGL00OX_04_0017">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ10BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002VGL00OX_04_0008">
<testtitle>REPLACE VACUUM HOSE</testtitle>
</testgrp>
<testgrp id="RM000002VGL00OX_04_0010">
<testtitle>REPLACE VSV FOR ENGINE MOUNT<xref label="Seep01" href="RM000003B0D006X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002VGL00OX_04_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002VGL00OX_04_0012">
<testtitle>REPLACE ENGINE MOUNTING INSULATOR FRONT<xref label="Seep01" href="RM000003B0H006X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002VGL00OX_04_0013">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202SX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>