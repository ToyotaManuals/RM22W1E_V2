<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3TB_T00ME" variety="T00ME">
<name>ENGINE IMMOBILISER SYSTEM (w/o Entry and Start System)</name>
<para id="RM000004UHG00FX" category="J" type-id="805TX" name-id="TD713-02" from="201308">
<dtccode/>
<dtcname>Engine does not Start because No Initial Combustion</dtcname>
<subpara id="RM000004UHG00FX_01" type-id="60" category="03" proc-id="RM22W0E___0000EWY00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>When the key is inserted into the ignition key cylinder, the antenna coil receives the key code. Then the amplifier amplifies the ID code and outputs it to the transponder key ECU assembly.</ptxt>
</item>
<item>
<ptxt>If this symptom occurs, there may be a communication problem between the transponder key amplifier and the transponder key ECU assembly.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000004UHG00FX_02" type-id="32" category="03" proc-id="RM22W0E___0000EWZ00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B289455E04" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000004UHG00FX_03" type-id="51" category="05" proc-id="RM22W0E___0000EX000001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the key and transponder key ECU assembly, refer to the Service Bulletin.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000004UHG00FX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004UHG00FX_05_0001" proc-id="RM22W0E___0000EX100001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001TCQ02BX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0002" proc-id="RM22W0E___0000EX200001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000001TCQ02BX"/>).</ptxt>
<atten4>
<ptxt>Before checking for DTCs, perform the "DTC Output Confirmation Operation" procedure.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0003" fin="false">A</down>
<right ref="RM000004UHG00FX_05_0024" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0003" proc-id="RM22W0E___0000EX300001">
<testtitle>CONFIRM ENGINE MODELS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the vehicle specifications.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>for 1GR-FE</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>for 1VD-FTV</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0004" fin="false">A</down>
<right ref="RM000004UHG00FX_05_0005" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0004" proc-id="RM22W0E___0000EX400001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (IMMOBILISER FUEL CUT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COLSPEC0" colwidth="1.78in"/>
<colspec colname="COLSPEC1" colwidth="1.78in"/>
<colspec colname="COLSPEC2" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.74in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Stored as Freeze Frame Data</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Immobiliser Fuel Cut</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status of immobiliser fuel cut/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Yes</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The item in the Data List indicates "OFF".</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0025" fin="true">OK</down>
<right ref="RM000004UHG00FX_05_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0005" proc-id="RM22W0E___0000EX500001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (IMMOBILISER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Immobiliser / Data List.</ptxt>
<table pgwide="1">
<title>Immobiliser</title>
<tgroup cols="4">
<colspec colname="COLSPEC0" colwidth="1.78in"/>
<colspec colname="COLSPEC1" colwidth="1.78in"/>
<colspec colname="COLSPEC2" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.74in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Immobiliser</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine immobiliser system status determined by transponder key ECU assembly /</ptxt>
<ptxt>Set or Unset</ptxt>
</entry>
<entry valign="middle">
<ptxt>Set: Engine immobiliser set (engine start prohibited (no key in ignition key cylinder))</ptxt>
<ptxt>Unset: Engine immobiliser unset (engine start permitted (key inserted in ignition key cylinder))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>When the engine immobiliser system does not change to the unset state, this item can be used to determine if the cause is the transponder key ECU assembly.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between Set and Unset as shown in the table above.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0034" fin="true">OK</down>
<right ref="RM000004UHG00FX_05_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0006" proc-id="RM22W0E___0000EX600001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (IMMOBILISER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform inspection using a different key that has been registered to the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Immobiliser / Data List.</ptxt>
<table pgwide="1">
<title>Immobiliser</title>
<tgroup cols="4">
<colspec colname="COLSPEC0" colwidth="1.78in"/>
<colspec colname="COLSPEC1" colwidth="1.78in"/>
<colspec colname="COLSPEC2" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.74in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Immobiliser</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine immobiliser system status determined by transponder key ECU assembly /</ptxt>
<ptxt>Set or Unset</ptxt>
</entry>
<entry valign="middle">
<ptxt>Set: Engine immobiliser set (engine start prohibited (no key in ignition key cylinder))</ptxt>
<ptxt>Unset: Engine immobiliser unset (engine start permitted (key inserted in ignition key cylinder))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>When the engine immobiliser system does not change to the unset state, this item can be used to determine if the cause is the transponder key ECU assembly.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between Set and Unset as shown in the table above.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0007" fin="false">OK</down>
<right ref="RM000004UHG00FX_05_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0007" proc-id="RM22W0E___0000EX700001">
<testtitle>CHECK WHETHER ENGINE STARTS WITH OTHER KEYS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform inspection using a different key that has been registered to the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, wait for 5 seconds, and then attempt to start the engine.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Engine cannot be started</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine can be started</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0034" fin="true">A</down>
<right ref="RM000004UHG00FX_05_0012" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0008" proc-id="RM22W0E___0000EX800001">
<testtitle>CHECK TRANSPONDER KEY AMPLIFIER (VC5 SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B287949E07" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Waveform (Reference)</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E95-1 (VC5) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transponder Key Amplifier)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0023" fin="true">OK</down>
<right ref="RM000004UHG00FX_05_0011" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0011" proc-id="RM22W0E___0000EXB00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSPONDER KEY AMPLIFIER - TRANSPONDER KEY ECU ASSEMBLY, IG TERMINAL VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E96 transponder key ECU assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" align="left" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E95-1 (VC5) - E96-14 (VC5)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E95-7 (AGND) - E96-5 (AGND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E95-1 (VC5) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the transponder key amplifier connector.</ptxt>
<figure>
<graphic graphicname="B287750E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" align="left" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E96-2 (IG) - E96-16 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transponder Key ECU Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0009" fin="false">OK</down>
<right ref="RM000004UHG00FX_05_0029" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0009" proc-id="RM22W0E___0000EX900001">
<testtitle>CHECK TRANSPONDER KEY AMPLIFIER (TXCT SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B287951E07" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Waveform (Reference)</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E95-5 (TXCT) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transponder Key Amplifier)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0023" fin="true">OK</down>
<right ref="RM000004UHG00FX_05_0032" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0032" proc-id="RM22W0E___0000EXK00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSPONDER KEY AMPLIFIER - TRANSPONDER KEY ECU ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E96 transponder key ECU assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" align="left" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E95-5 (TXCT) - E96-4 (TXCT)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E95-5 (TXCT) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0010" fin="false">OK</down>
<right ref="RM000004UHG00FX_05_0029" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0010" proc-id="RM22W0E___0000EXA00001">
<testtitle>CHECK TRANSPONDER KEY AMPLIFIER (CODE SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B287950E10" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Waveform (Reference)</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E95-4 (CODE) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transponder Key Amplifier)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0023" fin="true">OK</down>
<right ref="RM000004UHG00FX_05_0033" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0033" proc-id="RM22W0E___0000EXL00001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSPONDER KEY AMPLIFIER - TRANSPONDER KEY ECU ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E95 transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the E96 transponder key ECU assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" align="left" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E95-4 (CODE) - E96-15 (CODE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E95-4 (CODE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the transponder key ECU assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the transponder key amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B287950E10" width="2.775699831in" height="3.779676365in"/>
</figure>
<test2>
<ptxt>Waveform (Reference)</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E95-4 (CODE) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Transponder Key Amplifier)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal CODE stuck high (5 V)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal CODE stuck low (1 V or less)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Problem in wire harness or connector</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0019" fin="false">A</down>
<right ref="RM000004UHG00FX_05_0016" fin="false">B</right>
<right ref="RM000004UHG00FX_05_0029" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0019" proc-id="RM22W0E___0000EXI00001">
<testtitle>REPLACE TRANSPONDER KEY AMPLIFIER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the transponder key amplifier (See page <xref label="Seep01" href="RM000003YO101VX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0020" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0020" proc-id="RM22W0E___0000EXJ00001">
<testtitle>CHECK WHETHER ENGINE STARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the engine starts with an already registered vehicle key.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Engine starts normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0022" fin="true">OK</down>
<right ref="RM000004UHG00FX_05_0016" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0022">
<testtitle>END (TRANSPONDER KEY AMPLIFIER WAS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0016" proc-id="RM22W0E___0000EXF00001">
<testtitle>REPLACE TRANSPONDER KEY ECU ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the transponder key ECU assembly.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0037" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0037" proc-id="RM22W0E___0000EXM00001">
<testtitle>REGISTER KEY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reregister the key (refer to the Service Bulletin).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0017" proc-id="RM22W0E___0000EXG00001">
<testtitle>REGISTER ECU COMMUNICATION ID</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Register the ECU communication ID (refer to the Service Bulletin).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0018" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0018" proc-id="RM22W0E___0000EXH00001">
<testtitle>CHECK WHETHER ENGINE STARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the engine starts with an already registered vehicle key.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Engine starts normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0021" fin="true">OK</down>
<right ref="RM000004UHG00FX_05_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0021">
<testtitle>END (TRANSPONDER KEY ECU ASSEMBLY WAS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0012" proc-id="RM22W0E___0000EXC00001">
<testtitle>REPLACE KEY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the transponder key master transmitter with a new one (refer to the Service Bulletin).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0013" proc-id="RM22W0E___0000EXD00001">
<testtitle>REREGISTER KEY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reregister the key (refer to the Service Bulletin).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0014" proc-id="RM22W0E___0000EXE00001">
<testtitle>CHECK WHETHER ENGINE STARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the engine starts with an already registered vehicle key.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Engine starts normally.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHG00FX_05_0015" fin="true">A</down>
<right ref="RM000004UHG00FX_05_0025" fin="true">B</right>
<right ref="RM000004UHG00FX_05_0027" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0015">
<testtitle>END (KEY (TRANSPONDER KEY MASTER TRANSMITTER) WAS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0024">
<testtitle>GO TO DIAGNOSTIC TROUBLE CODE CHART<xref label="Seep01" href="RM000001TCA036X"/>
</testtitle>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0034">
<testtitle>GO TO ENGINE DOES NOT START BUT INITIAL COMBUSTION OCCURS<xref label="Seep01" href="RM000004UHH00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0023">
<testtitle>REPLACE TRANSPONDER KEY ECU ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0029">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0025">
<testtitle>GO TO SFI SYSTEM<xref label="Seep01" href="RM000000PD9119X"/>
</testtitle>
</testgrp>
<testgrp id="RM000004UHG00FX_05_0027">
<testtitle>GO TO ECD SYSTEM<xref label="Seep01" href="RM0000012WB08CX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>