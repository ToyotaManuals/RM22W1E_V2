<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000A" variety="S000A">
<name>1GR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000A_7C3EY_T0081" variety="T0081">
<name>ENGINE UNIT</name>
<para id="RM000002YYY011X" category="A" type-id="30014" name-id="EMBSU-01" from="201301">
<name>INSTALLATION</name>
<subpara id="RM000002YYY011X_01" type-id="01" category="01">
<s-1 id="RM000002YYY011X_01_0017" proc-id="RM22W0E___00004AD00000">
<ptxt>INSTALL FLYWHEEL RING GEAR (for Manual Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a torch, heat the ring gear evenly to approximately 200°C (392°F).</ptxt>
<figure>
<graphic graphicname="A177670E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Chamfer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ring Gear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flywheel</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to overheat the ring gear.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a brass bar, tap the ring gear onto the flywheel with its chamfered gear teeth facing the block.</ptxt>
<atten3>
<ptxt>After installing, allow the ring gear to cool before handling.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YYY011X_01_0004" proc-id="RM22W0E___00004AB00000">
<ptxt>INSTALL FRONT NO. 1 ENGINE MOUNTING BRACKET RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front No. 1 engine mounting bracket RH with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>43</t-value1>
<t-value2>438</t-value2>
<t-value4>32</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YYY011X_01_0003" proc-id="RM22W0E___00004AA00000">
<ptxt>INSTALL FRONT NO. 1 ENGINE MOUNTING BRACKET LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front No. 1 engine mounting bracket LH with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>43</t-value1>
<t-value2>438</t-value2>
<t-value4>32</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YYY011X_01_0019" proc-id="RM22W0E___00004AE00000">
<ptxt>INSTALL ENGINE OIL LEVEL DIPSTICK GUIDE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new O-ring to the engine oil level dipstick guide.</ptxt>
<figure>
<graphic graphicname="A223631E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>New O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Apply a light coat of engine oil to the O-ring.</ptxt>
</s2>
<s2>
<ptxt>Push the engine oil level dipstick guide end into the guide hole.</ptxt>
</s2>
<s2>
<ptxt>Install the engine oil level dipstick guide with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the engine oil level dipstick.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0020" proc-id="RM22W0E___00004AF00000">
<ptxt>INSTALL V-RIBBED BELT TENSIONER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the V-ribbed belt tensioner with the 5 bolts.</ptxt>
<figure>
<graphic graphicname="A226495E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Bolt</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Length</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>70 mm (2.76 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>33 mm (1.30 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Bolt A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Bolt B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten bolts 1 and 2 in numerical order.</ptxt>
<torque>
<torqueitem>
<t-value1>36</t-value1>
<t-value2>367</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the other bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>36</t-value1>
<t-value2>367</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0027" proc-id="RM22W0E___00004AG00000">
<ptxt>INSTALL NO. 2 IDLER PULLEY SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 No. 2 idler pulleys with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>54</t-value1>
<t-value2>551</t-value2>
<t-value4>40</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0028" proc-id="RM22W0E___00004AH00000">
<ptxt>INSTALL NO. 1 IDLER PULLEY SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 idler pulley with the bolt.</ptxt>
<figure>
<graphic graphicname="A076438E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>54</t-value1>
<t-value2>551</t-value2>
<t-value4>40</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>DOUBLE</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>"DOUBLE" is marked on the No. 1 idler pulley to distinguish it from the No. 2 idler pulley.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0029" proc-id="RM22W0E___00004AI00000">
<ptxt>INSTALL WATER BY-PASS PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the water by-pass pipe with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A223698E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 2 hoses.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upward</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rearward</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The direction of the hose clamp is indicated in the illustration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0032" proc-id="RM22W0E___00004AJ00000">
<ptxt>INSTALL INTAKE MANIFOLD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set a new gasket on each cylinder head.</ptxt>
<figure>
<graphic graphicname="A221467" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Align the port holes of the gasket and cylinder head.</ptxt>
</item>
<item>
<ptxt>Be careful of the installation direction.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Set the intake manifold on the cylinder heads.</ptxt>
</s2>
<s2>
<ptxt>Install and uniformly tighten the 6 bolts and 4 nuts in several passes.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Tighten the inner installation bolts of the intake manifold before tightening the outer bolts.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0034" proc-id="RM22W0E___00004AK00000">
<ptxt>INSTALL FUEL INJECTOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new insulator to each fuel injector.</ptxt>
<figure>
<graphic graphicname="A223045E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>New Insulator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>New O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Apply a light coat of spindle oil or gasoline to new O-rings and install one to each fuel injector.</ptxt>
</s2>
<s2>
<ptxt>Install the 6 injectors.</ptxt>
<s3>
<ptxt>While turning each fuel injector left and right, install it to the fuel delivery pipe.</ptxt>
<figure>
<graphic graphicname="A223046E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Outward</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100137" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Position the fuel injectors with the connectors facing outward.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0035" proc-id="RM22W0E___00004AL00000">
<ptxt>INSTALL FUEL DELIVERY PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the fuel delivery pipe together with the 6 fuel injectors on the intake manifold.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install the 4 bolts, which are used to hold the fuel delivery pipe in place, to the intake manifold.</ptxt>
</s2>
<s2>
<ptxt>Check that the fuel injectors rotate smoothly.</ptxt>
<figure>
<graphic graphicname="A223047E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the fuel injectors do not rotate smoothly, replace the O-ring of any injector that does not rotate smoothly.</ptxt>
</s2>
<s2>
<ptxt>Position the fuel injectors with the connectors facing outward.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 6 fuel injector connectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0036" proc-id="RM22W0E___00004AM00000">
<ptxt>INSTALL FUEL PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 fuel pipe and No. 2 fuel pipe with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>9.0</t-value1>
<t-value2>92</t-value2>
<t-value3>80</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 2 fuel pipes (See page <xref label="Seep01" href="RM0000028RU03VX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YYY011X_01_0037" proc-id="RM22W0E___000011400000">
<ptxt>INSTALL IGNITION COIL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 6 ignition coils with the 6 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 6 ignition coil connectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0038" proc-id="RM22W0E___000011700000">
<ptxt>INSTALL NO. 2 EMISSION CONTROL VALVE SET (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 emission control valve set with the 3 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Align the paint mark with the rib and connect the No. 1 air hose.</ptxt>
<figure>
<graphic graphicname="A224703E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Rib</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Top</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>LH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Make sure the direction of the hose clamp is as shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Connect the No. 2 emission control valve set connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0039" proc-id="RM22W0E___000011800000">
<ptxt>INSTALL NO. 1 EMISSION CONTROL VALVE SET (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 emission control valve set with the 3 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Align the paint mark with the rib and connect the No. 1 air hose.</ptxt>
<figure>
<graphic graphicname="A267739E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Rib</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>RH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Top</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Make sure the direction of the hose clamp is as shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Connect the No. 1 emission control valve set connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002YYY011X_01_0013" proc-id="RM22W0E___00004AC00000">
<ptxt>INSTALL HEATER WATER HOSE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the heater water hose assembly and connect the 2 hoses with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YYY011X_01_0007">
<ptxt>INSTALL ENGINE WIRE</ptxt>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>