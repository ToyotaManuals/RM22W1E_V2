<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000001J2807GX" category="J" type-id="801EU" name-id="ACBLG-03" from="201301" to="201308">
<dtccode/>
<dtcname>Air Conditioning Control Panel does not Operate</dtcname>
<subpara id="RM000001J2807GX_01" type-id="60" category="03" proc-id="RM22W0E___0000H5Y00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit* consists of the air conditioning control assembly and the air conditioning amplifier assembly. When the air conditioning control assembly is operated, signals are transmitted to the air conditioning amplifier assembly through the LIN communication system.</ptxt>
<ptxt>If the LIN communication system malfunctions, the air conditioning amplifier assembly does not operate even if the air conditioning control assembly is operated.</ptxt>
<atten4>
<ptxt>*: w/o Multi-display</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001J2807GX_02" type-id="32" category="03" proc-id="RM22W0E___0000H5Z00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E157369E08" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="E157369E09" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001J2807GX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001J2807GX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001J2807GX_04_0010" proc-id="RM22W0E___0000H6300000">
<testtitle>INSPECT FUSE (ECU-IG NO. 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the ECU-IG NO. 2 fuse from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ECU-IG NO. 2 fuse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2807GX_04_0001" fin="false">OK</down>
<right ref="RM000001J2807GX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2807GX_04_0001" proc-id="RM22W0E___0000H6000000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING CONTROL - BODY GROUND AND BATTERY)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E157365E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the F10 air conditioning control assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F10-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>F10-1 (GND) - F10-7 (IG+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2807GX_04_0003" fin="false">OK</down>
<right ref="RM000001J2807GX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2807GX_04_0003" proc-id="RM22W0E___0000H6100000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - AIR CONDITIONING CONTROL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>w/ Rear Heater</ptxt>
<figure>
<graphic graphicname="E157366E03" width="2.775699831in" height="5.787629434in"/>
</figure>
<test2>
<ptxt>Disconnect the F10 air conditioning control assembly connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E36 amplifier connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E36-20 (LIN1) - F10-5 (LIN1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E36-20 (LIN1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>w/o Rear Heater</ptxt>
<figure>
<graphic graphicname="E160297E02" width="2.775699831in" height="5.787629434in"/>
</figure>
<test2>
<ptxt>Disconnect the F10 air conditioning control assembly connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E81 amplifier connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E81-37 (LIN1) - F10-5 (LIN1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E81-37 (LIN1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000001J2807GX_04_0004" fin="false">OK</down>
<right ref="RM000001J2807GX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2807GX_04_0004" proc-id="RM22W0E___0000H6200000">
<testtitle>CHECK AIR CONDITIONING CONTROL ASSEMBLY (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air conditioning control assembly with a new or properly functioning one.</ptxt>
</test1>
<test1>
<ptxt>Operate the front air conditioning control to check that it functions properly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Air conditioning control assembly function operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2807GX_04_0008" fin="true">OK</down>
<right ref="RM000001J2807GX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2807GX_04_0011">
<testtitle>REPLACE FUSE</testtitle>
</testgrp>
<testgrp id="RM000001J2807GX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001J2807GX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001J2807GX_04_0009">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001J2807GX_04_0008">
<testtitle>REPLACE AIR CONDITIONING CONTROL ASSEMBLY<xref label="Seep01" href="RM000003B4601DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>