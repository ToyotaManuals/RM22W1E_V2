<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000PDJ139X" category="D" type-id="303F3" name-id="ES9SY-11" from="201308">
<name>DIAGNOSIS SYSTEM</name>
<subpara id="RM000000PDJ139X_z0" proc-id="RM22W0E___00002AL00001">
<content5 releasenbr="1">
<step1>
<ptxt>EURO-OBD (EUROPE)</ptxt>
<list1 type="nonmark">
<item>
<ptxt>When troubleshooting Europe On-Board Diagnostic (Euro-OBD) vehicles, the vehicle must be connected to an OBD scan tool (complying with ISO 15765-4). Various data output from the vehicle ECM can then be read.</ptxt>
</item>
</list1>
<list1 type="nonmark">
<item>
<ptxt>Euro-OBD regulations require that the on-board computer of the vehicle illuminate the Malfunction Indicator Lamp (MIL) on the instrument panel when the computer detects a malfunction in:</ptxt>
<figure>
<graphic graphicname="FI02547E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<list2 type="ordered">
<item>
<ptxt>The emission control system components</ptxt>
</item>
<item>
<ptxt>The powertrain control components (which affect vehicle emissions)</ptxt>
</item>
<item>
<ptxt>The computer itself</ptxt>
</item>
</list2>
</list1>
<list1 type="nonmark">
<item>
<ptxt>In addition, the applicable Diagnostic Trouble Codes (DTCs) prescribed by ISO 15765-4 are stored in the ECM memory. If the malfunction does not reoccur in 3 consecutive trips, the MIL turns off automatically but the DTCs remain stored in the ECM memory.</ptxt>
</item>
</list1>
<list1 type="nonmark">
<item>
<ptxt>To check the DTCs, connect the intelligent tester to the Data Link Connector 3 (DLC3) of the vehicle. The scan tool displays DTCs, the freeze frame data and a variety of the engine data. The DTCs and freeze frame data can be cleared with the scan tool. </ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>M-OBD (EXCEPT EUROPEAN SPEC.)</ptxt>
<figure>
<graphic graphicname="FI02547E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<list1 type="nonmark">
<item>
<ptxt>When troubleshooting Multiplex On-Board Diagnostic (M-OBD) vehicle, the vehicle must be connected to the intelligent tester. Various data output from the ECM can then be read.</ptxt>
</item>
<item>
<ptxt>OBD regulations require that the on-board computer of the vehicle illuminate the MIL on the instrument panel when the computer detects a malfunction in:</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>The emission control system components</ptxt>
</item>
<item>
<ptxt>The powertrain control components (which affect vehicle emissions)</ptxt>
</item>
<item>
<ptxt>The computer</ptxt>
</item>
</list2>
</list1>
<list1 type="nonmark">
<item>
<ptxt>In addition, the applicable DTCs are stored in the ECM memory. If the malfunction does not recur in 3 consecutive trips, the MIL turns off automatically but the DTCs remain stored in the ECM memory.</ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>NORMAL MODE AND CHECK MODE</ptxt>
<ptxt>The diagnosis system operates in normal mode during normal vehicle use. In normal mode, 2 trip detection logic is used to ensure accurate detection of malfunctions. Check mode is also available as an option for technicians. In check mode, 1 trip detection logic is used for simulating malfunction symptoms and increasing the ability of the system to detect malfunctions, including intermittent problems (intelligent tester only).</ptxt>
</step1>
<step1>
<ptxt>2 TRIP DETECTION LOGIC</ptxt>
<list1 type="nonmark">
<item>
<ptxt>When a malfunction is first detected, the malfunction is temporarily stored in the ECM memory (1st trip). If the same malfunction is detected during the next subsequent driving cycle, the MIL is illuminated (2nd trip).</ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>FREEZE FRAME DATA</ptxt>
<list1 type="nonmark">
<item>
<ptxt>The ECM records vehicle and driving condition information as freeze frame data the moment a DTC is stored. When troubleshooting, freeze frame data can be helpful in determining whether the vehicle was running or stopped, whether the engine was warmed up or not, whether the air-fuel ratio was lean or rich, as well as other data recorded at the time of a malfunction.</ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>BATTERY VOLTAGE</ptxt>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If voltage is below 11 V, replace or recharge the battery before proceeding.</ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>MIL (Malfunction Indicator Lamp)</ptxt>
<step2>
<ptxt>The MIL is illuminated when the engine switch is first turned on (IG) (the engine is not running).</ptxt>
</step2>
<step2>
<ptxt>The MIL should turn off when the engine is started. If the MIL remains illuminated, the diagnosis system has detected a malfunction or abnormality in the system.</ptxt>
<atten4>
<ptxt>If the MIL is not illuminated when the engine switch is first turned on (IG), check the MIL circuit (See page <xref label="Seep01" href="RM000000WZ1149X"/>).</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>ALL READINESS</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>With "All Readiness" it is possible to check whether or not the DTC judgment has been completed by using the intelligent tester.</ptxt>
</item>
<item>
<ptxt>Check "All Readiness" after simulating malfunction symptoms or for validation after finishing repairs.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>Turn the tester on.</ptxt>
</step2>
<step2>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK17ZX"/>).</ptxt>
</step2>
<step2>
<ptxt>Perform the DTC judgment driving pattern to run the DTC judgment.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</step2>
<step2>
<ptxt>Input the DTCs to be confirmed.</ptxt>
</step2>
<step2>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>Tester Display</ptxt>
</entry>
<entry>
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>NORMAL</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry>
<ptxt>ABNORMAL</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry>
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry>
<ptxt>N/A</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>