<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3T9_T00MC" variety="T00MC">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000000XUI0ICX" category="J" type-id="800Q3" name-id="TD8QA-01" from="201308">
<dtccode/>
<dtcname>Front Passenger Side Door Entry Lock and Unlock Functions do not Operate</dtcname>
<subpara id="RM000000XUI0ICX_01" type-id="60" category="03" proc-id="RM22W0E___0000EOF00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the front passenger side door entry lock and unlock functions do not operate, one of the following may be malfunctioning: 1) power door lock system, 2) front passenger side door electrical key oscillator, or 3) certification ECU (smart key ECU assembly).</ptxt>
</content5>
</subpara>
<subpara id="RM000000XUI0ICX_02" type-id="32" category="03" proc-id="RM22W0E___0000EOG00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B296362E06" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XUI0ICX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000XUI0ICX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XUI0ICX_05_0020" proc-id="RM22W0E___0000EOH00001">
<testtitle>CHECK POWER DOOR LOCK OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When the master switch assembly door control switch is operated, check that the locked doors unlock.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Door locks operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0050" fin="false">OK</down>
<right ref="RM000000XUI0ICX_05_0034" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0050" proc-id="RM22W0E___0000EOQ00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR LOCK POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Main Body / Data List.</ptxt>
</test1>
<test1>
<ptxt>Read the Data List according to the display on the intelligent tester.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P Door Lock Position SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side door lock position switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front passenger side door unlocked</ptxt>
<ptxt>OFF: Front passenger side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between ON and OFF as shown in the chart above.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0021" fin="false">OK</down>
<right ref="RM000000XUI0ICX_05_0051" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0021" proc-id="RM22W0E___0000EOI00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (TOUCH SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Entry &amp; Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P-Door Touch Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front passenger side door touch sensor / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Touch sensor touched</ptxt>
<ptxt>OFF: Touch sensor not touched</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P-Door Trigger Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front passenger side door outside handle lock switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Entry lock switch pushed</ptxt>
<ptxt>OFF: Entry lock switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Display changes according to manual operation</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Display does not change according to manual operation</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0026" fin="false">A</down>
<right ref="RM000000XUI0ICX_05_0028" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0026" proc-id="RM22W0E___0000EOJ00001">
<testtitle>CHECK WAVE ENVIRONMENT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bring the electrical key transmitter near the front passenger side door outside handle, and perform a front passenger side door entry lock and unlock operation check.</ptxt>
<figure>
<graphic graphicname="B188468E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<atten3>
<ptxt>If the key is brought within 0.2 m (0.656 ft.) of the door handle, communication is not possible.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the electrical key transmitter is brought near the front passenger side door outside handle, the possibility of wave interference decreases, and it can be determined if wave interference is causing the problem symptom.</ptxt>
</item>
<item>
<ptxt>If the operation check is normal, the possibility of wave interference is high. Also, added vehicle components may cause wave interference. If installed, remove them and perform the operation check.</ptxt>
</item>
</list1>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Operation check fails</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Operation check is normal</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0027" fin="false">A</down>
<right ref="RM000000XUI0ICX_05_0039" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0027" proc-id="RM22W0E___0000EOK00001">
<testtitle>PERFORM KEY DIAGNOSTIC MODE INSPECTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Diagnostic mode inspection (front passenger side door electrical key oscillator).</ptxt>
<figure>
<graphic graphicname="B188471E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<test2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test2>
<test2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test2>
<test2>
<ptxt>Turn the intelligent tester on.</ptxt>
</test2>
<test2>
<ptxt>Enter the following menus: Body / Entry &amp; Start / Key Communication Check / Overhead + Passenger Side.</ptxt>
</test2>
<test2>
<ptxt>When the electrical key transmitter is in the position shown in the illustration, check that the wireless door lock buzzer sounds.</ptxt>
<atten4>
<ptxt>If the buzzer sounds, it can be determined that the front passenger seat outside transmitter is operating normally.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Wireless door lock buzzer sounds.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0040" fin="true">OK</down>
<right ref="RM000000XUI0ICX_05_0028" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0028" proc-id="RM22W0E___0000EOL00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU [SMART KEY ECU ASSEMBLY] - DOOR ELECTRICAL KEY OSCILLATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the E29 ECU connector.</ptxt>
<figure>
<graphic graphicname="B187935E12" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the z22 or z24 oscillator connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-6 (SEL2) - z22-4 (SEL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-35 (CLG2) - z22-10 (CLG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-36 (CG2B) - z22-5 (CLGB)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-6 (SEL2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-35 (CLG2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-36 (CG2B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E29-6 (SEL2) - z24-4 (SEL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-35 (CLG2) - z24-10 (CLG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-36 (CG2B) - z24-5 (CLGB)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-6 (SEL2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-35 (CLG2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E29-36 (CG2B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0029" fin="false">OK</down>
<right ref="RM000000XUI0ICX_05_0036" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0029" proc-id="RM22W0E___0000EOM00001">
<testtitle>CHECK HARNESS AND CONNECTOR (DOOR ELECTRICAL KEY OSCILLATOR - FRONT DOOR OUTSIDE HANDLE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the z21 or z23 handle connector.</ptxt>
<figure>
<graphic graphicname="B164339E60" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the z22 or z24 oscillator connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z21-2 (ANT2) - z22-8 (ANT2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z21-5 (ANT1) - z22-3 (ANT1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z21-2 (ANT2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z21-5 (ANT1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z23-2 (ANT2) - z24-8 (ANT2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z23-5 (ANT1) - z24-3 (ANT1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z23-2 (ANT2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z23-5 (ANT1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0030" fin="false">OK</down>
<right ref="RM000000XUI0ICX_05_0037" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0030" proc-id="RM22W0E___0000EON00001">
<testtitle>CHECK HARNESS AND CONNECTOR (DOOR ELECTRICAL KEY OSCILLATOR - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the z22 or z24 oscillator connector.</ptxt>
<figure>
<graphic graphicname="B164336E20" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<subtitle>for LHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z22-1 (B) - z22-7 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z24-1 (B) - z24-7 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0049" fin="false">OK</down>
<right ref="RM000000XUI0ICX_05_0038" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0049" proc-id="RM22W0E___0000EOP00001">
<testtitle>REPLACE DOOR ELECTRICAL KEY OSCILLATOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the door electrical key oscillator (See page <xref label="Seep01" href="RM000002M7W04BX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0031" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0031" proc-id="RM22W0E___0000EOO00001">
<testtitle>CHECK DOOR ELECTRICAL KEY OSCILLATOR (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the entry function operates normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0044" fin="true">OK</down>
<right ref="RM000000XUI0ICX_05_0052" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0034">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM<xref label="Seep01" href="RM000002T6K05PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0036">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0037">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0038">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0039">
<testtitle>AFFECTED BY WAVE INTERFERENCE</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0040">
<testtitle>REPLACE CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0044">
<testtitle>END (DOOR ELECTRICAL KEY OSCILLATOR IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0048">
<testtitle>END (FRONT DOOR OUTSIDE HANDLE IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0051">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM (Proceed to Only Passenger Door LOCK/UNLOCK Functions do not Operate)<xref label="Seep01" href="RM000000TNU074X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0052" proc-id="RM22W0E___0000EOR00001">
<testtitle>REPLACE FRONT DOOR OUTSIDE HANDLE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the front door outside handle (See page <xref label="Seep01" href="RM000002M7W04BX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0036" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XUI0ICX_05_0053" proc-id="RM22W0E___0000EOS00001">
<testtitle>CHECK FRONT DOOR OUTSIDE HANDLE OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the entry function operates normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ICX_05_0048" fin="true">OK</down>
<right ref="RM000000XUI0ICX_05_0040" fin="true">NG</right>
</res>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>