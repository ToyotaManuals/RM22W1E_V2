<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12065_S002H" variety="S002H">
<name>SLIDING ROOF / CONVERTIBLE</name>
<ttl id="12065_S002H_7C3XI_T00QL" variety="T00QL">
<name>SLIDING ROOF SYSTEM</name>
<para id="RM000002B5401WX" category="D" type-id="303FF" name-id="RF086-07" from="201301">
<name>OPERATION CHECK</name>
<subpara id="RM000002B5401WX_z0" proc-id="RM22W0E___0000I8Q00000">
<content5 releasenbr="1">
<atten3>
<ptxt>Perform initialization before performing the operation check.</ptxt>
</atten3>
<step1>
<ptxt>CHECK AUTO OPERATION</ptxt>
<step2>
<ptxt>Turn the engine switch on (IG).</ptxt>
</step2>
<step2>
<ptxt>When the roof glass is fully closed, press the OPEN switch for 0.3 seconds or more. Check that the roof glass automatically slides until it is fully opened.</ptxt>
</step2>
<step2>
<ptxt>When the roof glass is fully open, press the CLOSE switch for 0.3 seconds or more. Check that the roof glass automatically slides until it is fully closed.</ptxt>
</step2>
<step2>
<ptxt>When the roof glass is fully closed, press the UP switch for 0.3 seconds or more. Check that the roof glass automatically tilts until it is fully tilted up.</ptxt>
</step2>
<step2>
<ptxt>When the roof glass is fully up, press the DOWN switch for 0.3 seconds or more. Check that the roof glass automatically tilts until it is fully tilted down.</ptxt>
</step2>
<step2>
<ptxt>When the auto operation is operating, check that pressing any sliding roof switch stops the roof glass operation.</ptxt>
<atten4>
<ptxt>When pressing the switch for less than 0.3 seconds, the roof glass moves but auto operation does not operate.</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>CHECK SLIDING ROOF OPERATION AFTER ENGINE SWITCH IS TURNED OFF</ptxt>
<step2>
<ptxt>Turn the engine switch from on (IG) to off, and check that the sliding roof operates. Then open the driver side door once, and check that the sliding roof does not operate.</ptxt>
</step2>
<step2>
<ptxt>Turn the engine switch from on (IG) to off and wait for approximately 45 seconds. Check that the sliding roof does not operate.</ptxt>
</step2>
<step2>
<ptxt>Operate the auto (slide open/close or tilt up/down) operation. While the roof glass is in motion, turn the engine switch from on (IG) to off. Check that the auto operation continues until the roof glass opens or closes fully.</ptxt>
<atten4>
<ptxt>When the driver door opens, the sliding roof operation will stop.</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>CHECK JAM PROTECTION FUNCTION</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Do not use a part of your body, for example, your hand, to check the jam protection.</ptxt>
</item>
<item>
<ptxt>Do not allow anything to become caught in the sliding roof by accident in this procedure. </ptxt>
</item>
<item>
<ptxt>Perform the inspection from the inside of the vehicle.</ptxt>
</item>
</list1>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not use hard objects, such as a hammer, to avoid damage to the roof.</ptxt>
</item>
<item>
<ptxt>If the jam protection does not operate, initialize the sliding roof drive gear (motor).</ptxt>
</item>
</list1>
</atten3>
<step2>
<ptxt>When the sliding roof auto close operation is operating and an object is caught between the vehicle body and glass, check that the roof glass opens a distance of 218 mm (8.58 in.) from the point of contact with the object, or opens fully if an opening distance of 218 mm (8.58 in.) is not available.</ptxt>
<figure>
<graphic graphicname="B068295" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>When the auto tilt down function is operating, and an object is caught between the vehicle body and the roof glass, check that the sliding roof tilts up fully.</ptxt>
<figure>
<graphic graphicname="B068296" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>CHECK TRANSMITTER-LINKED OPEN/CLOSE OPERATION FUNCTION (w/ Remote Function)</ptxt>
<step2>
<ptxt>Close all the doors.</ptxt>
</step2>
<step2>
<ptxt>Hold down the transmitter unlock switch for approximately 3 seconds or more and check that the slide open/tilt up operation occurs. Also, when the unlock switch is released, check that the roof glass stops moving.</ptxt>
</step2>
<step2>
<ptxt>Hold down the transmitter lock switch for approximately 3 seconds or more and check that the slide close/tilt down operation occurs. Also, when the lock switch is released, check that the roof glass stops moving.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CHECK KEY-LINKED OPEN/CLOSE OPERATION FUNCTION (DRIVER DOOR ONLY) (w/ Remote Function)</ptxt>
<step2>
<ptxt>Insert the key into the driver side door key lock cylinder, and turn the key to the unlock position for approximately 3 seconds or more. Check that the sliding roof starts moving (slide open/tilt up operation). Then release the key and check that the sliding roof stops moving.</ptxt>
</step2>
<step2>
<ptxt>Insert the key into the driver side door key lock cylinder, and turn the key to the lock position for approximately 3 seconds or more. Check that the sliding roof starts moving (slide close/tilt down operation). Then release the key and check that the sliding roof stops moving.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CHECK ENTRY LOCK SWITCH-LINKED CLOSE OPERATION FUNCTION (w/ Remote Function)</ptxt>
<step2>
<ptxt>Close all the doors, open the roof glass and check that the key is not in the vehicle.</ptxt>
</step2>
<step2>
<ptxt>When the entry lock switch on the door outside handle is pressed for 3 seconds or more, check that the roof glass closes. Also, when the entry lock switch is released, check that the roof glass stops operating.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>