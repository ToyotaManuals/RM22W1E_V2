<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0018" variety="S0018">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0018_7C3LW_T00EZ" variety="T00EZ">
<name>CRUISE CONTROL SYSTEM</name>
<para id="RM000002L7101QX" category="J" type-id="3037E" name-id="CC39U-03" from="201301" to="201308">
<dtccode/>
<dtcname>TC and CG Terminal Circuit</dtcname>
<subpara id="RM000002L7101QX_04" type-id="60" category="03" proc-id="RM22W0E___000079400000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Connecting terminals TC and CG of the DLC3 causes the system to enter self-diagnostic mode. If a malfunction is present, DTCs will be output.</ptxt>
<atten4>
<ptxt>When a particular warning light remains blinking, a ground short in the wiring of terminal TC of the DLC3 or an internal ground short in the relevant ECU is suspected.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002L7101QX_01" type-id="32" category="03" proc-id="RM22W0E___000079200000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E158854E04" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002L7101QX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002L7101QX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002L7101QX_03_0001" proc-id="RM22W0E___000079300000">
<testtitle>CHECK HARNESS AND CONNECTOR (DLC3 - ECM AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E158855E04" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Disconnect the A38*1 or A52*2 ECM connector.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for 1GR-FE LHD, 1UR-FE LHD, 3UR-FE LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E22-13 (TC) - A38-16 (TC)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E22-4 (CG) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E22-13 (TC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 1GR-FE RHD, 1UR-FE RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E22-13 (TC) - A52-16 (TC)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E22-4 (CG) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E22-13 (TC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 1VD-FTV LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E22-13 (TC) - A38-27 (TC)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E22-4 (CG) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E22-13 (TC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 1VD-FTV RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>E22-13 (TC) - A52-27 (TC)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E22-4 (CG) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>E22-13 (TC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002L7101QX_03_0002" fin="true">OK</down>
<right ref="RM000002L7101QX_03_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L7101QX_03_0002">
<testtitle>PROCEED TO NEXT CIRCUIT INSPECTION SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000PLS0DAX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002L7101QX_03_0003">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>