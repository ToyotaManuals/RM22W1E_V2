<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000SVT0REX" category="C" type-id="302B4" name-id="ES11I7-002" from="201301" to="201308">
<dtccode>P0327</dtccode>
<dtcname>Knock Sensor 1 Circuit Low Input (Bank 1 or Single Sensor)</dtcname>
<dtccode>P0328</dtccode>
<dtcname>Knock Sensor 1 Circuit High Input (Bank 1 or Single Sensor)</dtcname>
<dtccode>P032C</dtccode>
<dtcname>Knock Sensor 3 Circuit Low</dtcname>
<dtccode>P032D</dtccode>
<dtcname>Knock Sensor 3 Circuit High</dtcname>
<dtccode>P0332</dtccode>
<dtcname>Knock Sensor 2 Circuit Low Input (Bank 2)</dtcname>
<dtccode>P0333</dtccode>
<dtcname>Knock Sensor 2 Circuit High Input (Bank 2)</dtcname>
<dtccode>P033C</dtccode>
<dtcname>Knock Sensor 4 Circuit Low Input</dtcname>
<dtccode>P033D</dtccode>
<dtcname>Knock Sensor 4 Circuit High Input</dtcname>
<subpara id="RM000000SVT0REX_01" type-id="60" category="03" proc-id="RM22W0E___000023U00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>A flat type knock sensor (non-resonant type) has a structure that can detect vibrations between approximately 6 kHz and 15 kHz.</ptxt>
<ptxt>Knock sensors are fitted onto the engine block to detect engine knocking.</ptxt>
<ptxt>The knock sensor contains a piezoelectric element which generates a voltage when it becomes deformed.</ptxt>
<ptxt>The voltage is generated when the engine block vibrates due to knocking. Any occurrence of engine knocking can be suppressed by delaying the ignition timing.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0327</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The output voltage of the knock sensor (for Bank 1 Sensor 1) is 0.5 V or less (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Short in knock sensor (for Bank 1 Sensor 1) circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Knock sensor (for Bank 1 Sensor 1)</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0328</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The output voltage of the knock sensor (for Bank 1 Sensor 1) is 4.5 V or higher (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Open in knock sensor (for Bank 1 Sensor 1) circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Knock sensor (for Bank 1 Sensor 1)</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0332</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The output voltage of the knock sensor (for Bank 2 Sensor 1) is 0.5 V or less (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Short in knock sensor (for Bank 2 Sensor1) circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Knock sensor (for Bank 2 Sensor 1)</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0333</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The output voltage of the knock sensor (for Bank 2 Sensor 1) is 4.5 V or higher (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Open in knock sensor (for Bank 2 Sensor1) circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Knock sensor (for Bank 2 Sensor 1)</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P032C</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The output voltage of the knock sensor (for Bank 1 Sensor 2) is 0.5 V or less (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Short in knock sensor (for Bank 1 Sensor 2) circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Knock sensor (for Bank 1 Sensor 2)</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P032D</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The output voltage of the knock sensor (for Bank 1 Sensor 2) is 4.5 V or higher (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Open in knock sensor (for Bank 1 Sensor 2) circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Knock sensor (for Bank 1 Sensor 2)</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P033C</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The output voltage of the knock sensor (for Bank 2 Sensor 2) is 0.5 V or less (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Short in knock sensor (for Bank 2 Sensor 2) circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Knock sensor (for Bank 2 Sensor 2)</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P033D</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The output voltage of the knock sensor (for Bank 2 Sensor 2) is 4.5 V or higher (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Open in knock sensor (for Bank 2 Sensor 2) circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Knock sensor (for Bank 2 Sensor 2)</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When any of DTCs P0327, P0328, P0332, P0333, P032C, P032D, P033C and P033D is stored, the ECM enters fail-safe mode. During fail-safe mode, the ignition timing is delayed to its maximum retardation. Fail-safe mode continues until the engine switch is turned off.</ptxt>
</atten4>
<ptxt>Reference: Inspection using an oscilloscope.</ptxt>
<figure>
<graphic graphicname="A150243E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard</title>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.78in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.75in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>C45-94 (KNK1) - C45-93 (EKNK)</ptxt>
</entry>
<entry>
<ptxt>0.01 to 10 V/DIV.</ptxt>
<ptxt>0.01 to 10 msec./DIV.</ptxt>
</entry>
<entry>
<ptxt>Keep engine speed at 4000 rpm with warm engine</ptxt>
</entry>
<entry>
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C45-117 (KNK2) - C45-116 (EKN2)</ptxt>
</entry>
<entry>
<ptxt>0.01 to 10 V/DIV.</ptxt>
<ptxt>0.01 to 10 msec./DIV.</ptxt>
</entry>
<entry>
<ptxt>Keep engine speed at 4000 rpm with warm engine</ptxt>
</entry>
<entry>
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C45-96 (KNK3) - C45-95 (EKN3)</ptxt>
</entry>
<entry>
<ptxt>0.01 to 10 V/DIV.</ptxt>
<ptxt>0.01 to 10 msec./DIV.</ptxt>
</entry>
<entry>
<ptxt>Keep engine speed at 4000 rpm with warm engine</ptxt>
</entry>
<entry>
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C45-119 (KNK4) - C45-118 (EKN4)</ptxt>
</entry>
<entry>
<ptxt>0.01 to 10 V/DIV.</ptxt>
<ptxt>0.01 to 10 msec./DIV.</ptxt>
</entry>
<entry>
<ptxt>Keep engine speed at 4000 rpm with warm engine</ptxt>
</entry>
<entry>
<ptxt>The correct waveform is as shown</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</content5>
</subpara>
<subpara id="RM000000SVT0REX_02" type-id="64" category="03" proc-id="RM22W0E___000023V00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The knock sensor, located on the cylinder block, detects spark knock. When spark knock occurs, the piezoelectric element of the sensor vibrates. When the ECM detects a voltage in this frequency range, it retards the ignition timing to suppress the spark knock.</ptxt>
<ptxt>The ECM also senses background engine noise with the knock sensor and uses this noise to check for faults in the sensor. If the knock sensor signal level is too low for more than 10 seconds, or if the knock sensor output voltage is outside the normal range, the ECM interprets this as a fault in the knock sensor and stores DTC(s).</ptxt>
</content5>
</subpara>
<subpara id="RM000000SVT0REX_06" type-id="32" category="03" proc-id="RM22W0E___000023W00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A165784E08" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000SVT0REX_07" type-id="51" category="05" proc-id="RM22W0E___000023X00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTCs P0327 and P0328 are for the bank 1 sensor 1 knock sensor circuit.</ptxt>
</item>
<item>
<ptxt>DTCs P032C and P032D are for the bank 1 sensor 2 knock sensor circuit.</ptxt>
</item>
<item>
<ptxt>DTCs P0332 and P0333 are for the bank 2 sensor 1 knock sensor circuit.</ptxt>
</item>
<item>
<ptxt>DTCs P033C and P033D are for the bank 2 sensor 2 knock sensor circuit.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000SVT0REX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000SVT0REX_08_0007" proc-id="RM22W0E___000023Z00000">
<testtitle>INSPECT KNOCK SENSOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A187305E05" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Disconnect the C45 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C45-94 (KNK1) - C45-93 (EKNK)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>120 to 280 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-117 (KNK2) - C45-116 (EKN2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>120 to 280 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-96 (KNK3) - C45-95 (EKN3)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>120 to 280 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C45-119 (KNK4) - C45-118 (EKN4)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>120 to 280 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Result</ptxt>
</entry>
<entry>
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>NG</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry>
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000SVT0REX_08_0008" fin="false">A</down>
<right ref="RM000000SVT0REX_08_0003" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000SVT0REX_08_0008" proc-id="RM22W0E___000024000000">
<testtitle>CHECK HARNESS AND CONNECTOR (ECM - KNOCK SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C45 ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the U3, U4, U5 or U6 knock sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>U3-2 - C45-94 (KNK1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U3-1 - C45-93 (EKNK)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U4-2 - C45-117 (KNK2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U4-1 - C45-116 (EKN2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U5-2 - C45-96 (KNK3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U5-1 - C45-95 (EKN3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U6-2 - C45-119 (KNK4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U6-1 - C45-118 (EKN4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U3-2 or C45-94 (KNK1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U3-1 or C45-93 (EKNK) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U4-2 or C45-117 (KNK2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U4-1 or C45-116 (EKN2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U5-2 or C45-96 (KNK3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U5-1 or C45-95 (EKN3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U6-2 or C45-119 (KNK4) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U6-1 or C45-118 (EKN4) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000SVT0REX_08_0011" fin="true">OK</down>
<right ref="RM000000SVT0REX_08_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000SVT0REX_08_0003" proc-id="RM22W0E___000023Y00000">
<testtitle>INSPECT ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the U3, U4, U5 or U6 knock sensor connector.</ptxt>
<figure>
<graphic graphicname="A148715E62" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC3" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000SVT0REX_08_0006" fin="true">OK</down>
<right ref="RM000000SVT0REX_08_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000SVT0REX_08_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000SVT0REX_08_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000SVT0REX_08_0011">
<testtitle>REPLACE KNOCK SENSOR<xref label="Seep01" href="RM000002PQK035X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000SVT0REX_08_0006">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ109X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>