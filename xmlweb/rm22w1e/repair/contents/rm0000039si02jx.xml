<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001S" variety="S001S">
<name>STEERING COLUMN</name>
<ttl id="12038_S001S_7C3QI_T00JL" variety="T00JL">
<name>STEERING COLUMN ASSEMBLY (for Manual Tilt and Manual Telescopic Steering Column)</name>
<para id="RM0000039SI02JX" category="A" type-id="80001" name-id="SR3WU-02" from="201308">
<name>REMOVAL</name>
<subpara id="RM0000039SI02JX_01" type-id="11" category="10" proc-id="RM22W0E___0000BIF00001">
<content3 releasenbr="1">
<atten2>
<ptxt>Some of these service operations affect the SRS airbag system. Read the precautionary notices concerning the SRS airbag system before servicing the steering column (See page <xref label="Seep01" href="RM000000KT10L4X"/>).</ptxt>
</atten2>
</content3>
</subpara>
<subpara id="RM0000039SI02JX_02" type-id="01" category="01">
<s-1 id="RM0000039SI02JX_02_0001">
<ptxt>FRONT WHEELS FACING STRAIGHT AHEAD</ptxt>
</s-1>
<s-1 id="RM0000039SI02JX_02_0003" proc-id="RM22W0E___0000BIG00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After the engine switch is turned off, the navigation system requires approximately 90 seconds to record various types of memory and settings. As a result, after turning the engine switch off, wait 90 seconds or more before disconnecting the cable from the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003C32006X"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039SI02JX_02_0004" proc-id="RM22W0E___000077Q00001">
<ptxt>REMOVE LOWER NO. 3 STEERING WHEEL COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the steering wheel cover.</ptxt>
<figure>
<graphic graphicname="B181563" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0005" proc-id="RM22W0E___000077R00001">
<ptxt>REMOVE LOWER NO. 2 STEERING WHEEL COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the steering wheel cover.</ptxt>
<figure>
<graphic graphicname="B189186E01" width="2.775699831in" height="4.7836529in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0006" proc-id="RM22W0E___000077P00001">
<ptxt>REMOVE STEERING PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T30 "TORX" socket wrench, loosen the 2 screws until the groove along the screw circumference catches on the screw case.</ptxt>
<figure>
<graphic graphicname="B187261E01" width="2.775699831in" height="5.787629434in"/>
</figure>
</s2>
<s2>
<ptxt>Pull out the steering pad from the steering wheel, as shown in the illustration. Then support the steering pad with one hand.</ptxt>
<figure>
<graphic graphicname="B181568" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When removing the steering pad, do not pull the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Disconnect the horn connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 connectors and remove the steering pad.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0031" proc-id="RM22W0E___0000A9A00001">
<ptxt>REMOVE STEERING WHEEL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the steering wheel set nut.</ptxt>
</s2>
<s2>
<ptxt>Put matchmarks on the steering wheel and main shaft.</ptxt>
<figure>
<graphic graphicname="C172673E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using SST, remove the steering wheel assembly.</ptxt>
<sst>
<sstitem>
<s-number>09950-50013</s-number>
<s-subnumber>09951-05010</s-subnumber>
<s-subnumber>09952-05010</s-subnumber>
<s-subnumber>09953-05020</s-subnumber>
<s-subnumber>09954-05011</s-subnumber>
</sstitem>
</sst>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0008" proc-id="RM22W0E___0000A9B00001">
<ptxt>REMOVE LOWER STEERING COLUMN COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="C179481" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws to remove the lower steering column cover.</ptxt>
<atten3>
<ptxt>Do not damage the tilt and telescopic switch.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SI02JX_02_0009" proc-id="RM22W0E___0000A9C00001">
<ptxt>REMOVE UPPER STEERING COLUMN COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 clips.</ptxt>
<figure>
<graphic graphicname="C179482" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw to remove the upper steering column cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SI02JX_02_0011" proc-id="RM22W0E___0000BIH00001">
<ptxt>REMOVE COMBINATION SWITCH ASSEMBLY WITH SPIRAL CABLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connectors from the combination switch with spiral cable.</ptxt>
</s2>
<s2>
<ptxt>Using pliers, grip the claws of the clamp and remove the combination switch with spiral cable from the steering column.</ptxt>
<figure>
<graphic graphicname="C179484E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SI02JX_02_0012" proc-id="RM22W0E___00008UQ00000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292994E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the No. 2 instrument panel finish panel cushion.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 7 claws and remove the No. 2 instrument panel finish panel cushion.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0013" proc-id="RM22W0E___00008UR00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL PAD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B292995" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the clip and screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 8 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors, detach the 2 clamps and remove the lower instrument panel pad sub-assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0014" proc-id="RM22W0E___0000BB300001">
<ptxt>REMOVE INSTRUMENT SIDE PANEL LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154740E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 6 claws and remove the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0015" proc-id="RM22W0E___0000BB400001">
<ptxt>REMOVE NO. 1 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E154744E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 3 claws and remove the No. 1 instrument cluster finish panel garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0016" proc-id="RM22W0E___0000BB500001">
<ptxt>REMOVE NO. 2 INSTRUMENT CLUSTER FINISH PANEL GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place protective tape as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws and remove the No. 2 instrument cluster finish panel garnish.</ptxt>
<figure>
<graphic graphicname="B291251E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Entry and Start System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0017" proc-id="RM22W0E___0000BB600001">
<ptxt>REMOVE INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY (w/ Multi-information Display)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180017" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 9 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the instrument cluster finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0032" proc-id="RM22W0E___0000BBB00001">
<ptxt>REMOVE INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY (w/o Multi-information Display)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180016" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 9 claws and remove the instrument cluster finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0019" proc-id="RM22W0E___000014A00001">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY (w/ Floor Under Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180655" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the No. 1 instrument panel under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0018">
<ptxt>REMOVE FRONT DOOR SCUFF PLATE LH</ptxt>
</s-1>
<s-1 id="RM0000039SI02JX_02_0020" proc-id="RM22W0E___0000A9R00001">
<ptxt>REMOVE COWL SIDE TRIM BOARD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181911" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the cap nut.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clips and remove the cowl side trim board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0021" proc-id="RM22W0E___0000A9S00001">
<ptxt>REMOVE LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180295E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the hole cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 16 claws.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts.</ptxt>
</s3>
<s3>
<ptxt>Detach the 9 claws.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Detach the 2 claws and remove the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Detach the 2 claws and disconnect the 2 control cables.</ptxt>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the lower No. 1 instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0022" proc-id="RM22W0E___0000A9P00001">
<ptxt>REMOVE NO. 1 SWITCH HOLE BASE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180298" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the No. 1 switch hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0023" proc-id="RM22W0E___0000ATC00001">
<ptxt>REMOVE DRIVER SIDE KNEE AIRBAG ASSEMBLY (w/ Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and driver side knee airbag.</ptxt>
<figure>
<graphic graphicname="B189602E01" width="2.775699831in" height="6.791605969in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0033" proc-id="RM22W0E___0000BBC00001">
<ptxt>REMOVE LOWER INSTRUMENT PANEL SUB-ASSEMBLY (w/o Driver Side Knee Airbag)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180299" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws and disconnect the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Remove the 5 bolts and lower instrument panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0024" proc-id="RM22W0E___0000BII00001">
<ptxt>DISCONNECT WIRE HARNESS PROTECTOR AND WIRE HARNESS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws to disconnect the wire harness protector and wire harness.</ptxt>
<figure>
<graphic graphicname="C172656" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SI02JX_02_0030" proc-id="RM22W0E___0000BB900001">
<ptxt>REMOVE NO. 3 AIR DUCT SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E156901" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clip.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the duct.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SI02JX_02_0027" proc-id="RM22W0E___0000BIK00001">
<ptxt>REMOVE STEERING COLUMN ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put matchmarks on the steering intermediate shaft and the steering column.</ptxt>
<figure>
<graphic graphicname="C172648E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the bolt.</ptxt>
</s2>
<s2>
<ptxt>w/o Entry and Start System:</ptxt>
<s3>
<ptxt>Remove the 4 nuts and steering column.</ptxt>
<figure>
<graphic graphicname="C246797E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Entry and Start System:</ptxt>
<s3>
<ptxt>Remove the 4 nuts and steering column.</ptxt>
<figure>
<graphic graphicname="C172654" width="2.775699831in" height="3.779676365in"/>
</figure>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SI02JX_02_0026" proc-id="RM22W0E___0000BIJ00001">
<ptxt>DISCONNECT STEERING INTERMEDIATE SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put matchmarks on the steering intermediate shaft and the No. 2 steering intermediate shaft.</ptxt>
<figure>
<graphic graphicname="C172649E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the bolt, and then pull out the intermediate shaft toward the inside of the vehicle.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SI02JX_02_0034" proc-id="RM22W0E___0000BIM00001">
<ptxt>REMOVE STEERING COLUMN HOLE COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Hold the clamp with needle nose pliers, then insert a screwdriver and turn it in the direction shown in the illustration to remove the clamp of the steering column hole cover.</ptxt>
<figure>
<graphic graphicname="C179097" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 bolts and nut with the steering column hole cover from the vehicle.</ptxt>
<figure>
<graphic graphicname="C179099" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SI02JX_02_0029" proc-id="RM22W0E___0000BIL00001">
<ptxt>DISCONNECT NO. 2 STEERING INTERMEDIATE SHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the bolt and remove the No. 2 intermediate shaft.</ptxt>
<figure>
<graphic graphicname="C172652E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>It is possible to install the intermediate shaft to the same position it was removed from without placing matchmarks due to the dust cover part labeled A. Therefore, do not remove the dust cover from the steering link.</ptxt>
</atten3>
<atten4>
<ptxt>If the dust cover is removed/installed or replaced, place matchmarks on the dust cover and steering link.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>