<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WK_T00PN" variety="T00PN">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000003E0P03OX" category="C" type-id="300BD" name-id="ACBKV-03" from="201301" to="201308">
<dtccode>B1423/23</dtccode>
<dtcname>Pressure Sensor Circuit</dtcname>
<subpara id="RM000003E0P03OX_01" type-id="60" category="03" proc-id="RM22W0E___0000H2700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is output when refrigerant pressure is extremely low (-0.4566 MPaG (-4.6 kgf/cm<sup>2</sup>, -66 psi) or less) or extremely high (3.2943 MPaG (33.6 kgf/cm<sup>2</sup>, 477 psi) or more). The air conditioner pressure sensor, which is installed on the pipe of the high pressure side to detect refrigerant pressure, outputs the refrigerant pressure signal to the air conditioning amplifier assembly. The air conditioning amplifier assembly converts the signal to pressure according to the sensor characteristics to control the compressor.</ptxt>
<atten4>
<ptxt>Be sure to check the refrigerant volume first when this DTC is output because this DTC can also be output if there is no refrigerant in the cycle.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1423/23</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>An open or short in the air conditioner pressure sensor circuit</ptxt>
</item>
<item>
<ptxt>Refrigerant pressure on the high pressure side is extremely low (-0.4566 MPaG (-4.6 kgf/cm<sup>2</sup>, -66 psi) or less) or extremely high (3.2943 MPaG (33.6 kgf/cm<sup>2</sup>, 477 psi) or more)</ptxt>
</item>
</list1>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Air conditioner pressure sensor</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Cooler expansion valve (blocked, stuck)</ptxt>
</item>
<item>
<ptxt>Cooler condenser assembly (blocked, deterioration of cooling capacity due to dirt)</ptxt>
</item>
<item>
<ptxt>Cooler dryer (moisture in the refrigerant cycle cannot be absorbed)</ptxt>
</item>
<item>
<ptxt>Condenser fan (condenser cannot be cooled down)</ptxt>
</item>
<item>
<ptxt>Air conditioning system (leaks, blocked)</ptxt>
</item>
<item>
<ptxt>Refrigerant pipe line</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003E0P03OX_02" type-id="32" category="03" proc-id="RM22W0E___0000H2800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E156766E07" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003E0P03OX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003E0P03OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003E0P03OX_04_0001" proc-id="RM22W0E___0000H2900000">
<testtitle>CHECK HARNESS AND CONNECTOR (POWER SOURCE CIRCUIT)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E160140E08" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the A13 air conditioner pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A13-3 (+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.4 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0002" fin="false">OK</down>
<right ref="RM000003E0P03OX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0002" proc-id="RM22W0E___0000H2A00000">
<testtitle>CHECK HARNESS AND CONNECTOR (GROUND CIRCUIT)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E160140E09" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A13-1 (-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0003" fin="false">OK</down>
<right ref="RM000003E0P03OX_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0003" proc-id="RM22W0E___0000H2B00000">
<testtitle>INSPECT AIR CONDITIONER PRESSURE SENSOR (SENSOR SIGNAL CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the A13 pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Remove the air conditioning amplifier with its connectors still connected.</ptxt>
<figure>
<graphic graphicname="E158866E07" width="2.775699831in" height="4.7836529in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<subtitle>w/ Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E35-20 (PRE) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>A/C switch off</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>0.7 to 4.8 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>w/o Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E81-9 (PRE) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>A/C switch off</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>0.7 to 4.8 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>If the measured voltage is not within the normal range, there may be a malfunction in the air conditioning amplifier assembly, air conditioner pressure sensor, or wire harness. It is also possible that the amount of refrigerant may not be appropriate.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0004" fin="false">OK</down>
<right ref="RM000003E0P03OX_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0004" proc-id="RM22W0E___0000H2C00000">
<testtitle>INSPECT AIR CONDITIONER PRESSURE SENSOR (SENSOR SIGNAL CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage when the following conditions are satisfied.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry align="center">
<ptxt>1500 rpm</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Vehicle Doors</ptxt>
</entry>
<entry align="center">
<ptxt>Fully open</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Temperature Setting</ptxt>
</entry>
<entry align="center">
<ptxt>MAX COLD</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Blower Speed</ptxt>
</entry>
<entry align="center">
<ptxt>HI</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>A/C Switch</ptxt>
</entry>
<entry align="center">
<ptxt>ON</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>R/F Switch</ptxt>
</entry>
<entry align="center">
<ptxt>RECIRCULATION</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Interior Temperature</ptxt>
</entry>
<entry align="center">
<ptxt>25 to 35°C (77 to 95°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>If refrigerant pressure on the high pressure side becomes extremely high during the inspection (if the voltage exceeds 4.8 V), the fail-safe function stops compressor operation. Therefore, measure the voltage before the fail-safe operation.</ptxt>
</item>
<item>
<ptxt>It is necessary to measure the voltage for a certain amount of time (approximately 10 minutes) because the problem symptom may recur after a while.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>When the outside air temperature is low (below -1.5°C (29.3°F)), the compressor stops due to operation of the ambient temperature and the evaporator temperature sensor to prevent the evaporator from freezing. In this case, perform the inspection in a warm indoor environment.</ptxt>
</atten4>
<test2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E158866E08" width="2.775699831in" height="4.7836529in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<subtitle>w/ Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>E35-20 (PRE) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>A/C switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>0.7 to 4.8 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>w/o Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B81-9 (PRE) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine switch on (IG)</ptxt>
</item>
<item>
<ptxt>A/C switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>0.7 to 4.8 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0020" fin="true">OK</down>
<right ref="RM000003E0P03OX_04_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0005" proc-id="RM22W0E___0000H2D00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - PRESSURE SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E158867E10" width="2.775699831in" height="6.791605969in"/>
</figure>
<test1>
<ptxt>Disconnect the E35*1 or E81*2 amplifier connector.</ptxt>
<atten4>
<ptxt>*1: w/ Rear Heater</ptxt>
<ptxt>*2: w/o Rear Heater</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>w/ Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A13-3 (+) - E35-27 (S5-1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E35-27 (S5-1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>w/o Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A13-3 (+) - E81-30 (S5-1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E81-30 (S5-1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0020" fin="true">OK</down>
<right ref="RM000003E0P03OX_04_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0006" proc-id="RM22W0E___0000H2E00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - PRESSURE SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E158867E11" width="2.775699831in" height="6.791605969in"/>
</figure>
<test1>
<ptxt>Disconnect the E35*1 or E81*2 amplifier connector.</ptxt>
<atten4>
<ptxt>*1: w/ Rear Heater</ptxt>
<ptxt>*2: w/o Rear Heater</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>w/ Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A13-1 (-) - E35-16 (SG-2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E35-16 (SG-2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>w/o Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A13-1 (-) - E81-13 (SG-2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E81-13 (SG-2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0020" fin="true">OK</down>
<right ref="RM000003E0P03OX_04_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0007" proc-id="RM22W0E___0000H2F00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - PRESSURE SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E158867E12" width="2.775699831in" height="6.791605969in"/>
</figure>
<test1>
<ptxt>Disconnect the E35*1 or E81*2 amplifier connector.</ptxt>
<atten4>
<ptxt>*1: w/ Rear Heater</ptxt>
<ptxt>*2: w/o Rear Heater</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>w/ Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A13-2 (PR) - E35-20 (PRE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E35-20 (PRE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>w/o Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A13-2 (PR) - E81-9 (PRE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E81-9 (PRE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0008" fin="false">OK</down>
<right ref="RM000003E0P03OX_04_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0008" proc-id="RM22W0E___0000H2G00000">
<testtitle>INSPECT FOR AIR CONDITIONING SYSTEM LEAK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Install the manifold gauge set.</ptxt>
</test1>
<test1>
<ptxt>Recover the refrigerant from the air conditioning system using a refrigerant recovery unit.</ptxt>
</test1>
<test1>
<ptxt>Evacuate the air conditioning system and check that vacuum can be maintained in it.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Vacuum can be maintained in the air conditioning system.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>If vacuum cannot be maintained in the air conditioning system, refrigerant may be leaking from it. In this case, it is necessary to repair or replace the leaking part of the air conditioning system.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0009" fin="false">OK</down>
<right ref="RM000003E0P03OX_04_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0009" proc-id="RM22W0E___0000H2H00000">
<testtitle>CHARGE REFRIGERANT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Add an appropriate amount of refrigerant (See page <xref label="Seep01" href="RM000001R8Y03SX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0010" proc-id="RM22W0E___0000H2I00000">
<testtitle>RECHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for the DTC when the following conditions are satisfied.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry align="center">
<ptxt>1500 rpm</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Vehicle Doors</ptxt>
</entry>
<entry align="center">
<ptxt>Fully open</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Temperature Setting</ptxt>
</entry>
<entry align="center">
<ptxt>MAX COLD</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Blower Speed</ptxt>
</entry>
<entry align="center">
<ptxt>HI</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>A/C Switch</ptxt>
</entry>
<entry align="center">
<ptxt>ON</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>R/F Switch</ptxt>
</entry>
<entry align="center">
<ptxt>RECIRCULATION</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Interior Temperature</ptxt>
</entry>
<entry align="center">
<ptxt>25 to 35°C (77 to 95°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>If refrigerant pressure on the high pressure side becomes high, the DTC will be stored. Therefore, it is necessary to measure the voltage for a certain amount of time (approximately 10 minutes) because the DTC may be stored after the air conditioning system operates for a while.</ptxt>
</atten3>
<atten4>
<ptxt>When the outside air temperature is low (below -1.5°C (29.3°F)), the compressor stops due to operation of the ambient temperature sensor and the evaporator temperature sensor to prevent the evaporator from freezing. In this case, perform the inspection in a warm indoor environment.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1423 is not output.</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>If the DTC was output due to an insufficient or excessive amount of refrigerant, the problem may have been solved after performing the previous step. However, the root cause of insufficient refrigerant may be refrigerant leaks. The root cause of excessive refrigerant may be adding refrigerant when the level was insufficient. Therefore, identify and repair the area where refrigerant leaks as necessary.</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0028" fin="true">OK</down>
<right ref="RM000003E0P03OX_04_0011" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0011" proc-id="RM22W0E___0000H2J00000">
<testtitle>CHECK AIR CONDITIONER PRESSURE SENSOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="E173740E01" width="2.775699831in" height="5.787629434in"/>
</figure>
<test1>
<ptxt>Install the manifold gauge set.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the A13 air conditioner pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Remove the air conditioning amplifier assembly with its connectors still connected.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<subtitle>w/ Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.18in"/>
<colspec colname="COL2" colwidth="1.71in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>E35-20 (PRE) - E35-16 (SG-2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Refrigerant pressure:</ptxt>
<ptxt>Normal pressure (less than 3.0732 MPa [31.4 kgf/cm<sup>2</sup>, 445 psi] and more than 0.1762 MPa [1.8 kgf/cm<sup>2</sup>, 25 psi])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.0 to 4.8 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Refrigerant pressure:</ptxt>
<ptxt>Abnormal (less than 0.1762 MPa [1.8 kgf/cm<sup>2</sup>, 25 psi])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Refrigerant pressure:</ptxt>
<ptxt>Abnormal (more than 3.0732 MPa [31.4 kgf/cm<sup>2</sup>, 445 psi])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.8 V or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>w/o Rear Heater</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.18in"/>
<colspec colname="COL2" colwidth="1.71in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>E81-9 (PRE) - E81-13 (SG-2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Refrigerant pressure:</ptxt>
<ptxt>Normal pressure (less than 3.0732 MPa [31.4 kgf/cm<sup>2</sup>, 445 psi] and more than 0.1762 MPa [1.8 kgf/cm<sup>2</sup>, 25 psi])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.0 to 4.8 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Refrigerant pressure:</ptxt>
<ptxt>Abnormal (less than 0.1762 MPa [1.8 kgf/cm<sup>2</sup>, 25 psi])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Refrigerant pressure:</ptxt>
<ptxt>Abnormal (more than 3.0732 MPa [31.4 kgf/cm<sup>2</sup>, 445 psi])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.8 V or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0020" fin="true">OK</down>
<right ref="RM000003E0P03OX_04_0029" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0012" proc-id="RM22W0E___0000H2K00000">
<testtitle>REPAIR AIR CONDITIONING SYSTEM LEAK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Identify the area where refrigerant leaks (See page <xref label="Seep01" href="RM000001R8Z03OX"/>).</ptxt>
</test1>
<test1>
<ptxt>Repair the identified area of the air conditioning system.</ptxt>
</test1>
<test1>
<ptxt>Evacuate the air conditioning system.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0032" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0013" proc-id="RM22W0E___0000H2L00000">
<testtitle>INSPECT CONDENSER FAN</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>For vehicles without an condenser fan, go to "CHARGE REFRIGERANT".</ptxt>
</atten3>
<test1>
<ptxt>Inspect that the condenser fan operates normally (See page <xref label="Seep01" href="RM00000393K010X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0014" fin="false">OK</down>
<right ref="RM000003E0P03OX_04_0033" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0014" proc-id="RM22W0E___0000H2M00000">
<testtitle>CHARGE REFRIGERANT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use a refrigerant recovery unit to recover refrigerant.</ptxt>
</test1>
<test1>
<ptxt>Evacuate the air conditioning system.</ptxt>
</test1>
<test1>
<ptxt>Add an appropriate amount of refrigerant (See page <xref label="Seep01" href="RM000001R8Y03SX"/>).</ptxt>
<atten4>
<ptxt>If refrigerant is added and the system has not been properly evacuated (insufficient vacuum time), moisture in the air remaining in the system will freeze in the expansion valve, blocking the flow on the high pressure side. Therefore, in order to confirm the problem, recover the refrigerant and properly evacuate the system. Add an appropriate amount of refrigerant, and check for the DTC.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0015" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0015" proc-id="RM22W0E___0000H2N00000">
<testtitle>RECHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for the DTC when the following conditions are satisfied.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry align="center">
<ptxt>1500 rpm</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Doors</ptxt>
</entry>
<entry align="center">
<ptxt>Fully open</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Temperature Setting</ptxt>
</entry>
<entry align="center">
<ptxt>MAX COLD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Blower Speed</ptxt>
</entry>
<entry align="center">
<ptxt>HI</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A/C Switch</ptxt>
</entry>
<entry align="center">
<ptxt>ON</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R/F Switch</ptxt>
</entry>
<entry align="center">
<ptxt>RECIRCULATION</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Interior Temperature</ptxt>
</entry>
<entry align="center">
<ptxt>25 to 35°C (77 to 95°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>If refrigerant pressure on the high pressure side becomes high, the DTC will be stored. Therefore, it is necessary to measure the voltage for a certain amount of time (approximately 10 minutes) because the DTC may be stored after the air conditioning system operates for a while.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the outside air temperature is low (below -1.5°C (29.3°F)), the compressor stops due to operation of the ambient temperature sensor and the evaporator temperature sensor to prevent the evaporator from freezing. In this case, perform the inspection in a warm indoor environment.</ptxt>
</item>
<item>
<ptxt>If refrigerant is added and the system has not been properly evacuated (insufficient vacuum time), moisture in the air remaining in the system will freeze in the expansion valve, blocking the flow on the high pressure side. Therefore, in order to confirm the problem, recover the refrigerant and properly evacuate the system. Add an appropriate amount of refrigerant, and check for the DTC. If the DTC is not output after this procedure, it indicates that the cooler dryer in the condenser is not able to absorb moisture in the refrigerant cycle. In this case, to complete the repair it is necessary to replace the cooler dryer.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1423 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0034" fin="true">OK</down>
<right ref="RM000003E0P03OX_04_0016" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0016" proc-id="RM22W0E___0000H2O00000">
<testtitle>REPLACE COOLER EXPANSION VALVE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the cooler expansion valve with a normal one.</ptxt>
<ptxt>for LHD (See page <xref label="Seep01" href="RM000003AXS02WX"/>)</ptxt>
<ptxt>for RHD (See page <xref label="Seep02" href="RM000003AXS02VX"/>)</ptxt>
<atten4>
<ptxt>Replace the cooler expansion valve with a normal one because the cooler expansion valve is either stuck or clogged.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0017" proc-id="RM22W0E___0000H2P00000">
<testtitle>CHARGE REFRIGERANT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Add an appropriate amount of refrigerant (See page <xref label="Seep01" href="RM000001R8Y03SX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0018" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0018" proc-id="RM22W0E___0000H2Q00000">
<testtitle>RECHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recheck for the DTC when the following conditions are satisfied.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry align="center">
<ptxt>1500 rpm</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Vehicle Doors</ptxt>
</entry>
<entry align="center">
<ptxt>Fully open</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Temperature Setting</ptxt>
</entry>
<entry align="center">
<ptxt>MAX COLD</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Blower Speed</ptxt>
</entry>
<entry align="center">
<ptxt>HI</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>A/C Switch</ptxt>
</entry>
<entry align="center">
<ptxt>ON</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>R/F Switch</ptxt>
</entry>
<entry align="center">
<ptxt>RECIRCULATION</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Interior Temperature</ptxt>
</entry>
<entry align="center">
<ptxt>25 to 35°C (77 to 95°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>If refrigerant pressure on the high pressure side becomes high, the DTC will be stored. Therefore, it is necessary to measure the voltage for a certain amount of time (approximately 10 minutes) because the DTC may be stored after the air conditioning system operates for a while.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the outside air temperature is low (below -1.5°C (29.3°F)), the compressor stops due to operation of the ambient temperature sensor and the evaporator temperature sensor to prevent the evaporator from freezing. In this case, perform the inspection in a warm indoor environment.</ptxt>
</item>
<item>
<ptxt>If refrigerant pressure is not normal after replacing the expansion valve with a normal one, the condenser or pipes may be clogged due to dirt, dust or other contaminants. In this case, clean or replace the condenser or pipes.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1423 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P03OX_04_0028" fin="true">OK</down>
<right ref="RM000003E0P03OX_04_0035" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0020">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0022">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0028">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0029">
<testtitle>REPLACE AIR CONDITIONER PRESSURE SENSOR<xref label="Seep01" href="RM0000030JI033X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0032">
<testtitle>CHARGE REFRIGERANT<xref label="Seep01" href="RM000001R8Y03SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0033">
<testtitle>REPLACE CONDENSER FAN<xref label="Seep01" href="RM00000180603WX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0034">
<testtitle>REPLACE COOLER DRYER<xref label="Seep01" href="RM00000180603XX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P03OX_04_0035">
<testtitle>REPLACE COOLER CONDENSER ASSEMBLY<xref label="Seep01" href="RM00000180603XX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>