<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000004KNM00LX" category="J" type-id="805CU" name-id="ESX42-03" from="201301" to="201308">
<dtccode/>
<dtcname>After Treatment Control System</dtcname>
<subpara id="RM000004KNM00LX_01" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000004KNM00LX_02" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004KNM00LX_02_0006" proc-id="RM22W0E___000041P00000">
<testtitle>PERFORM PM FORCED REGENERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / Coolant Temp.</ptxt>
</test1>
<test1>
<ptxt>Warm up the engine (engine coolant temperature is 70°C (158°F) or higher).</ptxt>
</test1>
<test1>
<ptxt>Perform PM forced regeneration (See page <xref label="Seep02" href="RM00000141502YX_01_0014"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNM00LX_02_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNM00LX_02_0001" proc-id="RM22W0E___000041M00000">
<testtitle>TAKE SNAPSHOT DURING IDLING AND 4000 RPM (PROCEDURE 2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / All Data.</ptxt>
</test1>
<test1>
<ptxt>Take a snapshot of the Data List items.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A snapshot can be used to compare vehicle data from the time of the malfunction to normal data and is very useful for troubleshooting. The data in the list below is that of a normal vehicle, but as the data varies between individual vehicles, this data should only be used for reference.</ptxt>
</item>
<item>
<ptxt>Check the Data List at idling and at 4000 rpm with no load after the engine is warmed up.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KNM00LX_02_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNM00LX_02_0002" proc-id="RM22W0E___000041N00000">
<testtitle>READ VALUE USING GTS (CATALYST DIFFERENTIAL PRESS AND CATALYST DIFFERENTIAL PRESS #2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check Catalyst Differential Press and Catalyst Differential Press #2 in the snapshot taken in procedure 2 when the engine was running at 4000 rpm with no load.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Catalyst Differential Press is more than 0.45</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Catalyst Differential Press #2 is more than 0.45</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004KNM00LX_02_0003" fin="false">A</down>
<right ref="RM000004KNM00LX_02_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000004KNM00LX_02_0003" proc-id="RM22W0E___000041O00000">
<testtitle>REPLACE EXHAUST PIPE ASSEMBLIES (DPF CATALYTIC CONVERTERS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the front exhaust pipe assembly (for bank 1 DPF catalytic converter) (See page <xref label="Seep01" href="RM000003B0N00HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Replace the front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter) (See page <xref label="Seep02" href="RM000003B0N00HX"/>).</ptxt>
<atten4>
<ptxt>Replace the DPF catalytic converter for bank 1 and bank 2 at the same time.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KNM00LX_02_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNM00LX_02_0007" proc-id="RM22W0E___000041Q00000">
<testtitle>CATALYST RECORD CLEAR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the menu options in this order: Powertrain / Engine and ECT / Utility / Catalyst Record Clear.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNM00LX_02_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNM00LX_02_0005">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<res>
<down ref="RM000004KNM00LX_02_0004" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNM00LX_02_0004">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>