<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM0000013H806DX" category="C" type-id="302L5" name-id="ESUBD-04" from="201301" to="201308">
<dtccode>P2A00</dtccode>
<dtcname>A/F Sensor Circuit Slow Response (Bank 1 Sensor 1)</dtcname>
<dtccode>P2A03</dtccode>
<dtcname>A/F Sensor Circuit Slow Response (Bank 2 Sensor 1)</dtcname>
<subpara id="RM0000013H806DX_03" type-id="60" category="03" proc-id="RM22W0E___00001IM00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2195 (See page <xref label="Seep01" href="RM000000WC40S7X_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2A00</ptxt>
</entry>
<entry valign="middle">
<ptxt>Calculated value of the air fuel ratio sensor response rate deterioration level is less than the threshold (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor (for Bank 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2A03</ptxt>
</entry>
<entry valign="middle">
<ptxt>Calculated value of the air fuel ratio sensor response rate deterioration level is less than the threshold (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor (for Bank 2)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000013H806DX_04" type-id="64" category="03" proc-id="RM22W0E___00001IN00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>After the engine is warmed up, the ECM performs air-fuel ratio feedback control to maintain the air-fuel ratio at the stoichiometric level. In addition, active A/F control is performed for approximately 10 seconds after the preconditions are met in order to measure the air fuel ratio sensor response rate. During active A/F control, the ECM forcibly increases and decreases the injection volume a certain amount based on the stoichiometric air-fuel ratio learned during normal air-fuel ratio control, and measures the air fuel ratio sensor response rate. The ECM receives a signal from the air fuel ratio sensor while performing active A/F control and uses it to calculate the air fuel ratio sensor response rate deterioration level.</ptxt>
<ptxt>If the air fuel ratio sensor response rate deterioration level is less than the threshold, the ECM interprets this as a malfunction and stores the DTC.</ptxt>
<figure>
<graphic graphicname="A115363E31" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000013H806DX_05" type-id="73" category="03" proc-id="RM22W0E___00001IO00000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Performing this confirmation driving pattern will activate the A/F sensor response monitor.</ptxt>
</atten4>
<figure>
<graphic graphicname="A231470E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG).</ptxt>
</item>
<item>
<ptxt>Turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Start the engine and warm it up until the engine coolant temperature is 75°C (167°F) or higher [A].</ptxt>
</item>
<item>
<ptxt>Drive the vehicle at approximately 60 km/h (38 mph) for 10 minutes or more in a city area [B].</ptxt>
<atten2>
<ptxt>When performing the confirmation driving pattern, obey all speed limits and traffic laws.</ptxt>
</atten2>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes [C].</ptxt>
</item>
<item>
<ptxt>Read the pending DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a pending DTC is output, the system is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If a pending DTC is not output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P2A00 or P2A03.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
<item>
<ptxt>If the test result is INCOMPLETE or N/A and no pending DTC is output, perform the following procedure.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Drive the vehicle at a speed between 75 and 100 km/h (47 and 63 mph) for 10 minutes [D].</ptxt>
<atten2>
<ptxt>When performing the confirmation driving pattern, obey all speed limits and traffic laws.</ptxt>
</atten2>
</item>
<item>
<ptxt>Check the DTC judgment result [E].</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM0000013H806DX_10" type-id="32" category="03" proc-id="RM22W0E___00001IP00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2195 (See page <xref label="Seep01" href="RM000000WC40S7X_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000013H806DX_11" type-id="51" category="05" proc-id="RM22W0E___00001IQ00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Sensor 1 refers to the sensor mounted in front of the Three-way Catalytic Converter (TWC) and located near the engine assembly.</ptxt>
</item>
<item>
<ptxt>DTC P2A00 indicates malfunctions related to the bank 1 air fuel ratio sensor.</ptxt>
</item>
<item>
<ptxt>DTC P2A03 indicates malfunctions related to the bank 2 air fuel ratio sensor.</ptxt>
</item>
<item>
<ptxt>DTC P2A00 or P2A03 may be stored when the air-fuel ratio is stuck at a rich or lean value.</ptxt>
</item>
<item>
<ptxt>A low air fuel ratio sensor voltage could be caused by a rich air-fuel mixture. Check for conditions that would cause the engine to run rich.</ptxt>
</item>
<item>
<ptxt>A high air fuel ratio sensor voltage could be caused by a lean air-fuel mixture. Check for conditions that would cause the engine to run lean.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>
<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Sensor 1 refers to the sensor closest to the engine assembly.</ptxt>
</item>
<item>
<ptxt>Sensor 2 refers to the sensor farthest away from the engine assembly.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM0000013H806DX_12" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000013H806DX_12_0002" proc-id="RM22W0E___00001IR00000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P2A00 AND/OR P2A03)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2A00 and/or P2A03 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2A00 and/or P2A03 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs relating to the air fuel ratio sensor (DTCs for the air fuel ratio sensor heater or air fuel ratio sensor admittance) are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000013H806DX_12_0024" fin="false">A</down>
<right ref="RM0000013H806DX_12_0011" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0024" proc-id="RM22W0E___00001IW00000">
<testtitle>PERFORM ACTIVE TEST USING GTS (INJECTION VOLUME)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Warm up the engine and run the engine at an engine speed of 2500 rpm for approximately 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F sensor.</ptxt>
</test1>
<test1>
<ptxt>Perform the Control the Injection Volume operation with the engine idling.</ptxt>
</test1>
<test1>
<ptxt>Monitor the output voltages of the air fuel ratio and heated oxygen sensors (AFS Voltage B1S1 and O2S B1S2 or AFS Voltage B2S1 and O2S B2S2) displayed on the GTS.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Change the fuel injection volume within the range of - 12.5% to +12.5%. The injection volume can be changed in fine gradations.</ptxt>
</item>
<item>
<ptxt>The air fuel ratio sensor has an output delay of a few seconds and the heated oxygen sensor has a maximum output delay of approximately 20 seconds.</ptxt>
</item>
<item>
<ptxt>If the air fuel ratio sensor output voltage does not change (almost no reaction) while performing the Active Test, the air fuel ratio sensor may be malfunctioning.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>GTS Display (Sensor)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injection Volume</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Voltage</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>AFS Voltage B1S1 or AFS Voltage B2S1</ptxt>
<ptxt>(Air fuel ratio)</ptxt>
</entry>
<entry valign="middle">
<ptxt>+12.5%</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 3.1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>AFS Voltage B1S1 or AFS Voltage B2S1</ptxt>
<ptxt>(Air fuel ratio)</ptxt>
</entry>
<entry valign="middle">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle">
<ptxt>Higher than 3.4 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>O2S B1S2 or O2S B2S2</ptxt>
<ptxt>(Heated oxygen)</ptxt>
</entry>
<entry valign="middle">
<ptxt>+12.5%</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Higher than 0.55 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>O2S B1S2 or O2S B2S2</ptxt>
<ptxt>(Heated oxygen)</ptxt>
</entry>
<entry valign="middle">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 0.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Result</title>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.57in"/>
<colspec colname="COL2" colwidth="1.57in"/>
<colspec colname="COL3" colwidth="2.47in"/>
<colspec colname="COL6" colwidth="1.47in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Status of AFS Voltage B1S1 or AFS Voltage B2S1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status of O2S B1S2 or O2S B2S2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air Fuel Ratio Condition and Air Fuel Ratio Sensor Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Actual air fuel ratio lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Actual air fuel ratio rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air fuel ratio sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air fuel ratio sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>Lean: During the Control the Injection Volume Active Test, the air fuel ratio sensor output voltage (AFS Voltage) is consistently higher than 3.4 V, and the heated oxygen sensor output voltage (O2S) is consistently below 0.4 V.</ptxt>
<ptxt>Rich: During the Control the Injection Volume Active Test, the AFS Voltage is consistently below 3.1 V, and the O2S is consistently higher than 0.55 V.</ptxt>
<ptxt>Lean/Rich: During the Control the Injection Volume Active Test, the output voltage of the heated oxygen sensor alternates correctly.</ptxt>
</test1>
<test1>
<ptxt>Refer to "Data List / Active Test" [AFS Voltage B1S1, AFS Voltage B2S1, O2S B1S2 and O2S B2S2] (See page <xref label="Seep01" href="RM000000SXS097X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013H806DX_12_0005" fin="false">A</down>
<right ref="RM0000013H806DX_12_0016" fin="false">B</right>
<right ref="RM0000013H806DX_12_0007" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0005" proc-id="RM22W0E___00001IS00000">
<testtitle>PERFORM CONFIRMATION DRIVING PATTERN</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle in accordance with the driving pattern described in Confirmation Driving Pattern.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</test1>
<test1>
<ptxt>Input the DTC: P2A00 or P2A03.</ptxt>
</test1>
<test1>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NORMAL (DTC is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABNORMAL (DTC P2A00 or P2A03 is output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<right ref="RM0000013H806DX_12_0014" fin="true">A</right>
<right ref="RM0000013H806DX_12_0007" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0016" proc-id="RM22W0E___00001EX00000">
<testtitle>INSPECT AIR FUEL RATIO SENSOR (HEATER RESISTANCE)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the air fuel ratio sensor (See page <xref label="Seep01" href="RM000000Q67041X"/>).</ptxt>
</test1>
</content6><res>
<down ref="RM0000013H806DX_12_0025" fin="false">OK</down>
<right ref="RM0000013H806DX_12_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0025" proc-id="RM22W0E___00001IX00000">
<testtitle>CHECK AIR INDUCTION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the air induction system (See page <xref label="Seep01" href="RM00000311201OX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013H806DX_12_0026" fin="false">OK</down>
<right ref="RM0000013H806DX_12_0030" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0026" proc-id="RM22W0E___00001IY00000">
<testtitle>CHECK FUEL PRESSURE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the fuel pressure (See page <xref label="Seep01" href="RM000000YL7050X_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013H806DX_12_0027" fin="false">OK</down>
<right ref="RM0000013H806DX_12_0029" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0027" proc-id="RM22W0E___00001IZ00000">
<testtitle>INSPECT FUEL INJECTOR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the fuel injector assembly (See page <xref label="Seep01" href="RM000000WQQ094X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013H806DX_12_0007" fin="false">OK</down>
<right ref="RM0000013H806DX_12_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0007" proc-id="RM22W0E___00001IT00000">
<testtitle>REPLACE AIR FUEL RATIO SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air fuel ratio sensor (See page <xref label="Seep01" href="RM0000031YH01QX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013H806DX_12_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0008" proc-id="RM22W0E___00001IU00000">
<testtitle>PERFORM CONFIRMATION DRIVING PATTERN</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle in accordance with the driving pattern described in Confirmation Driving Pattern.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</test1>
<test1>
<ptxt>Input the DTC: P2A00 or P2A03.</ptxt>
</test1>
<test1>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ABNORMAL (DTC P2A00 or P2A03 is output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NORMAL (DTC is not output)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000013H806DX_12_0020" fin="false">A</down>
<right ref="RM0000013H806DX_12_0010" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0020" proc-id="RM22W0E___00001IV00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202QX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013H806DX_12_0028" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0028" proc-id="RM22W0E___00001J000000">
<testtitle>PERFORM CONFIRMATION DRIVING PATTERN</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle in accordance with the driving pattern described in Confirmation Driving Pattern.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</test1>
<test1>
<ptxt>Input the DTC: P2A00 or P2A03.</ptxt>
</test1>
<test1>
<ptxt>Check that the DTC judgment result is NORMAL.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013H806DX_12_0023" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0029" proc-id="RM22W0E___00001J100000">
<testtitle>INSPECT FUEL PUMP</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the fuel pump (See page <xref label="Seep01" href="RM0000028S502IX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013H806DX_12_0033" fin="true">OK</down>
<right ref="RM0000013H806DX_12_0032" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013H806DX_12_0011">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000032SF04AX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000013H806DX_12_0014">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ108X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000013H806DX_12_0012">
<testtitle>REPLACE AIR FUEL RATIO SENSOR<xref label="Seep01" href="RM0000031YH01QX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000013H806DX_12_0030">
<testtitle>REPAIR OR REPLACE AIR INDUCTION SYSTEM</testtitle>
</testgrp>
<testgrp id="RM0000013H806DX_12_0031">
<testtitle>REPLACE FUEL INJECTOR ASSEMBLY<xref label="Seep01" href="RM000000Q4809UX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000013H806DX_12_0010">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM0000013H806DX_12_0023">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM0000013H806DX_12_0032">
<testtitle>REPLACE FUEL PUMP<xref label="Seep01" href="RM0000039W902PX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000013H806DX_12_0033">
<testtitle>REPAIR OR REPLACE FUEL LINE</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>