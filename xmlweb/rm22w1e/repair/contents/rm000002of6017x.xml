<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0029" variety="S0029">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0029_7C3U9_T00NC" variety="T00NC">
<name>REAR AIRBAG SENSOR</name>
<para id="RM000002OF6017X" category="A" type-id="30014" name-id="RSDVU-02" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM000002OF6017X_02" type-id="11" category="10" proc-id="RM22W0E___0000FKU00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the LH side and RH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000002OF6017X_01" type-id="01" category="01">
<s-1 id="RM000002OF6017X_01_0001" proc-id="RM22W0E___0000FKM00000">
<ptxt>INSTALL REAR AIRBAG SENSOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the ignition switch is turned off.</ptxt>
<figure>
<graphic graphicname="B181652" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Check that the cable is disconnected from the battery negative (-) terminal.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Install the rear airbag sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>9.0</t-value1>
<t-value2>92</t-value2>
<t-value3>80</t-value3>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>If the rear airbag sensor has been dropped, or there are any cracks, dents or other defects in the case, bracket or connector, replace it with a new one.</ptxt>
</item>
<item>
<ptxt>When installing the rear airbag sensor, be careful that the SRS wiring does not interfere with other parts and that it is not pinched between other parts. </ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Check that there is no looseness in the installation parts of the rear airbag sensor.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002OF6017X_01_0030" proc-id="RM22W0E___0000FKS00000">
<ptxt>INSTALL FRONT QUARTER TRIM PANEL ASSEMBLY LH (w/ Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182644" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When installing the front quarter trim panel, operate the reclining adjuster release handle and move the No. 1 rear seat to the position shown in the illustration.</ptxt>
</atten4>
<s2>
<ptxt>w/ Rear No. 2 Seat, except Face to Face Seat Type:</ptxt>
<s3>
<ptxt>w/o Rear Air Conditioning System:</ptxt>
<ptxt>Connect the rear seat lock control lever cable.</ptxt>
</s3>
<s3>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<ptxt>Connect the thermistor connector and rear seat lock control lever cable.</ptxt>
</s3>
<s3>
<ptxt>Attach the 18 clips and 2 claws to install the quarter trim panel.</ptxt>
</s3>
<s3>
<ptxt>Install the clip and bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B186889" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Rear No. 2 Seat or w/ Rear No. 2 Seat, for Face to Face Seat Type:</ptxt>
<s3>
<ptxt>Attach the 18 clips and 2 claws to install the quarter trim panel.</ptxt>
</s3>
<s3>
<ptxt>Install the clip.</ptxt>
<figure>
<graphic graphicname="B190221" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B181691E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>w/ Rear No. 2 Seat, except Face to Face Seat Type:</ptxt>
<ptxt>Install the rear No. 2 seat belt anchor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the vehicle body.</ptxt>
</atten3>
</s2>
<s2>
<figure>
<graphic graphicname="B181689" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the rear No. 1 seat belt anchor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="B181687" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 3 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002OF6017X_01_0013" proc-id="RM22W0E___0000FKP00000">
<ptxt>INSTALL FRONT QUARTER TRIM PANEL ASSEMBLY LH (w/o Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182644" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When installing the front quarter trim panel, operate the reclining adjuster release handle and move the No. 1 rear seat to the position shown in the illustration.</ptxt>
</atten4>
<s2>
<ptxt>w/ Rear No. 2 Seat, except Face to Face Seat Type:</ptxt>
<s3>
<ptxt>w/o Rear Air Conditioning System:</ptxt>
<ptxt>Connect the rear seat lock control lever cable.</ptxt>
</s3>
<s3>
<ptxt>w/ Rear Air Conditioning System:</ptxt>
<ptxt>Connect the thermistor connector and rear seat lock control lever cable.</ptxt>
</s3>
<s3>
<ptxt>Attach the 18 clips and 2 claws to install the quarter trim panel.</ptxt>
</s3>
<s3>
<ptxt>Install the clip and bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="B186889" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Rear No. 2 Seat or w/ Rear No. 2 Seat, for Face to Face Seat Type:</ptxt>
<s3>
<ptxt>Attach the 18 clips and 2 claws to install the quarter trim panel.</ptxt>
</s3>
<s3>
<ptxt>Install the clip.</ptxt>
<figure>
<graphic graphicname="B190221" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Tonneau Cover:</ptxt>
<s3>
<ptxt>Attach the 18 clips and 2 claws to install the quarter trim panel.</ptxt>
</s3>
<s3>
<ptxt>Install the screw and clip.</ptxt>
<figure>
<graphic graphicname="B186437" width="7.106578999in" height="4.7836529in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B181691E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>w/ Rear No. 2 Seat, except Face to Face Seat Type:</ptxt>
<ptxt>Install the rear No. 2 seat belt anchor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not overlap the anchor part of the seat belt and protruding parts of the vehicle body.</ptxt>
</atten3>
</s2>
<s2>
<figure>
<graphic graphicname="B181689" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the rear No. 1 seat belt anchor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="B181687" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 3 claws to install the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002OF6017X_01_0031" proc-id="RM22W0E___000067B00000">
<ptxt>INSTALL REAR SEAT COVER CAP (w/ Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190214" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the rear seat cover cap on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 3 claws to install the rear seat cover cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002OF6017X_01_0027" proc-id="RM22W0E___0000CFP00000">
<ptxt>INSTALL REAR SEAT COVER CAP (w/o Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B190214" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the rear seat cover cap on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 3 claws to install the rear seat cover cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002OF6017X_01_0032" proc-id="RM22W0E___000067D00000">
<ptxt>INSTALL REAR DOOR SCUFF PLATE LH (w/ Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181674" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws and 4 clips to install the scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002OF6017X_01_0014" proc-id="RM22W0E___0000FKQ00000">
<ptxt>INSTALL REAR DOOR SCUFF PLATE LH (w/o Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181674" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws and 4 clips to install the scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002OF6017X_01_0033" proc-id="RM22W0E___000067F00000">
<ptxt>INSTALL REAR STEP COVER (w/ Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181673" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the step cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the step cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002OF6017X_01_0029" proc-id="RM22W0E___0000CFR00000">
<ptxt>INSTALL REAR STEP COVER (w/o Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181673" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure to install the step cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the step cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002OF6017X_01_0034" proc-id="RM22W0E___000067C00000">
<ptxt>INSTALL REAR FLOOR MAT REAR SUPPORT PLATE (w/ Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 6 clips to install the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002OF6017X_01_0026" proc-id="RM22W0E___0000CFS00000">
<ptxt>INSTALL REAR FLOOR MAT REAR SUPPORT PLATE (w/o Sliding Roof)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 6 clips to install the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002OF6017X_01_0035" proc-id="RM22W0E___0000FKT00000">
<ptxt>INSTALL REAR NO. 2 SEAT ASSEMBLY LH (for Split Bench Seat Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear No. 2 seat assembly LH (See page <xref label="Seep01" href="RM00000311A003X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002OF6017X_01_0015" proc-id="RM22W0E___0000FKR00000">
<ptxt>INSTALL REAR NO. 2 SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear No. 2 seat assembly LH (See page <xref label="Seep01" href="RM00000391Q00UX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002OF6017X_01_0010" proc-id="RM22W0E___0000FKN00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002OF6017X_01_0012" proc-id="RM22W0E___0000FKO00000">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0IZX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>