<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001F" variety="S001F">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001F_7C3NZ_T00H2" variety="T00H2">
<name>REAR DIFFERENTIAL CARRIER ASSEMBLY (for LSD)</name>
<para id="RM00000318S003X" category="A" type-id="8000E" name-id="AD03B-01" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM00000318S003X_01" type-id="01" category="01">
<s-1 id="RM00000318S003X_01_0001" proc-id="RM22W0E___00009EF00000">
<ptxt>INSPECT AND ADJUST ADJUSTING SHIM</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Measure the differential case LH and RH dimensions labeled X shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C161909E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 thrust washers and 2 clutch plates to the side gear.</ptxt>
<figure>
<graphic graphicname="C179831E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST to press down the thrust washers and clutch plates with a force of approximately 98 N*m (1000 kgf*cm, 72 ft.*lbf), measure dimension "Y" shown in the illustration.</ptxt>
<sst>
<sstitem>
<s-number>09649-17010</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Referring to the following table, select the proper adjusting shim.</ptxt>
<spec>
<title>Standard Adjusting Shim Thickness</title>
<specitem>
<ptxt>Adjusting shim thickness = X - Y - 19.08 mm (0.751 in.)</ptxt>
</specitem>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Mark</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>A</ptxt>
</entry>
<entry>
<ptxt>0.20 mm (0.00787 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>B</ptxt>
</entry>
<entry>
<ptxt>0.25 mm (0.00984 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>C</ptxt>
</entry>
<entry>
<ptxt>0.30 mm (0.0118 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>D</ptxt>
</entry>
<entry>
<ptxt>0.35 mm (0.0138 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="C161911E02" width="7.106578999in" height="9.803535572in"/>
</figure>
<figure>
<graphic graphicname="C161912E02" width="7.106578999in" height="9.803535572in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0002" proc-id="RM22W0E___00009EG00000">
<ptxt>INSPECT REAR DIFFERENTIAL SIDE GEAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the thrust washers, clutch plates and adjusting shim to the side gear.</ptxt>
<figure>
<graphic graphicname="C124142" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When installing the adjusting shim, make sure the surface with no oil groove faces the differential case side.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the side gear to the differential case.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 pinion gears and thrust washers to the spider.</ptxt>
</s2>
<s2>
<ptxt>Align the spring retainer holes with the straight pins and install the retainer.</ptxt>
<figure>
<graphic graphicname="C161913" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the spider assembly to the differential case.</ptxt>
<atten4>
<ptxt>Install the spider to the differential case tightly and do not move the spring retainer.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using the dial indicator, measure the side gear backlash while holding the side gear and spider.</ptxt>
<figure>
<graphic graphicname="C161914" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard backlash</title>
<specitem>
<ptxt>0.02 to 0.15 mm (0.000787 to 0.00591 in.)</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Measure at all 4 pinion gear locations.</ptxt>
</item>
<item>
<ptxt>Measure the backlash at the differential case LH and RH.</ptxt>
</item>
<item>
<ptxt>Apply hypoid gear oil LSD to each sliding surface and rotating part.</ptxt>
</item>
</list1>
</atten4>
<ptxt>If the backlash is not within the specification, replace the side gear thrust washers with washers of a different thickness.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0003" proc-id="RM22W0E___00009EH00000">
<ptxt>ASSEMBLE DIFFERENTIAL CASE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the spider assembly to the differential case LH.</ptxt>
<atten4>
<ptxt>Install the spider to the differential case LH tightly and do not move the spring retainer.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the compression spring and spring retainer RH.</ptxt>
</s2>
<s2>
<ptxt>Install the side gear assembly RH.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to drop the side gear.</ptxt>
</item>
<item>
<ptxt>Check the pinion and side gear alignment.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Align the matchmarks and assemble the differential case LH and RH.</ptxt>
<figure>
<graphic graphicname="C161843E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 8 bolts and tighten them uniformly, a little at a time.</ptxt>
<torque>
<torqueitem>
<t-value1>47</t-value1>
<t-value2>479</t-value2>
<t-value4>35</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0034" proc-id="RM22W0E___00009F300000">
<ptxt>INSTALL REAR DIFFERENTIAL CASE BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a press, press the RH side bearing (inner race) into the differential case.</ptxt>
<figure>
<graphic graphicname="C161847E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09710-30050</s-number>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using SST and a press, press the LH side bearing (inner race) into the differential case.</ptxt>
<figure>
<graphic graphicname="C161848E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09710-30050</s-number>
</sstitem>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00480</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0004" proc-id="RM22W0E___00009EI00000">
<ptxt>INSTALL DIFFERENTIAL RING GEAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the threads of the bolts and differential case with non-residue solvent.</ptxt>
</s2>
<s2>
<ptxt>Clean the contact surface of the differential case and ring gear.</ptxt>
</s2>
<s2>
<ptxt>Heat the ring gear to about 100°C (212°F) in boiling water.</ptxt>
<figure>
<graphic graphicname="C161849E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Carefully take the ring gear out of the boiling water.</ptxt>
</s2>
<s2>
<ptxt>After the moisture on the ring gear has completely evaporated, quickly align the matchmarks on the ring gear and differential case and set the ring gear onto the differential case. Then temporarily install the 12 bolts so that the bolt holes in the ring gear and differential case are aligned.</ptxt>
</s2>
<s2>
<ptxt>After the ring gear cools down sufficiently, remove the 12 bolts. Then apply thread lock adhesive to the 12 bolts and install them.</ptxt>
<figure>
<graphic graphicname="C161840E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Thread lock</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1360 K, Three Bond 1360 K or equivalent</ptxt>
</specitem>
</spec>
<torque>
<torqueitem>
<t-value1>137</t-value1>
<t-value2>1397</t-value2>
<t-value4>101</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0005" proc-id="RM22W0E___00009EJ00000">
<ptxt>INSPECT DIFFERENTIAL RING GEAR RUNOUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the bearing outer races on their respective bearings. </ptxt>
<atten4>
<ptxt>Do not interchange the left and right outer races.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the assembled plate washers to the side bearing.</ptxt>
</s2>
<s2>
<ptxt>Install the differential case to the differential carrier.</ptxt>
<atten4>
<ptxt>If it is difficult to install the differential case to the carrier, replace the plate washer with a thinner one. However, select a plate washer that allows no clearance between it and the carrier.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Align the matchmarks on the bearing caps and differential carrier.</ptxt>
</s2>
<s2>
<ptxt>Install and uniformly tighten the 4 bolts a little at a time.</ptxt>
</s2>
<s2>
<ptxt>Using a dial indicator, measure the ring gear runout.</ptxt>
<figure>
<graphic graphicname="C161850" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum runout</title>
<specitem>
<ptxt>0.05 mm (0.00197 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the runout is more than the maximum, replace the ring gear.</ptxt>
</s2>
<s2>
<ptxt>Remove the differential case.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0006" proc-id="RM22W0E___00009EK00000">
<ptxt>INSTALL REAR DRIVE PINION FRONT TAPERED ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a press, press in the bearing (outer race).</ptxt>
<sst>
<sstitem>
<s-number>09950-60020</s-number>
<s-subnumber>09951-00890</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161851E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0007" proc-id="RM22W0E___00009EL00000">
<ptxt>INSTALL REAR DRIVE PINION REAR TAPERED ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a press, press in the bearing (outer race).</ptxt>
<sst>
<sstitem>
<s-number>09950-60020</s-number>
<s-subnumber>09951-00890</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07150</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C162726E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0030" proc-id="RM22W0E___00009EZ00000">
<ptxt>INSTALL REAR DRIVE PINION REAR TAPERED ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the washer to the drive pinion.</ptxt>
<atten4>
<ptxt>After installing the washer, check the tooth contact pattern. If necessary, replace the washer with one of a different thickness.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using SST and a press, press the rear bearing (inner race) into the drive pinion.</ptxt>
<sst>
<sstitem>
<s-number>09506-35010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161852E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0008" proc-id="RM22W0E___00009EM00000">
<ptxt>INSPECT DIFFERENTIAL DRIVE PINION PRELOAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the drive pinion, front bearing (inner race) and oil slinger.</ptxt>
<figure>
<graphic graphicname="C028218" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Install the spacer and oil seal after adjusting the gear contact pattern.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using SST, install the companion flange.</ptxt>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03040</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161853E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST to hold the companion flange, install the nut.</ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
<s-subnumber>09330-00030</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161854E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>441</t-value1>
<t-value2>4497</t-value2>
<t-value4>325</t-value4>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Apply hypoid gear oil LSD to the nut.</ptxt>
</item>
<item>
<ptxt>As there is no spacer, tighten a little at a time, being careful not to overtighten it.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Using a torque wrench, measure the preload.</ptxt>
<figure>
<graphic graphicname="C124126" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry align="center">
<ptxt>1.0 to 1.7 N*m (11 to 17 kgf*cm, 10 to 14 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry align="center">
<ptxt>0.9 to 1.4 N*m (9 to 13 kgf*cm, 8 to 12 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>Measure the total preload after turning the bearing clockwise and counterclockwise several times to make the bearing smooth.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0009" proc-id="RM22W0E___00009EN00000">
<ptxt>INSTALL REAR DIFFERENTIAL CASE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the bearing outer races on their respective bearings. </ptxt>
<atten4>
<ptxt>Do not interchange the left and right outer races.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the differential case to the carrier.</ptxt>
<atten4>
<ptxt>Make sure that there is backlash between the ring gear and drive pinion.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0010" proc-id="RM22W0E___00009EO00000">
<ptxt>INSTALL REAR DIFFERENTIAL BEARING ADJUSTING NUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 adjusting nuts to the carrier, making sure the nuts are threaded properly.</ptxt>
<figure>
<graphic graphicname="C161855" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0011" proc-id="RM22W0E___00009EP00000">
<ptxt>INSPECT DIFFERENTIAL RING GEAR BACKLASH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the matchmarks on the bearing caps and carrier. Screw in the 4 bearing cap bolts 2 or 3 turns and press down the bearing caps by hand.</ptxt>
<figure>
<graphic graphicname="C161856E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>If the bearing caps do not fit tightly on the carrier, the adjusting nuts are not engaged properly. Reinstall the adjusting nuts if necessary.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Tighten the 4 bolts.</ptxt>
<figure>
<graphic graphicname="C161857" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>90</t-value1>
<t-value2>918</t-value2>
<t-value4>66</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Loosen the 4 bolts to the point where the adjusting nuts can be turned by SST.</ptxt>
<figure>
<graphic graphicname="C161858E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09504-00011</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using SST, tighten the adjusting nut on the ring gear side until the ring gear has a backlash of about 0.2 mm (0.00787 in.).</ptxt>
</s2>
<s2>
<ptxt>While turning the ring gear, use SST to tighten the adjusting nut on the drive pinion side. After the bearings are settled, loosen the adjusting nut on the drive pinion side.</ptxt>
<figure>
<graphic graphicname="C161859E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09504-00011</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Place a dial indicator on the top of the adjusting nut on the ring gear side.</ptxt>
<figure>
<graphic graphicname="C161860E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Adjust the side bearing to 0 preload by tightening the other adjusting nut until the pointer on the indicator begins to move.</ptxt>
</s2>
<s2>
<ptxt>Using SST, tighten the adjusting nut 1 to 1.5 notches from the 0 preload position.</ptxt>
<figure>
<graphic graphicname="C161859E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09504-00011</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using a dial indicator, adjust the ring gear backlash until it is within the specification.</ptxt>
<figure>
<graphic graphicname="C161861E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09504-00011</s-number>
</sstitem>
</sst>
<spec>
<title>Standard backlash</title>
<specitem>
<ptxt>0.10 to 0.20 mm (0.00394 to 0.00787 in.)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The backlash is adjusted by turning the left and right adjusting nuts by an equal amount. For example, loosen the nut on the left side 1 notch and tighten the nut on the right side 1 notch.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Tighten the 4 bearing cap bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>90</t-value1>
<t-value2>918</t-value2>
<t-value4>66</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>After rotating the ring gear 5 turns or more, recheck the ring gear backlash.</ptxt>
<spec>
<title>Standard backlash</title>
<specitem>
<ptxt>0.10 to 0.20 mm (0.00394 to 0.00787 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0013" proc-id="RM22W0E___00009EQ00000">
<ptxt>INSPECT TOTAL PRELOAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a torque wrench, measure the preload with the teeth of the drive pinion and ring gear in contact.</ptxt>
<figure>
<graphic graphicname="C124126" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Drive Pinion Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry align="center">
<ptxt>Standard drive pinion preload plus</ptxt>
<ptxt>0.4 to 0.6 N*m (4 to 6 kgf*cm, 4 to 5 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry align="center">
<ptxt>Standard drive pinion preload plus</ptxt>
<ptxt>0.4 to 0.6 N*m (4 to 6 kgf*cm, 4 to 5 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0014" proc-id="RM22W0E___00009ER00000">
<ptxt>INSPECT TOOTH CONTACT BETWEEN RING GEAR AND DRIVE PINION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat 3 or 4 teeth at 3 different positions on the ring gear with Prussian blue.</ptxt>
<figure>
<graphic graphicname="C161916" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the companion flange in both directions to inspect the ring gear for proper tooth contact.</ptxt>
<figure>
<graphic graphicname="C116070E03" width="7.106578999in" height="5.787629434in"/>
</figure>
<ptxt>If the teeth are not contacting properly, use the following table to select a proper washer for correction.</ptxt>
<spec>
<title>Standard Washer Thickness</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>1.04 to 1.06 mm (0.0409 to 0.0417 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.315 to 1.335 mm (0.0518 to 0.0526 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.065 to 1.085 mm (0.0420 to 0.0427 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.34 to 1.36 mm (0.0528 to 0.0535 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.09 to 1.11 mm (0.0419 to 0.0437 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.365 to 1.385 mm (0.0537 to 0.0545 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.115 to 1.135 mm (0.0439 to 0.0447 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.39 to 1.41 mm (0.0547 to 0.0555 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.14 to 1.16 mm (0.0449 to 0.0457 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.415 to 1.435 mm (0.0558 to 0.0565 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.165 to 1.185 mm (0.0459 to 0.0467 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.44 to 1.46 mm (0.0567 to 0.0575 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.19 to 1.21 mm (0.0469 to 0.0476 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.465 to 1.485 mm (0.0577 to 0.0585 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.215 to 1.235 mm (0.0478 to 0.0486 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.49 to 1.51 mm (0.0587 to 0.0595 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.24 to 1.26 mm (0.0488 to 0.0496 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.515 to 1.535 mm (0.0597 to 0.0605 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.265 to 1.285 mm (0.0498 to 0.0506 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>1.54 to 1.56 mm (0.0607 to 0.0615 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>1.29 to 1.31 mm (0.0508 to 0.0516 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0036" proc-id="RM22W0E___00009F400000">
<ptxt>REMOVE DRIVE PINION COMPANION FLANGE REAR NUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST to hold the flange, remove the nut.</ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
<s-subnumber>09330-00030</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161836E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0037" proc-id="RM22W0E___00009DZ00000">
<ptxt>REMOVE REAR DRIVE PINION COMPANION FLANGE REAR SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the companion flange.</ptxt>
<figure>
<graphic graphicname="C161837E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03040</s-subnumber>
</sstitem>
</sst>
</s2>
</content1></s-1>
<s-1 id="RM00000318S003X_01_0016">
<ptxt>REMOVE REAR DIFFERENTIAL DRIVE PINION OIL SLINGER</ptxt>
</s-1>
<s-1 id="RM00000318S003X_01_0035" proc-id="RM22W0E___00009E100000">
<ptxt>REMOVE REAR DRIVE PINION FRONT TAPERED ROLLER BEARING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the front bearing (inner race) from the drive pinion.</ptxt>
<sst>
<sstitem>
<s-number>09556-22010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C124133E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If the front bearing is damaged or worn, replace the bearing.   </ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000318S003X_01_0019" proc-id="RM22W0E___00009ES00000">
<ptxt>INSTALL REAR DIFFERENTIAL DRIVE PINION BEARING SPACER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new spacer to the drive pinion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0020" proc-id="RM22W0E___00009ET00000">
<ptxt>INSTALL REAR DRIVE PINION FRONT TAPERED ROLLER BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, tap in the bearing (outer race).</ptxt>
<figure>
<graphic graphicname="C161827E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09316-60011</s-number>
<s-subnumber>09316-00011</s-subnumber>
<s-subnumber>09316-00021</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Install the bearing (inner race).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0022">
<ptxt>INSTALL REAR DIFFERENTIAL DRIVE PINION OIL SLINGER</ptxt>
</s-1>
<s-1 id="RM00000318S003X_01_0023" proc-id="RM22W0E___00009EU00000">
<ptxt>INSTALL REAR DIFFERENTIAL CARRIER OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, tap in a new oil seal.</ptxt>
<sst>
<sstitem>
<s-number>09214-76011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C161864E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard oil seal depth</title>
<specitem>
<ptxt>0.05 to 0.95 mm (0.00197 to 0.0374)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Coat the lip of the oil seal with MP grease.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0024" proc-id="RM22W0E___00009EV00000">
<ptxt>INSTALL REAR DRIVE PINION REAR COMPANION&#13;
FLANGE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, install the companion flange.</ptxt>
<figure>
<graphic graphicname="C161853E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-30012</s-number>
<s-subnumber>09951-03010</s-subnumber>
<s-subnumber>09953-03010</s-subnumber>
<s-subnumber>09954-03010</s-subnumber>
<s-subnumber>09955-03030</s-subnumber>
<s-subnumber>09956-03040</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0025" proc-id="RM22W0E___00009EW00000">
<ptxt>INSPECT DRIVE PINION PRELOAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat the threads of a new nut with hypoid gear oil.</ptxt>
</s2>
<s2>
<ptxt>Using SST to hold the companion flange, install the nut by tightening it until the standard preload is reached.</ptxt>
<sst>
<sstitem>
<s-number>09330-00021</s-number>
<s-subnumber>09330-00030</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="ZK09978E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>441</t-value1>
<t-value2>4497</t-value2>
<t-value4>325</t-value4>
<ptxt>or less</ptxt>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a torque wrench, measure the preload.</ptxt>
<figure>
<graphic graphicname="C124126" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry align="center">
<ptxt>1.0 to 1.7 N*m (11 to 17 kgf*cm, 10 to 14 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry align="center">
<ptxt>0.9 to 1.4 N*m (9 to 13 kgf*cm, 8 to 12 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0026" proc-id="RM22W0E___00009EX00000">
<ptxt>INSPECT TOTAL PRELOAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a torque wrench, measure the preload with the teeth of the drive pinion and ring gear in contact.</ptxt>
<figure>
<graphic graphicname="C124126" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Drive Pinion Preload (at Starting)</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Bearing</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>New</ptxt>
</entry>
<entry align="center">
<ptxt>Standard drive pinion preload plus</ptxt>
<ptxt>0.4 to 0.6 N*m (4 to 6 kgf*cm, 4 to 5 in.*lbf)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Reused</ptxt>
</entry>
<entry align="center">
<ptxt>Standard drive pinion preload plus</ptxt>
<ptxt>0.4 to 0.6 N*m (4 to 6 kgf*cm, 4 to 5 in.*lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0031" proc-id="RM22W0E___00009F000000">
<ptxt>INSPECT DIFFERENTIAL RING GEAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the ring gear backlash.</ptxt>
<figure>
<graphic graphicname="C161872" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard backlash</title>
<specitem>
<ptxt>0.10 to 0.20 mm (0.00394 to 0.00787 in.)</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Measure at 3 or more positions around the circumference of the ring gear.</ptxt>
</atten4>
<ptxt>If the backlash is not as specified, adjust the side bearing preload or repair as necessary.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0032" proc-id="RM22W0E___00009F100000">
<ptxt>INSPECT RUNOUT OF REAR DRIVE PINION COMPANION FLANGE REAR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the runout of the companion flange vertically and laterally.</ptxt>
<figure>
<graphic graphicname="F001237E12" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum Runout</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Runout</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Vertical runout</ptxt>
</entry>
<entry align="center">
<ptxt>0.10 mm (0.00394 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Lateral runout</ptxt>
</entry>
<entry align="center">
<ptxt>0.10 mm (0.00394 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the runout is more than the maximum, replace the companion flange.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0033" proc-id="RM22W0E___00009F200000">
<ptxt>STAKE DRIVE PINION COMPANION FLANGE REAR NUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a chisel and hammer, stake the nut.</ptxt>
<figure>
<graphic graphicname="C161318" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000318S003X_01_0028" proc-id="RM22W0E___00009EY00000">
<ptxt>INSTALL REAR DIFFERENTIAL BEARING ADJUSTING NUT LOCK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 2 new nut locks to the bearing caps.</ptxt>
<figure>
<graphic graphicname="C161917" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 2 bolts, and bend the nut locks.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>