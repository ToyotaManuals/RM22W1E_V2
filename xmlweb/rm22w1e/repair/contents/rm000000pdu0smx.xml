<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>3UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0007_7C3DV_T006Y" variety="T006Y">
<name>SFI SYSTEM</name>
<para id="RM000000PDU0SMX" category="C" type-id="302GP" name-id="ESRE0-03" from="201301" to="201308">
<dtccode>P0011</dtccode>
<dtcname>Camshaft Position "A" - Timing Over-Advanced or System Performance (Bank 1)</dtcname>
<dtccode>P0012</dtccode>
<dtcname>Camshaft Position "A" - Timing Over-Retarded (Bank 1)</dtcname>
<dtccode>P0021</dtccode>
<dtcname>Camshaft Position "A" - Timing Over-Advanced or System Performance (Bank 2)</dtcname>
<dtccode>P0022</dtccode>
<dtcname>Camshaft Position "A" - Timing Over-Retarded (Bank 2)</dtcname>
<subpara id="RM000000PDU0SMX_01" type-id="60" category="03" proc-id="RM22W0E___000025800000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The VVT system includes the ECM, Camshaft Oil Control Valve (OCV) and VVT controller. The ECM sends a target duty-cycle control signal to the OCV. This control signal regulates the oil pressure supplied to the VVT controller. Camshaft timing control is performed according to engine operating conditions such as the intake air volume, throttle valve position and engine coolant temperature. The ECM controls the OCV based on the signals transmitted by several sensors. The VVT controller regulates the intake camshaft angle using oil pressure through the OCV. As a result, the relative positions of the camshaft and crankshaft are optimized, the engine torque and fuel economy improve, and the exhaust emissions decrease under overall driving conditions. The ECM detects the actual intake valve timing using signals from the camshaft and crankshaft position sensors, and performs feedback control. This is how the target intake valve timing is verified by the ECM.</ptxt>
<figure>
<graphic graphicname="A130637E07" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0011</ptxt>
<ptxt>P0021</ptxt>
</entry>
<entry valign="middle">
<ptxt>The intake valve timing is not adjusted in the valve timing advance range (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Valve timing</ptxt>
</item>
<item>
<ptxt>Camshaft Oil Control Valve (OCV) (for intake side of Bank 1, 2)</ptxt>
</item>
<item>
<ptxt>OCV filter</ptxt>
</item>
<item>
<ptxt>Camshaft timing gear</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0012</ptxt>
<ptxt>P0022</ptxt>
</entry>
<entry valign="middle">
<ptxt>The intake valve timing is not adjusted in the valve timing retard range (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Valve timing</ptxt>
</item>
<item>
<ptxt>OCV (for intake side of Bank 1, 2)</ptxt>
</item>
<item>
<ptxt>OCV filter</ptxt>
</item>
<item>
<ptxt>Camshaft timing gear</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000PDU0SMX_02" type-id="64" category="03" proc-id="RM22W0E___000025900000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>The ECM optimizes the intake valve timing using the VVT (Variable Valve Timing) system to control the intake camshaft. The VVT system includes the ECM, Camshaft Oil Control Valve (OCV) and VVT controller. The ECM sends a target duty-cycle control signal to the OCV. This control signal regulates the oil pressure supplied to the VVT controller. The VVT controller can advance or retard the intake camshaft.</ptxt>
</item>
<item>
<ptxt>If the difference between the target and actual intake valve timings is large, and changes in the actual intake valve timing are small, the ECM interprets this as the VVT controller stuck malfunction and stores DTC(s).</ptxt>
</item>
</list1>
<list1 type="nonmark">
<item>
<ptxt>Example:</ptxt>
</item>
<item>
<ptxt>A DTC is stored when the following conditions 1 and 2 are met:</ptxt>
</item>
<list2 type="nonmark">
<item>
<ptxt>1. It takes 5 seconds or more to change the valve timing by 5°CA.</ptxt>
</item>
<item>
<ptxt>2. After the above condition 1 is met, the camshaft timing oil control valve is forcibly activated during 10 seconds.</ptxt>
</item>
</list2>
<item>
<ptxt>DTC P0011 and P0021 (Advanced Cam Timing) are subject to 1 trip detection logic.</ptxt>
</item>
<item>
<ptxt>DTC P0012 and P0022 (Retarded Cam Timing) are subject to 2 trip detection logic.</ptxt>
</item>
<item>
<ptxt>These DTCs indicate that the VVT controller cannot operate properly due to OCV malfunctions or the presence of foreign objects in the OCV.</ptxt>
</item>
<item>
<ptxt>The monitor will run if all of the following conditions are met:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>The engine is warm (the engine coolant temperature is 75°C [167°F] or higher).</ptxt>
</item>
<item>
<ptxt>The vehicle has been driven at more than 64 km/h (40 mph) for 3 minutes.</ptxt>
</item>
<item>
<ptxt>The engine has idled for 3 minutes.</ptxt>
</item>
</list2>
</list1>
</content5>
</subpara>
<subpara id="RM000000PDU0SMX_07" type-id="32" category="03" proc-id="RM22W0E___000025A00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0010 (See page <xref label="Seep01" href="RM000000PDW0Q9X_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PDU0SMX_08" type-id="51" category="05" proc-id="RM22W0E___000025B00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>DTC P0011, P0012, P0021 or P0022 may be stored when foreign objects in the engine oil are caught in some parts of the system. The DTC will remain stored even if the system returns to normal after a short time. Foreign objects are filtered out by the oil filter.</ptxt>
</atten4>
<atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Abnormal Bank</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Advanced Timing Over</ptxt>
<ptxt>(Valve Timing is Out of Specified Range)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Retarded Timing Over</ptxt>
<ptxt>(Valve Timing is Out of Specified Range)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Bank 1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P0011</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P0012</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Bank 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P0021</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P0022</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>If DTC P0011 or P0012 is output, check the left bank VVT system for intake side circuit (for Bank 1).</ptxt>
</item>
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder*.</ptxt>

<ptxt>*: The No. 1 cylinder is the cylinder which is farthest from the transmission.</ptxt>
</item>
<item>
<ptxt>If DTC P0021 or P0022 is output, check the right bank VVT system for intake side circuit (for Bank 2).</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PDU0SMX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PDU0SMX_09_0001" proc-id="RM22W0E___000025C00000">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0011, P0012, P0021 OR P0022)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0011, P0012, P0021 or P0022 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0011, P0012, P0021 or P0022 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0011, P0012, P0021 or P0022 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SMX_09_0002" fin="false">A</down>
<right ref="RM000000PDU0SMX_09_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0002" proc-id="RM22W0E___000025D00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (OPERATE OCV)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Warm up the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the A/C on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the VVT Linear (Bank 1) or Control the VVT Linear (Bank 2).</ptxt>
</test1>
<test1>
<ptxt>Check the engine speed while operating the Camshaft Oil Control Valve (OCV) using the tester.</ptxt>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>0%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal engine speed</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>100%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine idles roughly or stalls</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>Refer to "Data List / Active Test" [VVT OCV Duty #1, VVT Change Angle #1, VVT OCV Duty #2 and VVT Change Angle #2] (See page <xref label="Seep01" href="RM000000SXS098X"/>).</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SMX_09_0003" fin="false">OK</down>
<right ref="RM000000PDU0SMX_09_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0003" proc-id="RM22W0E___000025E00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0011, P0012, P0021 OR P0022)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the tester.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for more than 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs using the tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTC output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SMX_09_0004" fin="true">OK</down>
<right ref="RM000000PDU0SMX_09_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0004">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ109X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0005" proc-id="RM22W0E___000022500000">
<testtitle>CHECK VALVE TIMING (CHECK FOR LOOSE AND JUMPED TOOTH OF TIMING CHAIN)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A166451E02" width="7.106578999in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Remove the cylinder head cover LH and RH.</ptxt>
</test1>
<test1>
<ptxt>Turn the crankshaft pulley, and align its groove with the timing mark "0" of the timing chain cover.</ptxt>
</test1>
<test1>
<ptxt>Check that the timing marks of the camshaft timing gears and camshaft timing exhaust gears are at the positions shown in the illustration.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Timing marks on camshaft timing gears are aligned as shown in the illustration.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SMX_09_0006" fin="false">OK</down>
<right ref="RM000000PDU0SMX_09_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0006" proc-id="RM22W0E___000025F00000">
<testtitle>INSPECT CAMSHAFT OIL CONTROL VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A160397E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Remove the OCV.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6.9 to 7.9 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<figure>
<graphic graphicname="A097066E17" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Check the valve operation.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>No battery voltage applied to terminals 1 and 2 → Battery voltage applied to terminals 1 and 2</ptxt>
</entry>
<entry align="center">
<ptxt>Valve moves quickly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SMX_09_0007" fin="false">OK</down>
<right ref="RM000000PDU0SMX_09_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0007" proc-id="RM22W0E___000025G00000">
<testtitle>INSPECT OIL CONTROL VALVE FILTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the OCV filter (See page <xref label="Seep01" href="RM0000030XO01QX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check that the filter is not clogged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Filter is not clogged.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SMX_09_0016" fin="false">OK</down>
<right ref="RM000000PDU0SMX_09_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0016" proc-id="RM22W0E___000025I00000">
<testtitle>REPLACE INTAKE CAMSHAFT TIMING GEAR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the intake camshaft timing gear (See page <xref label="Seep01" href="RM0000030XO01QX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SMX_09_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0009">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000032SF04BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0010">
<testtitle>ADJUST VALVE TIMING<xref label="Seep01" href="RM0000030XP01QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0011">
<testtitle>REPLACE CAMSHAFT OIL CONTROL VALVE ASSEMBLY<xref label="Seep01" href="RM00000321401RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0012">
<testtitle>CLEAN OIL CONTROL VALVE FILTER</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0013" proc-id="RM22W0E___000025H00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK14HX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the tester.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for more than 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Read the output DTCs using the tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTC is output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>DTC P0011, P0012, P0021 or P0022 is output when foreign objects in the engine oil are caught in some parts of the system. These codes will stay stored even if the system returns to normal after a short time. These foreign objects are then captured by the oil filter, thus eliminating the source of the problem.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDU0SMX_09_0015" fin="true">OK</down>
<right ref="RM000000PDU0SMX_09_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0014">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0SMX_09_0015">
<testtitle>SYSTEM OK</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>