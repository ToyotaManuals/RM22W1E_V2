<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3XU_T00QX" variety="T00QX">
<name>BACK DOOR (for Double Swing Out Type)</name>
<para id="RM000004XHV001X" category="A" type-id="80002" name-id="DH2PQ-01" from="201301">
<name>DISASSEMBLY</name>
<subpara id="RM000004XHV001X_01" type-id="01" category="01">
<s-1 id="RM000004XHV001X_01_0001" proc-id="RM22W0E___0000EBG00000">
<ptxt>REMOVE ASSIST GRIP LH (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the 2 covers.</ptxt>
<figure>
<graphic graphicname="B291735E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 2 bolts and assist grip.</ptxt>
<figure>
<graphic graphicname="B291736" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0002" proc-id="RM22W0E___0000EBH00000">
<ptxt>REMOVE DOOR INSIDE HANDLE BEZEL LH (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw and open the cover.</ptxt>
<figure>
<graphic graphicname="B295796" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the screw.</ptxt>
<figure>
<graphic graphicname="B291737" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws.</ptxt>
<figure>
<graphic graphicname="B291738E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the guide and remove the door inside handle bezel in the direction indicated by the arrow in the illustration.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0003" proc-id="RM22W0E___0000EBI00000">
<ptxt>REMOVE BACK DOOR TRIM PANEL ASSEMBLY LH (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 10 clips and remove the back door trim panel.</ptxt>
<figure>
<graphic graphicname="B291739" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0004" proc-id="RM22W0E___0000EBJ00000">
<ptxt>REMOVE REAR ASSIST GRIP REINFORCEMENT LH (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and rear assist grip reinforcement.</ptxt>
<figure>
<graphic graphicname="B291740" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0005" proc-id="RM22W0E___0000EBK00000">
<ptxt>REMOVE BACK DOOR INSIDE HANDLE ASSEMBLY LH (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
<figure>
<graphic graphicname="B291741" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the guide and remove the back door inside handle in the direction indicated by the arrow in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 links from the back door inside handle.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0006" proc-id="RM22W0E___0000EBL00000">
<ptxt>REMOVE BACK DOOR SERVICE HOLE COVER LH (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 connectors and detach the 3 clamps.</ptxt>
<figure>
<graphic graphicname="B291742" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Pull out the 2 links and wire harness from the back door service hole cover, and then remove the back door service hole cover.</ptxt>
<figure>
<graphic graphicname="B292850" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Remove any remaining butyl tape from the back door panel.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0007" proc-id="RM22W0E___0000IIM00000">
<ptxt>REMOVE BACK DOOR LOCK CONTROL ASSEMBLY (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Rotate the snap in the direction indicated by the arrow in the illustration to detach the snap from the link.</ptxt>
<figure>
<graphic graphicname="B291743E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Snap</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Disconnect the link from the back door lock control.</ptxt>
</s2>
<s2>
<ptxt>Using a clip remover, detach the clip and remove the back door lock control.</ptxt>
<figure>
<graphic graphicname="B291744" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the clip from the back door panel.</ptxt>
<figure>
<graphic graphicname="B296426" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0008" proc-id="RM22W0E___0000IIN00000">
<ptxt>REMOVE BACK DOOR REMOTE CONTROL ASSEMBLY (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the guide and disconnect the cable.</ptxt>
<figure>
<graphic graphicname="B291745" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the clamp.</ptxt>
<figure>
<graphic graphicname="B291746" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a T30 "TORX" socket wrench, remove the bolt and back door remote control.</ptxt>
</s2>
<s2>
<ptxt>Detach the guide and disconnect the cable from the back door remote control.</ptxt>
<figure>
<graphic graphicname="B291747" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0009" proc-id="RM22W0E___0000IIO00000">
<ptxt>REMOVE BACK DOOR LOCK ASSEMBLY (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 connectors.</ptxt>
<figure>
<graphic graphicname="B291748" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a T30 "TORX" socket wrench, remove the 4 bolts and back door lock.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0010" proc-id="RM22W0E___0000IIP00000">
<ptxt>REMOVE BACK DOOR OUTSIDE HANDLE (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and back door outside handle.</ptxt>
<figure>
<graphic graphicname="B291749" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0040" proc-id="RM22W0E___0000IJ800000">
<ptxt>REMOVE BACK DOOR OUTSIDE GARNISH SUB-ASSEMBLY (for LH Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp and disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B291750E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the grommet, and then pull out the wire harness from the back door panel.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Grommet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 10 clips and remove the back door outside garnish.</ptxt>
<figure>
<graphic graphicname="B291751" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XHV001X_01_0018" proc-id="RM22W0E___0000IIS00000">
<ptxt>REMOVE REAR DOOR CHILD LOCK PROTECTION COVER (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear door child lock protection cover.</ptxt>
<figure>
<graphic graphicname="B291752" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0019" proc-id="RM22W0E___0000IIT00000">
<ptxt>REMOVE BACK DOOR NO. 3 WEATHERSTRIP (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 16 clips and remove the back door No. 3 weatherstrip.</ptxt>
<figure>
<graphic graphicname="B291753" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0020" proc-id="RM22W0E___0000IIU00000">
<ptxt>REMOVE BACK DOOR NO. 2 STOPPER CUSHION (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the back door No. 2 stopper cushion.</ptxt>
<figure>
<graphic graphicname="B291754" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0021" proc-id="RM22W0E___0000IIV00000">
<ptxt>REMOVE DOOR DUST PROOF SEAL LH (for LH Side)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B291755" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for both door dust proof seals.</ptxt>
</atten4>
<s2>
<ptxt>Remove the door dust proof seal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0022" proc-id="RM22W0E___0000IIW00000">
<ptxt>REMOVE BACK DOOR CHECK ASSEMBLY LH (for LH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp of the wire harness.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt, 2 nuts and back door check.</ptxt>
<figure>
<graphic graphicname="B291756" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0023" proc-id="RM22W0E___0000I7400000">
<ptxt>REMOVE ASSIST GRIP RH (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the 2 covers.</ptxt>
<figure>
<graphic graphicname="B291761E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 2 bolts and assist grip.</ptxt>
<figure>
<graphic graphicname="B291762" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0024" proc-id="RM22W0E___0000I7500000">
<ptxt>REMOVE BACK DOOR TRIM COVER RH (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 5 clips and 2 claws.</ptxt>
<figure>
<graphic graphicname="B291760" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the clamp and 2 claws and remove the back door trim cover.</ptxt>
<figure>
<graphic graphicname="B291796" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0041" proc-id="RM22W0E___0000IJ900000">
<ptxt>REMOVE CENTER STOP LIGHT ASSEMBLY (for RH Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the center stop light assembly from the center stop light cover.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004XHV001X_01_0026" proc-id="RM22W0E___0000I7600000">
<ptxt>REMOVE BACK DOOR TRIM PANEL ASSEMBLY RH (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 13 clips and remove the back door trim panel.</ptxt>
<figure>
<graphic graphicname="B291763" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0014" proc-id="RM22W0E___0000I7700000">
<ptxt>REMOVE REAR WIPER ARM AND BLADE ASSEMBLY (w/ Rear Wiper)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Open the wiper arm head cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and remove the rear wiper arm and blade assembly.</ptxt>
<figure>
<graphic graphicname="B283521" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XHV001X_01_0015" proc-id="RM22W0E___0000IIQ00000">
<ptxt>REMOVE REAR WIPER MOTOR ASSEMBLY (w/ Rear Wiper)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B283522" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and rear wiper motor assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004XHV001X_01_0016" proc-id="RM22W0E___0000IIR00000">
<ptxt>REMOVE REAR WIPER MOTOR GROMMET (w/ Rear Wiper)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear wiper motor grommet.</ptxt>
<figure>
<graphic graphicname="B283523" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XHV001X_01_0039" proc-id="RM22W0E___0000IJ700000">
<ptxt>REMOVE REAR WASHER NOZZLE (w/ Rear Wiper)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the washer hose.</ptxt>
<figure>
<graphic graphicname="B283561" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the rear washer nozzle.</ptxt>
<figure>
<graphic graphicname="B283529" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000004XHV001X_01_0027" proc-id="RM22W0E___0000IIX00000">
<ptxt>REMOVE REAR ASSIST GRIP REINFORCEMENT RH (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and rear assist grip reinforcement.</ptxt>
<figure>
<graphic graphicname="B291764" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0028" proc-id="RM22W0E___0000IIY00000">
<ptxt>REMOVE BACK DOOR SERVICE HOLE COVER RH (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the back door service hole cover.</ptxt>
<figure>
<graphic graphicname="B291765" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Remove any remaining butyl tape from the back door panel.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0029" proc-id="RM22W0E___0000IIZ00000">
<ptxt>REMOVE BACK DOOR INSIDE HANDLE ASSEMBLY RH (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T30 "TORX" socket wrench, remove the bolt and back door inside handle.</ptxt>
<figure>
<graphic graphicname="B291766" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0030" proc-id="RM22W0E___0000IJ000000">
<ptxt>REMOVE BACK DOOR HANDLE GROMMET (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the back door handle grommet.</ptxt>
<figure>
<graphic graphicname="B291767" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0031" proc-id="RM22W0E___0000IJ100000">
<ptxt>REMOVE LOWER BACK DOOR LOCK ASSEMBLY (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp and disconnect the cable.</ptxt>
<figure>
<graphic graphicname="B291768" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 bolts, 2 screws and lower back door lock.</ptxt>
<figure>
<graphic graphicname="B291769" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0032" proc-id="RM22W0E___0000IJ200000">
<ptxt>REMOVE UPPER BACK DOOR LOCK ASSEMBLY (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291770" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Pull out the cable from the back door panel, and then remove the upper back door lock.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0033" proc-id="RM22W0E___0000I7800000">
<ptxt>REMOVE BACK DOOR CENTER WEATHERSTRIP (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 5 clips and remove the back door center weatherstrip.</ptxt>
<figure>
<graphic graphicname="B291773" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0034" proc-id="RM22W0E___0000IJ300000">
<ptxt>REMOVE BACK DOOR NO. 2 STOPPER SUB-ASSEMBLY (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and back door No. 2 stopper.</ptxt>
<figure>
<graphic graphicname="B291774" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0035" proc-id="RM22W0E___0000IJ400000">
<ptxt>REMOVE DOOR DUST PROOF SEAL RH (for RH Side)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B291776" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for both door dust proof seals.</ptxt>
</atten4>
<s2>
<ptxt>Remove the door dust proof seal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0038" proc-id="RM22W0E___0000IJ600000">
<ptxt>REMOVE BACK DOOR LOCK STRIKER PLATE ASSEMBLY (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T40 "TORX" socket wrench, remove the 2 bolts and back door lock striker plate.</ptxt>
<figure>
<graphic graphicname="B291777" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004XHV001X_01_0036" proc-id="RM22W0E___0000IJ500000">
<ptxt>REMOVE BACK DOOR CHECK ASSEMBLY RH (for RH Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp of the wire harness.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt, 2 nuts and back door check.</ptxt>
<figure>
<graphic graphicname="B291775" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>