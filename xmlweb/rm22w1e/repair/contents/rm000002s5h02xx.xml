<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000C" variety="S000C">
<name>3UR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000C_7C3FJ_T008M" variety="T008M">
<name>CYLINDER BLOCK</name>
<para id="RM000002S5H02XX" category="G" type-id="3001K" name-id="EMDNQ-01" from="201301">
<name>INSPECTION</name>
<subpara id="RM000002S5H02XX_01" type-id="01" category="01">
<s-1 id="RM000002S5H02XX_01_0027" proc-id="RM22W0E___000051Y00000">
<ptxt>INSPECT NO. 1 OIL NOZZLE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A142819" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Push the check valve with a pin to check if it is stuck.</ptxt>
<ptxt>If stuck, replace the No. 1 oil nozzle sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Push the check valve with a pin to check if it moves smoothly.</ptxt>
<ptxt>If it does not move smoothly, clean or replace the No. 1 oil nozzle sub-assembly.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A150150E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>While covering A, apply air into B. Check that air does not leak through C. Perform the check again while covering B and applying air into A.</ptxt>
<ptxt>If air leaks, clean or replace the No. 1 oil nozzle sub-assembly.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A142820E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Push the check valve while covering A, and apply air into B. Check that air passes through C. Perform the check again while covering B, pushing the check valve and applying air into A.</ptxt>
<ptxt>If air does not pass through C, clean or replace the No. 1 oil nozzle sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0028" proc-id="RM22W0E___000051Z00000">
<ptxt>CHECK CYLINDER BLOCK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Visually check the cylinder for vertical scratches.</ptxt>
<ptxt>If necessary, replace the cylinder block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0014" proc-id="RM22W0E___000051M00000">
<ptxt>INSPECT CYLINDER BLOCK FOR WARPAGE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A160697" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using a precision straightedge and feeler gauge, measure the warpage of the surfaces where the cylinder head gaskets contact the cylinder block.</ptxt>
<spec>
<title>Maximum warpage</title>
<specitem>
<ptxt>0.07 mm (0.00276 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the warpage is more than the maximum, replace the cylinder block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0015" proc-id="RM22W0E___000051N00000">
<ptxt>INSPECT CYLINDER BORE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A160699E08" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using a cylinder gauge, measure the cylinder bore diameter at positions A and B in the thrust and axial directions.</ptxt>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>94.000 to 94.012 mm (3.7008 to 3.7013 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum diameter</title>
<specitem>
<ptxt>94.200 mm (3.7087 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Thrust Direction</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Axial Direction</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Center</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry>
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the diameter is more than the maximum, replace the cylinder block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0018" proc-id="RM22W0E___000051Q00000">
<ptxt>INSPECT PISTON SUB-ASSEMBLY WITH PIN</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A160700E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a micrometer, measure the piston diameter at a position that is 15.0 mm (0.591 in.) from the bottom of the piston (refer to the illustration).</ptxt>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>93.950 to 93.960 mm (3.6988 to 3.6992 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum diameter</title>
<specitem>
<ptxt>93.815 mm (3.6935 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is less than the minimum, replace the piston and piston pin as a set.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0019" proc-id="RM22W0E___000051R00000">
<ptxt>INSPECT PISTON OIL CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Measure the cylinder bore diameter in the thrust direction.</ptxt>
</s2>
<s2>
<ptxt>Subtract the piston diameter measurement from the cylinder bore diameter measurement.</ptxt>
<spec>
<title>Standard oil clearance</title>
<specitem>
<ptxt>0.020 to 0.072 mm (0.000787 to 0.00283 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt>0.385 mm (0.0152 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the oil clearance is more than the maximum, replace all the pistons. If necessary, replace the cylinder block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0016" proc-id="RM22W0E___000051O00000">
<ptxt>INSPECT RING GROOVE CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a feeler gauge, measure the clearance between a new piston ring and the wall of the ring groove.</ptxt>
<spec>
<title>Standard Ring Groove Clearance</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No. 1 compression ring</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.020 to 0.070 mm (0.000787 to 0.00276 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No. 2 compression ring</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.020 to 0.060 mm (0.000787 to 0.00236 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Oil ring</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.070 to 0.145 mm (0.00276 to 0.00571 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the clearance is not as specified, replace the piston and piston pin as a set.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0017" proc-id="RM22W0E___000051P00000">
<ptxt>INSPECT PISTON RING END GAP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Insert the piston ring into the cylinder bore.</ptxt>
</s2>
<s2>
<ptxt>Using a piston, push the piston ring a little beyond the bottom of the ring travel, 60 mm (2.36 in.) from the top of the cylinder block.</ptxt>
</s2>
<s2>
<ptxt>Using a feeler gauge, measure the end gap.</ptxt>
<spec>
<title>Standard End Gap</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No. 1 compression ring</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.22 to 0.32 mm (0.00866 to 0.0126 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No. 2 compression ring</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.35 to 0.45 mm (0.0138 to 0.0177 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Oil ring (Side rail)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.10 to 0.40 mm (0.00394 to 0.0157 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Maximum End Gap</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No. 1 compression ring</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.42 mm (0.0165 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No. 2 compression ring</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.55 mm (0.0217 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Oil ring (Side rail)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.45 mm (0.0177 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the end gap is more than the maximum, replace the piston ring. If the end gap is more than the maximum even with a new piston ring, replace the cylinder block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0020" proc-id="RM22W0E___000051S00000">
<ptxt>INSPECT PISTON PIN OIL CLEARANCE</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>There is only 1 type of supply part for piston sub-assembly with pin.</ptxt>
</atten4>
<s2>
<ptxt>Check each mark on the piston, piston pin and connecting rod.</ptxt>
<figure>
<graphic graphicname="A160703E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Front Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Position Pin Hole Inside Diameter Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Connecting Rod Bush Inside Diameter Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a caliper gauge, measure the inside diameter of the piston pin hole.</ptxt>
<figure>
<graphic graphicname="A160704" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Piston Pin Hole Inside Diameter</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle">
<ptxt>21.998 to 22.001 mm (0.86606 to 0.86618 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>22.001 to 22.004 mm (0.86618 to 0.86630 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
<entry valign="middle">
<ptxt>22.004 to 22.007 mm (0.86630 to 0.86642 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Using a micrometer, measure the piston pin diameter.</ptxt>
<figure>
<graphic graphicname="A132060E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Piston Pin Diameter</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle">
<ptxt>21.997 to 22.000 mm (0.86602 to 0.86614 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>22.000 to 22.003 mm (0.86614 to 0.86626 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
<entry valign="middle">
<ptxt>22.003 to 22.006 mm (0.86626 to 0.86638 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Subtract the piston pin diameter measurement from the piston pin hole diameter measurement.</ptxt>
<spec>
<title>Standard oil clearance</title>
<specitem>
<ptxt>-0.002 to 0.004 mm (-0.0000787 to 0.000157 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt>0.015 mm (0.000591 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the oil clearance is more than the maximum, replace the piston and piston pin as a set.</ptxt>
</s2>
<s2>
<ptxt>Using a caliper gauge, measure the inside diameter of the connecting rod bush.</ptxt>
<spec>
<title>Standard Bush Inside Diameter</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle">
<ptxt>22.005 to 22.008 mm (0.86634 to 0.86645 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>22.008 to 22.011 mm (0.86645 to 0.86657 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
<entry valign="middle">
<ptxt>22.011 to 22.014 mm (0.86657 to 0.86669 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Subtract the piston pin diameter measurement from the bush inside diameter measurement.</ptxt>
<spec>
<title>Standard oil clearance</title>
<specitem>
<ptxt>0.005 to 0.011 mm (0.000197 to 0.000433 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt>0.03 mm (0.00118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the oil clearance is more than the maximum, replace the connecting rod, and replace the piston and pin as a set.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0021" proc-id="RM22W0E___000051T00000">
<ptxt>INSPECT CONNECTING ROD SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A142814" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a rod aligner and feeler gauge, check the connecting rod alignment.</ptxt>
<s3>
<ptxt>Check for bend.</ptxt>
<spec>
<title>Maximum bend</title>
<specitem>
<ptxt>0.05 mm (0.00197 in.) per 100 mm (3.94 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the bend is more than the maximum, replace the connecting rod.</ptxt>
</s3>
<s3>
<figure>
<graphic graphicname="A142815" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Check for twist.</ptxt>
<spec>
<title>Maximum twist</title>
<specitem>
<ptxt>0.15 mm (0.00591 in.) per 100 mm (3.94 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the twist is more than the maximum, replace the connecting rod.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0025" proc-id="RM22W0E___000051W00000">
<ptxt>INSPECT CONNECTING ROD BOLT</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A109692E11" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a vernier caliper, measure the tension portion diameter of the bolt.</ptxt>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>8.5 to 8.6 mm (0.335 to 0.339 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum diameter</title>
<specitem>
<ptxt>8.3 mm (0.327 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Measuring Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the diameter is less than the minimum, replace the bolt.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0022" proc-id="RM22W0E___000051U00000">
<ptxt>INSPECT CRANKSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect circle runout.</ptxt>
<s3>
<ptxt>Place the crankshaft on V-blocks.</ptxt>
</s3>
<s3>
<ptxt>Using a dial indicator, measure the circle runout at the center journal.</ptxt>
<spec>
<title>Maximum circle runout</title>
<specitem>
<ptxt>0.06 mm (0.00236 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the circle runout is more than the maximum, replace the crankshaft.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="A142818" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Inspect the main journals.</ptxt>
<s3>
<ptxt>Using a micrometer, measure the diameter of each main journal.</ptxt>
<spec>
<title>Standard journal diameter</title>
<specitem>
<ptxt>66.988 to 67.000 mm (2.6373 to 2.6378 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is not as specified, check the oil clearance. If necessary, replace the crankshaft.</ptxt>
</s3>
<s3>
<ptxt>Check each main journal for taper and out-of-round as shown in the illustration.</ptxt>
<spec>
<title>Maximum taper and out-of-round</title>
<specitem>
<ptxt>0.02 mm (0.000787 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the taper and out-of-round is more than the maximum, replace the crankshaft.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="A142817" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Inspect the crank pins.</ptxt>
<s3>
<ptxt>Using a micrometer, measure the diameter of each crank pin.</ptxt>
<spec>
<title>Standard crank pin diameter</title>
<specitem>
<ptxt>55.982 to 56.000 mm (2.2040 to 2.2047 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is not as specified, check the oil clearance. If necessary, replace the crankshaft.</ptxt>
</s3>
<s3>
<ptxt>Check each crank pin for taper and out-of-round as shown in the illustration.</ptxt>
<spec>
<title>Maximum taper and out-of-round</title>
<specitem>
<ptxt>0.02 mm (0.000787 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the taper and out-of-round is more than the maximum, replace the crankshaft.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0023" proc-id="RM22W0E___000051V00000">
<ptxt>INSPECT CRANKSHAFT OIL CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean each main journal and bearing.</ptxt>
</s2>
<s2>
<ptxt>Check each main journal and bearing for pitting and scratches.</ptxt>
<ptxt>If the journal or bearing is damaged, replace the bearing.</ptxt>
</s2>
<s2>
<ptxt>Place the crankshaft on the cylinder block.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A160707E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Lay a strip of Plastigage across each journal.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Plastigage</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the crankshaft bearing caps (See page <xref label="Seep01" href="RM000002S5K02UX_01_0006"/>).</ptxt>
</s2>
<s2>
<ptxt>Remove the 30 bolts and bearing caps.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A160712E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Measure the Plastigage at its widest point.</ptxt>
<spec>
<title>Standard Oil Clearance</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Number Mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No. 1 and No. 5 journals</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.017 to 0.030 mm (0.000669 to 0.00118 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Other journals</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.024 to 0.037 mm (0.000945 to 0.00146 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Maximum Oil Clearance</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Number Mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No. 1 and No. 5 journals</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.050 mm (0.00197 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Other journals</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.060 mm (0.00236 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Plastigage</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the oil clearance is more than the maximum, replace the bearings. If necessary, replace the crankshaft.</ptxt>
<atten4>
<ptxt>If replacing a bearing, replace it with one that has the same number. If the number of the bearing cannot be determined, select the correct bearing by adding together the numbers imprinted on the cylinder block and crankshaft. Refer to the table below for the appropriate bearing number. There are 6 sizes of standard bearings. For the No. 1 and No. 5 position bearings, use bearings marked 4, 5, 6, 7, 8 or 9. For other bearings, use bearings marked 3, 4, 5, 6, 7 or 8.</ptxt>
</atten4>
<figure>
<graphic graphicname="A165692E06" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Number Mark</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Crankshaft Number Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Number Mark</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 and No. 5 Journal Bearings</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2, No. 3 and No. 4 Journal Bearings</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 3</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 5</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Example:</ptxt>
<ptxt>Cylinder block "07" + Crankshaft "06" = Total number 13 (Use upper bearing "6" and lower bearing "7")</ptxt>
<atten4>
<ptxt>A = Cylinder block number mark</ptxt>
<ptxt>B = Crankshaft number mark</ptxt>
</atten4>
<spec>
<title>Standard Bearing Center Wall Thickness</title>
<table pgwide="1">
<title>No. 1 and No. 5 Journals</title>
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry morerows="1" valign="middle">
<ptxt>(A) + (B)</ptxt>
</entry>
<entry namest="COL2" nameend="COL3" valign="middle">
<ptxt>Upper Bearing</ptxt>
</entry>
<entry namest="COL4" nameend="COL5" valign="middle">
<ptxt>Lower Bearing</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Number Mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Number Mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>00 to 02</ptxt>
</entry>
<entry valign="middle">
<ptxt>4</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.501 to 2.504 (0.0985 to 0.0986 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.488 to 2.491 (0.0980 to 0.0981 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>03 to 05</ptxt>
</entry>
<entry valign="middle">
<ptxt>5</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.504 to 2.507 (0.0986 to 0.0987 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.488 to 2.491 (0.0980 to 0.0981 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>06 to 08</ptxt>
</entry>
<entry valign="middle">
<ptxt>5</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.504 to 2.507 (0.0986 to 0.0987 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>6</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.491 to 2.494 (0.0981 to 0.0982 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>09 to 11</ptxt>
</entry>
<entry valign="middle">
<ptxt>6</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.507 to 2.510 (0.0987 to 0.0988 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>6</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.491 to 2.494 (0.0981 to 0.0982 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>12 to 14</ptxt>
</entry>
<entry valign="middle">
<ptxt>6</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.507 to 2.510 (0.0987 to 0.0988 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.494 to 2.497 (0.0982 to 0.0983 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>15 to 17</ptxt>
</entry>
<entry valign="middle">
<ptxt>7</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.510 to 2.513 (0.0988 to 0.0989 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.494 to 2.497 (0.0982 to 0.0983 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>18 to 20</ptxt>
</entry>
<entry valign="middle">
<ptxt>7</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.510 to 2.513 (0.0988 to 0.0989 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>8</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.497 to 2.500 (0.0983 to 0.0984 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>21 to 23</ptxt>
</entry>
<entry valign="middle">
<ptxt>8</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.513 to 2.516 (0.0989 to 0.0991 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>8</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.497 to 2.500 (0.0983 to 0.0984 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>24 to 26</ptxt>
</entry>
<entry valign="middle">
<ptxt>8</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.513 to 2.516 (0.0989 to 0.0991 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>9</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.500 to 2.503 (0.0984 to 0.0985 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>27 to 28</ptxt>
</entry>
<entry valign="middle">
<ptxt>9</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.516 to 2.519 (0.0991 to 0.0992 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>9</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.500 to 2.503 (0.0984 to 0.0985 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Other Journals</title>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry morerows="1" valign="middle">
<ptxt>(A) + (B)</ptxt>
</entry>
<entry namest="COL2" nameend="COL3" valign="middle">
<ptxt>Upper Bearing</ptxt>
</entry>
<entry namest="COL4" nameend="COL5" valign="middle">
<ptxt>Lower Bearing</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Number Mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Number Mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>00 to 02</ptxt>
</entry>
<entry valign="middle">
<ptxt>3</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.482 to 2.485 (0.0977 to 0.0978 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.501 to 2.504 (0.0985 to 0.0986 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>03 to 05</ptxt>
</entry>
<entry valign="middle">
<ptxt>4</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.485 to 2.488 (0.0978 to 0.0980 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>4</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.501 to 2.504 (0.0985 to 0.0986 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>06 to 08</ptxt>
</entry>
<entry valign="middle">
<ptxt>4</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.485 to 2.488 (0.0978 to 0.0980 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.504 to 2.507 (0.0986 to 0.0987 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>09 to 11</ptxt>
</entry>
<entry valign="middle">
<ptxt>5</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.488 to 2.491 (0.0980 to 0.0981 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.504 to 2.507 (0.0986 to 0.0987 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>12 to 14</ptxt>
</entry>
<entry valign="middle">
<ptxt>5</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.488 to 2.491 (0.0980 to 0.0981 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>6</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.507 to 2.510 (0.0987 to 0.0988 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>15 to 17</ptxt>
</entry>
<entry valign="middle">
<ptxt>6</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.491 to 2.494 (0.0981 to 0.0982 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>6</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.507 to 2.510 (0.0987 to 0.0988 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>18 to 20</ptxt>
</entry>
<entry valign="middle">
<ptxt>6</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.491 to 2.494 (0.0981 to 0.0982 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.510 to 2.513 (0.0988 to 0.0989 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>21 to 23</ptxt>
</entry>
<entry valign="middle">
<ptxt>7</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.494 to 2.497 (0.0982 to 0.0983 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.510 to 2.513 (0.0988 to 0.0989 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>24 to 26</ptxt>
</entry>
<entry valign="middle">
<ptxt>7</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.494 to 2.497 (0.0982 to 0.0983 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>8</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.513 to 2.516 (0.0989 to 0.0991 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>27 to 28</ptxt>
</entry>
<entry valign="middle">
<ptxt>8</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.497 to 2.500 (0.0983 to 0.0984 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>8</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.513 to 2.516 (0.0989 to 0.0991 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Completely remove the Plastigage.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S5H02XX_01_0026" proc-id="RM22W0E___000051X00000">
<ptxt>INSPECT CRANKSHAFT BEARING CAP SET BOLT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A142839E06" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the minimum diameter of the elongated thread at the measuring point.</ptxt>
<spec>
<title>Measuring point</title>
<specitem>
<ptxt>54.5 mm (2.15 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Standard Diameter</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Bolt A</ptxt>
</entry>
<entry>
<ptxt>10.5 to 11.0 mm (0.413 to 0.433 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Bolt B</ptxt>
</entry>
<entry>
<ptxt>9.5 to 10.0 mm (0.374 to 0.394 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Minimum Diameter</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Bolt A</ptxt>
</entry>
<entry>
<ptxt>10.4 mm (0.409 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Bolt B</ptxt>
</entry>
<entry>
<ptxt>9.4 mm (0.370 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>Bolt A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>Bolt B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the diameter is less than the minimum, replace the bolt.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>