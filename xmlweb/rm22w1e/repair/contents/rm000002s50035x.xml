<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000B" variety="S000B">
<name>1UR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000B_7C3F3_T0086" variety="T0086">
<name>CYLINDER HEAD GASKET</name>
<para id="RM000002S50035X" category="A" type-id="30014" name-id="EMBHM-02" from="201301" to="201308">
<name>INSTALLATION</name>
<subpara id="RM000002S50035X_02" type-id="01" category="01">
<s-1 id="RM000002S50035X_02_0034" proc-id="RM22W0E___00004G000000">
<ptxt>INSTALL NO. 2 CYLINDER HEAD GASKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place a new cylinder head gasket on the cylinder block surface with the front face of the Lot No. stamp upward.</ptxt>
<figure>
<graphic graphicname="A228408E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lot No.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Engine Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the gasket is installed facing the proper direction.</ptxt>
</item>
<item>
<ptxt>Make sure that no oil is on the front end (indicated by the arrows) of the No. 2 cylinder head gasket.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S50035X_02_0009" proc-id="RM22W0E___00004FW00000">
<ptxt>INSTALL CYLINDER HEAD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the cylinder head on the cylinder block.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that no oil is on the mounting surface of the cylinder head.</ptxt>
</item>
<item>
<ptxt>Place the cylinder head gently in order not to damage the gasket with the bottom part of the head.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the cylinder head bolts.</ptxt>
<atten4>
<ptxt>The cylinder head bolts are tightened in 3 progressive steps.</ptxt>
</atten4>
<s3>
<ptxt>Apply a light coat of engine oil to the threads and under the heads of the cylinder head bolts.</ptxt>
</s3>
<s3>
<ptxt>Step 1:</ptxt>
<ptxt>Using a 10 mm bi-hexagon wrench, install and uniformly tighten the 10 cylinder head bolts with the plate washers in several steps in the sequence shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>36</t-value1>
<t-value2>367</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A160737E08" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Mark the front side of each cylinder head bolt head with paint.</ptxt>
</s3>
<s3>
<ptxt>Step 2:</ptxt>
<ptxt>Tighten the cylinder head bolts 90°.</ptxt>
</s3>
<s3>
<ptxt>Step 3:</ptxt>
<ptxt>Tighten the cylinder head bolts an additional 90°.</ptxt>
</s3>
<s3>
<ptxt>Check that the paint marks are now at a 180° angle to the front.</ptxt>
</s3>
<s3>
<ptxt>Install and uniformly tighten the 2 bolts in the sequence shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A160736E08" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Thoroughly wipe clean any seal packing.</ptxt>
</atten3>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S50035X_02_0048" proc-id="RM22W0E___00004G300000">
<ptxt>INSTALL CYLINDER HEAD GASKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place a new cylinder head gasket on the cylinder block surface with the front face of the Lot No. stamp upward.</ptxt>
<figure>
<graphic graphicname="A228409E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lot No.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Engine Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the gasket is installed facing the proper direction.</ptxt>
</item>
<item>
<ptxt>Make sure that no oil is on the front end (indicated by the arrows) of the cylinder head gasket.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S50035X_02_0049" proc-id="RM22W0E___00004G400000">
<ptxt>INSTALL CYLINDER HEAD SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the cylinder head on the cylinder block.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Place the cylinder head gently in order not to damage the gasket with the bottom part of the head.</ptxt>
</item>
<item>
<ptxt>Make sure that no oil is on the mounting surface of the cylinder head.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the cylinder head bolts.</ptxt>
<atten4>
<ptxt>The cylinder head bolts are tightened in 3 progressive steps.</ptxt>
</atten4>
<s3>
<ptxt>Apply a light coat of engine oil to the threads and under the heads of the cylinder head bolts.</ptxt>
</s3>
<s3>
<ptxt>Step 1:</ptxt>
<ptxt>Using a 10 mm bi-hexagon wrench, install and uniformly tighten the 10 cylinder head bolts with the plate washers in several steps in the sequence shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>36</t-value1>
<t-value2>367</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A160739E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Mark the front side of each cylinder head bolt head with paint.</ptxt>
</s3>
<s3>
<ptxt>Step 2:</ptxt>
<ptxt>Tighten the cylinder head bolts 90°.</ptxt>
</s3>
<s3>
<ptxt>Step 3:</ptxt>
<ptxt>Tighten the cylinder head bolts an additional 90°.</ptxt>
</s3>
<s3>
<ptxt>Check that the paint marks are now at a 180° angle to the front.</ptxt>
</s3>
<s3>
<ptxt>Install and uniformly tighten the 2 bolts in the sequence shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A160738E07" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Thoroughly wipe clean any seal packing.</ptxt>
</atten3>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S50035X_02_0010" proc-id="RM22W0E___00004FX00000">
<ptxt>INSTALL VALVE STEM CAP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to the valve stem caps.</ptxt>
</s2>
<s2>
<ptxt>Install the 32 valve stem caps to the cylinder head.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S50035X_02_0011" proc-id="RM22W0E___00004FY00000">
<ptxt>INSTALL VALVE LASH ADJUSTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the valve lash adjusters (See page <xref label="Seep01" href="RM0000030XL03JX_01_0002"/>).</ptxt>
</s2>
<s2>
<ptxt>Install the 32 valve lash adjusters to the cylinder head.</ptxt>
<atten3>
<ptxt>Install each valve lash adjuster to the same place it was removed from.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S50035X_02_0012" proc-id="RM22W0E___00004FZ00000">
<ptxt>INSTALL NO. 1 VALVE ROCKER ARM SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply engine oil to the valve lash adjuster tips and valve stem cap ends.</ptxt>
</s2>
<s2>
<ptxt>Install the 32 No. 1 valve rocker arms as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A163454E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Valve Rocker Arm</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve Stem Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve Lash Adjuster</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000002S50035X_02_0046" proc-id="RM22W0E___00004G100000">
<ptxt>INSTALL CAMSHAFT SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000002S5303PX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000002S50035X_02_0047" proc-id="RM22W0E___00004G200000">
<ptxt>INSTALL EXHAUST MANIFOLD ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000031GV01WX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>