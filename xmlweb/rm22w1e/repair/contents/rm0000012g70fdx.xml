<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12060_S002C" variety="S002C">
<name>SEAT BELT</name>
<ttl id="12060_S002C_7C3V8_T00OB" variety="T00OB">
<name>SEAT BELT WARNING SYSTEM</name>
<para id="RM0000012G70FDX" category="U" type-id="303FJ" name-id="ME002T-238" from="201301">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM0000012G70FDX_z0" proc-id="RM22W0E___0000GGG00000">
<content5 releasenbr="1">
<step1>
<ptxt>READ DATA LIST</ptxt>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / (desired system) / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Combination Meter</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P-Seatbelt Buckle SW*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side seat belt buckle signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front passenger side seat is occupied, front passenger side seat belt is fastened</ptxt>
<ptxt>OFF: Front passenger side seat is occupied, front passenger side seat belt is unfastened</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>D - Seatbelt Warning Buzzer*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side seat belt warning signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side seat belt warning buzzer is sound</ptxt>
<ptxt>OFF: Driver side seat belt warning buzzer is not sound</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The current customize setting is displayed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P - Seatbelt Warning Buzzer*1, *2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side seat belt warning signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front passenger side seat belt warning buzzer is sound</ptxt>
<ptxt>OFF: Front passenger side seat belt warning buzzer is not sound</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>The current customize setting is displayed.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Airbag System</ptxt>
</item>
<item>
<ptxt>*2: w/ Seat Belt Warning Buzzer</ptxt>
</item>
</list1>
<table pgwide="1">
<title>SRS Airbag (w/ Airbag System)</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Driver Buckle SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side seat belt / Set, Unset or NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>Set: Driver side seat belt is fastened</ptxt>
<ptxt>Unset: Driver side seat belt is unfastened</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>NG: Data is not determined</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Passenger Buckle SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side seat belt / Set, Unset or NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front passenger side seat is occupied, front passenger side seat belt is fastened</ptxt>
<ptxt>OFF: Front passenger side seat is occupied, front passenger side seat belt is unfastened</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Main Body (w/o Airbag System)</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>D Seat Buckle SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side seat belt / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Driver side seat belt is fastened</ptxt>
<ptxt>OFF: Driver side seat belt is unfastened</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>PERFORM ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.  </ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / Combination Meter / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to display on the intelligent tester, perform the Active Test.</ptxt>
<table pgwide="1">
<title>Combination Meter</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Driver Side Seat Belt</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Driver side seat belt warning light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Confirm that the vehicle is stopped and the engine is idling.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Front Passenger Side Seat Belt*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side seat belt warning light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Confirm that the vehicle is stopped and the engine is idling.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Airbag System</ptxt>
</item>
</list1>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>