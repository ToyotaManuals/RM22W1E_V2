<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM00000452E013X" category="C" type-id="803L4" name-id="BC86P-17" from="201301" to="201308">
<dtccode>C1409</dtccode>
<dtcname>Front Speed Sensor RH Performance</dtcname>
<dtccode>C1410</dtccode>
<dtcname>Front Speed Sensor LH Performance</dtcname>
<subpara id="RM00000452E013X_01" type-id="60" category="03" proc-id="RM22W0E___0000AP600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1401 and C1402 (See page <xref label="Seep01" href="RM000000XI90T9X_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.24in"/>
<colspec colname="COL2" colwidth="3.63in"/>
<colspec colname="COL3" colwidth="2.21in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1409</ptxt>
<ptxt>C1410</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>When the vehicle is driven in reverse at a speed of 3 km/h (2 mph) or more and 3 of the wheel sensors detect the reverse signal, the other sensor detects a high frequency pulse 75 times while the ignition switch is ON.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 10 km/h (6 mph) or more, the vehicle speed sensor output drops by 50% for 5 seconds.</ptxt>
</item>
<item>
<ptxt>The lowest wheel speed is 10 km/h (6 mph) or more and the difference between the highest and lowest wheel speed values is 2 km/h (1 mph) or less for 15 seconds.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 30 km/h (19 mph) or more, one wheel direction is different from the other 3 wheels for 1 second.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 100 km/h (62 mph), a reverse signal is output for 1 second or more.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 30 km/h (19 mph) or more, the rotation direction of the one of the wheels is not detected normally and the rotation direction of the other 3 wheels is not the same for 1 second.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH/LH</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTC C1409 is for the front speed sensor RH.</ptxt>
</item>
<item>
<ptxt>DTC C1410 is for the front speed sensor LH.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000452E013X_03" type-id="51" category="05" proc-id="RM22W0E___0000AP700000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Check the speed sensor signal after replacement (See page <xref label="Seep01" href="RM0000054KD001X"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000452E013X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000452E013X_04_0006" proc-id="RM22W0E___0000AOC00000">
<testtitle>READ VALUE USING GTS (FR/FL WHEEL SPEED)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.16in"/>
<colspec colname="COL2" colwidth="2.22in"/>
<colspec colname="COL3" colwidth="1.74in"/>
<colspec colname="COLSPEC2" colwidth="1.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH reading/ Min.: 0 km/h (0 mph), Max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that there is no difference between the speed value output from the speed sensor displayed on the GTS and the speed value displayed on the speedometer when driving the vehicle.</ptxt>
<atten4>
<ptxt>Factors that affect the indicated vehicle speed include tire size, tire inflation and tire wear. The speed indicated on the speedometer has an allowable margin of error. This can be tested using a speedometer tester (calibrated chassis dynamometer). For details about testing and the margin of error, refer to the reference chart (See page <xref label="Seep01" href="RM000002Z4Q02RX"/>).</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The speed value output from the speed sensor displayed on the GTS is the same as the actual vehicle speed measured using a speedometer tester (calibrated chassis dynamometer).</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM00000452E013X_04_0004" fin="false">OK</down>
<right ref="RM00000452E013X_04_0001" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000452E013X_04_0004" proc-id="RM22W0E___0000AP800000">
<testtitle>READ VALUE USING GTS (FR/FL WHEEL DIRECTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.16in"/>
<colspec colname="COL2" colwidth="2.22in"/>
<colspec colname="COL3" colwidth="1.74in"/>
<colspec colname="COLSPEC2" colwidth="1.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Wheel Direction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wheel direction RH/ Forward or Back</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward: Forward</ptxt>
<ptxt>Back: Back</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Wheel Direction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front wheel direction LH/ Forward or Back</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward: Forward</ptxt>
<ptxt>Back: Back</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check the "FR/FL Wheel Direction" in the Data List both when the vehicle is being driven straight forward at a speed of 30 km/h (19 mph) or more and when the vehicle is being driven in reverse at a speed of 3 km/h (2 mph) or more. </ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The sensor signal matches wheel direction.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000452E013X_04_0005" fin="false">OK</down>
<right ref="RM00000452E013X_04_0001" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000452E013X_04_0005" proc-id="RM22W0E___0000AP900000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Perform a road test.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00NX"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC C1409 or C1410 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000452E013X_04_0001" fin="true">A</down>
<right ref="RM00000452E013X_04_0002" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000452E013X_04_0001">
<testtitle>REPLACE FRONT SPEED SENSOR<xref label="Seep01" href="RM000001B2J01KX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000452E013X_04_0002">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>