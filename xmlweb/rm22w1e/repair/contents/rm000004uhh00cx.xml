<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S0025" variety="S0025">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S0025_7C3TB_T00ME" variety="T00ME">
<name>ENGINE IMMOBILISER SYSTEM (w/o Entry and Start System)</name>
<para id="RM000004UHH00CX" category="J" type-id="805TY" name-id="TD714-01" from="201301" to="201308">
<dtccode/>
<dtcname>Engine does not Start but Initial Combustion Occurs</dtcname>
<subpara id="RM000004UHH00CX_01" type-id="60" category="03" proc-id="RM22W0E___0000EXN00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The key ID code of the transponder chip inside the IG key is compared against the key ID code registered in the transponder key ECU assembly.  While checking the code, the transponder key ECU assembly supplies electrical power to the transponder chip in the key by means of the transponder key amplifier.  If the codes match, the immobiliser is then UNSET (released), and after the engine start permission signal is sent to the ECM, if communication is functioning properly, engine start can be performed.</ptxt>
</content5>
</subpara>
<subpara id="RM000004UHH00CX_02" type-id="32" category="03" proc-id="RM22W0E___0000EXO00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B255097E11" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000004UHH00CX_03" type-id="51" category="05" proc-id="RM22W0E___0000EXP00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the key and transponder key ECU assembly, refer to the Service Bulletin.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000004UHH00CX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004UHH00CX_04_0001" proc-id="RM22W0E___0000EXQ00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000001TCQ02BX"/>).</ptxt>
<atten4>
<ptxt>Before checking for DTCs, perform the "DTC Output Confirmation Operation" procedure.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No DTC output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0021" fin="false">A</down>
<right ref="RM000004UHH00CX_04_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0021" proc-id="RM22W0E___0000EXZ00000">
<testtitle>CONFIRM ENGINE MODEL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the vehicle specifications.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1GR-FE</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1VD-FTV</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0022" fin="false">A</down>
<right ref="RM000004UHH00CX_04_0002" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0022" proc-id="RM22W0E___0000EY000000">
<testtitle>READ VALUE USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COLSPEC0" colwidth="1.78in"/>
<colspec colname="COLSPEC1" colwidth="1.78in"/>
<colspec colname="COLSPEC2" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.74in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Stored as Freeze Frame Data</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Immobiliser Fuel Cut</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status of immobiliser fuel cut/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Yes</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The item in the Data List indicates "OFF".</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0017" fin="true">OK</down>
<right ref="RM000004UHH00CX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0002" proc-id="RM22W0E___0000EXR00000">
<testtitle>CHECK WHETHER ENGINE STARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch to ON, wait for 5 seconds, and then attempt to start the engine.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Engine cannot be started</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine can be started</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0003" fin="false">A</down>
<right ref="RM000004UHH00CX_04_0010" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0003" proc-id="RM22W0E___0000EXS00000">
<testtitle>CHECK ECM (TERMINAL IMI)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B288681E03" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Transponder Key ECU Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform 1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A38-11 (IMI) - E96-13 (EFIO)*1</ptxt>
<ptxt>A52-11 (IMI) - E96-13 (EFIO)*2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 500 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Within 3 seconds after starter operates and initial combustion occurs, or within 3 seconds after ignition switch first turned to ON after battery disconnected and reconnected</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
<atten4>
<ptxt>The waveform shown in the illustration is an example for reference only. Noise, chattering, etc. are not shown.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Normal waveform</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Waveform 1 or 2 (Waveform A) not output, or has abnormal wavelength or shape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0005" fin="false">A</down>
<right ref="RM000004UHH00CX_04_0007" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0005" proc-id="RM22W0E___0000EXU00000">
<testtitle>CHECK ECM (TERMINAL IMO)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B288682E03" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Transponder Key ECU Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform 1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*f</ptxt>
</entry>
<entry valign="middle">
<ptxt>160 ms</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*g</ptxt>
</entry>
<entry valign="middle">
<ptxt>510 ms</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*h</ptxt>
</entry>
<entry valign="middle">
<ptxt>270 ms</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A38-10 (IMO) - E96-12 (EFII)*1</ptxt>
<ptxt>A52-10 (IMO) - E96-12 (EFII)*2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 500 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Within 3 seconds after starter operates and initial combustion occurs, or within 3 seconds after ignition switch first turned to ON after battery disconnected and reconnected</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
<atten4>
<ptxt>The waveform shown in the illustration is an example for reference only. Noise, chattering, etc. are not shown.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Normal waveform</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Waveform 1 or 2 (Waveform B) not output, or has abnormal wavelength or shape (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Waveform 1 or 2 (Waveform B) not output, or has abnormal wavelength or shape (for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0004" fin="false">A</down>
<right ref="RM000004UHH00CX_04_0011" fin="true">B</right>
<right ref="RM000004UHH00CX_04_0012" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0004" proc-id="RM22W0E___0000EXT00000">
<testtitle>CHECK ECM (TERMINAL IMI)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B288683E03" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Transponder Key ECU Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform 1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform C</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*f</ptxt>
</entry>
<entry valign="middle">
<ptxt>160 ms</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*g</ptxt>
</entry>
<entry valign="middle">
<ptxt>510 ms</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*h</ptxt>
</entry>
<entry valign="middle">
<ptxt>270 ms</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A38-11 (IMI) - E96-13 (EFIO)*1</ptxt>
<ptxt>A52-11 (IMI) - E96-13 (EFIO)*2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2 V/DIV., 500 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Within 3 seconds after starter operates and initial combustion occurs, or within 3 seconds after ignition switch first turned to ON after battery disconnected and reconnected</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
<atten4>
<ptxt>The waveform shown in the illustration is an example for reference only. Noise, chattering, etc. are not shown.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Normal waveform</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Waveform 1 or 2 (Waveform C) not output, or has abnormal wavelength or shape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0020" fin="false">A</down>
<right ref="RM000004UHH00CX_04_0008" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0020" proc-id="RM22W0E___0000EXY00000">
<testtitle>REGISTER ECU COMMUNICATION ID</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Register the ECU communication ID (refer to the Service Bulletin).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0006" proc-id="RM22W0E___0000EXV00000">
<testtitle>CHECK WHETHER ENGINE STARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the engine starts with the key.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Engine starts normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0014" fin="true">OK</down>
<right ref="RM000004UHH00CX_04_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0007" proc-id="RM22W0E___0000EXW00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ECM - TRANSPONDER KEY ECU ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LHD:</ptxt>
<test2>
<ptxt>Disconnect the A38 ECM connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E96 transponder key ECU assembly connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A38-11 (IMI) - E96-13 (EFIO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E96-13 (EFIO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for RHD:</ptxt>
<test2>
<ptxt>Disconnect the A52 ECM connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the E96 transponder key ECU assembly connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A52-11 (IMI) - E96-13 (EFIO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>E96-13 (EFIO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0008" fin="false">OK</down>
<right ref="RM000004UHH00CX_04_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0008" proc-id="RM22W0E___0000EXX00000">
<testtitle>REPLACE TRANSPONDER KEY ECU ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the transponder key ECU assembly.</ptxt>
</test1>
<test1>
<ptxt>Check that the engine starts with the key.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Engine starts normally.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for 1VD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004UHH00CX_04_0016" fin="true">A</down>
<right ref="RM000004UHH00CX_04_0017" fin="true">B</right>
<right ref="RM000004UHH00CX_04_0024" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0009">
<testtitle>GO TO DIAGNOSTIC TROUBLE CODE CHART<xref label="Seep01" href="RM000001TCA02PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0010">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0011">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0012">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0014">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0015">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0016">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0017">
<testtitle>GO TO SFI SYSTEM<xref label="Seep01" href="RM000000PD90Y1X"/>
</testtitle>
</testgrp>
<testgrp id="RM000004UHH00CX_04_0024">
<testtitle>GO TO ECD SYSTEM<xref label="Seep01" href="RM0000012WB07FX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>