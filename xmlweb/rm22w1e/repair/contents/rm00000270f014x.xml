<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001F" variety="S001F">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001F_7C3O9_T00HC" variety="T00HC">
<name>REAR AXLE SHAFT</name>
<para id="RM00000270F014X" category="A" type-id="80001" name-id="AD2HC-01" from="201301">
<name>REMOVAL</name>
<subpara id="RM00000270F014X_02" type-id="11" category="10" proc-id="RM22W0E___00009M500000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the LH side and RH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000270F014X_01" type-id="01" category="01">
<s-1 id="RM00000270F014X_01_0021" proc-id="RM22W0E___00009WH00000">
<ptxt>REMOVE STABILIZER CONTROL VALVE PROTECTOR (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp, and disconnect the connector from the protector.</ptxt>
<figure>
<graphic graphicname="C172095" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and protector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000270F014X_01_0020" proc-id="RM22W0E___00009WN00000">
<ptxt>OPEN STABILIZER CONTROL WITH ACCUMULATOR HOUSING SHUTTER VALVE (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, loosen the lower and upper chamber shutter valves of the stabilizer control with accumulator housing 2.0 to 3.5 turns.</ptxt>
<figure>
<graphic graphicname="C175131E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When loosening a shutter valve, make sure that the end protrudes 2 to 3.5 mm (0.0787 to 0.137 in.) from the surface of the block, and do not turn the shutter valve any further.</ptxt>
</item>
<item>
<ptxt>Do not remove the shutter valves.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000270F014X_01_0001">
<ptxt>REMOVE REAR WHEEL LH</ptxt>
</s-1>
<s-1 id="RM00000270F014X_01_0018" proc-id="RM22W0E___00009M300000">
<ptxt>DRAIN BRAKE FLUID
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Wash off brake fluid immediately if it comes in contact with any painted surface.</ptxt>
</atten3>
</content1></s-1>
<s-1 id="RM00000270F014X_01_0019" proc-id="RM22W0E___00009M400000">
<ptxt>DISCONNECT REAR BRAKE FLEXIBLE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the brake tube from the flexible hose with SST while holding the flexible hose with a wrench.</ptxt>
<figure>
<graphic graphicname="C154490E05" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not bend or damage the brake tube.</ptxt>
</item>
<item>
<ptxt>Do not allow any foreign matter such as dirt and dust to enter the brake tube from the connecting point.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000270F014X_01_0015" proc-id="RM22W0E___00009L400000">
<ptxt>DISCONNECT REAR DISC BRAKE CYLINDER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and disconnect the rear disc brake cylinder.</ptxt>
<figure>
<graphic graphicname="C158344" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not twist or bend the flexible hose.</ptxt>
</item>
<item>
<ptxt>Do not disconnect the flexible hose from the disc brake cylinder.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000270F014X_01_0003" proc-id="RM22W0E___00009L500000">
<ptxt>REMOVE REAR DISC LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put matchmarks on the rear disc and axle hub if planning to reuse the disc.</ptxt>
<figure>
<graphic graphicname="C172168E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the shoe adjuster as shown in the illustration until the disc turns freely, and then remove the disc.</ptxt>
<figure>
<graphic graphicname="C155453" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000270F014X_01_0004" proc-id="RM22W0E___00009L600000">
<ptxt>REMOVE PARKING BRAKE SHOE RETURN TENSION SPRING LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the return spring.</ptxt>
<sst>
<sstitem>
<s-number>09703-30011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172171E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000270F014X_01_0005" proc-id="RM22W0E___00009L700000">
<ptxt>REMOVE NO. 1 PARKING BRAKE SHOE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the shoe hold down spring cup, compression spring and shoe hold down spring pin.</ptxt>
<sst>
<sstitem>
<s-number>09718-00011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172174E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the tension spring from the No. 1 parking brake shoe.</ptxt>
<figure>
<graphic graphicname="C172180" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 1 parking brake shoe and shoe adjuster screw set.</ptxt>
<figure>
<graphic graphicname="C172179" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000270F014X_01_0006" proc-id="RM22W0E___00009L800000">
<ptxt>REMOVE NO. 2 PARKING BRAKE SHOE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the shoe hold down spring cup, compression spring and shoe hold down spring pin.</ptxt>
<sst>
<sstitem>
<s-number>09718-00011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C172175E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 2 parking brake shoe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000270F014X_01_0016" proc-id="RM22W0E___00009L900000">
<ptxt>REMOVE PARKING BRAKE SHOE LEVER SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 3 parking brake cable from the parking brake shoe lever.</ptxt>
</s2>
<s2>
<ptxt>Remove the parking brake shoe lever.</ptxt>
<figure>
<graphic graphicname="C172177" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000270F014X_01_0017" proc-id="RM22W0E___00009M200000">
<ptxt>DISCONNECT NO. 3 PARKING BRAKE CABLE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and No. 3 parking brake cable.</ptxt>
<figure>
<graphic graphicname="C170909" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000270F014X_01_0011" proc-id="RM22W0E___0000A8400000">
<ptxt>DISCONNECT REAR SPEED SENSOR LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the speed sensor with the nut.</ptxt>
<figure>
<graphic graphicname="C170903" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>8.3</t-value1>
<t-value2>85</t-value2>
<t-value3>73</t-value3>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure there are no pieces of iron or other foreign matter attached to the sensor tip.</ptxt>
</item>
<item>
<ptxt>While inserting the speed sensor into the knuckle hole, do not strike or damage the sensor tip.</ptxt>
</item>
<item>
<ptxt>After installing the speed sensor, make sure there is no clearance or foreign matter between the sensor stay part and the knuckle.</ptxt>
</item>
<item>
<ptxt>Make sure there is no foreign matter attached to the speed sensor rotor.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000270F014X_01_0012" proc-id="RM22W0E___00009M000000">
<ptxt>REMOVE REAR AXLE SHAFT LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 nuts and rear axle shaft together with the parking brake plate.</ptxt>
<figure>
<graphic graphicname="C159453" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000270F014X_01_0013" proc-id="RM22W0E___00009M100000">
<ptxt>REMOVE REAR AXLE SHAFT OIL SEAL LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, tap out the oil seal.</ptxt>
<figure>
<graphic graphicname="F042742E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09308-00010</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>Be careful not to damage the axle housing hole.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>