<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002N" variety="S002N">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002N_7C3ZS_T00SV" variety="T00SV">
<name>REAR BUMPER (w/ Pintle Hook)</name>
<para id="RM0000038K4018X" category="A" type-id="8000E" name-id="ET8EG-01" from="201301">
<name>REASSEMBLY</name>
<subpara id="RM0000038K4018X_01" type-id="01" category="01">
<s-1 id="RM0000038K4018X_01_0011" proc-id="RM22W0E___0000JGT00000">
<ptxt>INSTALL REAR NO. 2 BUMPER SIDE BRACKET (w/ Tire Carrier)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws and 2 clips to install the rear No. 2 bumper side bracket and rear No. 3 bumper side bracket.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4018X_01_0012" proc-id="RM22W0E___0000JGU00000">
<ptxt>INSTALL REAR NO. 2 STEP COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 10 claws to install the rear No. 2 step cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4018X_01_0003" proc-id="RM22W0E___0000JGP00000">
<ptxt>INSTALL REFLEX REFLECTOR ASSEMBLY LH (w/o Rear Fog Light)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the reflex reflector assembly LH.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 nuts.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4018X_01_0004" proc-id="RM22W0E___0000JGQ00000">
<ptxt>INSTALL REFLEX REFLECTOR ASSEMBLY RH (w/o Rear Fog Light)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038K4018X_01_0017" proc-id="RM22W0E___0000J2400000">
<ptxt>INSTALL REAR FOG LIGHT ASSEMBLY LH (w/ Rear Fog Light)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw.</ptxt>
<figure>
<graphic graphicname="E155858" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connector the connectors.</ptxt>
</s2>
<s2>
<ptxt>Install the fog light with the 2 nuts.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038K4018X_01_0018" proc-id="RM22W0E___0000JGV00000">
<ptxt>INSTALL REAR FOG LIGHT ASSEMBLY RH (w/ Rear Fog Light)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038K4018X_01_0005" proc-id="RM22W0E___0000JGR00000">
<ptxt>INSTALL REAR BUMPER ENERGY ABSORBER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 claws to install the rear bumper energy absorber.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038K4018X_01_0014" proc-id="RM22W0E___0000CTR00000">
<ptxt>INSTALL NO. 1 ULTRASONIC SENSOR RETAINER (w/ TOYOTA Parking Assist-sensor System)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to install the No. 1 ultrasonic sensor retainer on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Align the keyhole and protrusion as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E244163E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Keyhole</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not damage the bumper with the protrusion when installing the No. 1 ultrasonic sensor retainer.</ptxt>
</item>
<item>
<ptxt>Securely install the No. 1 ultrasonic sensor retainer so that there are no gaps between the retainer and surface of the rear bumper.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>The illustration shows the No. 1 ultrasonic sensor retainer for the RH corner. The vertical orientation of the No. 1 ultrasonic sensor retainers for the LH corner is opposite that of the image shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the 2 claws to install the No. 1 ultrasonic sensor retainer.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038K4018X_01_0013" proc-id="RM22W0E___0000CTS00000">
<ptxt>INSTALL NO. 2 ULTRASONIC SENSOR RETAINER (w/ TOYOTA Parking Assist-sensor System)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to install the No. 2 ultrasonic sensor retainer on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Align the keyhole and protrusion as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E244160E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Keyhole</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not damage the bumper with the protrusion when installing the No. 2 ultrasonic sensor retainer.</ptxt>
</item>
<item>
<ptxt>Securely install the No. 2 ultrasonic sensor retainer so that there are no gaps between the retainer and surface of the rear bumper.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Attach the 2 claws to install the No. 2 ultrasonic sensor retainer.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038K4018X_01_0015" proc-id="RM22W0E___0000CTT00000">
<ptxt>INSTALL NO. 1 ULTRASONIC SENSOR (w/ TOYOTA Parking Assist-sensor System)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure for all No. 1 ultrasonic sensors.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the No. 1 ultrasonic sensor.</ptxt>
<atten3>
<ptxt>Push the ultrasonic sensor retainer from the outside of the bumper when there is a gap between the retainer and the bumper surface. In this case, do not push on the ultrasonic sensor.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000038K4018X_01_0010" proc-id="RM22W0E___0000JGS00000">
<ptxt>INSTALL NO. 2 FRAME WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/o Rear Fog Light:</ptxt>
<s3>
<ptxt>Attach the 12 clamps to install the No. 2 frame wire.</ptxt>
</s3>
<s3>
<ptxt>Connect the 4 connectors.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/ Rear Fog Light:</ptxt>
<s3>
<ptxt>Attach the 14 clamps to install the No. 2 frame wire.</ptxt>
</s3>
<s3>
<ptxt>Connect the 6 connectors.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/o TOYOTA Parking Assist-sensor System, w/ Rear Fog Light:</ptxt>
<s3>
<ptxt>Attach the 13 clamps to install the No. 2 frame wire.</ptxt>
</s3>
<s3>
<ptxt>Connect the 2 connectors.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>