<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3W9_T00PC" variety="T00PC">
<name>FRONT EVAPORATOR TEMPERATURE SENSOR (for RHD)</name>
<para id="RM000001HA602HX" category="G" type-id="3001K" name-id="AC0U1-32" from="201301">
<name>INSPECTION</name>
<subpara id="RM000001HA602HX_01" type-id="01" category="01">
<s-1 id="RM000001HA602HX_01_0001" proc-id="RM22W0E___0000GX300000">
<ptxt>INSPECT FRONT EVAPORATOR TEMPERATURE SENSOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E160596E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry>
<ptxt>Tester Connection</ptxt>
</entry>
<entry>
<ptxt>Condition</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>-10°C (14°F)</ptxt>
</entry>
<entry>
<ptxt>7.30 to 9.10 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>-5°C (23°F)</ptxt>
</entry>
<entry>
<ptxt>5.65 to 6.95 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>0°C (32°F)</ptxt>
</entry>
<entry>
<ptxt>4.40 to 5.35 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>5°C (41°F)</ptxt>
</entry>
<entry>
<ptxt>3.40 to 4.15 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>10°C (50°F)</ptxt>
</entry>
<entry>
<ptxt>2.70 to 3.25 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>15°C (59°F)</ptxt>
</entry>
<entry>
<ptxt>2.14 to 2.58 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry>
<ptxt>1.71 to 2.05 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>25°C (77°F)</ptxt>
</entry>
<entry>
<ptxt>1.38 to 1.64 kΩ</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>1 - 2</ptxt>
</entry>
<entry>
<ptxt>30°C (86°F)</ptxt>
</entry>
<entry>
<ptxt>1.11 to 1.32 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the front evaporator temperature sensor.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Even slightly touching the sensor may change the resistance value. Be sure to hold the connector of the sensor.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>When measuring, the sensor temperature must be almost the same as the ambient temperature.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>As the temperature increases, the resistance decreases (see the graph).</ptxt>
</atten4>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>