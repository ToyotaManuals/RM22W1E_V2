<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S002D" variety="S002D">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S002D_7C3WJ_T00PM" variety="T00PM">
<name>AIR CONDITIONING PRESSURE SENSOR</name>
<para id="RM0000030JG033X" category="A" type-id="30014" name-id="ACBKI-02" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000030JG033X_01" type-id="01" category="01">
<s-1 id="RM0000030JG033X_01_0001" proc-id="RM22W0E___0000H0A00000">
<ptxt>INSTALL AIR CONDITIONER PRESSURE SENSOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E156981" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Sufficiently apply compressor oil to a new O-ring and the fitting surface of the pressure sensor.</ptxt>
<spec>
<title>Compressor oil</title>
<specitem>
<ptxt>ND-OIL 8 or equivalent</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Install O-ring on the pressure switch.</ptxt>
</s2>
<s2>
<ptxt>Install the pressure sensor.</ptxt>
<torque>
<torqueitem>
<t-value1>10.8</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000030JG033X_01_0002" proc-id="RM22W0E___000048900000">
<ptxt>CHARGE REFRIGERANT
</ptxt>
<content1 releasenbr="2">
<sst>
<sstitem>
<s-number>09985-20010</s-number>
<s-subnumber>09985-02130</s-subnumber>
<s-subnumber>09985-02150</s-subnumber>
<s-subnumber>09985-02090</s-subnumber>
<s-subnumber>09985-02110</s-subnumber>
<s-subnumber>09985-02010</s-subnumber>
<s-subnumber>09985-02050</s-subnumber>
<s-subnumber>09985-02060</s-subnumber>
<s-subnumber>09985-02070</s-subnumber>
</sstitem>
</sst>
<s2>
<ptxt>Perform vacuum purging using a vacuum pump.</ptxt>
</s2>
<s2>
<ptxt>Charge refrigerant HFC-134a (R134a).</ptxt>
<table pgwide="1">
<title>Standard:</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Condenser Core Thickness</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Conditioning Type</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>Refrigerant Charging Amount</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="3" valign="middle">
<ptxt>22 mm (0.866 in.)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>w/o Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>870 +/-30 g (30.7 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>870 +/-30 g (30.7 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>w/ Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>1010 +/-30 g (35.6 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>960 +/-30 g (33.9 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="3" valign="middle">
<ptxt>16 mm (0.630 in.)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>w/o Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>770 +/-30 g (27.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>770 +/-30 g (27.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>w/ Rear Cooler</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>970 +/-30 g (34.2 +/-1.1 oz.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Cool Box</ptxt>
</entry>
<entry valign="middle">
<ptxt>920 +/-30 g (32.5 +/-1.1 oz.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="I037365E19" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not operate the cooler compressor before charging refrigerant as the cooler compressor will not work properly without any refrigerant, and will overheat.</ptxt>
</item>
<item>
<ptxt>Approximately 200 g (7.05 oz.) of refrigerant may need to be charged after bubbles disappear. The refrigerant amount should be checked by measuring its quantity, and not with the sight glass.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000030JG033X_01_0003" proc-id="RM22W0E___000048A00000">
<ptxt>WARM UP ENGINE
</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Warm up the engine at less than 1850 rpm for 2 minutes or more after charging the refrigerant.</ptxt>
<atten3>
<ptxt>Be sure to warm up the compressor when turning the A/C switch is on after removing and installing the cooler refrigerant lines (including the compressor), to prevent damage to the compressor.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000030JG033X_01_0004" proc-id="RM22W0E___000048B00000">
<ptxt>CHECK REFRIGERANT GAS LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>After recharging the refrigerant gas, check for refrigerant gas leakage using a halogen leak detector.</ptxt>
</s2>
<s2>
<ptxt>Perform the operation under these conditions:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Stop the engine.</ptxt>
</item>
<item>
<ptxt>Secure good ventilation (the halogen leak detector may react to volatile gases other than refrigerant, such as evaporated gasoline or exhaust gas).</ptxt>
</item>
<item>
<ptxt>Repeat the test 2 or 3 times.</ptxt>
</item>
<item>
<ptxt>Make sure that some refrigerant remains in the refrigeration system. When compressor is off: approximately 392 to 588 kPa (4.0 to 6.0 kgf/cm<sup>2</sup>, 57 to 85 psi).</ptxt>
</item>
</list1>
</s2>
<s2>
<ptxt>Using a halogen leak detector, check the refrigerant line for leakage.</ptxt>
<figure>
<graphic graphicname="I042222E21" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>If a gas leak is not detected on the drain hose, remove the blower motor control (blower resistor) from the cooling unit. Insert the halogen leak detector sensor into the unit and perform the test.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and wait for approximately 20 minutes. Bring the halogen leak detector close to the pressure switch and perform the test.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000030JG033X_01_0005" proc-id="RM22W0E___0000H0B00000">
<ptxt>INSTALL FRONT BUMPER COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Standard:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM0000038JV01HX"/>)</ptxt>
</s2>
<s2>
<ptxt>w/ Winch:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM0000038JV01GX"/>)</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>