<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001F" variety="S001F">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001F_7C3O7_T00HA" variety="T00HA">
<name>STEERING KNUCKLE</name>
<para id="RM0000016X4038X" category="A" type-id="30014" name-id="AD15V-02" from="201301">
<name>INSTALLATION</name>
<subpara id="RM0000016X4038X_02" type-id="11" category="10" proc-id="RM22W0E___00009KS00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedures for the LH side and RH side.</ptxt>
</item>
<item>
<ptxt>The procedures listed below are for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000016X4038X_01" type-id="01" category="01">
<s-1 id="RM0000016X4038X_01_0039" proc-id="RM22W0E___00009KM00000">
<ptxt>INSTALL STEERING KNUCKLE OIL SEAL LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a press, press in a new oil seal.</ptxt>
<sst>
<sstitem>
<s-number>09950-60020</s-number>
<s-subnumber>09951-01030</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C179621E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The oil seal is a non-reusable part.</ptxt>
</item>
<item>
<ptxt>Do not damage the steering knuckle oil seal.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016X4038X_01_0002" proc-id="RM22W0E___00009KG00000">
<ptxt>INSTALL STEERING KNUCKLE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the steering knuckle to the front suspension upper arm with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>110</t-value1>
<t-value2>1122</t-value2>
<t-value4>81</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install a new clip.</ptxt>
<atten4>
<ptxt>If the holes for the clip are not aligned, tighten the nut up to another 60°.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016X4038X_01_0031" proc-id="RM22W0E___0000A1R00000">
<ptxt>CONNECT FRONT LOWER BALL JOINT ATTACHMENT LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the attachment to the steering knuckle with the 2 bolts.</ptxt>
<figure>
<graphic graphicname="C173480" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>300</t-value1>
<t-value2>3059</t-value2>
<t-value4>221</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000016X4038X_01_0032" proc-id="RM22W0E___00009KL00000">
<ptxt>CONNECT TIE ROD END SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the tie rod end LH to the steering knuckle with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>69</t-value1>
<t-value2>704</t-value2>
<t-value4>51</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install a new cotter pin.</ptxt>
<atten4>
<ptxt>If the holes for the cotter pin are not aligned, tighten the nut up to another 60°.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016X4038X_01_0040" proc-id="RM22W0E___00009KN00000">
<ptxt>INSTALL FRONT AXLE HUB SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front axle hub (See page <xref label="Seep01" href="RM000003BNP00KX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016X4038X_01_0042" proc-id="RM22W0E___00009KP00000">
<ptxt>CONNECT FRONT SPEED SENSOR LH</ptxt>
<content1 releasenbr="1">
<ptxt>Connect the front speed sensor (See page <xref label="Seep01" href="RM000001B2H01PX_03_0013"/>).</ptxt>
</content1>
</s-1>
<s-1 id="RM0000016X4038X_01_0020" proc-id="RM22W0E___00009KH00000">
<ptxt>INSTALL FRONT WHEEL</ptxt>
<content1 releasenbr="1">
<torque>
<subtitle>for Aluminum Wheel</subtitle>
<torqueitem>
<t-value1>131</t-value1>
<t-value2>1336</t-value2>
<t-value4>97</t-value4>
</torqueitem>
<subtitle>for Steel Wheel</subtitle>
<torqueitem>
<t-value1>209</t-value1>
<t-value2>2131</t-value2>
<t-value4>154</t-value4>
</torqueitem>
</torque>
</content1>
</s-1>
<s-1 id="RM0000016X4038X_01_0047" proc-id="RM22W0E___00009KR00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000016X4038X_01_0022" proc-id="RM22W0E___00009KJ00000">
<ptxt>CHECK SPEED SENSOR SIGNAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the speed sensor signal (See page <xref label="Seep01" href="RM0000054KD001X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016X4038X_01_0041" proc-id="RM22W0E___00009KO00000">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFE0JBX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016X4038X_01_0044" proc-id="RM22W0E___00009JQ00000">
<ptxt>MEASURE VEHICLE HEIGHT (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the inspection on a level surface.</ptxt>
</item>
<item>
<ptxt>Ensure that the wheels are on the ground and facing straight ahead.</ptxt>
</item>
<item>
<ptxt>Perform the inspection with the vehicle load completely on the suspension.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Perform this step with the fuel tank full.</ptxt>
</item>
<item>
<ptxt>If there are any parts installed to the vehicle which place any unbalanced load on the left or right side of the vehicle, remove them.</ptxt>
</item>
</list1>
</atten4>
<s2>
<ptxt>Set the tire pressure to the specified value(s) (See page <xref label="Seep01" href="RM000003DGS00LX"/>).</ptxt>
</s2>
<s2>
<ptxt>Bounce the vehicle to stabilize the suspension.</ptxt>
</s2>
<s2>
<ptxt>Measure the distance from the ground to the top of the bumper and calculate the difference in the vehicle height between left and right. Perform this procedure for both the front and rear wheels.</ptxt>
<figure>
<graphic graphicname="C171398" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Height difference of left and right sides</title>
<specitem>
<ptxt>15 mm (0.591 in.) or less</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>If not as specified, perform the vehicle tilt calibration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000016X4038X_01_0045" proc-id="RM22W0E___00009W800000">
<ptxt>CLOSE STABILIZER CONTROL WITH ACCUMULATOR HOUSING SHUTTER VALVE (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the inspection on a level surface.</ptxt>
</item>
<item>
<ptxt>Ensure that the wheels are on the ground and facing straight ahead.</ptxt>
</item>
<item>
<ptxt>Perform the inspection with the vehicle load completely on the suspension.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Perform this step with the fuel tank full.</ptxt>
</item>
<item>
<ptxt>If there are any parts installed to the vehicle which place any unbalanced load on the left or right side of the vehicle, remove them.</ptxt>
</item>
</list1>
</atten4>
<s2>
<ptxt>Using a 5 mm hexagon socket wrench, tighten the lower and upper chamber shutter valves of the stabilizer control with accumulator housing.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000016X4038X_01_0046" proc-id="RM22W0E___00009VZ00000">
<ptxt>INSTALL STABILIZER CONTROL VALVE PROTECTOR (w/ KDSS)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C172095" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the valve protector with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the clamp, and connect the connector to the valve protector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000016X4038X_01_0021" proc-id="RM22W0E___00009KI00000">
<ptxt>INSPECT AND ADJUST FRONT WHEEL ALIGNMENT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect and adjust the front wheel alignment (See page <xref label="Seep01" href="RM000001V43020X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016X4038X_01_0043" proc-id="RM22W0E___00009KQ00000">
<ptxt>ADJUST HEADLIGHT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Standard:</ptxt>
<ptxt>Adjust the headlight assembly (See page <xref label="Seep01" href="RM0000011MI096X"/>).</ptxt>
</s2>
<s2>
<ptxt>for HID Headlight:</ptxt>
<ptxt>Adjust the headlight assembly (See page <xref label="Seep02" href="RM0000011MI095X"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>