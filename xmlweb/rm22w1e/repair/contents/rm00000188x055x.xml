<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12048_S0021" variety="S0021">
<name>3UR-FE BATTERY / CHARGING</name>
<ttl id="12048_S0021_7C3S7_T00LA" variety="T00LA">
<name>GENERATOR</name>
<para id="RM00000188X055X" category="G" type-id="3001K" name-id="BH185-01" from="201301">
<name>INSPECTION</name>
<subpara id="RM00000188X055X_01" type-id="01" category="01">
<s-1 id="RM00000188X055X_01_0001" proc-id="RM22W0E___0000D4L00000">
<ptxt>INSPECT GENERATOR ROTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the resistance.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Slip ring - Slip ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.5 to 1.9 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="A117220E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Slip Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the generator rotor assembly.</ptxt>
</s3>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Slip ring - Rotor core</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="A117221E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Slip Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Rotor Core</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the generator rotor assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Using a vernier caliper, measure the slip ring diameter.</ptxt>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>14.2 to 14.8 mm (0.559 to 0.583 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum diameter</title>
<specitem>
<ptxt>14.0 mm (0.551 in.)</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="A117222E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Diameter</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the diameter is less than the minimum, replace the generator rotor assembly.</ptxt>
</s2>
<s2>
<ptxt>Check that the generator rotor bearing is not rough or worn.</ptxt>
<figure>
<graphic graphicname="A163064" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If necessary, replace the generator rotor assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000188X055X_01_0002" proc-id="RM22W0E___0000D4M00000">
<ptxt>INSPECT GENERATOR BRUSH HOLDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the brush length.</ptxt>
<spec>
<title>Standard exposed length</title>
<specitem>
<ptxt>9.5 to 11.5 mm (0.374 to 0.453 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum exposed length</title>
<specitem>
<ptxt>4.5 mm (0.177 in.)</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="A128079E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Length</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the brush length is less than the minimum, replace the generator brush holder assembly.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>