<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>1UR-FE ENGINE CONTROL</name>
<ttl id="12005_S0006_7C3DF_T006I" variety="T006I">
<name>SFI SYSTEM</name>
<para id="RM0000012FY046X" category="C" type-id="305NY" name-id="ESUAH-06" from="201301" to="201308">
<dtccode>P0412</dtccode>
<dtcname>Secondary Air Injection System Switching Valve "A" Circuit</dtcname>
<subpara id="RM0000012FY046X_01" type-id="60" category="03" proc-id="RM22W0E___00001PW00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The secondary air injection system injects air into the exhaust port of the cylinder head using an electric air pump, starting when the engine is started cold and operating until the catalyst warms up, in order to promote combustion of unburned fuel and decrease the amount of hydrocarbons (HC) and carbon monoxide (CO) in the exhaust gas.</ptxt>
<figure>
<graphic graphicname="A217752E03" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.26in"/>
<colspec colname="COL2" colwidth="2.91in"/>
<colspec colname="COL3" colwidth="2.92in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0412</ptxt>
</entry>
<entry valign="middle">
<ptxt>All conditions are met for 3 seconds or more while idling immediately after the engine is started (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>Secondary air injection system is not operating (the air pump and air switching valve are off).</ptxt>
</item>
<item>
<ptxt>Diagnostic signal from the air injection control driver has a duty cycle of 40% (this indicates an open in the air switching valve circuit).</ptxt>
</item>
<item>
<ptxt>Battery voltage is 8 V or higher.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in air switching valve drive circuit</ptxt>
</item>
<item>
<ptxt>Air injection control driver</ptxt>
</item>
<item>
<ptxt>Air switching valve assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0412</ptxt>
</entry>
<entry valign="middle">
<ptxt>All conditions are met for 3 seconds or more while idling immediately after the engine is started (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>Secondary air injection system is operating (the air pump and air switching valve are on).</ptxt>
</item>
<item>
<ptxt>Diagnostic signal from the air injection control driver has a duty cycle of 40% (this indicates a short in the air switching valve circuit).</ptxt>
</item>
<item>
<ptxt>Battery voltage is 8 V or higher.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short between air switching valve drive circuit and body ground</ptxt>
</item>
<item>
<ptxt>Air injection control driver</ptxt>
</item>
<item>
<ptxt>Air switching valve assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000012FY046X_02" type-id="64" category="03" proc-id="RM22W0E___00001PX00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates an open or short circuit in the circuit containing the air switching valve of the secondary air injection system. The air injection control driver performs diagnosis of the air pump, air switching valve (bank 1) and itself, and sends the results of this diagnosis to the ECM as a duty signal. When the ECM receives a signal indicating a malfunction in the air pump, air switching valve or air injection control driver, it illuminates the MIL and stores a DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM0000012FY046X_10" type-id="73" category="03" proc-id="RM22W0E___00001Q300000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>This Secondary Air Injection Check only allows technicians to operate the secondary air injection system for a maximum of 5 seconds. Furthermore, the check can only be performed up to 4 times per trip. If the test is repeated, intervals of at least 30 seconds are required between checks. While secondary air injection system operation using the GTS is prohibited, the GTS display indicates the prohibition (WAIT or ERROR). If ERROR is displayed on the GTS during the test, stop the engine for 10 minutes, and then try again.</ptxt>
</item>
<item>
<ptxt>Performing Secondary Air Injection Check repeatedly may cause damage to the secondary air injection system. If it is necessary to repeat the check, leave an interval of several minutes between System Check operations to prevent the system from overheating.</ptxt>
</item>
<item>
<ptxt>When performing the Secondary Air Injection Check operation after the battery cable has been reconnected, wait for 7 minutes with the engine switch turned on (IG) or the engine running.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off when the Secondary Air Injection Check operation finishes.</ptxt>
</item>
</list1>
</atten3>
<list1 type="ordered">
<item>
<ptxt>Start the engine and warm it up.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off.</ptxt>
</item>
<item>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG).</ptxt>
</item>
<item>
<ptxt>Turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation) (See page <xref label="Seep01" href="RM000000PDK14GX"/>).</ptxt>
</item>
<item>
<ptxt>Turn the engine switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / Secondary Air injection check / Automatic Mode.</ptxt>
</item>
<item>
<ptxt>Start the engine after the GTS initialization is finished.</ptxt>
</item>
<item>
<ptxt>Perform the System Check operation by pressing ENTER (Next).</ptxt>
</item>
<item>
<ptxt>Perform the following to confirm the secondary air injection system pending codes: Press ENTER (Exit).</ptxt>
</item>
<item>
<ptxt>Check for pending DTCs.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No pending DTC is output.</ptxt>
</specitem>
</spec>
</item>
<item>
<ptxt>After "Secondary Air injection check" is completed, check for All Readiness by entering the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</item>
<item>
<ptxt>Input the DTC: P0412.</ptxt>
</item>
<item>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>GTS Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>

</row>
<row>
<entry valign="middle" align="center">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving pattern after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N/A</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the judgment result shows NORMAL, the system is normal.</ptxt>
</item>
<item>
<ptxt>If the judgment result shows ABNORMAL, the system has a malfunction.</ptxt>
</item>
</list1>
</atten4>
</item>
<item>
<ptxt>Turn the engine switch off.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM0000012FY046X_07" type-id="32" category="03" proc-id="RM22W0E___00001PY00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A303412E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000012FY046X_08" type-id="51" category="05" proc-id="RM22W0E___00001PZ00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>By using the GTS to perform the Secondary Air Injection Check operation in the System Check, the air-fuel ratio and the pressure in the secondary air injection system passage can be checked while the secondary air injection system is operating. This helps technicians to troubleshoot the system when it malfunctions.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM0000012FY046X_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012FY046X_09_0002" proc-id="RM22W0E___00001Q000000">
<testtitle>INSPECT AIR SWITCHING VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the air switching valve assembly for bank 1 (See page <xref label="Seep01" href="RM00000436300AX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012FY046X_09_0003" fin="false">OK</down>
<right ref="RM0000012FY046X_09_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012FY046X_09_0003" proc-id="RM22W0E___00001Q100000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR SWITCHING VALVE - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air switching valve connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C137-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012FY046X_09_0007" fin="false">OK</down>
<right ref="RM0000012FY046X_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012FY046X_09_0007" proc-id="RM22W0E___00001Q200000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR SWITCHING VALVE - AIR INJECTION CONTROL DRIVER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air switching valve connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the air injection control driver connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C137-5 (+B) - C61-6 (VV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C137-5 (+B) or C61-6 (VV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012FY046X_09_0008" fin="true">OK</down>
<right ref="RM0000012FY046X_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012FY046X_09_0005">
<testtitle>REPLACE AIR SWITCHING VALVE ASSEMBLY<xref label="Seep01" href="RM00000436500AX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012FY046X_09_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000012FY046X_09_0008">
<testtitle>REPLACE AIR INJECTION CONTROL DRIVER<xref label="Seep01" href="RM00000398Q009X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>