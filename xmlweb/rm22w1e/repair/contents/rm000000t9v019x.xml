<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UK_T00NN" variety="T00NN">
<name>SEAT HEATER SYSTEM</name>
<para id="RM000000T9V019X" category="T" type-id="3001H" name-id="SE011-23" from="201301">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM000000T9V019X_z0" proc-id="RM22W0E___0000FT200000">
<content5 releasenbr="1">
<atten4>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Seat Heater System</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry morerows="3" colsep="1" valign="middle">
<ptxt>Both front seat heaters do not operate (w/o climate control system)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>LH-IG fuse</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>SEAT-HTR fuse</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Front seat heater switch</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF001RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry morerows="3" colsep="1" valign="middle">
<ptxt>Both front seat heaters do not operate (w/ climate control system)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>LH-IG fuse</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>SEAT-A/C FAN fuse</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Front seat heater switch</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF001SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="4" colsep="1" valign="middle">
<ptxt>Front seat heater LH does not operate (w/o climate control system)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front seat heater switch</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF001RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Seat heater control sub-assembly LH (for front side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000030EY00TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front seat cushion heater LH</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF802HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front seatback heater LH</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF402WX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry morerows="5" colsep="1" valign="middle">
<ptxt>Front seat heater LH does not operate (w/ climate control system)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>SEAT-A/C LH fuse</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Front seat heater switch</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF001SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Seat climate control ECU LH</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000390T01LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Front seat cushion heater LH</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF802HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Front seatback heater LH</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF402WX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="4" colsep="1" valign="middle">
<ptxt>Front seat heater RH does not operate (w/o climate control system)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front seat heater switch</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF001RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Seat heater control sub-assembly RH (for front side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000030EY00TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front seat cushion heater RH</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF802HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front seatback heater RH</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF402WX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry morerows="5" colsep="1" valign="middle">
<ptxt>Front seat heater RH does not operate (w/ climate control system)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>SEAT-A/C RH fuse</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Front seat heater switch</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF001SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Seat climate control ECU RH</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000390T01LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Front seat cushion heater RH</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF802HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Front seatback heater RH</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF402WX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry morerows="2" colsep="1" valign="middle">
<ptxt>Indicator light (LH side) of front seat heater switch blinks (w/ climate control system)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Front seat cushion heater LH</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF802HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Seat climate control ECU LH</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000390T01LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="2" colsep="1" valign="middle">
<ptxt>Indicator light (RH side) of front seat heater switch blinks (w/ climate control system)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Front seat cushion heater RH</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF802HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Seat climate control ECU RH</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000390T01LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="4" colsep="1" valign="middle">
<ptxt>Both rear seat heaters do not operate (w/ rear seat heater system)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>ECU-IG No. 2 fuse</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>RR S/HTR fuse</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>IG1 NO. 5 relay</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear air conditioning control panel</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="6" colsep="1" valign="middle">
<ptxt>Rear seat heater LH does not operate (w/ rear seat heater system)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Seat heater control sub-assembly LH (for rear side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear air conditioning control panel</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear seat cushion heater LH (for 60/40 Split Seat Type 40 Side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF802JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Rear seat cushion heater LH (for 60/40 Split Seat Type 60 Side)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF802IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear seatback heater LH (for 60/40 Split Seat Type 40 Side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF402XX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Rear seatback heater LH (for 60/40 Split Seat Type 60 Side)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF402VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="6" colsep="1" valign="middle">
<ptxt>Rear seat heater RH does not operate (w/ rear seat heater system)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Seat heater control sub-assembly RH (for rear side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear air conditioning control panel</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear seat cushion heater RH (for 60/40 Split Seat Type 40 Side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF802JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Rear seat cushion heater RH (for 60/40 Split Seat Type 60 Side)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF802IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear seatback heater RH (for 60/40 Split Seat Type 40 Side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF402XX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Rear seatback heater RH (for 60/40 Split Seat Type 60 Side)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002XF402VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Harness or connector</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>