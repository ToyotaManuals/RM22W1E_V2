<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000F" variety="S000F">
<name>1GR-FE FUEL</name>
<ttl id="12008_S000F_7C3GF_T009I" variety="T009I">
<name>FUEL FILTER</name>
<para id="RM0000039XN00GX" category="A" type-id="30019" name-id="FU950-01" from="201301" to="201308">
<name>REPLACEMENT</name>
<subpara id="RM0000039XN00GX_01" type-id="01" category="01">
<s-1 id="RM0000039XN00GX_01_0001" proc-id="RM22W0E___00005S500000">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000028RU03VX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0014" proc-id="RM22W0E___00005SG00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0002" proc-id="RM22W0E___00005S600000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0012" proc-id="RM22W0E___00005SE00000">
<ptxt>REMOVE NO. 1 FUEL TUBE PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and No. 1 fuel tube protector.</ptxt>
<figure>
<graphic graphicname="A181395" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0003" proc-id="RM22W0E___00005S700000">
<ptxt>DISCONNECT NO. 2 FUEL MAIN TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Pull up the retainer and disconnect the No. 2 fuel main tube.</ptxt>
<figure>
<graphic graphicname="A175098E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0004" proc-id="RM22W0E___00005S800000">
<ptxt>DISCONNECT FUEL MAIN TUBE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Pull up the retainer and disconnect the fuel main tube.</ptxt>
<figure>
<graphic graphicname="A175100E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0005" proc-id="RM22W0E___00005S900000">
<ptxt>REMOVE FUEL FILTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and fuel filter.</ptxt>
<figure>
<graphic graphicname="A175102" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0010" proc-id="RM22W0E___00005SD00000">
<ptxt>INSTALL FUEL FILTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel filter with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>199</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0006" proc-id="RM22W0E___00005SA00000">
<ptxt>CONNECT FUEL MAIN TUBE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel main tube and push the retainer.</ptxt>
<figure>
<graphic graphicname="A175101E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0007" proc-id="RM22W0E___00005SB00000">
<ptxt>CONNECT NO. 2 FUEL MAIN TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 2 fuel main tube and push the retainer.</ptxt>
<figure>
<graphic graphicname="A175099E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before installing the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After the connection, check if the connectors and pipes are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Push the parts together firmly until a "click" sound is heard.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0008" proc-id="RM22W0E___00005SC00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039XN00GX_01_0011" proc-id="RM22W0E___000047800000">
<ptxt>INSPECT FOR FUEL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Make sure that there are no fuel leaks after performing maintenance on the fuel system.</ptxt>
<s3>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the ignition switch to ON and turn the GTS on.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Fuel Pump / Speed.</ptxt>
</s3>
<s3>
<ptxt>Check that there are no leaks from the fuel system.</ptxt>
<ptxt>If there are fuel leaks, repair or replace parts as necessary.</ptxt>
</s3>
<s3>
<ptxt>Turn the ignition switch off.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the GTS from the DLC3.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000039XN00GX_01_0013" proc-id="RM22W0E___00005SF00000">
<ptxt>INSTALL NO. 1 FUEL TUBE PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 fuel tube protector and 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>