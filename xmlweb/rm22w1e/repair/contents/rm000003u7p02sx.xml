<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000003U7P02SX" category="C" type-id="8048H" name-id="ESTDY-26" from="201308">
<dtccode>P011C</dtccode>
<dtcname>Charge Air Temperature/Intake Air Temperature Correlation (Bank 1)</dtcname>
<subpara id="RM000003U7P02SX_01" type-id="60" category="03" proc-id="RM22W0E___00003UP00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P007C (See page <xref label="Seep01" href="RM00000188107UX_01"/>).</ptxt>
<ptxt>Refer to DTC P0112 (See page <xref label="Seep02" href="RM00000187L07KX_01"/>).</ptxt>
<table pgwide="1">
<title>P011C</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>After engine completely warmed up, allow engine to soak for long time (10 hours or more). </ptxt>
<ptxt>Start the engine and idle the engine for 3 minutes or more.</ptxt>
</entry>
<entry valign="middle">
<ptxt>After the engine is completely warmed up and then allowed to soak for a long time (10 hours or more), the difference in temperature at the intake air temperature sensor and intake air temperature sensor (turbo) after turbocharger after idling for 3 minutes is 15°C (59°F) or higher.</ptxt>
<ptxt>(2 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in intake air temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>Open or short in intake air temperature sensor (turbo) circuit</ptxt>
</item>
<item>
<ptxt>Intake air temperature sensor (built into mass air flow meter)</ptxt>
</item>
<item>
<ptxt>Intake air temperature sensor (turbo)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Engine off timer (P2610)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P011C</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Intake Air</ptxt>
</item>
<item>
<ptxt>Intake Air Temp (Turbo)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003U7P02SX_04" type-id="32" category="03" proc-id="RM22W0E___00003V200001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P007C (See page <xref label="Seep01" href="RM00000188107UX_02"/>).</ptxt>
<ptxt>Refer to DTC P0112 (See page <xref label="Seep02" href="RM00000187L07KX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000003U7P02SX_02" type-id="51" category="05" proc-id="RM22W0E___00003UQ00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK08XX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK08XX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000003U7P02SX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003U7P02SX_03_0001" proc-id="RM22W0E___00003UR00001">
<testtitle>CHECK OTHER DTC OUTPUT (IN ADDITION TO DTC P011C)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P011C is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P011C and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003U7P02SX_03_0013" fin="false">A</down>
<right ref="RM000003U7P02SX_03_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0013" proc-id="RM22W0E___00003V100001">
<testtitle>READ VALUE USING GTS (ENGINE COOLANT TEMPERATURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Allow the engine to soak for 1 to 2 hours.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>It is not necessary to allow the engine to soak if the engine is cold.</ptxt>
</item>
<item>
<ptxt>The engine cools faster if the hood is opened.</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / Coolant temp.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The difference between the displayed value and outside air temperature is below 5°C (9°F).</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>If the difference between "Coolant Temp" and the outside air temperature is 5°C (9°F) or higher, the heat from the engine affects the intake air temperature sensor, causing an incorrect value to be displayed. In this case, allow the engine to soak again.</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000003U7P02SX_03_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0002" proc-id="RM22W0E___00003US00001">
<testtitle>READ VALUE USING GTS (INTAKE AIR TEMPERATURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / Intake Air.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The difference between the displayed value and outside air temperature is below 15°C (27°F).</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003U7P02SX_03_0003" fin="false">OK</down>
<right ref="RM000003U7P02SX_03_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0003" proc-id="RM22W0E___00003UT00001">
<testtitle>READ VALUE USING GTS (INTAKE AIR TEMPERATURE (TURBO))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / Intake Air Temp (Turbo).</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The difference between the displayed value and outside air temperature is below 15°C (27°F).</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003U7P02SX_03_0004" fin="false">OK</down>
<right ref="RM000003U7P02SX_03_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0004" proc-id="RM22W0E___00003UU00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329203AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000003U7P02SX_03_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0005" proc-id="RM22W0E___00003UV00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MASS AIR FLOW METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C50-4 (THA) - C45-115 (THA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-5 (E2) - C45-92 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-4 (THA) or C45-115 (THA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C50-4 (THA) - C46-115 (THA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-5 (E2) - C46-92 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C50-4 (THA) or C46-115 (THA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003U7P02SX_03_0006" fin="false">OK</down>
<right ref="RM000003U7P02SX_03_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0006" proc-id="RM22W0E___00003UW00001">
<testtitle>REPLACE MASS AIR FLOW METER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the mass air flow meter (See page <xref label="Seep01" href="RM000002DFB00HX"/>).</ptxt>
<atten4>
<ptxt>If foreign matter is stuck inside the mass air flow meter, the output characteristics of the mass air flow meter may change, resulting in a malfunction.</ptxt>
</atten4>
</test1>
</content6>
<res>
<right ref="RM000003U7P02SX_03_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0007" proc-id="RM22W0E___00003UX00001">
<testtitle>CHECK HARNESS AND CONNECTOR (INTAKE AIR TEMPERATURE SENSOR (TURBO) - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the intake air temperature sensor (turbo) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.76in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C79-2 - C45-90 (THIA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C79-1 - C45-92 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C79-2 or C45-90 (THIA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.76in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C79-2 - C46-90 (THIA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C79-1 - C46-92 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C79-2 or C46-90 (THIA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the intake air temperature sensor (turbo) connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003U7P02SX_03_0008" fin="false">OK</down>
<right ref="RM000003U7P02SX_03_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0008" proc-id="RM22W0E___00003UY00001">
<testtitle>REPLACE INTAKE AIR TEMPERATURE SENSOR (TURBO)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the intake air temperature sensor (turbo) (See page <xref label="Seep01" href="RM0000014XP01ZX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000003U7P02SX_03_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0009" proc-id="RM22W0E___00003UZ00001">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003U7P02SX_03_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0010" proc-id="RM22W0E___00003V000001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Allow the engine to soak for 1 to 2 hours.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>It is not necessary to allow the engine to soak if the engine is cold.</ptxt>
</item>
<item>
<ptxt>The engine cools faster if the hood is opened.</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / Coolant temp.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<atten3>
<ptxt>If the difference between "Coolant Temp" and the outside air temperature is 5°C (9°F) or higher, the heat from the engine affects the intake air temperature sensor, causing an incorrect value to be displayed. In this case, allow the engine to soak again.</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / Intake Air and Intake Air Temp (Turbo).</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The difference between the displayed value and outside air temperature is below 15°C (27°F).</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003U7P02SX_03_0011" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0011">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000003U7P02SX_03_0012">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW065X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>