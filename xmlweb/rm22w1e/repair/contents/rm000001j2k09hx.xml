<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12048_S001Z" variety="S001Z">
<name>1GR-FE BATTERY / CHARGING</name>
<ttl id="12048_S001Z_7C3S1_T00L4" variety="T00L4">
<name>CHARGING SYSTEM</name>
<para id="RM000001J2K09HX" category="J" type-id="807NO" name-id="BH1KX-02" from="201308">
<dtccode/>
<dtcname>Noise Occurs from V-ribbed Belt or Generator Assembly</dtcname>
<subpara id="RM000001J2K09HX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001J2K09HX_01" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001J2K09HX_01_0008" proc-id="RM22W0E___0000K3800000">
<testtitle>CONFIRM PROBLEM SYMPTOM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Confirm the problem symptom.</ptxt>
<spec>
<title>Result</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Symptom</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Noise occurs from fan and generator V belt</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Noise occurs from generator assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2K09HX_01_0005" fin="false">A</down>
<right ref="RM000001J2K09HX_01_0012" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0005" proc-id="RM22W0E___0000K3600000">
<testtitle>CHECK FAN AND GENERATOR V BELT FOR WEAR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the fan and generator V belt for wear or damage.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The fan and generator V belt is not worn or damaged.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2K09HX_01_0006" fin="false">OK</down>
<right ref="RM000001J2K09HX_01_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0006" proc-id="RM22W0E___0000K3700000">
<testtitle>CHECK GENERATOR PULLEY WITH CLUTCH FOR WEAR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the generator pulley with clutch grooves for wear or damage.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The generator pulley with clutch grooves are not worn or damaged.</ptxt>
</specitem>
</spec>
<spec>
<title>Result</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>The Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Generator Assembly (for 100 A Type))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Generator Assembly (for 130 A Type))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2K09HX_01_0002" fin="false">A</down>
<right ref="RM000001J2K09HX_01_0003" fin="true">B</right>
<right ref="RM000001J2K09HX_01_0014" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0002" proc-id="RM22W0E___0000K3500000">
<testtitle>CHECK FOR NOISE WHILE CLUTCH PULLEY IS OPERATING</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Start the engine and check the generator pulley with clutch for looseness.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The generator pulley with clutch is not loose.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2K09HX_01_0009" fin="false">OK</down>
<right ref="RM000001J2K09HX_01_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0009" proc-id="RM22W0E___0000K3900000">
<testtitle>INSPECT GENERATOR PULLEY WITH CLUTCH (UNIT INSPECTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the generator assembly.</ptxt>
<list1 type="unordered">
<item>
<ptxt>for 100 A Type: (See page <xref label="Seep01" href="RM000001ONP03YX"/>)</ptxt>
</item>
<item>
<ptxt>for 130 A Type: (See page <xref label="Seep02" href="RM000001ONP03ZX"/>)</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Check the installation condition of the generator pulley cap.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The generator pulley cap is not loose or missing.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Check for grease leaks (for wet pulley) or particle formation due to friction (for dry pulley).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No grease leaks (for wet pulley) or large build-up of particles (for dry pulley).</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Check the generator pulley with clutch for misalignment (interference with the generator assembly).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The generator pulley with clutch is correctly aligned (no interference with the generator assembly).</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Turn the generator pulley with clutch clockwise and counterclockwise by hand and check for noise.</ptxt>
<figure>
<graphic graphicname="A288972" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>Noise does not occur when turned in both directions.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Turn the generator pulley with clutch clockwise and counterclockwise by hand and visually check for runout.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The generator pulley with clutch does not have runout.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Using a screwdriver, hold the fan of the generator rotor assembly located inside the generator assembly in place and check that the generator pulley with clutch locks when turned clockwise and turns freely when turned counterclockwise.</ptxt>
<figure>
<graphic graphicname="A288971" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>The generator pulley with clutch locks when turned clockwise and turns freely when turned counterclockwise.</ptxt>
</specitem>
</spec>
<spec>
<title>Result</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>The Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Generator Assembly (for 100 A Type))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Generator Assembly (for 130 A Type))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2K09HX_01_0007" fin="true">A</down>
<right ref="RM000001J2K09HX_01_0003" fin="true">B</right>
<right ref="RM000001J2K09HX_01_0014" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0007">
<testtitle>INSPECT PULLEY OTHER THAN GENERATOR ASSEMBLY PULLEY</testtitle>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0010">
<testtitle>REPLACE FAN AND GENERATOR V BELT</testtitle>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0003">
<testtitle>REPLACE GENERATOR PULLEY WITH CLUTCH (for 100 A Type)<xref label="Seep01" href="RM000001ONQ07NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0011">
<testtitle>TIGHTEN GENERATOR PULLEY WITH CLUTCH TO SPECIFIED TORQUE</testtitle>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0012" proc-id="RM22W0E___0000K3A00000">
<testtitle>CHECK FOR NOISE WHEN ROTOR IS TURNING FREELY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform a road test and check that noise does not occur when decelerating.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Noise does not occur.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Turn the engine off and check that no noise is produced by the generator pulley with clutch as the engine stops.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Noise does not occur.</ptxt>
</specitem>
</spec>
<spec>
<title>Result</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>The Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Generator Assembly (for 100 A Type))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Generator Assembly (for 130 A Type))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2K09HX_01_0013" fin="false">A</down>
<right ref="RM000001J2K09HX_01_0003" fin="true">B</right>
<right ref="RM000001J2K09HX_01_0014" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0013" proc-id="RM22W0E___0000K3B00000">
<testtitle>CHECK ENGAGEMENT OF GENERATOR PULLEY WITH CLUTCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the generator assembly.</ptxt>
<list1 type="unordered">
<item>
<ptxt>for 100 A Type: (See page <xref label="Seep01" href="RM000001ONP03YX"/>)</ptxt>
</item>
<item>
<ptxt>for 130 A Type: (See page <xref label="Seep02" href="RM000001ONP03ZX"/>)</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Using a screwdriver, hold the fan in the generator rotor assembly located inside the generator assembly in place and check that the generator pulley with clutch locks when turned clockwise (locking direction).</ptxt>
<figure>
<graphic graphicname="A288971" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>The generator pulley with clutch locks.</ptxt>
</specitem>
</spec>
<spec>
<title>Result</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>The Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK (Generator Assembly (for 100 A Type))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK (Generator Assembly (for 130 A Type))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Generator Assembly (for 100 A Type))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Generator Assembly (for 130 A Type))</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<right ref="RM000001J2K09HX_01_0004" fin="true">A</right>
<right ref="RM000001J2K09HX_01_0015" fin="true">B</right>
<right ref="RM000001J2K09HX_01_0003" fin="true">C</right>
<right ref="RM000001J2K09HX_01_0014" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0004">
<testtitle>REPAIR OR REPLACE GENERATOR ASSEMBLY (for 100 A Type)<xref label="Seep01" href="RM000001ONQ07NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0014">
<testtitle>REPLACE GENERATOR PULLEY WITH CLUTCH (for 130 A Type)<xref label="Seep01" href="RM000001ONQ07OX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001J2K09HX_01_0015">
<testtitle>REPAIR OR REPLACE GENERATOR ASSEMBLY (for 130 A Type)<xref label="Seep01" href="RM000001ONQ07OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>