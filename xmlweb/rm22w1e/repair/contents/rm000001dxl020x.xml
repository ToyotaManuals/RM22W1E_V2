<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PH_T00IK" variety="T00IK">
<name>VEHICLE STABILITY CONTROL SYSTEM</name>
<para id="RM000001DXL020X" category="J" type-id="300NF" name-id="BCE72-03" from="201308">
<dtccode/>
<dtcname>ABS Warning Light does not Come ON</dtcname>
<subpara id="RM000001DXL020X_01" type-id="60" category="03" proc-id="RM22W0E___0000AC100001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to ABS Warning Light Remains ON (See page <xref label="Seep01" href="RM000001DXE020X"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000001DXL020X_02" type-id="32" category="03" proc-id="RM22W0E___0000AC200001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to ABS Warning Light Remains ON (See page <xref label="Seep01" href="RM000001DXE020X_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000001DXL020X_03" type-id="51" category="05" proc-id="RM22W0E___0000AC300001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the master cylinder solenoid, perform zero point calibration and store the system information (See page <xref label="Seep01" href="RM00000452J00LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001DXL020X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001DXL020X_05_0001" proc-id="RM22W0E___0000AC400001">
<testtitle>INSPECT ABS WARNING LIGHT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A24 skid control ECU (master cylinder solenoid) connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check that the ABS warning light comes on.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>ABS warning light comes on.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXL020X_05_0015" fin="false">A</down>
<right ref="RM000001DXL020X_05_0005" fin="true">B</right>
<right ref="RM000001DXL020X_05_0013" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXL020X_05_0015" proc-id="RM22W0E___0000ABS00001">
<testtitle>CHECK CAN COMMUNICATION LINE
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Select "CAN Bus Check" from the System Selection Menu screen, and follow the prompts on the screen to inspect the CAN Bus.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>"CAN Bus Check" indicates no malfunctions in CAN communication.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001DXL020X_05_0016" fin="false">A</down>
<right ref="RM000001DXL020X_05_0011" fin="true">B</right>
<right ref="RM000001DXL020X_05_0014" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXL020X_05_0016" proc-id="RM22W0E___0000ABT00001">
<testtitle>CHECK DTC (CAN COMMUNICATION SYSTEM)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (for LHD: See page <xref label="Seep01" href="RM000001RSW03ZX"/>, for RHD: See page <xref label="Seep02" href="RM000001RSW040X"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>CAN system DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN system DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>CAN system DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001DXL020X_05_0017" fin="false">A</down>
<right ref="RM000001DXL020X_05_0012" fin="true">B</right>
<right ref="RM000001DXL020X_05_0014" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXL020X_05_0017" proc-id="RM22W0E___0000ABU00001">
<testtitle>PERFORM ACTIVE TEST USING GTS (INDICAT. LAMP ABS)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body Electrical / Combination Meter / Active Test.</ptxt>
</test1>
<test1>
<ptxt>Check the condition of the ABS warning light by operating the GTS.</ptxt>
<table pgwide="1">
<title>Combination Meter</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Indicat. Lamp ABS</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Perform the test with the vehicle stopped and engine idling.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The warning light turns on when operating the GTS.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000001DXL020X_05_0004" fin="true">OK</down>
<right ref="RM000001DXL020X_05_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXL020X_05_0005">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXL020X_05_0011">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXL020X_05_0012">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXL020X_05_0007">
<testtitle>GO TO METER / GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L048X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXL020X_05_0004">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000002V5U015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXL020X_05_0013">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXL020X_05_0014">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO09TX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>