<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>1VD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0008_7C3EB_T007E" variety="T007E">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000004ID500VX" category="C" type-id="804FR" name-id="ESTEZ-09" from="201301" to="201308">
<dtccode>P244B</dtccode>
<dtcname>Diesel Particulate Filter Differential Pressure Too High</dtcname>
<dtccode>P2465</dtccode>
<dtcname>Diesel Particulate Filter Differential Pressure Too High Bank 2</dtcname>
<subpara id="RM000004ID500VX_05" type-id="60" category="03" proc-id="RM22W0E___00003WE00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P244C (See page <xref label="Seep01" href="RM000004KN300SX_01"/>).</ptxt>
<table pgwide="1">
<title>P244B (Bank 1), P2465 (Bank 2)</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine is running at 4000 rpm with no load for 10 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<ptxt>While driving, Catalyst Differential Pressure becomes more than 0.45 for 3 seconds.</ptxt>
<ptxt>(1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Front exhaust pipe assembly (for bank 1 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter)</ptxt>
</item>
<item>
<ptxt>Differential pressure sensor assembly (for bank 1)</ptxt>
</item>
<item>
<ptxt>Differential pressure sensor assembly (for bank 2)</ptxt>
</item>
</list1>
<ptxt>Common Inspection Item:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Fuel injection system</ptxt>
</item>
<item>
<ptxt>Intake/exhaust system</ptxt>
</item>
<item>
<ptxt>After treatment control system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P244B</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DPF Differential Pressure</ptxt>
</item>
<item>
<ptxt>Catalyst Differential Press</ptxt>
</item>
<item>
<ptxt>Exhaust Fuel Addition FB</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2465</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>DPF Differential Pressure #2</ptxt>
</item>
<item>
<ptxt>Catalyst Differential Press #2</ptxt>
</item>
<item>
<ptxt>Exhaust Fuel Addition FB #2</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000004ID500VX_06" type-id="64" category="03" proc-id="RM22W0E___00003WF00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM compares pressure upstream and downstream of the DPF catalyst converter to monitor the differential pressure. If the differential pressure is large, the ECM determines that the DPF catalyst converter is contaminated or has deteriorated and illuminates the MIL.</ptxt>
<figure>
<graphic graphicname="A275258E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000004ID500VX_03" type-id="51" category="05" proc-id="RM22W0E___00003VY00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07ZX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN06OX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07ZX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the GTS. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000004ID500VX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004ID500VX_04_0001" proc-id="RM22W0E___00003VZ00000">
<testtitle>READ OUTPUT DTC (RECORD STORED DTC AND FREEZE FRAME DATA)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Record the stored DTC and freeze frame data.</ptxt>
</test1>
<test1>
<ptxt>Check Exhaust Fuel Addition FB, Exhaust Fuel Addition FB #2, Catalyst Differential Press and Catalyst Differential Press #2 in the freeze frame data.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the Exhaust Fuel Addition FB and Exhaust Fuel Addition FB #2 values are 1.45 or more, it indicates a high possibility of insufficient temperature increase during PM forced regeneration.</ptxt>
</item>
<item>
<ptxt>When the Catalyst Differential Press and Catalyst Differential Press #2 values are 0.45 or more, it indicates a high possibility that the DPF catalyst is clogged.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0002" proc-id="RM22W0E___00003W000000">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P244B OR P2465)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P244B or P2465 is output</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P244B or P2465 is output at the same time as P2463*</ptxt>
<ptxt>P2463 is detected just after P244B or P2465 is detected*</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Other relevant DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: When P244B or P2465 is output at the same time as P2463, perform this diagnostic procedure. When DTC P244B or P2465 is not output at the same time as P2463 is output, perform the diagnostic procedure for P244C or P244E (See page <xref label="Seep02" href="RM000004KN300SX"/>).</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0003" fin="false">A</down>
<right ref="RM000004ID500VX_04_0019" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0003" proc-id="RM22W0E___00003W100000">
<testtitle>READ VALUE USING GTS (DPF DIFFERENTIAL PRESSURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / DPF Differential Pressure and DPF Differential Pressure #2.</ptxt>
</test1>
<test1>
<ptxt>Check the DPF Differential Pressure and DPF Differential Pressure #2 values from idle speed to unloaded maximum engine speed.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DPF Differential Pressure and DPF Differential Pressure #2 increase as engine speed increases from idle speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="A241154E04" width="7.106578999in" height="5.787629434in"/>
</figure>
<atten4>
<ptxt>If DPF Differential Pressure and DPF Differential Pressure #2 indicate negative pressure for approx. 1 second as shown in the illustration (A) when the engine speed increases, the differential pressure sensor piping of the DPF catalyst may be clogged.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0004" fin="false">B</down>
<right ref="RM000004ID500VX_04_0007" fin="false">A</right>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0004" proc-id="RM22W0E___00003W200000">
<testtitle>CLEAN OR REPLACE VACUUM TRANSMITTING HOSE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Even though it has been determined that the vacuum transmitting hose needs to be repaired or cleaned, make sure to perform all diagnostic procedures for DTC P244B or P2465 before repairing or cleaning it.</ptxt>
</test1>
</content6>
<res>
<right ref="RM000004ID500VX_04_0007" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0007" proc-id="RM22W0E___00003W300000">
<testtitle>CHECK FOR BLACK SMOKE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Start the engine and drive the vehicle until the engine coolant temperature reaches 60°C (140°F) or higher.</ptxt>
</test1>
<test1>
<ptxt>Stop the vehicle and allow the engine to idle.</ptxt>
</test1>
<test1>
<ptxt>Fully depress the accelerator pedal for 5 seconds, and then release it [A].</ptxt>
</test1>
<test1>
<ptxt>Repeat the above procedure [A] 10 times [B].</ptxt>
</test1>
<test1>
<ptxt>Check for black smoke emission during procedures [A] and [B].</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Black smoke is emitted less than 5 times.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Even if the black smoke is very thin, count the number of black smoke emissions if there is any visible smoke.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0009" fin="false">OK</down>
<right ref="RM000004ID500VX_04_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0009" proc-id="RM22W0E___00003W500000">
<testtitle>PERFORM PM FORCED REGENERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Engine and ECT / Data List / Coolant Temp.</ptxt>
</test1>
<test1>
<ptxt>Warm up the engine (engine coolant temperature is 70°C (158°F) or higher).</ptxt>
</test1>
<test1>
<ptxt>Perform PM forced regeneration (See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0008" proc-id="RM22W0E___00003W400000">
<testtitle>READ VALUE USING GTS (CATALYST DIFFERENTIAL PRESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Catalyst Differential Press and Catalyst Differential Press #2.</ptxt>
</test1>
<test1>
<ptxt>Check the value of Catalyst Differential Press and Catalyst Differential Press #2 at 4000 rpm without load.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Catalyst Differential Press or Catalyst Differential Press #2 is more than 0.45</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0018" fin="true">B</down>
<right ref="RM000004ID500VX_04_0010" fin="false">A</right>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0010" proc-id="RM22W0E___00003W600000">
<testtitle>REPLACE EXHAUST PIPE ASSEMBLY (DPF CATALYST CONVERTER)</testtitle>
<content6 releasenbr="1">
<ptxt>Even though it has been determined that the DPF catalyst converter needs to be replaced, make sure to perform all diagnostic procedures for DTC P244B or P2465 before replacing it.</ptxt>
</content6>
<res>
<right ref="RM000004ID500VX_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0019" proc-id="RM22W0E___00003WC00000">
<testtitle>GO TO DTC CHART</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform inspection and repair of relevant DTCs (See page <xref label="Seep01" href="RM0000031HW05CX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0012" proc-id="RM22W0E___00003W700000">
<testtitle>CHECK FUEL INJECTION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the fuel injection system (See page <xref label="Seep01" href="RM000004KNK012X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0013" proc-id="RM22W0E___00003W800000">
<testtitle>CHECK INTAKE / EXHAUST SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the intake/exhaust system (See page <xref label="Seep01" href="RM000004KNL00TX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0015" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0015" proc-id="RM22W0E___00003W900000">
<testtitle>CHECK AFTER TREATMENT CONTROL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the after treatment control system (See page <xref label="Seep01" href="RM000004KNM00LX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0016" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0016" proc-id="RM22W0E___00003WA00000">
<testtitle>REPAIR OR REPLACE MALFUNCTIONING PARTS</testtitle>
<content6 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Repair, replace or clean the following items only when it has been determined necessary based on diagnosis for DTC P244B or P2465.</ptxt>
</item>
<item>
<ptxt>Replace the DPF catalytic converter for bank 1 and bank 2 at the same time.</ptxt>
</item>
</list1>
</atten4>
<ptxt>When DTC P244B is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Clean and/or repair the hoses of the differential pressure sensor (for bank 1).</ptxt>
</item>
<item>
<ptxt>Replace the exhaust pipe assembly (for bank 1 DPF catalytic converter) (See page <xref label="Seep01" href="RM000003B0N00HX"/>).</ptxt>
</item>
<item>
<ptxt>Replace the front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter) (See page <xref label="Seep02" href="RM000003B0N00HX"/>).</ptxt>
</item>
</list1>
<ptxt>When DTC P2465 is output:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Clean and/or repair the hoses of the differential pressure sensor (for bank 2).</ptxt>
</item>
<item>
<ptxt>Replace the exhaust pipe assembly (for bank 1 DPF catalytic converter) (See page <xref label="Seep03" href="RM000003B0N00HX"/>).</ptxt>
</item>
<item>
<ptxt>Replace the front No. 2 exhaust pipe assembly (for bank 2 DPF catalytic converter) (See page <xref label="Seep04" href="RM000003B0N00HX"/>).</ptxt>
</item>
</list1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0020" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0020" proc-id="RM22W0E___00003WD00000">
<testtitle>CATALYST RECORD CLEAR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform the catalyst record clear (See page <xref label="Seep01" href="RM000000TIN06OX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0017" proc-id="RM22W0E___00003WB00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the GTS to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the GTS on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK14JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform PM forced regeneration (See page <xref label="Seep01" href="RM00000141502YX_01_0014"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004ID500VX_04_0018" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004ID500VX_04_0018">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>