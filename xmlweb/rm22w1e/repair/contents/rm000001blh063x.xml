<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002I" variety="S002I">
<name>DOOR / HATCH</name>
<ttl id="12066_S002I_7C3XJ_T00QM" variety="T00QM">
<name>BACK DOOR</name>
<para id="RM000001BLH063X" category="A" type-id="80002" name-id="DH3EM-01" from="201301" to="201308">
<name>DISASSEMBLY</name>
<subpara id="RM000001BLH063X_01" type-id="01" category="01">
<s-1 id="RM000001BLH063X_01_0084" proc-id="RM22W0E___0000IBC00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000000UYX0G2X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0025" proc-id="RM22W0E___0000IAN00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32005X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0001" proc-id="RM22W0E___0000IAM00000">
<ptxt>REMOVE BACK DOOR GRIP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a moulding remover, detach the 5 claws and open the cover.</ptxt>
<figure>
<graphic graphicname="B309321" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a T30 "TORX" wrench, remove the 2 screws.</ptxt>
<figure>
<graphic graphicname="B309322" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and remove the back door grip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0071" proc-id="RM22W0E___0000IB400000">
<ptxt>REMOVE LOWER BACK DOOR STOPPER CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and 2 lower back door stopper cushions.</ptxt>
<figure>
<graphic graphicname="B232127" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0003" proc-id="RM22W0E___0000CGP00000">
<ptxt>REMOVE CENTER BACK DOOR GARNISH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 5 clips and 4 claws, and remove the center back door garnish.</ptxt>
<figure>
<graphic graphicname="B313300" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0063" proc-id="RM22W0E___0000IB300000">
<ptxt>REMOVE CENTER STOP LIGHT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the stop light.</ptxt>
<figure>
<graphic graphicname="E156856" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0026" proc-id="RM22W0E___0000CGQ00000">
<ptxt>REMOVE BACK DOOR SIDE GARNISH LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 clips and 2 claws, and remove the back door side garnish LH.</ptxt>
<figure>
<graphic graphicname="B313301" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0027" proc-id="RM22W0E___0000CGS00000">
<ptxt>REMOVE BACK DOOR SIDE GARNISH RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Power Back Door:</ptxt>
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>w/ Power Back Door:</ptxt>
<figure>
<graphic graphicname="B313302" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Detach the clip and 4 claws, and remove the back door side garnish RH.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0073" proc-id="RM22W0E___0000HMX00000">
<ptxt>REMOVE BACK DOOR SERVICE HOLE COVER RH (w/ Power Back Door)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Move the back door to a half-open position so that the hole in the center of the back door service hole cover RH is aligned lengthwise with the power back door rod.</ptxt>
<figure>
<graphic graphicname="B309323E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Back Door Rod</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hole of Back Door Service Hole Cover RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Back Door is Half-open</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 2 clips and separate the back door service hole cover RH, passing the power back door rod through the hole of the back door service hole cover RH.</ptxt>
<figure>
<graphic graphicname="B266396" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>If the back door is in a fully-open position, the power back door rod will interfere with the hole of the back door service hole cover RH, so do not perform this operation with the back door in a fully open position.</ptxt>
</atten3>
<atten4>
<ptxt>If any of the clips have remained on the back door, remove the clips from the back door and install them to the back door service hole cover RH.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the ball joint bolt, power back door rod and back door stay plate.</ptxt>
<figure>
<graphic graphicname="B193888" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the back door service hole cover RH from the power back door rod.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0064" proc-id="RM22W0E___0000CGU00000">
<ptxt>REMOVE ASSIST GRIP (for Face to Face Seat Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and assist grip.</ptxt>
<figure>
<graphic graphicname="B188374" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0065" proc-id="RM22W0E___0000CGV00000">
<ptxt>REMOVE NO. 2 BACK DOOR SERVICE HOLE COVER (for Face to Face Seat Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the No. 2 back door service hole cover. </ptxt>
<figure>
<graphic graphicname="B186428" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0088" proc-id="RM22W0E___0000CGW00000">
<ptxt>REMOVE DOOR OPENING SWITCH SUB-ASSEMBLY (for Face to Face Seat Type)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the door opening switch sub-assembly.</ptxt>
<figure>
<graphic graphicname="B186429" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0004" proc-id="RM22W0E___0000CUB00000">
<ptxt>REMOVE BACK DOOR GARNISH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 14 clips and remove the back door garnish.</ptxt>
<figure>
<graphic graphicname="B181329" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0072" proc-id="RM22W0E___0000CGO00000">
<ptxt>REMOVE REAR HEADER SPEAKER ASSEMBLY (for 14 Speakers)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure as for the opposite side.</ptxt>
</atten4>
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B181526" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the rear header speaker assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0067" proc-id="RM22W0E___0000E6200000">
<ptxt>REMOVE REAR ASSIST GRIP REINFORCEMENT (for Face to Face Seat Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts.</ptxt>
<figure>
<graphic graphicname="B186430" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the rear assist grip reinforcement. </ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0085" proc-id="RM22W0E___0000IBD00000">
<ptxt>REMOVE POWER BACK DOOR SENSOR ASSEMBLY LH (w/ Power Back Door)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B192442" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a T25 "TORX" wrench, remove the 6 screws.</ptxt>
<figure>
<graphic graphicname="B232114" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 clips and remove the power back door sensor assembly LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0075" proc-id="RM22W0E___0000IB500000">
<ptxt>REMOVE POWER BACK DOOR SENSOR ASSEMBLY RH (w/ Power Back Door)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0005" proc-id="RM22W0E___0000HUU00000">
<ptxt>REMOVE REAR WIPER ARM (w/ Rear Wiper)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Open the cover.</ptxt>
<figure>
<graphic graphicname="B181332" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut and rear wiper arm.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0035" proc-id="RM22W0E___0000HUV00000">
<ptxt>REMOVE REAR WIPER MOTOR GROMMET (w/ Rear Wiper)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the rear wiper motor grommet.</ptxt>
<figure>
<graphic graphicname="E156014" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0086" proc-id="RM22W0E___0000HUW00000">
<ptxt>REMOVE REAR WIPER MOTOR ASSEMBLY (w/ Rear Wiper)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B181333" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and rear wiper motor assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0092" proc-id="RM22W0E___0000E6100000">
<ptxt>REMOVE BACK DOOR LOCK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 claws and remove the back door lock cover.</ptxt>
<figure>
<graphic graphicname="B300746" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0093" proc-id="RM22W0E___0000E6000000">
<ptxt>REMOVE BACK DOOR LOCK ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Power Back Door:</ptxt>
<figure>
<graphic graphicname="B181330" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Disconnect the connector.</ptxt>
</s3>
<s3>
<ptxt>Remove the 3 bolts and back door lock assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Power Back Door:</ptxt>
<figure>
<graphic graphicname="B300764" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Disconnect the connector.</ptxt>
</s3>
<s3>
<ptxt>Detach the guide.</ptxt>
</s3>
<s3>
<ptxt>Remove the 4 bolts and back door lock assembly.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0094" proc-id="RM22W0E___0000E6300000">
<ptxt>REMOVE BACK DOOR LOCK PROTECTOR (w/ Power Back Door)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
<figure>
<graphic graphicname="B300763E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 7 claws, and remove the protector (B).</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protector (B)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 2 claws, and remove the protector (A).</ptxt>
<figure>
<graphic graphicname="B300762E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protector (A)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0091" proc-id="RM22W0E___0000IBG00000">
<ptxt>REMOVE BACK DOOR CONTROL SWITCH (w/ Power Back Door)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the back door control switch.</ptxt>
<figure>
<graphic graphicname="B191292" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0059" proc-id="RM22W0E___0000IB000000">
<ptxt>REMOVE LICENSE PLATE LIGHT LENS (for Standard)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws in the order shown in the illustration and remove the license plate light.</ptxt>
<figure>
<graphic graphicname="E155855E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0087" proc-id="RM22W0E___0000IBE00000">
<ptxt>REMOVE BACK DOOR OPENER SWITCH ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws.</ptxt>
<figure>
<graphic graphicname="B184339" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector and then remove the back door opener switch assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0055" proc-id="RM22W0E___0000CUA00000">
<ptxt>REMOVE REAR TELEVISION CAMERA ASSEMBLY (w/ Parking Assist Monitor System or Rear View Monitor System)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180274" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T30 "TORX" socket wrench, remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the rear television camera assembly.</ptxt>
<figure>
<graphic graphicname="B180789" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0031" proc-id="RM22W0E___0000IAO00000">
<ptxt>REMOVE LIFT GATE WEATHERSTRIP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 25 clips and remove the lift gate weatherstrip.</ptxt>
<figure>
<graphic graphicname="B181334" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not pull strongly on the lift gate weatherstrip as it may tear.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0089" proc-id="RM22W0E___0000IBF00000">
<ptxt>REMOVE CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 cushions.</ptxt>
<figure>
<graphic graphicname="B232921" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0032" proc-id="RM22W0E___0000IAP00000">
<ptxt>REMOVE LOWER BACK DOOR STOPPER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and lower back door stopper.</ptxt>
<figure>
<graphic graphicname="B192431" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0060" proc-id="RM22W0E___0000IB100000">
<ptxt>REMOVE BACK DOOR OUTSIDE GARNISH SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the back door garnish.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 clips and remove the double-sided tape to remove the back door garnish.</ptxt>
<figure>
<graphic graphicname="B181925E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0077" proc-id="RM22W0E___0000HUZ00000">
<ptxt>REMOVE BACK DOOR GLASS CHANNEL LH (w/o Power Back Door)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B189693E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a clip remover, remove the clip.</ptxt>
</s2>
<s2>
<ptxt>Remove the back door glass channel LH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0078" proc-id="RM22W0E___0000IB600000">
<ptxt>REMOVE BACK DOOR GLASS CHANNEL RH (w/o Power Back Door)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0090" proc-id="RM22W0E___0000HUT00000">
<ptxt>REMOVE REAR SPOILER SUB-ASSEMBLY (w/ Rear Spoiler)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Power Back Door:</ptxt>
<figure>
<graphic graphicname="B193121" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the 2 hole plugs and 4 bolts.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/o Power Back Door:</ptxt>
<figure>
<graphic graphicname="B180983" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 clips and remove the rear spoiler sub-assembly.</ptxt>
<figure>
<graphic graphicname="B180984" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0079" proc-id="RM22W0E___0000IB700000">
<ptxt>REMOVE REAR WASHER NOZZLE (w/ Rear Wiper)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the washer hose.</ptxt>
<figure>
<graphic graphicname="E156018" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the rear washer nozzle.</ptxt>
<figure>
<graphic graphicname="E156019" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0061" proc-id="RM22W0E___0000IB200000">
<ptxt>REMOVE BACK DOOR STAY ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and stay.</ptxt>
<figure>
<graphic graphicname="B181318" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Remove the door stay while supporting the back door with one hand.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Remove the bolt and stay.</ptxt>
<figure>
<graphic graphicname="B184292" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Remove the door stay while supporting the back door with one hand.</ptxt>
</atten2>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0040" proc-id="RM22W0E___0000IAQ00000">
<ptxt>REMOVE BACK DOOR STAY ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0056" proc-id="RM22W0E___0000CG200000">
<ptxt>REMOVE REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181672" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 6 clips and remove the support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0041" proc-id="RM22W0E___0000EZM00000">
<ptxt>REMOVE BACK DOOR TRIM COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the back door trim cover LH as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B313304" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0042" proc-id="RM22W0E___0000EZN00000">
<ptxt>REMOVE BACK DOOR TRIM COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the back door trim cover RH as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B313305" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0043" proc-id="RM22W0E___0000CGR00000">
<ptxt>REMOVE BACK DOOR TRIM PANEL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
<figure>

<graphic graphicname="B181336" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 16 clips and remove the back door trim panel assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0080" proc-id="RM22W0E___0000IB800000">
<ptxt>REMOVE NO. 2 BACK DOOR OUTSIDE GARNISH SUB-ASSEMBLY (w/ Tire Carrier)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B186238" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 nuts and disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Put protective tape around the No. 2 back door garnish.</ptxt>
</s2>
<s2>
<ptxt>Detach the 8 clips.</ptxt>
</s2>
<s2>
<ptxt>Pull out the wire harness of the license plate light from the tail gate, and remove the No. 2 back door garnish.</ptxt>
<figure>
<graphic graphicname="B186239E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0081" proc-id="RM22W0E___0000IB900000">
<ptxt>REMOVE LICENSE PLATE LIGHT ASSEMBLY (w/ Tire Carrier)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and 2 lights.</ptxt>
<figure>
<graphic graphicname="E156994" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0082" proc-id="RM22W0E___0000IBA00000">
<ptxt>REMOVE REAR LICENSE LIGHT COVER (w/ Tire Carrier)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B188295E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the rear license light cover.</ptxt>
</s2>
<s2>
<ptxt>Detach the 7 claws and remove the double-sided tape to remove the rear license light cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001BLH063X_01_0049" proc-id="RM22W0E___0000IAV00000">
<ptxt>REMOVE REAR BUMPER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Standard:</ptxt>
<s3>
<ptxt>Remove the rear bumper assembly (See page <xref label="Seep01" href="RM0000038K201JX"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Towing Hitch:</ptxt>
<s3>
<ptxt>Remove the rear bumper assembly (See page <xref label="Seep02" href="RM0000038K201KX"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Pintle Hook:</ptxt>
<s3>
<ptxt>Remove the rear bumper assembly (See page <xref label="Seep03" href="RM0000038K201LX"/>).</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0062">
<ptxt>REMOVE SPARE TIRE</ptxt>
</s-1>
<s-1 id="RM000001BLH063X_01_0050" proc-id="RM22W0E___0000IAW00000">
<ptxt>REMOVE LOWER BACK DOOR TORSION BAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and lower back door torsion bar assembly.</ptxt>
<figure>
<graphic graphicname="B309324" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0051" proc-id="RM22W0E___0000IAX00000">
<ptxt>REMOVE BACK DOOR TORSION BAR GUIDE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the back door torsion bar guide.</ptxt>
<figure>
<graphic graphicname="B181476" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0044" proc-id="RM22W0E___0000IAR00000">
<ptxt>REMOVE BACK DOOR INSIDE HANDLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw and back door inside handle.</ptxt>
<figure>
<graphic graphicname="B181339E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Back Door Handle Grommet</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the back door handle grommet.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0047" proc-id="RM22W0E___0000IAU00000">
<ptxt>REMOVE LOWER TAIL GATE LOCK ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Power Back Door:</ptxt>
<s3>
<ptxt>Using a T30 "TORX" wrench, remove the 3 screws and lower tail gate lock assembly RH.</ptxt>
<figure>
<graphic graphicname="B184140" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the cable from the lower tail gate lock assembly RH.</ptxt>
<figure>
<graphic graphicname="B184137" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Power Back Door:</ptxt>
<s3>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B193800" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Using a T30 "TORX" wrench, remove the 4 screws and lower tail gate lock assembly RH.</ptxt>
<figure>
<graphic graphicname="B232132" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0045" proc-id="RM22W0E___0000IAS00000">
<ptxt>REMOVE BACK DOOR REMOTE CONTROL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Power Back Door:</ptxt>
<s3>
<ptxt>Remove the 2 screws.</ptxt>
<figure>
<graphic graphicname="B184087" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the left side cable.</ptxt>
<figure>
<graphic graphicname="B181340" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the back door remote control assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Power Back Door:</ptxt>
<s3>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B191287" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the 2 screws and back door remote control assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0046" proc-id="RM22W0E___0000IAT00000">
<ptxt>REMOVE LOWER TAIL GATE LOCK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Power Back Door:</ptxt>
<s3>
<ptxt>Using a T30 "TORX" wrench, remove the 3 screws and lower tail gate lock assembly LH.</ptxt>
<figure>
<graphic graphicname="B181341" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Power Back Door:</ptxt>
<s3>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B191286" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Using a T30 "TORX" wrench, remove the 4 screws and lower tail gate lock assembly LH.</ptxt>
<figure>
<graphic graphicname="B232185" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0052" proc-id="RM22W0E___0000IAY00000">
<ptxt>REMOVE TAIL GATE STAY SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T40 "TORX" socket, remove the 2 screws and tail gate stay sub-assembly LH.</ptxt>
<figure>
<graphic graphicname="B181343" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0053" proc-id="RM22W0E___0000IAZ00000">
<ptxt>REMOVE TAIL GATE STAY SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000001BLH063X_01_0083" proc-id="RM22W0E___0000IBB00000">
<ptxt>REMOVE BACK DOOR DAMPER ASSEMBLY (w/ Power Back Door)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and back door damper assembly.</ptxt>
<figure>
<graphic graphicname="B191284" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>