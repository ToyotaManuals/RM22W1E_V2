<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UI_T00NL" variety="T00NL">
<name>FRONT POWER SEAT CONTROL SYSTEM (w/ Seat Position Memory System)</name>
<para id="RM000002RB203EX" category="J" type-id="300RO" name-id="SE6TN-02" from="201301">
<dtccode/>
<dtcname>Power Seat Position is not Memorized</dtcname>
<subpara id="RM000002RB203EX_01" type-id="60" category="03" proc-id="RM22W0E___0000FRR00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the seat memory SET switch and a seat memory switch (seat memory switch 1, seat memory switch 2 or seat memory switch 3) are pressed simultaneously, the outer mirror control ECU commands the position control ECU and switch through CAN communication to record each position value.</ptxt>
</content5>
</subpara>
<subpara id="RM000002RB203EX_02" type-id="32" category="03" proc-id="RM22W0E___0000FRS00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B183720E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002RB203EX_03" type-id="51" category="05" proc-id="RM22W0E___0000FRT00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>First perform the communication function inspections in How to Proceed with Troubleshooting to confirm that there are no CAN communication malfunctions before troubleshooting this symptom.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002RB203EX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002RB203EX_04_0001" proc-id="RM22W0E___0000FRU00000">
<testtitle>CHECK FRONT POWER SEAT CONTROL FUNCTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that each function of the power seat operates normally by using the front power seat switches.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Each function of power seat operates normally by using seat switches.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RB203EX_04_0002" fin="false">OK</down>
<right ref="RM000002RB203EX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RB203EX_04_0002" proc-id="RM22W0E___0000FRV00000">
<testtitle>CHECK SEAT MEMORY SWITCH FUNCTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform a memory operation properly. Check that the buzzer sounds to indicate the completion of the memory operation.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The seat position will not be recorded if the seat memory SET switch and 2 or more of the seat memory switches (for example, seat memory switch 1 and seat memory switch 2) are pressed simultaneously.</ptxt>
</item>
<item>
<ptxt>If a memorizing operation has failed, release all switches. The seat memory function does not operate unless the switches are released.</ptxt>
</item>
</list1>
</atten3>
<spec>
<title>OK</title>
<specitem>
<ptxt>Seat memory switch function operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RB203EX_04_0010" fin="true">OK</down>
<right ref="RM000002RB203EX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RB203EX_04_0003" proc-id="RM22W0E___0000FRW00000">
<testtitle>CHECK COMBINATION METER FUNCTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When the engine switch is on (IG) and the shift lever is in P, check that the combination meter shows that the shift lever is in P.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Meter shows that shift lever is in P.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RB203EX_04_0004" fin="false">OK</down>
<right ref="RM000002RB203EX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RB203EX_04_0004" proc-id="RM22W0E___0000FRX00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (SEAT MEMORY SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the Data List for proper functioning of the seat memory switch.</ptxt>
<table pgwide="1">
<title>Driver Seat</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>SET Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat memory SET switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Seat memory SET switch on</ptxt>
<ptxt>OFF: Seat memory SET switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>M1 Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat memory switch 1 signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Seat memory switch 1 on</ptxt>
<ptxt>OFF: Seat memory switch 1 off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>M2 Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat memory switch 2 signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Seat memory switch 2 on</ptxt>
<ptxt>OFF: Seat memory switch 2 off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>M3 Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat memory switch 3 signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Seat memory switch 3 on</ptxt>
<ptxt>OFF: Seat memory switch 3 off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<spec>
<title>OK</title>
<specitem>
<ptxt>On intelligent tester screen, each item changes between ON and OFF according to above chart.</ptxt>
</specitem>
</spec>
</content6>
<res>
<down ref="RM000002RB203EX_04_0012" fin="true">OK</down>
<right ref="RM000002RB203EX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RB203EX_04_0005" proc-id="RM22W0E___0000FRY00000">
<testtitle>INSPECT SEAT MEMORY SWITCH</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B179859E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Remove the seat memory switch (See page <xref label="Seep01" href="RM00000392X00JX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>4 (M1) - 6 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Seat memory switch 1 pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3 (M2) - 6 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Seat memory switch 2 pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (M3) - 6 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Seat memory switch 3 pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5 (MRY) - 6 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Seat memory SET switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RB203EX_04_0006" fin="false">OK</down>
<right ref="RM000002RB203EX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RB203EX_04_0006" proc-id="RM22W0E___0000FRZ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (OUTER MIRROR CONTROL ECU - SEAT MEMORY SWITCH)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B179860E02" width="2.775699831in" height="4.7836529in"/>
</figure>
<test1>
<ptxt>Disconnect the I18*1 or I24*2 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the X1*1 or X2*2 switch connector.</ptxt>
<atten4>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I18-8 (M1) - X1-4 (M1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-7 (M2) - X1-3 (M2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-6 (M3) - X1-2 (M3)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-9 (MM) - X1-5 (MRY)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-10 (MSWE) - X1-6 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-8 (M1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-7 (M2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-6 (M3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-9 (MM) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I18-10 (MSWE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I24-8 (M1) - X2-4 (M1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-7 (M2) - X2-3 (M2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-6 (M3) - X2-2 (M3)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-9 (MM) - X2-5 (MRY)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-10 (MSWE) - X2-6 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-8 (M1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-7 (M2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-6 (M3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-9 (MM) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I24-10 (MSWE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RB203EX_04_0007" fin="false">OK</down>
<right ref="RM000002RB203EX_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RB203EX_04_0007" proc-id="RM22W0E___0000FS000000">
<testtitle>CHECK OUTER MIRROR CONTROL ECU (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>After replacing the outer mirror control ECU, check that the front power seat control functions operate normally using the front seat switches and seat memory switch.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Front power seat control functions operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002RB203EX_04_0008" fin="true">OK</down>
<right ref="RM000002RB203EX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002RB203EX_04_0008">
<testtitle>END (REPLACE OUTER MIRROR CONTROL ECU)<xref label="Seep01" href="RM0000039C400JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002RB203EX_04_0009">
<testtitle>GO TO PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000039RM039X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002RB203EX_04_0010">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000002RB203EX_04_0011">
<testtitle>GO TO METER / GAUGE SYSTEM<xref label="Seep01" href="RM000002Z4L03NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002RB203EX_04_0012">
<testtitle>REPLACE POSITION CONTROL ECU AND SWITCH<xref label="Seep01" href="RM00000390T01LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002RB203EX_04_0013">
<testtitle>REPLACE SEAT MEMORY SWITCH<xref label="Seep01" href="RM00000392X00JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002RB203EX_04_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>