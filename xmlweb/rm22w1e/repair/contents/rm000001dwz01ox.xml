<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001L" variety="S001L">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001L_7C3PI_T00IL" variety="T00IL">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000001DWZ01OX" category="D" type-id="303FK" name-id="BCEHJ-01" from="201301">
<name>CALIBRATION</name>
<subpara id="RM000001DWZ01OX_z0" proc-id="RM22W0E___0000ANM00000">
<content5 releasenbr="1">
<step1>
<ptxt>DESCRIPTION</ptxt>
<ptxt>After replacing the relevant ABS components, clear the sensor calibration data and then perform calibration.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Replacement/Adjustment Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Necessary Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Skid control ECU (Master cylinder solenoid)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Deceleration sensor zero point calibration</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Deceleration sensor</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Clearing zero point calibration data</ptxt>
</item>
<item>
<ptxt>Deceleration sensor zero point calibration</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>CLEAR ZERO POINT CALIBRATION (SST CHECK WIRE)</ptxt>
<ptxt>After replacing the deceleration sensor, make sure to clear the zero point calibration data in the skid control ECU (master cylinder solenoid) and perform zero point calibration.</ptxt>
<step2>
<ptxt>Turn the ignition switch to ON. </ptxt>
</step2>
<step2>
<ptxt>Using SST, connect and disconnect terminals 12 (TS) and 4 (CG) of the DLC3 4 times or more within 8 seconds.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C146309E15" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Check that the ABS warning light comes on.</ptxt>
</step2>
<step2>
<ptxt>Using a check wire, perform zero point calibration of the deceleration sensor. </ptxt>
</step2>
</step1>
<step1>
<ptxt>PERFORM ZERO POINT CALIBRATION OF DECELERATION SENSOR (SST CHECK WIRE)</ptxt>
<ptxt>After replacing the master cylinder solenoid and/or deceleration sensor, make sure to perform deceleration sensor zero point calibration.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>While obtaining the zero point, do not vibrate the vehicle by tilting, moving or shaking it. Keep it stationary and do not start the engine. </ptxt>
</item>
<item>
<ptxt>Choose a level surface with an inclination of less than 1°.</ptxt>
</item>
</list1>
</atten3>
<step2>
<ptxt>Procedures for Test Mode:</ptxt>
<step3>
<ptxt>Turn the ignition switch off. </ptxt>
</step3>
<step3>
<ptxt>for A/T:</ptxt>
<ptxt>Move the shift lever to P.</ptxt>
<atten3>
<ptxt>DTC 98 will be stored if the shift lever is not in P.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>for M/T:</ptxt>
<ptxt>Pull the parking brake lever.</ptxt>
<atten3>
<ptxt>DTC 98 will be stored if the parking brake lever is not pulled.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>Using SST, connect terminals 12 (TS) and 4 (CG) of the DLC3.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C146309E15" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Check that the steering wheel is centered.</ptxt>
</step3>
<step3>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step3>
<step3>
<ptxt>Keep the vehicle stationary on a level surface for 2 seconds or more.</ptxt>
</step3>
<step3>
<ptxt>Check that the ABS warning light is blinking in test mode pattern (0.125 seconds on and 0.125 seconds off).</ptxt>
<atten3>
<ptxt>The ABS warning light stays on when obtaining the zero point.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the ABS warning light does not blink, perform the zero point calibration again.</ptxt>
</item>
<item>
<ptxt>The zero point calibration is performed only once after the system enters test mode.</ptxt>
</item>
<item>
<ptxt>Calibration cannot be performed again until the stored data is cleared once.</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>CLEAR ZERO POINT CALIBRATION (GTS) </ptxt>
<ptxt>After replacing the deceleration sensor, make sure to clear the zero point calibration data in the skid control ECU (master cylinder solenoid) and perform zero point calibration.</ptxt>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the GTS to the DLC3. </ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON. </ptxt>
</step2>
<step2>
<ptxt>Turn the GTS on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Utility / Reset Memory.</ptxt>
</step2>
<step2>
<ptxt>Using the GTS, perform zero point calibration of the deceleration sensor.</ptxt>
</step2>
</step1>
<step1>
<ptxt>PERFORM ZERO POINT CALIBRATION OF DECELERATION SENSOR (GTS)</ptxt>
<ptxt>After replacing the master cylinder solenoid and/or deceleration sensor, make sure to perform deceleration sensor zero point calibration.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>While obtaining the zero point, do not vibrate the vehicle by tilting, moving or shaking it. Keep it stationary and do not start the engine. </ptxt>
</item>
<item>
<ptxt>Choose a level surface with an inclination of less than 1°.</ptxt>
</item>
</list1>
</atten3>
<step2>
<ptxt>Procedures for Test Mode:</ptxt>
<step3>
<ptxt>Turn the ignition switch off.</ptxt>
</step3>
<step3>
<ptxt>for A/T:</ptxt>
<ptxt>Move the shift lever to P.</ptxt>
<atten3>
<ptxt>DTC C1336 will be stored if the shift lever is not in P.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>for M/T:</ptxt>
<ptxt>Pull the parking brake lever.</ptxt>
<atten3>
<ptxt>DTC C1336 will be stored if the parking brake lever is not pulled.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>Check that the steering wheel is centered.</ptxt>
</step3>
<step3>
<ptxt>Connect the GTS to the DLC3. </ptxt>
</step3>
<step3>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step3>
<step3>
<ptxt>Turn the GTS on.</ptxt>
</step3>
<step3>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Utility / Test Mode.</ptxt>
</step3>
<step3>
<ptxt>Keep the vehicle stationary on a level surface for 2 seconds or more. </ptxt>
</step3>
<step3>
<ptxt>Check that the ABS warning light is blinking in test mode pattern (0.125 seconds on and 0.125 seconds off).</ptxt>
<atten3>
<ptxt>The ABS warning light stays on when obtaining the zero point.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the ABS warning light does not blink, perform the zero point calibration again.</ptxt>
</item>
<item>
<ptxt>The zero point calibration is performed only once after the system enters test mode.</ptxt>
</item>
<item>
<ptxt>Calibration cannot be performed again until the stored data is cleared once.</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>