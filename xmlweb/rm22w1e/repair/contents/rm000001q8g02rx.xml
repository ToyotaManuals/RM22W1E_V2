<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001M" variety="S001M">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001M_7C3PN_T00IQ" variety="T00IQ">
<name>BRAKE PEDAL (for RHD)</name>
<para id="RM000001Q8G02RX" category="A" type-id="30014" name-id="BR5TE-03" from="201308">
<name>INSTALLATION</name>
<subpara id="RM000001Q8G02RX_01" type-id="01" category="01">
<s-1 id="RM000001Q8G02RX_01_0028" proc-id="RM22W0E___0000ATQ00001">
<ptxt>INSTALL BRAKE PEDAL SUPPORT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the brake pedal support assembly with the 4 nuts.</ptxt>
<figure>
<graphic graphicname="C172861" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>145</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the brake pedal support reinforcement set bolt.</ptxt>
<figure>
<graphic graphicname="C229351" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>163</t-value2>
<t-value4>12</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the hexagon bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>23</t-value1>
<t-value2>239</t-value2>
<t-value4>17</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Hold the bolt in place and tighten the nut.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q8G02RX_01_0030" proc-id="RM22W0E___0000ATR00001">
<ptxt>INSTALL PUSH ROD PIN</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat lithium soap base glycol grease to the inner surface of the hole on the brake pedal lever.</ptxt>
</s2>
<s2>
<ptxt>Set the master cylinder push rod clevis in place, insert the push rod pin from the outside of the vehicle and then install a new clip.</ptxt>
<figure>
<graphic graphicname="C172862" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q8G02RX_01_0043" proc-id="RM22W0E___0000ATS00001">
<ptxt>INSTALL STOP LIGHT SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the stop light switch (See page <xref label="Seep01" href="RM0000038XM00HX_01_0001"/>).</ptxt>
</s2>
<s2>
<ptxt>Connect the stop light switch connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q8G02RX_01_0048" proc-id="RM22W0E___0000AT300001">
<ptxt>INSTALL DRIVER SIDE KNEE AIRBAG ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B189604E01" width="2.775699831in" height="6.791605969in"/>
</figure>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the driver side knee airbag with the 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8G02RX_01_0049" proc-id="RM22W0E___0000A9I00001">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B180296" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for Automatic Air Conditioning System:</ptxt>
<s3>
<ptxt>Attach the 2 claws to install the room temperature sensor.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B181942" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to connect the 2 control cables.</ptxt>
</s2>
<s2>
<ptxt>w/ Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 16 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291249" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Driver Side Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 9 claws to install the lower No. 1 instrument panel finish panel.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B291250" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B182569" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 2 claws to close the hole cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8G02RX_01_0050" proc-id="RM22W0E___000014600001">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B182553" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 3 claws to install the No. 1 instrument panel under cover.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8G02RX_01_0047" proc-id="RM22W0E___0000ATT00001">
<ptxt>INSTALL COWL SIDE TRIM BOARD RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B180022" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the 2 clips to install the cowl side trim board.</ptxt>
</s2>
<s2>
<ptxt>Install the cap nut.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8G02RX_01_0051" proc-id="RM22W0E___0000ATU00001">
<ptxt>INSTALL FRONT DOOR SCUFF PLATE RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM000001Q8G02RX_01_0053" proc-id="RM22W0E___0000ATV00001">
<ptxt>WARM UP CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003C32006X"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001Q8G02RX_01_0044" proc-id="RM22W0E___0000ATN00000">
<ptxt>CHECK BRAKE PEDAL HEIGHT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the brake pedal height.</ptxt>
<figure>
<graphic graphicname="C222463E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Pedal Height from No. 2 Dash Panel Insulator Pad</title>
<specitem>
<ptxt>149.1 to 159.1 mm (5.87 to 6.26 in.)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not adjust the pedal height. Doing so by changing the push rod length will structurally change the pedal ratio.</ptxt>
</atten3>
<ptxt>If the pedal height is incorrect, adjust the rod operating adapter length.</ptxt>
</s2>
<s2>
<ptxt>Adjust the rod operating adapter length.</ptxt>
<figure>
<graphic graphicname="C172191E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Remove the clip and clevis pin.</ptxt>
</s3>
<s3>
<ptxt>Loosen the clevis lock nut.</ptxt>
</s3>
<s3>
<ptxt>Adjust the rod operating adapter length by turning the pedal push rod clevis.</ptxt>
<spec>
<title>Standard Rod Operating Adapter Length "A"</title>
<specitem>
<ptxt>201.7 to 202.7 mm (7.94 to 7.98 in.)</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Tighten the clevis lock nut.</ptxt>
<torque>
<torqueitem>
<t-value1>26</t-value1>
<t-value2>260</t-value2>
<t-value4>19</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Set the master cylinder push rod clevis in place, insert the push rod pin from the outside of the vehicle and then install a new clip. If the pedal height is incorrect even if the rod operating adapter is adjusted, check that there is no damage in the brake pedal, brake pedal lever, brake pedal support and dash panel.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Even if there is damage, there is no problem if the reserve distance is within the standard value.</ptxt>
</item>
<item>
<ptxt>If necessary, replace any damaged parts.</ptxt>
</item>
</list1>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8G02RX_01_0045" proc-id="RM22W0E___0000ATM00000">
<ptxt>CHECK PEDAL FREE PLAY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Push in the pedal until the beginning of the resistance is felt. Measure the pedal free play.</ptxt>
<figure>
<graphic graphicname="C172192E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Pedal Free Play</title>
<specitem>
<ptxt>1 to 6 mm (0.0394 to 0.236 in.)</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8G02RX_01_0046" proc-id="RM22W0E___0000ATP00000">
<ptxt>CHECK PEDAL RESERVE DISTANCE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Release the parking brake lever.</ptxt>
<figure>
<graphic graphicname="C222462E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>With the engine running, depress the pedal and measure the pedal reserve distance as shown in the illustration.</ptxt>
<spec>
<title>Standard Pedal Reserve Distance from No. 2 Dash Panel Insulator Pad at 490 N (50 kgf, 110.2 lbf)</title>
<specitem>
<ptxt>More than 67.5 mm (2.66 in.)</ptxt>
</specitem>
</spec>
<ptxt>If incorrect, troubleshoot the brake system.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>