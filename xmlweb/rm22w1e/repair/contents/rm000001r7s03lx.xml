<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM22W1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S002B" variety="S002B">
<name>SEAT</name>
<ttl id="12059_S002B_7C3UJ_T00NM" variety="T00NM">
<name>FRONT POWER SEAT CONTROL SYSTEM (w/o Seat Position Memory System)</name>
<para id="RM000001R7S03LX" category="D" type-id="303FF" name-id="SE27N-02" from="201301">
<name>OPERATION CHECK</name>
<subpara id="RM000001R7S03LX_z0" proc-id="RM22W0E___0000FSU00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK POWER SEAT FUNCTION</ptxt>
<figure>
<graphic graphicname="B179853E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<step2>
<ptxt>Check the basic functions.</ptxt>
<step3>
<ptxt>Operate the power seat switches and check to make sure each seat function works:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Sliding</ptxt>
</item>
<item>
<ptxt>Front vertical</ptxt>
</item>
<item>
<ptxt>Lifter</ptxt>
</item>
<item>
<ptxt>Reclining</ptxt>
</item>
<item>
<ptxt>Lumbar support (driver seat only)</ptxt>
</item>
</list1>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK POWER SEAT MOTOR ASSEMBLY (SLIDING, FRONT VERTICAL, LIFTER, AND RECLINING FUNCTIONS)</ptxt>
<step2>
<ptxt>Check the PTC operation inside the power seat motor.</ptxt>
<atten4>
<ptxt>The resistance of the PTC thermistor increases when the power seat switch is held down even after the seat has been moved to the farthest possible position in one direction. If the resistance increases beyond a specified level, the current is shut off to prevent a short circuit.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The inspection should be performed with the seat installed in the vehicle.</ptxt>
</item>
<item>
<ptxt>Perform the 4 steps below to check the full range of motion for each power seat function.</ptxt>
</item>
</list1>
</atten3>
<step3>
<ptxt>Choose a power seat function. Operate the power seat switch and move the seat to the farthest possible position in one direction. Keep the seat in that position for approximately 60 seconds.</ptxt>
</step3>
<step3>
<ptxt>Operate the power seat switch again and continue to try to move the seat in the same direction as in the previous step. Measure the time until the current is shut off (motor operation sound will stop).</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>4 to 90 seconds</ptxt>
</specitem>
</spec>
</step3>
<step3>
<ptxt>After the current is shut off, release the power seat switch and wait for approximately 60 seconds.</ptxt>
</step3>
<step3>
<ptxt>Operate the same power seat switch and move the seat in the opposite direction. Check that the motor operates.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK LUMBAR SUPPORT ADJUSTER ASSEMBLY</ptxt>
<step2>
<ptxt>Check the PTC operation inside the power seat motor.</ptxt>
<atten3>
<ptxt>The inspection should be performed with the seat installed in the vehicle.</ptxt>
</atten3>
<step3>
<ptxt>Operate the lumbar support switch and move the lumbar support to either the foremost or rearmost position. Keep the seat in that position for approximately 60 seconds.</ptxt>
</step3>
<step3>
<ptxt>Operate the lumbar support switch again and continue to try to move the lumbar support in the same direction as in the previous step. Measure the time until the current is shut off (motor operation sound will stop).</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>4 to 90 seconds</ptxt>
</specitem>
</spec>
</step3>
<step3>
<ptxt>After the current is shut off, release the lumbar support switch and wait for approximately 60 seconds.</ptxt>
</step3>
<step3>
<ptxt>Operate the lumbar support switch and move the seat in the opposite direction. Check that the motor operates.</ptxt>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>