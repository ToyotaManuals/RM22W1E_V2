/*
   Copyright (c) 2003 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/


var SVG_BOTTON_LEFT = 0;
var SVG_BOTTON_RIGHT = 2;

function processZoomEvent_svg(evt)
{
	try{
		var svgdoc = evt.target.ownerDocument;
		if( svgdoc.rootElement.currentScale == 1 )	return;
		zoomFig( svgdoc );
	}
	catch(e){
		window.alert(e.stack);
	}
}

function processPanEvent_svg(evt)
{
	try{

		var currentPos = evt.target.ownerDocument.rootElement.getCurrentTranslate();
		currentPos.setX(0);
		currentPos.setY(0);
	}
	catch(e){
		window.alert(e.stack);
	}
}

function processMouseDownEvent_svg(evt)
{
	
}
