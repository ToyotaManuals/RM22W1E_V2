class Rect {

constructor(x, y, width, height )
{
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
}

setRect( x, y, width, height )
{
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
}

setSize( width, height )
{
	this.width = width;
	this.height = height;
}

setLocation( x, y )
{
	this.x = x;
	this.y = y;
}

translate( x, y )
{
	this.x += x;
	this.y += y;
}

}

class SVGFig {

constructor()
{
	this.embedNode = null;
	this.svgRootNode = null;
	this.width = 0;
	this.heigth = 0;
}


getOriginalSize()
{
	try{
		return new Rect(0, 0, this.svgRootNode.getAttribute("width"), this.svgRootNode.getAttribute("height") );
	}
	catch(e){
		return null;
	}
}

setFigData(doc)
{
	this.svgRootNode = doc.rootElement;
	this.height = this.svgRootNode.getAttribute("height");
	this.width = this.svgRootNode.getAttribute("width");
}

setSize(width, height)
{
	try{
		this.embedNode.setAttribute("width", width);
		this.embedNode.setAttribute("height", height);

		this.width = width;
		this.height = height;
		return true;
	}
	catch(e){
		window.alert(e.stack);
		return false;
	}

}

setEmbedNode( node )
{
	this.embedNode = node;
}

addEventListener()
{
	try{
		this.svgRootNode.addEventListener("SVGZoom", window.parent.processZoomEvent_svg, false);

		return true;
	}
	catch(e){
		window.alert("EWD Viewer: event error occured");
		return false;
	}
}

addFigCtrlEventListener()
{
	try{
		this.svgRootNode.addEventListener("SVGScroll", window.parent.processPanEvent_svg, false);
		this.svgRootNode.addEventListener("mousedown", window.parent.processMouseDownEvent_svg, false);

		return true;
	}
	catch(e){
		return false;
	}
}
}

function call_execInitLoadProc_svg(type)
{
	var svgFig = new SVGFig();
	svgFig.setFigData(document);

	svgFig.addEventListener();

		svgFig.addFigCtrlEventListener();

	window.parent.sendOnloadMsg(svgFig);
}

