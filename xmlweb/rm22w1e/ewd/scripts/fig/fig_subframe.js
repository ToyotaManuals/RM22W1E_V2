/*
   Copyright (c) 2002 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/






function call_runOnLoadProc()
{

	try{
		loadHead();
		loadFigCtrl();
		loadFigMap();
	}
	catch( e ){
		window.alert(e.stack);
	}
}


function loadHead()
{
	d_header.src = d_header.src.replace(/[^/]*$/, '') + "../common/figtitle.html" + window.location.search;
}

function loadFigCtrl()
{
	d_figctrl.src = d_figctrl.src.replace(/[^/]*$/, '') + "../common/figctrl.html" + window.location.search;
}

function loadFigMap()
{
	d_figmap.src = d_figmap.src.replace(/[^/]*$/, '') + "../common/figmap.html" + window.location.search;
}
