/*
   Copyright (c) 2002 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/






function showPrintWindow()
{
	if( parent.d_mainframe.contentWindow.getMode == null ) return;
	var mode = parent.d_mainframe.contentWindow.getMode();
	
	switch (mode) {
		case "Intro":
			showPrintDialogIntro();
			break;
		
		case "ConnectorList":
			showPrintDialogConn();
			break;
		
		case "Loads":
			showPrintDialogLoads();
			break;
			
		case "FigMap":
			openPDF();
			break;
			
		default:
			break;
	}
}




function showPrintDialogIntro()
{
	var name = "intro";
	var bodyHTML = parent.d_mainframe.contentWindow.d_contents.contentWindow.document.body.innerHTML;
	
	openNewPrintDialog( name, bodyHTML );
}





function showPrintDialogConn()
{

	parent.d_mainframe.contentWindow.d_info.setHighlightPinInfo();
	
	var name = "parts";
	var bodyHTML = parent.d_mainframe.contentWindow.d_info.contentWindow.document.body.innerHTML;
	
	openNewPrintDialog( name, bodyHTML );
}




function showPrintDialogLoads()
{
	var name = "loads_ps";

	var bodyHTML = parent.d_mainframe.contentWindow.getPrintBodyHTML();
	
	openNewPrintDialog( name, bodyHTML );
}





function openPDF()
{
	var titleObj = parent.d_mainframe.contentWindow.getTitleObj();

	var fileName = titleObj.getPdfFileName();
	var ewdType = titleObj.getType();
	fileName = "./" + ewdType + "/pdf/" + fileName;

	

	doPrintPDF( fileName );
}







function getPrintDialogHeader()
{
	var xmlDoc = loadXML("../styles/common/print_head.xml");
	return xmlDoc.xml;
}
