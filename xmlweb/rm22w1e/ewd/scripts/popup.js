/*
   Copyright (c) 2002 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/



function call_runOnLoadProc()
{
	try{
		d_main.src = d_main.src.replace(/[^/]*$/, '') + "./index.html" + window.location.search;
	}
	catch( e ){
		window.alert(e.stack);
	}
}
