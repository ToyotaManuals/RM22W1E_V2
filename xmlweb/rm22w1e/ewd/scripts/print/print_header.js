/*
   Copyright (c) 2002-2007 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/


function call_runOnClickClose()
{
	try{
		window.top.document.getElementsByTagName('dialog')[0].close();
	}
	catch( e ){
		window.alert(e.stack);
	}
}


function call_runOnClickPrint()
{
	try{
		parent.d_preview.contentWindow.focus();

		parent.d_preview.contentWindow.print();
	}
	catch( e ){
		window.alert(e.stack);
	}
}
