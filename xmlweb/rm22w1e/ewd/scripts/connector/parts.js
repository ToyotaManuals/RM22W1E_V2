/*
   Copyright (c) 2002 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/



var MODE = "ConnectorList";




function getMode()
{
	return MODE;
}


function call_runOnLoadProc()
{
	try{

		if( parent != self ){
			parent.showGlobalNavi( true );
		}
		
		d_search.src = d_search.src.replace(/[^/]*$/, '') + "psearch.html" + window.location.search;
		d_list.src = d_list.src.replace(/[^/]*$/, '') + "partslist.html" + window.location.search;
		d_info.src = d_info.src.replace(/[^/]*$/, '') + "partsref.html" + window.location.search;
	}
	catch( e ){
		window.alert(e.stack);
	}
}



function showNameList()
{
	d_list.src = d_list.src.replace(/[^/]*$/, '') + "partslist_name.html";
}

function showCodeList()
{
	d_list.src = d_list.src.replace(/[^/]*$/, '') + "partslist_code.html";
}


function showPartNoList()
{
	d_list.src = d_list.src.replace(/[^/]*$/, '') + "partslist_no.html";
}

function showListByKey( key )
{
	d_list.contentWindow.showList(key);
}
