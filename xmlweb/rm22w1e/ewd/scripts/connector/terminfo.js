/*
   Copyright (c) 2002-2006 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/



var g_templateXslDoc;

var g_repairWireList;




function call_runOnLoadProc()
{
	try{
		

		g_repairWireList = new RepairWireList("connlist.xml");

		g_templateXslDoc = loadXML("../../styles/connector/termInfo.xsl");
		
		
		var termId = getURLParam("terminalId");
		var connNo = getURLParam("connNo");
		if( termId != null ){
			showTerminalInfo( termId, connNo );
		}
	}
	catch( e ){
		window.alert(e.stack);
	}
}



function showTerminalInfo(termId, connNo)
{
	var xmlDoc = createDOM("connector_list");
	var newNode = xmlDoc.documentElement;

	var connNode = g_repairWireList.getConnectorNode(connNo);

	var newConnNode = connNode.cloneNode(false);
	newNode.appendChild(newConnNode);

	if( getFirstElementByXPath(connNode.ownerDocument, "terminal", connNode).getAttribute("type") != "-" ){
		var termNode = g_repairWireList.getTerminalNode(connNo, termId);
		if( termNode != null ){
			newConnNode.appendChild(termNode);
		}
	}

	
	document.body.innerHTML = transformXmlWithXsl(xmlDoc, g_templateXslDoc);
}
