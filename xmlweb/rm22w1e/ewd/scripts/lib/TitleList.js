/*
   Copyright (c) 2003 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/





var TITLE_TYPE_SYSTEM  = "system";
var TITLE_TYPE_ROUTING = "routing";
var TITLE_TYPE_RELAY   = "relay";

var TITLE_SUFFIX_PLATE = "-Inn-P";
var TITLE_SUFFIX_INNER = "-Inn";






class TitleList {

constructor( path , type, subPath, pathPS )
{

	this.type = type;
	this.xmlDoc = null;
	this.titleArray = null;
	
	this.objGpList = null;
	this.objPSList = null;
	this.objAreaList = null;
	
	
	


	this.xmlDoc = loadXML( path );
	this.wordPack = new Title_WordPack(this.getLang());
	

	switch( type ){
		case TITLE_TYPE_SYSTEM:

			this.objGpList = new TitleListGP( subPath, this.wordPack );
			if( pathPS )	this.objPSList = new TitleListPS( pathPS, this.wordPack );
			if( getURLParam("category") == "overall"){

				this.titleArray = this.makeOverallTitleArray();
			}
			else{

				this.titleArray = this.makeSystemTitleArray();
			}
			break;
		
		case TITLE_TYPE_ROUTING:

		this.objAreaList = new AreaList( subPath );
		this.titleArray = this.makeRoutingTitleArray();		break;
		
		case TITLE_TYPE_RELAY:
		this.titleArray = this.makeRelayTitleArray();		break;
	}
}


getLang()
{
	return this.xmlDoc.documentElement.getAttribute("xml:lang");
}





filteringByTerm( term )
{
	if( term == null ) return;
	var filteredArray = new Array();
	
	var titleArray = this.titleArray;
	for( var i=0; i < titleArray.length; i++ ){
		if( titleArray[i].isInTerm(term) ){
			filteredArray[filteredArray.length] = titleArray[i];
		}
	}
	
	this.titleArray = filteredArray;
}



filteringGpByTerm( term )
{
	if( term == null ) return;
	var filteredArray = new Array();
	
	var titleArray = this.objGpList.titleArray;
	for( var i=0; i < titleArray.length; i++ ){
		if( titleArray[i].isInTerm(term) ){
			filteredArray[filteredArray.length] = titleArray[i];
		}
	}
	
	this.objGpList.titleArray = filteredArray;
}



filteringPsByTerm( term )
{
	if( term == null ) return;
	var filteredArray = new Array();
	
	var titleArray = this.objPSList.titleArray;
	for( var i=0; i < titleArray.length; i++ ){
		if( titleArray[i].isInTerm(term) ){
			filteredArray[filteredArray.length] = titleArray[i];
		}
	}
	
	this.objPSList.titleArray = filteredArray;
}






getExistScCodeArray()
{
	var scArray = new Array();
	
	for( var i=0; i < this.titleArray.length; i++ ){
		var code = this.titleArray[i].getSC();
		var bFound = false;
		for(var j=0; j < scArray.length; j++ ){
			if( scArray[j] == code ){
				bFound = true;
				break;
			}
		}
		if( bFound == false ){
			scArray[scArray.length] = code;
		}
	}
	
	return scArray;
}





makeRoutingGroupCodeArray()
{
	var groupCodeArray = new Array();
	
	for(var i=0; i < this.titleArray.length; i++ ){
		var groupCode = this.titleArray[i].getGroupCode();
		var bFound = false;
		for( var j=0; j < groupCodeArray.length; j++ ){
			if( groupCodeArray[j] == groupCode ){
				bFound = true;
				break;
			}
		}
		if( bFound == false ){
			groupCodeArray[groupCodeArray.length] = groupCode;
		}
	}
	
	return groupCodeArray;
}




getSystemTitleArrayBySC( sc )
{
	if( sc == null ) return this.titleArray;
	
	var retArray = new Array();
	
	for( var i=0; i < this.titleArray.length; i++ ){
		if( this.titleArray[i].getSC() == sc ){
			retArray[retArray.length] = this.titleArray[i];
		}
	}
	
	return retArray;
}




makeSystemTitleArray()
{
	var tagName = "System";
	
	var nodeList = this.xmlDoc.getElementsByTagName(tagName);
	var systemTitleArray = new Array(nodeList.length);
	for(var i=0; i < nodeList.length; i++ ){
		systemTitleArray[i] = new SystemTitle( nodeList[i] );
	}
	

	systemTitleArray = systemTitleArray.concat(this.objGpList.titleArray);
	
	return systemTitleArray;
}



makeOverallTitleArray()
{
	var tagName = "System";
	
	var nodeList = this.xmlDoc.getElementsByTagName(tagName);
	var overallTitleArray = new Array(nodeList.length);
	for(var i=0; i < nodeList.length; i++ ){
		overallTitleArray[i] = new OverallTitle( nodeList[i] );
	}
	
	return overallTitleArray;
}





makeRoutingTitleArray()
{
	var tagName = "Routing";
	
	var nodeList = this.xmlDoc.getElementsByTagName(tagName);
	var routingTitleArray = new Array(nodeList.length);
	for(var i=0; i < nodeList.length; i++ ){

		routingTitleArray[i] = new RoutingTitle( nodeList[i], this.objAreaList, this.wordPack );
	}
	
	return routingTitleArray;
}





makeRelayTitleArray()
{
	var tagName = "Relay";
	
	var nodeList = this.xmlDoc.getElementsByTagName(tagName);
	var relayTitleArray = new Array(nodeList.length);
	for(var i=0; i < nodeList.length; i++ ){
		relayTitleArray[i] = new RelayTitle( nodeList[i] );
	}
	
	return relayTitleArray;
}




getTitleArray()
{
	return this.titleArray;
}




getTitleObjByCode( code )
{
	if( code == null ) return null;
	
	var titleObj = null;
	for( var i=0; i < this.titleArray.length; i++ ){
		if( this.titleArray[i].getCode() == code ){
			titleObj = this.titleArray[i];
			break;
		}
	}
	
	return titleObj;
}


getTitleObjByBaseFig( baseFig )
{
	if( baseFig == null ) return null;
	
	var titleObj = null;
	for( var i=0; i < this.titleArray.length; i++ ){
		if( this.titleArray[i].getBaseFig() == baseFig ){
			titleObj = this.titleArray[i];
			break;
		}
	}
	
	return titleObj;
}




















}

class SystemTitle {

constructor( node )
{

	this.titleNode = node;
}





getSC()
{
	var sc = this.titleNode.getAttribute("sc");
	
	return sc;
}




getCode()
{
	var code = getFirstElementByXPath(this.titleNode.ownerDocument, "name", this.titleNode).getAttribute("code");
	
	return code;
}





getName()
{
	var name = getFirstElementByXPath(this.titleNode.ownerDocument, "name", this.titleNode).textContent;
	
	return makeNameWithSpec(name, this.getSpec());
}



getSpec()
{
	var specNode = getFirstElementByXPath(this.titleNode.ownerDocument, "spec", this.titleNode);
	if( specNode != null ){
		return specNode.textContent;
	} else {
		return "";
	}
}



getBaseFig()
{
	var figNode = getFirstElementByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	var figName = figNode.textContent;
	

	var pageIndex = figName.search(/_\d$/);
	if( pageIndex != -1 ){
		figName = figName.slice(0, pageIndex);
	}
	
	return figName;
}




getFigTextArray()
{
	var figTextArray = new Array();
	
	var figNodes = getElementsByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	for (var i=0; i < figNodes.length; i++) {
		figTextArray[figTextArray.length] = figNodes[i].textContent;
	}
	
	return figTextArray;
}





getFigFileName(figNo)
{
	var figIndex = 0;
	if(isNaN(parseInt(figNo)) == false){
		figIndex = parseInt(figNo) - 1;
	}
	
	var figNodes = getElementsByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	var fileName = figNodes[figIndex].textContent;
	var ext = figNodes[figIndex].getAttribute("type");
	fileName = fileName + "." + (ext || "svgz");
	
	return fileName;
}



getPdfFileName()
{
	var ext = "pdf";
	var fileName = this.getBaseFig() + "." + ext;
	
	return fileName;
}



isInTerm( term )
{
	if( term == null ) return true;
	
	var termNodes = getElementsByXPath(this.titleNode.ownerDocument, "term", this.titleNode);
	for( var i=0; i < termNodes.length; i++ ){
		var from = parseInt(termNodes[i].getAttribute("from"));

		if( from == null ) continue;
		if( from > term ) continue;
		
		var to = parseInt(termNodes[i].getAttribute("to"));

		if( to != null ){
			if( to < term ) continue;
		}
		
		return true;
	}
	
	return false;
}



getType()
{
	return TITLE_TYPE_SYSTEM;
}















}

class OverallTitle {

constructor( node )
{

	this.titleNode = node;
}





getSC()
{
	var sc = this.titleNode.getAttribute("sc");
	
	return sc;
}




getCode()
{
	var code = getFirstElementByXPath(this.titleNode.ownerDocument, "name", this.titleNode).getAttribute("code");
	
	return code;
}





getName()
{
	var name = getFirstElementByXPath(this.titleNode.ownerDocument, "name", this.titleNode).textContent;
	
	return makeNameWithSpec(name, this.getSpec());
}



getSpec()
{
	var specNode = getFirstElementByXPath(this.titleNode.ownerDocument, "spec", this.titleNode);
	if( specNode != null && specNode.textContent.charAt(0) != "*"){
		return specNode.textContent;
	} else {
		return "";
	}
}



getSpecList()
{
	var arraySpec = new Array();
	var specNodeList = this.titleNode.getElementsByTagName("spec");
	for( var i=0; i < specNodeList.length; i++ ){
		if( specNodeList[i].textContent.charAt(0) == '*'
			|| specNodeList[i].textContent.charAt(0) == '-' ){

			arraySpec[arraySpec.length] = specNodeList[i].textContent;
		}
	}
	return arraySpec

}


getBaseFig()
{
	var figNode = getFirstElementByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	var figName = figNode.textContent;
	

	var pageIndex = figName.search(/_\d$/);
	if( pageIndex != -1 ){
		figName = figName.slice(0, pageIndex);
	}
	
	return figName;
}




getFigTextArray()
{
	var figTextArray = new Array();
	
	var figNodes = getElementsByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	for (var i=0; i < figNodes.length; i++) {
		figTextArray[figTextArray.length] = figNodes[i].textContent;
	}
	
	return figTextArray;
}





getFigFileName(figNo)
{
	var figIndex = 0;
	if(isNaN(parseInt(figNo)) == false){
		figIndex = parseInt(figNo) - 1;
	}
	
	var figNodes = getElementsByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	var fileName = figNodes[figIndex].textContent;
	var ext = figNodes[figIndex].getAttribute("type");
	fileName = fileName + "." + (ext || "svgz");
	
	return fileName;
}



getPdfFileName()
{
	var ext = "pdf";
	var fileName = this.getBaseFig() + "." + ext;
	
	return fileName;
}



isInTerm( term )
{
	if( term == null ) return true;
	
	var termNodes = getElementsByXPath(this.titleNode.ownerDocument, "term", this.titleNode);
	for( var i=0; i < termNodes.length; i++ ){
		var from = parseInt(termNodes[i].getAttribute("from"));

		if( from == null ) continue;
		if( from > term ) continue;
		
		var to = parseInt(termNodes[i].getAttribute("to"));

		if( to != null ){
			if( to < term ) continue;
		}
		
		return true;
	}
	
	return false;
}



getType()
{
	return TITLE_TYPE_SYSTEM;
}




























}

class RoutingTitle {

constructor( node, areaList, wordPack )
{

	this.titleNode = node;
	this.objAreaList = areaList;
	this.txtIn = " " + wordPack.txtIn + " ";
}





getGroupCode()
{
	var groupCode = this.getSingleName();
	return groupCode;
}





getCode()
{
	var code = getFirstElementByXPath(this.titleNode.ownerDocument, "name", this.titleNode).getAttribute("code");
	return code;
}





getTitleName()
{
	var sectionCode = this.titleNode.getAttribute("section");
	var titleName = this.objAreaList.getNameByCode( sectionCode );
	
	titleName = makeNameWithSpec(titleName, this.getSpec());
	
	var note = getFirstElementByXPath(this.titleNode.ownerDocument, "note", this.titleNode);
	if( note != null ){
		if( note.textContent != "" )
			titleName = titleName + " " + note.textContent;
	}
	
	return titleName;
}



getRoutingNote()
{
	return this.titleNode.getAttribute("note");
}



getSingleName()
{
	var name = getFirstElementByXPath(this.titleNode.ownerDocument, "name", this.titleNode).textContent;
	return name;
}




getName()
{

	var titleName = this.getSingleName() + this.txtIn + this.getTitleName();
	return titleName;
}



getSpec()
{
	var specNode = getFirstElementByXPath(this.titleNode.ownerDocument, "spec", this.titleNode);
	if( specNode != null ){
		return specNode.textContent;
	} else {
		return "";
	}
}



getBaseFig()
{
	var figNode = getFirstElementByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	var figName = figNode.textContent;
	

	var pageIndex = figName.search(/_\d$/);
	if( pageIndex != -1 ){
		figName = figName.slice(0, pageIndex);
	}
	
	return figName;
}




getFigTextArray()
{
	var figTextArray = new Array();
	
	var figNodes = getElementsByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	for (var i=0; i < figNodes.length; i++) {
		figTextArray[figTextArray.length] = figNodes[i].textContent;
	}
	
	return figTextArray;
}





getFigFileName(figNo)
{
	var figIndex = 0;
	if(isNaN(parseInt(figNo)) == false){
		figIndex = parseInt(figNo) - 1;
	}
	
	var figNodes = getElementsByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	var fileName = figNodes[figIndex].textContent;
	var ext = figNodes[figIndex].getAttribute("type");
	fileName = fileName + "." + (ext || "svgz");
	
	return fileName;
}



getPdfFileName()
{
	var ext = "pdf";
	var fileName = this.getBaseFig() + "." + ext;
	
	return fileName;
}



isInTerm( term )
{
	if( term == null ) return true;
	
	var termNodes = getElementsByXPath(this.titleNode.ownerDocument, "term", this.titleNode);
	
	for( var i=0; i < termNodes.length; i++ ){
		var from = parseInt(termNodes[i].getAttribute("from"));

		if( from == null ) continue;
		if( from > term ) continue;
		
		var to = parseInt(termNodes[i].getAttribute("to"));

		if( to != null ){
			if( to < term ) continue;
		}
		
		return true;
	}
	
	return false;
}





getType()
{
	return TITLE_TYPE_ROUTING;
}




























}

class RelayTitle {

constructor( node )
{

	this.titleNode = node;
}






getCode()
{
	var code = getFirstElementByXPath(this.titleNode.ownerDocument, "name", this.titleNode).getAttribute("code");
	
	return code;
}





getName()
{
	var name = getFirstElementByXPath(this.titleNode.ownerDocument, "name", this.titleNode).textContent;
	
	return makeNameWithSpec(name, this.getSpec());
}



getSpec()
{
	var specNode = getFirstElementByXPath(this.titleNode.ownerDocument, "spec", this.titleNode);
	if( specNode != null ){
		return specNode.textContent;
	} else {
		return "";
	}
}


getNote()
{
	var elmNote = getFirstElementByXPath(this.titleNode.ownerDocument, "note", this.titleNode);
	if( elmNote != null ){
		return elmNote.textContent;
	} else {
		return "";
	}
}



getBaseFig()
{
	var figNode = getFirstElementByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	var figName = figNode.textContent;
	

	var pageIndex = figName.search(/_\d$/);
	if( pageIndex != -1 ){
		figName = figName.slice(0, pageIndex);
	}
	
	return figName;
}



getFigTextArray()
{
	var figTextArray = new Array();
	
	var figNodes = getElementsByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	for (var i=0; i < figNodes.length; i++) {
		figTextArray[figTextArray.length] = figNodes[i].textContent;
	}
	
	return figTextArray;
}




getFigFileName(figNo)
{
	var figIndex = 0;
	if(isNaN(parseInt(figNo)) == false){
		figIndex = parseInt(figNo) - 1;
	}
	
	var figNodes = getElementsByXPath(this.titleNode.ownerDocument, "fig", this.titleNode);
	var fileName = figNodes[figIndex].textContent;
	var ext = figNodes[figIndex].getAttribute("type");
	fileName = fileName + "." + (ext || "svgz");
	
	return fileName;
}




getPdfFileName()
{
	var ext = "pdf";
	var fileName = this.getBaseFig() + "." + ext;
	
	return fileName;
}




getRelayType()
{
	var type = this.titleNode.getAttribute("type");
	return type;
}


getLocation()
{
	var location = this.titleNode.getAttribute("location");
	return location;
}


getObject()
{
	var object = this.titleNode.getAttribute("object");
	return object;
}


getInner()
{
	var inner = this.titleNode.getAttribute("inner");
	return inner;
}


makeObjectCode()
{
	if( this.isPlate() ){
		var code = this.getCode();
		code = code.substr(0, code.length - TITLE_SUFFIX_PLATE.length);
		return code;
	}
	else{
		return this.getObject();
	}
}


makeInnerCode()
{
	if( this.isPlate() ){
		var code = this.getCode();
		code = code.substr(0, code.length - TITLE_SUFFIX_PLATE.length);
		code = code + TITLE_SUFFIX_INNER;
		return code;
	}
	else{
		return this.getInner();
	}
}


makePlateCode()
{
	if( this.isPlate() ){
		return this.getCode();
	}
	else if( this.isInner() ){
		return this.getObject() + TITLE_SUFFIX_PLATE;
	}
	else{
		return this.getCode() + TITLE_SUFFIX_PLATE;
	}
}


isPlate()
{


	var code = this.getCode();
	var index = code.lastIndexOf(TITLE_SUFFIX_PLATE);
	if( index != -1 ){
		if( code.substr(index) == TITLE_SUFFIX_PLATE )
			return true;
	}
	return false;
}


isInner()
{
	var type = this.getRelayType();
	if( type == "inner" )
		return true;
	else
		return false;
}


hasInner()
{
	var inner = this.getInner();
	if( inner != null && inner != "" )
		return true;
	else
		return false;
}


isInTerm( term )
{
	if( term == null ) return true;
	
	var termNodes = getElementsByXPath(this.titleNode.ownerDocument, "term", this.titleNode);
	
	for( var i=0; i < termNodes.length; i++ ){
		var from = parseInt(termNodes[i].getAttribute("from"));

		if( from == null ) continue;
		if( from > term ) continue;
		
		var to = parseInt(termNodes[i].getAttribute("to"));

		if( to != null ){
			if( to < term ) continue;
		}
		
		return true;
	}
	
	return false;
}


getSingleName()
{
	var name = getFirstElementByXPath(this.titleNode.ownerDocument, "name", this.titleNode).textContent;
	
	return name;
}



getType()
{
	return TITLE_TYPE_RELAY;
}






















}

class Title_WordPack {

constructor(lang)
{
	this.gp = null;
	this.ps = "";
	this.txtIn = null;
	
	this.init( lang );
}

init(lang)
{
	switch( lang )
	{
		case "fr":
			this.gp = "Point de mise à terre";
			this.ps = "Source d'alimentation";
			this.txtIn = "dans la";
			break;
		
		case "de":
			this.gp = "Massepunkt";
			this.ps = "Stromquelle";
			this.txtIn = "im";
			break;
		
		case "es":
			this.gp = "Punto a tierra";
			this.ps = "Alimentación";
			this.txtIn = "en el";
			break;
		
		case "en":
		default:
			this.gp = "Ground Point";
			this.ps = "Power Source";
			this.txtIn = "in";
			break;
	}
}











}

class TitleListGP {

constructor(path, wordPack)
{

	this.xmlDoc = null;
	this.titleArray = null;
	this.loaded = false;
	
	this.xmlDoc = loadXML(path);
	this.loaded = this.xmlDoc != null;
	if( this.loaded ){
		this.titleArray = this.makeTitleArray();
	}
	else{
		this.xmlDoc = new DOMParser().parseFromString("<root></root>","text/xml");
		this.titleArray = this.makeTitleArray4NoData(wordPack);
	}
}

makeTitleArray()
{
	var tagName = "Item";
	
	var nodeList = this.xmlDoc.getElementsByTagName(tagName);
	var gpTitleArray = new Array(nodeList.length);
	for(var i=0; i < nodeList.length; i++ ){
		gpTitleArray[i] = new SystemTitle( nodeList[i] );
	}
	
	return gpTitleArray;
}

makeTitleArray4NoData(wordPack)
{
	var GP_CODE = "GP";
	
	
	var elmItem = this.xmlDoc.createElement("Item");
	
	var elmName = this.xmlDoc.createElement("name");
	elmName.setAttribute("code", GP_CODE);
	elmName.textContent = wordPack.gp;
	
	var elmFig = this.xmlDoc.createElement("fig");
	elmFig.setAttribute("type", "svgz");
	elmFig.textContent = GP_CODE;
	
	elmItem.appendChild(elmName);
	elmItem.appendChild(elmFig);
	
	
	


	var gpTitleArray = new Array();
	gpTitleArray[0] = new SystemTitle( elmItem );
	
	return gpTitleArray;
}






}

class TitleListPS {

constructor(path, wordPack)
{

	this.xmlDoc = null;
	this.titleArray = null;
	this.loaded = false;
	
	this.xmlDoc = loadXML(path);
	this.loaded = this.xmlDoc != null;
	if( this.loaded ){
		this.titleArray = this.makeTitleArray();
	}
	else{
		this.xmlDoc = new DOMParser().parseFromString("<root></root>","text/xml");
		this.titleArray = this.makeTitleArray4NoData(wordPack);
	}
}

makeTitleArray()
{
	var tagName = "Item";
	
	var nodeList = this.xmlDoc.getElementsByTagName(tagName);
	var psTitleArray = new Array(nodeList.length);
	for(var i=0; i < nodeList.length; i++ ){
		psTitleArray[i] = new SystemTitle( nodeList[i] );
	}
	
	return psTitleArray;
}

makeTitleArray4NoData(wordPack)
{
	var PS_CODE = "PS";
	
	
	var elmItem = this.xmlDoc.createElement("Item");
	
	var elmName = this.xmlDoc.createElement("name");
	elmName.setAttribute("code", PS_CODE);
	elmName.textContent = wordPack.ps;
	
	var elmFig = this.xmlDoc.createElement("fig");
	elmFig.setAttribute("type", "svgz");
	elmFig.textContent = PS_CODE;
	
	elmItem.appendChild(elmName);
	elmItem.appendChild(elmFig);
	
	
	


	var psTitleArray = new Array();
	psTitleArray[0] = new SystemTitle( elmItem );
	
	return psTitleArray;
}







}

class AreaList {

constructor(path)
{

	this.xmlDoc = null;
	


	this.xmlDoc = loadXML( path );
}

getNameByCode( code )
{
	var name = null;
	
	var tagName = "Area";
	var nodeList = this.xmlDoc.getElementsByTagName(tagName);
	for( var i=0; i < nodeList.length; i++ ){
		if( nodeList[i].getAttribute("code") == code ){
			name = nodeList[i].textContent;
			break;
		}
	}
	
	return name;
}

}

function makeNameWithSpec(name, spec){
	if( spec != "" )
		return name + " (" + spec + ")";
	else
		return name;
}


function compareTitleObj(val1, val2)
{
	var name1 = val1.getName();
	var name2 = val2.getName();
	
	if( name1 < name2 )			return -1;
	else if( name1 > name2 )	return 1;
	else						return 0;
}
