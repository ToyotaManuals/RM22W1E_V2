/*
   Copyright (c) 2003 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/



class ServiceCategory {

constructor(path)
{

	this.xmlDoc = null;
	


	this.xmlDoc = loadXML( path );
}





getCategoryArrayByCodeArray( codeArray )
{
	var categoryArray = new Array();
	
	var nodeList = this.xmlDoc.getElementsByTagName("Category");
	for( var i=0; i < nodeList.length; i++ ){
		var code = nodeList[i].getAttribute("code");
		for( var j=0; j < codeArray.length; j++ ){
			if( code == codeArray[j] ){
				var category = new Category();
				category.init(nodeList[i]);
				categoryArray[categoryArray.length] = category;
				break;
			}
		}
	}
	
	return categoryArray;
}




getCategory( code )
{
	if( code == null ) return null;
	
	var categoryObj = null;
	
	var tagName = "Category";
	var nodeList = this.xmlDoc.getElementsByTagName(tagName);
	for( var i=0; i < nodeList.length; i++ ){
		if( nodeList[i].getAttribute("code") == code ){
			categoryObj = new Category();
			categoryObj.init(nodeList[i]);
			break;
		}
	}
	
	return categoryObj;
}

}









class Category {

constructor( code, name )
{

	this.code = code;
	this.name = name;
}




init( node )
{
	this.code = node.getAttribute("code");
	this.name = node.textContent;
}

}
