/*
   Copyright (c) 2003 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/

class EwdRefs {

constructor(objEwdPartsList)
{

	this.m_objPartsArray = new Array();
	


	this.makePartsArray(objEwdPartsList);
	


	this.routingCodeListMem = null;
}


makePartsArray( objEwdPartsList )
{
	for( var i=0; i < objEwdPartsList.ewdPartsArray.length; i++ ){
		var type = objEwdPartsList.ewdPartsArray[i].getType();
		var code = objEwdPartsList.ewdPartsArray[i].getCode();
		var subcode = objEwdPartsList.ewdPartsArray[i].getSubCode();
		if( this.searchSameParts(type, code, subcode) == null ){
			this.addPartsInfo(type, code, subcode);
		}
	}
}


searchSameParts(type, code, subcode)
{
	for( var i=0; i < this.m_objPartsArray.length; i++){
		if( this.m_objPartsArray[i].equals(type, code, subcode) ){
			return this.m_objPartsArray[i];
		}
	}
	
	return null;
}

addPartsInfo(type, code, subcode)
{
	var objParts = new EwdRefs_Parts(type, code, subcode);
	this.m_objPartsArray.push(objParts);
	
	return objParts;
}



appendRefsInfo( codesListObjArray )
{
	var titleObjArray = this.makeTitleObjArray( codesListObjArray );
	
	for(var i=0; i < codesListObjArray.length; i++){
		var codesDataArray = codesListObjArray[i].codesDataArray;
		for( var j=0; j < codesDataArray.length; j++ ){
			var itemArray = codesDataArray[j].selectEwdItem();
			for( var k=0; k < itemArray.length; k++ ){
				var type    = itemArray[k].type;
				var code    = itemArray[k].code;
				var subcode = itemArray[k].subcode;
				
				var objParts = this.searchSameParts(type, code, subcode);
				if( objParts == null ){
					continue;
					confirm("EwdRefs.appendRefsInfo(): Not Found Parts." + " - " + itemArray[k]);
				}
				
				objParts.addTitleInfo(titleObjArray[i]);
			}
		}
	}
}



appendRefsInfoTest( codesListObjArray )
{
	if( this.routingCodeListMem == null )	this.routingCodeListMem = codesListObjArray;
}


makeLinkInfoToRouting(type1, code1, subcode1)
{
	if( this.routingCodeListMem == null )	return null;
	var codesListObjArray = this.routingCodeListMem
	
	var titleObjArray = this.makeTitleObjArray( codesListObjArray );
	
	for(var i=0; i < codesListObjArray.length; i++){
		var codesDataArray = codesListObjArray[i].codesDataArray;
		
		for( var j=0; j < codesDataArray.length; j++ ){
			var itemArray = codesDataArray[j].selectEwdItem();
			for( var k=0; k < itemArray.length; k++ ){
				var type    = itemArray[k].type;
				var code    = itemArray[k].code;
				var subcode = itemArray[k].subcode;
				
				if( code != code1 )	continue;
				if( type != type1 )	continue;
				if( subcode != subcode1 )	continue;

				var objParts = this.searchSameParts(type, code, subcode);
				if( objParts == null ){
					continue;
					confirm("EwdRefs.appendRefsInfo(): Not Found Parts." + " - " + itemArray[k]);
				}
				
				objParts.addTitleInfo(titleObjArray[i]);
			}
		}
	}
	
	
}



makeTitleObjArray( codesListObjArray )
{
	var titleObjArray = new Array();
	
	for( var i=0; i < codesListObjArray.length; i++ ){
		var titleObj = new EwdRefs_EwdTitle( codesListObjArray[i].type, codesListObjArray[i].code );
		titleObjArray[titleObjArray.length] = titleObj;
	}
	
	return titleObjArray;
}




makeRefsPartsObj( type, code, subcode )
{
	for( var i=0; i < this.m_objPartsArray.length; i++ ){
		if( this.m_objPartsArray[i].equals(type, code, subcode) ){
			return new RefsParts( this.m_objPartsArray[i] );
		}
	}
	
	return null;
}









}

class EwdRefs_EwdTitle {

constructor(type, code)
{
	this.type = type;
	this.code = code;
}

equals(tmpType, tmpCode)
{

	if( this.code == tmpCode ){
		if( this.type == tmpType ){
			return true;
		}
	}
	
	return false;
}



}

class EwdRefs_Parts {

constructor(type, code, subcode)
{
	this.code = code;
	this.type = type;
	this.subcode = subcode;
	this.ewdTitleArray = new Array();
}


equals(type, code, subcode)
{

	if( this.code != code )
		return false;
	if( adjustType(this.type) != adjustType(type) )
		return false;
	if( this.subcode != subcode )
		return false;
	
	return true;
}


addTitleInfo( titleObj )
{
	for(var i=0; i < this.ewdTitleArray.length; i++ ){

		if( this.ewdTitleArray[i].equals( titleObj.type, titleObj.code )){
			return;
		}
	}
	
	this.ewdTitleArray[this.ewdTitleArray.length] = titleObj;
}








}

class RefsParts {

constructor( tmpObjParts )
{

	this.objParts = tmpObjParts;
}


getTitleObjArray( titleList )
{
	var titleObjArray = new Array();
	
	for( var i=0; i < this.objParts.ewdTitleArray.length; i++ ){
		if( this.objParts.ewdTitleArray[i].type != titleList.type ) continue;
		
		var ewdCode = this.objParts.ewdTitleArray[i].code;
		var tmpObj = titleList.getTitleObjByCode( ewdCode );
		if( tmpObj != null ){
			titleObjArray[titleObjArray.length] = tmpObj;
		}
	}
	
	return titleObjArray;
}

}
