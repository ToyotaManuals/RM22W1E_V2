/*
   Copyright (c) 2003 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/


class EwdPartsList {

constructor( path )
{

	this.xmlDoc = null;
	this.ewdPartsArray = new Array();
	this.relayPartsArray = new Array();
	this.partsArray = null;
	
	


	this.xmlDoc = loadXML( path );
	this.wordPack = new Parts_WordPack(this.getLang());
	
	this.ewdPartsArray = this.makeEwdPartsArray();
	this.updatePartsArray();
}

updatePartsArray()
{
	this.partsArray = this.ewdPartsArray.concat(this.relayPartsArray);
}


appendRelayPartsInfo( relayTitleList )
{
	for( var i=0; i < relayTitleList.titleArray.length; i++ ){
		this.relayPartsArray[this.relayPartsArray.length] = new EwdRelayParts(relayTitleList.titleArray[i]);
	}
	
	this.updatePartsArray();
}




makeEwdPartsArray()
{
	var tagName = "CodedItem";
	
	var nodeList = this.xmlDoc.getElementsByTagName(tagName);
	var itemClsArray = new Array(nodeList.length);
	for(var i=0; i < nodeList.length; i++ ){
		itemClsArray[i] = new EwdParts( nodeList[i] );
	}
	
	return itemClsArray;
}


getItemArray( type, code, subcode, flgSub )
{
	var retArray = new Array();
	
	if( type == "sp" || type == "gp" ){
		retArray[retArray.length] = new EwdItemParts( type, code, "", this.wordPack );
	}
	else
	{
		for( var i=0; i < this.partsArray.length; i++ ){
			var tmpParts = this.partsArray[i];
			if( this.isSameParts( tmpParts, type, code, subcode, flgSub ) ){
				retArray[retArray.length] = tmpParts;
			}
		}
	}
	
	return retArray;
}



getItemArrayByPartNo( partNo )
{
	var retArray = new Array();
	for( var i = 0; i < this.partsArray.length; i++ ){
		var tmpParts = this.partsArray[i];
		if( tmpParts.isConnectorInUse( partNo ) ){
				retArray[retArray.length] = tmpParts;
		}
	}

	return retArray;
}



getItem( type, code, subcode, flgSub )
{
	var retParts = null;
	
	if( type == "sp" || type == "gp" ){
		retParts = new EwdItemParts( type, code, "", this.wordPack );
	}
	else
	{
		for( var i=0; i < this.partsArray.length; i++ ){
			var tmpParts = this.partsArray[i];
			if( this.isSameParts( tmpParts, type, code, subcode, flgSub ) ){
				retParts = tmpParts;
			}
		}
	}
	
	return retParts;
}



makeItem( type, code, name )
{
	return new EwdItemParts( type, code, name, this.wordPack );
}




isSameParts( objEwdParts, type, code, subcode, flgSub )
{
	if( objEwdParts.getCode() != code )
		return false;
	if( adjustType(objEwdParts.getType()) != adjustType(type) )
		return false;
	if( flgSub ){
		if( objEwdParts.getSubCode() != subcode ){
			return false;
		}
	}
	
	return true;
}


getLang()
{
	return this.xmlDoc.documentElement.getAttribute("xml:lang");
}



}

class EwdParts {

constructor( node )
{

	this.ewdPartsNode = node;
}


getCode()
{
	var code = this.ewdPartsNode.getAttribute("code");
	return code;
}

getType()
{
	var type = this.ewdPartsNode.getAttribute("type");
	return type;
}

getName()
{
	var name = getFirstElementByXPath(this.ewdPartsNode.ownerDocument, "name", this.ewdPartsNode).textContent;
	return name;
}


getNameCode()
{
	var code = getFirstElementByXPath(this.ewdPartsNode.ownerDocument, "name", this.ewdPartsNode).getAttribute("code");
	return code;
}

getNo()
{
	var no = getFirstElementByXPath(this.ewdPartsNode.ownerDocument, "partNo", this.ewdPartsNode).textContent;
	return no;
}


hasRepairInfo()
{
	try{
		var strRepairInfo = getFirstElementByXPath(this.ewdPartsNode.ownerDocument, "partNo", this.ewdPartsNode).getAttribute("repairInfo");
		return strRepairInfo == "Y";
	}
	catch(e){
		return false;
	}
}

getColor()
{
	var color = getFirstElementByXPath(this.ewdPartsNode.ownerDocument, "color", this.ewdPartsNode).textContent;
	return color;
}

getSpec()
{
	var spec = getFirstElementByXPath(this.ewdPartsNode.ownerDocument, "spec", this.ewdPartsNode).textContent;
	return spec;
}


getRepair()
{
	var repair = getFirstElementByXPath(this.ewdPartsNode.ownerDocument, "repairWire", this.ewdPartsNode).textContent;
	return repair;
}


getFigFileName()
{
	var fig = getFirstElementByXPath(this.ewdPartsNode.ownerDocument, "fig", this.ewdPartsNode).textContent;
	if( fig != null && fig != "" ){
		fig = fig + "." + (getFirstElementByXPath(this.ewdPartsNode.ownerDocument, "fig", this.ewdPartsNode).getAttribute("type") || "svgz");
	}
	return fig;
}


getSubCode()
{
	var code = this.ewdPartsNode.getAttribute("subcode");
	return (code != null) ? code : "";

}


getDispCode()
{
	var subcode = this.getSubCode();
	if( subcode == "" )
		return this.getCode();
	else
		return this.getCode() + "(" + subcode + ")";
}


isConnectorInUse(partNo)
{
	var no = this.getNo();
	if( no.indexOf(partNo) == -1 )	return false;
	return true;

}
}

class EwdRelayParts {

hasRepairInfo()
{
	return false;
}

constructor( relayTitleObj )
{

	this.titleObj = relayTitleObj;
}


getCode()
{

	var code = this.titleObj.getNote();
	return code;
}

getType()
{
	var type = this.titleObj.getType();
	return type;
}

getName()
{

	var name = this.titleObj.getSingleName();
	return name;
}


getNameCode()
{
	return "";
}

getNo()
{
	return "";
}

getColor()
{
	return "";
}

getSpec()
{
	return "";
}


getRepair()
{
	return "";
}

getFigFileName()
{
	return "";
}


getSubCode()
{
	return "";
}


getDispCode()
{
	var subcode = this.getSubCode();
	if( subcode == "" )
		return this.getCode();
	else
		return this.getCode() + "(" + subcode + ")";
}


getTitleCode()
{
	return this.titleObj.getCode();
}

}

class EwdItemParts {

hasRepairInfo()
{
	return false;
}

constructor(  type, code, name, wordPack )
{

	this.type = type;
	this.code = code;
	this.name = name;
	
	if( this.type == "gp" ) this.name = wordPack.gp;
	if( this.type == "sp" ) this.name = wordPack.sp;
}

getCode()
{
	return this.code;
}

getType()
{
	return this.type;
}

getName()
{
	return this.name;
}


getNameCode()
{
	return "";
}

getNo()
{
	return "";
}

getColor()
{
	return "";
}

getSpec()
{
	return "";
}


getRepair()
{
	return "";
}

getFigFileName()
{
	return "";
}


getSubCode()
{
	return "";
}


getDispCode()
{
	var subcode = this.getSubCode();
	if( subcode == "" )
		return this.getCode();
	else
		return this.getCode() + "(" + subcode + ")";
}


}

class Parts_WordPack {

constructor(lang)
{
	this.gp = null;
	this.sp = null;
	
	this.init( lang );
}

init(lang)
{
	switch( lang )
	{
		case "fr":
			this.gp = "Points de mise à terre";
			this.sp = "Points d'épissage";
			break;
		
		case "de":
			this.gp = "Masseanschlüsse";
			this.sp = "Verbindungsstellen";
			break;
		
		case "es":
			this.gp = "Puntos a tierra";
			this.sp = "Puntos de la unión";
			break;
		
		case "en":
		default:
			this.gp = "Ground Point";
			this.sp = "Splice Point";
			break;
	}
}

}
