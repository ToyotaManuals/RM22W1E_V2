/*
   Copyright (c) 2002 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/

function transformXmlWithXsl(xmlDoc, xslDoc, args = null)
{
	var transform = new XSLTProcessor();
	transform.importStylesheet(xslDoc);
	if (args != null) {
		var keys = args.keys();
		for (var i=0; i<args.size(); i++) {
			var val = args.get(keys[i]);
			transform.setParameter(null, keys[i], args.get(keys[i]).toString());
		}
	}
	var resultTree = transform.transformToDocument(xmlDoc);

	return resultTree.documentElement.innerHTML;
}


function getElementsByXPath(document, xpath, parent)
{
    let results = [];
    let query = document.evaluate(xpath, parent || document, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
    var nextResult = query.iterateNext();
	while (nextResult != null) {
        results.push(nextResult);
        nextResult = query.iterateNext();
    }
    return results;
}

function getFirstElementByXPath(document, xpath, parent)
{
    let result = document.evaluate(xpath, parent || document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
	if (result.resultType != XPathResult.FIRST_ORDERED_NODE_TYPE)
	{
		return null;
	}
	return result.singleNodeValue;
}






function loadXML( path )
{
	if (path == "")
	{
		return null;
	}
	var xhttp=new XMLHttpRequest();
	xhttp.open("GET",path,false);
	xhttp.send();
	if (xhttp.status != 200)
	{
		return null;
	}
	var xmlDoc = xhttp.responseXML;
	var responseString = (new XMLSerializer()).serializeToString(xmlDoc);
	responseString = responseString.replace('/>\s*/g', '>').replace('/\s*</g', '<');
	var parser = new DOMParser();
	var xmlDocument = parser.parseFromString(responseString,"text/xml");

	if (path.endsWith(".xsl"))
	{
		var pathComponents = path.split('/');
		var xslDirectory = pathComponents[pathComponents.length-2];

		var includeNodes = xmlDocument.getElementsByTagName("xsl:include");
		for (i = 0; i < includeNodes.length; i++) {
			includeNodes[i].setAttribute("href", "../../styles/" + xslDirectory + "/" + includeNodes[i].getAttribute("href"));
		}
	}

	return xmlDocument;
}



function createDOM( rootName )
{
	var parser = new DOMParser();
	var xmlDocument = parser.parseFromString("<" + rootName + "></" + rootName + ">","text/xml");
	return xmlDocument;
}
