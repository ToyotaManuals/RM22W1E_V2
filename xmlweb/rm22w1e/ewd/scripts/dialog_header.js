/*
   Copyright (c) 2002-2007 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/

var type = 1;

function call_runOnLoadProc()
{
	try{
		switch( window.top.document.getElementById('dialog-body').dialogArguments[1] ){

			case "repair_wire":
				type = 1;
				break;


			case "terminal_info":
				type = 2;
				break;
		}
	}
	catch(e){
		window.alert(e.stack);
	}

}

function call_runOnClickClose()
{
	try{
		window.top.document.getElementsByTagName('dialog')[0].close();
	}
	catch( e ){
		window.alert(e.stack);
	}
}


function call_runOnClickPrint()
{
	try{
		parent.d_preview.contentWindow.focus();

		parent.d_preview.contentWindow.print();
	}
	catch( e ){
		window.alert(e.stack);
	}
}


function call_runOnClickHowTo()
{

	var top = 0;
	//if( type == 1 )	top = 280;

	var path = "intro-wh/intro/index.html"
	var style = "dialogWidth:900px; dialogHeight:600px; dialogTop:" + top + "px; resizable=yes; help=no;";
	openDialog(window, path, null, style );

}
